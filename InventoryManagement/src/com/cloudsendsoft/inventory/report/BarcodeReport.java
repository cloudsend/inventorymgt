/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.report;

import com.cloudsendsoft.inventory.bean.SalesBean;
import com.cloudsendsoft.inventory.bean.SalesPreformaBean;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.datasource.SalesDataSource;
import com.cloudsendsoft.inventory.datasource.SalesPreformaDataSource;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesCharge;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaCharge;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesPreformaTax;
import com.cloudsendsoft.inventory.model.SalesTax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.imageio.ImageIO;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class BarcodeReport {

    static final Logger log = Logger.getLogger(BarcodeReport.class.getName());
    CommonService commonService = new CommonService();

    public boolean createBarcodeReport(String itemCode,String sellingPrice,String format) {
        JasperPrint jasperPrint;
        JasperReport jasperReport;
        HashMap<String, Object> jasperParameter = new HashMap<String, Object>();
        try {
            //System.out.println("barcode:"+itemCode);
            jasperParameter.put("barcode",itemCode );
            jasperParameter.put("MRP","MRP: Rs."+commonService.formatIntoCurrencyAsString(Double.parseDouble(sellingPrice.trim())));

            String applicationPath = new File(".").getAbsolutePath();
            //String layoutFile="";
            InputStream layoutFile=null;
            if(format.equalsIgnoreCase("print")){
                //layoutFile = applicationPath + "\\jrxml\\barcode.jrxml";
                layoutFile = this.getClass().getClassLoader().getResourceAsStream("Repeat_barcode.jrxml");
            }else if(format.equalsIgnoreCase("png")){
               // layoutFile = applicationPath + "\\jrxml\\barcode_exportImage.jrxml";
                //InputStream imgInputStream =new InputStream(new File(""));
                //jasperParameter.put("barCodeImage", imgInputStream);
                //BufferedImage image = ImageIO.read(new FileInputStream("D:\\mybarcode.png"));
                //jasperParameter.put("logo", image );
            }
            
            jasperReport = JasperCompileManager.compileReport(layoutFile);
            jasperPrint = JasperFillManager.fillReport(jasperReport, jasperParameter,new JREmptyDataSource());

                    //print
                    JasperPrintManager.printReport(jasperPrint, false);
                    
                    //pdf
                    //String outputFile = System.getProperty("java.io.tmpdir") + "\\Sales.pdf";
                    //commonService.forceDeleteFile(outputFile);
                    //JasperExportManager.exportReportToPdfFile(jasperPrint, outputFile);
                    //commonService.openPDF(new File(outputFile));
        } catch (Exception e) {
            log.error("createSalesPreformaReport:", e);
            return false;
        }
        return true;
    }

    public static void main(String args[]) {

        HibernateUtil hibernateUtil = new HibernateUtil();
        System.out.println("Report");
        BarcodeReport report = new BarcodeReport();
       // report.createBarcodeReport("hoo", "kkkkk");
    }
}
