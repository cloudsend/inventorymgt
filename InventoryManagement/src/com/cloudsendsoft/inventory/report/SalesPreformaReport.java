/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.report;

import com.cloudsendsoft.inventory.bean.SalesPreformaBean;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.datasource.SalesPreformaDataSource;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaCharge;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesPreformaTax;
import com.cloudsendsoft.inventory.model.SalesTax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class SalesPreformaReport {

    static final Logger log = Logger.getLogger(SalesPreformaReport.class.getName());
    CommonService commonService = new CommonService();

    public boolean createSalesPreformaReport(SalesPreforma salesPreforma, String format) {
        JasperPrint jasperPrint;
        JasperReport jasperReport;
        HashMap<String, Object> jasperParameter = new HashMap<String, Object>();
        List<SalesPreformaBean> listOfSalesPreforma = new ArrayList<SalesPreformaBean>(0);
        try {
            String[] fields = new String[]{"srNo", "itemCode", "itemDescription", "unitPrice", "qty", "amount"};

            Integer srNo = 1;
            double grandTotal = 0.00;

            //adding items
            for (SalesPreformaItem preformaItem : salesPreforma.getSalesPreformaItems()) {
                SalesPreformaBean bean = new SalesPreformaBean();
                bean.setSrNo(srNo);
                bean.setItemCode(preformaItem.getItem().getItemCode());
                bean.setDescription(preformaItem.getItem().getDescription());
                bean.setUnitPrice(salesPreforma.getMultiCurrency().getSymbol() +" "+ commonService.formatIntoCurrencyAsString(preformaItem.getUnitPrice()));
                bean.setQty(preformaItem.getQuantity());
                bean.setAmount(salesPreforma.getMultiCurrency().getSymbol() +" "+commonService.formatIntoCurrencyAsString(preformaItem.getUnitPrice() * preformaItem.getQuantity()));
                listOfSalesPreforma.add(bean);
                grandTotal += preformaItem.getUnitPrice() * preformaItem.getQuantity();
                srNo++;
            }

            //adding taxes
            for (SalesPreformaTax salesPreformaTax : salesPreforma.getSalesPreformaTaxs()) {
                SalesPreformaBean bean = new SalesPreformaBean();
                bean.setDescription("\t" + salesPreformaTax.getTax().getName());
                bean.setAmount(salesPreforma.getMultiCurrency().getSymbol() +" "+commonService.formatIntoCurrencyAsString(salesPreformaTax.getAmount()));
                listOfSalesPreforma.add(bean);
                grandTotal += salesPreformaTax.getAmount();
            }

            //adding charges
            for (SalesPreformaCharge salesPreformaCharge : salesPreforma.getSalesPreformaCharges()) {
                SalesPreformaBean bean = new SalesPreformaBean();
                bean.setDescription("\t" + salesPreformaCharge.getCharges().getName());
                bean.setAmount(salesPreforma.getMultiCurrency().getSymbol() +" "+commonService.formatIntoCurrencyAsString(salesPreformaCharge.getAmount()));
                listOfSalesPreforma.add(bean);
                grandTotal += salesPreformaCharge.getAmount();
            }

            SalesPreformaDataSource datasource = new SalesPreformaDataSource(listOfSalesPreforma, fields);
            String companyDetails = "";
            jasperParameter.put("billDate", commonService.sqlDateToString(salesPreforma.getBillDate()));
            jasperParameter.put("invoiceNumber", salesPreforma.getInvoiceNumber());
            jasperParameter.put("customerName", salesPreforma.getCustomer().getLedgerName());
            jasperParameter.put("address", salesPreforma.getAddress());
            jasperParameter.put("paymentTerms", salesPreforma.getPaymentTerms());
            jasperParameter.put("deleveryTerms", salesPreforma.getDeleveryTerms());
            jasperParameter.put("shipmentMode", (salesPreforma.getShipmentMode()==null)?"":salesPreforma.getShipmentMode().getName());
            jasperParameter.put("packing", salesPreforma.getPacking());
            jasperParameter.put("delivery", salesPreforma.getDelivery());
            if(salesPreforma.getDiscount()>0){
                jasperParameter.put("discountLabel","Discount");
                jasperParameter.put("discount", salesPreforma.getMultiCurrency().getSymbol() +" "+commonService.formatIntoCurrencyAsString(salesPreforma.getDiscount()));
            }else{
                jasperParameter.put("discountLabel","");
                jasperParameter.put("discount", "");
            }
            jasperParameter.put("grandTotal", salesPreforma.getMultiCurrency().getSymbol() +" "+commonService.formatIntoCurrencyAsString(grandTotal - salesPreforma.getDiscount()));
//            jasperParameter.put("companyAddress",GlobalProperty.getCompany().getMailingAddress());
//            jasperParameter.put("companyName",GlobalProperty.getCompany().getName());
//            jasperParameter.put("companyPin",GlobalProperty.getCompany().getPinCode());
//            jasperParameter.put("companyEmail",GlobalProperty.getCompany().getEmail());
//            jasperParameter.put("companyPhone",GlobalProperty.getCompany().getPhone());
//            jasperParameter.put("companyFax",GlobalProperty.getCompany().getFax());
            jasperParameter.put("accountName", GlobalProperty.getCompany().getAccountName());
            jasperParameter.put("accountNumber", GlobalProperty.getCompany().getAccountNumber());
            jasperParameter.put("bankName", GlobalProperty.getCompany().getBankName());
            jasperParameter.put("branch", GlobalProperty.getCompany().getBranch());
            jasperParameter.put("swiftCode", GlobalProperty.getCompany().getSwiftCode());
            jasperParameter.put("managingDirector", GlobalProperty.getCompany().getManagingDirector());

            if (GlobalProperty.getCompany().getName() != null) {
                if (GlobalProperty.getCompany().getName().trim().length() > 0) {
                    companyDetails += GlobalProperty.getCompany().getName() + "\n";
                }
            }
            if (GlobalProperty.getCompany().getMailingAddress() != null) {
                if (GlobalProperty.getCompany().getMailingAddress().trim().length() > 0) {
                    companyDetails += GlobalProperty.getCompany().getMailingAddress() + "\n";
                }
            }
            if (GlobalProperty.getCompany().getPinCode() != null) {
                if (GlobalProperty.getCompany().getPinCode().trim().length() > 0) {
                    companyDetails += GlobalProperty.getCompany().getPinCode() + "\n";
                }
            }
            if (GlobalProperty.getCompany().getEmail() != null) {
                if (GlobalProperty.getCompany().getEmail().trim().length() > 0) {
                    companyDetails += "Email:" + GlobalProperty.getCompany().getEmail() + "\n";
                }
            }
            if (GlobalProperty.getCompany().getPhone() != null) {
                if (GlobalProperty.getCompany().getPhone().trim().length() > 0) {
                    companyDetails += "Phone:" + GlobalProperty.getCompany().getPhone() + "\n";
                }
            }
            if (GlobalProperty.getCompany().getFax() != null) {
                if (GlobalProperty.getCompany().getFax().trim().length() > 0) {
                    companyDetails += "Fax:" + GlobalProperty.getCompany().getFax();
                }
            }
            jasperParameter.put("customerDetails", salesPreforma.getAddress());
            jasperParameter.put("companyDetails", companyDetails);
            

            String applicationPath = new File(".").getAbsolutePath();
            //String layoutFile = applicationPath + "\\jrxml\\salesPreforma.jrxml";
            InputStream layoutFile = this.getClass().getClassLoader().getResourceAsStream("salesPreforma.jrxml");
            jasperReport = JasperCompileManager.compileReport(layoutFile);
            jasperPrint = JasperFillManager.fillReport(jasperReport, jasperParameter, datasource);

            switch (format) {
                case "print":
                    JasperPrintManager.printReport(jasperPrint, false);
                    break;
                case "pdf":
                    String outputFile = System.getProperty("java.io.tmpdir") + "\\salesPreforma.pdf";
                    commonService.forceDeleteFile(outputFile);
                    JasperExportManager.exportReportToPdfFile(jasperPrint, outputFile);
                    commonService.openPDF(new File(outputFile));
                    break;
            }
            return true;
        } catch (Exception e) {
            log.error("createSalesPreformaReport:", e);
            return false;
        }
    }

    public static void main(String args[]) {

        HibernateUtil hibernateUtil = new HibernateUtil();
        System.out.println("Report");
        SalesPreformaReport report = new SalesPreformaReport();

        SalesPreformaDAO salesPreformaDAO = new SalesPreformaDAO();
        List<SalesPreforma> listOfSalesPreforma = salesPreformaDAO.findAll();

        //report.createSalesPreformaReport(listOfSalesPreforma.get(0));
    }
}
