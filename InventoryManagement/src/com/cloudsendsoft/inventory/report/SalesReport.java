/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.report;

import com.cloudsendsoft.inventory.bean.SalesBean;
import com.cloudsendsoft.inventory.bean.SalesPreformaBean;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.datasource.SalesDataSource;
import com.cloudsendsoft.inventory.datasource.SalesPreformaDataSource;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesCharge;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaCharge;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesPreformaTax;
import com.cloudsendsoft.inventory.model.SalesTax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class SalesReport {

    static final Logger log = Logger.getLogger(SalesReport.class.getName());
    CommonService commonService = new CommonService();

    public boolean createSalesReport(Sales sales, String format, Boolean isDuplicateColumnsVisible) {
        JasperPrint jasperPrint;
        JasperReport jasperReport;
        HashMap<String, Object> jasperParameter = new HashMap<String, Object>();
        List<SalesBean> listOfSales = new ArrayList<SalesBean>(0);
        try {
            String[] fields = new String[]{"srNo", "itemCode", "itemDescription", "unitPrice", "qty", "amount"};

            Integer srNo = 1;
            double grandTotal = 0.00;

            //adding items
            for (SalesItem salesItem : sales.getSalesItems()) {
                SalesBean bean = new SalesBean();
                bean.setSrNo(srNo);
                bean.setItemCode(salesItem.getItem().getItemCode());
                bean.setDescription(salesItem.getItem().getDescription());
                bean.setUnitPrice(commonService.formatIntoCurrencyAsString(salesItem.getUnitPrice()));
                if (isDuplicateColumnsVisible) {
                    bean.setQty(salesItem.getQuantity() + salesItem.getQuantity1());
                    bean.setAmount(commonService.formatIntoCurrencyAsString(salesItem.getUnitPrice() * (salesItem.getQuantity() + salesItem.getQuantity1())));
                    grandTotal += salesItem.getUnitPrice() * (salesItem.getQuantity() + salesItem.getQuantity1());
                } else {
                    bean.setQty(salesItem.getQuantity());
                    bean.setAmount(commonService.formatIntoCurrencyAsString(salesItem.getUnitPrice() * salesItem.getQuantity()));
                    grandTotal += salesItem.getUnitPrice() * salesItem.getQuantity();
                }
                listOfSales.add(bean);
                srNo++;
            }

            //adding taxes
            for (SalesTax salesTax : sales.getSalesTaxs()) {
                SalesBean bean = new SalesBean();
                bean.setDescription("\t" + salesTax.getTax().getName());
                bean.setAmount(commonService.formatIntoCurrencyAsString(salesTax.getAmount()));
                listOfSales.add(bean);
                grandTotal += salesTax.getAmount();
            }

            //adding charges
            for (SalesCharge salesCharge : sales.getSalesCharges()) {
                SalesBean bean = new SalesBean();
                bean.setDescription("\t" + salesCharge.getCharges().getName());
                if (isDuplicateColumnsVisible) {
                    bean.setAmount(commonService.formatIntoCurrencyAsString(salesCharge.getAmount() + salesCharge.getAmount1()));
                    grandTotal += (salesCharge.getAmount() + salesCharge.getAmount1());
                } else {
                    bean.setAmount(commonService.formatIntoCurrencyAsString(salesCharge.getAmount()));
                    grandTotal += salesCharge.getAmount();
                }
                listOfSales.add(bean);
            }

            SalesDataSource datasource = new SalesDataSource(listOfSales, fields);
            String companyDetails = "";
            jasperParameter.put("billDate", "Date: "+commonService.sqlDateToString(sales.getBillDate()));
            jasperParameter.put("invoiceNumber", sales.getInvoiceNumber());
            jasperParameter.put("customerName", sales.getCustomer().getLedgerName());
            jasperParameter.put("address", sales.getAddress());
            //jasperParameter.put("paymentTerms",salesPreforma.getPaymentTerms());
            //jasperParameter.put("deleveryTerms",salesPreforma.getDeleveryTerms());
            
            
            jasperParameter.put("shipmentMode", (sales.getShipmentMode() == null) ? "" : sales.getShipmentMode().getName());
            //jasperParameter.put("packing",sales.get);
            jasperParameter.put("deliveryDate", sales.getDeliveryDate());
            if (sales.getDiscount() > 0 || sales.getDiscount1() > 0) {
                jasperParameter.put("discountLabel", "\tDiscount");
                if (isDuplicateColumnsVisible) {
                    jasperParameter.put("discount", commonService.formatIntoCurrencyAsString(sales.getDiscount() + sales.getDiscount1()));
                } else {
                    jasperParameter.put("discount", commonService.formatIntoCurrencyAsString(sales.getDiscount()));
                }
            } else {
                jasperParameter.put("discountLabel", "");
                jasperParameter.put("discount", "");
            }
            jasperParameter.put("grandTotalLabel", "\tGrand Total");
            if (isDuplicateColumnsVisible) {
                jasperParameter.put("grandTotal", sales.getMultiCurrency().getSymbol() +  commonService.formatIntoCurrencyAsString(grandTotal - (sales.getDiscount() + sales.getDiscount1())));
            } else {
                jasperParameter.put("grandTotal", sales.getMultiCurrency().getSymbol() +  commonService.formatIntoCurrencyAsString(grandTotal - sales.getDiscount()));
            }
            //jasperParameter.put("companyAddress",GlobalProperty.getCompany().getMailingAddress());
//            jasperParameter.put("companyName",GlobalProperty.getCompany().getName());
//            jasperParameter.put("companyPin",GlobalProperty.getCompany().getPinCode());
//            jasperParameter.put("companyEmail",GlobalProperty.getCompany().getEmail());
//            jasperParameter.put("companyPhone",GlobalProperty.getCompany().getPhone());
//            jasperParameter.put("companyFax",GlobalProperty.getCompany().getFax());
            jasperParameter.put("accountName", GlobalProperty.getCompany().getAccountName());
            jasperParameter.put("accountNumber", GlobalProperty.getCompany().getAccountNumber());
            jasperParameter.put("bankName", GlobalProperty.getCompany().getBankName());
            jasperParameter.put("branch", GlobalProperty.getCompany().getBranch());
            jasperParameter.put("swiftCode", GlobalProperty.getCompany().getSwiftCode());
            jasperParameter.put("managingDirector", GlobalProperty.getCompany().getManagingDirector());

            if (GlobalProperty.getCompany().getName() != null) {
                if (GlobalProperty.getCompany().getName().trim().length() > 0) {
                    companyDetails += GlobalProperty.getCompany().getName() + "\n";
                }
            }
            if (GlobalProperty.getCompany().getMailingAddress() != null) {
                if (GlobalProperty.getCompany().getMailingAddress().trim().length() > 0) {
                    companyDetails += GlobalProperty.getCompany().getMailingAddress() + "\n";
                }
            }
            if (GlobalProperty.getCompany().getPinCode() != null) {
                if (GlobalProperty.getCompany().getPinCode().trim().length() > 0) {
                    companyDetails += GlobalProperty.getCompany().getPinCode() + "\n";
                }
            }
            if (GlobalProperty.getCompany().getEmail() != null) {
                if (GlobalProperty.getCompany().getEmail().trim().length() > 0) {
                    companyDetails += "Email:" + GlobalProperty.getCompany().getEmail() + "\n";
                }
            }
            if (GlobalProperty.getCompany().getPhone() != null) {
                if (GlobalProperty.getCompany().getPhone().trim().length() > 0) {
                    companyDetails += "Phone:" + GlobalProperty.getCompany().getPhone() + "\n";
                }
            }
            if (GlobalProperty.getCompany().getFax() != null) {
                if (GlobalProperty.getCompany().getFax().trim().length() > 0) {
                    companyDetails += "Fax:" + GlobalProperty.getCompany().getFax();
                }
            }
            jasperParameter.put("companyDetails", companyDetails);
            jasperParameter.put("customerDetails", sales.getAddress());

            String applicationPath = new File("").getAbsolutePath();
            //String layoutFile = applicationPath + "\\jrxml\\Sales.jrxml";
            //System.out.println("applicationPath:"+applicationPath);
            //String layoutFile = applicationPath + "\\jrxml\\Sales_Sadows.jrxml";
           
            //InputStream layoutFile = this.getClass().getResourceAsStream("\\jrxml\\Sales_Sadows.jrxml");
            InputStream layoutFile = this.getClass().getClassLoader().getResourceAsStream("Sales_Repeat.jrxml");
            jasperReport = JasperCompileManager.compileReport(layoutFile);
            jasperPrint = JasperFillManager.fillReport(jasperReport, jasperParameter, datasource);

            switch (format) {
                case "print":
                    JasperPrintManager.printReport(jasperPrint, false);
                    break;
                case "pdf":
                    String outputFile = System.getProperty("java.io.tmpdir") + "\\Sales.pdf";
                    commonService.forceDeleteFile(outputFile);
                    JasperExportManager.exportReportToPdfFile(jasperPrint, outputFile);
                    commonService.openPDF(new File(outputFile));
                    break;
            }
            return true;
        } catch (Exception e) {
            log.error("createSalesPreformaReport:", e);
            return false;
        }
    }

    public static void main(String args[]) {

        HibernateUtil hibernateUtil = new HibernateUtil();
        System.out.println("Report");
        SalesReport report = new SalesReport();

        SalesDAO salesDAO = new SalesDAO();
        List<Sales> listOfSales = salesDAO.findAll();

        //  report.createSalesReport(listOfSales.get(0));
    }
}
