package com.cloudsendsoft.inventory.services;

import javax.swing.JOptionPane;

/**
 *
 * @author user
 */
public class MsgBox {
    public static void warning(String s){
        JOptionPane.showMessageDialog(null, s, "Warning", JOptionPane.WARNING_MESSAGE, null);
    }
    public static void success(String s){
        JOptionPane.showMessageDialog(null, s, "Success", JOptionPane.INFORMATION_MESSAGE, null);
    }
     public static void abort(String s){
        JOptionPane.showMessageDialog(null, s, "Error", JOptionPane.ERROR_MESSAGE, null);
    }
    public static boolean confirm(String s){
        int dialogButton=JOptionPane.YES_NO_OPTION;
        int dialogResult=JOptionPane.showConfirmDialog(null, s, "Confirm", dialogButton);
        if(dialogResult==JOptionPane.YES_OPTION){
            return true;
        }
        return false;
    }
}
