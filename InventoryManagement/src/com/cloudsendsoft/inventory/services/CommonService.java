/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.services;

import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.ibm.icu.text.NumberFormat;
import com.toedter.calendar.JDateChooser;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.TextField;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author Sangeeth
 */
public class CommonService {
    //get all components from parent componet
    public Component[] getComponents(Component container) {
        ArrayList<Component> list = null;

        try {
            list = new ArrayList<Component>(Arrays.asList(
                    ((Container) container).getComponents()));
            for (int index = 0; index < list.size(); index++) {
                for (Component currentComponent : getComponents(list.get(index))) {
                    list.add(currentComponent);
                }
            }
        } catch (ClassCastException e) {
            list = new ArrayList<Component>();
        }

        return list.toArray(new Component[list.size()]);
    }

    //disable all components in a container component
    public void disableAllComponents(Component container) {
        for (Component component : getComponents(container)) {
            component.setEnabled(false);
        }
    }
    
    //enable all components in a container component
    public void enableAllComponents(Component container) {
        for (Component component : getComponents(container)) {
            component.setEnabled(true);
        }
    }

    private static List<SimpleDateFormat> dateFormats = new ArrayList<SimpleDateFormat>() {
        {
            
            add(new SimpleDateFormat("dd/MM/yyyy"));
            add(new SimpleDateFormat("dd-MM-yyyy"));
            add(new SimpleDateFormat("dd.MM.yyyy"));
            
            add(new SimpleDateFormat("dd/M/yyyy"));
            add(new SimpleDateFormat("dd-M-yyyy"));
            add(new SimpleDateFormat("dd.M.yyyy"));
            
            add(new SimpleDateFormat("d/M/yyyy"));
            add(new SimpleDateFormat("d-M-yyyy"));
            add(new SimpleDateFormat("d.M.yyyy"));
            
            add(new SimpleDateFormat("dd/M/yyyy"));
            add(new SimpleDateFormat("dd-M-yyyy"));
            add(new SimpleDateFormat("dd.M.yyyy"));
            
            add(new SimpleDateFormat("yyyy-MM-dd"));
            
          //  add(new SimpleDateFormat("EEE MMM dd HH:mm:ss IST "));
            
            
        }
    };
    
    public String isIntegerBlank(Integer number){
        if(number==0){
            return "";
        }
        return number+"";
    }
     public String blankWhenZero(double number){
        if(number==0){
            return "";
        }
        return number+"";
    }
      
    // Regardless of sort order (ascending or descending), null values always appear last.
// colIndex specifies a column in model.
    public void sortAllRowsBy(DefaultTableModel model, int colIndex, boolean ascending) {
        Vector data = model.getDataVector();
        Collections.sort(data, new ColumnSorter(colIndex, ascending));
        model.fireTableStructureChanged();
    }

    public double roundOffDecimal(double amt){
        double roundOff = (double) Math.round(amt);
        return roundOff;
    }
    public static Date convertToDate(String input) {
        Date date = null;
        if(null == input) {
            return null;
        }
        for (SimpleDateFormat format : dateFormats) {
            try {
                format.setLenient(false);
                date = format.parse(input);
            } catch (ParseException e) {
                //Shhh.. try other formats
               // e.printStackTrace();
            }
            if (date != null) {
                break;
            }
        }
 
        return date;
    }
    
    public static void clearTextArea(JTextArea[] tf) {
        for (JTextArea t : tf) {
            t.setText("");
        }
    }
            
    public static String sqlDateToString(java.sql.Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String text = df.format(date);
        return text;
    }
    public static void clearCombo(JComboBox[] tf) {
        for (JComboBox t : tf) {
            t.setSelectedIndex(0);
        }
    }
    
    public static java.sql.Date strToSqlDate(String date) throws ParseException {
        //SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        java.sql.Date sqlDate=null;
        java.util.Date utilDate = convertToDate(date);
        if(null!=utilDate){
            sqlDate = new java.sql.Date(utilDate.getTime());
        }
        return sqlDate;
    }
    
    public java.sql.Date jDateChooserToSqlDate(JDateChooser dateChooser) throws ParseException {
         java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("dd-MM-yyyy");
            String formattedDate = fmt.format(dateChooser.getDate());
        java.sql.Date sqlDate=null;
        java.util.Date utilDate = convertToDate(formattedDate);
        if(null!=utilDate){
            sqlDate = new java.sql.Date(utilDate.getTime());
        }
        return sqlDate;
    }
  

    public void removeInternalFrameTitleDropdown(JInternalFrame internalFrame) {
        BasicInternalFrameUI ui = (BasicInternalFrameUI) internalFrame.getUI();
        Container north = (Container) ui.getNorthPane();
        north.remove(0);
        north.validate();
        north.repaint();
    }

    public void setInternalFrameCenter(JFrame jMainFrame, JInternalFrame internalFrame) {
        Dimension desktopSize = jMainFrame.getSize();
        Dimension jInternalFrameSize = internalFrame.getSize();
        int width = (desktopSize.width - jInternalFrameSize.width) / 2;
        int height = (desktopSize.height - jInternalFrameSize.height) / 2;
        internalFrame.setLocation(width, height);
    }

    public java.sql.Date utilDateToSqlDate(java.util.Date utilDate) {
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        return sqlDate;
    }

    public static ArrayList getTableRowDataJDialog(JTable jTable) throws Exception {
        ArrayList rowData = null;
        int row = jTable.getSelectedRow() - 1;
        row = ((row == -1) ? (jTable.getRowCount() - 1) : row);
        if (row >= 0) {
            rowData = new ArrayList();
            int column = jTable.getColumnCount();
            for (int i = 0; i < column; i++) {
                rowData.add(jTable.getValueAt(row, i));
            }
        }
        return rowData;
    }

    public static ArrayList getTableRowData(JTable jTable) throws Exception {
        ArrayList rowData = null;
        int row = jTable.getSelectedRow();
       // System.err.println("row:" + row);
        if (row >= 0) {
            rowData = new ArrayList();
            int column = jTable.getColumnCount();
            for (int i = 0; i < column; i++) {
                rowData.add(jTable.getValueAt(row, i));
            }
        }
        return rowData;
    }

    public static boolean stringValidator(String[] strs) {
        for (String s : strs) {
            if (s.trim().length() <= 0) {
                return false;
            }
        }
        return true;
    }

    public static void clearTextFields(JTextField[] tf) {
        for (JTextField t : tf) {
            t.setText("");
        }
    }

    public static void clearTextPane(JTextPane[] tf) {
        for (JTextPane t : tf) {
            t.setText("");
        }
    }

    public static void clearNumberFields(JTextField[] tf) {
        for (JTextField t : tf) {
            t.setText("0");
        }
    }

    public static void clearCurrencyFields(JTextField[] tf) {
        for (JTextField t : tf) {
            t.setText("0.00");
        }
    }

    public static void currencyValidator(KeyEvent evt) {
        char c = evt.getKeyChar();
        if (!Character.isDigit(c) && c != '.') {
            evt.consume();
        }
    }

    public static void numberValidator(KeyEvent evt) {
        char c = evt.getKeyChar();
        if (!Character.isDigit(c)) {
            evt.consume();
        }
    }

    public static void setWidthAsPercentages(JTable table,
            double... percentages) {
        final double factor = 10000;

        TableColumnModel model = table.getColumnModel();
        for (int columnIndex = 0; columnIndex < percentages.length; columnIndex++) {
            TableColumn column = model.getColumn(columnIndex);
            column.setPreferredWidth((int) (percentages[columnIndex] * factor));
        }
    }

    public static void removeJpanel(JPanel jPanel) {
        jPanel.removeAll();
        jPanel.repaint();
        jPanel.revalidate();
        jPanel.add(GlobalProperty.getHomePageIcon());
    }

    //format decimal value like indian rupee format
    public String formatIntoCurrencyAsString(double amount){
        com.ibm.icu.text.NumberFormat format = com.ibm.icu.text.NumberFormat.getNumberInstance(new Locale("en", "IN"));
        format.setMinimumFractionDigits(2);
        format.setMaximumFractionDigits(2);
        return  format.format(amount);
    }

    //formated currency into original decimal
    public double currencyIntoDecimal(String currencyAmount) {
        NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
        Number number = null;
        try {
            number = format.parse(currencyAmount);
        } catch (ParseException ex) {
            Logger.getLogger(CommonService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return number.doubleValue();
    }

    public void openPDF(File pdfFile) throws IOException {
        if (pdfFile.exists()) {
            if (Desktop.isDesktopSupported()) {
                Desktop.getDesktop().open(pdfFile);
            } else {
                MsgBox.abort("Operating System is not supporting PDF!");//Awt Desktop is not supported!
            }
        } else {
            MsgBox.abort("Permission Denide to create pdf!");
        }
    }
    public void openXls(File xlsFile) throws IOException {
        if (xlsFile.exists()) {
            if (Desktop.isDesktopSupported()) {
                Desktop.getDesktop().open(xlsFile);
            } else {
                MsgBox.abort("Operating System is not supporting xls!");//Awt Desktop is not supported!
            }
        } else {
            MsgBox.abort("Permission Denide to create xls!");
        }
    }
    public void forceDeleteFile(String outputFile)throws IOException {
        File outFile = new File(outputFile);
        if (outFile.exists()) {
            FileWriter fileWriter = new FileWriter(outFile);
            fileWriter.close();
            outFile.delete();
        }
    }
    
    public Boolean isCurrentPeriod(java.util.Date date){
        CommonService commonService=new CommonService();
        if((GlobalProperty.company.getStartDate().after(commonService.utilDateToSqlDate(date)) && 
                GlobalProperty.company.getEndDate().before(commonService.utilDateToSqlDate(date)))||
                (GlobalProperty.company.getStartDate().equals(commonService.utilDateToSqlDate(date))|| 
                GlobalProperty.company.getEndDate().equals(commonService.utilDateToSqlDate(date)))){
            return true;
        }
        return false;
    }
    
    public void setCurrentPeriodOnCalendar(JDateChooser dateChooser){
        dateChooser.setMaxSelectableDate(GlobalProperty.company.getEndDate());
        dateChooser.setMinSelectableDate(GlobalProperty.company.getStartDate());
    }
    public void setCurrentPeriodStartOnCalendar(JDateChooser dateChooser){
        dateChooser.setMinSelectableDate(GlobalProperty.company.getStartDate());
    }
    public void setCurrentPeriodEndOnCalendar(JDateChooser dateChooser){
        dateChooser.setMaxSelectableDate(GlobalProperty.company.getEndDate());
    }
    
    public java.util.Date addDaysToDate(java.util.Date date, int days) {
        Calendar c = Calendar.getInstance();
        c.setTime(date); // Now use today date.
        c.add(Calendar.DATE, days); // Adding days
        return c.getTime();
    }
}
