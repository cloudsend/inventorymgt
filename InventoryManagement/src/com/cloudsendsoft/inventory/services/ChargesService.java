package com.cloudsendsoft.inventory.services;

import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.PopupTableDialog;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author user
 */
public class ChargesService {

    public void fillChargesListTable(javax.swing.JTable itemTable) {
        DefaultTableModel model = (DefaultTableModel) itemTable.getModel();
        model.setRowCount(0);
        ChargesDAO chargesDAO = new ChargesDAO();
        List<Charges> items = chargesDAO.findAllByDESC();
        int i = 0;
        for (Charges itm : items) {
            ((DefaultTableModel) itemTable.getModel()).insertRow(i, new Object[]{itm.getName()});
            i++;
        }
    }

}
