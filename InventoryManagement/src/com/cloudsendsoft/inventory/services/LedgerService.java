package com.cloudsendsoft.inventory.services;

import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.PopupTableDialog;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author user
 */
public class LedgerService {

    public void fillLedgerListTable(javax.swing.JTable ledgerListTable ){
        DefaultTableModel model = (DefaultTableModel)ledgerListTable.getModel();
        model.setRowCount(0);
        LedgerDAO ledgerDAO=new LedgerDAO();
        List<Ledger> items= ledgerDAO.findAllByDESC();
            int i=0;
            for(Ledger itm:items){
               ((DefaultTableModel) ledgerListTable.getModel()).insertRow(i, new Object [] {itm.getLedgerName(), itm.getNarration()
                       , itm.getLedgerGroup().getGroupName(),itm.getAvailableBalance()});i++;
            }
    }
    
    public List<Ledger> populatePopupTableBySupplier(String supplier,PopupTableDialog popupTableDialog) throws Exception{
        LedgerDAO ledgerDAO=new LedgerDAO();
        List<Ledger> suppliers=null;
        if(supplier.length()<=0){
            suppliers = ledgerDAO.findAllSupplier();
        }else{
            suppliers=ledgerDAO.findAllStartsWithSupplier(supplier);
        }
        ArrayList<Object[]> tableData=new ArrayList<>();
        int slNo=1;
        for(Ledger itm:suppliers){
               tableData.add(new Object [] {slNo++,itm.getLedgerName(), itm.getLedgerGroup().getGroupName(), itm.getAvailableBalance()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.","Name","Under","Balance Amount"});
        popupTableDialog.setTableData(tableData);
        return suppliers;
    }
    public List<Ledger> populatePopupTableByCustomer(String customer,PopupTableDialog popupTableDialog) throws Exception{
        LedgerDAO ledgerDAO=new LedgerDAO();
        List<Ledger> customers=null;
        if(customer.length()<=0){
            customers = ledgerDAO.findAllCustomer();
        }else{
            customers=ledgerDAO.findAllStartsWithCustomer(customer);
        }
        ArrayList<Object[]> tableData=new ArrayList<>();
        int slNo=1;
        for(Ledger itm:customers){
               tableData.add(new Object [] {slNo++,itm.getLedgerName(), itm.getLedgerGroup().getGroupName(), itm.getAvailableBalance()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.","Name","Under","Balance Amount"});
        popupTableDialog.setTableData(tableData);
        return customers;
    }
    public List<Ledger> populatePopupTableByPaymentBy(String customer,PopupTableDialog popupTableDialog) throws Exception{
        LedgerDAO ledgerDAO=new LedgerDAO();
        List<Ledger> customers=null;
        if(customer.length()<=0){
            customers = ledgerDAO.findAllCustomerPaymentBy();
        }else{
            customers=ledgerDAO.findAllStartsWithCustomerPaymentBy(customer);
        }
        ArrayList<Object[]> tableData=new ArrayList<>();
        int slNo=1;
        for(Ledger itm:customers){
               tableData.add(new Object [] {slNo++,itm.getLedgerName()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.","Name"});
        popupTableDialog.setTableData(tableData);
        return customers;
    }
    public List<Ledger> populatePopupTableByJournalBy(String customer,PopupTableDialog popupTableDialog) throws Exception{
        LedgerDAO ledgerDAO=new LedgerDAO();
        List<Ledger> customers=null;
        if(customer.length()<=0){
            customers = ledgerDAO.findAllCustomerJournalBy();
        }else{
            customers=ledgerDAO.findAllStartsWithCustomerJournalBy(customer);
        }
        ArrayList<Object[]> tableData=new ArrayList<>();
        int slNo=1;
        for(Ledger itm:customers){
               tableData.add(new Object [] {slNo++,itm.getLedgerName()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.","Name"});
        popupTableDialog.setTableData(tableData);
        return customers;
    }
    public List<Ledger> populatePopupTableByContraBy(String customer,PopupTableDialog popupTableDialog) throws Exception{
        LedgerDAO ledgerDAO=new LedgerDAO();
        List<Ledger> customers=null;
        if(customer.length()<=0){
            customers = ledgerDAO.findAllCustomerContraBy();
        }else{
            customers=ledgerDAO.findAllStartsWithCustomerContraBy(customer);
        }
        ArrayList<Object[]> tableData=new ArrayList<>();
        int slNo=1;
        for(Ledger itm:customers){
               tableData.add(new Object [] {slNo++,itm.getLedgerName()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.","Name"});
        popupTableDialog.setTableData(tableData);
        return customers;
    }
    public List<Ledger> populatePopupTableByPaymentTo(String customer,PopupTableDialog popupTableDialog) throws Exception{
        LedgerDAO ledgerDAO=new LedgerDAO();
        List<Ledger> customers=null;
        if(customer.length()<=0){
            customers = ledgerDAO.findAllCustomerPaymentTo();
        }else{
            customers=ledgerDAO.findAllStartsWithCustomerPaymentTo(customer);
        }
        ArrayList<Object[]> tableData=new ArrayList<>();
        int slNo=1;
        for(Ledger itm:customers){
               tableData.add(new Object [] {slNo++,itm.getLedgerName()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.","Name"});
        popupTableDialog.setTableData(tableData);
        return customers;
    }
    public List<Ledger> populatePopupTableByJournalTo(String customer,PopupTableDialog popupTableDialog) throws Exception{
        LedgerDAO ledgerDAO=new LedgerDAO();
        List<Ledger> customers=null;
        if(customer.length()<=0){
            customers = ledgerDAO.findAllCustomerJournalTo();
        }else{
            customers=ledgerDAO.findAllStartsWithCustomerJournalTo(customer);
        }
        ArrayList<Object[]> tableData=new ArrayList<>();
        int slNo=1;
        for(Ledger itm:customers){
               tableData.add(new Object [] {slNo++,itm.getLedgerName()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.","Name"});
        popupTableDialog.setTableData(tableData);
        return customers;
    }
    public List<Ledger> populatePopupTableByContraTo(String customer,PopupTableDialog popupTableDialog) throws Exception{
        LedgerDAO ledgerDAO=new LedgerDAO();
        List<Ledger> customers=null;
        if(customer.length()<=0){
            customers = ledgerDAO.findAllCustomerContraTo();
        }else{
            customers=ledgerDAO.findAllStartsWithCustomerContraTo(customer);
        }
        ArrayList<Object[]> tableData=new ArrayList<>();
        int slNo=1;
        for(Ledger itm:customers){
               tableData.add(new Object [] {slNo++,itm.getLedgerName()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.","Name"});
        popupTableDialog.setTableData(tableData);
        return customers;
    }
    public List<Ledger> populatePopupTableByReceiptBy(String customer,PopupTableDialog popupTableDialog) throws Exception{
        LedgerDAO ledgerDAO=new LedgerDAO();
        List<Ledger> customers=null;
        if(customer.length()<=0){
            customers = ledgerDAO.findAllCustomerReceiptBy();
        }else{
            customers=ledgerDAO.findAllStartsWithCustomerReceiptBy(customer);
        }
        ArrayList<Object[]> tableData=new ArrayList<>();
        int slNo=1;
        for(Ledger itm:customers){
               tableData.add(new Object [] {slNo++,itm.getLedgerName()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.","Name"});
        popupTableDialog.setTableData(tableData);
        return customers;
    }
    public List<Ledger> populatePopupTableByReceiptTo(String customer,PopupTableDialog popupTableDialog) throws Exception{
        LedgerDAO ledgerDAO=new LedgerDAO();
        List<Ledger> customers=null;
        if(customer.length()<=0){
            customers = ledgerDAO.findAllCustomerReceiptTo();
        }else{
            customers=ledgerDAO.findAllStartsWithCustomerReceiptTo(customer);
        }
        ArrayList<Object[]> tableData=new ArrayList<>();
        int slNo=1;
        for(Ledger itm:customers){
               tableData.add(new Object [] {slNo++,itm.getLedgerName()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.","Name"});
        popupTableDialog.setTableData(tableData);
        return customers;
    }
}
