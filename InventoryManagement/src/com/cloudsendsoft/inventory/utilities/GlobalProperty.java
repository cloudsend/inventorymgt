package com.cloudsendsoft.inventory.utilities;

import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.model.Company;
import com.cloudsendsoft.inventory.model.Privileges;
import com.cloudsendsoft.inventory.model.User;
import com.cloudsendsoft.inventory.view.MainForm;
import java.awt.Font;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import org.hibernate.SessionFactory;

/**
 *
 * @author Sangeeth
 */
public class GlobalProperty {

    public static Company company = null;

    public static int tableRowHieght = 30;
    public static Font font = new Font("Tahoma", Font.PLAIN, 13);

    public static MainForm mainForm = null;

    public static User user = null;

    public static List<String> listOfPrivileges = null;
    
    public static JLabel homePageIcon=null;
    
    public static Company getCompany() {
        return company;
    }

    public static void setCompany(Company company) {
        GlobalProperty.company = company;
    }

    public MainForm getMainForm() {
        return mainForm;
    }

    public void setMainForm(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    public static void setCurrentPeriodeOnLabel() {
        CommonService commonService = new CommonService();
        GlobalProperty.mainForm.getCurrentPeriodeLabel().setText("Current Period: " + commonService.sqlDateToString(company.getStartDate()) + " to " + commonService.sqlDateToString(company.getEndDate()));
    }
    
    public static void setCompanyNameOnLabel() {
        GlobalProperty.mainForm.getCompanyNameLabel().setText("Company Name: " + company.getName());
    }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        GlobalProperty.user = user;
    }

    public static List<String> getListOfPrivileges() {
        return listOfPrivileges;
    }

    public static void setListOfPrivileges(List<Privileges> listOfPrivileges) {
        GlobalProperty.listOfPrivileges = new ArrayList<String>();
        for (Privileges privileges : listOfPrivileges) {
            GlobalProperty.listOfPrivileges.add(privileges.getName());
        }
    }

    public static int getTableRowHieght() {
        return tableRowHieght;
    }

    public static void setTableRowHieght(int tableRowHieght) {
        GlobalProperty.tableRowHieght = tableRowHieght;
    }

    public static Font getFont() {
        return font;
    }

    public static void setFont(Font font) {
        GlobalProperty.font = font;
    }

    public static JLabel getHomePageIcon() {
        return homePageIcon;
    }

    public static void setHomePageIcon(JLabel homePageIcon) {
        GlobalProperty.homePageIcon = homePageIcon;
    }
    
    
}
