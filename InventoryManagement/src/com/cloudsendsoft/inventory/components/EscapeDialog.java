/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.components;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;

/**
 *
 * @author Sangeeth
 */
public class EscapeDialog extends JDialog { 
    
  public EscapeDialog(Frame owner) { 
    super(owner, true);
    getContentPane().add(new JComboBox());
  } 
  protected JRootPane createRootPane() { 
    JRootPane rootPane = new JRootPane();
    KeyStroke stroke = KeyStroke.getKeyStroke("ESCAPE");
    Action actionListener = new AbstractAction() { 
      public void actionPerformed(ActionEvent actionEvent) { 
        setVisible(false);
      } 
    } ;
    InputMap inputMap = rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
    inputMap.put(stroke, "ESCAPE");
    rootPane.getActionMap().put("ESCAPE", actionListener);

    return rootPane;
  } 

    public EscapeDialog(Frame owner, boolean modal) {
        super(owner, modal);
    }
} 
