/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.components;

import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Chindu
 */
public class PendingSalesTableModal extends DefaultTableModel{

     public PendingSalesTableModal() {
    }

    public PendingSalesTableModal(int rowCount, int columnCount) {
        super(rowCount, columnCount);
    }

    public PendingSalesTableModal(Vector columnNames, int rowCount) {
        super(columnNames, rowCount);
    }

    public PendingSalesTableModal(Object[] columnNames, int rowCount) {
        super(columnNames, rowCount);
    }

    public PendingSalesTableModal(Vector data, Vector columnNames) {
        super(data, columnNames);
    }

    public PendingSalesTableModal(Object[][] data, Object[] columnNames) {
        super(data, columnNames);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
       // return super.isCellEditable(row, column); //To change body of generated methods, choose Tools | Templates.
        if(column==8||column==9){
            return true;
        }
        return false;
    }
    
}
