/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.datasource;

import com.cloudsendsoft.inventory.bean.JTableReportBean;
//import com.cloudsendsoft.inventory.bean.SalesBean;
//import com.cloudsendsoft.inventory.bean.SalesPreformaBean;
//import com.cloudsendsoft.inventory.dao.SalesDAO;
//import com.cloudsendsoft.inventory.model.SalesPreforma;
//import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class JTableDataSource implements JRDataSource {

    static final Logger log = Logger.getLogger(JTableDataSource.class.getName());
    private String[] fields;
    private Iterator iterator;
    private JTableReportBean currentValue;

    public JTableDataSource(List list, String[] fields) {
        this.iterator = list.iterator();
        this.fields = fields;
    }

    @Override
    public boolean next() throws JRException {
        currentValue = (JTableReportBean) (iterator.hasNext() ? iterator.next() : null);
        return (currentValue != null);
    }

    @Override
    public Object getFieldValue(JRField field) throws JRException {
        Object value = null;
        JTableReportBean objectInfo = null;
        int index = getFieldIndex(field.getName());
        try {
            if (index > -1) {
                objectInfo = currentValue;
                value = null;
                switch (index) {
                    case 0: {
                        value = objectInfo.getVal1();
                        break;
                    }
                    case 1: {
                        value = objectInfo.getVal2();
                        break;
                    }
                     case 2: {
                        value = objectInfo.getVal3();
                        break;
                    }
                    case 3: {
                        value = objectInfo.getVal4();
                        break;
                    }
                    case 4: {
                        value = objectInfo.getVal5();
                        break;
                    }
                    case 5: {
                        value = objectInfo.getVal6();
                        break;
                    }
                    case 6: {
                        value = objectInfo.getVal7();
                        break;
                    }
                    case 7: {
                        value = objectInfo.getVal8();
                        break;
                    }
                    case 8: {
                        value = objectInfo.getVal9();
                        break;
                    }
                    case 9: {
                        value = objectInfo.getVal10();
                        break;
                    }
                }
            }
        } catch (Exception e) {
            log.error("getFieldValue:", e);
        }
        return value;
    }

    private int getFieldIndex(String field) {
        int index = -1;
        for (int i = 0; i < fields.length; i++) {
            if (fields[i].equals(field)) {
                index = i;
                break;
            }
        }
        return index;
    }

}
