/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.datasource;

import com.cloudsendsoft.inventory.bean.PackingListBean;
import com.cloudsendsoft.inventory.bean.SalesPreformaBean;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class PackingListDataSource implements JRDataSource {

    static final Logger log = Logger.getLogger(PackingListDataSource.class.getName());
    private String[] fields;
    private Iterator iterator;
    private PackingListBean currentValue;

    public PackingListDataSource(List list, String[] fields) {
        this.iterator = list.iterator();
        this.fields = fields;
    }

    @Override
    public boolean next() throws JRException {
        currentValue = (PackingListBean) (iterator.hasNext() ? iterator.next() : null);
        return (currentValue != null);
    }

    @Override
    public Object getFieldValue(JRField field) throws JRException {
        Object value = null;
        PackingListBean objectInfo = null;
        int index = getFieldIndex(field.getName());
        try {
            if (index > -1) {
                objectInfo = currentValue;
                value = null;
                switch (index) {
                    case 0: {
                        value = objectInfo.getSrNo();
                        break;
                    }
                    case 1: {
                        value = objectInfo.getItemCode();
                        break;
                    }
                     case 2: {
                        value = objectInfo.getDescription();
                        break;
                    }
                    case 3: {
                        value = objectInfo.getNumberOfPack();
                        break;
                    }
                    case 4: {
                        value = objectInfo.getKindOfPack();
                        break;
                    }
                    case 5: {
                        value = objectInfo.getContentOfPack();
                        break;
                    }
                    case 6: {
                        value = objectInfo.getNetWeight();
                        break;
                    }
                    case 7: {
                        value = objectInfo.getGrossWeight();
                        break;
                    }
                }
            }
        } catch (Exception e) {
            log.error("getFieldValue:", e);
            e.printStackTrace();
        }
        return value;
    }

    private int getFieldIndex(String field) {
        int index = -1;
        for (int i = 0; i < fields.length; i++) {
            if (fields[i].equals(field)) {
                index = i;
                break;
            }
        }
        return index;
    }

}
