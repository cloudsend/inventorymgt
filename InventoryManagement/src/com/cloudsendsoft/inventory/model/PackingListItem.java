/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.model;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import static org.eclipse.jdt.internal.compiler.parser.Parser.name;

/**
 *
 * @author Sangeeth
 */
@Entity
public class PackingListItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    
    @ManyToOne(cascade = CascadeType.ALL)
    private Item item;
    
    Long numberOfPack;
    
    @ManyToOne(cascade = CascadeType.ALL)
    PackingType kindOfPackage;
    
   /* @ManyToOne
    Company company;*/

   /* public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }*/
    
    @Column(name = "contentsOfPackageQty", columnDefinition="Decimal(10,2) default '0.00'")
    Double contentsOfPackageQty=0.00;
    
    @Column(name = "netWeight", columnDefinition="Decimal(10,2) default '0.00'")
    Double netWeight=0.00;
    
    @Column(name = "grossWeight", columnDefinition="Decimal(10,2) default '0.00'")
    Double grossWeight=0.00;

  /*  public PackingListItem() {
        this.numberOfPack = 0;
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Long getNumberOfPack() {
        return numberOfPack;
    }

    public void setNumberOfPack(Long numberOfPack) {
        this.numberOfPack = numberOfPack;
    }

   
    public PackingType getKindOfPackage() {
        return kindOfPackage;
    }

    public void setKindOfPackage(PackingType kindOfPackage) {
        this.kindOfPackage = kindOfPackage;
    }

    public Double getContentsOfPackageQty() {
        return contentsOfPackageQty;
    }

    public void setContentsOfPackageQty(Double contentsOfPackageQty) {
        this.contentsOfPackageQty = contentsOfPackageQty;
    }


    public Double getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(Double netWeight) {
        this.netWeight = netWeight;
    }

    public Double getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(Double grossWeight) {
        this.grossWeight = grossWeight;
    }

}
