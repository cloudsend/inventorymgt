/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.model;

import java.sql.Date;
import java.util.Currency;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author Sangeeth
 */
@Entity
public class Sales {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    
    @Column(name="billDate")
    private Date billDate;
    
    @Column(name = "address")
    String address;
    
    @Column(name = "invoiceNumber", nullable = false)
    String invoiceNumber;
        
    @ManyToOne
    Ledger customer;
    
    @Column(name="deliveryDate")
    private String deliveryDate;
    
    @Column(name="discount", columnDefinition="Decimal(10,2) default '0.00'")
    Double discount;
    
    @Column(name="discount1", columnDefinition="Decimal(10,2) default '0.00'")
    Double discount1;
    
    @Column(name = "currentCurrencyRate",columnDefinition="Decimal(10,2) default '0.00'")
    Double currentCurrencyRate=0.00;
    
    @Column(name="grandTotal", columnDefinition="Decimal(10,2) default '0.00'")
    Double grandTotal;
                 
    @Column(name="grandTotal1", columnDefinition="Decimal(10,2) default '0.00'")
    Double grandTotal1;
    
    @ManyToOne
    MultiCurrency multiCurrency;
    
    @ManyToOne
    ShipmentMode shipmentMode;
    
    @ManyToOne
    Company company;
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    List<SalesItem> salesItems;
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    List<SalesTax> salesTaxs;
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    List<SalesCharge> salesCharges;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Ledger getCustomer() {
        return customer;
    }

    public void setCustomer(Ledger customer) {
        this.customer = customer;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getCurrentCurrencyRate() {
        return currentCurrencyRate;
    }

    public void setCurrentCurrencyRate(Double currentCurrencyRate) {
        this.currentCurrencyRate = currentCurrencyRate;
    }

    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

    
    public MultiCurrency getMultiCurrency() {
        return multiCurrency;
    }

    public void setMultiCurrency(MultiCurrency multiCurrency) {
        this.multiCurrency = multiCurrency;
    }

    public ShipmentMode getShipmentMode() {
        return shipmentMode;
    }

    public void setShipmentMode(ShipmentMode shipmentMode) {
        this.shipmentMode = shipmentMode;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<SalesItem> getSalesItems() {
        return salesItems;
    }

    public void setSalesItems(List<SalesItem> salesItems) {
        this.salesItems = salesItems;
    }

    public List<SalesTax> getSalesTaxs() {
        return salesTaxs;
    }

    public void setSalesTaxs(List<SalesTax> salesTaxs) {
        this.salesTaxs = salesTaxs;
    }

    public List<SalesCharge> getSalesCharges() {
        return salesCharges;
    }

    public void setSalesCharges(List<SalesCharge> salesCharges) {
        this.salesCharges = salesCharges;
    }

    public Double getGrandTotal1() {
        return grandTotal1;
    }

    public void setGrandTotal1(Double grandTotal1) {
        this.grandTotal1 = grandTotal1;
    }

    public Double getDiscount1() {
        return discount1;
    }

    public void setDiscount1(Double discount1) {
        this.discount1 = discount1;
    }
     
    
}
