/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.model;

import java.sql.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author Sangeeth
 */
@Entity
public class SalesReturn {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    
    @Column(name="billDate")
    private Date billDate;
    
    @Column(name = "invoiceNumber", nullable = false)
    String invoiceNumber;
    
    @ManyToOne
    Ledger customer;
    
    @ManyToOne
    Sales sales;
    
    @Column(name="deliveryDate")
    private String deliveryDate;
    
     
    @Column(name="grandTotal", columnDefinition="Decimal(10,2) default '0.00'")
    Double grandTotal;
    
    @Column(name="grandTotal1", columnDefinition="Decimal(10,2) default '0.00'")
    Double grandTotal1;
    
    @ManyToOne
    MultiCurrency multiCurrency;
    
    @ManyToOne
    ShipmentMode shipmentMode;
    
    @ManyToOne
    Company company;
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    List<SalesReturnItem> salesReturnItems;
         
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    List<SalesReturnCharge> salesReturnCharges;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Ledger getCustomer() {
        return customer;
    }

    public void setCustomer(Ledger customer) {
        this.customer = customer;
    }

    public Sales getSales() {
        return sales;
    }

    public void setSales(Sales sales) {
        this.sales = sales;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }
    
    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public MultiCurrency getMultiCurrency() {
        return multiCurrency;
    }

    public void setMultiCurrency(MultiCurrency multiCurrency) {
        this.multiCurrency = multiCurrency;
    }

    public ShipmentMode getShipmentMode() {
        return shipmentMode;
    }

    public void setShipmentMode(ShipmentMode shipmentMode) {
        this.shipmentMode = shipmentMode;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<SalesReturnItem> getSalesReturnItems() {
        return salesReturnItems;
    }

    public void setSalesReturnItems(List<SalesReturnItem> salesReturnItems) {
        this.salesReturnItems = salesReturnItems;
    }

    public List<SalesReturnCharge> getSalesReturnCharges() {
        return salesReturnCharges;
    }

    public void setSalesReturnCharges(List<SalesReturnCharge> salesReturnCharges) {
        this.salesReturnCharges = salesReturnCharges;
    }

    public Double getGrandTotal1() {
        return grandTotal1;
    }

    public void setGrandTotal1(Double grandTotal1) {
        this.grandTotal1 = grandTotal1;
    }

       
    
}
