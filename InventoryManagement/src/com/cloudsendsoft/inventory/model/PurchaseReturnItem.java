/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.model;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Sangeeth
 */
@Entity
public class PurchaseReturnItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    
    @Column(name="discount",columnDefinition="Decimal(10,2) default '0.00'")
    private Double discount;
          
    @ManyToOne(cascade = CascadeType.ALL)
    private Item item;
            
    @Column(name = "quantity",columnDefinition="Decimal(10,2) default '0.00'")
    double quantity;

    @Column(name = "unitPrice", columnDefinition="Decimal(10,2) default '0.00'")
    private Double unitPrice=0.00;
    
    @Column(name = "quantity1",columnDefinition="Decimal(10,2) default '0.00'")
    double quantity1;

    @Column(name = "unitPrice1", columnDefinition="Decimal(10,2) default '0.00'")
    private Double unitPrice1=0.00;
    
    @ManyToOne
    Company company; 
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public double getQuantity1() {
        return quantity1;
    }

    public void setQuantity1(double quantity1) {
        this.quantity1 = quantity1;
    }

    public Double getUnitPrice1() {
        return unitPrice1;
    }

    public void setUnitPrice1(Double unitPrice1) {
        this.unitPrice1 = unitPrice1;
    }
       
}
