package com.cloudsendsoft.inventory.model;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.swing.JComboBox;

/**
 *
 * @author sangeeth
 */
@Entity
@Table(name = "Ledger")
public class Ledger {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    Integer id;

    @Column(name = "ledgerName", nullable = false)
    String ledgerName;

    @Column(name = "narration")
    String narration;
    
    @ManyToOne
    LedgerGroup ledgerGroup;

    @Column(name = "openingBalance", columnDefinition = "Decimal(10,2) default '0.00'")
    Double openingBalance = 0.00;
    

    @Column(name = "name")
    String name;

    @Column(name = "address")
    String address;
    
    @Column(name = "telephone")
    String telephone;

    @Column(name = "email")
    String email;
    
    @Column(name = "fax")
    String fax;
    
    @Column(name = "incomeTaxNo")
    String incomeTaxNo;
    
    @Column(name = "salesTaxNo")
    String salesTaxNo;

    @Column(name = "availableBalance", columnDefinition = "Decimal(10,2) default '0.00'")
    Double availableBalance = 0.00;

    @Column(name = "availableBalance1", columnDefinition = "Decimal(10,2) default '0.00'")
    Double availableBalance1 = 0.00;
    
    Boolean editable=false;
    
    @ManyToOne
    Company company;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public LedgerGroup getLedgerGroup() {
        return ledgerGroup;
    }

    public void setLedgerGroup(LedgerGroup ledgerGroup) {
        this.ledgerGroup = ledgerGroup;
    }

    public Double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(Double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getIncomeTaxNo() {
        return incomeTaxNo;
    }

    public void setIncomeTaxNo(String incomeTaxNo) {
        this.incomeTaxNo = incomeTaxNo;
    }

    public String getSalesTaxNo() {
        return salesTaxNo;
    }

    public void setSalesTaxNo(String salesTaxNo) {
        this.salesTaxNo = salesTaxNo;
    }

    public Double getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(Double availableBalance) {
        this.availableBalance = availableBalance;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Double getAvailableBalance1() {
        return availableBalance1;
    }

    public void setAvailableBalance1(Double availableBalance1) {
        this.availableBalance1 = availableBalance1;
    }

    public Boolean isEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

}
