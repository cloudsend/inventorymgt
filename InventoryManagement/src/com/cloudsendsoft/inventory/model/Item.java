package com.cloudsendsoft.inventory.model;

import java.sql.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author sangeeth
 */
@Entity
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    Long id;
    
    @Column(name = "itemCode", nullable = false)
    String itemCode;
    
    @Column(name = "description", nullable = false)
    String description;
    
    @ManyToOne
    StockGroup stockGroup;
    
    @ManyToOne
    Unit unit;
    
    @Column(name = "weight", columnDefinition="Decimal(10,2) default '0.00'")
    Double weight=0.00; 
    
    @Column(name = "sellingPrice", columnDefinition="Decimal(10,2) default '0.00'")
    Double sellingPrice=0.00;
    
    @Column(name="openingUnitPrice", columnDefinition="Decimal(10,2) default '0.00'")
    private Double openingUnitPrice=0.00;
    @Column(name="openingUnitPrice1", columnDefinition="Decimal(10,2) default '0.00'")
    private Double openingUnitPrice1=0.00;
    
    @Column(name = "openingQty", columnDefinition="Decimal(10,2) default '0.00'")
    Double openingQty=0.00;
    @Column(name = "openingQty1", columnDefinition="Decimal(10,2) default '0.00'")
    Double openingQty1=0.00;
    
    @Column(name = "openingBal", columnDefinition="Decimal(10,2) default '0.00'")
    Double openingBal=0.00; 
    @Column(name = "openingBal1", columnDefinition="Decimal(10,2) default '0.00'")
    Double openingBal1=0.00; 
    
    @Column(name = "availableQty", columnDefinition="Decimal(10,2) default '0.00'")
    Double availableQty=0.00;
    
    @Column(name = "availableQty1", columnDefinition="Decimal(10,2) default '0.00'")
    Double availableQty1=0.00;
    
    @Column(name = "visible", nullable = false)
    Boolean visible;
    
    @ManyToOne
    Company company;
    
    @ManyToOne//(fetch=FetchType.EAGER,cascade = CascadeType.ALL)
    Godown godown;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public StockGroup getStockGroup() {
        return stockGroup;
    }

    public void setStockGroup(StockGroup stockGroup) {
        this.stockGroup = stockGroup;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(Double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public Double getOpeningUnitPrice() {
        return openingUnitPrice;
    }

    public void setOpeningUnitPrice(Double openingUnitPrice) {
        this.openingUnitPrice = openingUnitPrice;
    }

 

    public Double getOpeningQty() {
        return openingQty;
    }

    public void setOpeningQty(Double openingQty) {
        this.openingQty = openingQty;
    }

    

    public Double getOpeningBal() {
        return openingBal;
    }

    public void setOpeningBal(Double openingBal) {
        this.openingBal = openingBal;
    }

    public Double getAvailableQty() {
        return availableQty;
    }

    public void setAvailableQty(Double availableQty) {
        this.availableQty = availableQty;
    }

   
    public Boolean isVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Godown getGodown() {
        return godown;
    }

    public void setGodown(Godown godown) {
        this.godown = godown;
    }
   
    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public boolean equals(Object arg0) {
        Item obj = (Item) arg0;
        if (this.id == obj.getId()) {
            return true;
        }
        return false;
    }

    public Double getAvailableQty1() {
        return availableQty1;
    }

    public void setAvailableQty1(Double availableQty1) {
        this.availableQty1 = availableQty1;
    }

    public Double getOpeningUnitPrice1() {
        return openingUnitPrice1;
    }

    public void setOpeningUnitPrice1(Double openingUnitPrice1) {
        this.openingUnitPrice1 = openingUnitPrice1;
    }

    public Double getOpeningQty1() {
        return openingQty1;
    }

    public void setOpeningQty1(Double openingQty1) {
        this.openingQty1 = openingQty1;
    }

    public Double getOpeningBal1() {
        return openingBal1;
    }

    public void setOpeningBal1(Double openingBal1) {
        this.openingBal1 = openingBal1;
    }
    
}
