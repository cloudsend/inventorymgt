
package com.cloudsendsoft.inventory.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Sangeeth
 */
@Entity
public class StockGroup implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    Long id;
    
    @Column(name = "name")
    String name;
    
    @ManyToOne
    Company company;
    
    @OneToMany
    Set<StockGroup> stockGroups = new HashSet<StockGroup>();

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<StockGroup> getStockGroups() {
        return stockGroups;
    }

    public void setStockGroups(Set<StockGroup> stockGroups) {
        this.stockGroups = stockGroups;
    }

    
}
