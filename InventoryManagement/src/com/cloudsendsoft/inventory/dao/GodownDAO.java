/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import com.cloudsendsoft.inventory.model.Company;
import com.cloudsendsoft.inventory.model.Godown;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class GodownDAO {
    
    static final Logger log = Logger.getLogger(GodownDAO.class.getName());  
    
    public List<Godown> findAllByDESC() {
        Session session = null;
        List<Godown> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Godown where company = :comp ORDER BY id DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByDESC:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

    public List<Godown> findAll() {
        Session session = null;
        List<Godown> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Godown where company = :comp ORDER BY id");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public Godown findByName(String name) {
        Session session = null;
         Godown godown= null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Godown i where i.name = :key and i.company = :comp");
            query.setParameter("comp", GlobalProperty.getCompany());
            query.setString("key", name);
            godown = (Godown) query.uniqueResult();
        } catch (Exception e) {
            log.error("findByName:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return godown;
    }
    
    public Godown findByNameAndCompany(String name,Company company) {
        Session session = null;
         Godown godown= null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Godown i where i.name = :key and i.company = :comp");
            query.setParameter("comp", company);
            query.setString("key", name);
            godown = (Godown) query.uniqueResult();
        } catch (Exception e) {
            log.error("findByNameAndCompany:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return godown;
    }

}
