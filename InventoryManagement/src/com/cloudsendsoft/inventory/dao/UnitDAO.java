/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.dao;

import static com.cloudsendsoft.inventory.dao.ShipmentModeDAO.log;
import com.cloudsendsoft.inventory.model.Company;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.model.Unit;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.PurchaseView;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author Sangeeth
 */
public class UnitDAO {
    
    static final Logger log = Logger.getLogger(UnitDAO.class.getName());
    
    public static List<Unit> findAll() {
        Session session = null;
        List<Unit> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Unit where company = :comp");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }
    
    public Unit findByName(String unitName) {
        Session session = null;
         Unit unit = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Unit u where u.name = :key and company = :comp");
            query.setString("key", unitName);
            query.setParameter("comp", GlobalProperty.getCompany());
            unit = (Unit) query.uniqueResult();
        } catch (Exception e) {
            log.error("findUnitByName:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return unit;
    }
    
    public Unit findByNameAndCompany(String unitName,Company company) {
        Session session = null;
         Unit unit = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Unit u where u.name = :key and company = :comp");
            query.setString("key", unitName);
            query.setParameter("comp", company);
            unit = (Unit) query.uniqueResult();
        } catch (Exception e) {
            log.error("findByNameAndCompany:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return unit;
    }
    
    public List<Unit> findAllByDESC() {
        Session session = null;
        List<Unit> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Unit where company = :comp ORDER BY id DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByDESC:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
}
