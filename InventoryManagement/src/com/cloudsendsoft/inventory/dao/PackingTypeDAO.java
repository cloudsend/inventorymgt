/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import com.cloudsendsoft.inventory.model.Company;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.PackingType;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class PackingTypeDAO {
    
    static final Logger log = Logger.getLogger(PackingTypeDAO.class.getName());   

    public List<PackingType> findAllByDESC() {
        Session session = null;
        List<PackingType> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PackingType where company = :comp ORDER BY id DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByDESC:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<PackingType> findAllByNameASC() {
        Session session = null;
        List<PackingType> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PackingType where company = :comp ORDER BY name ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByNameASC:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<PackingType> findAll() {
        Session session = null;
        List<PackingType> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PackingType where company = :comp ORDER BY id");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<PackingType> findAll(Company comp) {
        Session session = null;
        List<PackingType> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PackingType c where c.company = :key ORDER BY id");
            query.setParameter("key", comp);
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

    public PackingType findByName(String name) {
        Session session = null;
         PackingType packingName= null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PackingType i where i.name = :key and i.company = :comp");
            query.setString("key", name);
            query.setParameter("comp", GlobalProperty.getCompany());
            packingName = (PackingType) query.uniqueResult();
        } catch (Exception e) {
            log.error("findByName:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return packingName;
    }
}