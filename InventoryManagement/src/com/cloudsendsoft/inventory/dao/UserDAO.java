/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Privileges;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.model.User;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class UserDAO {
    
    static final Logger log = Logger.getLogger(UserDAO.class.getName());   

    public List<User> findAllByDesc() {
        Session session = null;
        List<User> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from User order by id desc");
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByDesc:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public Boolean isUserExist(String userName){
        Session session = null;
        User user=null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from User where userName=:userName");
            query.setParameter("userName", userName);
            user = (User) query.uniqueResult();
        } catch (Exception e) {
            log.error("isUserExist:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        if(user==null){
            return false;
        }else{
            return true;
        }
    }
     public User findUser(String userName,String password){
        Session session = null;
        User user=null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from User where userName=:userName and password=:password");
            query.setParameter("userName", userName);
            query.setParameter("password", password);
            user = (User) query.uniqueResult();
        } catch (Exception e) {
            log.error("findAllByDesc:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
            return user;
        }
     
}
