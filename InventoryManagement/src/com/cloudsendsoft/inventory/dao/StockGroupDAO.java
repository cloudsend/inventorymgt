/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Company;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class StockGroupDAO {
    
    static final Logger log = Logger.getLogger(StockGroupDAO.class.getName());   

    public List<StockGroup> findAllByDESC() {
        Session session = null;
        List<StockGroup> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from StockGroup where company = :comp ORDER BY id DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByDESC:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<StockGroup> findAll() {
        Session session = null;
        List<StockGroup> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from StockGroup where company = :comp ORDER BY id");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) { 
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

    public StockGroup findByName(String name) {
        Session session = null;
         StockGroup stockGroup= null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from StockGroup i where i.name = :key and i.company = :comp");
            query.setString("key", name.trim());
            query.setParameter("comp", GlobalProperty.getCompany());
            stockGroup = (StockGroup) query.uniqueResult();
        } catch (Exception e) {
            log.error("findByName:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return stockGroup;
    }
    
    public StockGroup findByNameAndCompany(String name,Company company) {
        Session session = null;
         StockGroup stockGroup= null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from StockGroup i where i.name = :key and i.company = :comp");
            query.setString("key", name.trim());
            query.setParameter("comp", company);
            stockGroup = (StockGroup) query.uniqueResult();
        } catch (Exception e) {
            log.error("findByName:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return stockGroup;
    }
    
   public List<StockGroup> findAllByNameASC() {
        Session session = null;
        List<StockGroup> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from StockGroup i where i.company = :comp ORDER BY i.name");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByNameASC:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
}
