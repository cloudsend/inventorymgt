/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import com.cloudsendsoft.inventory.model.Company;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class TaxDAO {
    
    static final Logger log = Logger.getLogger(TaxDAO.class.getName());   

    public List<Tax> findAllByDESC() {
        Session session = null;
        List<Tax> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Tax where company = :comp ORDER BY id DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByDESC:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Tax> findAll() {
        Session session = null;
        List<Tax> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Tax c where c.company = :key ORDER BY id");
            query.setParameter("key", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }


    public Tax findByName(String name) {
        Session session = null;
         Tax taxAndExpense= null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Tax i where i.name = :key and i.company = :comp");
            query.setString("key", name);
            query.setParameter("comp", GlobalProperty.getCompany());
            taxAndExpense = (Tax) query.uniqueResult();
        } catch (Exception e) {
            log.error("findByName:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return taxAndExpense;
    }
    
    public Tax findByNameAndCompany(String name,Company company) {
        Session session = null;
         Tax taxAndExpense= null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Tax i where i.name = :key and i.company = :comp");
            query.setString("key", name);
            query.setParameter("comp",company);
            taxAndExpense = (Tax) query.uniqueResult();
        } catch (Exception e) {
            log.error("findByNameAndCompany:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return taxAndExpense;
    }
}
