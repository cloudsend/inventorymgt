/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.dao;


import static com.cloudsendsoft.inventory.dao.LedgerDAO.log;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.model.Unit;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author Sangeeth
 */
public class LedgerGroupDAO {
    
     static final Logger log = Logger.getLogger(LedgerGroupDAO.class.getName());
     
    public static List<LedgerGroup> findAll() {
        Session session = null;
        List<LedgerGroup> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from LedgerGroup");
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }
    
    public LedgerGroup findByGroupName(String name) {
        Session session = null;
         LedgerGroup group = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from LedgerGroup u where u.groupName = :key");
            query.setString("key", name);
            group = (LedgerGroup) query.uniqueResult();
        } catch (Exception e) {
            log.error("findByGroupName:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return group;
    }
    
     public List<LedgerGroup> findAllByAsc() {
        Session session = null;
        List<LedgerGroup> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from LedgerGroup l ORDER BY l.groupName ASC");
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByAsc:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }
}
