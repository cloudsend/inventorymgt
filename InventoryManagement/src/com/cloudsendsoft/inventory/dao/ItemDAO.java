/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import static com.cloudsendsoft.inventory.dao.LedgerDAO.log;
import com.cloudsendsoft.inventory.model.Company;
import com.cloudsendsoft.inventory.model.Godown;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class ItemDAO {
    
    static final Logger log = Logger.getLogger(ItemDAO.class.getName());
    
    public List<Item> findAllByStockGroup(StockGroup stockGroup) {
        Session session = null;
         List<Item> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Item i where i.stockGroup = :key and company = :comp order by i.itemCode");
            query.setParameter("key", stockGroup);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findItemsByStockGroup:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
     public List<Item> findAllByItemCode(String itemCode) {
        Session session = null;
         List<Item> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Item i where i.itemCode = :key and company = :comp");
            query.setParameter("key", itemCode);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByItemCode:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
     
     public Item findAllByItemCodeCompanyGodown(String itemCode,Company company,Godown godown) {
        Session session = null;
         Item item = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Item i where i.itemCode = :key and i.company = :comp and i.godown=:godown");
            query.setParameter("key", itemCode);
            query.setParameter("comp", company);
            query.setParameter("godown", godown);
            item = (Item) query.uniqueResult();
        } catch (Exception e) {
            log.error("findAllByItemCodeCompanyGodown:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return item;
    }
    
    public List<Item> findAllByDESC() {
        Session session = null;
        List<Item> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Item where company = :comp ORDER BY id DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByDESC:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }
    public List<Item> findAll() {
        Session session = null;
        List<Item> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Item where company = :comp ORDER BY id");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }
    public List<Item> findAll(Company comp) {
        Session session = null;
        List<Item> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Item c where c.company = :key ORDER BY id");
            query.setParameter("key", comp);
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }
    
    public List<Item> findAll(Boolean visible) {
        Session session = null;
        List<Item> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Item i WHERE i.company = :comp and i.visible=:visible ORDER BY id");
            query.setParameter("comp", GlobalProperty.getCompany());
            query.setBoolean("visible", visible);
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }

    public List<Item> findDistictItem() {
        Session session = null;
        List<Item> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Item i where company = :comp group by itemCode");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }
    
    
    public List<Item> findAllOrderByDescription(Boolean visible) {
        Session session = null;
        List<Item> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Item i WHERE i.visible=:visible and i.company = :comp ORDER BY description ASC");
            query.setBoolean("visible", visible);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllOrderByDescription:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }
    
    public List<Item> findAllItemsByItemCode(String itemCode,Boolean visible) {
         Session session = null;
        List<Item> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Item i WHERE i.itemCode = :itemCode and i.visible=:visible and company = :comp ORDER BY description ASC");
            query.setString("itemCode", itemCode);
            query.setBoolean("visible", visible);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllItemsByItemCode:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }
    public Item findItemByItemCode(String prodId,String godown) {
        Session session = null;
         Item item= null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Item i where i.itemCode = :key and i.godown.name = :godown and company = :comp");
            query.setString("key", prodId);
            query.setString("godown", godown);
            query.setParameter("comp", GlobalProperty.getCompany());
            item = (Item) query.uniqueResult();
        } catch (Exception e) {
            log.error("findItemByItemCode:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return item;
    }
    
   
    public double findTotItemQty(String itemId) {
        Session session = null;
        double itemVal= 0;
        try {       
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(i.availableQty)from Item i where i.itemCode = :key and company = :comp");
            query.setString("key", itemId.trim());
            query.setParameter("comp", GlobalProperty.getCompany());
            itemVal = (double) query.uniqueResult();
        } catch (Exception e) {
            log.error("findTotItemQty:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return itemVal;
    }
    
    public double findTotItemQty1(String itemId) {
        Session session = null;
        double itemVal= 0;
        try {       
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(i.availableQty+i.availableQty1)from Item i where i.itemCode = :key and company = :comp");
            query.setString("key", itemId.trim());
            query.setParameter("comp", GlobalProperty.getCompany());
            itemVal = (double) query.uniqueResult();
        } catch (Exception e) {
            log.error("findTotItemQty1:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return itemVal;
    }
    
    public boolean isItemCodeExist(String pId) {
        Query query=null;
        try{
            query = HibernateUtil.getSessionFactory().openSession().
                createQuery("select 1 from Item i where i.itemCode = :key and i.company = :comp");
        query.setString("key", pId);
        query.setParameter("comp", GlobalProperty.getCompany());
        }catch(Exception e){
            log.error("isItemCodeExist:",e);
        }
        return (query.uniqueResult() != null);
    }

    public List<Item> findAllStartsWithItemCode(String prodId,Boolean visible) {
        Session session = null;
        List<Item> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Item i where i.itemCode LIKE :key and i.company = :comp and i.visible=:visible ORDER BY i.description ASC");
            query.setString("key", prodId + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            query.setBoolean("visible", visible);
            list = query.list();
        } catch (Exception e) {
            log.error("findAllStartsWithItemCode:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<Item> findAllStartsWithDescription(String itemName,Boolean visible) {
        Session session = null;
        List<Item> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Item i where i.description LIKE :key and i.company = :comp and i.visible=:visible ORDER BY i.description ASC");
            query.setString("key", itemName + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            query.setBoolean("visible", visible);
            list = query.list();
        } catch (Exception e) {
            log.error("findAllStartsWithDescription:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<StockGroup> findDistinctStockGroupFromItem() {
        Session session = null;
        List<StockGroup> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select distinct lc from Item p inner join p.stockGroup lc where p.company = :comp order by lc.name asc");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findDistinctStockGroupFromItem:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

    public double findOpeningBalanceTotal() {
        Session session = null;
        double itemVal= 0;
        try {       
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(i.openingBal)from Item i where i.company = :comp");
            query.setParameter("comp", GlobalProperty.getCompany());
            itemVal = (double) query.uniqueResult();
        } catch (Exception e) {
            log.error("findItemByItemCode:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return itemVal;
    }
    
    public List<Object[]> findOpeningBalanceByItemStockGroup(StockGroup stockGroup) {
        Session session = null;
        List<Object[]> list=new ArrayList<Object[]>(0);
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select i.description,sum(i.openingBal)from Item i where i.stockGroup=:stockGroup and i.company = :comp group by i.itemCode");
            query.setParameter("stockGroup", stockGroup);
            query.setParameter("comp", GlobalProperty.getCompany());
            for (Iterator it = query.iterate(); it.hasNext();) {
                Object[] row = (Object[]) it.next();
                list.add(row);
            }
        } catch (Exception e) {
            log.error("findOpeningBalanceByItem:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<Object[]> findClosingBalanceByItemStockGroup(StockGroup stockGroup) {
        Session session = null;
        List<Object[]> list=new ArrayList<Object[]>(0);
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select i.description,sum(i.availableQty),i.itemCode from Item i where i.stockGroup=:stockGroup and i.company = :comp group by i.itemCode");
            query.setParameter("stockGroup", stockGroup);
            query.setParameter("comp", GlobalProperty.getCompany());
            for (Iterator it = query.iterate(); it.hasNext();) {
                Object[] row = (Object[]) it.next();
                list.add(row);
            }
        } catch (Exception e) {
            log.error("findClosingBalanceByItemStockGroup:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
}
