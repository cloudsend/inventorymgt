/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import static com.cloudsendsoft.inventory.dao.PurchaseDAO.log;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseReturn;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesReturn;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.sql.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class PurchaseReturnDAO {
    
    static final Logger log = Logger.getLogger(PurchaseReturnDAO.class.getName());
    
    public List<PurchaseReturn> findAllPurchaseReturn() {
        Session session = null;
         List<PurchaseReturn> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseReturn i where i.company = :comp ORDER BY i.billDate DESC,i.id DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllPurchaseReturn:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<PurchaseReturn> findAllPurchaseReturnTill(java.util.Date date) {
        Session session = null;
         List<PurchaseReturn> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseReturn i where i.company = :comp and i.billDate<:date ORDER BY i.billDate DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            query.setDate("date", date);
            list = query.list();
        } catch (Exception e) {
            log.error("findAllPurchaseReturnTill:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
           
    public List<PurchaseReturn> findAllPurchaseReturn(Purchase purchase) {
        Session session = null;
         List<PurchaseReturn> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseReturn i where i.purchase = :key and i.company = :comp");
            query.setParameter("key", purchase);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllPurchaseReturn:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
         
  public List<PurchaseReturn> findAllByInvoiceNo(String invoiceNo) {
        Session session = null;
         List<PurchaseReturn> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseReturn i where i.company = :comp and i.invoiceNumber LIKE :key");
            query.setParameter("key", invoiceNo + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByInvoiceNo:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<PurchaseReturn> findAllByDate(Date  fromDate,Date toDate) {
        Session session = null;
         List<PurchaseReturn> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseReturn i where i.company = :comp and i.billDate >= :key1 AND i.billDate<=:key2");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByDate:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

    public List<PurchaseReturn> findAllByMonthName(String month) {
        Session session = null;
         List<PurchaseReturn> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseReturn i where i.company = :comp and monthname(i.billDate) =  :month");
            query.setString("month", month);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByMonthName:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
}
