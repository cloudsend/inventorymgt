/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.CommercialInvoice;
import com.cloudsendsoft.inventory.model.Company;
import com.cloudsendsoft.inventory.model.Contra;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Journal;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.Payment;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseOrder;
import com.cloudsendsoft.inventory.model.PurchaseReturn;
import com.cloudsendsoft.inventory.model.Receipt;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesReturn;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.math.BigInteger;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class NumberPropertyDAO {

    static final Logger log = Logger.getLogger(NumberPropertyDAO.class.getName());

    public List<NumberProperty> findAll() {
        Session session = null;
        List<NumberProperty> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from NumberProperty np WHERE np.company = :comp order by np.id");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }

    public List<NumberProperty> findAll(Company comp) {
        Session session = null;
        List<NumberProperty> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from NumberProperty c where c.company = :key order by id");
            query.setParameter("key", comp);
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }

    public NumberProperty findInvoiceNo(String category) {
        Session session = null;
        NumberProperty numberProperty = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from NumberProperty s where s.category = :key and s.company = :comp");
            query.setString("key", category);
            query.setParameter("comp", GlobalProperty.getCompany());
            numberProperty = (NumberProperty) query.uniqueResult();
        } catch (Exception e) {
            log.error("findInvoiceNo:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return numberProperty;
    }

    public boolean isSalesExist(String newSaleNo) {

        Session session = null;
        List<Sales> sales = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Sales s where s.invoiceNumber = :key and s.company = :comp");
            query.setString("key", newSaleNo);
            query.setParameter("comp", GlobalProperty.getCompany());
            sales = query.list();

            if (sales.size() > 0) {
                return true;
            }

        } catch (Exception e) {
            log.error("isSalesExist:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return false;
    }

    public boolean isContraExist(String newContraNo) {

        Session session = null;
        List<Contra> contra = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Contra s where s.contraNo = :key and s.company = :comp");
            query.setString("key", newContraNo);
            query.setParameter("comp", GlobalProperty.getCompany());
            contra = query.list();
            if (contra.size() > 0) {
                return true;
            }

        } catch (Exception e) {
            log.error("isContraExist:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return false;
    }

    public boolean isJournalExist(String newJournalNo) {

        Session session = null;
        List<Journal> journal = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Journal s where s.journalNo = :key and s.company = :comp");
            query.setString("key", newJournalNo);
            query.setParameter("comp", GlobalProperty.getCompany());
            journal = query.list();
            if (journal.size() > 0) {
                return true;
            }

        } catch (Exception e) {
            log.error("isJournalExist:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return false;
    }

    public boolean isComInvoiceExist(String newComInvoiceNo) {

        Session session = null;
        List<CommercialInvoice> commercialInvoice = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from CommercialInvoice s where s.invoiceNumber = :key and s.company = :comp");
            query.setString("key", newComInvoiceNo);
            query.setParameter("comp", GlobalProperty.getCompany());
            commercialInvoice = query.list();

            if (commercialInvoice.size() > 0) {
                return true;
            }

        } catch (Exception e) {
            log.error("isComInvoiceExist:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return false;
    }

    public boolean isPurchaseReturnExist(String newPurchaseReturnNo) {

        Session session = null;
        List<PurchaseReturn> purchaseReturn = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseReturn s where s.invoiceNumber = :key and s.company = :comp");
            query.setString("key", newPurchaseReturnNo);
            query.setParameter("comp", GlobalProperty.getCompany());
            purchaseReturn = query.list();

            if (purchaseReturn.size() > 0) {
                return true;
            }

        } catch (Exception e) {
            log.error("isPurchaseReturnExist:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return false;
    }

    public boolean isSalesReturnExist(String newSalesReturnNo) {

        Session session = null;
        List<SalesReturn> salesReturn = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesReturn s where s.invoiceNumber = :key and s.company = :comp");
            query.setString("key", newSalesReturnNo);
            query.setParameter("comp", GlobalProperty.getCompany());
            salesReturn = query.list();

            if (salesReturn.size() > 0) {
                return true;
            }

        } catch (Exception e) {
            log.error("isSalesReturnExist:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return false;
    }

    public boolean isPurchaseExist(String newPurchaseNo) {

        Session session = null;
        List<Purchase> purchases = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Purchase s where s.invoiceNumber = :key and s.company = :comp");
            query.setString("key", newPurchaseNo);
            query.setParameter("comp", GlobalProperty.getCompany());
            purchases = query.list();

            if (purchases.size() > 0) {
                return true;
            }

        } catch (Exception e) {
            log.error("isPurchaseExist:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return false;
    }
    public boolean isPurchaseOrderExist(String newPurchaseOrderNo) {

        Session session = null;
        List<PurchaseOrder> purchasesOrder = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseOrder s where s.invoiceNumber = :key and s.company = :comp");
            query.setString("key", newPurchaseOrderNo);
            query.setParameter("comp", GlobalProperty.getCompany());
            purchasesOrder = query.list();

            if (purchasesOrder.size() > 0) {
                return true;
            }

        } catch (Exception e) {
            log.error("isPurchaseOrderExist:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return false;
    }

    public boolean isSalesPreformaExist(String newSalePreformaNo) {

        Session session = null;
        List<SalesPreforma> salesPreforma = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesPreforma s where s.invoiceNumber = :key and s.company = :comp");
            query.setString("key", newSalePreformaNo);
            query.setParameter("comp", GlobalProperty.getCompany());
            salesPreforma = query.list();

            if (salesPreforma.size() > 0) {
                return true;
            }

        } catch (Exception e) {
            log.error("isSalesPreformaExist:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return false;
    }

    public boolean isPaymentExist(String newPaymentNo) {

        Session session = null;
        List<Payment> payment = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Payment s where s.paymentNo = :key and s.company = :comp");
            query.setString("key", newPaymentNo);
            query.setParameter("comp", GlobalProperty.getCompany());
            payment = query.list();

            if (payment.size() > 0) {
                return true;
            }

        } catch (Exception e) {
            log.error("isPaymentExist:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return false;
    }

    public boolean isReceiptExist(String newReceiptNo) {

        Session session = null;
        List<Receipt> receipt = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Receipt s where s.receiptNo = :key and s.company = :comp");
            query.setString("key", newReceiptNo);
            query.setParameter("comp", GlobalProperty.getCompany());
            receipt = query.list();

            if (receipt.size() > 0) {
                return true;
            }

        } catch (Exception e) {
            log.error("isReceiptExist:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return false;
    }

}
