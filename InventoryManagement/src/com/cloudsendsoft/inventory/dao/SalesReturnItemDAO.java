/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.dao;

import static com.cloudsendsoft.inventory.dao.SalesItemDAO.log;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.PurchaseReturnItem;
import com.cloudsendsoft.inventory.model.SalesReturnItem;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Sangeeth
 */
public class SalesReturnItemDAO {
    public List<SalesReturnItem> findAll() {
        Session session = null;
        List<SalesReturnItem> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesReturnItem ORDER BY id");
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public double findSalesReturnItemTotal() {
        Session session = null;
        double itemVal= 0;
        try {       
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum((i.quantity*i.unitPrice)-i.discount)from SalesReturnItem i");
            itemVal = (query.uniqueResult()==null)?0:(double) query.uniqueResult();
        } catch (Exception e) {
            log.error("findSalesReturnItemTotal:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return itemVal;
    }
    public Double totalQuantityByItem(Item item) {
        Session session = null;
         Double amt = 0.0;
        try {    
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(i.quantity) from SalesReturnItem i where i.item = :item");
            query.setParameter("item", item);
            amt = (Double) query.uniqueResult();
        } catch (Exception e) {
            log.error("totalQuantityByItem:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return (amt==null)?0:amt;
    }
    public Double totalQuantityByItem1(Item item) {
        Session session = null;
         Double amt = 0.0;
        try {    
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(i.quantity+i.quantity1) from SalesReturnItem i where i.item = :item");
            query.setParameter("item", item);
            amt = (Double) query.uniqueResult();
        } catch (Exception e) {
            log.error("totalQuantityByItem1:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return (amt==null)?0:amt;
    }
}
