/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.dao;

import static com.cloudsendsoft.inventory.dao.ItemDAO.log;
import com.cloudsendsoft.inventory.model.Company;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Unit;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Sangeeth
 */
public class CompanyDAO {
    
    static final Logger log = Logger.getLogger(CompanyDAO.class.getName());
    
     public Company findCompanyById(Long id) {
        Session session = null;
        Company company = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction trans = session.getTransaction();
            trans.begin();
            company = (Company) session.get(Company.class, Long.parseLong(id + ""));
            trans.commit();
        } catch (Exception e) {
            log.error("findItemById:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return company;
    }
     
    public static List<Company> findAll() {
        Session session = null;
        List<Company> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Company");
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }
    
    
    public static List<Company> findAllByDesc() {
        Session session = null;
        List<Company> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Company order by id desc");
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByDesc:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }
    
    public Company findByName(String name) {
        Session session = null;
        Company company = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Company c where c.name = :key");
            query.setString("key", name);
            company = (Company) query.uniqueResult();
        } catch (Exception e) {
            log.error("findByName:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return company;
    }
    
    public Company getDefaultCompany() {
        Session session = null;
        Company company = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Company c where c.isDefault = :key");
            query.setBoolean("key", true);
            company = (Company) query.uniqueResult();
        } catch (Exception e) {
            log.error("getDefaultCompany:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return company;
    }
    
    public boolean isCompanyExist(String name,Date startDate,Date endDate){
        Session session = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Company c where c.name = :name and c.startDate=:startDate and c.endDate=:endDate");
            query.setString("name", name);
            query.setDate("startDate", startDate);
            query.setDate("endDate", endDate);
            if(query.uniqueResult()!=null){
                return true;
            }
        } catch (Exception e) {
            log.error("findByName:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return false;
    }
    
}
