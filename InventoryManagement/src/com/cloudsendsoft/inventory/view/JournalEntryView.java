/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.MultiCurrencyDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.dao.ShipmentModeDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.ItemService;
import com.cloudsendsoft.inventory.services.LedgerService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Contra;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Journal;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.Payment;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseCharge;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseTax;
import com.cloudsendsoft.inventory.model.Receipt;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesCharge;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreformaTax;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaCharge;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesTax;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class JournalEntryView extends javax.swing.JPanel {

    /**
     * Creates new form PurchaseViewPanel
     */
    static double totalAmount = 0;
    int bl_tag_by;
    int bl_tag_to;
    double subTotal = 0.00;
    double grandTotal = 0.00;
    double availBal_to;
    double availBal_by;
    static final Logger log = Logger.getLogger(JournalEntryView.class.getName());

    CRUDServices cRUDServices = new CRUDServices();
    ItemDAO itemDAO = new ItemDAO();
    CommonService commonService = new CommonService();
    MainForm mainForm = null;
    LedgerService ledgerService = new LedgerService();
    TaxDAO taxDAO = new TaxDAO();
    ChargesDAO chargesDAO = new ChargesDAO();
    MultiCurrencyDAO multiCurrencyDAO = new MultiCurrencyDAO();
    ShipmentModeDAO shipmentModeDAO = new ShipmentModeDAO();

    //global usage
    Ledger byLedger = null;
    Ledger toLedger = null;
    NumberProperty numberProperty = null;
    NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
    ArrayList<SalesItem> salesItemList = new ArrayList<SalesItem>();
    Item item = null;
    ArrayList<SalesTax> salesTaxList = new ArrayList<SalesTax>();
    ArrayList<SalesCharge> salesChargesList = new ArrayList<SalesCharge>();

    MultiCurrency currency = null;

    SalesItem salesItem = null;
    DefaultTableModel salesItemTableModel = null;
    Journal journal = null;
    Contra contra = null;
    Payment payment = null;
    Receipt receipt = null;

    int check = 0;
    String journalEntryVal;

    public JournalEntryView(String journalEntryVal) {

        try {
            initComponents();

            commonService.setCurrentPeriodOnCalendar(journalEntryDate);

            if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
                journalEntrySaveButton.setEnabled(false);
            }
            this.journalEntryVal = journalEntryVal;
            if ("Journal".equalsIgnoreCase(journalEntryVal.trim())) {
                journalEntryNameLabel.setText("Journal");
                numberProperty = numberPropertyDAO.findInvoiceNo("Journal");
                journalEntryNoLabel.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
            }
            if ("Contra".equalsIgnoreCase(journalEntryVal.trim())) {
                journalEntryNameLabel.setText("Contra");

                numberProperty = numberPropertyDAO.findInvoiceNo("Contra");
                journalEntryNoLabel.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
            }
            if ("Payment".equalsIgnoreCase(journalEntryVal.trim())) {
                journalEntryNameLabel.setText("Payment");

                numberProperty = numberPropertyDAO.findInvoiceNo("Payment");
                journalEntryNoLabel.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
            }
            if ("Receipt".equalsIgnoreCase(journalEntryVal.trim())) {
                journalEntryNameLabel.setText("Receipt");

                numberProperty = numberPropertyDAO.findInvoiceNo("Receipt");
                journalEntryNoLabel.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
            }

            journalEntryDate.setDate(new java.util.Date());
            byAvalBalLabel.setVisible(false);
            toAvalBalLabel.setVisible(false);
            setFocusOrder();
        } catch (Exception e) {
            log.error("Journal:", e);
        }

    }

    void setFocusOrder() {
        try {
           
            journalEntryDeditorText.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    journalEntryDebitText.requestFocusInWindow();
                }
           });
           journalEntryDebitText.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    journalEntryCreditorText.requestFocusInWindow();
                }
           }); 
           journalEntryCreditorText.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    journalEntrySaveButton.requestFocusInWindow();
                }
           });
          
           
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

////
////    /**
////     * This method is called from within the constructor to initialize the form.
////     * WARNING: Do NOT modify this code. The content of this method is always
////     * regenerated by the Form Editor.
////     */
////    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headingPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        journalEntryNameLabel = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        journalEntryNoLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        journalEntryDeditorText = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        byAvalBalLabel = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        journalEntryCreditorText = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        toAvalBalLabel = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel15 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        journalEntryNarration = new javax.swing.JTextArea();
        debitTotalLabel = new javax.swing.JLabel();
        creditTotalLabel = new javax.swing.JLabel();
        journalEntryDebitText = new javax.swing.JFormattedTextField();
        journalEntryCreditText = new javax.swing.JFormattedTextField();
        journalEntrySaveButton = new javax.swing.JButton();
        journalEntryDate = new com.toedter.calendar.JDateChooser();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        headingPanel.setOpaque(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Accounting Voucher");

        journalEntryNameLabel.setBackground(new java.awt.Color(255, 255, 51));
        journalEntryNameLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        journalEntryNameLabel.setText("Journal");
        journalEntryNameLabel.setOpaque(true);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("No:-");

        journalEntryNoLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        journalEntryNoLabel.setText("1");

        javax.swing.GroupLayout headingPanelLayout = new javax.swing.GroupLayout(headingPanel);
        headingPanel.setLayout(headingPanelLayout);
        headingPanelLayout.setHorizontalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(headingPanelLayout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(headingPanelLayout.createSequentialGroup()
                                .addComponent(journalEntryNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(journalEntryNoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(headingPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 963, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        headingPanelLayout.setVerticalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(journalEntryNameLabel)
                    .addComponent(jLabel3)
                    .addComponent(journalEntryNoLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        add(headingPanel, java.awt.BorderLayout.PAGE_START);

        jPanel1.setOpaque(false);

        jPanel2.setOpaque(false);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Date");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Particulars");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Debit");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("Credit");

        jLabel9.setText("By");

        journalEntryDeditorText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                journalEntryDeditorTextActionPerformed(evt);
            }
        });
        journalEntryDeditorText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                journalEntryDeditorTextKeyPressed(evt);
            }
        });

        jLabel10.setText("Current Balance:");

        byAvalBalLabel.setText("00.00");

        jLabel12.setText("To");

        journalEntryCreditorText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                journalEntryCreditorTextActionPerformed(evt);
            }
        });
        journalEntryCreditorText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                journalEntryCreditorTextFocusLost(evt);
            }
        });
        journalEntryCreditorText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                journalEntryCreditorTextKeyPressed(evt);
            }
        });

        jLabel13.setText("Current Balance:");

        toAvalBalLabel.setText("00.00");

        jLabel15.setText("Narration");

        journalEntryNarration.setColumns(20);
        journalEntryNarration.setRows(5);
        jScrollPane1.setViewportView(journalEntryNarration);

        debitTotalLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        debitTotalLabel.setText("00.00");

        creditTotalLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        creditTotalLabel.setText("00.00");

        journalEntryDebitText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        journalEntryDebitText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        journalEntryDebitText.setText("0.00");
        journalEntryDebitText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                journalEntryDebitTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                journalEntryDebitTextFocusLost(evt);
            }
        });
        journalEntryDebitText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                journalEntryDebitTextKeyTyped(evt);
            }
        });

        journalEntryCreditText.setEditable(false);
        journalEntryCreditText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        journalEntryCreditText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        journalEntryCreditText.setText("0.00");
        journalEntryCreditText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                journalEntryCreditTextActionPerformed(evt);
            }
        });
        journalEntryCreditText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                journalEntryCreditTextFocusGained(evt);
            }
        });
        journalEntryCreditText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                journalEntryCreditTextKeyTyped(evt);
            }
        });

        journalEntrySaveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        journalEntrySaveButton.setText("Save");
        journalEntrySaveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                journalEntrySaveButtonActionPerformed(evt);
            }
        });

        journalEntryDate.setDateFormatString("dd/MM/yyyy");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(546, 546, 546)
                        .addComponent(debitTotalLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(journalEntryDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(28, 28, 28)
                                        .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(176, 176, 176)
                                        .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(jLabel13)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(toAvalBalLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(jLabel10)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(byAvalBalLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                        .addGap(402, 402, 402))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(journalEntryCreditorText, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(journalEntryDeditorText, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel6)
                                .addGap(218, 218, 218)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(57, 57, 57)
                                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(52, 52, 52)
                                        .addComponent(journalEntryDebitText, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel15)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 379, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(creditTotalLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(journalEntrySaveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(547, 547, 547))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(780, 780, 780)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(780, 780, 780)
                        .addComponent(journalEntryCreditText, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jSeparator2, javax.swing.GroupLayout.DEFAULT_SIZE, 955, Short.MAX_VALUE)
                            .addComponent(jSeparator3)
                            .addComponent(jSeparator4))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(journalEntryDeditorText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(journalEntryDebitText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(journalEntryDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(byAvalBalLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(journalEntryCreditorText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(journalEntryCreditText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(toAvalBalLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                .addComponent(jLabel15)
                .addGap(11, 11, 11)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(creditTotalLabel)
                    .addComponent(debitTotalLabel))
                .addGap(15, 15, 15)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(journalEntrySaveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 978, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        add(jPanel1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void journalEntryDeditorTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_journalEntryDeditorTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_journalEntryDeditorTextActionPerformed

    private void journalEntryDeditorTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_journalEntryDeditorTextKeyPressed
        try {
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                //String productId = productIdText.getText().trim();
                if ("Journal".equalsIgnoreCase(journalEntryVal.trim())) {
                    List<Ledger> listOfByLedgers = ledgerService.populatePopupTableByJournalBy(journalEntryDeditorText.getText().trim(), popupTableDialog);
                    popupTableDialog.setVisible(true);
                    ArrayList selectedRow = popupTableDialog.getSelectedRow();
                    if (null != selectedRow) {
                        int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                        byLedger = listOfByLedgers.get(index);
                        journalEntryDeditorText.setText(byLedger.getLedgerName());
                        byAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(byLedger.getAvailableBalance()) + "");
                        byAvalBalLabel.setVisible(true);
                    }

                }
                if ("Contra".equalsIgnoreCase(journalEntryVal.trim())) {
                    List<Ledger> listOfByLedgers = ledgerService.populatePopupTableByContraBy(journalEntryDeditorText.getText().trim(), popupTableDialog);
                    popupTableDialog.setVisible(true);
                    ArrayList selectedRow = popupTableDialog.getSelectedRow();
                    if (null != selectedRow) {
                        int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                        byLedger = listOfByLedgers.get(index);
                        journalEntryDeditorText.setText(byLedger.getLedgerName());
                        byAvalBalLabel.setVisible(true);
                        byAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(byLedger.getAvailableBalance()) + "");

                    }

                }
                if ("Payment".equalsIgnoreCase(journalEntryVal.trim())) {

                    List<Ledger> listOfByLedgers = ledgerService.populatePopupTableByPaymentBy(journalEntryDeditorText.getText().trim(), popupTableDialog);
                    popupTableDialog.setVisible(true);
                    ArrayList selectedRow = popupTableDialog.getSelectedRow();
                    if (null != selectedRow) {
                        int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                        byLedger = listOfByLedgers.get(index);
                        journalEntryDeditorText.setText(byLedger.getLedgerName());
                        byAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(byLedger.getAvailableBalance()) + "");
                    }
                }
                if ("Receipt".equalsIgnoreCase(journalEntryVal.trim())) {
                    List<Ledger> listOfByLedgers = ledgerService.populatePopupTableByReceiptBy(journalEntryDeditorText.getText().trim(), popupTableDialog);
                    popupTableDialog.setVisible(true);
                    ArrayList selectedRow = popupTableDialog.getSelectedRow();
                    if (null != selectedRow) {
                        int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                        byLedger = listOfByLedgers.get(index);
                        journalEntryDeditorText.setText(byLedger.getLedgerName());
                        byAvalBalLabel.setVisible(true);
                        byAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(byLedger.getAvailableBalance()) + "");
                    }
                }

            }

        } catch (Exception e) {
            log.error("productIdTextKeyPressed:", e);
        } // TODO add your handling code here:
    }//GEN-LAST:event_journalEntryDeditorTextKeyPressed

    private void journalEntryCreditorTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_journalEntryCreditorTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_journalEntryCreditorTextActionPerformed

    private void journalEntryCreditorTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_journalEntryCreditorTextFocusLost
        //double debitAmt=Double.parseDouble(journalDebitText.getText());

    }//GEN-LAST:event_journalEntryCreditorTextFocusLost

    private void journalEntryCreditorTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_journalEntryCreditorTextKeyPressed
        try {

            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                if ("Journal".equalsIgnoreCase(journalEntryVal.trim())) {
                    List<Ledger> listOfByLedgers = ledgerService.populatePopupTableByJournalTo(journalEntryCreditorText.getText().trim(), popupTableDialog);
                    popupTableDialog.setVisible(true);
                    ArrayList selectedRow = popupTableDialog.getSelectedRow();
                    if (null != selectedRow) {
                        int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                        toLedger = listOfByLedgers.get(index);
                        journalEntryCreditorText.setText(toLedger.getLedgerName());
                        /* toAvalBalLabel.setVisible(true);
                         toAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(toLedger.getAvailableBalance()) + "");*/

                        journalEntryCreditText.setText(journalEntryDebitText.getText());
                        toAvalBalLabel.setVisible(true);
                        toAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(toLedger.getAvailableBalance()) + "");
                        debitTotalLabel.setText(commonService.formatIntoCurrencyAsString(Double.parseDouble(journalEntryDebitText.getText())));
                        creditTotalLabel.setText(commonService.formatIntoCurrencyAsString(Double.parseDouble(journalEntryDebitText.getText())));
                    }

                }
                if ("Contra".equalsIgnoreCase(journalEntryVal.trim())) {
                    List<Ledger> listOfByLedgers = ledgerService.populatePopupTableByContraTo(journalEntryCreditorText.getText().trim(), popupTableDialog);
                    popupTableDialog.setVisible(true);
                    ArrayList selectedRow = popupTableDialog.getSelectedRow();
                    if (null != selectedRow) {
                        int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                        toLedger = listOfByLedgers.get(index);
                        journalEntryCreditorText.setText(toLedger.getLedgerName());
                        /* toAvalBalLabel.setVisible(true);
                         toAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(toLedger.getAvailableBalance()) + "");*/
                        journalEntryCreditText.setText(journalEntryDebitText.getText());
                        toAvalBalLabel.setVisible(true);
                        toAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(toLedger.getAvailableBalance()) + "");
                        debitTotalLabel.setText(commonService.formatIntoCurrencyAsString(Double.parseDouble(journalEntryDebitText.getText())));
                        creditTotalLabel.setText(commonService.formatIntoCurrencyAsString(Double.parseDouble(journalEntryDebitText.getText())));

                    }

                }
                if ("Payment".equalsIgnoreCase(journalEntryVal.trim())) {
                    List<Ledger> listOfByLedgers = ledgerService.populatePopupTableByPaymentTo(journalEntryCreditorText.getText().trim(), popupTableDialog);
                    popupTableDialog.setVisible(true);
                    ArrayList selectedRow = popupTableDialog.getSelectedRow();
                    if (null != selectedRow) {
                        int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                        toLedger = listOfByLedgers.get(index);
                        journalEntryCreditorText.setText(toLedger.getLedgerName());
                        /*toAvalBalLabel.setVisible(true);
                         toAvalBalLabel.setText(toLedger.getAvailableBalance() + "");*/

                        //double debitAmt = Double.parseDouble(journalEntryDebitText.getText());
                        journalEntryCreditText.setText(journalEntryDebitText.getText());
                        toAvalBalLabel.setVisible(true);
                        toAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(toLedger.getAvailableBalance()) + "");
                        debitTotalLabel.setText(commonService.formatIntoCurrencyAsString(Double.parseDouble(journalEntryDebitText.getText())));
                        creditTotalLabel.setText(commonService.formatIntoCurrencyAsString(Double.parseDouble(journalEntryDebitText.getText())));
                    }

                }
                if ("Receipt".equalsIgnoreCase(journalEntryVal.trim())) {
                    List<Ledger> listOfByLedgers = ledgerService.populatePopupTableByReceiptTo(journalEntryCreditorText.getText().trim(), popupTableDialog);
                    popupTableDialog.setVisible(true);
                    ArrayList selectedRow = popupTableDialog.getSelectedRow();
                    if (null != selectedRow) {
                        int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                        toLedger = listOfByLedgers.get(index);
                        journalEntryCreditorText.setText(toLedger.getLedgerName());
                        journalEntryCreditText.setText(journalEntryDebitText.getText());
                        toAvalBalLabel.setVisible(true);
                        toAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(toLedger.getAvailableBalance()) + "");
                        debitTotalLabel.setText(commonService.formatIntoCurrencyAsString(Double.parseDouble(journalEntryDebitText.getText())));
                        creditTotalLabel.setText(commonService.formatIntoCurrencyAsString(Double.parseDouble(journalEntryDebitText.getText())));
                    }

                }

            }

        } catch (Exception e) {
            log.error("productIdTextKeyPressed:", e);
        }
    }//GEN-LAST:event_journalEntryCreditorTextKeyPressed

    private void journalEntryDebitTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_journalEntryDebitTextFocusGained
        journalEntryDebitText.selectAll();
    }//GEN-LAST:event_journalEntryDebitTextFocusGained

    private void journalEntryDebitTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_journalEntryDebitTextFocusLost

        byAvalBalLabel.setVisible(true);
        byAvalBalLabel.setText(byLedger.getAvailableBalance() + "");
    }//GEN-LAST:event_journalEntryDebitTextFocusLost

    private void journalEntryDebitTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_journalEntryDebitTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_journalEntryDebitTextKeyTyped

    private void journalEntryCreditTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_journalEntryCreditTextFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_journalEntryCreditTextFocusGained

    private void journalEntryCreditTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_journalEntryCreditTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_journalEntryCreditTextKeyTyped

    private void journalEntrySaveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_journalEntrySaveButtonActionPerformed
        try {
            if (journalEntryDate.getDate() != null && journalEntryDeditorText.getText().length() > 0 && journalEntryCreditText.getText().length() > 0) {
                if (MsgBox.confirm("Are you sure you want to save?")) {
                    if ("Journal".equalsIgnoreCase(journalEntryVal.trim())) {
                        if (journal == null) {
                            journal = new Journal();
                            check = 0;
                        }
                        journal.setJournalDate(commonService.utilDateToSqlDate(journalEntryDate.getDate()));
                        journal.setJournalNo(journalEntryNoLabel.getText());
                        journal.setAmount(Double.parseDouble(journalEntryDebitText.getText()));
                        journal.setNarration(journalEntryNarration.getText());
                        journal.setLedgerBy(byLedger);
                        journal.setLedgerTo(toLedger);
                        journal.setCompany(GlobalProperty.getCompany());
                        if (check == 0) {
                            cRUDServices.saveModel(journal);
                        } else {
                            cRUDServices.saveOrUpdateModel(journal);
                        }
                        if (byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Asset") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Indirect Expense") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Direct Expense")) {
                            byLedger.setAvailableBalance(byLedger.getAvailableBalance() + Double.parseDouble(journalEntryDebitText.getText()));
                            cRUDServices.saveOrUpdateModel(byLedger);
                        } else if (byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Liability") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Indirect Income") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Direct Income") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Capital")) {
                            byLedger.setAvailableBalance(byLedger.getAvailableBalance() - Double.parseDouble(journalEntryDebitText.getText()));
                            cRUDServices.saveOrUpdateModel(byLedger);
                        }

                        if (toLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Asset") || toLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Indirect Expense") || toLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Direct Expense")) {
                            toLedger.setAvailableBalance(toLedger.getAvailableBalance() - Double.parseDouble(journalEntryCreditText.getText()));
                            cRUDServices.saveOrUpdateModel(toLedger);
                        } else if (toLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Liability") || toLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Indirect Income") || toLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Direct Income") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Capital")) {
                            toLedger.setAvailableBalance(toLedger.getAvailableBalance() - Double.parseDouble(journalEntryCreditText.getText()));
                            cRUDServices.saveOrUpdateModel(toLedger);
                        }
                        numberProperty.setNumber(numberProperty.getNumber() + 1);
                        cRUDServices.saveOrUpdateModel(numberProperty);
                        MsgBox.success("Journal Saved Successfully");
                        clearAllFields();
                    }
                    if ("Contra".equalsIgnoreCase(journalEntryVal.trim())) {
                        if (contra == null) {
                            contra = new Contra();
                            check = 0;
                        }
                        contra.setContraDate(commonService.utilDateToSqlDate(journalEntryDate.getDate()));
                        contra.setContraNo(journalEntryNoLabel.getText());
                        contra.setAmount(Double.parseDouble(journalEntryDebitText.getText()));
                        contra.setNarration(journalEntryNarration.getText());
                        contra.setLedgerBy(byLedger);
                        contra.setLedgerTo(toLedger);
                        contra.setCompany(GlobalProperty.getCompany());
                        if (check == 0) {
                            cRUDServices.saveModel(contra);
                        } else {
                            cRUDServices.saveOrUpdateModel(contra);
                        }
                        numberProperty.setNumber(numberProperty.getNumber() + 1);
                        cRUDServices.saveOrUpdateModel(numberProperty);
                        byLedger.setAvailableBalance(byLedger.getAvailableBalance() + Double.parseDouble(journalEntryDebitText.getText()));
                        cRUDServices.saveOrUpdateModel(byLedger);
                        toLedger.setAvailableBalance(toLedger.getAvailableBalance() - Double.parseDouble(journalEntryCreditText.getText()));
                        cRUDServices.saveOrUpdateModel(toLedger);
                        MsgBox.success("Contra Saved Successfully");
                        clearAllFields();
                    }
                    if ("Payment".equalsIgnoreCase(journalEntryVal.trim())) {
                        if (payment == null) {
                            payment = new Payment();
                            check = 0;
                        }
                        payment.setPaymentDate(commonService.utilDateToSqlDate(journalEntryDate.getDate()));
                        payment.setPaymentNo(journalEntryNoLabel.getText());
                        payment.setAmount(Double.parseDouble(journalEntryDebitText.getText()));
                        payment.setNarration(journalEntryNarration.getText());
                        payment.setLedgerBy(byLedger);
                        payment.setLedgerTo(toLedger);
                        payment.setCompany(GlobalProperty.getCompany());
                        if (check == 1) {
                            cRUDServices.saveOrUpdateModel(payment);
                        } else {
                            cRUDServices.saveModel(payment);
                        }
                        if (byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Liability") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Direct Income") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Indirect Income") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Capital")) {
                            byLedger.setAvailableBalance(byLedger.getAvailableBalance() - Double.parseDouble(journalEntryDebitText.getText()));
                            cRUDServices.saveOrUpdateModel(byLedger);
                        } else {
                            byLedger.setAvailableBalance(byLedger.getAvailableBalance() + Double.parseDouble(journalEntryDebitText.getText()));
                            cRUDServices.saveOrUpdateModel(byLedger);
                        }
                        if (toLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Asset") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Direct Expense") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Indirect Expence")) {
                            toLedger.setAvailableBalance(toLedger.getAvailableBalance() - Double.parseDouble(journalEntryCreditText.getText()));
                            cRUDServices.saveOrUpdateModel(toLedger);
                        } else {
                            toLedger.setAvailableBalance(toLedger.getAvailableBalance() + Double.parseDouble(journalEntryCreditText.getText()));
                            cRUDServices.saveOrUpdateModel(toLedger);
                        }
                        numberProperty.setNumber(numberProperty.getNumber() + 1);
                        cRUDServices.saveOrUpdateModel(numberProperty);
                        MsgBox.success("Payment Saved Successfully");
                        clearAllFields();
                    }
                    if ("Receipt".equalsIgnoreCase(journalEntryVal.trim())) {
                        if (receipt == null) {
                            receipt = new Receipt();
                            check = 0;
                        }
                        receipt.setReceiptDate(commonService.utilDateToSqlDate(journalEntryDate.getDate()));
                        receipt.setReceiptNo(journalEntryNoLabel.getText());
                        receipt.setAmount(Double.parseDouble(journalEntryDebitText.getText()));
                        receipt.setNarration(journalEntryNarration.getText());
                        receipt.setLedgerBy(byLedger);
                        receipt.setLedgerTo(toLedger);
                        receipt.setCompany(GlobalProperty.getCompany());
                        if (check == 1) {
                            cRUDServices.saveOrUpdateModel(receipt);
                        } else {
                            cRUDServices.saveModel(receipt);
                        }
                        if (byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Liability") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Direct Income") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Indirect Income") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Capital")) {
                            byLedger.setAvailableBalance(byLedger.getAvailableBalance() - Double.parseDouble(journalEntryDebitText.getText()));
                            cRUDServices.saveOrUpdateModel(byLedger);
                        } else {
                            byLedger.setAvailableBalance(byLedger.getAvailableBalance() + Double.parseDouble(journalEntryDebitText.getText()));
                            cRUDServices.saveOrUpdateModel(byLedger);
                        }
                        if (toLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Asset") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Direct Expense") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Indirect Expence")) {
                            toLedger.setAvailableBalance(toLedger.getAvailableBalance() - Double.parseDouble(journalEntryCreditText.getText()));
                            cRUDServices.saveOrUpdateModel(toLedger);
                        } else {
                            toLedger.setAvailableBalance(toLedger.getAvailableBalance() + Double.parseDouble(journalEntryCreditText.getText()));
                            cRUDServices.saveOrUpdateModel(toLedger);
                        }
                        numberProperty.setNumber(numberProperty.getNumber() + 1);
                        cRUDServices.saveOrUpdateModel(numberProperty);
                        MsgBox.success("Invoice Saved Successfully");
                        clearAllFields();
                    }
                }
            } else {
                MsgBox.warning("Required fields are not entered");
            }
            //listReceiptItemHistory(receipt);
        } catch (Exception ex) {
            log.error(ex);
        }
    }//GEN-LAST:event_journalEntrySaveButtonActionPerformed

    private void journalEntryCreditTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_journalEntryCreditTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_journalEntryCreditTextActionPerformed
    public void listReceiptItemHistory(Receipt receipt) {
        this.receipt = receipt;
        journalEntryDate.setDate(receipt.getReceiptDate());
        journalEntryDeditorText.setText(receipt.getLedgerBy().getLedgerName() + "");
        journalEntryCreditorText.setText(receipt.getLedgerTo().getLedgerName() + "");
        journalEntryDebitText.setText(receipt.getAmount() + "");
        journalEntryCreditText.setText(receipt.getAmount() + "");
        journalEntryNarration.setText(receipt.getNarration());
        byAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(receipt.getLedgerBy().getAvailableBalance()) + "");
        toAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(receipt.getLedgerTo().getAvailableBalance()) + "");
        debitTotalLabel.setText(commonService.formatIntoCurrencyAsString(receipt.getAmount()) + "");
        creditTotalLabel.setText(commonService.formatIntoCurrencyAsString(receipt.getAmount()) + "");
        journalEntryNoLabel.setText(receipt.getReceiptNo());
        byLedger = receipt.getLedgerBy();
        toLedger = receipt.getLedgerTo();
        journalEntrySaveButton.setText("Update");
        check = 1;
    }

    public void listJournalItemHistory(Journal journal) {
        this.journal = journal;
        journalEntryDate.setDate(journal.getJournalDate());
        journalEntryDeditorText.setText(journal.getLedgerBy().getLedgerName() + "");
        journalEntryCreditorText.setText(journal.getLedgerTo().getLedgerName() + "");
        journalEntryDebitText.setText(journal.getAmount() + "");
        journalEntryCreditText.setText(journal.getAmount() + "");
        journalEntryNarration.setText(journal.getNarration());
        byAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(journal.getLedgerBy().getAvailableBalance()) + "");
        toAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(journal.getLedgerTo().getAvailableBalance()) + "");
        debitTotalLabel.setText(commonService.formatIntoCurrencyAsString(journal.getAmount()) + "");
        creditTotalLabel.setText(commonService.formatIntoCurrencyAsString(journal.getAmount()) + "");
        journalEntryNoLabel.setText(journal.getJournalNo());
        byLedger = journal.getLedgerBy();
        toLedger = journal.getLedgerTo();
        journalEntrySaveButton.setText("Update");
        check = 1;
    }

    public void listContraItemHistory(Contra contra) {
        this.contra = contra;
        journalEntryDate.setDate(contra.getContraDate());
        journalEntryDeditorText.setText(contra.getLedgerBy().getLedgerName() + "");
        journalEntryCreditorText.setText(contra.getLedgerTo().getLedgerName() + "");
        journalEntryDebitText.setText(contra.getAmount() + "");
        journalEntryCreditText.setText(contra.getAmount() + "");
        journalEntryNarration.setText(contra.getNarration());
        byAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(contra.getLedgerBy().getAvailableBalance()) + "");
        toAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(contra.getLedgerTo().getAvailableBalance()) + "");
        debitTotalLabel.setText(commonService.formatIntoCurrencyAsString(contra.getAmount()) + "");
        creditTotalLabel.setText(commonService.formatIntoCurrencyAsString(contra.getAmount()) + "");
        journalEntryNoLabel.setText(contra.getContraNo());
        byLedger = contra.getLedgerBy();
        toLedger = contra.getLedgerTo();
        journalEntrySaveButton.setText("Update");
        check = 1;
    }

    public void listPaymentItemHistory(Payment payment) {
        this.payment = payment;
        journalEntryDate.setDate(payment.getPaymentDate());
        journalEntryDeditorText.setText(payment.getLedgerBy().getLedgerName() + "");
        journalEntryCreditorText.setText(payment.getLedgerTo().getLedgerName() + "");
        journalEntryDebitText.setText(payment.getAmount() + "");
        journalEntryCreditText.setText(payment.getAmount() + "");
        journalEntryNarration.setText(payment.getNarration());
        byAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(payment.getLedgerBy().getAvailableBalance()) + "");
        toAvalBalLabel.setText(commonService.formatIntoCurrencyAsString(payment.getLedgerTo().getAvailableBalance()) + "");
        debitTotalLabel.setText(commonService.formatIntoCurrencyAsString(payment.getAmount()) + "");
        creditTotalLabel.setText(commonService.formatIntoCurrencyAsString(payment.getAmount()) + "");
        journalEntryNoLabel.setText(payment.getPaymentNo());
        byLedger = payment.getLedgerBy();
        toLedger = payment.getLedgerTo();
        journalEntrySaveButton.setText("Update");
        check = 1;
    }

    void fillSalesItemTable() {
//        
    }

    void clearAllFields() {
        try {
            journalEntryDate.setDate(new java.util.Date());
            commonService.clearTextFields(new JTextField[]{journalEntryDeditorText, journalEntryCreditorText});
            commonService.clearTextArea(new JTextArea[]{journalEntryNarration});
            commonService.clearCurrencyFields(new JTextField[]{journalEntryDebitText, journalEntryCreditText});
            debitTotalLabel.setText("0.00");
            creditTotalLabel.setText("0.00");
            byAvalBalLabel.setText("0.00");
            toAvalBalLabel.setText("0.00");
            numberProperty = numberPropertyDAO.findInvoiceNo("Journal");
            journalEntryNoLabel.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());

        } catch (Exception e) {
            log.error("clearAllFields:", e);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel byAvalBalLabel;
    private javax.swing.JLabel creditTotalLabel;
    private javax.swing.JLabel debitTotalLabel;
    private javax.swing.JPanel headingPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JFormattedTextField journalEntryCreditText;
    private javax.swing.JTextField journalEntryCreditorText;
    private com.toedter.calendar.JDateChooser journalEntryDate;
    private javax.swing.JFormattedTextField journalEntryDebitText;
    private javax.swing.JTextField journalEntryDeditorText;
    private javax.swing.JLabel journalEntryNameLabel;
    private javax.swing.JTextArea journalEntryNarration;
    private javax.swing.JLabel journalEntryNoLabel;
    private javax.swing.JButton journalEntrySaveButton;
    private javax.swing.JLabel toAvalBalLabel;
    // End of variables declaration//GEN-END:variables
}
