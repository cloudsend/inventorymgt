/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.CTableModel;
import com.cloudsendsoft.inventory.dao.CompanyDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.LedgerGroupDAO;
import com.cloudsendsoft.inventory.dao.PurchaseDAO;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.StockGroupDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.report.BalanceSheetReport;
import com.cloudsendsoft.inventory.report.JTableReport;
import com.cloudsendsoft.inventory.report.TrialBalanceReport;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import static com.cloudsendsoft.inventory.view.MainForm.jMainFrame;
import static com.cloudsendsoft.inventory.view.PurchaseView.log;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class TrialBalanceView extends javax.swing.JPanel {

    static final Logger log = Logger.getLogger(TrialBalanceView.class.getName());

    CommonService commonService = new CommonService();

    CTableModel trialBalanceTableModel = null;

//    List<StockGroup> listOfStockGroup = null;
//    List<Item> listOfItem = null;
//    List<PurchaseItem> listOfPurchaseItem = null;
//    List<Purchase> listOfPurchase = null;
//
//    List<SalesItem> listOfSalesItem = null;
//    List<Sales> listOfSales = null;
//
//    StockGroup stockGroup = null;
//    Item item = null;
//    String month = null;
//
//    StockGroupDAO stockGroupDAO = new StockGroupDAO();
//    ItemDAO itemDAO = new ItemDAO();
//    PurchaseDAO purchaseDAO = new PurchaseDAO();
//    PurchaseItemDAO purchaseItemDAO = new PurchaseItemDAO();
//    SalesDAO salesDAO = new SalesDAO();
    int screen = 1;
    int index = 0;

    boolean isDetailedView = false;

//    String[] months = new String[13];
//
//    String invoiceDate = null;
//    String invoiceType = null;
//    String invoiceLedger = null;
//    String invoiceNumber = null;
//
//    PurchaseView purchase = null;
    LedgerGroupDAO ledgerGroupDAO = new LedgerGroupDAO();
    List<LedgerGroup> listOfLedgerGroup = null;

    LedgerDAO ledgerDAO = new LedgerDAO();
    List<Ledger> listOfLedger = null;

    LedgerGroup ledgerGroup = null;

    double totalDebitAmt = 0.00;
    double totalCreditAmt = 0.00;

    ItemDAO itemDAO=new ItemDAO();
    
    List<StockGroup> listOfStockGroup = null;
    
    public TrialBalanceView() {
        initComponents();
        centerHeadingLabel.setText("Trial Balance as at " + commonService.sqlDateToString(GlobalProperty.company.getEndDate()));
        //table background color settings
        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);
        /*       stockSummaryTable.setBackground(this.getBackground());
         stockSummaryTable.getTableHeader().setBackground(this.getBackground());
         stockSummaryTable.getTableHeader().setForeground(this.getBackground());
         */
        //set table design
        trialBalanceTable.setRowHeight(GlobalProperty.tableRowHieght);

        trialBalanceTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {

            //alternate row color jtable
            @Override
            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int column) {
                Component c = super.getTableCellRendererComponent(table,
                        value, isSelected, hasFocus, row, column);
                c.setBackground(row % 2 == 0 ? Color.GRAY : Color.LIGHT_GRAY);
                return c;
            }
        ;
        });

        trialBalanceTable.setShowGrid(false);
        trialBalanceTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{"Particulars", "Debit", "Credit"}
        ));
        trialBalanceTableModel = (CTableModel) trialBalanceTable.getModel();
        CommonService.setWidthAsPercentages(trialBalanceTable, .80, .20, .20);

        trialBalanceTableModel.setRowCount(0);

        //fetch all ledger groups
        //listOfLedgerGroup = ledgerGroupDAO.findAllByAsc();
        listOfLedgerGroup = ledgerDAO.findDistinctLedgerGroupFromLedger();

        //fetching all stockgroups
        listOfStockGroup = itemDAO.findDistinctStockGroupFromItem();
        
        fillTrialBalanceTable();
    }

    void fillTrialBalanceTable() {
        try {
            trialBalanceTableModel.setRowCount(0);
            totalDebitAmt = 0.00;
            totalCreditAmt = 0.00;
            
            //Opening Stock
            double openingBalanceTot = itemDAO.findOpeningBalanceTotal();
            totalDebitAmt += openingBalanceTot;

            //double purchaseTotal = 0;
            //double salesTotal = 0;
            if (openingBalanceTot != 0) {
                trialBalanceTableModel.addRow(new Object[]{"<html><b>Opening Stock</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(openingBalanceTot) + "</b></html>"});
                //Detailed View of Op Bal.
                if (detailViewCheckBox.isSelected()) {
                    for (StockGroup stockGroup : listOfStockGroup) {
                        List<Object[]> itemStockGroupList = itemDAO.findOpeningBalanceByItemStockGroup(stockGroup);
                        boolean isGroupNameFirst = true;
                        for (Object[] objects : itemStockGroupList) {
                            if (isGroupNameFirst && Double.parseDouble(String.valueOf(objects[1])) != 0) {
                                trialBalanceTableModel.addRow(new Object[]{"<html>&nbsp;&nbsp;&nbsp;&nbsp;<u><b>" + stockGroup.getName() + "</b></u></html>"});
                                isGroupNameFirst = false;
                            }
                            if (Double.parseDouble(String.valueOf(objects[1])) != 0) {
                                trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + objects[0] + "\t\t" + commonService.formatIntoCurrencyAsString(Double.parseDouble(String.valueOf(objects[1]))) + "</i></html>"});
                            }
                        }
                    }
                }
            }
            
            switch (screen) {
                case 1:
                    //detailViewCheckBox.setVisible(true);
                    //totalDebitAmt = 0.00;
                    //totalCreditAmt = 0.00;
                    for (LedgerGroup ledgerGroup : listOfLedgerGroup) {
                        listOfLedger = ledgerDAO.findAllByLedgerGroup(ledgerGroup);
                        double ledgerGroupTotal = 0.00;
                        for (Ledger ledger : listOfLedger) {
                            ledgerGroupTotal += ledger.getAvailableBalance();
                        }
                        if (ledgerGroupTotal > 0 || ledgerGroupTotal < 0) {
                            if (ledgerGroup.getAccountType().equalsIgnoreCase("Asset")
                                    || ledgerGroup.getAccountType().equalsIgnoreCase("Expense")) {
                                //Debit Balance - LedgerGroup
                                if (ledgerGroupTotal > 0) {
                                    trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                        + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>", ""});
                                    totalDebitAmt += ledgerGroupTotal;
                                } else if (ledgerGroupTotal < 0) {
                                    trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                        + "</b></html>", "", "<html><b>" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerGroupTotal)) + "</b></html>"});
                                    totalCreditAmt += Math.abs(ledgerGroupTotal);
                                }
                                //Detailed View of Ledger
                                if (detailViewCheckBox.isSelected()) {
                                    for (Ledger ledger : listOfLedger) {
                                        if (ledger.getAvailableBalance() > 0) {
                                            trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                                + "</i></html>", "<html><i>" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>", ""});
                                        } else if (ledger.getAvailableBalance() < 0) {
                                            trialBalanceTableModel.addRow(new Object[]{"<html><i>" + ledger.getLedgerName()
                                                + "</i></html>", "", "<html><i>" + commonService.formatIntoCurrencyAsString(Math.abs(ledger.getAvailableBalance())) + "</i></html>"});
                                        }
                                    }
                                }

                            } else {
                                //Debit Balance - LedgerGroup
                                if (ledgerGroupTotal > 0) {
                                    trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                        + "</b></html>", "", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                    totalCreditAmt += ledgerGroupTotal;
                                } else if (ledgerGroupTotal < 0) {
                                    trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                        + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerGroupTotal)) + "</b></html>", ""});
                                    totalDebitAmt += Math.abs(ledgerGroupTotal);
                                }
                                //Detailed View of Ledger
                                if (detailViewCheckBox.isSelected()) {
                                    for (Ledger ledger : listOfLedger) {
                                        if (ledger.getAvailableBalance() > 0) {
                                            trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                                + "</i></html>", "", "<html><i>" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                                        } else if (ledger.getAvailableBalance() < 0) {
                                            trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                                + "</i></html>", "<html><i>" + commonService.formatIntoCurrencyAsString(Math.abs(ledger.getAvailableBalance())) + "</i></html>", ""});
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //total amount calculation
                    double diffAmount = 0.00;
                    trialBalanceTableModel.addRow(new Object[]{null});
                    if (totalDebitAmt > totalCreditAmt) {
                        diffAmount = totalDebitAmt - totalCreditAmt;
                        trialBalanceTableModel.addRow(new Object[]{"<html><b>" + "Difference" + "</b></html>", "", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmount) + "</b></html>"});
                        trialBalanceTableModel.addRow(new Object[]{null});
                        trialBalanceTableModel.addRow(new Object[]{"<html><b>" + " Total " + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(totalDebitAmt) + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(totalDebitAmt) + "</b></html>"});
                    } else {
                        diffAmount = totalCreditAmt - totalDebitAmt;
                        trialBalanceTableModel.addRow(new Object[]{"<html><b>" + "Difference" + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmount) + "</b></html>", ""});
                        trialBalanceTableModel.addRow(new Object[]{null});
                        trialBalanceTableModel.addRow(new Object[]{"<html><b>" + " Total ", "<html><b>" + commonService.formatIntoCurrencyAsString(totalCreditAmt) + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(totalCreditAmt) + "</b></html>"});
                    }
                    break;
                case 2:
                    //detailViewCheckBox.setVisible(false);
                    if (null != ledgerGroup) {
                        listOfLedger = ledgerDAO.findAllByLedgerGroup(ledgerGroup);
                        for (Ledger ledger : listOfLedger) {
                            if (ledgerGroup.getAccountType().equalsIgnoreCase("Asset")
                                    || ledgerGroup.getAccountType().equalsIgnoreCase("Expense")) {
                                //Debit Balance
                                if (ledger.getAvailableBalance() > 0) {
                                    trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                        + "</i></html>", commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()), ""});
                                } else if (ledger.getAvailableBalance() < 0) {
                                    trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                        + "</i></html>", "", commonService.formatIntoCurrencyAsString(Math.abs(ledger.getAvailableBalance()))});
                                }
                            } else {
                                if (ledger.getAvailableBalance() > 0) {
                                    trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                        + "</i></html>", "", commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance())});
                                } else if (ledger.getAvailableBalance() < 0) {
                                    trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                        + "</i></html>", commonService.formatIntoCurrencyAsString(Math.abs(ledger.getAvailableBalance())), ""});
                                }
                            }
                        }
                    }
                    break;
            }
        } catch (Exception e) {
            log.error("fillTrialBalanceTable:", e);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headingPanel = new javax.swing.JPanel();
        centerHeadingLabel = new javax.swing.JLabel();
        printPanel = new javax.swing.JPanel();
        printButton = new javax.swing.JButton();
        pdfButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        detailViewCheckBox = new javax.swing.JCheckBox();
        bodyPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        trialBalanceTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        headingPanel.setBackground(new java.awt.Color(134, 0, 0));
        headingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        headingPanel.setPreferredSize(new java.awt.Dimension(1124, 50));

        centerHeadingLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        centerHeadingLabel.setForeground(new java.awt.Color(255, 255, 153));
        centerHeadingLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        centerHeadingLabel.setText("Trial Balance ");

        printPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        printPanel.setOpaque(false);
        printPanel.setLayout(new java.awt.GridLayout(1, 0));

        printButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/print.png"))); // NOI18N
        printButton.setText("Print");
        printButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printButtonActionPerformed(evt);
            }
        });
        printPanel.add(printButton);

        pdfButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/pdf-icon.png"))); // NOI18N
        pdfButton.setText("PDF");
        pdfButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdfButtonActionPerformed(evt);
            }
        });
        printPanel.add(pdfButton);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel1.setOpaque(false);

        detailViewCheckBox.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        detailViewCheckBox.setForeground(new java.awt.Color(255, 255, 153));
        detailViewCheckBox.setText("Detailed View");
        detailViewCheckBox.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        detailViewCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        detailViewCheckBox.setOpaque(false);
        detailViewCheckBox.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                detailViewCheckBoxStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(detailViewCheckBox, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(detailViewCheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout headingPanelLayout = new javax.swing.GroupLayout(headingPanel);
        headingPanel.setLayout(headingPanelLayout);
        headingPanelLayout.setHorizontalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(centerHeadingLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 760, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(printPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        headingPanelLayout.setVerticalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(printPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(centerHeadingLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(headingPanel, java.awt.BorderLayout.PAGE_START);

        bodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        bodyPanel.setOpaque(false);

        trialBalanceTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        trialBalanceTable.setOpaque(false);
        trialBalanceTable.setSelectionBackground(new java.awt.Color(153, 153, 153));
        trialBalanceTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                trialBalanceTableMouseClicked(evt);
            }
        });
        trialBalanceTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                trialBalanceTableKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(trialBalanceTable);

        javax.swing.GroupLayout bodyPanelLayout = new javax.swing.GroupLayout(bodyPanel);
        bodyPanel.setLayout(bodyPanelLayout);
        bodyPanelLayout.setHorizontalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1128, Short.MAX_VALUE)
        );
        bodyPanelLayout.setVerticalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 397, Short.MAX_VALUE)
        );

        add(bodyPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void trialBalanceTableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_trialBalanceTableKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE && screen > 1) {
            screen--;
            fillTrialBalanceTable();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            screen++;
            fillTrialBalanceTable();
        }
    }//GEN-LAST:event_trialBalanceTableKeyPressed

    private void trialBalanceTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_trialBalanceTableMouseClicked
        try {
            if (evt.getClickCount() == 2) {
                index = trialBalanceTable.rowAtPoint(evt.getPoint());
                screen++;
                clickAction();

                //screen++;
                //fillStockSummaryTable();
            }
        } catch (Exception e) {
            log.error("stockSummaryTableMouseClicked", e);
        }
    }//GEN-LAST:event_trialBalanceTableMouseClicked

    private void detailViewCheckBoxStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_detailViewCheckBoxStateChanged
        fillTrialBalanceTable();
    }//GEN-LAST:event_detailViewCheckBoxStateChanged

    private void printButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printButtonActionPerformed
         try {
           TrialBalanceReport trialBalanceReport = new TrialBalanceReport();
             if (MsgBox.confirm("Are you sure you want to Print?")) {
                    if (trialBalanceReport.createTrialBalanceReport(trialBalanceTable, "TrialBalance", "print")) {
                        
                    } else {
                        MsgBox.warning("Something went wrong, please re-click the button again!");
                    }
                }
        } catch (Exception ex) {
            log.error("printButtonActionPerformed:",ex);
        }
    }//GEN-LAST:event_printButtonActionPerformed

    private void pdfButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdfButtonActionPerformed
         try {
           TrialBalanceReport trialBalanceReport = new TrialBalanceReport();
             if (MsgBox.confirm("Are you sure you want to create pdf?")) {
                    if (trialBalanceReport.createTrialBalanceReport(trialBalanceTable, "TrialBalance", "pdf")) {
                        
                    } else {
                        MsgBox.warning("Something went wrong, please re-click the button again!");
                    }
                }
        } catch (Exception ex) {
            log.error("pdfButtonActionPerformed:",ex);
        }
    }//GEN-LAST:event_pdfButtonActionPerformed

    void clickAction() {
        try {
            switch (screen) {
                case 2:
                    ledgerGroup = listOfLedgerGroup.get(index);
                    fillTrialBalanceTable();
                    break;
                case 3:
                    // item = listOfItem.get(index);
                    //fillStockSummaryTable();
                    break;
                case 4:
                    // month = selectedFirstCol.trim();
                    if (index > 0) {
//                        month = months[index - 1];
                        // fillStockSummaryTable();
                    } else {
                        screen--;
                    }
                    break;
                case 5:
                    ArrayList row = CommonService.getTableRowData(trialBalanceTable);
                    if (row.size() > 0) {
                     //   invoiceDate = (String) row.get(0);
                        //  invoiceType = (String) row.get(2);
                        //  invoiceNumber = (String) row.get(3);
                        //fillStockSummaryTable();
                    }

                    break;

            }
            //screen++;

        } catch (Exception e) {
            log.error("clickAction:", e);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bodyPanel;
    private javax.swing.JLabel centerHeadingLabel;
    private javax.swing.JCheckBox detailViewCheckBox;
    private javax.swing.JPanel headingPanel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton pdfButton;
    private javax.swing.JButton printButton;
    private javax.swing.JPanel printPanel;
    private javax.swing.JTable trialBalanceTable;
    // End of variables declaration//GEN-END:variables

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    //HibernateUtil hibernateUtil=new HibernateUtil();

                    CompanyDAO companyDAO = new CompanyDAO();
                    GlobalProperty.setCompany(companyDAO.getDefaultCompany());
                    MainForm jMainFrame = new MainForm();
                    jMainFrame.setVisible(true);
                    jMainFrame.setExtendedState(MainForm.MAXIMIZED_BOTH);

                    TrialBalanceView trialBalanceView = new TrialBalanceView();
                    jMainFrame.getContentPanel().removeAll();
                    jMainFrame.getContentPanel().repaint();
                    jMainFrame.getContentPanel().revalidate();
                    jMainFrame.getContentPanel().add(trialBalanceView);

                } catch (Exception e) {
                    log.error("MainForm run :", e);
                }
            }
        });
    }
}
