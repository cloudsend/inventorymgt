package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.MultiCurrencyDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.services.MultiCurrencyService;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class MultiCurrencyView extends javax.swing.JInternalFrame {

    /**
     * Creates new form AddItem
     */
    static final Logger log = Logger.getLogger(MultiCurrencyView.class.getName());
    CRUDServices cRUDServices = new CRUDServices();
    ;
    MultiCurrencyDAO multiCurrencyDAO = new MultiCurrencyDAO();
    MultiCurrencyService multiCurrencyService = new MultiCurrencyService();
    CommonService commonService = new CommonService();

    public MultiCurrencyView() {
        initComponents();
        this.getRootPane().setDefaultButton(saveButton);
        KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);

        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);

        currencyListTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{
                    "Currency Name", "Symbol", "Today's Rate"
                }
        ));
        currencyListTable.setRowHeight(GlobalProperty.tableRowHieght);

        multiCurrencyService.fillCurrencyListTable(currencyListTable);

        if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
            saveButton.setEnabled(false);
            newButton.setEnabled(false);
            updateButton.setEnabled(false);
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        currencyListTable = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        currencyNameText = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        todayRateText = new javax.swing.JFormattedTextField();
        symbolText = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        buttonPanel = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        updateButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();

        setClosable(true);
        setTitle("Multi Currency");
        setPreferredSize(new java.awt.Dimension(566, 653));
        getContentPane().setLayout(new java.awt.CardLayout());

        mainPanel.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("List Of Currencies"));
        jPanel2.setOpaque(false);

        currencyListTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        currencyListTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                currencyListTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(currencyListTable);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 509, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel4.setOpaque(false);

        jLabel1.setText("Currency Name");

        jLabel2.setText("Today's Rate");

        todayRateText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        todayRateText.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        todayRateText.setText("0.00");
        todayRateText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                todayRateTextFocusGained(evt);
            }
        });
        todayRateText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                todayRateTextKeyTyped(evt);
            }
        });

        jLabel3.setText("Unicode/Symbol :");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(22, 22, 22)))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(todayRateText, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(symbolText, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(currencyNameText))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(currencyNameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(symbolText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(todayRateText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        buttonPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new java.awt.GridLayout(1, 0));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("New");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(newButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(saveButton);

        updateButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/update.png"))); // NOI18N
        updateButton.setText("Update");
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(updateButton);

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/delete-icon.png"))); // NOI18N
        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(deleteButton);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(mainPanel, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        try {
            String currencyName = currencyNameText.getText().trim();
            //Tax currency = multiCurrencyDAO.findByName(currencyName);
            MultiCurrency multiCurrency = multiCurrencyDAO.findByName(currencyName);
            if (null == multiCurrency) {
                if (CommonService.stringValidator(new String[]{currencyName, todayRateText.getText().trim(), symbolText.getText().trim()})) {
                    if (MsgBox.confirm("Are you sure you want to save?")) {
                        multiCurrency = new MultiCurrency();
                        multiCurrency.setName(currencyName);
                        if (todayRateText.getText().trim().length() > 0) {
                            multiCurrency.setRate(Double.parseDouble(todayRateText.getText().trim()));
                        }
                        multiCurrency.setSymbol(StringEscapeUtils.unescapeJava("\\" + symbolText.getText()));
                        //multiCurrency.setCompany(GlobalProperty.getCompany());
                        cRUDServices.saveModel(multiCurrency);
                        MsgBox.success("Successfully Added " + currencyName);
                        CommonService.clearTextFields(new JTextField[]{currencyNameText, todayRateText, symbolText});
                        CommonService.clearCurrencyFields(new JTextField[]{todayRateText});
                        multiCurrencyService.fillCurrencyListTable(currencyListTable);
                        currencyNameText.requestFocusInWindow();
                    }
                } else {
                    MsgBox.warning("Currency Name and Today's Rate are mandatory.");
                    currencyNameText.requestFocusInWindow();
                }
            } else {
                MsgBox.warning("Currency Name '" + currencyName + "' is already exists.");
                currencyNameText.requestFocusInWindow();
            }

        } catch (Exception e) {
            log.error("addButtonActionPerformed:", e);
        }

    }//GEN-LAST:event_saveButtonActionPerformed

    private void currencyListTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_currencyListTableMouseClicked
        try {

            ArrayList selectedRow = commonService.getTableRowData(currencyListTable);
            MultiCurrency currency = multiCurrencyDAO.findByName(selectedRow.get(0) + "");
            currencyNameText.setText(currency.getName());
            todayRateText.setText(currency.getRate() + "");
            symbolText.setText(currency.getSymbol());
        } catch (Exception e) {
            log.error("currencyListTableMouseClicked", e);
        }
    }//GEN-LAST:event_currencyListTableMouseClicked

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
        try {
            String currencyName = currencyNameText.getText().trim();
            MultiCurrency multiCurrency = multiCurrencyDAO.findByName(currencyName);
            if (null != multiCurrency) {
                if (CommonService.stringValidator(new String[]{currencyName, todayRateText.getText().trim(), symbolText.getText().trim()})) {
                    if (MsgBox.confirm("Are you sure you want to update?")) {
                        multiCurrency.setName(currencyName);
                        if (todayRateText.getText().trim().length() > 0) {
                            multiCurrency.setRate(Double.parseDouble(todayRateText.getText().trim()));
                        }
                        multiCurrency.setSymbol(StringEscapeUtils.unescapeJava("\\" + symbolText.getText()));
                        //multiCurrency.setCompany(GlobalProperty.getCompany());
                        cRUDServices.saveOrUpdateModel(multiCurrency);
                        MsgBox.success("Successfully Updated " + currencyName);
                        CommonService.clearTextFields(new JTextField[]{currencyNameText, todayRateText, symbolText});
                        CommonService.clearCurrencyFields(new JTextField[]{todayRateText});
                        multiCurrencyService.fillCurrencyListTable(currencyListTable);
                        currencyNameText.requestFocusInWindow();
                    }
                } else {
                    MsgBox.warning("Currency Name and Currency Rate are mandatory.");
                    currencyNameText.requestFocusInWindow();
                }
            } else {
                MsgBox.warning("Currency Name '" + currencyName + "' Not Found.");
                currencyNameText.requestFocusInWindow();
            }
        } catch (Exception e) {
            log.error("updateButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_updateButtonActionPerformed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        CommonService.clearTextFields(new JTextField[]{currencyNameText, todayRateText});
        todayRateText.setText("0.00");
    }//GEN-LAST:event_newButtonActionPerformed

    private void todayRateTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_todayRateTextFocusGained
        todayRateText.selectAll();
    }//GEN-LAST:event_todayRateTextFocusGained

    private void todayRateTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_todayRateTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_todayRateTextKeyTyped

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        Session session = null;
        try {
            String currencyName = currencyNameText.getText().trim();
            MultiCurrency multiCurrency = multiCurrencyDAO.findByName(currencyName);
            if (null != multiCurrency) {
                    if (MsgBox.confirm("Are you sure you want to delete?")) {
                        try {
                            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
                            session = sessionFactory.openSession();
                            Transaction transaction = session.beginTransaction();
                            session.delete(multiCurrency);
                            transaction.commit();
                        MsgBox.success("Successfully Deleted " + currencyName);
                        CommonService.clearTextFields(new JTextField[]{currencyNameText, todayRateText, symbolText});
                        CommonService.clearCurrencyFields(new JTextField[]{todayRateText});
                        multiCurrencyService.fillCurrencyListTable(currencyListTable);
                        currencyNameText.requestFocusInWindow();
                        }catch (Exception e) {
                            MsgBox.warning("Tax '" + currencyName + "' is already used for transactions!");
                            currencyNameText.requestFocusInWindow();
                        } finally {
                            if (session != null) {
                                session.clear();
                                session.close();
                            }
                        }
                    }
            } else {
                MsgBox.warning("Currency Name '" + currencyName + "' Not Found.");
                currencyNameText.requestFocusInWindow();
            }
        } catch (Exception e) {
            log.error("deleteButtonActionPerformed:", e);
        }        
    }//GEN-LAST:event_deleteButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JTable currencyListTable;
    private javax.swing.JTextField currencyNameText;
    private javax.swing.JButton deleteButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton newButton;
    private javax.swing.JButton saveButton;
    private javax.swing.JTextField symbolText;
    private javax.swing.JFormattedTextField todayRateText;
    private javax.swing.JButton updateButton;
    // End of variables declaration//GEN-END:variables
}
