package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.LedgerGroupDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.dao.UnitDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.ItemService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.services.TaxService;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.model.Unit;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import static com.cloudsendsoft.inventory.view.ShipmentModeView.log;
import java.awt.TextField;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class TaxView extends javax.swing.JInternalFrame {

    /**
     * Creates new form AddItem
     */
    static final Logger log = Logger.getLogger(TaxView.class.getName());
    CRUDServices cRUDServices = new CRUDServices();
    Tax tax = null;
    TaxDAO taxDAO = new TaxDAO();
    TaxService taxService = new TaxService();
    CommonService commonService = new CommonService();

    LedgerGroupDAO ledgerGroupDAO = new LedgerGroupDAO();
    LedgerGroup ledgerGroup = null;
    Ledger ledger = null;
    LedgerDAO ledgerDAO = new LedgerDAO();

    public TaxView() {
        initComponents();

        if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
            mainPanel.remove(buttonPanel);
            mainPanel.repaint();
            mainPanel.revalidate();
        }
        this.getRootPane().setDefaultButton(addButton);

        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);

        taxListTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{
                    "Tax Name", "Tax Rate"
                }
        ));
        taxListTable.setRowHeight(25);

        ledgerGroup = ledgerGroupDAO.findByGroupName("Duties & Taxes");

        taxService.fillTaxListTable(taxListTable);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taxListTable = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        taxNameText = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        taxRateText = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        buttonPanel = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();
        updateButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();

        setClosable(true);
        setTitle("Tax");
        getContentPane().setLayout(new java.awt.CardLayout());

        mainPanel.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("List Of Taxes"));
        jPanel2.setOpaque(false);

        taxListTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        taxListTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                taxListTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(taxListTable);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 518, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel4.setOpaque(false);

        jLabel1.setText("Tax Name");

        jLabel2.setText("Tax Rate");

        taxRateText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        taxRateText.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        taxRateText.setText("0.00");
        taxRateText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                taxRateTextFocusGained(evt);
            }
        });
        taxRateText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                taxRateTextKeyTyped(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("%");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(22, 22, 22)))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(taxNameText)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(taxRateText)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addGap(155, 155, 155)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(taxNameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(taxRateText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        buttonPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new java.awt.GridLayout(1, 0));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("   New");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(newButton);

        addButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        addButton.setText("    Save");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(addButton);

        updateButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/update.png"))); // NOI18N
        updateButton.setText("   Update");
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(updateButton);

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/delete-icon.png"))); // NOI18N
        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(deleteButton);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(mainPanel, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        try {
            String taxName = taxNameText.getText().trim();
            tax = taxDAO.findByName(taxName);
            ledger = ledgerDAO.findByLedgerName(taxName);
            if (null == tax && null == ledger) {
                if (CommonService.stringValidator(new String[]{taxName, taxRateText.getText().trim()})) {
                    if (MsgBox.confirm("Are you sure you want to save?")) {
                        tax = new Tax();
                        tax.setName(taxName);
                        if (taxRateText.getText().trim().length() > 0) {
                            tax.setTaxRate(Double.parseDouble(taxRateText.getText().trim()));
                        }
                        tax.setCompany(GlobalProperty.getCompany());
                        cRUDServices.saveModel(tax);

                        //adding tax to ledger under Duties and Taxes
                        ledger = new Ledger();
                        ledger.setAvailableBalance(0.00);
                        ledger.setLedgerName(taxName);
                        ledger.setLedgerGroup(ledgerGroup);
                        ledger.setCompany(GlobalProperty.getCompany());
                        cRUDServices.saveModel(ledger);

                        MsgBox.success("Successfully Added " + taxName);
                        CommonService.clearTextFields(new JTextField[]{taxNameText, taxRateText});
                        taxRateText.setText("0.00");
                        taxService.fillTaxListTable(taxListTable);
                        taxNameText.requestFocusInWindow();
                    }
                } else {
                    MsgBox.warning("Tax Name and Tax Rate are mandatory.");
                    taxNameText.requestFocusInWindow();
                }
            } else {
                if (null != tax) {
                    MsgBox.warning("Tax Name '" + taxName + "' is already exists.");
                } else {
                    MsgBox.warning("Ledger Name '" + taxName + "' is already exists.");
                }
                taxNameText.requestFocusInWindow();
            }

        } catch (Exception e) {
            log.error("addItemButtonActionPerformed:", e);
        }

    }//GEN-LAST:event_addButtonActionPerformed

    private void taxListTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_taxListTableMouseClicked
        try {

            ArrayList selectedRow = commonService.getTableRowData(taxListTable);
            tax = taxDAO.findByName(selectedRow.get(0) + "");
            taxNameText.setText(tax.getName());
            taxRateText.setText(tax.getTaxRate() + "");
            ledger = ledgerDAO.findByLedgerName(selectedRow.get(0) + "");
        } catch (Exception e) {
            log.error("itemTableMouseClicked", e);
        }
    }//GEN-LAST:event_taxListTableMouseClicked

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
        try {
            String taxName = taxNameText.getText().trim();
            if (null != tax && null != ledger) {
                if (CommonService.stringValidator(new String[]{taxName, taxRateText.getText().trim()})) {
                    if (MsgBox.confirm("Are you sure you want to update?")) {
                        tax.setName(taxName);
                        if (taxRateText.getText().trim().length() > 0) {
                            tax.setTaxRate(Double.parseDouble(taxRateText.getText().trim()));
                        }

                        cRUDServices.saveOrUpdateModel(tax);

                        ledger.setLedgerName(taxName);
                        cRUDServices.saveOrUpdateModel(ledger);

                        MsgBox.success("Successfully Updated " + taxName);
                        CommonService.clearTextFields(new JTextField[]{taxNameText, taxRateText});
                        taxRateText.setText("0.00");
                        taxService.fillTaxListTable(taxListTable);
                        taxNameText.requestFocusInWindow();
                    }
                } else {
                    MsgBox.warning("Tax Name and Tax Rate are mandatory.");
                    taxNameText.requestFocusInWindow();
                }
            } else {
                if (null == tax) {
                    MsgBox.warning("Tax Name '" + taxName + "' Not Found.");
                } else {
                    MsgBox.warning("Ledger Name '" + taxName + "' Not Found.");
                }
                taxNameText.requestFocusInWindow();
            }

        } catch (Exception e) {
            log.error("updateButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_updateButtonActionPerformed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        CommonService.clearTextFields(new JTextField[]{taxNameText, taxRateText});
        taxRateText.setText("0.00");
        ledger = null;
        tax = null;
    }//GEN-LAST:event_newButtonActionPerformed

    private void taxRateTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_taxRateTextFocusGained
        taxRateText.selectAll();
    }//GEN-LAST:event_taxRateTextFocusGained

    private void taxRateTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_taxRateTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_taxRateTextKeyTyped

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        Session session = null;
        try {
            String taxName = taxNameText.getText().trim();
            if (null != tax && null != ledger) {
                if (!tax.getName().equalsIgnoreCase("N/A")) {
                    if (MsgBox.confirm("Are you sure you want to delete?")) {
                        try {
                            if (ledger.getAvailableBalance() == 0) {

                                SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
                                session = sessionFactory.openSession();
                                Transaction transaction = session.beginTransaction();
                                session.delete(tax);
                                transaction.commit();
                                session.clear();
                                session.close();

                                sessionFactory = HibernateUtil.getSessionFactory();
                                session = sessionFactory.openSession();
                                transaction = session.beginTransaction();
                                session.delete(ledger);
                                transaction.commit();

                                MsgBox.success("Successfully Deleted " + taxName);
                                CommonService.clearTextFields(new JTextField[]{taxNameText, taxRateText});
                                taxRateText.setText("0.00");
                                taxService.fillTaxListTable(taxListTable);
                                taxNameText.requestFocusInWindow();
                            } else {
                                MsgBox.abort("Tax '" + taxName + "' transactions already entered in ledger");
                            }
                        } catch (Exception e) {
                            MsgBox.warning("Tax '" + taxName + "' is already used for transactions!");
                            taxNameText.requestFocusInWindow();
                        } finally {
                            if (session != null) {
                                session.clear();
                                session.close();
                            }
                        }
                    }
                }
            } else {
                if (null == tax) {
                    MsgBox.warning("Tax Name '" + taxName + "' Not Found.");
                } else {
                    MsgBox.warning("Ledger Name '" + taxName + "' Not Found.");
                }
                taxNameText.requestFocusInWindow();
            }

        } catch (Exception e) {
            log.error("deleteButtonActionPerformed:", e);
        }

    }//GEN-LAST:event_deleteButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton deleteButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton newButton;
    private javax.swing.JTable taxListTable;
    private javax.swing.JTextField taxNameText;
    private javax.swing.JFormattedTextField taxRateText;
    private javax.swing.JButton updateButton;
    // End of variables declaration//GEN-END:variables
}
