package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.GodownDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.services.ChargesService;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.GodownService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Godown;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.awt.Color;
import java.awt.color.ColorSpace;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JColorChooser;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class GodownView extends javax.swing.JInternalFrame {

    /**
     * Creates new form AddItem
     */
    static final Logger log = Logger.getLogger(GodownView.class.getName());
    CRUDServices cRUDServices = new CRUDServices();
    ;
    GodownDAO godownDAO = new GodownDAO();
    GodownService godownService = new GodownService();
    CommonService commonService = new CommonService();
    List<Item> itemList = null;
    ItemDAO itemDAO = new ItemDAO();
    Color color = new Color(255, 255, 255);

    void setButtonColor() {
        colorButton.setContentAreaFilled(false);
        colorButton.setOpaque(true);
        colorButton.setBackground(color);
        colorButton.setBorder(new javax.swing.border.MatteBorder(null));
    }

    public GodownView() {
        initComponents();

        if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
            mainPanel.remove(buttonPanel);
            mainPanel.repaint();
            mainPanel.revalidate();
        } else {
            setButtonColor();
        }
        this.getRootPane().setDefaultButton(addButton);
        KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);

        godownListTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{
                    "Godown", ""
                }
        ));
        godownListTable.setRowHeight(25);

        godownService.fillGodownListTable(godownListTable);

        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        godownListTable = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        godownNameText = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        buttonPanel = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();
        updateButton = new javax.swing.JButton();
        colorButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();

        setClosable(true);
        setTitle("Godown");

        mainPanel.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("List Of Godown"));
        jPanel2.setOpaque(false);

        godownListTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        godownListTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                godownListTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(godownListTable);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 544, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 476, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel4.setOpaque(false);

        jLabel1.setText("Godown Name");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(godownNameText)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(godownNameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buttonPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new java.awt.GridLayout(1, 1));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("New");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(newButton);

        addButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        addButton.setText("Save");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(addButton);

        updateButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/update.png"))); // NOI18N
        updateButton.setText("Update");
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(updateButton);

        colorButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/chooseColor.png"))); // NOI18N
        colorButton.setText("Choose Color");
        colorButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        colorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(colorButton);

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/delete-icon.png"))); // NOI18N
        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(deleteButton);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        try {
            String godownName = godownNameText.getText().trim();
            Godown godown = godownDAO.findByName(godownName);
            if (null == godown) {
                if (CommonService.stringValidator(new String[]{godownName})) {
                    if (MsgBox.confirm("Are you sure you want to save?")) {
                        godown = new Godown();
                        godown.setName(godownName);
                        godown.setCompany(GlobalProperty.getCompany());
                        godown.setColor(String.valueOf(color.getRGB()));
                        godown.setCompany(GlobalProperty.getCompany());
                        cRUDServices.saveModel(godown);

                        //Add item duplication in item Table
                        itemList = itemDAO.findDistictItem();
                        for (Item item : itemList) {
                            Item itemNew = new Item();
                            itemNew.setItemCode(item.getItemCode());
                            itemNew.setDescription(item.getDescription());
                            itemNew.setAvailableQty(0.00);
                            itemNew.setOpeningBal(0.00);
                            itemNew.setOpeningUnitPrice(0.00);
                            itemNew.setOpeningQty(0.00);
                            itemNew.setSellingPrice(item.getSellingPrice());
                            itemNew.setWeight(item.getWeight());
                            itemNew.setVisible(false);
                            itemNew.setCompany(GlobalProperty.company);
                            itemNew.setStockGroup(item.getStockGroup());
                            itemNew.setUnit(item.getUnit());
                            itemNew.setGodown(godown);
                            itemNew.setCompany(GlobalProperty.getCompany());
                            cRUDServices.saveModel(itemNew);

                        }
                        MsgBox.success("Successfully Added " + godownName);
                        CommonService.clearTextFields(new JTextField[]{godownNameText});
                        godownService.fillGodownListTable(godownListTable);
                        godownNameText.requestFocusInWindow();
                    }
                } else {
                    MsgBox.warning("Godown Name is mandatory.");
                    godownNameText.requestFocusInWindow();
                }
            } else {
                MsgBox.warning("Godown Name '" + godownName + "' is already exists.");
                godownNameText.requestFocusInWindow();
            }

        } catch (Exception e) {
            log.error("addButtonActionPerformed:", e);
        }

    }//GEN-LAST:event_addButtonActionPerformed

    private void godownListTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_godownListTableMouseClicked
        try {

            ArrayList selectedRow = commonService.getTableRowData(godownListTable);
            Godown godown = godownDAO.findByName(selectedRow.get(0) + "");
            godownNameText.setText(godown.getName());

            color = new Color(Integer.parseInt(godown.getColor()));
            if (color == null) {
                color = new Color(255, 255, 255);
            }
            colorButton.setBackground(color);

        } catch (Exception e) {
            log.error("godownListTableMouseClicked", e);
        }
    }//GEN-LAST:event_godownListTableMouseClicked

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
        try {
            String godownName = godownNameText.getText().trim();
            Godown godown = godownDAO.findByName(godownName);
            if (null != godown) {
                if (CommonService.stringValidator(new String[]{godownName})) {
                    if (MsgBox.confirm("Are you sure you want to update?")) {
                        godown.setName(godownName);
                        godown.setColor(String.valueOf(color.getRGB()));
                        cRUDServices.saveOrUpdateModel(godown);
                        MsgBox.success("Successfully Updated " + godownName);
                        CommonService.clearTextFields(new JTextField[]{godownNameText});
                        godownService.fillGodownListTable(godownListTable);
                        godownNameText.requestFocusInWindow();
                    }
                } else {
                    MsgBox.warning("Godown Name is mandatory.");
                    godownNameText.requestFocusInWindow();
                }
            } else {
                MsgBox.warning("Godown Name '" + godownName + "' Not Found.");
                godownNameText.requestFocusInWindow();
            }

        } catch (Exception e) {
            log.error("updateButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_updateButtonActionPerformed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        CommonService.clearTextFields(new JTextField[]{godownNameText});
        color = new Color(255, 255, 255);
    }//GEN-LAST:event_newButtonActionPerformed

    private void colorButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorButtonActionPerformed
        try {
            color = JColorChooser.showDialog(this, "Godown Color", Color.WHITE);
            if (color == null) {
                color = new Color(255, 255, 255);
            }
            colorButton.setBackground(color);
        } catch (Exception e) {
            log.error("colorButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_colorButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        Session session = null;
        try {
            String godownName = godownNameText.getText().trim();
            Godown godown = godownDAO.findByName(godownName);
            if (null != godown) {
                if (MsgBox.confirm("Are you sure you want to delete?")) {
                    try {
                        session = HibernateUtil.getSessionFactory().openSession();
                        org.hibernate.Query query = session.createQuery("delete from Item i where i.godown=:godown and i.company = :comp");
                        query.setParameter("comp", GlobalProperty.getCompany());
                        query.setParameter("godown", godown);
                        query.executeUpdate();
                        session.clear();
                        session.close();

                        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
                        session = sessionFactory.openSession();
                        Transaction transaction = session.beginTransaction();
                        session.delete(godown);
                        transaction.commit();

                        MsgBox.success("Successfully deleted " + godownName);
                        CommonService.clearTextFields(new JTextField[]{godownNameText});
                        godownService.fillGodownListTable(godownListTable);
                        godownNameText.requestFocusInWindow();
                    } catch (Exception e) {
                        e.printStackTrace();
                        MsgBox.warning("Godown '" + godownName + "' is already used for transactions!");
                        godownNameText.requestFocusInWindow();
                    } finally {
                        if (session != null) {
                            session.clear();
                            session.close();
                        }
                    }
                }
            } else {
                MsgBox.warning("Godown Name '" + godownName + "' Not Found.");
                godownNameText.requestFocusInWindow();
            }
        } catch (Exception e) {
            log.error("deleteButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_deleteButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton colorButton;
    private javax.swing.JButton deleteButton;
    private javax.swing.JTable godownListTable;
    private javax.swing.JTextField godownNameText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton newButton;
    private javax.swing.JButton updateButton;
    // End of variables declaration//GEN-END:variables
}
