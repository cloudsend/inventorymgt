/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.CompanyDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.dao.PackingListDAO;
import com.cloudsendsoft.inventory.dao.PackingTypeDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.report.PackingListReport;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.PackingList;
import com.cloudsendsoft.inventory.model.PackingListItem;
import com.cloudsendsoft.inventory.model.PackingType;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaCharge;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesPreformaTax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class PackingListView extends javax.swing.JPanel {

    /**
     * Creates new form PackingListView
     */
    static final Logger log = Logger.getLogger(PackingListView.class.getName());
    SalesPreforma salesPreforma = null;
    SalesPreformaItem salesPreformaItem = null;
    PackingListItem packingListItem = null;
    PackingList packingList = null;
    DefaultTableModel salesPreformaItemTableModel = null;
    DefaultTableModel packingListItemTableModel = null;
    List<SalesPreformaItem> salesPreformaItemList = null;
    MultiCurrency currency = null;
    List<SalesPreformaTax> salesPreformaTaxList = null;
    List<SalesPreformaCharge> salesPreformaChargesList = null;
    List<PackingListItem> packingListItemList = new ArrayList<PackingListItem>(0);
    NumberProperty numberPropertyPackingListInvoice = null;
    NumberProperty numberPropertyPackingListNumberOfPack = null;
    NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
    CommonService commonService = new CommonService();
    PackingTypeDAO packingDAO = new PackingTypeDAO();
    PackingType packing = null;
    CRUDServices cRUDServices = new CRUDServices();
    boolean isPackingListTableClicked = false;
    boolean isPackingListNew = false;
    String customerDetails = "";

    double netTotal = 0.00;
    double totDiscount = 0.00;
    double grandTotal = 0.00;

    int selectedRowIndex = -1;

    long numOfPack;

    double totalNetWeight = 0;
    double totalGrossWeight = 0;

    String casesList = "";

    boolean isStoreKeeper = false;

    public PackingListView(SalesPreforma salesPreforma) {
        initComponents();

        try {
            commonService.setCurrentPeriodOnCalendar(pInvoiceDate);

            if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
                saveButton.setEnabled(false);
            }
            if (GlobalProperty.listOfPrivileges.contains("StoreKeeper")) {
                isStoreKeeper = true;
            }

            //table background color removed
            jScrollPane1.setOpaque(false);
            jScrollPane1.getViewport().setOpaque(false);
            jScrollPane2.setOpaque(false);
            jScrollPane2.getViewport().setOpaque(false);

            packingListItemTable.setRowHeight(GlobalProperty.tableRowHieght);
            packingListItemTable.setFont(GlobalProperty.getFont());
            salePreformaItemTable.setRowHeight(GlobalProperty.tableRowHieght);
            salePreformaItemTable.setFont(GlobalProperty.getFont());

            this.salesPreforma = salesPreforma;

            //sales prefoma table design
            salePreformaItemTable.setRowHeight(GlobalProperty.tableRowHieght);

            if (!isStoreKeeper) {
                salePreformaItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                        null,
                        new String[]{"Sr.No.", "Item Code", "Description", "Unit Price", "Qty.", "Discount", "Total Amount"}
                ));
                salesPreformaItemTableModel = (DefaultTableModel) salePreformaItemTable.getModel();
                CommonService.setWidthAsPercentages(salePreformaItemTable, .03, .10, .20, .08, .08, .08, .08);
            } else {
                salePreformaItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                        null,
                        new String[]{"Sr.No.", "Item Code", "Description", "Qty."}
                ));
                salesPreformaItemTableModel = (DefaultTableModel) salePreformaItemTable.getModel();
                CommonService.setWidthAsPercentages(salePreformaItemTable, .03, .10, .08, .08);
            }

            salesPreformaItemList = salesPreforma.getSalesPreformaItems();
            salesPreformaTaxList = salesPreforma.getSalesPreformaTaxs();
            salesPreformaChargesList = salesPreforma.getSalesPreformaCharges();
            fillSalesPrefomaTable();

            //packing list table design
            packingListItemTable.setRowHeight(GlobalProperty.tableRowHieght);
            packingListItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                    null,
                    new String[]{"Sr.No", "DESCRIPTION OF GOODS", "NUMBER OF PACKS", "KIND OF PACKAGE", "CONTENTS OF PACKAGE", "NET WEIGHT(KG)", "GROSS WEIGHT(KG)"}
            ));
            packingListItemTableModel = (DefaultTableModel) packingListItemTable.getModel();
            CommonService.setWidthAsPercentages(packingListItemTable, .05, .30, .10, .10, .08, .08, .08);

            //fill all kind of packing
            for (PackingType packing : packingDAO.findAllByNameASC()) {
                pKindOfPackageCombo.addItem(new ComboKeyValue((packing.getName()), packing));
            }

            if (salesPreforma.getPackingList() != null) {
                packingList = salesPreforma.getPackingList();
                pInvoiceDate.setDate(packingList.getInvoiceDate());
                pInvoiceNumberText.setText(packingList.getInvoiceNumber());
                pNumberOfPackStartText.setText(packingList.getNumberOfPack() + "");
                packingListItemList = packingList.getPackingListItems();
                numOfPack = packingList.getNumberOfPack();
                fillPackingListTable();
            } else {
                packingList = new PackingList();
                pInvoiceDate.setDate(new java.util.Date());
                numberPropertyPackingListInvoice = numberPropertyDAO.findInvoiceNo("PackingListInvoice");
                pInvoiceNumberText.setText(numberPropertyPackingListInvoice.getCategoryPrefix() + numberPropertyPackingListInvoice.getNumber());

                isPackingListNew = true;

                numberPropertyPackingListNumberOfPack = numberPropertyDAO.findInvoiceNo("PackingListNumberOfPack");
                numOfPack = numberPropertyPackingListNumberOfPack.getNumber();
                pNumberOfPackStartText.setText(numberPropertyPackingListNumberOfPack.getNumber() + "");
                packingList.setInvoiceNumber(pInvoiceNumberText.getText());
                packingList.setNumberOfPack(numberPropertyPackingListNumberOfPack.getNumber());

            }
        } catch (Exception e) {
            log.error("PackingListView:", e);
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        leftHeadingLabel = new javax.swing.JLabel();
        bodyPanel = new javax.swing.JPanel();
        leftSidePanel = new javax.swing.JPanel();
        tablePanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        salePreformaItemTable = new javax.swing.JTable();
        maxMinPreformaButton = new javax.swing.JButton();
        salesPrefomaDetailPanel = new javax.swing.JPanel();
        billDateFormattedText = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        invoiceNumberText = new javax.swing.JTextField();
        customerText = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        deliveryText = new javax.swing.JTextField();
        paymentTermsText = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        deleveryTermsText = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        addressTextArea = new javax.swing.JTextArea();
        jLabel15 = new javax.swing.JLabel();
        telNoText = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        cancelButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();
        rightSidePanel = new javax.swing.JPanel();
        rightTablePanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        packingListItemTable = new javax.swing.JTable();
        maxMinPackingListButton = new javax.swing.JButton();
        packingDetailsPanel = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        pInvoiceNumberText = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        pItemDescriptionText = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        pNumberOfPackStartText = new javax.swing.JTextField();
        pKindOfPackageCombo = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        pUnitLabel = new javax.swing.JLabel();
        pContentOfPackageQtyText = new javax.swing.JFormattedTextField();
        pPackageWeightText = new javax.swing.JFormattedTextField();
        pNetWeightText = new javax.swing.JFormattedTextField();
        pGrossWeightText = new javax.swing.JFormattedTextField();
        pUnitLabel1 = new javax.swing.JLabel();
        pUnitLabel2 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        pItemWeightText = new javax.swing.JFormattedTextField();
        pItemAddButton = new javax.swing.JButton();
        pItemMinusButton1 = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        pInvoiceDate = new com.toedter.calendar.JDateChooser();
        buttonPanel = new javax.swing.JPanel();
        pdfButton = new javax.swing.JButton();
        printButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(102, 102, 102));
        setLayout(new java.awt.BorderLayout());

        jPanel3.setBackground(new java.awt.Color(51, 51, 51));
        jPanel3.setPreferredSize(new java.awt.Dimension(1124, 50));

        leftHeadingLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        leftHeadingLabel.setForeground(new java.awt.Color(255, 255, 153));
        leftHeadingLabel.setText("Packing List");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(leftHeadingLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE)
                .addGap(966, 966, 966))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addComponent(leftHeadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        add(jPanel3, java.awt.BorderLayout.PAGE_START);

        bodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        bodyPanel.setLayout(new java.awt.GridLayout(1, 0));

        leftSidePanel.setBackground(new java.awt.Color(102, 204, 255));
        leftSidePanel.setOpaque(false);

        tablePanel.setOpaque(false);

        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder("Sales Prefoma"));
        jScrollPane1.setOpaque(false);

        salePreformaItemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        salePreformaItemTable.setOpaque(false);
        salePreformaItemTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                salePreformaItemTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(salePreformaItemTable);

        javax.swing.GroupLayout tablePanelLayout = new javax.swing.GroupLayout(tablePanel);
        tablePanel.setLayout(tablePanelLayout);
        tablePanelLayout.setHorizontalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 562, Short.MAX_VALUE)
        );
        tablePanelLayout.setVerticalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
        );

        maxMinPreformaButton.setText(">>");
        maxMinPreformaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maxMinPreformaButtonActionPerformed(evt);
            }
        });

        salesPrefomaDetailPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Sales Prefoma Invoice"));
        salesPrefomaDetailPanel.setOpaque(false);

        billDateFormattedText.setEditable(false);
        billDateFormattedText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));

        jLabel6.setText("Bill Date*");

        jLabel5.setText("Invoice Number*");

        invoiceNumberText.setEditable(false);
        invoiceNumberText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                invoiceNumberTextActionPerformed(evt);
            }
        });
        invoiceNumberText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                invoiceNumberTextKeyPressed(evt);
            }
        });

        customerText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customerTextActionPerformed(evt);
            }
        });
        customerText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                customerTextKeyPressed(evt);
            }
        });

        jLabel13.setText("Customer*");

        jLabel14.setText("Delivery");

        deliveryText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deliveryTextActionPerformed(evt);
            }
        });
        deliveryText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                deliveryTextKeyPressed(evt);
            }
        });

        paymentTermsText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                paymentTermsTextKeyPressed(evt);
            }
        });

        jLabel12.setText("Payment Terms");

        jLabel11.setText("Delivery Terms");

        deleveryTermsText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleveryTermsTextActionPerformed(evt);
            }
        });
        deleveryTermsText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                deleveryTermsTextKeyPressed(evt);
            }
        });

        jLabel2.setText("Address :");

        addressTextArea.setColumns(20);
        addressTextArea.setLineWrap(true);
        addressTextArea.setRows(5);
        jScrollPane3.setViewportView(addressTextArea);

        jLabel15.setText("Tel No:");

        jPanel4.setOpaque(false);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/cancel.png"))); // NOI18N
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        backButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/back.png"))); // NOI18N
        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(backButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(backButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout salesPrefomaDetailPanelLayout = new javax.swing.GroupLayout(salesPrefomaDetailPanel);
        salesPrefomaDetailPanel.setLayout(salesPrefomaDetailPanelLayout);
        salesPrefomaDetailPanelLayout.setHorizontalGroup(
            salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(deliveryText, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(customerText, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(invoiceNumberText, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(billDateFormattedText, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(paymentTermsText))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(telNoText)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(deleveryTermsText, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        salesPrefomaDetailPanelLayout.setVerticalGroup(
            salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                .addComponent(deleveryTermsText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                        .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(billDateFormattedText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                                .addGap(98, 98, 98)
                                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                    .addComponent(jLabel12)
                                    .addComponent(paymentTermsText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(telNoText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(invoiceNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(customerText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                    .addComponent(jLabel14)
                                    .addComponent(deliveryText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout leftSidePanelLayout = new javax.swing.GroupLayout(leftSidePanel);
        leftSidePanel.setLayout(leftSidePanelLayout);
        leftSidePanelLayout.setHorizontalGroup(
            leftSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tablePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(maxMinPreformaButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(salesPrefomaDetailPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        leftSidePanelLayout.setVerticalGroup(
            leftSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(leftSidePanelLayout.createSequentialGroup()
                .addComponent(maxMinPreformaButton)
                .addGap(6, 6, 6)
                .addComponent(tablePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(salesPrefomaDetailPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        bodyPanel.add(leftSidePanel);

        rightSidePanel.setBackground(new java.awt.Color(255, 204, 204));
        rightSidePanel.setOpaque(false);

        rightTablePanel.setOpaque(false);

        jScrollPane2.setBorder(javax.swing.BorderFactory.createTitledBorder("Packing List"));
        jScrollPane2.setOpaque(false);

        packingListItemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        packingListItemTable.setOpaque(false);
        packingListItemTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                packingListItemTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(packingListItemTable);

        javax.swing.GroupLayout rightTablePanelLayout = new javax.swing.GroupLayout(rightTablePanel);
        rightTablePanel.setLayout(rightTablePanelLayout);
        rightTablePanelLayout.setHorizontalGroup(
            rightTablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );
        rightTablePanelLayout.setVerticalGroup(
            rightTablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rightTablePanelLayout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 243, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        maxMinPackingListButton.setText("<<");
        maxMinPackingListButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maxMinPackingListButtonActionPerformed(evt);
            }
        });

        packingDetailsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Packing Details"));
        packingDetailsPanel.setOpaque(false);

        jLabel7.setText("Invoice Number*");

        jLabel8.setText("Item Descreption");

        jLabel9.setText("No.Of Pack Start*");

        pNumberOfPackStartText.setEditable(false);

        pKindOfPackageCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                pKindOfPackageComboItemStateChanged(evt);
            }
        });

        jLabel10.setText("Kind Of Package");

        jLabel16.setText("Contents Of Package");

        jLabel17.setText("Net Weight");

        jLabel18.setText("Gross Weight");

        jLabel19.setText("Package Weight");

        pContentOfPackageQtyText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        pContentOfPackageQtyText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        pContentOfPackageQtyText.setText("0.00");
        pContentOfPackageQtyText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                pContentOfPackageQtyTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                pContentOfPackageQtyTextFocusLost(evt);
            }
        });
        pContentOfPackageQtyText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                pContentOfPackageQtyTextKeyTyped(evt);
            }
        });

        pPackageWeightText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        pPackageWeightText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        pPackageWeightText.setText("0.00");
        pPackageWeightText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                pPackageWeightTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                pPackageWeightTextFocusLost(evt);
            }
        });
        pPackageWeightText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                pPackageWeightTextKeyTyped(evt);
            }
        });

        pNetWeightText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        pNetWeightText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        pNetWeightText.setText("0.00");
        pNetWeightText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                pNetWeightTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                pNetWeightTextFocusLost(evt);
            }
        });
        pNetWeightText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                pNetWeightTextKeyTyped(evt);
            }
        });

        pGrossWeightText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        pGrossWeightText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        pGrossWeightText.setText("0.00");
        pGrossWeightText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                pGrossWeightTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                pGrossWeightTextFocusLost(evt);
            }
        });
        pGrossWeightText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                pGrossWeightTextKeyTyped(evt);
            }
        });

        pUnitLabel1.setText("KG");

        pUnitLabel2.setText("KG");

        jLabel20.setText("Item Weight");

        pItemWeightText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        pItemWeightText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        pItemWeightText.setText("0.00");
        pItemWeightText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                pItemWeightTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                pItemWeightTextFocusLost(evt);
            }
        });
        pItemWeightText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                pItemWeightTextKeyTyped(evt);
            }
        });

        pItemAddButton.setText("+");
        pItemAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pItemAddButtonActionPerformed(evt);
            }
        });

        pItemMinusButton1.setText("-");
        pItemMinusButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pItemMinusButton1ActionPerformed(evt);
            }
        });

        jLabel21.setText("Invoicel Date*");

        pInvoiceDate.setDateFormatString("dd/MM/yyyy");

        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new java.awt.GridLayout(0, 1, 0, 15));

        pdfButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/pdf-icon.png"))); // NOI18N
        pdfButton.setText(" PDF");
        pdfButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdfButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(pdfButton);

        printButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/print.png"))); // NOI18N
        printButton.setText("Print");
        printButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(printButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(saveButton);

        javax.swing.GroupLayout packingDetailsPanelLayout = new javax.swing.GroupLayout(packingDetailsPanel);
        packingDetailsPanel.setLayout(packingDetailsPanelLayout);
        packingDetailsPanelLayout.setHorizontalGroup(
            packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(pItemWeightText))
                    .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(8, 8, 8)
                        .addComponent(pItemDescriptionText))
                    .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(3, 3, 3)
                        .addComponent(pNumberOfPackStartText))
                    .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21))
                        .addGap(8, 8, 8)
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pInvoiceDate, javax.swing.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE)
                            .addComponent(pInvoiceNumberText))))
                .addGap(5, 5, 5)
                .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                        .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(pGrossWeightText, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, packingDetailsPanelLayout.createSequentialGroup()
                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(pNetWeightText))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, packingDetailsPanelLayout.createSequentialGroup()
                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(pPackageWeightText))
                    .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(2, 2, 2)
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pKindOfPackageCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pContentOfPackageQtyText))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pUnitLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pUnitLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pItemAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)
                            .addComponent(pItemMinusButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)
                            .addComponent(pUnitLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(10, 10, 10)
                        .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        packingDetailsPanelLayout.setVerticalGroup(
            packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pContentOfPackageQtyText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pUnitLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pInvoiceDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                                .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(pInvoiceNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(pKindOfPackageCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(10, 10, 10)
                                .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(pNumberOfPackStartText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(pPackageWeightText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                                .addComponent(pItemAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(pItemMinusButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pItemDescriptionText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(pNetWeightText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(pUnitLabel1)))
                        .addGap(10, 10, 10)
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pItemWeightText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(pGrossWeightText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(pUnitLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout rightSidePanelLayout = new javax.swing.GroupLayout(rightSidePanel);
        rightSidePanel.setLayout(rightSidePanelLayout);
        rightSidePanelLayout.setHorizontalGroup(
            rightSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rightSidePanelLayout.createSequentialGroup()
                .addGroup(rightSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(maxMinPackingListButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(rightTablePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(packingDetailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        rightSidePanelLayout.setVerticalGroup(
            rightSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rightSidePanelLayout.createSequentialGroup()
                .addComponent(maxMinPackingListButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rightTablePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(packingDetailsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        bodyPanel.add(rightSidePanel);

        add(bodyPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    void fillPackingListTable() {

        try {
            if (packingListItemList.size() > 0) {
                packingListItemTableModel.setRowCount(0);
                int slNo = 1;
                totalNetWeight = 0;
                totalGrossWeight = 0;
                long numOfPack = this.numOfPack;
                HashMap<String, Integer> packetCaseTypes = new HashMap<String, Integer>(0);
                for (PackingListItem packingListItem : packingListItemList) {
                    packingListItemTableModel.addRow(new Object[]{slNo++, packingListItem.getItem().getDescription(), numOfPack++, packingListItem.getKindOfPackage().getName(), packingListItem.getContentsOfPackageQty(), packingListItem.getNetWeight(), packingListItem.getGrossWeight()});
                    packingListItem.setNumberOfPack(numOfPack);
                    totalNetWeight += packingListItem.getNetWeight();
                    totalGrossWeight += packingListItem.getGrossWeight();
                    if (packetCaseTypes.get(packingListItem.getKindOfPackage().getName()) == null) {
                        packetCaseTypes.put(packingListItem.getKindOfPackage().getName(), 1);
                    } else {
                        packetCaseTypes.put(packingListItem.getKindOfPackage().getName(), packetCaseTypes.get(packingListItem.getKindOfPackage().getName()) + 1);
                    }

                }

                ////////////         packingListItemTableModel.addRow(new Object[]{null});
                packingListItemTableModel.addRow(new Object[]{"", "<html><b>\t\tTotal</b></html>", "", "", "", "<html><b>" + totalNetWeight + "</b></html>", "<html><b>" + totalGrossWeight + "</b></html>"});

                //total of each cases
/////////////            packingListItemTableModel.addRow(new Object[]{null});
                packingListItemTableModel.addRow(new Object[]{"", "<html><b>Total Number Of Packages : " + packingListItemList.size() + "</b></html>"});
                Iterator it = packetCaseTypes.entrySet().iterator();
                casesList = "";
                while (it.hasNext()) {
                    Map.Entry pairs = (Map.Entry) it.next();
                    packingListItemTableModel.addRow(new Object[]{"", "<html><b>            " + pairs.getKey() + " : " + pairs.getValue() + "</b></html>"});
                    casesList += pairs.getValue() + " " + pairs.getKey() + " + ";
                }
                if (casesList.length() > 3) {
                    casesList = casesList.substring(0, casesList.length() - 3);
                } else {
                    casesList = "";
                }

            } else {
                packingListItemTableModel.setRowCount(0);
            }
        } catch (Exception e) {
            log.error("fillPackingListTable:", e);
        }

    }

    void fillSalesPrefomaTable() {
        try {
            //adding items on prefoma table
            // boolean flag = false;
            int slNo = 1;
            currency = salesPreforma.getMultiCurrency();
            String currencySymbol = (null == currency) ? "" : currency.getSymbol();

            if (!isStoreKeeper) {
                for (SalesPreformaItem pItem : salesPreformaItemList) {
                    double itemTotal = ((pItem.getUnitPrice() / salesPreforma.getCurrentCurrencyRate()) - (pItem.getDiscount() / salesPreforma.getCurrentCurrencyRate())) * pItem.getQuantity();
                    salesPreformaItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / salesPreforma.getCurrentCurrencyRate()), pItem.getQuantity(), commonService.formatIntoCurrencyAsString(pItem.getDiscount() / salesPreforma.getCurrentCurrencyRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal)});
                    netTotal += itemTotal;
                }
                //adding tax,chrgs,etc
                if (netTotal > 0) {
                    // flag = false;
                    for (SalesPreformaTax salesPreformaTax : salesPreformaTaxList) {
//                    if (!flag) {
//                        salesPreformaItemTableModel.addRow(new Object[]{null});
//                        flag = true;
//                    }
                        salesPreformaItemTableModel.addRow(new Object[]{"", "", "\t" + salesPreformaTax.getTax().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(salesPreformaTax.getAmount() / salesPreforma.getCurrentCurrencyRate())});
                        netTotal += salesPreformaTax.getAmount() / salesPreforma.getCurrentCurrencyRate();
                    }
                    //          flag = false;
                    for (SalesPreformaCharge salesPreformaCharge : salesPreformaChargesList) {
//                    if (!flag) {
//                        salesPreformaItemTableModel.addRow(new Object[]{null});
//                        flag = true;
//                    }
                        salesPreformaItemTableModel.addRow(new Object[]{"", "", "\t" + salesPreformaCharge.getCharges().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(salesPreformaCharge.getAmount() / salesPreforma.getCurrentCurrencyRate())});
                        netTotal += salesPreformaCharge.getAmount() / salesPreforma.getCurrentCurrencyRate();
                    }
                }
                totDiscount = salesPreforma.getDiscount() / salesPreforma.getCurrentCurrencyRate();
                grandTotal = netTotal - totDiscount;
                if (totDiscount > 0) {
                    ///////               salesPreformaItemTableModel.addRow(new Object[]{null});
                    salesPreformaItemTableModel.addRow(new Object[]{"", "", "Discount", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(totDiscount)});
                }

                //adding netTotal & grandTotal
                //////           salesPreformaItemTableModel.addRow(new Object[]{null});
                salesPreformaItemTableModel.addRow(new Object[]{"", "", "Net Total", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(netTotal)});
                ///////           salesPreformaItemTableModel.addRow(new Object[]{null});
                salesPreformaItemTableModel.addRow(new Object[]{"", "", "<html><b>Grand Total</b></html>", "", "", "", "<html><b>" + currencySymbol + " " + commonService.formatIntoCurrencyAsString(grandTotal) + "</b></html>"});
            } else {
                for (SalesPreformaItem pItem : salesPreformaItemList) {
                    salesPreformaItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getQuantity()});
                }
            }

            billDateFormattedText.setText(commonService.sqlDateToString(salesPreforma.getBillDate()));
            invoiceNumberText.setText(salesPreforma.getInvoiceNumber());
            customerText.setText(salesPreforma.getCustomer().getLedgerName());
            deleveryTermsText.setText(salesPreforma.getDeleveryTerms());
            paymentTermsText.setText(salesPreforma.getPaymentTerms());

            customerDetails = "";
            if (salesPreforma.getAddress() != null) {
                if (salesPreforma.getAddress().trim().length() > 0) {
                    customerDetails += salesPreforma.getAddress() + "\n";
                }
            }

            addressTextArea.setText(customerDetails);
            telNoText.setText(salesPreforma.getCustomer().getTelephone());
        } catch (Exception e) {
            log.error("fillSalesPrefomaTable:", e);
        }

    }
    private void salePreformaItemTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salePreformaItemTableMouseClicked
        try {
            isPackingListTableClicked = false;
            int selectedRowIndex = salePreformaItemTable.rowAtPoint(evt.getPoint());
            if (salesPreformaItemList.size() >= selectedRowIndex) {
                salesPreformaItem = salesPreformaItemList.get(selectedRowIndex);
                pItemDescriptionText.setText(salesPreformaItem.getItem().getDescription());
                pItemWeightText.setText(salesPreformaItem.getItem().getWeight() + "");
                pUnitLabel.setText(salesPreformaItem.getItem().getUnit().getName());
                pNetWeightText.setText(Double.parseDouble(pContentOfPackageQtyText.getText()) * Double.parseDouble(pItemWeightText.getText()) + "");
                pGrossWeightText.setText(Double.parseDouble(pNetWeightText.getText()) + Double.parseDouble(pPackageWeightText.getText()) + "");
                //fillPackingListTable();
            } else {
                commonService.clearTextFields(new JTextField[]{pItemDescriptionText, pInvoiceNumberText});
                commonService.clearCurrencyFields(new JTextField[]{pItemWeightText, pItemWeightText, pNetWeightText, pGrossWeightText});
                pUnitLabel.setText("");
                salesPreformaItem = null;
            }
        } catch (Exception e) {
            log.error("salePreformaItemTableMouseClicked:", e);
        }
    }//GEN-LAST:event_salePreformaItemTableMouseClicked

    private void invoiceNumberTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_invoiceNumberTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_invoiceNumberTextActionPerformed

    private void invoiceNumberTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_invoiceNumberTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_invoiceNumberTextKeyPressed

    private void customerTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customerTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customerTextActionPerformed

    private void customerTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_customerTextKeyPressed

    }//GEN-LAST:event_customerTextKeyPressed

    private void deliveryTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deliveryTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deliveryTextActionPerformed

    private void deliveryTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_deliveryTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_deliveryTextKeyPressed

    private void paymentTermsTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_paymentTermsTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_paymentTermsTextKeyPressed

    private void deleveryTermsTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleveryTermsTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleveryTermsTextActionPerformed

    private void deleveryTermsTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_deleveryTermsTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleveryTermsTextKeyPressed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        try {
            PendingCommercialInvoiceView pendingCommercialInvoiceView = new PendingCommercialInvoiceView(GlobalProperty.mainForm.getContentPanel());
            GlobalProperty.mainForm.getContentPanel().removeAll();
            GlobalProperty.mainForm.getContentPanel().repaint();
            GlobalProperty.mainForm.getContentPanel().revalidate();
            GlobalProperty.mainForm.getContentPanel().add(pendingCommercialInvoiceView);
        } catch (Exception e) {
            log.error("backButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_backButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        try {
            CommonService.removeJpanel(GlobalProperty.mainForm.getContentPanel());
        } catch (Exception e) {
            log.error("cancelButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void packingListItemTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_packingListItemTableMouseClicked
        try {
            isPackingListTableClicked = true;
            selectedRowIndex = packingListItemTable.rowAtPoint(evt.getPoint());
            if (packingListItemList.size() > selectedRowIndex) {
                packingListItem = packingListItemList.get(selectedRowIndex);
                pItemDescriptionText.setText(packingListItem.getItem().getDescription());
                pItemWeightText.setText(packingListItem.getItem().getWeight() + "");
                pContentOfPackageQtyText.setText(packingListItem.getContentsOfPackageQty() + "");
                pUnitLabel.setText(packingListItem.getItem().getUnit().getName());
                pNetWeightText.setText(packingListItem.getNetWeight() + "");
                pGrossWeightText.setText(packingListItem.getGrossWeight() + "");

                //set combo box
                int count = pKindOfPackageCombo.getItemCount();
                for (int i = 0; i < count; i++) {
                    ComboKeyValue ckv = (ComboKeyValue) pKindOfPackageCombo.getItemAt(i);
                    if (ckv.getKey().trim().equalsIgnoreCase(packingListItem.getKindOfPackage().getName())) {
                        pKindOfPackageCombo.setSelectedItem(ckv);
                    }
                }

            } else {
                commonService.clearTextFields(new JTextField[]{pItemDescriptionText, pInvoiceNumberText});
                commonService.clearCurrencyFields(new JTextField[]{pItemWeightText, pItemWeightText, pNetWeightText, pGrossWeightText});
                pUnitLabel.setText("");
            }
        } catch (Exception e) {
            log.error("packingListItemTableMouseClicked:", e);
        }
    }//GEN-LAST:event_packingListItemTableMouseClicked

    private void pKindOfPackageComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_pKindOfPackageComboItemStateChanged
        try {
            Object packing1 = pKindOfPackageCombo.getSelectedItem();
            packing = (PackingType) ((ComboKeyValue) packing1).getValue();
            pPackageWeightText.setText(packing.getWeight() + "");
            pGrossWeightText.setText(Double.parseDouble(pNetWeightText.getText()) + Double.parseDouble(pPackageWeightText.getText()) + "");
            fillPackingListTable();
        } catch (Exception e) {
            log.error("pKindOfPackageComboItemStateChanged:", e);
        }
    }//GEN-LAST:event_pKindOfPackageComboItemStateChanged

    private void pContentOfPackageQtyTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pContentOfPackageQtyTextFocusGained
        try {
            pContentOfPackageQtyText.selectAll();
        } catch (Exception e) {
            log.error("pContentOfPackageQtyTextFocusGained", e);
        }
    }//GEN-LAST:event_pContentOfPackageQtyTextFocusGained

    private void pContentOfPackageQtyTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pContentOfPackageQtyTextFocusLost
        try {
            pNetWeightText.setText(Double.parseDouble(pContentOfPackageQtyText.getText()) * Double.parseDouble(pItemWeightText.getText()) + "");
            pGrossWeightText.setText(Double.parseDouble(pNetWeightText.getText()) + Double.parseDouble(pPackageWeightText.getText()) + "");
        } catch (Exception e) {
            log.error("pContentOfPackageQtyTextFocusLost", e);
        }

    }//GEN-LAST:event_pContentOfPackageQtyTextFocusLost

    private void pContentOfPackageQtyTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pContentOfPackageQtyTextKeyTyped
        try {
            CommonService.currencyValidator(evt);
        } catch (Exception e) {
            log.error("pContentOfPackageQtyTextKeyTyped", e);
        }
    }//GEN-LAST:event_pContentOfPackageQtyTextKeyTyped

    private void pPackageWeightTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pPackageWeightTextFocusGained
        try {
            fillPackingListTable();
        } catch (Exception e) {
            log.error("pPackageWeightTextFocusGained", e);
        }

    }//GEN-LAST:event_pPackageWeightTextFocusGained

    private void pPackageWeightTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pPackageWeightTextFocusLost
        try {
            pGrossWeightText.setText(Double.parseDouble(pNetWeightText.getText()) + Double.parseDouble(pPackageWeightText.getText()) + "");
        } catch (Exception e) {
            log.error("pPackageWeightTextFocusLost", e);
        }
    }//GEN-LAST:event_pPackageWeightTextFocusLost

    private void pPackageWeightTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pPackageWeightTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_pPackageWeightTextKeyTyped

    private void pNetWeightTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pNetWeightTextFocusGained
        pNetWeightText.selectAll();
    }//GEN-LAST:event_pNetWeightTextFocusGained

    private void pNetWeightTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pNetWeightTextFocusLost
        /*pNetWeightText.setText(Double.parseDouble(pContentOfPackageQtyText.getText())*Double.parseDouble(pItemWeightText.getText())+"");
         pGrossWeightText.setText(Double.parseDouble(pNetWeightText.getText())+Double.parseDouble(pPackageWeightText.getText())+"");*/
    }//GEN-LAST:event_pNetWeightTextFocusLost

    private void pNetWeightTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pNetWeightTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_pNetWeightTextKeyTyped

    private void pGrossWeightTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pGrossWeightTextFocusGained
        pGrossWeightText.selectAll();
    }//GEN-LAST:event_pGrossWeightTextFocusGained

    private void pGrossWeightTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pGrossWeightTextFocusLost
        //fillPackingListTable();
    }//GEN-LAST:event_pGrossWeightTextFocusLost

    private void pGrossWeightTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pGrossWeightTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_pGrossWeightTextKeyTyped

    private void pItemWeightTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pItemWeightTextFocusGained
        pItemWeightText.selectAll();
    }//GEN-LAST:event_pItemWeightTextFocusGained

    private void pItemWeightTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pItemWeightTextFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_pItemWeightTextFocusLost

    private void pItemWeightTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pItemWeightTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_pItemWeightTextKeyTyped

    private void pItemAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pItemAddButtonActionPerformed
        try {
            System.out.println("inside + button");
            double contentsOfPack = Double.parseDouble(pContentOfPackageQtyText.getText());
            if (null != salesPreformaItem && isPackingListTableClicked == false) {
                if (contentsOfPack > 0) {
                    double totalQty = 0;
                    for (PackingListItem packingListItm : packingListItemList) {
                        if (packingListItm.getItem().getItemCode().equalsIgnoreCase(salesPreformaItem.getItem().getItemCode())
                                && packingListItm.getItem().getGodown().getName().equalsIgnoreCase(salesPreformaItem.getItem().getGodown().getName())) {
                            totalQty += packingListItm.getContentsOfPackageQty();
                        }
                    }

                    totalQty = salesPreformaItem.getQuantity() - totalQty;
                    double itm = 0.00;
                    System.out.println("reached + button2 contentsOfPack:" + contentsOfPack);
                    for (itm = contentsOfPack; itm <= totalQty; totalQty -= itm) {
                        System.out.println("itm:" + itm + "\tcontentsOfPack:" + contentsOfPack + "\ttotalQty:" + totalQty);
                        packingListItem = new PackingListItem();
                        packingListItem.setItem(salesPreformaItem.getItem());
                        packingListItem.setKindOfPackage(packing);
                        packingListItem.setContentsOfPackageQty(Double.parseDouble(pContentOfPackageQtyText.getText()));
                        packingListItem.setNetWeight(Double.parseDouble(pContentOfPackageQtyText.getText()) * Double.parseDouble(pItemWeightText.getText()));
                        packingListItem.setGrossWeight(packingListItem.getNetWeight() + Double.parseDouble(pPackageWeightText.getText()));
                        packingListItemList.add(packingListItem);
                    }
                    System.out.println("reached + button3");
                    double balQty = totalQty - itm;
                    if (totalQty > 0) {
                        //adding bal qty
                        packingListItem = new PackingListItem();
                        packingListItem.setItem(salesPreformaItem.getItem());
                        packingListItem.setKindOfPackage(packing);
                        packingListItem.setContentsOfPackageQty(totalQty);
                        packingListItem.setNetWeight(Double.parseDouble(pContentOfPackageQtyText.getText()) * Double.parseDouble(pItemWeightText.getText()));
                        packingListItem.setGrossWeight(packingListItem.getNetWeight() + Double.parseDouble(pPackageWeightText.getText()));
                        packingListItemList.add(packingListItem);
                    }
                    salesPreformaItem = null;
                } else if (isPackingListTableClicked && null != packingListItem) {
                    packingListItem.setKindOfPackage(packing);
                    packingListItem.setContentsOfPackageQty(Double.parseDouble(pContentOfPackageQtyText.getText()));
                    packingListItem.setNetWeight(Double.parseDouble(pContentOfPackageQtyText.getText()) * Double.parseDouble(pItemWeightText.getText()));
                    packingListItem.setGrossWeight(packingListItem.getNetWeight() + Double.parseDouble(pPackageWeightText.getText()));
                }
                fillPackingListTable();
            }
        } catch (Exception e) {
            log.error("pItemAddButtonActionPerformed:", e);
        }

    }//GEN-LAST:event_pItemAddButtonActionPerformed

    private void pItemMinusButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pItemMinusButton1ActionPerformed
        try {
            if (packingListItemList.size() > selectedRowIndex) {
                packingListItemList.remove(selectedRowIndex);
                fillPackingListTable();
            }
        } catch (Exception e) {
            log.error("pItemMinusButton1ActionPerformed:", e);
        }
    }//GEN-LAST:event_pItemMinusButton1ActionPerformed

    void saveOrUpdatePackingList() {
        try {
            packingList.setCustomer(salesPreforma.getCustomer());
            packingList.setInvoiceDate(commonService.utilDateToSqlDate(pInvoiceDate.getDate()));
            packingList.setAddress(addressTextArea.getText());
            packingList.setPackingListItems(null);
            packingList.setPackingListItems(packingListItemList);

            salesPreforma.setPackingList(packingList);

            // for commercial invoice billing 
            packingList.setTotalNumberOfPackages(packingListItemList.size() + " PACKAGES");
            packingList.setCasesList(casesList);
            packingList.setNetWeight(totalNetWeight + " : KGS");
            packingList.setGrossWeight(totalGrossWeight + " : KGS");

            cRUDServices.saveOrUpdateModel(salesPreforma);

            //update number property
            if (isPackingListNew) {
                numberPropertyPackingListInvoice.setNumber(numberPropertyPackingListInvoice.getNumber() + 1);
                cRUDServices.saveOrUpdateModel(numberPropertyPackingListInvoice);
                numberPropertyPackingListNumberOfPack.setNumber(numOfPack);
                cRUDServices.saveOrUpdateModel(numberPropertyPackingListNumberOfPack);
            }
        } catch (Exception e) {
            log.error("savePackingList:", e);
        }
    }
    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed

        try {
            if (salesPreforma != null && packingListItemList.size() > 0) {
                if (MsgBox.confirm("Are you sure you want to save?")) {
                    saveOrUpdatePackingList();
                    MsgBox.success("Successfylly Saved Packing List.");
                }
            } else {
                MsgBox.warning("No Packing List items found.");
            }
        } catch (Exception e) {
            log.error("saveButtonActionPerformed", e);
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void pdfButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdfButtonActionPerformed
        try {
            if (salesPreforma != null && packingListItemList.size() > 0) {
                if (MsgBox.confirm("Are you sure you want to save and create pdf?")) {
                    saveOrUpdatePackingList();
                    PackingListReport report = new PackingListReport();
                    if (!report.createPackingListReport(salesPreforma, "pdf")) {
                        MsgBox.warning("Something went wrong, please re-click the button again!");
                    }
                }
            }
        } catch (Exception e) {
            log.error("printButtonActionPerformed", e);
        }
    }//GEN-LAST:event_pdfButtonActionPerformed

    private void printButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printButtonActionPerformed
        try {
            if (salesPreforma != null && packingListItemList.size() > 0) {
                if (MsgBox.confirm("Are you sure you want to save and print?")) {
                    saveOrUpdatePackingList();
                    PackingListReport report = new PackingListReport();
                    report.createPackingListReport(salesPreforma, "print");
                }
            }
        } catch (Exception e) {
            log.error("printButtonActionPerformed", e);
        }
    }//GEN-LAST:event_printButtonActionPerformed

    private void maxMinPreformaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maxMinPreformaButtonActionPerformed
        try {
            if (maxMinPreformaButton.getText().equalsIgnoreCase(">>")) {
                bodyPanel.removeAll();
                bodyPanel.add(leftSidePanel);
                bodyPanel.revalidate();
                bodyPanel.repaint();
                maxMinPreformaButton.setText("<<");
            } else {
                bodyPanel.removeAll();
                bodyPanel.add(leftSidePanel);
                bodyPanel.add(rightSidePanel);
                bodyPanel.revalidate();
                bodyPanel.repaint();
                maxMinPreformaButton.setText(">>");
            }
        } catch (Exception e) {
            log.error("maxMinPreformaButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_maxMinPreformaButtonActionPerformed

    private void maxMinPackingListButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maxMinPackingListButtonActionPerformed
        try {
            if (maxMinPackingListButton.getText().equalsIgnoreCase("<<")) {
                bodyPanel.removeAll();
                bodyPanel.add(rightSidePanel);
                bodyPanel.revalidate();
                bodyPanel.repaint();
                maxMinPackingListButton.setText(">>");
            } else {

                bodyPanel.removeAll();
                bodyPanel.add(leftSidePanel);
                bodyPanel.add(rightSidePanel);
                bodyPanel.revalidate();
                bodyPanel.repaint();
                maxMinPackingListButton.setText("<<");
            }

        } catch (Exception e) {
            log.error("maxMinPackingListButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_maxMinPackingListButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea addressTextArea;
    private javax.swing.JButton backButton;
    private javax.swing.JFormattedTextField billDateFormattedText;
    private javax.swing.JPanel bodyPanel;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField customerText;
    private javax.swing.JTextField deleveryTermsText;
    private javax.swing.JTextField deliveryText;
    private javax.swing.JTextField invoiceNumberText;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel leftHeadingLabel;
    private javax.swing.JPanel leftSidePanel;
    private javax.swing.JButton maxMinPackingListButton;
    private javax.swing.JButton maxMinPreformaButton;
    private javax.swing.JFormattedTextField pContentOfPackageQtyText;
    private javax.swing.JFormattedTextField pGrossWeightText;
    private com.toedter.calendar.JDateChooser pInvoiceDate;
    private javax.swing.JTextField pInvoiceNumberText;
    private javax.swing.JButton pItemAddButton;
    private javax.swing.JTextField pItemDescriptionText;
    private javax.swing.JButton pItemMinusButton1;
    private javax.swing.JFormattedTextField pItemWeightText;
    private javax.swing.JComboBox pKindOfPackageCombo;
    private javax.swing.JFormattedTextField pNetWeightText;
    private javax.swing.JTextField pNumberOfPackStartText;
    private javax.swing.JFormattedTextField pPackageWeightText;
    private javax.swing.JLabel pUnitLabel;
    private javax.swing.JLabel pUnitLabel1;
    private javax.swing.JLabel pUnitLabel2;
    private javax.swing.JPanel packingDetailsPanel;
    private javax.swing.JTable packingListItemTable;
    private javax.swing.JTextField paymentTermsText;
    private javax.swing.JButton pdfButton;
    private javax.swing.JButton printButton;
    private javax.swing.JPanel rightSidePanel;
    private javax.swing.JPanel rightTablePanel;
    private javax.swing.JTable salePreformaItemTable;
    private javax.swing.JPanel salesPrefomaDetailPanel;
    private javax.swing.JButton saveButton;
    private javax.swing.JPanel tablePanel;
    private javax.swing.JTextField telNoText;
    // End of variables declaration//GEN-END:variables

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {

                    CompanyDAO companyDAO = new CompanyDAO();
                    GlobalProperty.setCompany(companyDAO.findCompanyById(1L));

                    SalesPreformaDAO salesPreformaDAO = new SalesPreformaDAO();
                    List<SalesPreforma> listOfSalesPreforma = salesPreformaDAO.findAll();

                    MainForm jMainFrame = new MainForm();
                    jMainFrame.setVisible(true);
                    jMainFrame.setExtendedState(MainForm.MAXIMIZED_BOTH);

                    PackingListView packingListView = new PackingListView(listOfSalesPreforma.get(0));
                    jMainFrame.getContentPanel().removeAll();
                    jMainFrame.getContentPanel().repaint();
                    jMainFrame.getContentPanel().revalidate();
                    jMainFrame.getContentPanel().add(packingListView);

                } catch (Exception e) {
                    log.error("MainForm run :", e);
                }
            }
        });
    }
}
