/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.CompanyDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.MultiCurrencyDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.dao.PackingTypeDAO;
import com.cloudsendsoft.inventory.dao.ShipmentModeDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Company;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.PackingType;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.services.InitialDataService;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class CompanyView extends javax.swing.JPanel {

    /**
     * Creates new form CompanyView
     */
    static final Logger log = Logger.getLogger(CompanyView.class.getName());
    CommonService commonService = new CommonService();
    DefaultTableModel companyListTableModel = null;
    CRUDServices cRUDServices = new CRUDServices();
    CompanyDAO companyDAO = new CompanyDAO();
    List<Company> listOfCompanies = null;

    //int defaultSelectedIndex = 0;
    Company company = null;
    int selectedRowIndex = 0;

    InitialDataService initialDataService = new InitialDataService();

    public CompanyView() {
        //disable all buttons if no company in DB
        if (GlobalProperty.company == null) {
            commonService.disableAllComponents(GlobalProperty.mainForm.getLeftMenuPanel());
            commonService.disableAllComponents(GlobalProperty.mainForm.getCompanyHeaderPanel());
        }

        initComponents();

        //hiding ref company option 
//        refCompanyComboBox.setVisible(false);
        // jLabel20.setVisible(false);
        if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
            saveOrUpdateButton.setEnabled(false);
            newButton.setEnabled(false);
        }

        //table background color removed
        jScrollPane2.setOpaque(false);
        jScrollPane2.getViewport().setOpaque(false);
        companyListTable.setRowHeight(GlobalProperty.tableRowHieght);

        //set column name & width
        companyListTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{"Company Name", "Start Date", "End Date"}
        ));
        companyListTableModel = (DefaultTableModel) companyListTable.getModel();
        CommonService.setWidthAsPercentages(companyListTable, .70, .15, .15);

        //load reference company
        //for (Company comp : companyDAO.findAll()) {
        //     refCompanyComboBox.addItem(new ComboKeyValue((comp.getName()), comp));
        // }
        fillCompanyListTable();
    }

    void fillCompanyListTable() {
        try {
            listOfCompanies = companyDAO.findAllByDesc();

            if (listOfCompanies.size() > 0) {
                companyListTableModel.setRowCount(0);
                int count = 0;
                for (Company company : listOfCompanies) {

                    companyListTableModel.addRow(new Object[]{company.getName(), commonService.sqlDateToString(company.getStartDate()), commonService.sqlDateToString(company.getEndDate())});
                    if (company.isIsDefault()) {
                        //MsgBox.warning("count"+count);
                        selectedRowIndex = count;
                        this.company = company;
                    }
                    count++;
                }
                companyListTable.setRowSelectionInterval(selectedRowIndex, selectedRowIndex);

                fillTextFields();
            } else {
////                finYearStart.setValue(commonService.convertToDate("01/04/" + (new java.util.Date().getYear() + 1900)));
                fiancialYearStart.setDate(new java.util.Date());

                Calendar c = Calendar.getInstance();
                c.setTime(new java.util.Date()); // Now use today date.
                c.add(Calendar.YEAR, 1); // Adding 1 Year
                c.add(Calendar.DAY_OF_MONTH, -1);
                fiancialYearEnd.setDate(c.getTime());
            }

        } catch (Exception e) {
            log.error("fillCompanyListTable:", e);
        }
    }

    void fillTextFields() {
        //fill textfields with default company
        // MsgBox.warning("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");

        companyNameText.setText(company.getName());
        mailingAddressCompTextArea.setText(company.getMailingAddress());
        stateCompText.setText(company.getState());
        pinCompText.setText(company.getPinCode());
        emailCompText.setText(company.getEmail());
        faxCompText.setText(company.getFax());
        phoneCompText.setText(company.getPhone());
        salesTaxNumCompText.setText(company.getSalesTaxNumber());
        incomeTaxNumCompText.setText(company.getIncomeTaxNumber());
////        finYearStart.setText(commonService.sqlDateToString(company.getStartDate()));
        fiancialYearStart.setDate(company.getStartDate());
        fiancialYearEnd.setDate(company.getEndDate());
        managingDirectorText.setText(company.getManagingDirector());
        accNumBankText.setText(company.getAccountNumber());
        bankBankText.setText(company.getAccountName());
        branchBankText.setText(company.getBranch());
        placeBankText.setText(company.getBankPlace());
        swiftBankText.setText(company.getSwiftCode());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headerPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        bodyPanel = new javax.swing.JPanel();
        companyBankPanel = new javax.swing.JPanel();
        companyPrflPanel = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        faxCompText = new javax.swing.JTextField();
        stateCompText = new javax.swing.JTextField();
        incomeTaxNumCompText = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        salesTaxNumCompText = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        managingDirectorText = new javax.swing.JTextField();
        emailCompText = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        companyNameText = new javax.swing.JTextField();
        phoneCompText = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        mailingAddressCompTextArea = new javax.swing.JTextArea();
        fiancialYearStart = new com.toedter.calendar.JDateChooser();
        fiancialYearEnd = new com.toedter.calendar.JDateChooser();
        pinCompText = new javax.swing.JTextField();
        bankDetailsPanel = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        bankBankText = new javax.swing.JTextField();
        accNumBankText = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        accountNameBankText = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        branchBankText = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        placeBankText = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        swiftBankText = new javax.swing.JTextField();
        listOfCompaniesPanel = new javax.swing.JPanel();
        btnPanel = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        saveOrUpdateButton = new javax.swing.JButton();
        makeAsDefaultButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        companyListTable = new javax.swing.JTable();

        headerPanel.setBackground(new java.awt.Color(51, 51, 51));
        headerPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 204, 204));
        jLabel1.setText("Company Creation");

        javax.swing.GroupLayout headerPanelLayout = new javax.swing.GroupLayout(headerPanel);
        headerPanel.setLayout(headerPanelLayout);
        headerPanelLayout.setHorizontalGroup(
            headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        headerPanelLayout.setVerticalGroup(
            headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headerPanelLayout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        bodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        bodyPanel.setLayout(new java.awt.GridLayout(1, 0));

        companyBankPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        companyBankPanel.setOpaque(false);

        companyPrflPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Company Profile"));
        companyPrflPanel.setOpaque(false);

        jLabel22.setText("Phone");

        jLabel7.setText("Sales Tax Number");

        jLabel2.setText("Name");

        jLabel6.setText("E-Mail Address");

        jLabel21.setText("Fax");

        jLabel23.setText("To");

        jLabel16.setText("Managing Director");

        faxCompText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                faxCompTextActionPerformed(evt);
            }
        });

        incomeTaxNumCompText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                incomeTaxNumCompTextActionPerformed(evt);
            }
        });

        jLabel3.setText("Mailing Address");
        jLabel3.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jLabel4.setText("State");

        jLabel8.setText("Income-Tax Number");

        managingDirectorText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                managingDirectorTextActionPerformed(evt);
            }
        });

        jLabel9.setText("Financial Year from");

        jLabel5.setText("PIN Code");

        phoneCompText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                phoneCompTextActionPerformed(evt);
            }
        });

        mailingAddressCompTextArea.setColumns(20);
        mailingAddressCompTextArea.setRows(5);
        jScrollPane1.setViewportView(mailingAddressCompTextArea);

        fiancialYearStart.setDateFormatString("dd/MM/yyyy");

        fiancialYearEnd.setDateFormatString("dd/MM/yyyy");

        pinCompText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pinCompTextActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout companyPrflPanelLayout = new javax.swing.GroupLayout(companyPrflPanel);
        companyPrflPanel.setLayout(companyPrflPanelLayout);
        companyPrflPanelLayout.setHorizontalGroup(
            companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(companyPrflPanelLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(companyPrflPanelLayout.createSequentialGroup()
                        .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(managingDirectorText))
                    .addGroup(companyPrflPanelLayout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(salesTaxNumCompText))
                    .addGroup(companyPrflPanelLayout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(emailCompText))
                    .addGroup(companyPrflPanelLayout.createSequentialGroup()
                        .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(phoneCompText))
                    .addGroup(companyPrflPanelLayout.createSequentialGroup()
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(faxCompText))
                    .addGroup(companyPrflPanelLayout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pinCompText))
                    .addGroup(companyPrflPanelLayout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stateCompText))
                    .addGroup(companyPrflPanelLayout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1))
                    .addGroup(companyPrflPanelLayout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(companyNameText))
                    .addGroup(companyPrflPanelLayout.createSequentialGroup()
                        .addGroup(companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(incomeTaxNumCompText)
                            .addGroup(companyPrflPanelLayout.createSequentialGroup()
                                .addComponent(fiancialYearStart, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fiancialYearEnd, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        companyPrflPanelLayout.setVerticalGroup(
            companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(companyPrflPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(companyNameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stateCompText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pinCompText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(faxCompText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(phoneCompText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(companyPrflPanelLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(emailCompText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(salesTaxNumCompText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(incomeTaxNumCompText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fiancialYearStart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fiancialYearEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(companyPrflPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(managingDirectorText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3))
        );

        bankDetailsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Bank Details"));
        bankDetailsPanel.setOpaque(false);

        jLabel14.setText("Place");

        jLabel12.setText("Bank");

        jLabel11.setText("Account Number");

        jLabel13.setText("Branch");

        jLabel10.setText("Account Name");

        jLabel15.setText("Swift Code");

        javax.swing.GroupLayout bankDetailsPanelLayout = new javax.swing.GroupLayout(bankDetailsPanel);
        bankDetailsPanel.setLayout(bankDetailsPanelLayout);
        bankDetailsPanelLayout.setHorizontalGroup(
            bankDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bankDetailsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bankDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bankDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bankDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(swiftBankText)
                    .addComponent(accountNameBankText)
                    .addComponent(accNumBankText)
                    .addComponent(bankBankText)
                    .addComponent(branchBankText)
                    .addComponent(placeBankText))
                .addContainerGap())
        );
        bankDetailsPanelLayout.setVerticalGroup(
            bankDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bankDetailsPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(bankDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(accountNameBankText)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bankDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(accNumBankText)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bankDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(bankBankText)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bankDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(branchBankText)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bankDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(placeBankText)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(bankDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(swiftBankText)))
        );

        javax.swing.GroupLayout companyBankPanelLayout = new javax.swing.GroupLayout(companyBankPanel);
        companyBankPanel.setLayout(companyBankPanelLayout);
        companyBankPanelLayout.setHorizontalGroup(
            companyBankPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(companyPrflPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(bankDetailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        companyBankPanelLayout.setVerticalGroup(
            companyBankPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(companyBankPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(companyPrflPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bankDetailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        bodyPanel.add(companyBankPanel);

        listOfCompaniesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 1, 12))); // NOI18N
        listOfCompaniesPanel.setOpaque(false);

        btnPanel.setOpaque(false);
        btnPanel.setLayout(new java.awt.GridLayout(1, 0));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("New");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        btnPanel.add(newButton);

        saveOrUpdateButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveOrUpdateButton.setText("Save / Update");
        saveOrUpdateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveOrUpdateButtonActionPerformed(evt);
            }
        });
        btnPanel.add(saveOrUpdateButton);

        makeAsDefaultButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/default.png"))); // NOI18N
        makeAsDefaultButton.setText("Make As Default");
        makeAsDefaultButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                makeAsDefaultButtonActionPerformed(evt);
            }
        });
        btnPanel.add(makeAsDefaultButton);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/cancel.png"))); // NOI18N
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        btnPanel.add(cancelButton);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("List Of Companies"));
        jPanel1.setOpaque(false);

        jScrollPane2.setOpaque(false);

        companyListTable.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        companyListTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        companyListTable.setOpaque(false);
        companyListTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                companyListTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(companyListTable);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jScrollPane2)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout listOfCompaniesPanelLayout = new javax.swing.GroupLayout(listOfCompaniesPanel);
        listOfCompaniesPanel.setLayout(listOfCompaniesPanelLayout);
        listOfCompaniesPanelLayout.setHorizontalGroup(
            listOfCompaniesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 433, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        listOfCompaniesPanelLayout.setVerticalGroup(
            listOfCompaniesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, listOfCompaniesPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(btnPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        bodyPanel.add(listOfCompaniesPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(headerPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(bodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(headerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(bodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        CommonService.removeJpanel(GlobalProperty.mainForm.getContentPanel());
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void makeAsDefaultButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_makeAsDefaultButtonActionPerformed
        try {
            if (selectedRowIndex >= 0 && selectedRowIndex < listOfCompanies.size()) {
                Company previousCompany = companyDAO.getDefaultCompany();
                previousCompany.setIsDefault(false);
                cRUDServices.saveOrUpdateModel(previousCompany);
                company.setIsDefault(true);
                cRUDServices.saveOrUpdateModel(company);
                GlobalProperty.company = company;
                GlobalProperty.setCurrentPeriodeOnLabel();
                GlobalProperty.setCompanyNameOnLabel();
                MsgBox.success(GlobalProperty.company.getName() + " marked as default Company");
            }
        } catch (Exception e) {
            log.error("makeAsDefaultButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_makeAsDefaultButtonActionPerformed

    private void saveOrUpdateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveOrUpdateButtonActionPerformed
        try {
            boolean isNewCompany = false;
            if (companyNameText.getText().trim().length() > 0) {
                if (!companyDAO.isCompanyExist(companyNameText.getText().trim(), fiancialYearStart.getDate(), fiancialYearStart.getDate())) {
                    if (fiancialYearStart.getDate() != null && fiancialYearStart.getDate() != null) {
                        if (MsgBox.confirm("Are you sure you want to save?")) {

                            if (company == null) {
                                isNewCompany = true;
                                company = new Company();
                            }
                            company.setName(companyNameText.getText().trim());
                            company.setMailingAddress(mailingAddressCompTextArea.getText().trim());
                            company.setState(stateCompText.getText().trim());
                            company.setPinCode(pinCompText.getText().trim());
                            company.setEmail(emailCompText.getText().trim());
                            company.setFax(faxCompText.getText().trim());
                            company.setPhone(phoneCompText.getText().trim());
                            company.setSalesTaxNumber(salesTaxNumCompText.getText().trim());
                            company.setIncomeTaxNumber(incomeTaxNumCompText.getText().trim());
                            company.setStartDate(commonService.utilDateToSqlDate(fiancialYearStart.getDate()));
                            company.setEndDate(commonService.utilDateToSqlDate(fiancialYearEnd.getDate()));
                            company.setManagingDirector(managingDirectorText.getText().trim());
                            company.setAccountName(accountNameBankText.getText().trim());
                            company.setAccountNumber(accNumBankText.getText().trim());
                            company.setBankName(bankBankText.getText().trim());
                            company.setBranch(branchBankText.getText().trim());
                            company.setBankPlace(placeBankText.getText().trim());
                            company.setSwiftCode(swiftBankText.getText().trim());
                            if (listOfCompanies.size() == 0) {
                                company.setIsDefault(true);
                            } else if(company!=null){
                                if(company.isIsDefault()){
                                    company.setIsDefault(true);
                                }else{
                                    company.setIsDefault(false);
                                }
                            }else{
                                company.setIsDefault(false);
                            }
                            cRUDServices.saveOrUpdateModel(company);

                            //initial data for company
                            if (isNewCompany) {
                                initialDataService.createDefaultNumberProperties(company);
                                initialDataService.createDefaultLedgers(company);
                                initialDataService.loadOtherTables(company);
                            }

                            /* if (refCompanyComboBox.getSelectedItem() != "N/A") {
                             //referencing company
                             Object comp = refCompanyComboBox.getSelectedItem();
                             Company refCompany = ((Company) ((ComboKeyValue) comp).getValue());
                             //Adding charges
                             List<Charges> chargesList = null;
                             ChargesDAO chargesDAO = new ChargesDAO();

                             chargesList = chargesDAO.findAll(refCompany);
                             for (Charges chrg : chargesList) {
                             Charges newChrg = new Charges();
                             newChrg.setName(chrg.getName());
                             newChrg.setCompany(company);
                             cRUDServices.saveModel(newChrg);

                             }
                             //Adding multicurrency
                             List<MultiCurrency> multiCurrencyList = null;
                             MultiCurrencyDAO multiCurrencyDAO = new MultiCurrencyDAO();
                             multiCurrencyList = multiCurrencyDAO.findAll(refCompany);
                             for (MultiCurrency multiCurrency : multiCurrencyList) {
                             MultiCurrency newMultiCurrency = new MultiCurrency();
                             newMultiCurrency.setName(multiCurrency.getName());
                             newMultiCurrency.setRate(multiCurrency.getRate());
                             newMultiCurrency.setSymbol(multiCurrency.getSymbol());
                             //newMultiCurrency.setCompany(company);
                             cRUDServices.saveModel(newMultiCurrency);
                             }
                             List<NumberProperty> numberPropertyList = null;
                             NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
                             numberPropertyList = numberPropertyDAO.findAll(refCompany);
                             for (NumberProperty numberProp : numberPropertyList) {
                             NumberProperty newNumberProperty = new NumberProperty();
                             newNumberProperty.setCategory(numberProp.getCategory());
                             newNumberProperty.setCategoryPrefix(numberProp.getCategoryPrefix());
                             newNumberProperty.setNumber(numberProp.getNumber() + 1);
                             newNumberProperty.setCompany(company);
                             cRUDServices.saveModel(newNumberProperty);
                             }
                             //Adding Packing Type
                             List<PackingType> packingTypeList = null;
                             PackingTypeDAO packingTypeDAO = new PackingTypeDAO();

                             packingTypeList = packingTypeDAO.findAll(refCompany);
                             for (PackingType packingType : packingTypeList) {
                             PackingType newPackingType = new PackingType();
                             newPackingType.setName(packingType.getName());
                             newPackingType.setWeight(packingType.getWeight());
                             newPackingType.setCompany(company);
                             cRUDServices.saveModel(newPackingType);

                             }
                             List<ShipmentMode> shipmentModesList = null;
                             ShipmentModeDAO shipmentModeDAO = new ShipmentModeDAO();

                             shipmentModesList = shipmentModeDAO.findAll(refCompany);
                             for (ShipmentMode shipmentMode : shipmentModesList) {
                             ShipmentMode newShipmentMode = new ShipmentMode();
                             newShipmentMode.setName(shipmentMode.getName());
                             newShipmentMode.setCompany(company);
                             cRUDServices.saveModel(newShipmentMode);

                             }
                             List<Tax> taxList = null;
                             TaxDAO taxDAO = new TaxDAO();

                             taxList = taxDAO.findAll();
                             for (Tax tax : taxList) {
                             Tax newTax = new Tax();
                             newTax.setName(tax.getName());
                             newTax.setTaxRate(tax.getTaxRate());
                             newTax.setCompany(company);
                             cRUDServices.saveModel(newTax);

                             }
                             List<Item> itemList = null;
                             ItemDAO itemDAO = new ItemDAO();

                             itemList = itemDAO.findAll();
                             for (Item item : itemList) {
                             Item newItem = new Item();
                             newItem.setAvailableQty(item.getAvailableQty());
                             newItem.setOpeningQty(item.getAvailableQty());
                             newItem.setOpeningBal(0.00);
                             newItem.setOpeningUnitPrice(0.00);
                             newItem.setDescription(item.getDescription());
                             newItem.setItemCode(item.getItemCode());
                             newItem.setSellingPrice(item.getSellingPrice());
                             newItem.setVisible(item.isVisible());
                             newItem.setWeight(item.getWeight());
                             newItem.setGodown(item.getGodown());
                             newItem.setStockGroup(item.getStockGroup());
                             newItem.setUnit(item.getUnit());
                             newItem.setCompany(company);
                             cRUDServices.saveModel(newItem);
                             }
                             }*/
                            MsgBox.success("Successfully Saved Company!");

                            if (GlobalProperty.company == null) {
                                GlobalProperty.company = company;
                                commonService.enableAllComponents(GlobalProperty.mainForm.getLeftMenuPanel());
                                commonService.enableAllComponents(GlobalProperty.mainForm.getCompanyHeaderPanel());
                                GlobalProperty.mainForm.addMenuAndButtons(GlobalProperty.getUser().getListOfPrivileges());
                                GlobalProperty.setCurrentPeriodeOnLabel();
                            }
                            fillCompanyListTable();
                        }
                    } else {
                        MsgBox.warning("Please correct 'Financial Year From/To' field, format should be dd/mm/yyyy");
                    }
                } else {
                    MsgBox.warning("Company Already Exist! (CompanyName, StartDate, EndDate)");
                }
            } else {
                MsgBox.warning("Company Name is mandatory!");
            }
        } catch (Exception e) {
            log.error("addButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_saveOrUpdateButtonActionPerformed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        clearAll();
    }//GEN-LAST:event_newButtonActionPerformed

    private void companyListTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_companyListTableMouseClicked
        selectedRowIndex = companyListTable.rowAtPoint(evt.getPoint());
        company = listOfCompanies.get(selectedRowIndex);
        fillTextFields();
    }//GEN-LAST:event_companyListTableMouseClicked

    private void phoneCompTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_phoneCompTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_phoneCompTextActionPerformed

    private void faxCompTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_faxCompTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_faxCompTextActionPerformed

    private void managingDirectorTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_managingDirectorTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_managingDirectorTextActionPerformed

    private void incomeTaxNumCompTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_incomeTaxNumCompTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_incomeTaxNumCompTextActionPerformed

    private void pinCompTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pinCompTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pinCompTextActionPerformed

    void clearAll() {
        commonService.clearTextFields(new JTextField[]{companyNameText, stateCompText, pinCompText, emailCompText, salesTaxNumCompText, incomeTaxNumCompText,
            managingDirectorText, accNumBankText, bankBankText, branchBankText, placeBankText, swiftBankText, faxCompText, phoneCompText, accountNameBankText});
        commonService.clearTextArea(new JTextArea[]{mailingAddressCompTextArea});
        fiancialYearStart.setDate(new java.util.Date());
        Calendar c = Calendar.getInstance();
        c.setTime(new java.util.Date()); // Now use today date.
        c.add(Calendar.YEAR, 1); // Adding 1 Year
        c.add(Calendar.DAY_OF_MONTH, -1);
        fiancialYearEnd.setDate(c.getTime());
        company = null;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField accNumBankText;
    private javax.swing.JTextField accountNameBankText;
    private javax.swing.JTextField bankBankText;
    private javax.swing.JPanel bankDetailsPanel;
    private javax.swing.JPanel bodyPanel;
    private javax.swing.JTextField branchBankText;
    private javax.swing.JPanel btnPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JPanel companyBankPanel;
    private javax.swing.JTable companyListTable;
    private javax.swing.JTextField companyNameText;
    private javax.swing.JPanel companyPrflPanel;
    private javax.swing.JTextField emailCompText;
    private javax.swing.JTextField faxCompText;
    private com.toedter.calendar.JDateChooser fiancialYearEnd;
    private com.toedter.calendar.JDateChooser fiancialYearStart;
    private javax.swing.JPanel headerPanel;
    private javax.swing.JTextField incomeTaxNumCompText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel listOfCompaniesPanel;
    private javax.swing.JTextArea mailingAddressCompTextArea;
    private javax.swing.JButton makeAsDefaultButton;
    private javax.swing.JTextField managingDirectorText;
    private javax.swing.JButton newButton;
    private javax.swing.JTextField phoneCompText;
    private javax.swing.JTextField pinCompText;
    private javax.swing.JTextField placeBankText;
    private javax.swing.JTextField salesTaxNumCompText;
    private javax.swing.JButton saveOrUpdateButton;
    private javax.swing.JTextField stateCompText;
    private javax.swing.JTextField swiftBankText;
    // End of variables declaration//GEN-END:variables
}
