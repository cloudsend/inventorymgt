package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.dao.CompanyDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.LedgerGroupDAO;
import com.cloudsendsoft.inventory.dao.PrivilegesDAO;
import com.cloudsendsoft.inventory.dao.UserDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Company;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Privileges;
import com.cloudsendsoft.inventory.services.InitialDataService;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author sangeeth
 */
public class MainForm extends javax.swing.JFrame {

    /**
     * Creates new form MainForm
     */
   
    
   static final Logger log = Logger.getLogger(MainForm.class.getName());
   CommonService commonService=new CommonService();
   GlobalProperty globalProperty=new GlobalProperty();
   
   public void triggerInitComponent(){
       //this.setVisible(true);
        //            this.setExtendedState(MainForm.MAXIMIZED_BOTH);
       mainPanel.setLayout(new java.awt.BorderLayout());
       mainPanel.removeAll();
       mainPanel.repaint();
       mainPanel.revalidate();
       mainPanel.add(CompanyHeader, java.awt.BorderLayout.PAGE_START);
       mainPanel.add(ContentPanel, java.awt.BorderLayout.CENTER);
       mainPanel.add(LeftMenuPanel, java.awt.BorderLayout.LINE_START);
      
   }
    public MainForm() {

        log.info("Started MainForm...");
        initComponents();
        globalProperty.setMainForm(this);
       
        //set root icon
       setTitle("Inventory Management System");
       ImageIcon img = new ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/2M_LogoOriginal.png"));
       setIconImage(img.getImage());
       
        //temperory
        CompanyDAO companyDAO=new CompanyDAO();
        Company company= companyDAO.getDefaultCompany();
        GlobalProperty.setCompany(company);
        
        //set homePageIcon
        GlobalProperty.setHomePageIcon(homePageLabel);
        //temp

        /* 
         // close windows when Esc press
         KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
         Action action = new AbstractAction() {   
         public void actionPerformed(ActionEvent e) {
         jDesktopPane.getSelectedFrame().dispose();
         }
         };      
        
         jDesktopPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(escape, "escape");
         jDesktopPane.getActionMap().put("escape", action);
         */
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu5 = new javax.swing.JMenu();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem16 = new javax.swing.JMenuItem();
        jMenuItem21 = new javax.swing.JMenuItem();
        jDesktopPane = new javax.swing.JDesktopPane();
        mainPanel = new javax.swing.JPanel();
        CompanyHeader = new javax.swing.JPanel();
        currentPeriodeLabel = new javax.swing.JLabel();
        logoLabel = new javax.swing.JLabel();
        userNameLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        companyNameLabel = new javax.swing.JLabel();
        LeftMenuPanel = new javax.swing.JPanel();
        homeButton = new javax.swing.JButton();
        purchaseButton = new javax.swing.JButton();
        salesPreformaButton = new javax.swing.JButton();
        packingListButton = new javax.swing.JButton();
        salesButton = new javax.swing.JButton();
        paymentButton = new javax.swing.JButton();
        receiptButton = new javax.swing.JButton();
        contraButton = new javax.swing.JButton();
        purchaseReturnButton = new javax.swing.JButton();
        salesReturnButton = new javax.swing.JButton();
        trialBalanceButton = new javax.swing.JButton();
        PandLButton = new javax.swing.JButton();
        balanceSheetButton = new javax.swing.JButton();
        journalButton1 = new javax.swing.JButton();
        logOutButton = new javax.swing.JButton();
        ContentPanel = new javax.swing.JPanel();
        homePageLabel = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        File_Menu = new javax.swing.JMenu();
        jMenuItem5 = new javax.swing.JMenuItem();
        Purchase_Menu = new javax.swing.JMenu();
        purchaseMenuItem = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        Stock_Menu = new javax.swing.JMenu();
        Inventory_MenuItem = new javax.swing.JMenuItem();
        StockGroup_MenuItem = new javax.swing.JMenuItem();
        StockSummary_MenuItem = new javax.swing.JMenuItem();
        jMenuItem19 = new javax.swing.JMenuItem();
        Accounts_Menu = new javax.swing.JMenu();
        ledgerMenuItem = new javax.swing.JMenuItem();
        trialBalanceItem24 = new javax.swing.JMenuItem();
        profitAndLossMenuItem25 = new javax.swing.JMenuItem();
        balanceSheetMenuItem = new javax.swing.JMenuItem();
        Options_Menu = new javax.swing.JMenu();
        taxMenuItem = new javax.swing.JMenuItem();
        chargesMenuItem = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        Packing = new javax.swing.JMenuItem();
        unitMenuItem = new javax.swing.JMenuItem();
        shipmentModeMenuItem = new javax.swing.JMenuItem();
        Company_Menu = new javax.swing.JMenu();
        jMenuItem7 = new javax.swing.JMenuItem();
        splitCompanyMenuItem = new javax.swing.JMenuItem();
        History_Menu = new javax.swing.JMenu();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenuItem15 = new javax.swing.JMenuItem();
        jMenuItem17 = new javax.swing.JMenuItem();
        jMenuItem18 = new javax.swing.JMenuItem();
        journalMenuItem = new javax.swing.JMenuItem();
        jMenuItem22 = new javax.swing.JMenuItem();
        jMenuItem23 = new javax.swing.JMenuItem();
        jMenuItem27 = new javax.swing.JMenuItem();
        User_Menu = new javax.swing.JMenu();
        userMgtMenuItem = new javax.swing.JMenuItem();
        dbSettingsMenuItem = new javax.swing.JMenuItem();

        jMenu5.setText("jMenu5");

        jMenuItem10.setText("jMenuItem10");

        jMenuItem12.setText("jMenuItem12");

        jMenuItem16.setText("jMenuItem16");

        jMenuItem21.setText("jMenuItem21");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Inventory with Accounts");

        jDesktopPane.setBackground(new java.awt.Color(255, 255, 255));

        mainPanel.setLayout(new java.awt.BorderLayout());

        CompanyHeader.setBackground(new java.awt.Color(255, 255, 255));
        CompanyHeader.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        currentPeriodeLabel.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        currentPeriodeLabel.setForeground(new java.awt.Color(134, 0, 0));
        currentPeriodeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        logoLabel.setFont(new java.awt.Font("Algerian", 0, 48)); // NOI18N
        logoLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        logoLabel.setText("Repeat");
        logoLabel.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);

        userNameLabel.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        userNameLabel.setForeground(new java.awt.Color(134, 0, 0));
        userNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/people.png"))); // NOI18N

        companyNameLabel.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        companyNameLabel.setForeground(new java.awt.Color(134, 0, 0));
        companyNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        javax.swing.GroupLayout CompanyHeaderLayout = new javax.swing.GroupLayout(CompanyHeader);
        CompanyHeader.setLayout(CompanyHeaderLayout);
        CompanyHeaderLayout.setHorizontalGroup(
            CompanyHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CompanyHeaderLayout.createSequentialGroup()
                .addComponent(logoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 427, Short.MAX_VALUE)
                .addGroup(CompanyHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(CompanyHeaderLayout.createSequentialGroup()
                        .addComponent(userNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 363, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(currentPeriodeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(CompanyHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CompanyHeaderLayout.createSequentialGroup()
                    .addContainerGap(717, Short.MAX_VALUE)
                    .addComponent(companyNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );
        CompanyHeaderLayout.setVerticalGroup(
            CompanyHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CompanyHeaderLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CompanyHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                    .addComponent(userNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addComponent(currentPeriodeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(logoLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(CompanyHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CompanyHeaderLayout.createSequentialGroup()
                    .addContainerGap(47, Short.MAX_VALUE)
                    .addComponent(companyNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(27, 27, 27)))
        );

        mainPanel.add(CompanyHeader, java.awt.BorderLayout.PAGE_START);

        LeftMenuPanel.setBackground(new java.awt.Color(255, 255, 255));
        LeftMenuPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        homeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/Home-icon.png"))); // NOI18N
        homeButton.setText("   Home");
        homeButton.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        homeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                homeButtonActionPerformed(evt);
            }
        });

        purchaseButton.setText("Purchase");
        purchaseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                purchaseButtonActionPerformed(evt);
            }
        });

        salesPreformaButton.setText("Sales Performa");
        salesPreformaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salesPreformaButtonActionPerformed(evt);
            }
        });

        packingListButton.setText("Packing List");
        packingListButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                packingListButtonActionPerformed(evt);
            }
        });

        salesButton.setText("Sales");
        salesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salesButtonActionPerformed(evt);
            }
        });

        paymentButton.setText("Payment");
        paymentButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paymentButtonActionPerformed(evt);
            }
        });

        receiptButton.setText("Receipt");
        receiptButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                receiptButtonActionPerformed(evt);
            }
        });

        contraButton.setText("Contra");
        contraButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                contraButtonActionPerformed(evt);
            }
        });

        purchaseReturnButton.setText("Purchase Return");
        purchaseReturnButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                purchaseReturnButtonActionPerformed(evt);
            }
        });

        salesReturnButton.setText("Sales Return");
        salesReturnButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salesReturnButtonActionPerformed(evt);
            }
        });

        trialBalanceButton.setText("Trial Balance");
        trialBalanceButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                trialBalanceButtonActionPerformed(evt);
            }
        });

        PandLButton.setText("P & L A/C");
        PandLButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PandLButtonActionPerformed(evt);
            }
        });

        balanceSheetButton.setText("Balance Sheet");
        balanceSheetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                balanceSheetButtonActionPerformed(evt);
            }
        });

        journalButton1.setText("Journal");
        journalButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                journalButton1ActionPerformed(evt);
            }
        });

        logOutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/logout-icon.png"))); // NOI18N
        logOutButton.setText("    LogOut");
        logOutButton.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        logOutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logOutButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout LeftMenuPanelLayout = new javax.swing.GroupLayout(LeftMenuPanel);
        LeftMenuPanel.setLayout(LeftMenuPanelLayout);
        LeftMenuPanelLayout.setHorizontalGroup(
            LeftMenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(purchaseButton, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(salesPreformaButton, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(packingListButton, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(salesButton, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(paymentButton, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(receiptButton, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(contraButton, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(journalButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(purchaseReturnButton, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(salesReturnButton, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(trialBalanceButton, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(PandLButton, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(balanceSheetButton, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(logOutButton, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(homeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        LeftMenuPanelLayout.setVerticalGroup(
            LeftMenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LeftMenuPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(homeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(purchaseButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(salesPreformaButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(packingListButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(salesButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(paymentButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(receiptButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(contraButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(journalButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(purchaseReturnButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(salesReturnButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(trialBalanceButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(PandLButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(balanceSheetButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(logOutButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );

        mainPanel.add(LeftMenuPanel, java.awt.BorderLayout.LINE_START);

        ContentPanel.setBackground(new java.awt.Color(117, 0, 0));
        ContentPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        ContentPanel.setLayout(new java.awt.CardLayout());

        homePageLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        homePageLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/logo1.png"))); // NOI18N
        ContentPanel.add(homePageLabel, "card2");

        mainPanel.add(ContentPanel, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout jDesktopPaneLayout = new javax.swing.GroupLayout(jDesktopPane);
        jDesktopPane.setLayout(jDesktopPaneLayout);
        jDesktopPaneLayout.setHorizontalGroup(
            jDesktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jDesktopPaneLayout.setVerticalGroup(
            jDesktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jDesktopPane.setLayer(mainPanel, javax.swing.JLayeredPane.DEFAULT_LAYER);

        getContentPane().add(jDesktopPane, java.awt.BorderLayout.CENTER);

        File_Menu.setText("File");

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem5.setText("Exit");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        File_Menu.add(jMenuItem5);

        jMenuBar1.add(File_Menu);

        Purchase_Menu.setText("Purchase");

        purchaseMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F9, 0));
        purchaseMenuItem.setText("Purchase Invoice");
        purchaseMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                purchaseMenuItemActionPerformed(evt);
            }
        });
        Purchase_Menu.add(purchaseMenuItem);

        jMenuItem2.setText("PurchaseOrder");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        Purchase_Menu.add(jMenuItem2);

        jMenuBar1.add(Purchase_Menu);

        Stock_Menu.setMnemonic('S');
        Stock_Menu.setText("Stock");

        Inventory_MenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        Inventory_MenuItem.setText("Inventory");
        Inventory_MenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Inventory_MenuItemActionPerformed(evt);
            }
        });
        Stock_Menu.add(Inventory_MenuItem);

        StockGroup_MenuItem.setText("Stock Group");
        StockGroup_MenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StockGroup_MenuItemActionPerformed(evt);
            }
        });
        Stock_Menu.add(StockGroup_MenuItem);

        StockSummary_MenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        StockSummary_MenuItem.setText("Stock Summary");
        StockSummary_MenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StockSummary_MenuItemActionPerformed(evt);
            }
        });
        Stock_Menu.add(StockSummary_MenuItem);

        jMenuItem19.setText("Move Stock");
        jMenuItem19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem19ActionPerformed(evt);
            }
        });
        Stock_Menu.add(jMenuItem19);

        jMenuBar1.add(Stock_Menu);

        Accounts_Menu.setText("Accounts");

        ledgerMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        ledgerMenuItem.setText("Ledger");
        ledgerMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ledgerMenuItemActionPerformed(evt);
            }
        });
        Accounts_Menu.add(ledgerMenuItem);

        trialBalanceItem24.setText("Trial Balance");
        trialBalanceItem24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                trialBalanceItem24ActionPerformed(evt);
            }
        });
        Accounts_Menu.add(trialBalanceItem24);

        profitAndLossMenuItem25.setText("Profit & Loss A/C");
        profitAndLossMenuItem25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                profitAndLossMenuItem25ActionPerformed(evt);
            }
        });
        Accounts_Menu.add(profitAndLossMenuItem25);

        balanceSheetMenuItem.setText("Balance Sheet");
        balanceSheetMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                balanceSheetMenuItemActionPerformed(evt);
            }
        });
        Accounts_Menu.add(balanceSheetMenuItem);

        jMenuBar1.add(Accounts_Menu);

        Options_Menu.setMnemonic('O');
        Options_Menu.setText("Options");

        taxMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.CTRL_MASK));
        taxMenuItem.setText("Tax ");
        taxMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                taxMenuItemActionPerformed(evt);
            }
        });
        Options_Menu.add(taxMenuItem);

        chargesMenuItem.setText("Charges");
        chargesMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesMenuItemActionPerformed(evt);
            }
        });
        Options_Menu.add(chargesMenuItem);

        jMenuItem6.setText("Multi Currency");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        Options_Menu.add(jMenuItem6);

        jMenuItem4.setText("NumberProperty");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        Options_Menu.add(jMenuItem4);

        jMenuItem8.setText("Godown");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        Options_Menu.add(jMenuItem8);

        Packing.setText("Packing");
        Packing.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PackingActionPerformed(evt);
            }
        });
        Options_Menu.add(Packing);

        unitMenuItem.setText("Units");
        unitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                unitMenuItemActionPerformed(evt);
            }
        });
        Options_Menu.add(unitMenuItem);

        shipmentModeMenuItem.setText("Shipment Mode");
        shipmentModeMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                shipmentModeMenuItemActionPerformed(evt);
            }
        });
        Options_Menu.add(shipmentModeMenuItem);

        jMenuBar1.add(Options_Menu);

        Company_Menu.setText("Company");

        jMenuItem7.setText("Company Mgt.");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        Company_Menu.add(jMenuItem7);

        splitCompanyMenuItem.setText("Split Company");
        splitCompanyMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                splitCompanyMenuItemActionPerformed(evt);
            }
        });
        Company_Menu.add(splitCompanyMenuItem);

        jMenuBar1.add(Company_Menu);

        History_Menu.setText("History");
        History_Menu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                History_MenuActionPerformed(evt);
            }
        });

        jMenuItem9.setText("Purchase");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        History_Menu.add(jMenuItem9);

        jMenuItem11.setText("Sales");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        History_Menu.add(jMenuItem11);

        jMenuItem13.setText("SalesPreforma");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        History_Menu.add(jMenuItem13);

        jMenuItem14.setText("SalesReturn");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        History_Menu.add(jMenuItem14);

        jMenuItem15.setText("PurchaseReturn");
        jMenuItem15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem15ActionPerformed(evt);
            }
        });
        History_Menu.add(jMenuItem15);

        jMenuItem17.setText("Payment");
        jMenuItem17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem17ActionPerformed(evt);
            }
        });
        History_Menu.add(jMenuItem17);

        jMenuItem18.setText("Receipt");
        jMenuItem18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem18ActionPerformed(evt);
            }
        });
        History_Menu.add(jMenuItem18);

        journalMenuItem.setText("Journal");
        journalMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                journalMenuItemActionPerformed(evt);
            }
        });
        History_Menu.add(journalMenuItem);

        jMenuItem22.setText("Contra");
        jMenuItem22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem22ActionPerformed(evt);
            }
        });
        History_Menu.add(jMenuItem22);

        jMenuItem23.setText("Commercial Invoice");
        jMenuItem23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem23ActionPerformed(evt);
            }
        });
        History_Menu.add(jMenuItem23);

        jMenuItem27.setText("Purchase Order");
        jMenuItem27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem27ActionPerformed(evt);
            }
        });
        History_Menu.add(jMenuItem27);

        jMenuBar1.add(History_Menu);

        User_Menu.setText("User");

        userMgtMenuItem.setText("User Management");
        userMgtMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userMgtMenuItemActionPerformed(evt);
            }
        });
        User_Menu.add(userMgtMenuItem);

        dbSettingsMenuItem.setText("Database Settings");
        dbSettingsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dbSettingsMenuItemActionPerformed(evt);
            }
        });
        User_Menu.add(dbSettingsMenuItem);

        jMenuBar1.add(User_Menu);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void purchaseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_purchaseButtonActionPerformed
        PurchaseView purchase = new PurchaseView(jMainFrame);
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(purchase);
    }//GEN-LAST:event_purchaseButtonActionPerformed

    public JPanel getContentPanel() {
        return ContentPanel;
    }

    public JPanel getLeftMenuPanel() {
        return LeftMenuPanel;
    }

    public JPanel getCompanyHeaderPanel() {
        return CompanyHeader;
    }

    public JLabel getCurrentPeriodeLabel() {
        return currentPeriodeLabel;
    }
    public JLabel getCompanyNameLabel() {
        return companyNameLabel;
    }
    public JLabel getUnerNameLabel() {
        return userNameLabel;
    }

    private void Inventory_MenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Inventory_MenuItemActionPerformed
        ItemInfo addItem = new ItemInfo();
        jDesktopPane.add(addItem);
        addItem.setVisible(true);       
        addItem.setFrameIcon(null);
        commonService.setInternalFrameCenter(this, addItem);
        commonService.removeInternalFrameTitleDropdown(addItem);
    }//GEN-LAST:event_Inventory_MenuItemActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void ledgerMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ledgerMenuItemActionPerformed
        LedgerView ledgerView=new LedgerView();
        jDesktopPane.add(ledgerView);
        ledgerView.setVisible(true);       
        ledgerView.setFrameIcon(null);
        commonService.setInternalFrameCenter(this, ledgerView);
        commonService.removeInternalFrameTitleDropdown(ledgerView);
    }//GEN-LAST:event_ledgerMenuItemActionPerformed

    private void taxMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_taxMenuItemActionPerformed
        TaxView taxView=new TaxView();
        jDesktopPane.add(taxView);
        taxView.setVisible(true);  
        taxView.setFrameIcon(null);
        commonService.setInternalFrameCenter(this, taxView);
        commonService.removeInternalFrameTitleDropdown(taxView);
    }//GEN-LAST:event_taxMenuItemActionPerformed

    private void chargesMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesMenuItemActionPerformed
        ChargesView chargesView=new ChargesView();
        jDesktopPane.add(chargesView);
        chargesView.setVisible(true);  
        chargesView.setFrameIcon(null);
        commonService.setInternalFrameCenter(this, chargesView);
        commonService.removeInternalFrameTitleDropdown(chargesView);
    }//GEN-LAST:event_chargesMenuItemActionPerformed

    private void salesPreformaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salesPreformaButtonActionPerformed
       SalesPreformaView preformaView=new SalesPreformaView(jMainFrame);
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(preformaView);
    }//GEN-LAST:event_salesPreformaButtonActionPerformed

    private void packingListButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_packingListButtonActionPerformed
        PendingCommercialInvoiceView pendingCommercialInvoiceView=new PendingCommercialInvoiceView(ContentPanel);
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(pendingCommercialInvoiceView);
    }//GEN-LAST:event_packingListButtonActionPerformed

    private void salesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salesButtonActionPerformed
        SalesView salesView=new SalesView(jMainFrame);
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(salesView);
    }//GEN-LAST:event_salesButtonActionPerformed

    private void StockGroup_MenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StockGroup_MenuItemActionPerformed
        StockGroupView stockGroupView=new StockGroupView();
        jDesktopPane.add(stockGroupView);
        stockGroupView.setVisible(true);  
        stockGroupView.setFrameIcon(null);
        commonService.setInternalFrameCenter(this, stockGroupView);
        commonService.removeInternalFrameTitleDropdown(stockGroupView);
    }//GEN-LAST:event_StockGroup_MenuItemActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        MultiCurrencyView multiCurrencyView=new MultiCurrencyView();
        jDesktopPane.add(multiCurrencyView);
        multiCurrencyView.setVisible(true);  
        multiCurrencyView.setFrameIcon(null);
        commonService.setInternalFrameCenter(this, multiCurrencyView);
        commonService.removeInternalFrameTitleDropdown(multiCurrencyView);
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void StockSummary_MenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StockSummary_MenuItemActionPerformed
        StockSummary stockSummary=new StockSummary();
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(stockSummary);
    }//GEN-LAST:event_StockSummary_MenuItemActionPerformed

    private void paymentButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paymentButtonActionPerformed
        JournalEntryView createJournal=new JournalEntryView("Payment");
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(createJournal);
    }//GEN-LAST:event_paymentButtonActionPerformed

    private void receiptButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_receiptButtonActionPerformed
       JournalEntryView createJournal=new JournalEntryView("Receipt");
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(createJournal);
    }//GEN-LAST:event_receiptButtonActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        NumberPropertyView numberPropertyView=new NumberPropertyView();
        jDesktopPane.add(numberPropertyView);
        numberPropertyView.setVisible(true);  
        numberPropertyView.setFrameIcon(null);
        commonService.setInternalFrameCenter(this, numberPropertyView);
        commonService.removeInternalFrameTitleDropdown(numberPropertyView);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        CompanyView createCompany=new CompanyView();
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(createCompany);
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        GodownView godownView=new GodownView();
        jDesktopPane.add(godownView);
        godownView.setVisible(true);  
        godownView.setFrameIcon(null);
        commonService.setInternalFrameCenter(this, godownView);
        commonService.removeInternalFrameTitleDropdown(godownView);
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void PackingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PackingActionPerformed
        PackingView packingView=new PackingView();
        jDesktopPane.add(packingView);
        packingView.setVisible(true);  
        packingView.setFrameIcon(null);
        commonService.setInternalFrameCenter(this, packingView);
        commonService.removeInternalFrameTitleDropdown(packingView);
    }//GEN-LAST:event_PackingActionPerformed

    private void contraButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contraButtonActionPerformed
       
        JournalEntryView createJournal=new JournalEntryView("Contra");
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(createJournal);
    }//GEN-LAST:event_contraButtonActionPerformed

    private void purchaseReturnButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_purchaseReturnButtonActionPerformed
        PurchaseReturnView createPurchaseReturn=new PurchaseReturnView();
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(createPurchaseReturn);
    }//GEN-LAST:event_purchaseReturnButtonActionPerformed

    private void salesReturnButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salesReturnButtonActionPerformed
       SalesReturnView createSalesReturn=new SalesReturnView();
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(createSalesReturn);
    }//GEN-LAST:event_salesReturnButtonActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
       HistoryView historyView;
       historyView = new HistoryView("Purchase");
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(historyView);
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        // TODO add your handling code here:
        HistoryView historyView;
       historyView = new HistoryView("Sales");
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(historyView);
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        // TODO add your handling code here:
        HistoryView historyView;
       historyView = new HistoryView("SalesPreforma");
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(historyView);
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void History_MenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_History_MenuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_History_MenuActionPerformed

    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed
        // TODO add your handling code here:
        HistoryView historyView;
       historyView = new HistoryView("SalesReturn");
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(historyView);
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    private void jMenuItem15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem15ActionPerformed
        // TODO add your handling code here:
        HistoryView historyView;
       historyView = new HistoryView("PurchaseReturn");
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(historyView);
    }//GEN-LAST:event_jMenuItem15ActionPerformed

    private void jMenuItem17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem17ActionPerformed
        // TODO add your handling code here:
        HistoryView historyView;
       historyView = new HistoryView("Payment");
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(historyView);
    }//GEN-LAST:event_jMenuItem17ActionPerformed

    private void jMenuItem18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem18ActionPerformed
        // TODO add your handling code here:
        HistoryView historyView;
       historyView = new HistoryView("Receipt");
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(historyView);
    }//GEN-LAST:event_jMenuItem18ActionPerformed

    private void userMgtMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userMgtMenuItemActionPerformed
        UserAccountView userAccountView = new UserAccountView();
        jMainFrame.getContentPanel().removeAll();
        jMainFrame.getContentPanel().repaint();
        jMainFrame.getContentPanel().revalidate();
        jMainFrame.getContentPanel().add(userAccountView);
    }//GEN-LAST:event_userMgtMenuItemActionPerformed

    private void trialBalanceButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_trialBalanceButtonActionPerformed
        TrialBalanceView trialBalanceView = new TrialBalanceView();
        jMainFrame.getContentPanel().removeAll();
        jMainFrame.getContentPanel().repaint();
        jMainFrame.getContentPanel().revalidate();
        jMainFrame.getContentPanel().add(trialBalanceView);
    }//GEN-LAST:event_trialBalanceButtonActionPerformed

    private void PandLButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PandLButtonActionPerformed
        PLAccountView pLAccountView = new PLAccountView(false);
        jMainFrame.getContentPanel().removeAll();
        jMainFrame.getContentPanel().repaint();
        jMainFrame.getContentPanel().revalidate();
        jMainFrame.getContentPanel().add(pLAccountView);
    }//GEN-LAST:event_PandLButtonActionPerformed

    private void balanceSheetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_balanceSheetButtonActionPerformed
        PLAccountView pLAccountView = new PLAccountView(true);
        jMainFrame.getContentPanel().removeAll();
        jMainFrame.getContentPanel().repaint();
        jMainFrame.getContentPanel().revalidate();
        jMainFrame.getContentPanel().add(pLAccountView);
    }//GEN-LAST:event_balanceSheetButtonActionPerformed

    private void journalButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contraButton8ActionPerformed
    }//GEN-LAST:event_contraButton8ActionPerformed

    private void journalButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_journalButton1ActionPerformed
        // TODO add your handling code here:
        JournalEntryView createJournal=new JournalEntryView("Journal");
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(createJournal);
    }//GEN-LAST:event_journalButton1ActionPerformed

    private void journalMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_journalMenuItemActionPerformed
        // TODO add your handling code here:
        HistoryView historyView;
       historyView = new HistoryView("Journal");
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(historyView);
    }//GEN-LAST:event_journalMenuItemActionPerformed

    private void jMenuItem22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem22ActionPerformed
        // TODO add your handling code here:
        HistoryView historyView;
       historyView = new HistoryView("Contra");
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(historyView);
    }//GEN-LAST:event_jMenuItem22ActionPerformed

    private void jMenuItem19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem19ActionPerformed
        // TODO add your handling code here:
        StockMoveView stockMoveView=new StockMoveView(jMainFrame);
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(stockMoveView);
    }//GEN-LAST:event_jMenuItem19ActionPerformed

    private void jMenuItem23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem23ActionPerformed
        // TODO add your handling code here:
        HistoryView historyView;
       historyView = new HistoryView("CommercialInvoice");
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(historyView);
    }//GEN-LAST:event_jMenuItem23ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        PurchaseOrderView purchaseOrder = new PurchaseOrderView(jMainFrame);
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(purchaseOrder);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem27ActionPerformed
        // TODO add your handling code here:
        HistoryView historyView;
       historyView = new HistoryView("PurchaseOrder");
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(historyView);
    }//GEN-LAST:event_jMenuItem27ActionPerformed

    private void shipmentModeMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_shipmentModeMenuItemActionPerformed
        ShipmentModeView shipmentModeView=new ShipmentModeView();
        jDesktopPane.add(shipmentModeView);
        shipmentModeView.setVisible(true);  
        shipmentModeView.setFrameIcon(null);
        commonService.setInternalFrameCenter(this, shipmentModeView);
        commonService.removeInternalFrameTitleDropdown(shipmentModeView);
    }//GEN-LAST:event_shipmentModeMenuItemActionPerformed

    private void logOutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logOutButtonActionPerformed
        LogInView logInView = new LogInView();
        //jMenuBar1.setVisible(false);
        //remove all menu & button
        jMainFrame.removeMenuBar();
        jMainFrame.removeButtons();
        
        jMainFrame.mainPanel.removeAll();
        jMainFrame.mainPanel.repaint();
        jMainFrame.mainPanel.revalidate();
        jMainFrame.mainPanel.add(logInView);
    }//GEN-LAST:event_logOutButtonActionPerformed

    private void unitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_unitMenuItemActionPerformed
        UnitView unitView=new UnitView();
        jDesktopPane.add(unitView);
        unitView.setVisible(true);  
        unitView.setFrameIcon(null);
        commonService.setInternalFrameCenter(this, unitView);
        commonService.removeInternalFrameTitleDropdown(unitView);
    }//GEN-LAST:event_unitMenuItemActionPerformed

    private void splitCompanyMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_splitCompanyMenuItemActionPerformed
        SplitCompanyView splitCompanyView=new SplitCompanyView();
        jDesktopPane.add(splitCompanyView);
        splitCompanyView.setVisible(true);  
        splitCompanyView.setFrameIcon(null);
        commonService.setInternalFrameCenter(this, splitCompanyView);
        commonService.removeInternalFrameTitleDropdown(splitCompanyView);
    }//GEN-LAST:event_splitCompanyMenuItemActionPerformed

    private void trialBalanceItem24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_trialBalanceItem24ActionPerformed
         TrialBalanceView trialBalanceView = new TrialBalanceView();
        jMainFrame.getContentPanel().removeAll();
        jMainFrame.getContentPanel().repaint();
        jMainFrame.getContentPanel().revalidate();
        jMainFrame.getContentPanel().add(trialBalanceView);
    }//GEN-LAST:event_trialBalanceItem24ActionPerformed

    private void profitAndLossMenuItem25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_profitAndLossMenuItem25ActionPerformed
        PLAccountView pLAccountView = new PLAccountView(false);
        jMainFrame.getContentPanel().removeAll();
        jMainFrame.getContentPanel().repaint();
        jMainFrame.getContentPanel().revalidate();
        jMainFrame.getContentPanel().add(pLAccountView);
    }//GEN-LAST:event_profitAndLossMenuItem25ActionPerformed

    private void balanceSheetMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_balanceSheetMenuItemActionPerformed
        PLAccountView pLAccountView = new PLAccountView(true);
        jMainFrame.getContentPanel().removeAll();
        jMainFrame.getContentPanel().repaint();
        jMainFrame.getContentPanel().revalidate();
        jMainFrame.getContentPanel().add(pLAccountView);
    }//GEN-LAST:event_balanceSheetMenuItemActionPerformed

    private void purchaseMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_purchaseMenuItemActionPerformed
        PurchaseView purchase = new PurchaseView(jMainFrame);
        ContentPanel.removeAll();
        ContentPanel.repaint();
        ContentPanel.revalidate();
        ContentPanel.add(purchase);
    }//GEN-LAST:event_purchaseMenuItemActionPerformed

    private void homeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_homeButtonActionPerformed
        CommonService.removeJpanel(GlobalProperty.mainForm.getContentPanel());
    }//GEN-LAST:event_homeButtonActionPerformed

    private void dbSettingsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dbSettingsMenuItemActionPerformed
        DatabaseSettingsView databaseSettingsView=new DatabaseSettingsView();
        jDesktopPane.add(databaseSettingsView);
        databaseSettingsView.setVisible(true);  
        databaseSettingsView.setFrameIcon(null);
        commonService.setInternalFrameCenter(this, databaseSettingsView);
        commonService.removeInternalFrameTitleDropdown(databaseSettingsView);
    }//GEN-LAST:event_dbSettingsMenuItemActionPerformed

    public void removeMenuBar() {
        jMenuBar1.removeAll();
        jMenuBar1.revalidate();
    }
    public void removeButtons() {
        for(Component component: LeftMenuPanel.getComponents()){
            component.setVisible(false);
        }
    }

    //adding menu & buttons accoding to privileges
    public void addMenuAndButtons(List<Privileges> privileges) {
        //menu items
        jMenuBar1.add(File_Menu);
        for (Privileges privilege : privileges) {
            if (privilege.getName().equalsIgnoreCase("PurchaseMenuItem")) {
                jMenuBar1.add(Purchase_Menu);
            }else if (privilege.getName().equalsIgnoreCase("StockMenuItem")) {
                jMenuBar1.add(Stock_Menu);
            }else if (privilege.getName().equalsIgnoreCase("AccountsMenuItem")) {
                jMenuBar1.add(Accounts_Menu);
            }else if (privilege.getName().equalsIgnoreCase("OptionsMenuItem")) {
                jMenuBar1.add(Options_Menu);
            }else if (privilege.getName().equalsIgnoreCase("CompanyMenuItem")) {
                jMenuBar1.add(Company_Menu);
            }else if (privilege.getName().equalsIgnoreCase("HistoryMenuItem")) {
                jMenuBar1.add(History_Menu);
            }else if (privilege.getName().equalsIgnoreCase("UserMenuItem")) {
                jMenuBar1.add(User_Menu);
            }
            //buttons
            else if (privilege.getName().equalsIgnoreCase("PurchaseSidePanel")) {
                purchaseButton.setVisible(true);
            }else if (privilege.getName().equalsIgnoreCase("SalesPreformaSidePanel")) {
                salesPreformaButton.setVisible(true);
            }else if (privilege.getName().equalsIgnoreCase("PackingListSidePanel")) {
                packingListButton.setVisible(true);
            }else if (privilege.getName().equalsIgnoreCase("SalesSidePanel")) {
                salesButton.setVisible(true);
            }else if (privilege.getName().equalsIgnoreCase("PaymentSidePanel")) {
                paymentButton.setVisible(true);
            }else if (privilege.getName().equalsIgnoreCase("ReceiptSidePanel")) {
                receiptButton.setVisible(true);
            }else if (privilege.getName().equalsIgnoreCase("ContraSidePanel")) {
                contraButton.setVisible(true);
            }else if (privilege.getName().equalsIgnoreCase("JournalSidePanel")) {
                journalButton1.setVisible(true);
            }else if (privilege.getName().equalsIgnoreCase("PurchaseReturnsSidePanel")) {
                purchaseReturnButton.setVisible(true);
            }else if (privilege.getName().equalsIgnoreCase("SalesReturnsSidePanel")) {
                salesReturnButton.setVisible(true);
            }else if (privilege.getName().equalsIgnoreCase("TrialBalanceSidePanel")) {
                trialBalanceButton.setVisible(true);
            }else if (privilege.getName().equalsIgnoreCase("PLAccountSidePanel")) {
                PandLButton.setVisible(true);
            }else if (privilege.getName().equalsIgnoreCase("BalanceSheetSidePanel")) {
                balanceSheetButton.setVisible(true);
            }
        }
        
        logOutButton.setVisible(true);
        homeButton.setVisible(true);
      /*  jMenuBar1.add(Orders_Menu);
        jMenuBar1.add(MenuItem_Menu);
        jMenuBar1.add(Options_Menu);
        jMenuBar1.add(Company_Menu);
        jMenuBar1.add(History_Menu);
        jMenuBar1.add(User_Menu);*/
        jMenuBar1.revalidate();
    }
    /**
     * @param args the command line arguments
     */
    static MainForm jMainFrame=null;
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
       try{
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName()); 
        }catch(Exception e){
            e.printStackTrace();
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    HibernateUtil hibernateUtil=new HibernateUtil();
                    CommonService commonService=new CommonService();
  
                    
                    jMainFrame = new MainForm();
                    
                    jMainFrame.setVisible(true);
                   // jMainFrame.setJMenuBar(null);
                    jMainFrame.setExtendedState(MainForm.MAXIMIZED_BOTH);
                   
                    //remove menu by default                    
                    jMainFrame.removeMenuBar();

                    jMainFrame.removeButtons();
                    
                    //initial dataloading
                    LedgerGroupDAO ledgerGroupDAO=new LedgerGroupDAO();
                    UserDAO userDAO=new UserDAO();
                    PrivilegesDAO privilegesDAO=new PrivilegesDAO();                    
                    
                    if(privilegesDAO.findAll().size()==0 && ledgerGroupDAO.findAllByAsc().size()==0 
                            && userDAO.findAllByDesc().size()==0){
                        InitialDataService dataService=new InitialDataService();
                        dataService.loadLedgerGroupTable();
                        dataService.loadPrivilegesTable();
                        dataService.loadUserTable();
                        dataService.loadMultiCurrencyTable();
                    }
                    
                    //uncomment for log in page
                    LogInView logInView=new LogInView();
                    jMainFrame.mainPanel.removeAll();
                    jMainFrame.mainPanel.repaint();
                    jMainFrame.mainPanel.revalidate();
                    jMainFrame.mainPanel.add(logInView);
                    
                } catch (Exception e) {
                   log.error("MainForm run :",e);
                }
            }
        });
    }
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu Accounts_Menu;
    private javax.swing.JPanel CompanyHeader;
    private javax.swing.JMenu Company_Menu;
    private javax.swing.JPanel ContentPanel;
    private javax.swing.JMenu File_Menu;
    private javax.swing.JMenu History_Menu;
    private javax.swing.JMenuItem Inventory_MenuItem;
    private javax.swing.JPanel LeftMenuPanel;
    private javax.swing.JMenu Options_Menu;
    private javax.swing.JMenuItem Packing;
    private javax.swing.JButton PandLButton;
    private javax.swing.JMenu Purchase_Menu;
    private javax.swing.JMenuItem StockGroup_MenuItem;
    private javax.swing.JMenuItem StockSummary_MenuItem;
    private javax.swing.JMenu Stock_Menu;
    private javax.swing.JMenu User_Menu;
    private javax.swing.JButton balanceSheetButton;
    private javax.swing.JMenuItem balanceSheetMenuItem;
    private javax.swing.JMenuItem chargesMenuItem;
    private javax.swing.JLabel companyNameLabel;
    private javax.swing.JButton contraButton;
    private javax.swing.JLabel currentPeriodeLabel;
    private javax.swing.JMenuItem dbSettingsMenuItem;
    private javax.swing.JButton homeButton;
    private javax.swing.JLabel homePageLabel;
    private javax.swing.JDesktopPane jDesktopPane;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem17;
    private javax.swing.JMenuItem jMenuItem18;
    private javax.swing.JMenuItem jMenuItem19;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem21;
    private javax.swing.JMenuItem jMenuItem22;
    private javax.swing.JMenuItem jMenuItem23;
    private javax.swing.JMenuItem jMenuItem27;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JButton journalButton1;
    private javax.swing.JMenuItem journalMenuItem;
    private javax.swing.JMenuItem ledgerMenuItem;
    private javax.swing.JButton logOutButton;
    private javax.swing.JLabel logoLabel;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton packingListButton;
    private javax.swing.JButton paymentButton;
    private javax.swing.JMenuItem profitAndLossMenuItem25;
    private javax.swing.JButton purchaseButton;
    private javax.swing.JMenuItem purchaseMenuItem;
    private javax.swing.JButton purchaseReturnButton;
    private javax.swing.JButton receiptButton;
    private javax.swing.JButton salesButton;
    private javax.swing.JButton salesPreformaButton;
    private javax.swing.JButton salesReturnButton;
    private javax.swing.JMenuItem shipmentModeMenuItem;
    private javax.swing.JMenuItem splitCompanyMenuItem;
    private javax.swing.JMenuItem taxMenuItem;
    private javax.swing.JButton trialBalanceButton;
    private javax.swing.JMenuItem trialBalanceItem24;
    private javax.swing.JMenuItem unitMenuItem;
    private javax.swing.JMenuItem userMgtMenuItem;
    private javax.swing.JLabel userNameLabel;
    // End of variables declaration//GEN-END:variables

    public JDesktopPane getjDesktopPane() {
        return jDesktopPane;
    }
}
