/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.GodownDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.MultiCurrencyDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.dao.PurchaseOrderDAO;
import com.cloudsendsoft.inventory.dao.ShipmentModeDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.ItemService;
import com.cloudsendsoft.inventory.services.LedgerService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Godown;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.PurchaseOrder;
import com.cloudsendsoft.inventory.model.PurchaseOrderCharge;
import com.cloudsendsoft.inventory.model.PurchaseOrderItem;
import com.cloudsendsoft.inventory.model.PurchaseOrderTax;
import com.cloudsendsoft.inventory.model.PurchaseTax;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class PurchaseOrderView extends javax.swing.JPanel {

    /**
     * Creates new form PurchaseViewPanel
     */
    static double totalAmount = 0;
    double subTotal = 0.00;
    double grandTotal = 0.00;
    double netTotal = 0.00;
    double totDiscount = 0.00;
    static final Logger log = Logger.getLogger(PurchaseOrderView.class.getName());

    ItemDAO itemDAO = new ItemDAO();
    CommonService commonService = new CommonService();
    MainForm mainForm = null;
    LedgerService ledgerService = new LedgerService();
    TaxDAO taxDAO = new TaxDAO();
    GodownDAO godownDAO = new GodownDAO();
    ChargesDAO chargesDAO = new ChargesDAO();
    MultiCurrencyDAO multiCurrencyDAO = new MultiCurrencyDAO();
    ShipmentModeDAO shipmentModeDAO = new ShipmentModeDAO();
    PurchaseOrderDAO purchaseOrderDAO = new PurchaseOrderDAO();
    NumberProperty numberProperty = null;
    NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
    //global usage
    Ledger supplierLedger = null;
    double itemQty = 0;
    List<PurchaseOrderItem> purchaseOrderItemList = new ArrayList<PurchaseOrderItem>();
     List<PurchaseOrderItem> purchaseOrderItemListHistory = null;
    Item item = null;
    List<PurchaseOrderTax> purchaseOrderTaxList = new ArrayList<PurchaseOrderTax>();
    List<PurchaseOrderCharge> purchaseOrderChargesList = new ArrayList<PurchaseOrderCharge>();

    MultiCurrency currency = null;
    PurchaseOrder purchaseOrder = null;
    int check = 0;

    PurchaseOrderItem purchaseOrderItem = null;
    DefaultTableModel purchaseOrderItemTableModel = null;

    LedgerDAO ledgerDAO = new LedgerDAO();

    CRUDServices cRUDServices = new CRUDServices();

    public PurchaseOrderView(MainForm mainForm) {

        try {
            initComponents();

            commonService.setCurrentPeriodOnCalendar(billDateFormattedText);

            //table background color removed
            jScrollPane1.setOpaque(false);
            jScrollPane1.getViewport().setOpaque(false);

            billDateFormattedText.setDate(new java.util.Date());
            this.mainForm = mainForm;

            numberProperty = numberPropertyDAO.findInvoiceNo("PurchaseOrder");
            invoiceNumberText.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());

            //set table design
            purchaseOrderItemTable.setRowHeight(GlobalProperty.tableRowHieght);
            purchaseOrderItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                    null,
                    new String[]{"Sr. No.", "Item Code", "Description", "Godown", "Unit Price", "Qty.", "Total Amount"}
            ));
            purchaseOrderItemTableModel = (DefaultTableModel) purchaseOrderItemTable.getModel();
            CommonService.setWidthAsPercentages(purchaseOrderItemTable, .03, .10, .20, .08, .08, .08, .08);

            // Combo boxes initialization
            for (Tax tacs : taxDAO.findAll()) {
                taxComboBox.addItem(new ComboKeyValue((tacs.getName()), tacs));
            }

            for (Charges charges : chargesDAO.findAll()) {
                chargesComboBox.addItem(new ComboKeyValue(charges.getName(), charges));
            }

            for (MultiCurrency currency : multiCurrencyDAO.findAll()) {
                if (currency == null) {
                    this.currency = currency;
                }
                multiCurrencyComboBox.addItem(new ComboKeyValue(currency.getName(), currency));
            }

            for (ShipmentMode shipmentMode : shipmentModeDAO.findAll()) {
                shipmentComboBox.addItem(new ComboKeyValue(shipmentMode.getName(), shipmentMode));
            }

            //jTable rows & columns alignment
            ((DefaultTableCellRenderer) purchaseOrderItemTable.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);

            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(JLabel.CENTER);
            purchaseOrderItemTable.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
            purchaseOrderItemTable.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
            purchaseOrderItemTable.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
            purchaseOrderItemTable.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
            //jTable rows & columns alignment
            setFocusOrder();

        } catch (Exception e) {
            log.error("Purchase:", e);
        }

    }

    public void listPurchaseOrderItemHistory(PurchaseOrder purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
        this.supplierLedger = purchaseOrder.getSupplier();
        purchaseOrderItemList = purchaseOrder.getPurchaseOrderItems();
        purchaseOrderItemListHistory=new ArrayList<PurchaseOrderItem>();
        for (PurchaseOrderItem purchaseOrderItem : purchaseOrderItemList) {
            //copy sales items 
            PurchaseOrderItem purchaseOrderItem1=new PurchaseOrderItem();
            purchaseOrderItem1.setItem(purchaseOrderItem.getItem());
            purchaseOrderItem1.setQuantity(purchaseOrderItem.getQuantity());
            //purchaseOrderItem1.setQuantity1(purchaseOrderItem.getQuantity1());
            purchaseOrderItemListHistory.add(purchaseOrderItem1);
            
            
            purchaseOrderItem.setUnitPrice(purchaseOrderItem.getUnitPrice());
        }
        purchaseOrderTaxList = purchaseOrder.getPurchaseOrderTaxes();
        for (PurchaseOrderTax purchaseOrderTax : purchaseOrderTaxList) {
            purchaseOrderTax.setAmount(purchaseOrderTax.getAmount() / purchaseOrder.getCurrentCurrencyRate());
        }
        purchaseOrderChargesList = purchaseOrder.getPurchaseOrderCharges();
        for (PurchaseOrderCharge purchaseOrderCharge : purchaseOrderChargesList) {
            purchaseOrderCharge.setAmount(purchaseOrderCharge.getAmount() / purchaseOrder.getCurrentCurrencyRate());
        }
        billDateFormattedText.setDate(purchaseOrder.getBillDate());
        invoiceNumberText.setText(purchaseOrder.getInvoiceNumber());
        supplierText.setText(purchaseOrder.getSupplier().getName());
        deliveryDateFormattedText.setText(purchaseOrder.getDeliveryDate() + "");
        //paymentTermsText.setText(purchase.());
        addressTextArea.setText(purchaseOrder.getAddress());
        totDiscount = purchaseOrder.getDiscount() / purchaseOrder.getCurrentCurrencyRate();
        //currency=purchaseOrder.getMultiCurrency();
        saveButton.setText("Update");
        currency = purchaseOrder.getMultiCurrency();
        currency.setRate(purchaseOrder.getCurrentCurrencyRate());
        check = 1;

        int count = multiCurrencyComboBox.getItemCount();
        for (int i = 0; i < count; i++) {
            ComboKeyValue ckv = (ComboKeyValue) multiCurrencyComboBox.getItemAt(i);
            if (ckv.getKey().trim().equalsIgnoreCase(purchaseOrder.getMultiCurrency().getName())) {
                multiCurrencyComboBox.setSelectedItem(ckv);
            }
        }
        
        int count1 = shipmentComboBox.getItemCount();
        if(purchaseOrder.getShipmentMode()!=null)
        {
        for (int i = 0; i < count1; i++) {
                ComboKeyValue ckv = (ComboKeyValue) shipmentComboBox.getItemAt(i);
                if (ckv.getKey().trim().equalsIgnoreCase(purchaseOrder.getShipmentMode().getName())) {
                    shipmentComboBox.setSelectedItem(ckv);
                }
            }
        }
        fillPurchaseOrderItemTable();

    }

    void generateAllList(String billDate, String ledgerName, String invoiceNumber) {
        try {
            java.sql.Date invDate = new java.sql.Date(commonService.convertToDate(billDate).getTime());
            PurchaseOrder purchaseOrder = purchaseOrderDAO.findByDateInvNumSupplier(invDate, invoiceNumber, ledgerName);
            purchaseOrderItemList = purchaseOrder.getPurchaseOrderItems();
            purchaseOrderTaxList = purchaseOrder.getPurchaseOrderTaxes();
            purchaseOrderChargesList = purchaseOrder.getPurchaseOrderCharges();
            saveButton.setEnabled(false);
            newButton.setEnabled(false);
            cancelButton.setEnabled(false);
            addItemButton.setEnabled(false);
            itemRemoveButton.setEnabled(false);
            taxAddButton.setEnabled(false);
            chargesAddButton.setEnabled(false);
            chargesMinusButton.setEnabled(false);
            fillPurchaseOrderItemTable();
        } catch (Exception e) {
            log.error("generateAllList:", e);
        }
    }

    void setFocusOrder() {
        try {

            supplierText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    deliveryDateFormattedText.requestFocusInWindow();
                }
            });
            deliveryDateFormattedText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    multiCurrencyComboBox.requestFocusInWindow();
                }
            });
            
            multiCurrencyComboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    itemCodeText.requestFocusInWindow();
                }
            });

            itemCodeText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    itemQtyText.requestFocusInWindow();
                }
            });
            itemQtyText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    itemUnitPriceText.requestFocusInWindow();
                }
            });
            itemUnitPriceText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addItemButton.requestFocusInWindow();
                }
            });
            addItemButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    taxComboBox.requestFocusInWindow();
                }
            });
            taxComboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    taxAddButton.requestFocusInWindow();
                }
            });
            taxAddButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chargesComboBox.requestFocusInWindow();
                }
            });
            chargesComboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chargesText.requestFocusInWindow();
                }
            });
            chargesText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chargesAddButton.requestFocusInWindow();
                }
            });
            chargesAddButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    shipmentComboBox.requestFocusInWindow();
                }
            });
            shipmentComboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    discountText.requestFocusInWindow();
                }
            });
            discountText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    totDiscountAddButton.requestFocusInWindow();
                }
            });
            totDiscountAddButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveButton.requestFocusInWindow();
                }
            });

        } catch (Exception e) {
            log.error("setFocusOrder:",e);
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        bottomPanel = new javax.swing.JPanel();
        itemInfoPanel = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        itemDescriptionText = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        itemCodeText = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        addItemButton = new javax.swing.JButton();
        itemRemoveButton = new javax.swing.JButton();
        itemQtyText = new javax.swing.JTextField();
        itemUnitPriceText = new javax.swing.JFormattedTextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        totavailableQtyLabel = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        availableQtyLabel = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        itemWeightText = new javax.swing.JTextField();
        godownText = new javax.swing.JTextField();
        chargesPanel = new javax.swing.JPanel();
        chargesComboBox = new javax.swing.JComboBox();
        chargesText = new javax.swing.JFormattedTextField();
        chargesAddButton = new javax.swing.JButton();
        chargesMinusButton = new javax.swing.JButton();
        InvoiceDetailsPanel = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        addressTextArea = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        multiCurrencyComboBox = new javax.swing.JComboBox();
        jLabel16 = new javax.swing.JLabel();
        invoiceNumberText = new javax.swing.JTextField();
        deliveryDateFormattedText = new javax.swing.JFormattedTextField();
        supplierText = new javax.swing.JTextField();
        expiryDateChooser = new com.toedter.calendar.JDateChooser();
        billDateFormattedText = new com.toedter.calendar.JDateChooser();
        taxPanel = new javax.swing.JPanel();
        taxComboBox = new javax.swing.JComboBox();
        taxAddButton = new javax.swing.JButton();
        shipmentModePanel = new javax.swing.JPanel();
        shipmentComboBox = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        grandTotalPanel1 = new javax.swing.JPanel();
        grandTotalLabel = new javax.swing.JLabel();
        shipmentModePanel1 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        discountText = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        totDiscountAddButton = new javax.swing.JButton();
        netAmountLabel = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        tableBorderPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        purchaseOrderItemTable = new javax.swing.JTable();

        jTextField1.setText("jTextField1");

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        bottomPanel.setOpaque(false);

        itemInfoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Item Info"));
        itemInfoPanel.setOpaque(false);
        itemInfoPanel.setPreferredSize(new java.awt.Dimension(382, 179));

        jLabel10.setText("Unit Price");

        jLabel9.setText("Quantity");

        itemDescriptionText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemDescriptionTextKeyPressed(evt);
            }
        });

        jLabel7.setText("Item Code");

        itemCodeText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemCodeTextKeyPressed(evt);
            }
        });

        jLabel8.setText("Description");

        addItemButton.setText("+");
        addItemButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addItemButtonActionPerformed(evt);
            }
        });

        itemRemoveButton.setText("-");
        itemRemoveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemRemoveButtonActionPerformed(evt);
            }
        });

        itemQtyText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemQtyText.setText("0");
        itemQtyText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemQtyTextFocusGained(evt);
            }
        });
        itemQtyText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemQtyTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemQtyTextKeyTyped(evt);
            }
        });

        itemUnitPriceText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        itemUnitPriceText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemUnitPriceText.setText("0.00");
        itemUnitPriceText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemUnitPriceTextFocusGained(evt);
            }
        });
        itemUnitPriceText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemUnitPriceTextKeyTyped(evt);
            }
        });

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel6.setOpaque(false);
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Total Aval Quandity");
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel6.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 50, 120, 20));

        totavailableQtyLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        totavailableQtyLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel6.add(totavailableQtyLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 70, 90, 40));

        jLabel12.setText("Available Quantity:");
        jLabel12.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel6.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 100, 20));

        availableQtyLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        availableQtyLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel6.add(availableQtyLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 10, 50, 40));

        jLabel4.setText("Weight");

        jLabel11.setText("Godown");

        itemWeightText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemWeightText.setText("0");
        itemWeightText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemWeightTextFocusGained(evt);
            }
        });
        itemWeightText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemWeightTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemWeightTextKeyTyped(evt);
            }
        });

        godownText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        godownText.setText("0");
        godownText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                godownTextFocusGained(evt);
            }
        });
        godownText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                godownTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                godownTextKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout itemInfoPanelLayout = new javax.swing.GroupLayout(itemInfoPanel);
        itemInfoPanel.setLayout(itemInfoPanelLayout);
        itemInfoPanelLayout.setHorizontalGroup(
            itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(itemInfoPanelLayout.createSequentialGroup()
                        .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(14, 14, 14)))
                .addGap(10, 10, 10)
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(itemWeightText)
                    .addComponent(itemUnitPriceText, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                    .addComponent(itemQtyText)
                    .addComponent(itemDescriptionText)
                    .addComponent(itemCodeText)
                    .addComponent(godownText, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(26, 26, 26)
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(itemInfoPanelLayout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(itemInfoPanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(addItemButton, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(itemRemoveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(38, 38, 38))))
        );
        itemInfoPanelLayout.setVerticalGroup(
            itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(itemInfoPanelLayout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(16, 16, 16)
                        .addComponent(jLabel8)
                        .addGap(13, 13, 13)
                        .addComponent(jLabel9)
                        .addGap(10, 10, 10)
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(itemUnitPriceText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(17, 17, 17)
                        .addComponent(jLabel4)
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel11))
                            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(godownText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(itemInfoPanelLayout.createSequentialGroup()
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                                .addComponent(itemCodeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(itemDescriptionText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(addItemButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(itemRemoveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(7, 7, 7)
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                                .addComponent(itemQtyText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(38, 38, 38)
                                .addComponent(itemWeightText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );

        chargesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Charges"));
        chargesPanel.setOpaque(false);

        chargesComboBox.setEditable(true);
        chargesComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                chargesComboBoxMouseClicked(evt);
            }
        });
        chargesComboBox.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesComboBoxFocusGained(evt);
            }
        });

        chargesText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        chargesText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        chargesText.setText("0.00");
        chargesText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesTextFocusGained(evt);
            }
        });
        chargesText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                chargesTextKeyTyped(evt);
            }
        });

        chargesAddButton.setText("+");
        chargesAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesAddButtonActionPerformed(evt);
            }
        });

        chargesMinusButton.setText("-");
        chargesMinusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesMinusButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout chargesPanelLayout = new javax.swing.GroupLayout(chargesPanel);
        chargesPanel.setLayout(chargesPanelLayout);
        chargesPanelLayout.setHorizontalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, chargesPanelLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chargesComboBox, 0, 110, Short.MAX_VALUE)
                    .addComponent(chargesText))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        chargesPanelLayout.setVerticalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chargesPanelLayout.createSequentialGroup()
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chargesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chargesText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        InvoiceDetailsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Invoice Details"));
        InvoiceDetailsPanel.setOpaque(false);

        jLabel6.setText("Bill Date");

        jLabel5.setText("Invoice Number");

        jLabel13.setText("Supplier");

        jLabel14.setText("Delivery");

        jLabel15.setText("ExpiryDate:");

        addressTextArea.setColumns(20);
        addressTextArea.setLineWrap(true);
        addressTextArea.setRows(5);
        jScrollPane3.setViewportView(addressTextArea);

        jLabel1.setText("Address :");

        multiCurrencyComboBox.setEditable(true);
        multiCurrencyComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                multiCurrencyComboBoxMouseClicked(evt);
            }
        });
        multiCurrencyComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                multiCurrencyComboBoxItemStateChanged(evt);
            }
        });
        multiCurrencyComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                multiCurrencyComboBoxActionPerformed(evt);
            }
        });
        multiCurrencyComboBox.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                multiCurrencyComboBoxFocusGained(evt);
            }
        });

        jLabel16.setText("Multi Currency");

        invoiceNumberText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                invoiceNumberTextKeyPressed(evt);
            }
        });

        deliveryDateFormattedText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));

        supplierText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supplierTextActionPerformed(evt);
            }
        });
        supplierText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                supplierTextKeyPressed(evt);
            }
        });

        expiryDateChooser.setDateFormatString("dd/MM/yyyy");

        billDateFormattedText.setDateFormatString("dd/MM/yyyy");
        billDateFormattedText.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                billDateFormattedTextPropertyChange(evt);
            }
        });

        javax.swing.GroupLayout InvoiceDetailsPanelLayout = new javax.swing.GroupLayout(InvoiceDetailsPanel);
        InvoiceDetailsPanel.setLayout(InvoiceDetailsPanelLayout);
        InvoiceDetailsPanelLayout.setHorizontalGroup(
            InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(37, 37, 37))
                            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(10, 10, 10)))
                        .addGap(10, 10, 10))
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(invoiceNumberText, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                    .addComponent(deliveryDateFormattedText)
                    .addComponent(supplierText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                    .addComponent(billDateFormattedText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(23, 23, 23))
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(4, 4, 4)
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(multiCurrencyComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE)
                    .addComponent(expiryDateChooser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        InvoiceDetailsPanelLayout.setVerticalGroup(
            InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(multiCurrencyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16))
                        .addGap(12, 12, 12)
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(billDateFormattedText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(12, 12, 12)
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(invoiceNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addGap(16, 16, 16)
                                .addComponent(jLabel13))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(supplierText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(2, 2, 2)))))
                .addGap(9, 9, 9)
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(deliveryDateFormattedText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.TRAILING))
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(expiryDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        taxPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Tax"));
        taxPanel.setOpaque(false);

        taxComboBox.setEditable(true);
        taxComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                taxComboBoxMouseClicked(evt);
            }
        });
        taxComboBox.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
                taxComboBoxPopupMenuWillBecomeVisible(evt);
            }
        });
        taxComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                taxComboBoxItemStateChanged(evt);
            }
        });
        taxComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                taxComboBoxActionPerformed(evt);
            }
        });
        taxComboBox.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                taxComboBoxFocusGained(evt);
            }
        });

        taxAddButton.setText("+");
        taxAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                taxAddButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout taxPanelLayout = new javax.swing.GroupLayout(taxPanel);
        taxPanel.setLayout(taxPanelLayout);
        taxPanelLayout.setHorizontalGroup(
            taxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(taxPanelLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(taxComboBox, 0, 108, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(taxAddButton)
                .addContainerGap())
        );
        taxPanelLayout.setVerticalGroup(
            taxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(taxPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(taxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(taxAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(taxComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        shipmentModePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        shipmentModePanel.setOpaque(false);

        shipmentComboBox.setEditable(true);
        shipmentComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                shipmentComboBoxMouseClicked(evt);
            }
        });
        shipmentComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                shipmentComboBoxActionPerformed(evt);
            }
        });
        shipmentComboBox.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                shipmentComboBoxFocusGained(evt);
            }
        });

        jLabel3.setText("Shipment Mode");

        javax.swing.GroupLayout shipmentModePanelLayout = new javax.swing.GroupLayout(shipmentModePanel);
        shipmentModePanel.setLayout(shipmentModePanelLayout);
        shipmentModePanelLayout.setHorizontalGroup(
            shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, shipmentModePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(shipmentComboBox, 0, 109, Short.MAX_VALUE)
                .addContainerGap())
        );
        shipmentModePanelLayout.setVerticalGroup(
            shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(shipmentModePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(shipmentComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        grandTotalPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Grand Total", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N
        grandTotalPanel1.setOpaque(false);
        grandTotalPanel1.setLayout(new java.awt.BorderLayout());

        grandTotalLabel.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        grandTotalLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        grandTotalLabel.setText("0.00");
        grandTotalLabel.setFocusable(false);
        grandTotalLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        grandTotalPanel1.add(grandTotalLabel, java.awt.BorderLayout.CENTER);

        shipmentModePanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        shipmentModePanel1.setOpaque(false);

        jLabel17.setText("Discount");

        discountText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                discountTextActionPerformed(evt);
            }
        });
        discountText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                discountTextFocusGained(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel18.setText("Net Amount");

        totDiscountAddButton.setText("+");
        totDiscountAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totDiscountAddButtonActionPerformed(evt);
            }
        });

        netAmountLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        javax.swing.GroupLayout shipmentModePanel1Layout = new javax.swing.GroupLayout(shipmentModePanel1);
        shipmentModePanel1.setLayout(shipmentModePanel1Layout);
        shipmentModePanel1Layout.setHorizontalGroup(
            shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(shipmentModePanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(shipmentModePanel1Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(netAmountLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(shipmentModePanel1Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(discountText, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(totDiscountAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        shipmentModePanel1Layout.setVerticalGroup(
            shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, shipmentModePanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(shipmentModePanel1Layout.createSequentialGroup()
                        .addGap(0, 7, Short.MAX_VALUE)
                        .addComponent(jLabel18))
                    .addComponent(netAmountLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(discountText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totDiscountAddButton))
                .addContainerGap())
        );

        jPanel1.setLayout(new java.awt.GridLayout(1, 0));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("New");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        jPanel1.add(newButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveButton.setText("<HTML><U>S</U>ave<HTML>");
        saveButton.setToolTipText("");
        saveButton.setOpaque(false);
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        jPanel1.add(saveButton);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/cancel.png"))); // NOI18N
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        jPanel1.add(cancelButton);

        javax.swing.GroupLayout bottomPanelLayout = new javax.swing.GroupLayout(bottomPanel);
        bottomPanel.setLayout(bottomPanelLayout);
        bottomPanelLayout.setHorizontalGroup(
            bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bottomPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(InvoiceDetailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(itemInfoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 389, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chargesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(taxPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(shipmentModePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(grandTotalPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(shipmentModePanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(58, 58, 58))
        );
        bottomPanelLayout.setVerticalGroup(
            bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bottomPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bottomPanelLayout.createSequentialGroup()
                        .addComponent(taxPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(chargesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(shipmentModePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(bottomPanelLayout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(shipmentModePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(grandTotalPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, bottomPanelLayout.createSequentialGroup()
                            .addComponent(InvoiceDetailsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(itemInfoPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(bottomPanel, java.awt.BorderLayout.PAGE_END);

        tableBorderPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Purchase Order"));
        tableBorderPanel.setOpaque(false);

        jScrollPane1.setName(""); // NOI18N
        jScrollPane1.setOpaque(false);

        purchaseOrderItemTable.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        purchaseOrderItemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        purchaseOrderItemTable.setOpaque(false);
        purchaseOrderItemTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                purchaseOrderItemTableMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                purchaseOrderItemTableMouseEntered(evt);
            }
        });
        jScrollPane1.setViewportView(purchaseOrderItemTable);

        javax.swing.GroupLayout tableBorderPanelLayout = new javax.swing.GroupLayout(tableBorderPanel);
        tableBorderPanel.setLayout(tableBorderPanelLayout);
        tableBorderPanelLayout.setHorizontalGroup(
            tableBorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tableBorderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1372, Short.MAX_VALUE)
                .addContainerGap())
        );
        tableBorderPanelLayout.setVerticalGroup(
            tableBorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tableBorderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE))
        );

        add(tableBorderPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void itemCodeTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemCodeTextKeyPressed
        try {
            ItemService itemService = new ItemService();
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                String productId = itemCodeText.getText().trim();
                List<Item> items = itemService.populatePurchaseOrderPopupTableByProductId(productId, popupTableDialog, purchaseOrderItemList,currency,purchaseOrderItemListHistory);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();

                itemQtyText.requestFocusInWindow();

                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    item = items.get(index);
                    itemCodeText.setText(item.getItemCode());
                    itemDescriptionText.setText(item.getDescription());
                    availableQtyLabel.setText(Double.toString(item.getAvailableQty()));
                    itemWeightText.setText(Double.toString(item.getWeight()));
                    godownText.setText(item.getGodown().getName());
                    itemQty = itemDAO.findTotItemQty(item.getItemCode());
                    totavailableQtyLabel.setText(itemQty + "");
                } else {
                    MsgBox.warning("No Item Selected.");
                    itemQtyText.requestFocusInWindow();
                }
            }
        } catch (Exception e) {
            log.error("productIdTextKeyPressed:", e);
        }
    }//GEN-LAST:event_itemCodeTextKeyPressed

    void fillPurchaseOrderItemTable() {
        try {
            //boolean flag = false;

            purchaseOrderItemTableModel.setRowCount(0);
            //BigDecimal grandTotal = new BigDecimal(0.00) ;
            //grandTotal.setScale(1);
            subTotal = 0.00;
            netTotal = 0.00;

            int slNo = 1;
            String currencySymbol = (null == currency) ? "" : currency.getSymbol();

            //{"Sr. No.", "Item Code","Description", "Unit Price", "Qty.", "Total Amount"}
            for (PurchaseOrderItem pItem : purchaseOrderItemList) {
                double itemTotal = 0;
                if (purchaseOrder == null) {
                    itemTotal = ((pItem.getUnitPrice() / currency.getRate())) * pItem.getQuantity();
                    purchaseOrderItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getItem().getGodown().getName(), currency.getSymbol() + " " + (pItem.getUnitPrice() / currency.getRate()), pItem.getQuantity(), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal)});

                } else {
                    itemTotal = ((pItem.getUnitPrice() / currency.getRate()) * pItem.getQuantity());
                    purchaseOrderItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getItem().getGodown().getName(), currency.getSymbol() + " " + (pItem.getUnitPrice() / currency.getRate()), pItem.getQuantity(), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal)});

                }
                //purchaseItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getItem().getGodown().getName(),currency.getSymbol() + ". " + pItem.getUnitPrice()/purchase.getCurrentCurrencyRate(), pItem.getQuantity(), currency.getSymbol() + " " + itemTotal});
                //grandTotal.add(BigDecimal.pa itemTotal);
                subTotal += itemTotal;
                netTotal = subTotal;
            }

            if (subTotal > 0) {
                for (PurchaseOrderTax purchaseOrderTax : purchaseOrderTaxList) {
                    /*if (!flag) {
                     purchaseOrderItemTableModel.addRow(new Object[]{null});
                     flag = true;
                     }*/
                    //double taxTotal = subTotal * (purchaseTax.getTax().getTaxRate() / 100);
                    //if (purchaseOrder == null) {
                    purchaseOrderItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseOrderTax.getTax().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(purchaseOrderTax.getAmount()/currency.getRate())});
                    netTotal += (purchaseOrderTax.getAmount());
//                    } else {
//                        purchaseOrderItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseOrderTax.getTax().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(purchaseOrderTax.getAmount() / purchaseOrder.getCurrentCurrencyRate())});
//                        netTotal += (purchaseOrderTax.getAmount() / purchaseOrder.getCurrentCurrencyRate());
//                    }
                }
                // flag = false;
                for (PurchaseOrderCharge purchaseOrderCharge : purchaseOrderChargesList) {
                    //double chargesTotal = Double.parseDouble(chargesText.getText());
                    /*if (!flag) {
                     purchaseOrderItemTableModel.addRow(new Object[]{null});
                     flag = true;
                     }*/
                    //if (purchaseOrder == null) {
                    purchaseOrderItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseOrderCharge.getCharges().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(purchaseOrderCharge.getAmount())});
                    netTotal += (purchaseOrderCharge.getAmount());
//                    } else {
//                        purchaseOrderItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseOrderCharge.getCharges().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(purchaseOrderCharge.getAmount() / purchaseOrder.getCurrentCurrencyRate())});
//                        netTotal += (purchaseOrderCharge.getAmount() / purchaseOrder.getCurrentCurrencyRate());
//                    }
                }
            }
            grandTotal = netTotal - totDiscount;
            if (totDiscount > 0) {

                purchaseOrderItemTableModel.addRow(new Object[]{"", "", "Discount", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(totDiscount)});
            }
            netAmountLabel.setText(currencySymbol + " " + commonService.formatIntoCurrencyAsString(netTotal));
            grandTotalLabel.setText(currencySymbol + "" + commonService.formatIntoCurrencyAsString(grandTotal));

            //clear fields
            CommonService.clearCurrencyFields(new JTextField[]{itemUnitPriceText});
            CommonService.clearNumberFields(new JTextField[]{itemQtyText, itemWeightText});
            CommonService.clearTextFields(new JTextField[]{itemCodeText, itemDescriptionText, godownText});
            totavailableQtyLabel.setText("");
            availableQtyLabel.setText("");

        } catch (Exception e) {
            log.error("fillPurchaseItemTable:", e);
        }

    }

    private void addItemButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addItemButtonActionPerformed

        try {
            if (CommonService.stringValidator(new String[]{itemCodeText.getText(), itemDescriptionText.getText()})) {
                double itemQty = Double.parseDouble(itemQtyText.getText());
                double itemUnitPrice = Double.parseDouble(itemUnitPriceText.getText());

                PurchaseOrderItem purchaseOrderItem = null;
                for (PurchaseOrderItem pItem : purchaseOrderItemList) {
                    if (pItem.getItem().getItemCode().equalsIgnoreCase(item.getItemCode()) && pItem.getItem().getGodown().getName().equalsIgnoreCase(item.getGodown().getName())) {
                        purchaseOrderItem = pItem;
                        break;
                    }
                }

                if (null == purchaseOrderItem) {
                    purchaseOrderItem = new PurchaseOrderItem();
                    purchaseOrderItemList.add(purchaseOrderItem);
                }

                purchaseOrderItem.setItem(item);
                purchaseOrderItem.setQuantity(itemQty);
                purchaseOrderItem.setUnitPrice(Double.parseDouble(itemUnitPriceText.getText())*currency.getRate());
                purchaseOrderItem.setDiscount(0.00);
                fillPurchaseOrderItemTable();

            } else {
                MsgBox.warning("No Item Selected.");
            }
        } catch (Exception e) {
            log.error("addItemButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_addItemButtonActionPerformed

    private void itemRemoveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemRemoveButtonActionPerformed
        if (null != purchaseOrderItem) {
            purchaseOrderItemList.remove(purchaseOrderItem);
            fillPurchaseOrderItemTable();
            purchaseOrderItem = null;
        }
    }//GEN-LAST:event_itemRemoveButtonActionPerformed

    private void invoiceNumberTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_invoiceNumberTextKeyPressed

    }//GEN-LAST:event_invoiceNumberTextKeyPressed

    private void itemQtyTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyTextFocusGained
        itemQtyText.selectAll();
    }//GEN-LAST:event_itemQtyTextFocusGained

    private void itemQtyTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyTextKeyPressed

    }//GEN-LAST:event_itemQtyTextKeyPressed

    private void itemQtyTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyTextKeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_itemQtyTextKeyTyped

    private void itemUnitPriceTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemUnitPriceTextFocusGained
        itemUnitPriceText.selectAll();
    }//GEN-LAST:event_itemUnitPriceTextFocusGained

    private void itemUnitPriceTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemUnitPriceTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_itemUnitPriceTextKeyTyped

    private void chargesTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesTextFocusGained
        chargesText.selectAll();
    }//GEN-LAST:event_chargesTextFocusGained

    private void chargesTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chargesTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_chargesTextKeyTyped

    private void chargesAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesAddButtonActionPerformed

        try {
            Object chargesItem = chargesComboBox.getSelectedItem();
            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();
            double chargeAmt = Double.parseDouble(chargesText.getText());
            if (Double.parseDouble(chargesText.getText()) > 0 && !charges.getName().equalsIgnoreCase("N/A")) {
                PurchaseOrderCharge purchaseOrderCharge = new PurchaseOrderCharge();
                for (PurchaseOrderCharge purchaseOrderChrg : purchaseOrderChargesList) {
                    if (charges.getName().equalsIgnoreCase(purchaseOrderChrg.getCharges().getName())) {
                        purchaseOrderChargesList.remove(purchaseOrderChrg);
                    }
                }
                Iterator<PurchaseOrderCharge> iter = purchaseOrderChargesList.iterator();
                while (iter.hasNext()) {
                    if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                        iter.remove();
                    }
                }
                purchaseOrderCharge.setCharges(charges);
                purchaseOrderCharge.setAmount(chargeAmt);
                purchaseOrderChargesList.add(purchaseOrderCharge);
                chargesComboBox.setSelectedIndex(0);
                chargesText.setText("0.00");
                fillPurchaseOrderItemTable();
            }

        } catch (Exception e) {
            log.error("chargesAddButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesAddButtonActionPerformed

    private void itemDescriptionTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemDescriptionTextKeyPressed
        try {
            ItemService itemService = new ItemService();
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                String itemName = itemDescriptionText.getText().trim();
                List<Item> items = itemService.populatePurchaseOrderPopupTableByItemName(itemName, popupTableDialog, purchaseOrderItemList, currency,purchaseOrderItemListHistory);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();

                itemQtyText.requestFocusInWindow();

                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    item = items.get(index);
                    itemCodeText.setText(item.getItemCode());
                    itemDescriptionText.setText(item.getDescription());
                    availableQtyLabel.setText(Double.toString(item.getAvailableQty()));
                    itemWeightText.setText(Double.toString(item.getWeight()));
                    godownText.setText(item.getGodown().getName());
                    itemQty = itemDAO.findTotItemQty(item.getItemCode());
                    totavailableQtyLabel.setText(itemQty + "");
                } else {
                    MsgBox.warning("No Item Selected.");
                    itemDescriptionText.requestFocusInWindow();
                }
            }
        } catch (Exception e) {
            log.error("itemNameTextKeyPressed:", e);
        }
    }//GEN-LAST:event_itemDescriptionTextKeyPressed

    private void multiCurrencyComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_multiCurrencyComboBoxItemStateChanged
//        System.err.println("Propert changed");
        Object currencyItem = multiCurrencyComboBox.getSelectedItem();

        if (((ComboKeyValue) currencyItem) != null) {
            MultiCurrency tempCurrency = (MultiCurrency) ((ComboKeyValue) currencyItem).getValue();
            if (purchaseOrder != null) {
                if (purchaseOrder.getMultiCurrency().getId() != tempCurrency.getId()) {
                    currency = (MultiCurrency) ((ComboKeyValue) currencyItem).getValue();
                }
            } else {
                currency = (MultiCurrency) ((ComboKeyValue) currencyItem).getValue();
            }
            System.out.println("currency,,,," + currency.getSymbol() + currency.getRate());
            fillPurchaseOrderItemTable();
        }

    }//GEN-LAST:event_multiCurrencyComboBoxItemStateChanged

    private void purchaseOrderItemTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purchaseOrderItemTableMouseClicked
        try {
            ArrayList row = CommonService.getTableRowData(purchaseOrderItemTable);
            String itemCode = row.get(1) + "";

            if (itemCode.trim().length() > 0) {
                purchaseOrderItem = purchaseOrderItemList.get(Integer.parseInt(row.get(0) + "") - 1);
                item=purchaseOrderItem.getItem();
                itemCodeText.setText(purchaseOrderItem.getItem().getItemCode());
                itemDescriptionText.setText(purchaseOrderItem.getItem().getDescription());
                itemQtyText.setText(purchaseOrderItem.getQuantity() + "");
                if (purchaseOrder == null) {
                    itemUnitPriceText.setText(purchaseOrderItem.getUnitPrice()/currency.getRate() + "");
                } else {
                    itemUnitPriceText.setText(purchaseOrderItem.getUnitPrice() / purchaseOrder.getCurrentCurrencyRate() + "");
                }
                totavailableQtyLabel.setText(purchaseOrderItem.getItem().getAvailableQty() + "");
                godownText.setText(purchaseOrderItem.getItem().getGodown().getName());

            } else {
                String itemDesc = row.get(2) + "";
                itemDesc = itemDesc.trim();
                System.err.println("itemDesc" + itemDesc);
                int count = taxComboBox.getItemCount();

                if (itemDesc.equalsIgnoreCase("discount")) {
                    totDiscountAddButton.setText("-");
                }

                for (int i = 0; i < count; i++) {
                    ComboKeyValue ckv = (ComboKeyValue) taxComboBox.getItemAt(i);
                    System.err.println("tax.getName()" + ckv.getKey());
                    if (ckv.getKey().trim().equalsIgnoreCase(itemDesc)) {
                        taxComboBox.setSelectedItem(ckv);
                        taxAddButton.setText("-");
                    }
                }

                count = chargesComboBox.getItemCount();
                for (int i = 0; i < count; i++) {
                    ComboKeyValue ckv = (ComboKeyValue) chargesComboBox.getItemAt(i);
                    if (ckv.getKey().trim().equalsIgnoreCase(itemDesc)) {
                        chargesComboBox.setSelectedItem(ckv);
                        chargesText.setText(row.get(6) + "");
                    }
                }
            }

        } catch (Exception e) {
            log.error("purchaseItemTableMouseClicked:", e);
        }
    }//GEN-LAST:event_purchaseOrderItemTableMouseClicked

    private void taxComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_taxComboBoxItemStateChanged


    }//GEN-LAST:event_taxComboBoxItemStateChanged

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        CommonService.removeJpanel(mainForm.getContentPanel());
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void chargesMinusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesMinusButtonActionPerformed
        try {
            Object chargesItem = chargesComboBox.getSelectedItem();
            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();
            Iterator<PurchaseOrderCharge> iter = purchaseOrderChargesList.iterator();
            while (iter.hasNext()) {
                if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                    iter.remove();
                }
            }
            chargesComboBox.setSelectedIndex(0);
            chargesText.setText("0.00");
            fillPurchaseOrderItemTable();

        } catch (Exception e) {
            log.error("chargesMinusButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesMinusButtonActionPerformed

    private void taxAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_taxAddButtonActionPerformed
        Object taxItem = taxComboBox.getSelectedItem();
        Tax tax = (Tax) ((ComboKeyValue) taxItem).getValue();

        if (tax.getTaxRate() > 0 && subTotal > 0 && !tax.getName().equalsIgnoreCase("N/A")) {
            if (taxAddButton.getText().trim().equalsIgnoreCase("+")) {
                PurchaseOrderTax purchaseOrderTax = new PurchaseOrderTax();
                Iterator<PurchaseOrderTax> iter = purchaseOrderTaxList.iterator();
                while (iter.hasNext()) {
                    if (tax.getName().equalsIgnoreCase(iter.next().getTax().getName())) {
                        iter.remove();
                    }
                }
                purchaseOrderTax.setTax(tax);
                purchaseOrderTax.setTaxRate(tax.getTaxRate());
                double taxTotal = (subTotal * tax.getTaxRate()) / 100;
                purchaseOrderTax.setAmount(taxTotal);
                purchaseOrderTaxList.add(purchaseOrderTax);
                taxComboBox.setSelectedIndex(0);
            } else {
                Iterator<PurchaseOrderTax> iter = purchaseOrderTaxList.iterator();
                while (iter.hasNext()) {
                    if (tax.getName().equalsIgnoreCase(iter.next().getTax().getName())) {
                        iter.remove();
                    }
                }
                taxAddButton.setText("+");
            }

            fillPurchaseOrderItemTable();
        }
        taxComboBox.setSelectedIndex(0);
    }//GEN-LAST:event_taxAddButtonActionPerformed

    private void purchaseOrderItemTableMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purchaseOrderItemTableMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_purchaseOrderItemTableMouseEntered

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        try {
            
            if (billDateFormattedText.getDate() != null && supplierLedger != null && purchaseOrderItemList != null) {
                if (MsgBox.confirm("Are you sure you want to save?")) {
                    if (purchaseOrder == null) {
                        check = 0;
                        purchaseOrder = new PurchaseOrder();
                    }
                    purchaseOrder.setBillDate(commonService.utilDateToSqlDate(billDateFormattedText.getDate()));
                    purchaseOrder.setInvoiceNumber(invoiceNumberText.getText());
                    purchaseOrder.setAddress(addressTextArea.getText().trim());
                    purchaseOrder.setSupplier(supplierLedger);
                    purchaseOrder.setDeliveryDate((deliveryDateFormattedText.getText()));
                    purchaseOrder.setMultiCurrency(currency);
                    purchaseOrder.setExpiryDate(commonService.jDateChooserToSqlDate(expiryDateChooser));
                    Object shipmentMod = shipmentComboBox.getSelectedItem();
                    ShipmentMode shipmentMode = (ShipmentMode) ((ComboKeyValue) shipmentMod).getValue();
                    if (shipmentMode.getName().equalsIgnoreCase("N/A")) {
                        purchaseOrder.setShipmentMode(null);
                    }else{
                        purchaseOrder.setShipmentMode(shipmentMode);
                    }
                    purchaseOrder.setDiscount(totDiscount * currency.getRate());
                    for (PurchaseOrderItem purchaseOrderItem : purchaseOrderItemList) {
                        purchaseOrderItem.setUnitPrice(purchaseOrderItem.getUnitPrice());
                    }
                    purchaseOrder.setPurchaseOrderItems(purchaseOrderItemList);
                    for (PurchaseOrderTax purchaseOrderTax : purchaseOrderTaxList) {
                        purchaseOrderTax.setAmount(purchaseOrderTax.getAmount() * currency.getRate());
                    }
                    purchaseOrder.setPurchaseOrderTaxes(purchaseOrderTaxList);
                    for (PurchaseOrderCharge purchaseOrderCharge : purchaseOrderChargesList) {
                        purchaseOrderCharge.setAmount(purchaseOrderCharge.getAmount() * currency.getRate());
                    }
                    purchaseOrder.setPurchaseOrderCharges(purchaseOrderChargesList);
                    purchaseOrder.setCurrentCurrencyRate(currency.getRate());
                    purchaseOrder.setGrandTotal(grandTotal * currency.getRate());
                    purchaseOrder.setCompany(GlobalProperty.getCompany());
                    //if (check == 0) {
                    //    cRUDServices.saveModel(purchaseOrder);
                    // } else {
                    cRUDServices.saveOrUpdateModel(purchaseOrder);
                    //}
                    //Adding available quantity on each item
//                for(PurchaseOrderItem pi:purchaseOrderItemList){
//                    Item item=pi.getItem();
//                    item.setAvailableQty(item.getAvailableQty()+pi.getQuantity());
//                    CRUDServices.saveOrUpdateModel(item);
//                }
//                //Updating Purchase Ledger available balance
//                Ledger ledger=ledgerDAO.findByLedgerName("Purchase Account");
//                //MsgBox.warning("oooooooooooooooooooooooooooooooooooooooooooooooo"+ledger.getLedgerName());
//                ledger.setAvailableBalance(ledger.getAvailableBalance()+subTotal);
//                CRUDServices.saveOrUpdateModel(ledger);
//                
//                ledger=ledgerDAO.findByLedgerName("Discount Received");
//                ledger.setAvailableBalance(ledger.getAvailableBalance()+totDiscount);
//                CRUDServices.saveOrUpdateModel(ledger);
                    numberProperty.setNumber(numberProperty.getNumber() + 1);
                    cRUDServices.saveOrUpdateModel(numberProperty);

                    //tax
//                for (PurchaseTax purchaseTax : purchaseTaxList) {
//                    ledger=ledgerDAO.findByLedgerName(purchaseTax.getTax().getName());
//                    ledger.setAvailableBalance(ledger.getAvailableBalance()+((purchaseTax.getAmount()*purchaseTax.getTaxRate())/100));
//                    CRUDServices.saveOrUpdateModel(ledger);
//                }
//                //charges
//                for (PurchaseCharge purchaseCharge : purchaseChargesList) {
//                    ledger=ledgerDAO.findByLedgerName(purchaseCharge.getCharges().getName());
//                    ledger.setAvailableBalance(ledger.getAvailableBalance()+purchaseCharge.getAmount());
//                    CRUDServices.saveOrUpdateModel(ledger);
//
//                }
                    MsgBox.success("PurchaseOrder Invoice Saved Successfully");
                    //clearAllFields();
                }
            } else {
                MsgBox.warning("Required fields are not entered");
            }
            listPurchaseOrderItemHistory(purchaseOrder);
            
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(ex);
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void supplierTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_supplierTextKeyPressed
        try {

            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                //String productId = productIdText.getText().trim();
                List<Ledger> suppliers = ledgerService.populatePopupTableBySupplier(supplierText.getText().trim(), popupTableDialog);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();
                System.out.println("Sang:" + selectedRow);
                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    supplierLedger = suppliers.get(index);
                    supplierText.setText(supplierLedger.getLedgerName());
                    addressTextArea.setText(supplierLedger.getAddress());
                    //salesTaxText.setText(supplierLedger.getSalesTaxNo());
                }
            }

        } catch (Exception e) {
            log.error("supplierTextKeyPressed:", e);
        }
    }//GEN-LAST:event_supplierTextKeyPressed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        clearAllFields();
        
    }//GEN-LAST:event_newButtonActionPerformed

    private void itemWeightTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemWeightTextFocusGained
        itemWeightText.selectAll();
    }//GEN-LAST:event_itemWeightTextFocusGained

    private void itemWeightTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemWeightTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemWeightTextKeyPressed

    private void itemWeightTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemWeightTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemWeightTextKeyTyped

    private void godownTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_godownTextFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_godownTextFocusGained

    private void godownTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_godownTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_godownTextKeyPressed

    private void godownTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_godownTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_godownTextKeyTyped

    private void taxComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_taxComboBoxActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_taxComboBoxActionPerformed

    private void supplierTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supplierTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_supplierTextActionPerformed

    private void discountTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_discountTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_discountTextActionPerformed

    private void totDiscountAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totDiscountAddButtonActionPerformed
        // TODO add your handling code here:
        if (totDiscountAddButton.getText().trim().equalsIgnoreCase("+")) {
            totDiscount = Double.parseDouble(discountText.getText().trim());
        } else {
            totDiscount = 0;
            discountText.setText("0.00");
            totDiscountAddButton.setText("+");
        }

        fillPurchaseOrderItemTable();
    }//GEN-LAST:event_totDiscountAddButtonActionPerformed

    private void taxComboBoxFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_taxComboBoxFocusGained
        // TODO add your handling code here:
        //taxComboBox.setSelectedIndex(0);
        //taxComboBox.removeAllItems();

    }//GEN-LAST:event_taxComboBoxFocusGained

    private void chargesComboBoxFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesComboBoxFocusGained
        // TODO add your handling code here:

    }//GEN-LAST:event_chargesComboBoxFocusGained

    private void multiCurrencyComboBoxFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_multiCurrencyComboBoxFocusGained
        // TODO add your handling code here:

    }//GEN-LAST:event_multiCurrencyComboBoxFocusGained

    private void shipmentComboBoxFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_shipmentComboBoxFocusGained
        // TODO add your handling code here:

    }//GEN-LAST:event_shipmentComboBoxFocusGained

    private void taxComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_taxComboBoxMouseClicked
        // TODO add your handling code here:
        taxComboBox.removeAllItems();
        for (Tax tacs : taxDAO.findAll()) {
            taxComboBox.addItem(new ComboKeyValue((tacs.getName()), tacs));
        }
    }//GEN-LAST:event_taxComboBoxMouseClicked

    private void taxComboBoxPopupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_taxComboBoxPopupMenuWillBecomeVisible
        // TODO add your handling code here:

    }//GEN-LAST:event_taxComboBoxPopupMenuWillBecomeVisible

    private void shipmentComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_shipmentComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_shipmentComboBoxActionPerformed

    private void shipmentComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_shipmentComboBoxMouseClicked
        // TODO add your handling code here:
        shipmentComboBox.removeAllItems();
        for (ShipmentMode shipmentMode : shipmentModeDAO.findAll()) {
            shipmentComboBox.addItem(new ComboKeyValue(shipmentMode.getName(), shipmentMode));
        }
    }//GEN-LAST:event_shipmentComboBoxMouseClicked

    private void multiCurrencyComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_multiCurrencyComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_multiCurrencyComboBoxActionPerformed

    private void multiCurrencyComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_multiCurrencyComboBoxMouseClicked
        // TODO add your handling code here:
        multiCurrencyComboBox.removeAllItems();
        for (MultiCurrency currency : multiCurrencyDAO.findAll()) {
            if (currency == null) {
                this.currency = currency;
            }
            multiCurrencyComboBox.addItem(new ComboKeyValue(currency.getName(), currency));
        }
    }//GEN-LAST:event_multiCurrencyComboBoxMouseClicked

    private void chargesComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chargesComboBoxMouseClicked
        // TODO add your handling code here:
        chargesComboBox.removeAllItems();
        for (Charges charges : chargesDAO.findAll()) {
            chargesComboBox.addItem(new ComboKeyValue(charges.getName(), charges));
        }
    }//GEN-LAST:event_chargesComboBoxMouseClicked

    private void billDateFormattedTextPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_billDateFormattedTextPropertyChange
        expiryDateChooser.setDate(commonService.addDaysToDate(billDateFormattedText.getDate(), 10));
    }//GEN-LAST:event_billDateFormattedTextPropertyChange

    private void discountTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_discountTextFocusGained
        discountText.selectAll();
    }//GEN-LAST:event_discountTextFocusGained

    //methode to clear all fields
    void clearAllFields() {
        try {
            billDateFormattedText.setDate(new java.util.Date());
            CommonService.clearTextFields(new JTextField[]{invoiceNumberText, supplierText, deliveryDateFormattedText});
            CommonService.clearTextArea(new JTextArea[]{addressTextArea});
            CommonService.clearCurrencyFields(new JTextField[]{chargesText});
            multiCurrencyComboBox.setSelectedIndex(0);
            taxComboBox.setSelectedIndex(0);
            chargesComboBox.setSelectedIndex(0);
            shipmentComboBox.setSelectedIndex(0);
            purchaseOrderItemList = null;
            purchaseOrderTaxList = null;
            purchaseOrderChargesList = null;
            supplierLedger = null;

            grandTotal = 0.00;
            subTotal = 0.00;
            numberProperty = numberPropertyDAO.findInvoiceNo("Purchase");
            invoiceNumberText.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
            Object currencyItem = multiCurrencyComboBox.getSelectedItem();
            currency = (MultiCurrency) ((ComboKeyValue) currencyItem).getValue();
            String currencySymbol = (null == currency) ? "" : currency.getSymbol();
            grandTotalLabel.setText(currencySymbol + " " + grandTotal);
            netAmountLabel.setText(currencySymbol + "" + netTotal);
            discountText.setText(grandTotal + "");
            purchaseOrderItemTableModel.setRowCount(0);

            purchaseOrder = null;
            saveButton.setText("Save");
        } catch (Exception e) {
            log.error("clearAllFields:", e);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel InvoiceDetailsPanel;
    private javax.swing.JButton addItemButton;
    private javax.swing.JTextArea addressTextArea;
    private javax.swing.JLabel availableQtyLabel;
    private com.toedter.calendar.JDateChooser billDateFormattedText;
    private javax.swing.JPanel bottomPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton chargesAddButton;
    private javax.swing.JComboBox chargesComboBox;
    private javax.swing.JButton chargesMinusButton;
    private javax.swing.JPanel chargesPanel;
    private javax.swing.JFormattedTextField chargesText;
    private javax.swing.JFormattedTextField deliveryDateFormattedText;
    private javax.swing.JTextField discountText;
    private com.toedter.calendar.JDateChooser expiryDateChooser;
    private javax.swing.JTextField godownText;
    private javax.swing.JLabel grandTotalLabel;
    private javax.swing.JPanel grandTotalPanel1;
    private javax.swing.JTextField invoiceNumberText;
    private javax.swing.JTextField itemCodeText;
    private javax.swing.JTextField itemDescriptionText;
    private javax.swing.JPanel itemInfoPanel;
    private javax.swing.JTextField itemQtyText;
    private javax.swing.JButton itemRemoveButton;
    private javax.swing.JFormattedTextField itemUnitPriceText;
    private javax.swing.JTextField itemWeightText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JComboBox multiCurrencyComboBox;
    private javax.swing.JLabel netAmountLabel;
    private javax.swing.JButton newButton;
    private javax.swing.JTable purchaseOrderItemTable;
    private javax.swing.JButton saveButton;
    private javax.swing.JComboBox shipmentComboBox;
    private javax.swing.JPanel shipmentModePanel;
    private javax.swing.JPanel shipmentModePanel1;
    private javax.swing.JTextField supplierText;
    private javax.swing.JPanel tableBorderPanel;
    private javax.swing.JButton taxAddButton;
    private javax.swing.JComboBox taxComboBox;
    private javax.swing.JPanel taxPanel;
    private javax.swing.JButton totDiscountAddButton;
    private javax.swing.JLabel totavailableQtyLabel;
    // End of variables declaration//GEN-END:variables
}
