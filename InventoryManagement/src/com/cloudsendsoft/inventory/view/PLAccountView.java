/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.CTableModel;
import com.cloudsendsoft.inventory.dao.CompanyDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.LedgerGroupDAO;
import com.cloudsendsoft.inventory.dao.PurchaseDAO;
import com.cloudsendsoft.inventory.dao.PurchaseReturnDAO;
import com.cloudsendsoft.inventory.dao.PurchaseReturnItemDAO;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.dao.SalesReturnDAO;
import com.cloudsendsoft.inventory.dao.SalesReturnItemDAO;
import com.cloudsendsoft.inventory.model.CommercialInvoice;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseReturn;
import com.cloudsendsoft.inventory.model.PurchaseReturnItem;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesReturn;
import com.cloudsendsoft.inventory.model.SalesReturnItem;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.report.BalanceSheetReport;
import com.cloudsendsoft.inventory.report.SalesReport;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.text.Format;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class PLAccountView extends javax.swing.JPanel {

    static final Logger log = Logger.getLogger(PLAccountView.class.getName());

    CommonService commonService = new CommonService();

    CTableModel debitSideTableModel = null;
    CTableModel creditSideTableModel = null;

    int screen = 1;
    int index = 0;

    LedgerGroupDAO ledgerGroupDAO = new LedgerGroupDAO();
    List<LedgerGroup> listOfLedgerGroup = null;

    LedgerDAO ledgerDAO = new LedgerDAO();
    List<Ledger> listOfLedger = null;

    ItemDAO itemDAO = new ItemDAO();
    List<Item> listOfItem = null;
    StockGroup stockGroup = null;
    List<StockGroup> listOfStockGroup = null;

    PurchaseDAO purchaseDAO = new PurchaseDAO();
    PurchaseReturnDAO purchaseReturnDAO = new PurchaseReturnDAO();
    List<PurchaseItem> listofPurchaseItem = new ArrayList<>(0);

    SalesDAO salesDAO = new SalesDAO();
    SalesReturnDAO salesReturnDAO = new SalesReturnDAO();

    //using for Commercial Invoice
    SalesPreformaDAO salesPreformaDAO = new SalesPreformaDAO();

    LedgerGroup ledgerGroup = null;

    double tradingDebitAmt = 0.00;
    double tradingCreditAmt = 0.00;

    double profitLosssDebitAmt = 0.00;
    double profitLosssCreditAmt = 0.00;

    boolean isBalancesheet = false;

    double diffAmountPLandTrading = 0.00;

    boolean PL_DebitBalance = true;

    List<Object[]> closingStockList = new ArrayList<>(0);
    List<Object[]> closingStockListDetailed = new ArrayList<>(0);
    double closingStock = 0.00;

    public PLAccountView(boolean isBalancesheet) {
        this.isBalancesheet = isBalancesheet;
        initComponents();
        //centerHeadingLabel.setText("Trial Balance as at "+commonService.sqlDateToString(GlobalProperty.company.getEndDate()));
        //table background color settings
        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);
        jScrollPane2.setOpaque(false);
        jScrollPane2.getViewport().setOpaque(false);
        /*       stockSummaryTable.setBackground(this.getBackground());
         stockSummaryTable.getTableHeader().setBackground(this.getBackground());
         stockSummaryTable.getTableHeader().setForeground(this.getBackground());
         */
        //*******************set debit side table design*****************
        debitSideTable.setRowHeight(GlobalProperty.tableRowHieght);

        debitSideTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {

            //alternate row color jtable
            @Override
            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int column) {
                Component c = super.getTableCellRendererComponent(table,
                        value, isSelected, hasFocus, row, column);
                c.setBackground(row % 2 == 0 ? Color.GRAY : Color.LIGHT_GRAY);
                return c;
            }
        ;
        });

        debitSideTable.setShowGrid(false);
        debitSideTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{"Particulars", "Amount"}
        ));
        debitSideTableModel = (CTableModel) debitSideTable.getModel();
        CommonService.setWidthAsPercentages(debitSideTable, .80, .20);

        debitSideTableModel.setRowCount(0);

        //*******************set credit side table design**************************************
        creditSideTable.setRowHeight(GlobalProperty.tableRowHieght);

        creditSideTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {

            //alternate row color jtable
            @Override
            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int column) {
                Component c = super.getTableCellRendererComponent(table,
                        value, isSelected, hasFocus, row, column);
                c.setBackground(row % 2 == 0 ? Color.GRAY : Color.LIGHT_GRAY);
                return c;
            }
        ;
        });

        creditSideTable.setShowGrid(false);
        creditSideTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{"Particulars", "Amount"}
        ));
        creditSideTableModel = (CTableModel) creditSideTable.getModel();
        CommonService.setWidthAsPercentages(creditSideTable, .80, .20);

        creditSideTableModel.setRowCount(0);

        //fetch all ledger groups
        //listOfLedgerGroup = ledgerGroupDAO.findAllByAsc();
        listOfLedgerGroup = ledgerDAO.findDistinctLedgerGroupFromLedger();
        //System.out.println("listOfLedgerGroup:" + listOfLedgerGroup.size());

        listOfStockGroup = itemDAO.findDistinctStockGroupFromItem();
        // System.out.println("listOfStockGroup:" + listOfStockGroup.size());

        fillPLAccountTable();
    }

    void fillBalanceSheet() {
        try {
            //BS table style
            centerHeadingLabel.setText("Balance Sheet");
            debitSideTableModel.setRowCount(0);
            creditSideTableModel.setRowCount(0);
            debitSideTable.getColumnModel().getColumn(0).setHeaderValue("Liabilities");
            creditSideTable.getColumnModel().getColumn(0).setHeaderValue("Assets");

            double liabilitySideAmt = 0.00;
            double assetSideAmt = 0.00;

            List<Object[]> capitalListLiabilitySide = new ArrayList<>(0);
            List<Object[]> capitalListAssetSide = new ArrayList<>(0);

            List<Object[]> liabilityListLiabilitySide = new ArrayList<>(0);
            List<Object[]> liabilityListAssetSide = new ArrayList<>(0);

            List<Object[]> assetListLiabilitySide = new ArrayList<>(0);
            List<Object[]> assetListAssetSide = new ArrayList<>(0);

            /* List<Object[]> closingStockList = new ArrayList<>(0);
             List<Object[]> closingStockListDetailed = new ArrayList<>(0);*/
            //all records from ledger table
            for (LedgerGroup ledgerGroup : listOfLedgerGroup) {
                listOfLedger = ledgerDAO.findAllByLedgerGroup(ledgerGroup);
                double ledgerGroupTotal = 0.00;
                for (Ledger ledger : listOfLedger) {
                    ledgerGroupTotal += ledger.getAvailableBalance();
                }

                if (ledgerGroupTotal > 0) {
                    if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Capital")) {
                        capitalListLiabilitySide.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                            + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                        liabilitySideAmt += ledgerGroupTotal;
                    } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Liability")) {
                        liabilityListLiabilitySide.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                            + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                        liabilitySideAmt += ledgerGroupTotal;
                    } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Asset")) {
                        assetListAssetSide.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                            + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                        assetSideAmt += ledgerGroupTotal;
                    }
                } else if (ledgerGroupTotal < 0) {
                    //ledgerGroupTotal=Math.abs(ledgerGroupTotal);
                    if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Capital")) {
                        capitalListAssetSide.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                            + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerGroupTotal)) + "</b></html>"});
                        assetSideAmt += Math.abs(ledgerGroupTotal);
                    } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Liability")) {
                        liabilityListAssetSide.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                            + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerGroupTotal)) + "</b></html>"});
                        assetSideAmt += Math.abs(ledgerGroupTotal);
                    } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Asset")) {
                        assetListLiabilitySide.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                            + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerGroupTotal)) + "</b></html>"});
                        liabilitySideAmt += Math.abs(ledgerGroupTotal);
                    }
                }

                //Debit Side - Detailed View of Ledger
                if (detailViewCheckBox.isSelected()) {
                    for (Ledger ledger : listOfLedger) {
                        if (ledgerGroupTotal > 0) {
                            if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Capital")) {
                                capitalListLiabilitySide.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                    + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                            } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Liability")) {
                                liabilityListLiabilitySide.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                    + "&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                            } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Asset")) {
                                assetListAssetSide.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                    + "&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                            }
                        } else if (ledgerGroupTotal < 0) {
                            ledger.setAvailableBalance(ledger.getAvailableBalance());
                            if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Capital")) {
                                capitalListAssetSide.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                    + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                            } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Liability")) {
                                liabilityListAssetSide.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                    + "&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                            } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Asset")) {
                                assetListLiabilitySide.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                    + "&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                            }
                        }
                    }
                }
            }

            //Liability - capital list
            for (Object[] objects : capitalListLiabilitySide) {
                debitSideTableModel.addRow(objects);
            }

            //Profit & Loss Balance
            if (diffAmountPLandTrading > 0 && PL_DebitBalance) {
                liabilitySideAmt -= diffAmountPLandTrading;
            } else {
                liabilitySideAmt += diffAmountPLandTrading;
            }
            debitSideTableModel.addRow(new Object[]{"<html><b>" + "Profit & Loss A/C"
                + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmountPLandTrading) + "</b></html>"});

            //Liability - liability list
            for (Object[] objects : liabilityListLiabilitySide) {
                debitSideTableModel.addRow(objects);
            }
            //Minus(-) - asset list
            for (Object[] objects : assetListLiabilitySide) {
                debitSideTableModel.addRow(objects);
            }

            //Asset - asset list
            for (Object[] objects : assetListAssetSide) {
                creditSideTableModel.addRow(objects);
            }
            //Minus(-) - liability list
            for (Object[] objects : liabilityListAssetSide) {
                creditSideTableModel.addRow(objects);
            }
            //Minus(-) - capital list
            for (Object[] objects : capitalListAssetSide) {
                creditSideTableModel.addRow(objects);
            }

            //closing stock
            for (Object[] objects : closingStockList) {
                creditSideTableModel.addRow(objects);
            }
            assetSideAmt += closingStock;

            //closing stock detailed
            for (Object[] objects : closingStockListDetailed) {
                creditSideTableModel.addRow(objects);
            }

            //total Trading A/c amount calculation
            double totalAmount = 0.00;
            double balSheetDiffAmount = 0.00;
            boolean debitBalance = true;

            if (liabilitySideAmt > assetSideAmt) {
                balSheetDiffAmount = liabilitySideAmt - assetSideAmt;
                creditSideTableModel.addRow(new Object[]{null});
                creditSideTableModel.addRow(new Object[]{"<html><b>" + "Diff. in opening balance" + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(balSheetDiffAmount) + "</b></html>"});
                creditSideTableModel.addRow(new Object[]{null});
                totalAmount = liabilitySideAmt;
            } else {
                balSheetDiffAmount = assetSideAmt - liabilitySideAmt;
                debitSideTableModel.addRow(new Object[]{null});
                debitSideTableModel.addRow(new Object[]{"<html><b>" + "Diff. in opening balance" + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(balSheetDiffAmount) + "</b></html>"});
                debitSideTableModel.addRow(new Object[]{null});
                totalAmount = assetSideAmt;
                debitBalance = false;
            }

            //equalize rows in both debit & credit table
            int row = debitSideTable.getRowCount();
            if (creditSideTable.getRowCount() > row) {
                row = creditSideTable.getRowCount();
            }
            for (int i = debitSideTable.getRowCount(); i < row; i++) {
                debitSideTableModel.addRow(new Object[]{null});
            }
            for (int i = creditSideTable.getRowCount(); i < row; i++) {
                creditSideTableModel.addRow(new Object[]{null});
            }

            debitSideTableModel.addRow(new Object[]{"<html><b><u Style=”border-bottom:1px solid black;”>" + " Total" + "</u></b></html>", "<html><b><u Style=”border-bottom:1px solid black;”>" + commonService.formatIntoCurrencyAsString(totalAmount) + "</u></b></html>"});
            creditSideTableModel.addRow(new Object[]{"<html><b><u Style=”border-bottom:1px solid black;”>" + " Total" + "</u></b></html>", "<html><b><u Style=”border-bottom:1px solid black;”>" + commonService.formatIntoCurrencyAsString(totalAmount) + "</u></b></html>"});

        } catch (Exception e) {
            log.error("fillBalanceSheet:", e);
        }
    }

    void fillPLAccountTable() {
        try {
            debitSideTableModel.setRowCount(0);
            creditSideTableModel.setRowCount(0);
            listofPurchaseItem = new ArrayList<>(0);
            //  switch (screen) {
            //      case 1:
            detailViewCheckBox.setVisible(true);
            tradingDebitAmt = 0.00;
            profitLosssDebitAmt = 0.00;
            profitLosssCreditAmt = 0.00;
            tradingCreditAmt = 0.00;

            List<Object[]> openingStockList = new ArrayList<>(0);
            closingStockList = new ArrayList<>(0);
            closingStockListDetailed = new ArrayList<>(0);

            List<Object[]> directExpenseList = new ArrayList<>(0);
            List<Object[]> indirectExpenseList = new ArrayList<>(0);
            List<Object[]> directIncomeList = new ArrayList<>(0);
            List<Object[]> indirectIncomeList = new ArrayList<>(0);

            //Opening Stock
            double openingBalanceTot = itemDAO.findOpeningBalanceTotal();
            tradingDebitAmt += openingBalanceTot;

            //double purchaseTotal = 0;
            //double salesTotal = 0;
            if (openingBalanceTot != 0) {
                openingStockList.add(new Object[]{"<html><b>Opening Stock</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(openingBalanceTot) + "</b></html>"});
                //Detailed View of Op Bal.
                if (detailViewCheckBox.isSelected()) {
                    for (StockGroup stockGroup : listOfStockGroup) {
                        List<Object[]> itemStockGroupList = itemDAO.findOpeningBalanceByItemStockGroup(stockGroup);
                        boolean isGroupNameFirst = true;
                        for (Object[] objects : itemStockGroupList) {
                            if (isGroupNameFirst && Double.parseDouble(String.valueOf(objects[1])) != 0) {
                                openingStockList.add(new Object[]{"<html>&nbsp;&nbsp;&nbsp;&nbsp;<u><b>" + stockGroup.getName() + "</b></u></html>"});
                                isGroupNameFirst = false;
                            }
                            if (Double.parseDouble(String.valueOf(objects[1])) != 0) {
                                openingStockList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + objects[0] + "\t\t" + commonService.formatIntoCurrencyAsString(Double.parseDouble(String.valueOf(objects[1]))) + "</i></html>"});
                            }
                        }
                    }
                }
            }

            //calc returns
            PurchaseReturnItemDAO purchaseReturnItemDAO = new PurchaseReturnItemDAO();
            double purchaseReturnTotal = purchaseReturnItemDAO.findPurchaseReturnItemTotal();
            SalesReturnItemDAO salesReturnItemDAO = new SalesReturnItemDAO();
            double salesReturnTotal = salesReturnItemDAO.findSalesReturnItemTotal();

            //all records from ledger table
            for (LedgerGroup ledgerGroup : listOfLedgerGroup) {
                listOfLedger = ledgerDAO.findAllByLedgerGroup(ledgerGroup);
                double ledgerGroupTotal = 0.00;
                for (Ledger ledger : listOfLedger) {
                    ledgerGroupTotal += ledger.getAvailableBalance();
                }
                if (ledgerGroupTotal > 0 || ledgerGroupTotal < 0) {

                    //Debit Balance - LedgerGroup
                    if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Expense")
                            || ledgerGroup.getAccountType().trim().equalsIgnoreCase("Indirect Expense")) {

                        if (ledgerGroupTotal > 0) {
                            if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Expense")) {
                                if (ledgerGroup.getGroupName().trim().equalsIgnoreCase("Purchase Accounts")) {
                                    ledgerGroupTotal -= purchaseReturnTotal;
                                }
                                directExpenseList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                    + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                tradingDebitAmt += ledgerGroupTotal;
                            } else {
                                indirectExpenseList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                    + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                profitLosssDebitAmt += ledgerGroupTotal;
                            }

                        } else if (ledgerGroupTotal < 0) {
                            if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Expense")) {
                                if (ledgerGroup.getGroupName().trim().equalsIgnoreCase("Purchase Accounts")) {
                                    ledgerGroupTotal += purchaseReturnTotal;
                                }
                                directIncomeList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                    + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                tradingCreditAmt += Math.abs(ledgerGroupTotal);
                            } else {
                                indirectIncomeList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                    + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                profitLosssCreditAmt += Math.abs(ledgerGroupTotal);
                            }

                        }
                        //Debit Side - Detailed View of Ledger
                        if (detailViewCheckBox.isSelected()) {
                            for (Ledger ledger : listOfLedger) {
                                if (ledger.getAvailableBalance() > 0) {
                                    if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Expense")) {
                                        directExpenseList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                                        if (ledgerGroup.getGroupName().trim().equalsIgnoreCase("Purchase Accounts") && purchaseReturnTotal > 0) {
                                            directExpenseList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + "Purchase Returns&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                                + commonService.formatIntoCurrencyAsString(purchaseReturnTotal) + "</i></html>"});
                                        }
                                    } else {
                                        indirectExpenseList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                                    }
                                } else if (ledger.getAvailableBalance() < 0) {
                                    if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Expense")) {
                                        directIncomeList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                                    } else {
                                        indirectIncomeList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                                    }
                                }
                            }
                        }

                    } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Income")
                            || ledgerGroup.getAccountType().trim().equalsIgnoreCase("Indirect Income")) {
                        //Credit Side Balance - LedgerGroup
                        if (ledgerGroupTotal > 0) {
                            if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Income")) {
                                if (ledgerGroup.getGroupName().trim().equalsIgnoreCase("Sales Accounts")) {
                                    ledgerGroupTotal -= salesReturnTotal;
                                    directIncomeList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                        + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                }
                                tradingCreditAmt += ledgerGroupTotal;
                            } else {
                                indirectIncomeList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                    + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                if (ledgerGroup.getGroupName().trim().equalsIgnoreCase("Sales Accounts")) {
                                    ledgerGroupTotal += salesReturnTotal;
                                }
                                profitLosssCreditAmt += ledgerGroupTotal;
                            }

                        } else if (ledgerGroupTotal < 0) {
                            if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Income")) {
                                directExpenseList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                    + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                tradingDebitAmt += Math.abs(ledgerGroupTotal);
                            } else {
                                indirectExpenseList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                    + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                profitLosssDebitAmt += Math.abs(ledgerGroupTotal);
                            }

                        }

                        //Credit Side - Detailed View of Ledger
                        if (detailViewCheckBox.isSelected()) {
                            for (Ledger ledger : listOfLedger) {
                                if (ledger.getAvailableBalance() > 0) {
                                    if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Income")) {
                                        directIncomeList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                                        if (ledgerGroup.getGroupName().trim().equalsIgnoreCase("Sales Accounts") && salesReturnTotal > 0) {
                                            directIncomeList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + "Sales Returns"
                                                + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(salesReturnTotal) + "</i></html>"});
                                        }
                                    } else {
                                        indirectIncomeList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "</i></html>", "<html><i>" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                                    }
                                } else if (ledger.getAvailableBalance() < 0) {
                                    if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Income")) {
                                        directExpenseList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                                    } else {
                                        indirectExpenseList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                                    }
                                }
                            }
                        }
                    }//direct or indirect income closed
                }
            }

            /* if (openingBalanceTot > 0) {
             openingStockList.add(new Object[]{"<html><b>Opening Stock</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(openingBalanceTot) + "</b></html>"});
             //Detailed View of Op Bal.
             if (detailViewCheckBox.isSelected()) {
             for (StockGroup stockGroup : listOfStockGroup) {
             List<Object[]> itemStockGroupList = itemDAO.findOpeningBalanceByItemStockGroup(stockGroup);
             boolean isGroupNameFirst=true;
             for (Object[] objects : itemStockGroupList) {
             if(isGroupNameFirst && Double.parseDouble(String.valueOf(objects[1]))>0){
             openingStockList.add(new Object[]{"<html>&nbsp;&nbsp;&nbsp;&nbsp;<u><b>" + stockGroup.getName() + "</b></u></html>"});
             isGroupNameFirst=false;
             }
             if(Double.parseDouble(String.valueOf(objects[1]))>0){
             openingStockList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + objects[0] + "\t\t" + commonService.formatIntoCurrencyAsString(Double.parseDouble(String.valueOf(objects[1]))) + "</i></html>"});
             }
             }
             }
             }
             }*/
            //Closing Stock - Using FIFO methode
            //To get all exact purchase items after deducting purchase returns qty
            List<Purchase> listOfPurchase = purchaseDAO.findAllByBillDateASC();
            List<PurchaseReturn> listOfPurchaseReturns = purchaseReturnDAO.findAllPurchaseReturn();
            List<Sales> listOfSales = salesDAO.findAllSales();
            List<SalesReturn> listOfSalesReturns = salesReturnDAO.findAllSalesReturn();
            List<SalesPreforma> listOfCommercialInvoice = salesPreformaDAO.findAllSalesPreformaCommercialInvoice();

            HashMap<Item, Double> itemAvailableQtyHashMap = new HashMap<>(0);
            Set<String> stockGroupNameList = new TreeSet<String>();

            //opening...
            List<Item> listOfItems = itemDAO.findAll();
            //calculating available qty by adding opening 
            for (Item item : listOfItems) {
                if (itemAvailableQtyHashMap.get(item) == null) {
                    itemAvailableQtyHashMap.put(item, item.getOpeningQty());
                } else {
                    itemAvailableQtyHashMap.put(item, item.getOpeningQty() + itemAvailableQtyHashMap.get(item));
                }
                stockGroupNameList.add(item.getStockGroup().getName());
            }

            HashMap<Purchase, PurchaseReturn> purchasePurchaseReturnHashMap = new HashMap<Purchase, PurchaseReturn>(0);
            for (PurchaseReturn purchaseReturn : listOfPurchaseReturns) {
                purchasePurchaseReturnHashMap.put(purchaseReturn.getPurchase(), purchaseReturn);
            }

            for (Purchase purchase : listOfPurchase) {
                PurchaseReturn purchaseReturn = purchasePurchaseReturnHashMap.get(purchase);
                for (PurchaseItem purchaseItem : purchase.getPurchaseItems()) {
                    if (purchaseReturn != null) {
                        for (PurchaseReturnItem purchaseReturnItem : purchaseReturn.getPurchaseReturnItems()) {
                            if (purchaseReturnItem.getItem().equals(purchaseItem.getItem())) {
                                purchaseItem.setQuantity(purchaseItem.getQuantity() - purchaseReturnItem.getQuantity());
                                break;
                            }
                        }
                    }
                    listofPurchaseItem.add(purchaseItem);

                    if (itemAvailableQtyHashMap.get(purchaseItem.getItem()) == null) {
                        itemAvailableQtyHashMap.put(purchaseItem.getItem(), purchaseItem.getQuantity());
                    } else {
                        itemAvailableQtyHashMap.put(purchaseItem.getItem(), purchaseItem.getQuantity() + itemAvailableQtyHashMap.get(purchaseItem.getItem()));
                    }
                    //for showing stockgroup name under closing stock as a category
                    stockGroupNameList.add(purchaseItem.getItem().getStockGroup().getName());
                }
            }
            //calculating available qty by adding salesReturn 
            for (SalesReturn salesReturn : listOfSalesReturns) {
                for (SalesReturnItem salesReturnItem : salesReturn.getSalesReturnItems()) {
                    if (itemAvailableQtyHashMap.get(salesReturnItem.getItem()) == null) {
                        itemAvailableQtyHashMap.put(salesReturnItem.getItem(), salesReturnItem.getQuantity());
                    } else {
                        itemAvailableQtyHashMap.put(salesReturnItem.getItem(), salesReturnItem.getQuantity() + itemAvailableQtyHashMap.get(salesReturnItem.getItem()));
                    }
                    stockGroupNameList.add(salesReturnItem.getItem().getStockGroup().getName());
                }

            }
            //calculating available qty by deducting sales 
            for (Sales sales : listOfSales) {
                for (SalesItem salesItem : sales.getSalesItems()) {
                    if (itemAvailableQtyHashMap.get(salesItem.getItem()) == null) {
                        itemAvailableQtyHashMap.put(salesItem.getItem(), (double) salesItem.getQuantity());
                    } else {
                        itemAvailableQtyHashMap.put(salesItem.getItem(), itemAvailableQtyHashMap.get(salesItem.getItem()) - salesItem.getQuantity());
                    }
                    stockGroupNameList.add(salesItem.getItem().getStockGroup().getName());
                }
            }

            //calculating available qty by deducting Commercial Invoice 
            for (SalesPreforma commercialInvoice : listOfCommercialInvoice) {
                for (SalesPreformaItem salesPreformaItem : commercialInvoice.getSalesPreformaItems()) {
                    if (itemAvailableQtyHashMap.get(salesPreformaItem.getItem()) == null) {
                        itemAvailableQtyHashMap.put(salesPreformaItem.getItem(), (double) salesPreformaItem.getQuantity());
                    } else {
                        itemAvailableQtyHashMap.put(salesPreformaItem.getItem(), itemAvailableQtyHashMap.get(salesPreformaItem.getItem()) - salesPreformaItem.getQuantity());
                    }
                    stockGroupNameList.add(salesPreformaItem.getItem().getStockGroup().getName());
                }
            }

            HashMap<String, List<Object[]>> closingStockListHashMap = new HashMap<>(0);
            for (String stockGroupName : stockGroupNameList) {
                Iterator it = itemAvailableQtyHashMap.entrySet().iterator();
                List<Object[]> stockObjectsList = new ArrayList<>();
                
                while (it.hasNext()) {
                    double itemAvailQty = 0;
                    Map.Entry pairs = (Map.Entry) it.next();
                    //  System.out.println(((Item) pairs.getKey()).getDescription() + " = " + pairs.getValue());
                    if (((Item) pairs.getKey()).getStockGroup().getName().equalsIgnoreCase(stockGroupName)) {
                        Item item = (Item) pairs.getKey();
                        Object[] objects = new Object[3];
                        itemAvailQty += (Double) pairs.getValue();
                        objects[0] = item.getDescription();
                        objects[1] = itemAvailQty;
                        objects[2] = item.getItemCode();
                        stockObjectsList.add(objects);
                        //System.out.println("pairs.getValue():"+pairs.getValue());
                    }
                }
                closingStockListHashMap.put(stockGroupName, stockObjectsList);
            }

//            Iterator it1 = closingStockListHashMap.entrySet().iterator();
//    while (it1.hasNext()) {
//        Map.Entry pairs = (Map.Entry)it1.next();
//       // it1.remove(); // avoids a ConcurrentModificationException
//        for(Object[] object:(List<Object[]>)pairs.getValue()){
//            System.out.println(String.valueOf(object[0])+"="+String.valueOf(object[1])+"="+String.valueOf(object[2]));
//      }
//    }
    
            //calculating closing stock qty & price - FIFO
            closingStock = 0;
            for (String stockGroupName : stockGroupNameList) {
                if (detailViewCheckBox.isSelected()) {
                    closingStockListDetailed.add(new Object[]{"<html>&nbsp;&nbsp;&nbsp;&nbsp;<u><b>" + stockGroupName + "</b></u></html>"});
                }
                List<Object[]> itemStockGroupList = closingStockListHashMap.get(stockGroupName);
                for (Object[] objects : itemStockGroupList) {
                    double closingQty = Double.parseDouble(String.valueOf(objects[1]));
                    double closingQtyBal = 0;
                    double itemBal = 0;
                    String itemCode = String.valueOf(objects[2]);
                    for (int i = listofPurchaseItem.size() - 1; i >= 0; i--) {
                        PurchaseItem purchaseItem = listofPurchaseItem.get(i);
                        double purItemQty = purchaseItem.getQuantity();
                        if (itemCode.trim().equalsIgnoreCase(purchaseItem.getItem().getItemCode())) {
                            if (closingQty >= purItemQty) {
                                closingQty -= purItemQty;
                                closingStock += (purItemQty * purchaseItem.getUnitPrice());
                                itemBal += (purItemQty * purchaseItem.getUnitPrice());
                                closingQtyBal = closingQty;
                            } else {
                                closingStock += (closingQty * purchaseItem.getUnitPrice());
                                itemBal += (closingQty * purchaseItem.getUnitPrice());
                                closingQtyBal = closingQty;
                                closingQty = 0;
                                break;
                            }
                        }

                    }
                    if (closingQty > 0) {
                        List<Item> listOfItem = itemDAO.findAllByItemCode(itemCode);
                        for (Item item : listOfItem) {
                            if (item.getOpeningQty() > 0) {
                                closingStock += (closingQty * item.getOpeningUnitPrice());
                                itemBal += (closingQty * item.getOpeningUnitPrice());
                                closingQtyBal = closingQty;
                                closingQty = 0;
                            }
                            /*if(closingQty>=item.getOpeningQty()){
                             closingQty -= item.getOpeningQty();
                             closingStock += (item.getOpeningQty() * item.getOpeningUnitPrice());
                             itemBal += (item.getOpeningQty() * item.getOpeningUnitPrice());
                             }else{
                             closingStock += (closingQty * item.getOpeningUnitPrice());
                             itemBal += (closingQty * item.getOpeningUnitPrice());
                             //closingQty = 0;
                             break;
                             }*/
                        }
                    }
                    if (detailViewCheckBox.isSelected()) {
                        if (itemBal != 0) {
                            //with Qty
                            //closingStockListDetailed.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + objects[0] + "\t\t(" +closingQtyBal+ "\t.Qty)\t\t"+ itemBal + "</i></html>"});
                            closingStockListDetailed.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + objects[0] + "\t\t" + commonService.formatIntoCurrencyAsString(itemBal) + "</i></html>"});
                        }
                    }
                }
            }
            tradingCreditAmt += closingStock;
            if (closingStock != 0) {
                closingStockList.add(new Object[]{"<html><b>Closing Stock</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(closingStock) + "</b></html>"});
            }

            //opening Stock
            for (Object[] objects : openingStockList) {
                debitSideTableModel.addRow(objects);
            }

            //direct expenses
            for (Object[] objects : directExpenseList) {
                debitSideTableModel.addRow(objects);
            }

            //Direct Income
            for (Object[] objects : directIncomeList) {
                creditSideTableModel.addRow(objects);
            }

            //closing stock
            for (Object[] objects : closingStockList) {
                creditSideTableModel.addRow(objects);
            }

            //closing stock detailed
            for (Object[] objects : closingStockListDetailed) {
                creditSideTableModel.addRow(objects);
            }

            //total Trading A/c amount calculation
            double totalAmount = 0.00;
            diffAmountPLandTrading = 0.00;
            PL_DebitBalance = true;

            if (tradingDebitAmt > tradingCreditAmt) {
                diffAmountPLandTrading = tradingDebitAmt - tradingCreditAmt;
                creditSideTableModel.addRow(new Object[]{null});
                creditSideTableModel.addRow(new Object[]{"<html><b>" + "Gross Loss c/d" + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmountPLandTrading) + "</b></html>"});
                creditSideTableModel.addRow(new Object[]{null});
                totalAmount = tradingDebitAmt;
            } else {
                diffAmountPLandTrading = tradingCreditAmt - tradingDebitAmt;
                debitSideTableModel.addRow(new Object[]{null});
                debitSideTableModel.addRow(new Object[]{"<html><b>" + "Gross Profit c/d" + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmountPLandTrading) + "</b></html>"});
                debitSideTableModel.addRow(new Object[]{null});
                totalAmount = tradingCreditAmt;
                PL_DebitBalance = false;
            }

            //equalize rows in both debit & credit table
            int row = debitSideTable.getRowCount();
            if (creditSideTable.getRowCount() > row) {
                row = creditSideTable.getRowCount();
            }
            for (int i = debitSideTable.getRowCount(); i < row; i++) {
                debitSideTableModel.addRow(new Object[]{null});
            }
            for (int i = creditSideTable.getRowCount(); i < row; i++) {
                creditSideTableModel.addRow(new Object[]{null});
            }

            debitSideTableModel.addRow(new Object[]{"<html><b><u Style=”border-bottom:1px solid black;”>" + " Total" + "</u></b></html>", "<html><b><u Style=”border-bottom:1px solid black;”>" + commonService.formatIntoCurrencyAsString(totalAmount) + "</u></b></html>"});
            creditSideTableModel.addRow(new Object[]{"<html><b><u Style=”border-bottom:1px solid black;”>" + " Total" + "</u></b></html>", "<html><b><u Style=”border-bottom:1px solid black;”>" + commonService.formatIntoCurrencyAsString(totalAmount) + "</u></b></html>"});

            // Profit & Loss Calculations
            debitSideTableModel.addRow(new Object[]{null});
            creditSideTableModel.addRow(new Object[]{null});

            if (PL_DebitBalance) {
                debitSideTableModel.addRow(new Object[]{"<html><b>" + "Gross Loss b/d", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmountPLandTrading) + "</b></html>"});
                profitLosssDebitAmt += diffAmountPLandTrading;
            } else {
                creditSideTableModel.addRow(new Object[]{"<html><b>" + "Gross Profit b/d", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmountPLandTrading) + "</b></html>"});
                profitLosssCreditAmt += diffAmountPLandTrading;
            }

            //indirect Expenses
            for (Object[] objects : indirectExpenseList) {
                debitSideTableModel.addRow(objects);
            }

            //indirect Income
            for (Object[] objects : indirectIncomeList) {
                creditSideTableModel.addRow(objects);
            }

            //total Profit & Loss A/c amount calculation
            totalAmount = 0.00;
            diffAmountPLandTrading = 0.00;
            PL_DebitBalance = true;
            debitSideTableModel.addRow(new Object[]{null});
            if (profitLosssDebitAmt > profitLosssCreditAmt) {
                diffAmountPLandTrading = profitLosssDebitAmt - profitLosssCreditAmt;
                creditSideTableModel.addRow(new Object[]{null});
                creditSideTableModel.addRow(new Object[]{"<html><b>" + "Net Loss c/d" + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmountPLandTrading) + "</b></html>"});
                creditSideTableModel.addRow(new Object[]{null});
                totalAmount = profitLosssDebitAmt;
            } else {
                diffAmountPLandTrading = profitLosssCreditAmt - profitLosssDebitAmt;
                debitSideTableModel.addRow(new Object[]{null});
                debitSideTableModel.addRow(new Object[]{"<html><b>" + "Gross Profit c/d" + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmountPLandTrading) + "</b></html>"});
                debitSideTableModel.addRow(new Object[]{null});
                totalAmount = profitLosssCreditAmt;
                PL_DebitBalance = false;
            }

            //equalize rows in both debit & credit table after trading
            row = debitSideTable.getRowCount();
            if (creditSideTable.getRowCount() > row) {
                row = creditSideTable.getRowCount();
            }
            for (int i = debitSideTable.getRowCount(); i < row; i++) {
                debitSideTableModel.addRow(new Object[]{null});
            }
            for (int i = creditSideTable.getRowCount(); i < row; i++) {
                creditSideTableModel.addRow(new Object[]{null});
            }

            debitSideTableModel.addRow(new Object[]{"<html><b><u Style=”border-bottom:1px solid black;”>" + " Total" + "</u></b></html>", "<html><b><u Style=”border-bottom:1px solid black;”>" + commonService.formatIntoCurrencyAsString(totalAmount) + "</u></b></html>"});
            creditSideTableModel.addRow(new Object[]{"<html><b><u Style=”border-bottom:1px solid black;”>" + " Total" + "</u></b></html>", "<html><b><u Style=”border-bottom:1px solid black;”>" + commonService.formatIntoCurrencyAsString(totalAmount) + "</u></b></html>"});

            //Balace Sheet Section
            if (isBalancesheet) {
                fillBalanceSheet();
            }
        } catch (Exception e) {
            log.error("fillPLAccountTable:", e);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headingPanel = new javax.swing.JPanel();
        centerHeadingLabel = new javax.swing.JLabel();
        printPanel = new javax.swing.JPanel();
        printButton = new javax.swing.JButton();
        pdfButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        detailViewCheckBox = new javax.swing.JCheckBox();
        bodyPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        debitSideTable = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        creditSideTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        headingPanel.setBackground(new java.awt.Color(134, 0, 0));
        headingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        headingPanel.setPreferredSize(new java.awt.Dimension(1124, 50));

        centerHeadingLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        centerHeadingLabel.setForeground(new java.awt.Color(255, 255, 153));
        centerHeadingLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        centerHeadingLabel.setText("Trading And Profit And Loss Account");

        printPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        printPanel.setOpaque(false);
        printPanel.setLayout(new java.awt.GridLayout(1, 0));

        printButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/print.png"))); // NOI18N
        printButton.setText("Print");
        printButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printButtonActionPerformed(evt);
            }
        });
        printPanel.add(printButton);

        pdfButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/pdf-icon.png"))); // NOI18N
        pdfButton.setText("PDF");
        pdfButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdfButtonActionPerformed(evt);
            }
        });
        printPanel.add(pdfButton);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel1.setOpaque(false);

        detailViewCheckBox.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        detailViewCheckBox.setForeground(new java.awt.Color(255, 255, 153));
        detailViewCheckBox.setText("Detailed View");
        detailViewCheckBox.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        detailViewCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        detailViewCheckBox.setOpaque(false);
        detailViewCheckBox.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                detailViewCheckBoxStateChanged(evt);
            }
        });
        detailViewCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                detailViewCheckBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(detailViewCheckBox, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(detailViewCheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout headingPanelLayout = new javax.swing.GroupLayout(headingPanel);
        headingPanel.setLayout(headingPanelLayout);
        headingPanelLayout.setHorizontalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(centerHeadingLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 765, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(printPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        headingPanelLayout.setVerticalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(printPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(centerHeadingLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(headingPanel, java.awt.BorderLayout.PAGE_START);

        bodyPanel.setOpaque(false);
        bodyPanel.setLayout(new java.awt.GridLayout(1, 0));

        debitSideTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        debitSideTable.setOpaque(false);
        debitSideTable.setSelectionBackground(new java.awt.Color(153, 153, 153));
        debitSideTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                debitSideTableMouseClicked(evt);
            }
        });
        debitSideTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                debitSideTableKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(debitSideTable);

        bodyPanel.add(jScrollPane1);

        creditSideTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        creditSideTable.setOpaque(false);
        creditSideTable.setSelectionBackground(new java.awt.Color(153, 153, 153));
        creditSideTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                creditSideTableMouseClicked(evt);
            }
        });
        creditSideTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                creditSideTableKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(creditSideTable);

        bodyPanel.add(jScrollPane2);

        add(bodyPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void detailViewCheckBoxStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_detailViewCheckBoxStateChanged

    }//GEN-LAST:event_detailViewCheckBoxStateChanged

    private void debitSideTableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_debitSideTableKeyPressed

        /*  if (evt.getKeyCode() == KeyEvent.VK_ESCAPE && screen > 1) {
         screen--;
         fillPLAccountTable();
         } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
         screen++;
         fillPLAccountTable();
         }*/
    }//GEN-LAST:event_debitSideTableKeyPressed

    private void debitSideTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_debitSideTableMouseClicked
        /* try {
         if (evt.getClickCount() == 2) {
         index = debitSideTable.rowAtPoint(evt.getPoint());
         screen++;
         clickAction();

         //screen++;
         //fillStockSummaryTable();
         }
         } catch (Exception e) {
         log.error("stockSummaryTableMouseClicked", e);
         }*/
    }//GEN-LAST:event_debitSideTableMouseClicked

    private void creditSideTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_creditSideTableMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_creditSideTableMouseClicked

    private void creditSideTableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_creditSideTableKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_creditSideTableKeyPressed

    private void detailViewCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_detailViewCheckBoxActionPerformed
        fillPLAccountTable();
    }//GEN-LAST:event_detailViewCheckBoxActionPerformed

    private void printButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printButtonActionPerformed
        try {
            BalanceSheetReport balanceSheetReport = new BalanceSheetReport();
             if (MsgBox.confirm("Are you sure you want to Print?")) {
                    if (balanceSheetReport.createBalanceSheetReport(debitSideTable, creditSideTable, "BalanceSheet", "print")) {
                        
                    } else {
                        MsgBox.warning("Something went wrong, please re-click the button again!");
                    }
                }
        } catch (Exception ex) {
            log.error("printButtonActionPerformed:",ex);
        }
    }//GEN-LAST:event_printButtonActionPerformed

    private void pdfButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdfButtonActionPerformed
        try {
            BalanceSheetReport balanceSheetReport = new BalanceSheetReport();
            if (MsgBox.confirm("Are you sure you want to create pdf?")) {
                if (balanceSheetReport.createBalanceSheetReport(debitSideTable, creditSideTable, "BalanceSheet", "pdf")) {

                } else {
                    MsgBox.warning("Something went wrong, please re-click the button again!");
                }
            }
        } catch (Exception ex) {
            log.error("pdfButtonActionPerformed:", ex);
        }
    }//GEN-LAST:event_pdfButtonActionPerformed

    /*  void clickAction() {
     try {
     switch (screen) {
     case 2:
     ledgerGroup = listOfLedgerGroup.get(index);
     fillPLAccountTable();
     break;
     case 3:
     // item = listOfItem.get(index);
     //fillStockSummaryTable();
     break;
     case 4:
     // month = selectedFirstCol.trim();
     if (index > 0) {
     //                        month = months[index - 1];
     // fillStockSummaryTable();
     } else {
     screen--;
     }
     break;
     case 5:
     ArrayList row = CommonService.getTableRowData(debitSideTable);
     if (row.size() > 0) {
     //   invoiceDate = (String) row.get(0);
     //  invoiceType = (String) row.get(2);
     //  invoiceNumber = (String) row.get(3);
     //fillStockSummaryTable();
     }

     break;

     }
     //screen++;

     } catch (Exception e) {
     log.error("clickAction:", e);
     }
     }*/

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bodyPanel;
    private javax.swing.JLabel centerHeadingLabel;
    private javax.swing.JTable creditSideTable;
    private javax.swing.JTable debitSideTable;
    private javax.swing.JCheckBox detailViewCheckBox;
    private javax.swing.JPanel headingPanel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton pdfButton;
    private javax.swing.JButton printButton;
    private javax.swing.JPanel printPanel;
    // End of variables declaration//GEN-END:variables

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    //HibernateUtil hibernateUtil=new HibernateUtil();

                    CompanyDAO companyDAO = new CompanyDAO();
                    GlobalProperty.setCompany(companyDAO.getDefaultCompany());
                    MainForm jMainFrame = new MainForm();
                    jMainFrame.setVisible(true);
                    jMainFrame.setExtendedState(MainForm.MAXIMIZED_BOTH);

                    PLAccountView trialBalanceView = new PLAccountView(false);
                    jMainFrame.getContentPanel().removeAll();
                    jMainFrame.getContentPanel().repaint();
                    jMainFrame.getContentPanel().revalidate();
                    jMainFrame.getContentPanel().add(trialBalanceView);

                } catch (Exception e) {
                    log.error("MainForm run :", e);
                }
            }
        });
    }
}
