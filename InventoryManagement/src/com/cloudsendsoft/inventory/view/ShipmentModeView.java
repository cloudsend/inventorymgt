package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.ShipmentModeDAO;
import com.cloudsendsoft.inventory.dao.StockGroupDAO;
import com.cloudsendsoft.inventory.services.ChargesService;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.services.StockGroupService;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author sangeeth
 */
public class ShipmentModeView extends javax.swing.JInternalFrame {

    /**
     * Creates new form AddItem
     */
    static final Logger log = Logger.getLogger(ShipmentModeView.class.getName());
    CRUDServices cRUDServices = new CRUDServices();
    ;
    ShipmentModeDAO shipmentModeDAO = new ShipmentModeDAO();
    //StockGroupService stockGroupService=new StockGroupService();
    CommonService commonService = new CommonService();
    ShipmentMode shipmentMode = null;

    public ShipmentModeView() {
        initComponents();

        if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
            mainPanel.remove(buttonPanel);
            mainPanel.repaint();
            mainPanel.revalidate();
        }

        this.getRootPane().setDefaultButton(addButton);
        KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);

        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);

        shipmentModeListTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{
                    "Shipment Modes"
                }
        ));
        shipmentModeListTable.setRowHeight(GlobalProperty.tableRowHieght);

        fillShipmentModeTable();
    }

    void fillShipmentModeTable() {
        DefaultTableModel model = (DefaultTableModel) shipmentModeListTable.getModel();
        model.setRowCount(0);
        ShipmentModeDAO shipmentModeDAO = new ShipmentModeDAO();
        List<ShipmentMode> items = shipmentModeDAO.findAllByDESC();
        int i = 0;
        for (ShipmentMode itm : items) {
            ((DefaultTableModel) shipmentModeListTable.getModel()).insertRow(i, new Object[]{itm.getName()});
            i++;
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        shipmentModeListTable = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        shipmentNameText = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        buttonPanel = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();
        updateButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();

        setClosable(true);
        setTitle("Shipment Mode");
        getContentPane().setLayout(new java.awt.CardLayout());

        mainPanel.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("List Of Shipment Mode"));
        jPanel2.setOpaque(false);

        shipmentModeListTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        shipmentModeListTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                shipmentModeListTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(shipmentModeListTable);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 518, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel4.setOpaque(false);

        jLabel1.setText("Shipment Mode Name");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(shipmentNameText)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(shipmentNameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buttonPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new java.awt.GridLayout(1, 0));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("New");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(newButton);

        addButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        addButton.setText("Save");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(addButton);

        updateButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/update.png"))); // NOI18N
        updateButton.setText("Update");
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(updateButton);

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/delete-icon.png"))); // NOI18N
        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(deleteButton);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(mainPanel, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        try {
            String shipmentName = shipmentNameText.getText().trim();
            shipmentMode = shipmentModeDAO.findByName(shipmentName);
            if (null == shipmentMode) {
                if (CommonService.stringValidator(new String[]{shipmentName})) {
                    if (MsgBox.confirm("Are you sure you want to save?")) {
                        shipmentMode = new ShipmentMode();
                        shipmentMode.setName(shipmentName);
                        shipmentMode.setCompany(GlobalProperty.getCompany());
                        cRUDServices.saveModel(shipmentMode);
                        MsgBox.success("Successfully Saved " + shipmentName);
                        CommonService.clearTextFields(new JTextField[]{shipmentNameText});
                        fillShipmentModeTable();
                        shipmentNameText.requestFocusInWindow();
                    }
                } else {
                    MsgBox.warning("Shipment Mode Name is mandatory.");
                    shipmentNameText.requestFocusInWindow();
                }
            } else {
                MsgBox.warning("Shipment Mode Name '" + shipmentName + "' is already exists.");
                shipmentNameText.requestFocusInWindow();
            }

        } catch (Exception e) {
            log.error("addButtonActionPerformed:", e);
        }

    }//GEN-LAST:event_addButtonActionPerformed

    private void shipmentModeListTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_shipmentModeListTableMouseClicked
        try {

            ArrayList selectedRow = commonService.getTableRowData(shipmentModeListTable);
            shipmentMode = shipmentModeDAO.findByName(selectedRow.get(0) + "");
            shipmentNameText.setText(shipmentMode.getName());
        } catch (Exception e) {
            log.error("shipmentModeListTableMouseClicked", e);
        }
    }//GEN-LAST:event_shipmentModeListTableMouseClicked

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
        try {
            String groupName = shipmentNameText.getText().trim();
            if (null != shipmentMode) {
                if (CommonService.stringValidator(new String[]{groupName})) {
                    if (MsgBox.confirm("Are you sure you want to update?")) {
                        shipmentMode.setName(groupName);
                        cRUDServices.saveOrUpdateModel(shipmentMode);
                        MsgBox.success("Successfully Updated " + groupName);
                        CommonService.clearTextFields(new JTextField[]{shipmentNameText});
                        fillShipmentModeTable();
                        shipmentNameText.requestFocusInWindow();
                    }
                } else {
                    MsgBox.warning("Shipment Mode Name is mandatory.");
                    shipmentNameText.requestFocusInWindow();
                }
                shipmentMode = null;
            } else {
                MsgBox.warning("Shipment Mode Name '" + groupName + "' Not Found.");
                shipmentNameText.requestFocusInWindow();
            }

        } catch (Exception e) {
            log.error("updateButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_updateButtonActionPerformed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        CommonService.clearTextFields(new JTextField[]{shipmentNameText});
    }//GEN-LAST:event_newButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        Session session = null;
        try {
            String groupName = shipmentNameText.getText().trim();
            if (null != shipmentMode) {
                if (!shipmentMode.getName().equalsIgnoreCase("N/A")) {
                    try {
                        if (MsgBox.confirm("Are you sure you want to delete?")) {
                            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
                            session = sessionFactory.openSession();
                            Transaction transaction = session.beginTransaction();
                            session.delete(shipmentMode);
                            transaction.commit();
                            MsgBox.success("Successfully Deleted " + groupName);
                            CommonService.clearTextFields(new JTextField[]{shipmentNameText});
                            fillShipmentModeTable();
                            shipmentNameText.requestFocusInWindow();
                            shipmentMode = null;
                        }
                    } catch (Exception e) {
                        MsgBox.warning("Shipment Mode '" + groupName + "' is already used for transactions!");
                        shipmentNameText.requestFocusInWindow();
                    } finally {
                        session.clear();
                        session.close();
                    }
                }
            } else {
                MsgBox.warning("Shipment Mode Name '" + groupName + "' Not Found.");
                shipmentNameText.requestFocusInWindow();
            }
        } catch (Exception e) {
            log.error("deleteButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_deleteButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton deleteButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton newButton;
    private javax.swing.JTable shipmentModeListTable;
    private javax.swing.JTextField shipmentNameText;
    private javax.swing.JButton updateButton;
    // End of variables declaration//GEN-END:variables
}
