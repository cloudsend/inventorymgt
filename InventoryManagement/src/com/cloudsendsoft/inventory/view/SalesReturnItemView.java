/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.GodownDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.MultiCurrencyDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.dao.PackingTypeDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.dao.SalesReturnDAO;
import com.cloudsendsoft.inventory.dao.ShipmentModeDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.PackingList;
import com.cloudsendsoft.inventory.model.PackingListItem;
import com.cloudsendsoft.inventory.model.PackingType;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseCharge;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseReturn;
import com.cloudsendsoft.inventory.model.PurchaseReturnCharge;
import com.cloudsendsoft.inventory.model.PurchaseReturnItem;
import com.cloudsendsoft.inventory.model.PurchaseTax;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesCharge;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaCharge;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesPreformaTax;
import com.cloudsendsoft.inventory.model.SalesReturn;
import com.cloudsendsoft.inventory.model.SalesReturnCharge;
import com.cloudsendsoft.inventory.model.SalesReturnItem;
import com.cloudsendsoft.inventory.model.SalesTax;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import static com.cloudsendsoft.inventory.view.MainForm.jMainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class SalesReturnItemView extends javax.swing.JPanel {

    /**
     * Creates new form PackingListView
     */
    static final Logger log = Logger.getLogger(SalesReturnItemView.class.getName());
    SalesPreforma salesPreforma = null;
    SalesPreformaItem salesPreformaItem = null;
    PackingListItem packingListItem = null;
    PackingList packingList = null;
    DefaultTableModel salesItemTableModel = null;
    DefaultTableModel salesReturnItemTableModel = null;
    List<SalesPreformaItem> salesPreformaItemList = null;
    //MultiCurrency currency=null;
    List<SalesPreformaTax> salesPreformaTaxList = null;
    List<SalesPreformaCharge> salesPreformaChargesList = null;
    List<PackingListItem> packingListItemList = new ArrayList<PackingListItem>(0);
    NumberProperty numberPropertyPackingListInvoice = null;
    NumberProperty numberPropertyPackingListNumberOfPack = null;
    NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
    CommonService commonService = new CommonService();
    PackingTypeDAO packingDAO = new PackingTypeDAO();
    PackingType packing = null;
    CRUDServices cRUDServices = new CRUDServices();
    //boolean isPackingListTableClicked=false;
    boolean isPackingListNew = false;

    List<SalesItem> salesItemList = new ArrayList<SalesItem>();
    List<SalesReturnItem> salesReturnItemList = new ArrayList<SalesReturnItem>();
    List<SalesReturnItem> salesReturnItemRemovedList = new ArrayList<SalesReturnItem>();
    MultiCurrency currency = null;
    List<SalesTax> salesTaxList = null;
    List<SalesCharge> salesChargesList = null;
    //MultiCurrency currency=null;
    List<SalesTax> salesRetuenTaxList = new ArrayList<SalesTax>();
    List<SalesReturnCharge> salesReturnChargesList = new ArrayList<SalesReturnCharge>();
    int selectedIndex = -1;
    Sales sales = null;
    SalesReturn salesReturn = new SalesReturn();
    boolean isPackingListTableClicked = false;
    SalesItem salesItem = null;
    SalesReturnItem salesReturnItem = null;
    LedgerDAO ledgerDAO = new LedgerDAO();

    NumberProperty numberProperty = null;
    TaxDAO taxDAO = new TaxDAO();
    GodownDAO godownDAO = new GodownDAO();
    ChargesDAO chargesDAO = new ChargesDAO();
    MultiCurrencyDAO multiCurrencyDAO = new MultiCurrencyDAO();
    ShipmentModeDAO shipmentModeDAO = new ShipmentModeDAO();
    List<SalesReturn> salesReturnList = new ArrayList<SalesReturn>();
    SalesReturnDAO salesReturnDAO = new SalesReturnDAO();

    //Double itemQty = 0.0;
    
    Ledger salesReturnsLedger = null;

    double subTotal = 0.00;
    double subTotal1 = 0.00;
    Item item = null;
    double itemQty = 0.0;
    double itemQty1 = 0.0;
    double netTotal = 0.00;
    double totDiscount = 0.00;
    double grandTotal = 0.00;
    double netTotal1 = 0.00;
    double totDiscount1 = 0.00;
    double grandTotal1 = 0.00;

    double netReturnTotal = 0.00;
    double totReturnDiscount = 0.00;
    double grandReturnTotal = 0.00;
    
    double netReturnTotal1 = 0.00;
    double totReturnDiscount1 = 0.00;
    double grandReturnTotal1 = 0.00;

    int selectedRowIndex = -1;
    int check = 0;

    boolean isDuplicateColumnsVisible = false;
    
    public SalesReturnItemView(Sales sales) {

        initComponents();

        //shortcut
        // HERE ARE THE KEY BINDINGS
        itemQtyPanel.remove(itemQtyText1);
                    //discountPanel.remove(itemDiscountText1);
                    chrgsPanel.remove(chargesText1);
                    
                    itemQtyPanel.revalidate();
                    discountPanel.revalidate();
                    chrgsPanel.revalidate();
                    
                    
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                put(KeyStroke.getKeyStroke(KeyEvent.VK_W,
                                java.awt.event.InputEvent.SHIFT_DOWN_MASK
                                | java.awt.event.InputEvent.CTRL_DOWN_MASK), "forward");
        this.getActionMap().put("forward", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isDuplicateColumnsVisible) {
                    isDuplicateColumnsVisible = false;
                    itemQtyPanel.remove(itemQtyText1);
                   // discountPanel.remove(itemDiscountText1);
                    chrgsPanel.remove(chargesText1);
                    
                    itemQtyPanel.revalidate();
                   // discountPanel.revalidate();
                    chrgsPanel.revalidate();
                    

                    //set table design
                   salesItemTable.setRowHeight(GlobalProperty.tableRowHieght);
                    salesItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                            null,
                            new String[]{"Sr. No.", "Item Code", "Description", "Godown", "Qty.", "Unit Price", "Total Amount"}
                    ));
                    salesItemTableModel = (DefaultTableModel) salesItemTable.getModel();
                    CommonService.setWidthAsPercentages(salesItemTable, .03, .10, .20, .08, .08, .08, .08);

                    //set table design-2
                    salesReturnItemTable.setRowHeight(GlobalProperty.tableRowHieght);
                    salesReturnItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                            null,
                            new String[]{"Sr.No.", "Item Code", "Description", "Unit Price", "Qty.", "Total Amount"}
                    ));
                    salesReturnItemTableModel = (DefaultTableModel) salesReturnItemTable.getModel();
                    CommonService.setWidthAsPercentages(salesReturnItemTable, .03, .10, .20, .08, .08, .08);
                
                    } else {
                    isDuplicateColumnsVisible = true;
                    itemQtyPanel.add(itemQtyText1);
                   // discountPanel.add(itemDiscountText1);
                    chrgsPanel.add(chargesText1);
                    
                    itemQtyPanel.revalidate();
                    discountPanel.revalidate();
                    chrgsPanel.revalidate();
                    
                    //set table design
                    salesItemTable.setRowHeight(GlobalProperty.tableRowHieght);
                    salesItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                            null,
                            new String[]{"Sr. No.", "Item Code", "Description", "Godown", "Qty.", "Unit Price", "Qty1", "Unit Price1", "Total Amount"}
                    ));
                    salesItemTableModel = (DefaultTableModel) salesItemTable.getModel();
                    CommonService.setWidthAsPercentages(salesItemTable, .03, .10, .20, .08, .08, .08, .08, .08, .08);

                    //set table design-2
                    salesReturnItemTable.setRowHeight(GlobalProperty.tableRowHieght);
                    salesReturnItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                            null,
                            new String[]{"Sr.No.", "Item Code", "Description", "Qty.", "Unit Price", "Qty1", "Unit Price1", "Total Amount"}
                    ));
                    salesReturnItemTableModel = (DefaultTableModel) salesReturnItemTable.getModel();
                    CommonService.setWidthAsPercentages(salesReturnItemTable, .03, .10, .08, .08, .08, .08, .08, .08);
                
                    }
                fillSalesItemTable();
                fillSalesReturnTable();
            }
        });
        // END OF KEY BINDINGS
        //shortcut
        
        salesReturnsLedger = ledgerDAO.findByLedgerName("Sales Returns");
        
        commonService.setCurrentPeriodOnCalendar(pInvoiceDateFormattedText);

        if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
            saveButton.setEnabled(false);
        }

        for (Charges charges : chargesDAO.findAll()) {
            chargesComboBox.addItem(new ComboKeyValue(charges.getName(), charges));
        }

        for (ShipmentMode shipmentMode : shipmentModeDAO.findAll()) {
            shipmentComboBox.addItem(new ComboKeyValue(shipmentMode.getName(), shipmentMode));
        }
        pInvoiceDateFormattedText.setDate(new java.util.Date());
        numberProperty = numberPropertyDAO.findInvoiceNo("SalesReturn");
        pInvoiceNumberText.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
        this.sales = sales;
        //table background color removed
        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);
        jScrollPane2.setOpaque(false);
        jScrollPane2.getViewport().setOpaque(false);

        salesItemTable.setRowHeight(GlobalProperty.tableRowHieght);
        salesItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{"Sr.No.", "Item Code", "Description", "Godown", "Qty.", "Unit Price", "Total Amount"}
        ));
        salesItemTableModel = (DefaultTableModel) salesItemTable.getModel();
        CommonService.setWidthAsPercentages(salesItemTable, .03, .10, .20, .08, .08, .08, .08);

        //salesPreformaItemList=salesPreforma.getSalesPreformaItems();
        salesItemList = sales.getSalesItems();
        salesTaxList = sales.getSalesTaxs();
        salesChargesList = sales.getSalesCharges();
        //=salesPreforma.getSalesPreformaTaxs();
        //salesPreformaChargesList=salesPreforma.getSalesPreformaCharges();
        fillSalesItemTable();

        //packing list table design
        salesReturnItemTable.setRowHeight(GlobalProperty.tableRowHieght);
        salesReturnItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{"Sr.No.", "Item Code", "Description", "Qty.", "Unit Price","Total Amount"}
        ));
        salesReturnItemTableModel = (DefaultTableModel) salesReturnItemTable.getModel();
        CommonService.setWidthAsPercentages(salesReturnItemTable, .03, .10, .20, .08, .08, .08);

    }

    public void listSalesReturnItemHistory(SalesReturn salesReturn) {
        //Hide the fields
        // leftSidePanel.setVisible(false);
        // purchaseItemTable.setVisible(false);

        this.salesReturn = salesReturn;
        this.sales = salesReturn.getSales();
        //this.PurchaseReturnItemView(purchase);
        //this.SupplierLedger=purchase.getSupplier();
        salesReturnItemList = salesReturn.getSalesReturnItems();
        //purchaseReturnTaxList=purchase.getPurchaseTaxes();
        salesReturnChargesList = salesReturn.getSalesReturnCharges();
        billDateFormattedText.setText(commonService.sqlDateToString(salesReturn.getBillDate()));
        invoiceNumberText.setText(salesReturn.getSales().getInvoiceNumber());
        //.setText(purchaseReturn.getDeliveryDate()+"");
        customerText.setText(salesReturn.getCustomer().getLedgerName());
        // System.out.println("salesPreforma.getCustomer():"+purchase.getSupplier().getName());
        deliveryText.setText(salesReturn.getDeliveryDate() + "");
        addressTextArea.setText(salesReturn.getCustomer().getAddress());
        telNoText.setText(salesReturn.getCustomer().getTelephone());
        pInvoiceNumberText.setText(salesReturn.getInvoiceNumber());

        //Updating availableQty of Supplier Ledger
        //this.customerLedger = sales.getCustomer();
        if (salesReturn.getCustomer().getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
            salesReturn.getCustomer().setAvailableBalance(salesReturn.getCustomer().getAvailableBalance() - salesReturn.getGrandTotal());
        } else if (salesReturn.getCustomer().getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
            salesReturn.getCustomer().setAvailableBalance(salesReturn.getCustomer().getAvailableBalance() + salesReturn.getGrandTotal());
        }

        //addressTextArea.setText(purchase.getSupplier().getAddress());
        int count1 = shipmentComboBox.getItemCount();
        if (salesReturn.getShipmentMode() != null) {
            for (int i = 0; i < count1; i++) {
                ComboKeyValue ckv = (ComboKeyValue) shipmentComboBox.getItemAt(i);
                if (ckv.getKey().trim().equalsIgnoreCase(salesReturn.getShipmentMode().getName())) {
                    shipmentComboBox.setSelectedItem(ckv);
                }
            }
        }
        saveButton.setText("Update");
        check = 1;
        fillSalesReturnTable();

    }

    void fillSalesReturnTable() {

        System.out.println("Inside fill table");
        int slNo = 1;
        netReturnTotal = 0.0;
        subTotal=0.00;
        netReturnTotal1 = 0.0;
        subTotal1=0.00;
        currency = sales.getMultiCurrency();
        salesReturnItemTableModel.setRowCount(0);
        String currencySymbol = (null == currency) ? "" : currency.getSymbol();
        for (SalesReturnItem pItem : salesReturnItemList) {
            double itemReturnTotal = ((pItem.getUnitPrice() / sales.getCurrentCurrencyRate()) - (pItem.getDiscount() / sales.getCurrentCurrencyRate())) * pItem.getQuantity();
            double itemReturnTotal1 = ((pItem.getUnitPrice() / sales.getCurrentCurrencyRate()) - (pItem.getDiscount() / sales.getCurrentCurrencyRate())) * pItem.getQuantity1();
            if (isDuplicateColumnsVisible) {
                if (itemReturnTotal > 0 || itemReturnTotal1 > 0) {
                    salesReturnItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getQuantity(), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / sales.getCurrentCurrencyRate()), pItem.getQuantity1(), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / sales.getCurrentCurrencyRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemReturnTotal + itemReturnTotal1)});
                }
            } else {
                if (itemReturnTotal > 0) {
                    salesReturnItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getQuantity(), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / sales.getCurrentCurrencyRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemReturnTotal)});
                }
            }
            netReturnTotal += itemReturnTotal;
            netReturnTotal1 += itemReturnTotal1;

            subTotal += itemReturnTotal;
            subTotal1 += itemReturnTotal1;
        }
        //adding tax,chrgs,etc
        if (netReturnTotal > 0 ||netReturnTotal1 > 0) {
            for (SalesReturnCharge salesReturnCharge : salesReturnChargesList) {
                 if (isDuplicateColumnsVisible) {
                    salesReturnItemTableModel.addRow(new Object[]{"", "", "\t" + salesReturnCharge.getCharges().getName(), "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString((salesReturnCharge.getAmount1() / sales.getCurrentCurrencyRate()) + (salesReturnCharge.getAmount() / sales.getCurrentCurrencyRate()))});
                } else {
                    salesReturnItemTableModel.addRow(new Object[]{"", "", "\t" + salesReturnCharge.getCharges().getName(), "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(salesReturnCharge.getAmount() / sales.getCurrentCurrencyRate())});
                }

                netReturnTotal += salesReturnCharge.getAmount() / sales.getCurrentCurrencyRate();
                netReturnTotal1 += salesReturnCharge.getAmount1() / sales.getCurrentCurrencyRate();
            }
        }
        grandReturnTotal = netReturnTotal;//-totReturnDiscount;
        grandReturnTotal1 = netReturnTotal1;

        //adding netTotal & grandTotal
        if (netReturnTotal > 0 || netReturnTotal1 > 0 || grandReturnTotal > 0 || grandReturnTotal1 > 0) {
            if (isDuplicateColumnsVisible) {
                salesReturnItemTableModel.addRow(new Object[]{"", "", "Net Total", "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(netReturnTotal + netReturnTotal1)});
                salesReturnItemTableModel.addRow(new Object[]{"", "", "<html><b>Grand Total</b></html>", "", "", "", "", "<html><b>" + currencySymbol + " " + commonService.formatIntoCurrencyAsString(grandReturnTotal + grandReturnTotal1) + "</b></html>"});

            } else {
                salesReturnItemTableModel.addRow(new Object[]{"", "", "Net Total", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(netReturnTotal)});
                salesReturnItemTableModel.addRow(new Object[]{"", "", "<html><b>Grand Total</b></html>", "", "", "<html><b>" + currencySymbol + " " + commonService.formatIntoCurrencyAsString(grandReturnTotal) + "</b></html>"});

            }
        }
        commonService.clearTextFields(new JTextField[]{pItemDescriptionText});
        commonService.clearNumberFields(new JTextField[]{pItemWeightText, itemQtyText,itemQtyText1, itemUnitPriceText,itemQtyText1,itemDiscountText});

    }

    void fillSalesItemTable() {
        //adding items on prefoma table
        netTotal = 0.00;
        netTotal1 = 0.00;
        salesItemTableModel.setRowCount(0);
        int slNo = 1;
        currency = sales.getMultiCurrency();
        String currencySymbol = (null == currency) ? "" : currency.getSymbol();
        for (SalesItem pItem : salesItemList) {
            double itemTotal = ((pItem.getUnitPrice() / sales.getCurrentCurrencyRate()) - (pItem.getDiscount() / sales.getCurrentCurrencyRate())) * pItem.getQuantity();
            double itemTotal1 = ((pItem.getUnitPrice() / sales.getCurrentCurrencyRate()) - (pItem.getDiscount() / sales.getCurrentCurrencyRate())) * pItem.getQuantity1();

            if (isDuplicateColumnsVisible) {
                    if (itemTotal1 > 0 || itemTotal > 0) { //only add to jtable if tota is >0
                        salesItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getItem().getGodown().getName(), pItem.getQuantity(), currency.getSymbol() + " " + (pItem.getUnitPrice() / currency.getRate()), pItem.getQuantity1(), currency.getSymbol() + " " + (pItem.getUnitPrice() / currency.getRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal + itemTotal1)});
                    }
                } else {
                    if (itemTotal > 0) {
                        salesItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getItem().getGodown().getName(), pItem.getQuantity(), currency.getSymbol() + " " + (pItem.getUnitPrice() / currency.getRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal)});
                    }
                }

                netTotal += itemTotal;
                netTotal1 += itemTotal1;
            }
        //adding tax,chrgs,etc
        if (netTotal > 0 || netTotal1 > 0) {
            for (SalesTax salesTax : salesTaxList) {
                if (isDuplicateColumnsVisible) {
                        salesItemTableModel.addRow(new Object[]{"", "", "\t" + salesTax.getTax().getName(), "", "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(salesTax.getAmount() / sales.getCurrentCurrencyRate())});
                    } else {
                        salesItemTableModel.addRow(new Object[]{"", "", "\t" + salesTax.getTax().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(salesTax.getAmount() / sales.getCurrentCurrencyRate())});
                    }
                netTotal += salesTax.getAmount() / sales.getCurrentCurrencyRate();
            }
            for (SalesCharge salesCharge : salesChargesList) {
                if (isDuplicateColumnsVisible) {
                        salesItemTableModel.addRow(new Object[]{"", "", "\t" + salesCharge.getCharges().getName(), "", "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString((salesCharge.getAmount() / sales.getCurrentCurrencyRate()) + (salesCharge.getAmount1() / sales.getCurrentCurrencyRate()))});
                    } else {
                        salesItemTableModel.addRow(new Object[]{"", "", "\t" + salesCharge.getCharges().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(salesCharge.getAmount() / sales.getCurrentCurrencyRate())});
                    }
                netTotal += salesCharge.getAmount() / sales.getCurrentCurrencyRate();
                netTotal1 += salesCharge.getAmount1() / sales.getCurrentCurrencyRate();
            }
        }
        totDiscount = sales.getDiscount() / sales.getCurrentCurrencyRate();
        grandTotal = netTotal - totDiscount;
        totDiscount1 = sales.getDiscount1() / sales.getCurrentCurrencyRate();
        grandTotal1 = netTotal1 - totDiscount1;
        if (totDiscount > 0 ||totDiscount1>0) {
            if (isDuplicateColumnsVisible) {
                    salesItemTableModel.addRow(new Object[]{"", "", "Discount", "", "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(totDiscount + totDiscount1)});
                } else {
                    salesItemTableModel.addRow(new Object[]{"", "", "Discount", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(totDiscount)});
                }
        }

        //adding netTotal & grandTotal
        if (isDuplicateColumnsVisible) {
                salesItemTableModel.addRow(new Object[]{"", "", "Net Total", "", "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(netTotal + netTotal1)});
                salesItemTableModel.addRow(new Object[]{"", "", "<html><b>Grand Total</b></html>", "", "", "", "", "", "<html><b>" + currencySymbol + " " + commonService.formatIntoCurrencyAsString(grandTotal + grandTotal1) + "</b></html>"});

            } else {
               salesItemTableModel.addRow(new Object[]{"", "", "Net Total", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(netTotal)});
               salesItemTableModel.addRow(new Object[]{"", "", "<html><b>Grand Total</b></html>", "", "", "", "<html><b>" + currencySymbol + " " + commonService.formatIntoCurrencyAsString(grandTotal) + "</b></html>"});

            }
        billDateFormattedText.setText(commonService.sqlDateToString(sales.getBillDate()));
        invoiceNumberText.setText(sales.getInvoiceNumber());
        customerText.setText(sales.getCustomer().getLedgerName());
        deleveryTermsText.setText(sales.getDeliveryDate() + "");
        //paymentTermsText.setText(purchase.());
        addressTextArea.setText(sales.getCustomer().getAddress());
        telNoText.setText(sales.getCustomer().getTelephone());

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headingPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        bodyPanel = new javax.swing.JPanel();
        leftSidePanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        salesItemTable = new javax.swing.JTable();
        salesPrefomaBottomPanel = new javax.swing.JPanel();
        salesPrefomaDetailPanel = new javax.swing.JPanel();
        billDateFormattedText = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        invoiceNumberText = new javax.swing.JTextField();
        customerText = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        deliveryText = new javax.swing.JTextField();
        paymentTermsText = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        deleveryTermsText = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        addressTextArea = new javax.swing.JTextArea();
        jLabel15 = new javax.swing.JLabel();
        telNoText = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        cancelButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();
        maxMinPreformaButton = new javax.swing.JButton();
        rightSidePanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        salesReturnItemTable = new javax.swing.JTable();
        packingListBottomPanel = new javax.swing.JPanel();
        packingDetailsPanel = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        pInvoiceNumberText = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        pItemDescriptionText = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        pItemWeightText = new javax.swing.JFormattedTextField();
        pItemAddButton = new javax.swing.JButton();
        pItemMinusButton1 = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        chargesPanel = new javax.swing.JPanel();
        chargesComboBox = new javax.swing.JComboBox();
        chargesAddButton = new javax.swing.JButton();
        chargesMinusButton = new javax.swing.JButton();
        chrgsPanel = new javax.swing.JPanel();
        chargesText = new javax.swing.JFormattedTextField();
        chargesText1 = new javax.swing.JFormattedTextField();
        shipmentModePanel = new javax.swing.JPanel();
        shipmentComboBox = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        itemUnitPriceText = new javax.swing.JFormattedTextField();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        pInvoiceDateFormattedText = new com.toedter.calendar.JDateChooser();
        sRtrndeliveryText = new javax.swing.JTextField();
        saveButton = new javax.swing.JButton();
        itemQtyPanel = new javax.swing.JPanel();
        itemQtyText1 = new javax.swing.JFormattedTextField();
        itemQtyText = new javax.swing.JFormattedTextField();
        discountPanel = new javax.swing.JPanel();
        itemDiscountText = new javax.swing.JFormattedTextField();
        maxMinPackingListButton = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        headingPanel.setBackground(new java.awt.Color(134, 0, 0));
        headingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        headingPanel.setLayout(new javax.swing.BoxLayout(headingPanel, javax.swing.BoxLayout.LINE_AXIS));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 153));
        jLabel1.setText("Sales Return Items");
        headingPanel.add(jLabel1);

        add(headingPanel, java.awt.BorderLayout.PAGE_START);

        bodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        bodyPanel.setLayout(new java.awt.GridLayout(1, 0));

        leftSidePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        leftSidePanel.setOpaque(false);

        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder("Saled Items"));
        jScrollPane1.setOpaque(false);

        salesItemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        salesItemTable.setOpaque(false);
        salesItemTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                salesItemTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(salesItemTable);

        salesPrefomaBottomPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        salesPrefomaBottomPanel.setOpaque(false);

        salesPrefomaDetailPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("User Information"));
        salesPrefomaDetailPanel.setOpaque(false);

        billDateFormattedText.setEditable(false);
        billDateFormattedText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));
        billDateFormattedText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                billDateFormattedTextActionPerformed(evt);
            }
        });

        jLabel6.setText("Bill Date");

        jLabel5.setText("Invoice Number");

        invoiceNumberText.setEditable(false);
        invoiceNumberText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                invoiceNumberTextActionPerformed(evt);
            }
        });
        invoiceNumberText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                invoiceNumberTextKeyPressed(evt);
            }
        });

        customerText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customerTextActionPerformed(evt);
            }
        });
        customerText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                customerTextKeyPressed(evt);
            }
        });

        jLabel13.setText("Customer");

        jLabel14.setText("Delivery");

        deliveryText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deliveryTextActionPerformed(evt);
            }
        });
        deliveryText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                deliveryTextKeyPressed(evt);
            }
        });

        paymentTermsText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                paymentTermsTextKeyPressed(evt);
            }
        });

        jLabel12.setText("Payment Terms");

        jLabel11.setText("Delivery Terms");

        deleveryTermsText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleveryTermsTextActionPerformed(evt);
            }
        });
        deleveryTermsText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                deleveryTermsTextKeyPressed(evt);
            }
        });

        jLabel2.setText("Address :");

        addressTextArea.setColumns(20);
        addressTextArea.setLineWrap(true);
        addressTextArea.setRows(5);
        jScrollPane3.setViewportView(addressTextArea);

        jLabel15.setText("Tel No:");

        javax.swing.GroupLayout salesPrefomaDetailPanelLayout = new javax.swing.GroupLayout(salesPrefomaDetailPanel);
        salesPrefomaDetailPanel.setLayout(salesPrefomaDetailPanelLayout);
        salesPrefomaDetailPanelLayout.setHorizontalGroup(
            salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(paymentTermsText)
                    .addComponent(deliveryText)
                    .addComponent(customerText)
                    .addComponent(invoiceNumberText)
                    .addComponent(billDateFormattedText))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(deleveryTermsText)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(telNoText))
                .addGap(1, 1, 1))
        );
        salesPrefomaDetailPanelLayout.setVerticalGroup(
            salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, salesPrefomaDetailPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                        .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(deleveryTermsText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addComponent(jLabel2))))
                    .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(telNoText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                            .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(billDateFormattedText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(11, 11, 11)
                            .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(invoiceNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(customerText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(deliveryText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel14))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(paymentTermsText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel12)))))
                .addContainerGap())
        );

        jPanel1.setOpaque(false);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/cancel.png"))); // NOI18N
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        backButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/back.png"))); // NOI18N
        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cancelButton, javax.swing.GroupLayout.DEFAULT_SIZE, 132, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(backButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(72, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout salesPrefomaBottomPanelLayout = new javax.swing.GroupLayout(salesPrefomaBottomPanel);
        salesPrefomaBottomPanel.setLayout(salesPrefomaBottomPanelLayout);
        salesPrefomaBottomPanelLayout.setHorizontalGroup(
            salesPrefomaBottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(salesPrefomaBottomPanelLayout.createSequentialGroup()
                .addComponent(salesPrefomaDetailPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        salesPrefomaBottomPanelLayout.setVerticalGroup(
            salesPrefomaBottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, salesPrefomaBottomPanelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(salesPrefomaDetailPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, salesPrefomaBottomPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        maxMinPreformaButton.setText(">>");
        maxMinPreformaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maxMinPreformaButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout leftSidePanelLayout = new javax.swing.GroupLayout(leftSidePanel);
        leftSidePanel.setLayout(leftSidePanelLayout);
        leftSidePanelLayout.setHorizontalGroup(
            leftSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, leftSidePanelLayout.createSequentialGroup()
                .addGroup(leftSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 501, Short.MAX_VALUE)
                    .addComponent(maxMinPreformaButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(salesPrefomaBottomPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        leftSidePanelLayout.setVerticalGroup(
            leftSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(leftSidePanelLayout.createSequentialGroup()
                .addComponent(maxMinPreformaButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(salesPrefomaBottomPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        bodyPanel.add(leftSidePanel);

        rightSidePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        rightSidePanel.setOpaque(false);

        jScrollPane2.setBorder(javax.swing.BorderFactory.createTitledBorder("Items to be Returned"));
        jScrollPane2.setOpaque(false);

        salesReturnItemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        salesReturnItemTable.setOpaque(false);
        salesReturnItemTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                salesReturnItemTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(salesReturnItemTable);

        packingListBottomPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        packingListBottomPanel.setOpaque(false);

        packingDetailsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Item to return"));
        packingDetailsPanel.setOpaque(false);

        jLabel7.setText("Invoice Number");

        jLabel8.setText("Item Descreption");

        jLabel16.setText("Item Quandity");

        jLabel19.setText("Delivery");

        jLabel20.setText("Item Weight");

        pItemWeightText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        pItemWeightText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        pItemWeightText.setText("0.00");
        pItemWeightText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                pItemWeightTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                pItemWeightTextFocusLost(evt);
            }
        });
        pItemWeightText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                pItemWeightTextKeyTyped(evt);
            }
        });

        pItemAddButton.setText("+");
        pItemAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pItemAddButtonActionPerformed(evt);
            }
        });

        pItemMinusButton1.setText("-");
        pItemMinusButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pItemMinusButton1ActionPerformed(evt);
            }
        });

        jLabel21.setText("Invoice Date");

        chargesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Charges"));
        chargesPanel.setOpaque(false);

        chargesAddButton.setText("+");
        chargesAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesAddButtonActionPerformed(evt);
            }
        });

        chargesMinusButton.setText("-");
        chargesMinusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesMinusButtonActionPerformed(evt);
            }
        });

        chrgsPanel.setLayout(new java.awt.GridLayout(1, 0));

        chargesText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        chargesText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        chargesText.setText("0.00");
        chargesText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesTextFocusGained(evt);
            }
        });
        chargesText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                chargesTextKeyTyped(evt);
            }
        });
        chrgsPanel.add(chargesText);

        chargesText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        chargesText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        chargesText1.setText("0.00");
        chargesText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesText1FocusGained(evt);
            }
        });
        chargesText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                chargesText1KeyTyped(evt);
            }
        });
        chrgsPanel.add(chargesText1);

        javax.swing.GroupLayout chargesPanelLayout = new javax.swing.GroupLayout(chargesPanel);
        chargesPanel.setLayout(chargesPanelLayout);
        chargesPanelLayout.setHorizontalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, chargesPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chargesComboBox, 0, 164, Short.MAX_VALUE)
                    .addComponent(chrgsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        chargesPanelLayout.setVerticalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chargesPanelLayout.createSequentialGroup()
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chargesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chrgsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        shipmentModePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        shipmentModePanel.setOpaque(false);

        jLabel3.setText("Shipment Mode");

        javax.swing.GroupLayout shipmentModePanelLayout = new javax.swing.GroupLayout(shipmentModePanel);
        shipmentModePanel.setLayout(shipmentModePanelLayout);
        shipmentModePanelLayout.setHorizontalGroup(
            shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, shipmentModePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(shipmentComboBox, 0, 127, Short.MAX_VALUE)
                .addContainerGap())
        );
        shipmentModePanelLayout.setVerticalGroup(
            shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(shipmentModePanelLayout.createSequentialGroup()
                .addGroup(shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(shipmentComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        itemUnitPriceText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        itemUnitPriceText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemUnitPriceText.setText("0.00");
        itemUnitPriceText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemUnitPriceTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemUnitPriceTextFocusLost(evt);
            }
        });
        itemUnitPriceText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemUnitPriceTextKeyTyped(evt);
            }
        });

        jLabel22.setText("Unit Price");

        jLabel23.setText("Discount");

        pInvoiceDateFormattedText.setDateFormatString("dd/MM/yyyy");

        sRtrndeliveryText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sRtrndeliveryTextActionPerformed(evt);
            }
        });
        sRtrndeliveryText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                sRtrndeliveryTextKeyPressed(evt);
            }
        });

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        itemQtyPanel.setLayout(new java.awt.GridLayout(1, 0));

        itemQtyText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        itemQtyText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemQtyText1.setText("0.00");
        itemQtyText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemQtyText1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemQtyText1FocusLost(evt);
            }
        });
        itemQtyText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemQtyText1KeyTyped(evt);
            }
        });
        itemQtyPanel.add(itemQtyText1);

        itemQtyText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        itemQtyText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemQtyText.setText("0.00");
        itemQtyText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemQtyTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemQtyTextFocusLost(evt);
            }
        });
        itemQtyText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemQtyTextKeyTyped(evt);
            }
        });
        itemQtyPanel.add(itemQtyText);

        discountPanel.setLayout(new java.awt.GridLayout(1, 0));

        itemDiscountText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        itemDiscountText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemDiscountText.setText("0.00");
        itemDiscountText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemDiscountTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemDiscountTextFocusLost(evt);
            }
        });
        itemDiscountText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemDiscountTextKeyTyped(evt);
            }
        });
        discountPanel.add(itemDiscountText);

        javax.swing.GroupLayout packingDetailsPanelLayout = new javax.swing.GroupLayout(packingDetailsPanel);
        packingDetailsPanel.setLayout(packingDetailsPanelLayout);
        packingDetailsPanelLayout.setHorizontalGroup(
            packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, packingDetailsPanelLayout.createSequentialGroup()
                                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(pInvoiceDateFormattedText, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, packingDetailsPanelLayout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(pInvoiceNumberText)))
                        .addGap(10, 10, 10)
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                                .addComponent(sRtrndeliveryText)
                                .addGap(4, 4, 4))
                            .addComponent(discountPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel20, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(pItemWeightText, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(pItemDescriptionText, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(itemUnitPriceText)))
                            .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(itemQtyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(chargesPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(5, 5, 5))
                            .addComponent(shipmentModePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                        .addComponent(pItemAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pItemMinusButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(saveButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        packingDetailsPanelLayout.setVerticalGroup(
            packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pInvoiceDateFormattedText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(discountPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(pInvoiceNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(sRtrndeliveryText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(10, 10, 10)
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                                .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(pItemDescriptionText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(10, 10, 10)
                                .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(pItemWeightText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(10, 10, 10)
                                .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(itemQtyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(chargesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pItemAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pItemMinusButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(41, 41, 41)
                        .addComponent(saveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11)
                .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(itemUnitPriceText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(shipmentModePanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout packingListBottomPanelLayout = new javax.swing.GroupLayout(packingListBottomPanel);
        packingListBottomPanel.setLayout(packingListBottomPanelLayout);
        packingListBottomPanelLayout.setHorizontalGroup(
            packingListBottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(packingListBottomPanelLayout.createSequentialGroup()
                .addComponent(packingDetailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        packingListBottomPanelLayout.setVerticalGroup(
            packingListBottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, packingListBottomPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(packingDetailsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        maxMinPackingListButton.setText("<<");
        maxMinPackingListButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maxMinPackingListButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout rightSidePanelLayout = new javax.swing.GroupLayout(rightSidePanel);
        rightSidePanel.setLayout(rightSidePanelLayout);
        rightSidePanelLayout.setHorizontalGroup(
            rightSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rightSidePanelLayout.createSequentialGroup()
                .addGroup(rightSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(maxMinPackingListButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, rightSidePanelLayout.createSequentialGroup()
                        .addGroup(rightSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(packingListBottomPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE))
                        .addGap(2, 2, 2)))
                .addGap(0, 0, 0))
        );
        rightSidePanelLayout.setVerticalGroup(
            rightSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rightSidePanelLayout.createSequentialGroup()
                .addComponent(maxMinPackingListButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(packingListBottomPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        bodyPanel.add(rightSidePanel);

        add(bodyPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void maxMinPackingListButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maxMinPackingListButtonActionPerformed
        try {
            if (maxMinPackingListButton.getText().equalsIgnoreCase("<<")) {
                bodyPanel.removeAll();
                bodyPanel.add(rightSidePanel);
                bodyPanel.revalidate();
                bodyPanel.repaint();
                maxMinPackingListButton.setText(">>");
            } else {

                bodyPanel.removeAll();
                bodyPanel.add(leftSidePanel);
                bodyPanel.add(rightSidePanel);
                bodyPanel.revalidate();
                bodyPanel.repaint();
                maxMinPackingListButton.setText("<<");

            }
        } catch (Exception e) {
            log.error("maxMinPackingListButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_maxMinPackingListButtonActionPerformed

    private void maxMinPreformaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maxMinPreformaButtonActionPerformed
        try {
            if (maxMinPreformaButton.getText().equalsIgnoreCase(">>")) {
                bodyPanel.removeAll();
                bodyPanel.add(leftSidePanel);
                bodyPanel.revalidate();
                bodyPanel.repaint();
                maxMinPreformaButton.setText("<<");
            } else {
                bodyPanel.removeAll();
                bodyPanel.add(leftSidePanel);
                bodyPanel.add(rightSidePanel);
                bodyPanel.revalidate();
                bodyPanel.repaint();
                maxMinPreformaButton.setText(">>");
            }
        } catch (Exception e) {
            log.error("maxMinPreformaButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_maxMinPreformaButtonActionPerformed

    private void invoiceNumberTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_invoiceNumberTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_invoiceNumberTextActionPerformed

    private void invoiceNumberTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_invoiceNumberTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_invoiceNumberTextKeyPressed

    private void customerTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customerTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customerTextActionPerformed

    private void customerTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_customerTextKeyPressed

    }//GEN-LAST:event_customerTextKeyPressed

    private void deliveryTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deliveryTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deliveryTextActionPerformed

    private void deliveryTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_deliveryTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_deliveryTextKeyPressed

    private void paymentTermsTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_paymentTermsTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_paymentTermsTextKeyPressed

    private void deleveryTermsTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleveryTermsTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleveryTermsTextActionPerformed

    private void deleveryTermsTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_deleveryTermsTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleveryTermsTextKeyPressed

    private void salesItemTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salesItemTableMouseClicked
        isPackingListTableClicked = false;
        salesReturnList = salesReturnDAO.findAllSalesReturn(sales);
        int selectedRowIndex = salesItemTable.rowAtPoint(evt.getPoint());
        if (salesItemList.size() >= selectedRowIndex) {
            salesItem = salesItemList.get(selectedRowIndex);
            itemQty = salesItem.getQuantity();
            itemQty1 = salesItem.getQuantity1();
            pItemDescriptionText.setText(salesItem.getItem().getDescription());
            pItemWeightText.setText(salesItem.getItem().getWeight() + "");

            for (SalesReturn salesReturn : salesReturnList) {

                for (SalesReturnItem salesReturnItem : salesReturn.getSalesReturnItems()) {
                    //System.out.println(" isssssssss id1"+purchaseItem.getItem().getId());
                    // System.out.println(" isssssssss id1"+purchaseReturnItem.getItem().getId());
                    if (salesReturnItem.getItem().getId() == salesItem.getItem().getId()) {
                        //System.out.println(" isssssssss id"+purchaseItem.getItem().getId());
                        //System.out.println(" isssssssss id"+purchaseReturnItem.getItem().getId());
                        //purchaseItem.setQuantity(purchaseItem.getQuantity()-purchaseReturnItem.getQuantity());
                        itemQty -= salesReturnItem.getQuantity();
                        itemQty1 -= salesReturnItem.getQuantity1();
                    }
                }
            }
            itemQtyText.setText(itemQty + "");
            itemUnitPriceText.setText(commonService.formatIntoCurrencyAsString(salesItem.getUnitPrice() / sales.getCurrentCurrencyRate()));
            itemDiscountText.setText(commonService.formatIntoCurrencyAsString(salesItem.getDiscount() / sales.getCurrentCurrencyRate()));
            
            if(isDuplicateColumnsVisible)
            {
            itemQtyText1.setText(itemQty1 + "");
            //itemUnitPriceText1.setText(commonService.formatIntoCurrencyAsString(salesItem.getUnitPrice() / sales.getCurrentCurrencyRate()));
            //itemDiscountText1.setText(commonService.formatIntoCurrencyAsString(sales.getDiscount1() / sales.getCurrentCurrencyRate()));
             
            }
            
            item = salesItem.getItem();
        } else {
            commonService.clearTextFields(new JTextField[]{pItemDescriptionText, pInvoiceNumberText});
            commonService.clearCurrencyFields(new JTextField[]{pItemWeightText, pItemWeightText, itemQtyText, itemUnitPriceText});
            //  pUnitLabel.setText("");
            salesPreformaItem = null;
        }
    }//GEN-LAST:event_salesItemTableMouseClicked

    private void pItemWeightTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pItemWeightTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_pItemWeightTextKeyTyped

    private void pItemWeightTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pItemWeightTextFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_pItemWeightTextFocusLost

    private void pItemWeightTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pItemWeightTextFocusGained
        pItemWeightText.selectAll();
    }//GEN-LAST:event_pItemWeightTextFocusGained

    private void itemQtyTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_itemQtyTextKeyTyped

    private void itemQtyTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyTextFocusLost
        //pNetWeightText.setText(Double.parseDouble(itemQtyText.getText())*Double.parseDouble(pItemWeightText.getText())+"");
        //pGrossWeightText.setText(Double.parseDouble(pNetWeightText.getText())+Double.parseDouble(itemUnitPriceText.getText())+"");
    }//GEN-LAST:event_itemQtyTextFocusLost

    private void itemQtyTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyTextFocusGained
        itemQtyText.selectAll();
    }//GEN-LAST:event_itemQtyTextFocusGained

    private void pItemAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pItemAddButtonActionPerformed

        try {
            if (CommonService.stringValidator(new String[]{pItemDescriptionText.getText()}) && pItemDescriptionText.getText().trim().length() > 0) {
                double itemQtyNow = Double.parseDouble(itemQtyText.getText());
                double itemQtyNow1 = Double.parseDouble(itemQtyText1.getText());
                double itemUnitPrice = Double.parseDouble(itemUnitPriceText.getText());

                if (itemQtyNow <= (itemQty) && itemQtyNow > 0 || itemQtyNow1 <= (itemQty1) && itemQtyNow1 > 0) {
                    if(itemQtyNow>0)
                    {
                    salesItem.setQuantity(itemQty);
                    }
                    if(itemQtyNow1>0)
                    {
                    salesItem.setQuantity1(itemQty1);
                    }

                    SalesReturnItem salesReturnItem = null;
                    for (SalesReturnItem pItem : salesReturnItemList) {
                        if (pItem.getItem().getItemCode().equalsIgnoreCase(item.getItemCode()) && pItem.getItem().getGodown().getName().equalsIgnoreCase(item.getGodown().getName())) {
                            salesReturnItem = pItem;
                            break;
                        }
                    }

                    if (null == salesReturnItem) {
                        salesReturnItem = new SalesReturnItem();
                        salesReturnItemList.add(salesReturnItem);
                    }

                    salesReturnItem.setItem(item);
                    salesReturnItem.setQuantity(itemQtyNow);
                    salesReturnItem.setQuantity1(itemQtyNow1);
                    salesReturnItem.setUnitPrice(salesItem.getUnitPrice());
                    salesReturnItem.setDiscount(salesItem.getDiscount() * sales.getCurrentCurrencyRate());
                    fillSalesReturnTable();
                    //purchaseItem=null;
                } else {
                    MsgBox.warning("You do not have enough quandity to return");
                }

            } else {
                MsgBox.warning("No Item Selected.");
            }
        } catch (Exception e) {
            log.error("addItemButtonActionPerformed:", e);
        }

    }//GEN-LAST:event_pItemAddButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed

        try {
            if (sales != null ) {
                if (MsgBox.confirm("Are you sure you want to save?")) {
                    if (salesReturn == null) {
                        salesReturn = new SalesReturn();
                        check = 0;
                    }
                    salesReturn.setCustomer(sales.getCustomer());
                    salesReturn.setBillDate(commonService.utilDateToSqlDate(pInvoiceDateFormattedText.getDate()));
                    salesReturn.setInvoiceNumber(pInvoiceNumberText.getText());
                    salesReturn.setGrandTotal(grandReturnTotal * sales.getCurrentCurrencyRate());
                    salesReturn.setGrandTotal1(grandReturnTotal1 * sales.getCurrentCurrencyRate());
                    
                    salesReturn.setMultiCurrency(sales.getMultiCurrency());
                    Object shipmentMod = shipmentComboBox.getSelectedItem();
                    ShipmentMode shipmentMode = (ShipmentMode) ((ComboKeyValue) shipmentMod).getValue();
                    if (shipmentMode.getName().equalsIgnoreCase("N/A")) {
                        salesReturn.setShipmentMode(null);
                    }else{
                        salesReturn.setShipmentMode(shipmentMode);
                    }
                    salesReturn.setDeliveryDate(sRtrndeliveryText.getText());

                    salesReturn.setSalesReturnItems(salesReturnItemList);
                    salesReturn.setSales(sales);

                    salesReturn.setSalesReturnCharges(salesReturnChargesList);
                    salesReturn.setCompany(GlobalProperty.getCompany());
                    if (check == 0) {
                        cRUDServices.saveModel(salesReturn);
                        numberProperty.setNumber(numberProperty.getNumber() + 1);
                        cRUDServices.saveOrUpdateModel(numberProperty);
                    } else {
                        cRUDServices.saveOrUpdateModel(salesReturn);
                    }
                    //Updating availableQty of Supplier Ledger
                    //this.customerLedger = sales.getCustomer();
                    if (salesReturn.getCustomer().getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                        salesReturn.getCustomer().setAvailableBalance(salesReturn.getCustomer().getAvailableBalance() + grandReturnTotal * sales.getCurrentCurrencyRate());
                        salesReturn.getCustomer().setAvailableBalance1(salesReturn.getCustomer().getAvailableBalance1() + grandReturnTotal1 * sales.getCurrentCurrencyRate());
                    } else if (salesReturn.getCustomer().getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                        salesReturn.getCustomer().setAvailableBalance(salesReturn.getCustomer().getAvailableBalance() - grandReturnTotal * sales.getCurrentCurrencyRate());
                        salesReturn.getCustomer().setAvailableBalance1(salesReturn.getCustomer().getAvailableBalance1() - grandReturnTotal1 * sales.getCurrentCurrencyRate());
                    }

                    for (SalesReturnItem salesReturnItem : salesReturnItemList) {
                        double newavalQty = salesReturnItem.getItem().getAvailableQty();
                        salesReturnItem.getItem().setAvailableQty(newavalQty + salesReturnItem.getQuantity());
                        double newavalQty1 = salesReturnItem.getItem().getAvailableQty1();
                        salesReturnItem.getItem().setAvailableQty1(newavalQty1 + salesReturnItem.getQuantity1());
                        
                        cRUDServices.saveOrUpdateModel(salesReturnItem.getItem());
                    }
                    
                    for (SalesReturnItem salesReturnItem : salesReturnItemRemovedList) {
                        double newavalQty = salesReturnItem.getItem().getAvailableQty();
                        salesReturnItem.getItem().setAvailableQty(newavalQty - salesReturnItem.getQuantity());
                        double newavalQty1 = salesReturnItem.getItem().getAvailableQty1();
                        salesReturnItem.getItem().setAvailableQty1(newavalQty1 - salesReturnItem.getQuantity1());
                        
                        cRUDServices.saveOrUpdateModel(salesReturnItem.getItem());
                    }
               
                    //Updating Purchase Ledger available balance
                    salesReturnsLedger.setAvailableBalance((salesReturnsLedger.getAvailableBalance()) + subTotal);
                    salesReturnsLedger.setAvailableBalance1((salesReturnsLedger.getAvailableBalance1()) + subTotal1);
                    cRUDServices.saveOrUpdateModel(salesReturnsLedger);

                    //charges
                    //for (Ledger ledger : chargesLedgerList) {
                    //   cRUDServices.saveOrUpdateModel(ledger);
                    //}
                    for (SalesReturnCharge salesReturnCharge : salesReturnChargesList) {
                        Ledger ledger = ledgerDAO.findByLedgerName(salesReturnCharge.getCharges().getName());
                        ledger.setAvailableBalance(ledger.getAvailableBalance() + salesReturnCharge.getAmount());
                        ledger.setAvailableBalance1(ledger.getAvailableBalance1() + salesReturnCharge.getAmount1());
                        cRUDServices.saveOrUpdateModel(ledger);

                    }
                    
                    MsgBox.success("Successfylly Saved Sales Return");
                    SalesReturnView createSalesReturn = new SalesReturnView();
                    GlobalProperty.mainForm.getContentPanel().removeAll();
                    GlobalProperty.mainForm.getContentPanel().repaint();
                    GlobalProperty.mainForm.getContentPanel().revalidate();
                    GlobalProperty.mainForm.getContentPanel().add(createSalesReturn);
                }
            }
            listSalesReturnItemHistory(salesReturn);
        } catch (Exception e) {
            log.error("saveButtonActionPerformed", e);
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void salesReturnItemTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salesReturnItemTableMouseClicked
//        isPackingListTableClicked=true;
        selectedRowIndex = salesReturnItemTable.rowAtPoint(evt.getPoint());
//        if (packingListItemList.size() > selectedRowIndex) {
//            packingListItem = packingListItemList.get(selectedRowIndex);
//            pItemDescriptionText.setText(packingListItem.getItem().getDescription());
//            pItemWeightText.setText(packingListItem.getItem().getWeight() + "");
//            itemQtyText.setText(packingListItem.getContentsOfPackageQty()+"");
//            pUnitLabel.setText(packingListItem.getItem().getUnit().getName());
//           // pNetWeightText.setText(packingListItem.getNetWeight()+"");
//            pGrossWeightText.setText(packingListItem.getGrossWeight()+"");
//            
//            //set combo box
//            int count = pKindOfPackageCombo.getItemCount();
//                for (int i = 0; i < count; i++) {
//                    ComboKeyValue ckv = (ComboKeyValue) pKindOfPackageCombo.getItemAt(i);
//                    if (ckv.getKey().trim().equalsIgnoreCase(packingListItem.getKindOfPackage().getName())) {
//                        pKindOfPackageCombo.setSelectedItem(ckv);
//                    }
//                }
//                
//        }else{
//            commonService.clearTextFields(new JTextField[]{pItemDescriptionText,pInvoiceNumberText});
//            commonService.clearCurrencyFields(new JTextField[]{pItemWeightText,pItemWeightText,pNetWeightText,pGrossWeightText});
//            pUnitLabel.setText("");
//        }
    }//GEN-LAST:event_salesReturnItemTableMouseClicked

    private void pItemMinusButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pItemMinusButton1ActionPerformed
        //System.out.println("selectedRowIndex:"+selectedRowIndex);
        //System.out.println("packingListItemList:"+packingListItemList.size());
        if (salesReturnItemList.size() > selectedRowIndex) {
            SalesReturnItem salesReturnItem = salesReturnItemList.get(selectedRowIndex);
           // salesItem.setQuantity(salesItem.getQuantity() + salesReturnItem.getQuantity());
            salesReturnItemRemovedList.add(salesReturnItem);
                    
            salesReturnItemList.remove(selectedRowIndex);
            fillSalesReturnTable();
        }
    }//GEN-LAST:event_pItemMinusButton1ActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        PendingCommercialInvoiceView pendingCommercialInvoiceView = new PendingCommercialInvoiceView(GlobalProperty.mainForm.getContentPanel());
        GlobalProperty.mainForm.getContentPanel().removeAll();
        GlobalProperty.mainForm.getContentPanel().repaint();
        GlobalProperty.mainForm.getContentPanel().revalidate();
        GlobalProperty.mainForm.getContentPanel().add(pendingCommercialInvoiceView);
    }//GEN-LAST:event_backButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        CommonService.removeJpanel(GlobalProperty.mainForm.getContentPanel());
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void chargesTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesTextFocusGained
        chargesText.selectAll();
    }//GEN-LAST:event_chargesTextFocusGained

    private void chargesTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chargesTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_chargesTextKeyTyped

    private void chargesAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesAddButtonActionPerformed

        try {
            Object chargesItem = chargesComboBox.getSelectedItem();
            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();
            double chargeAmt = Double.parseDouble(chargesText.getText());
            double chargeAmt1 = Double.parseDouble(chargesText1.getText());
            if (Double.parseDouble(chargesText.getText()) > 0 && !charges.getName().equalsIgnoreCase("N/A")) {
                SalesReturnCharge salesReturnCharge = new SalesReturnCharge();

                Iterator<SalesReturnCharge> iter = salesReturnChargesList.iterator();
                while (iter.hasNext()) {
                    if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                        iter.remove();
                    }
                }

                salesReturnCharge.setCharges(charges);
                salesReturnCharge.setAmount(chargeAmt * sales.getCurrentCurrencyRate());
                salesReturnCharge.setAmount1(chargeAmt1 * sales.getCurrentCurrencyRate());
                salesReturnChargesList.add(salesReturnCharge);
                chargesComboBox.setSelectedIndex(0);
                chargesText.setText("0.00");
                chargesText1.setText("0.00");
                fillSalesReturnTable();
            }

        } catch (Exception e) {
            log.error("chargesAddButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesAddButtonActionPerformed

    private void chargesMinusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesMinusButtonActionPerformed
        try {
            Object chargesItem = chargesComboBox.getSelectedItem();
            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();

            Iterator<SalesReturnCharge> iter = salesReturnChargesList.iterator();
            while (iter.hasNext()) {
                if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                    iter.remove();
                }
            }
            chargesComboBox.setSelectedIndex(0);
            chargesText.setText("0.00");
            chargesText1.setText("0.00");
            fillSalesReturnTable();

        } catch (Exception e) {
            log.error("chargesMinusButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesMinusButtonActionPerformed

    private void itemUnitPriceTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemUnitPriceTextFocusGained
        itemUnitPriceText.selectAll();
    }//GEN-LAST:event_itemUnitPriceTextFocusGained

    private void itemUnitPriceTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemUnitPriceTextFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_itemUnitPriceTextFocusLost

    private void itemUnitPriceTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemUnitPriceTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemUnitPriceTextKeyTyped

    private void itemDiscountTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemDiscountTextFocusGained
        itemDiscountText.selectAll();
    }//GEN-LAST:event_itemDiscountTextFocusGained

    private void itemDiscountTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemDiscountTextFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_itemDiscountTextFocusLost

    private void itemDiscountTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemDiscountTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemDiscountTextKeyTyped

    private void billDateFormattedTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_billDateFormattedTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_billDateFormattedTextActionPerformed

    private void sRtrndeliveryTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sRtrndeliveryTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sRtrndeliveryTextActionPerformed

    private void sRtrndeliveryTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_sRtrndeliveryTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_sRtrndeliveryTextKeyPressed

    private void itemQtyText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyText1FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_itemQtyText1FocusGained

    private void itemQtyText1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyText1FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_itemQtyText1FocusLost

    private void itemQtyText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyText1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemQtyText1KeyTyped

    private void chargesText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesText1FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_chargesText1FocusGained

    private void chargesText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chargesText1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_chargesText1KeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea addressTextArea;
    private javax.swing.JButton backButton;
    private javax.swing.JFormattedTextField billDateFormattedText;
    private javax.swing.JPanel bodyPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton chargesAddButton;
    private javax.swing.JComboBox chargesComboBox;
    private javax.swing.JButton chargesMinusButton;
    private javax.swing.JPanel chargesPanel;
    private javax.swing.JFormattedTextField chargesText;
    private javax.swing.JFormattedTextField chargesText1;
    private javax.swing.JPanel chrgsPanel;
    private javax.swing.JTextField customerText;
    private javax.swing.JTextField deleveryTermsText;
    private javax.swing.JTextField deliveryText;
    private javax.swing.JPanel discountPanel;
    private javax.swing.JPanel headingPanel;
    private javax.swing.JTextField invoiceNumberText;
    private javax.swing.JFormattedTextField itemDiscountText;
    private javax.swing.JPanel itemQtyPanel;
    private javax.swing.JFormattedTextField itemQtyText;
    private javax.swing.JFormattedTextField itemQtyText1;
    private javax.swing.JFormattedTextField itemUnitPriceText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPanel leftSidePanel;
    private javax.swing.JButton maxMinPackingListButton;
    private javax.swing.JButton maxMinPreformaButton;
    private com.toedter.calendar.JDateChooser pInvoiceDateFormattedText;
    private javax.swing.JTextField pInvoiceNumberText;
    private javax.swing.JButton pItemAddButton;
    private javax.swing.JTextField pItemDescriptionText;
    private javax.swing.JButton pItemMinusButton1;
    private javax.swing.JFormattedTextField pItemWeightText;
    private javax.swing.JPanel packingDetailsPanel;
    private javax.swing.JPanel packingListBottomPanel;
    private javax.swing.JTextField paymentTermsText;
    private javax.swing.JPanel rightSidePanel;
    private javax.swing.JTextField sRtrndeliveryText;
    private javax.swing.JTable salesItemTable;
    private javax.swing.JPanel salesPrefomaBottomPanel;
    private javax.swing.JPanel salesPrefomaDetailPanel;
    private javax.swing.JTable salesReturnItemTable;
    private javax.swing.JButton saveButton;
    private javax.swing.JComboBox shipmentComboBox;
    private javax.swing.JPanel shipmentModePanel;
    private javax.swing.JTextField telNoText;
    // End of variables declaration//GEN-END:variables

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {

                    SalesPreformaDAO salesPreformaDAO = new SalesPreformaDAO();
                    List<SalesPreforma> listOfSalesPreforma = salesPreformaDAO.findAll();

                    MainForm jMainFrame = new MainForm();
                    jMainFrame.setVisible(true);
                    jMainFrame.setExtendedState(MainForm.MAXIMIZED_BOTH);

                    //PurchaseReturnItemView packingListView=new PurchaseReturnItemView(listOfSalesPreforma.get(0));
                    jMainFrame.getContentPanel().removeAll();
                    jMainFrame.getContentPanel().repaint();
                    jMainFrame.getContentPanel().revalidate();
                    //jMainFrame.getContentPanel().add(packingListView);

                } catch (Exception e) {
                    log.error("MainForm run :", e);
                }
            }
        });
    }
}
