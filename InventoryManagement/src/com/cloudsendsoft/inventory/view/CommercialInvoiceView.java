/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.MultiCurrencyDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.dao.ShipmentModeDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.report.CommercialInvoiceReport;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.CommercialInvoice;
import com.cloudsendsoft.inventory.model.CommercialInvoiceCharge;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.PurchaseCharge;
import com.cloudsendsoft.inventory.model.SalesPreformaTax;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaCharge;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.services.LedgerService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class CommercialInvoiceView extends javax.swing.JPanel {

    /**
     * Creates new form CommercialInvoiceView
     */
    static double totalAmount = 0;

    static final Logger log = Logger.getLogger(CommercialInvoiceView.class.getName());

    CRUDServices cRUDServices = new CRUDServices();
    ItemDAO itemDAO = new ItemDAO();
    CommonService commonService = new CommonService();
    MainForm mainForm = null;
    LedgerService ledgerService = new LedgerService();
    TaxDAO taxDAO = new TaxDAO();
    ChargesDAO chargesDAO = new ChargesDAO();
    MultiCurrencyDAO multiCurrencyDAO = new MultiCurrencyDAO();
    ShipmentModeDAO shipmentModeDAO = new ShipmentModeDAO();

    //global usage
    Ledger customerLedger = null;
    long itemQty = 0;
    List<SalesPreformaItem> salesPreformaItemList = null;
    List<SalesPreformaTax> salesPreformaTaxList = null;
    Item item = null;
    // ArrayList<SalesPreformaTax> salesPreformaTaxList = new ArrayList<SalesPreformaTax>();
    List<SalesPreformaCharge> salesPreformaChargesList = null;
    NumberProperty numberProperty = null;
    NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
    CommercialInvoice commercialInvoice = null;
    SalesPreforma salesPreforma = new SalesPreforma();
    List<CommercialInvoiceCharge> commercialInvoiceChargesList = new ArrayList<CommercialInvoiceCharge>();

    MultiCurrency currency = null;
    String customerDetails = "";
    SalesPreformaItem salesPreformaItem = null;
    DefaultTableModel salesPreformaItemTableModel = null;
    double netamount = 0.0;
    double discount = 0.0;
    double grandtotal = 0.0;
    double subTotal = 0.0;
    LedgerDAO ledgerDAO = new LedgerDAO();

    public CommercialInvoiceView(SalesPreforma salesPreforma) {

        try {
            initComponents();
            this.customerLedger=salesPreforma.getCustomer();
            if(customerLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")){
                customerLedger.setAvailableBalance(customerLedger.getAvailableBalance()-salesPreforma.getGrandTotal());
            }else if(customerLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")){
                customerLedger.setAvailableBalance(customerLedger.getAvailableBalance()+salesPreforma.getGrandTotal());
            }

            commonService.setCurrentPeriodOnCalendar(billDateFormattedText);

            if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
                saveButton.setEnabled(false);
            }
            //charge load
            commercialInvoice = salesPreforma.getCommercialInvoice();

            for (Charges charges : chargesDAO.findAll()) {
                chargesComboBox.addItem(new ComboKeyValue(charges.getName(), charges));
            }
            //invoice number load

            this.salesPreforma = salesPreforma;

            if (salesPreforma.getCommercialInvoice() == null) {
                discount = salesPreforma.getDiscount() / salesPreforma.getCurrentCurrencyRate();
                netamount = salesPreforma.getGrandTotal() / salesPreforma.getCurrentCurrencyRate() + salesPreforma.getDiscount() / salesPreforma.getCurrentCurrencyRate();
                grandtotal = salesPreforma.getGrandTotal() / salesPreforma.getCurrentCurrencyRate();
                numberProperty = numberPropertyDAO.findInvoiceNo("CommercialInvoice");
                invoiceNumberText.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
            } else {
                invoiceNumberText.setText(salesPreforma.getCommercialInvoice().getInvoiceNumber() + "");
                discount = salesPreforma.getCommercialInvoice().getDiscount() / salesPreforma.getCurrentCurrencyRate();
                netamount = salesPreforma.getCommercialInvoice().getGrandTotal() / salesPreforma.getCurrentCurrencyRate() + salesPreforma.getCommercialInvoice().getDiscount() / salesPreforma.getCurrentCurrencyRate();
                grandtotal = salesPreforma.getCommercialInvoice().getGrandTotal() / salesPreforma.getCurrentCurrencyRate();
                saveButton.setText("Update");
            }

            jScrollPane1.setOpaque(false);
            jScrollPane1.getViewport().setOpaque(false);

            billDateFormattedText.setDate(new java.util.Date());
            customerText.setText(salesPreforma.getCustomer().getLedgerName());
            deliveryText.setText(salesPreforma.getDelivery());
            deleveryTermsText.setText(salesPreforma.getDeleveryTerms());
            paymentTermsText.setText(salesPreforma.getPaymentTerms());
            packingText.setText(salesPreforma.getPaymentTerms());

            customerDetails = "";
            if (salesPreforma.getCommercialInvoice() == null) {
                tableBorderPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Pending CommercialInvoice"));
                if (salesPreforma.getAddress() != null) {
                    if (salesPreforma.getAddress().trim().length() > 0) {
                        customerDetails = salesPreforma.getAddress();
                    }
                }
            } else {
                tableBorderPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Saved CommercialInvoice"));
                if (salesPreforma.getCommercialInvoice().getAddress() != null) {
                    if (salesPreforma.getCommercialInvoice().getAddress().trim().length() > 0) {
                        customerDetails = salesPreforma.getCommercialInvoice().getAddress();
                    }
                }
            }

            addressTextArea.setText(customerDetails);
            salesTaxText.setText(salesPreforma.getCustomer().getSalesTaxNo());
            currencyLabel.setText(salesPreforma.getMultiCurrency().getSymbol() + "");
            netAmountlLabel.setText(salesPreforma.getMultiCurrency().getSymbol() + commonService.formatIntoCurrencyAsString(netamount) + "");
            discountText.setText(commonService.formatIntoCurrencyAsString(discount));
            grandTotalLabel.setText(salesPreforma.getMultiCurrency().getSymbol() + "" + commonService.formatIntoCurrencyAsString(grandtotal) + "");

            this.mainForm = mainForm;
            salePreformaItemTable.setRowHeight(25);

            salePreformaItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                    null,
                    new String[]{"Sr. No.", "Item Code", "Description", "Unit Price", "Discount", "Qty.", "Total Amount"}
            ));
            salesPreformaItemTableModel = (DefaultTableModel) salePreformaItemTable.getModel();
            CommonService.setWidthAsPercentages(salePreformaItemTable, .03, .10, .20, .08, .08, .08, .08);
            salesPreformaItemList = salesPreforma.getSalesPreformaItems();

            for (SalesPreformaItem pItem : salesPreformaItemList) {
                salesPreformaItemTableModel.addRow(new Object[]{"", " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), salesPreforma.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / salesPreforma.getCurrentCurrencyRate()), salesPreforma.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getDiscount() / salesPreforma.getCurrentCurrencyRate()), pItem.getQuantity(), salesPreforma.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString(((pItem.getUnitPrice() / salesPreforma.getCurrentCurrencyRate()) - (pItem.getDiscount() / salesPreforma.getCurrentCurrencyRate())) * pItem.getQuantity())});
                subTotal += (((pItem.getUnitPrice() / salesPreforma.getCurrentCurrencyRate()) - (pItem.getDiscount() / salesPreforma.getCurrentCurrencyRate())) * pItem.getQuantity());
            }
            salesPreformaTaxList = salesPreforma.getSalesPreformaTaxs();

            for (SalesPreformaTax salesPreformaTax : salesPreformaTaxList) {
                // salesPreformaItemTableModel.addRow(new Object[]{null});
                salesPreformaItemTableModel.addRow(new Object[]{"", "", "\t" + salesPreformaTax.getTax().getName(), "", "", "", salesPreforma.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString((salesPreformaTax.getAmount()) / salesPreforma.getCurrentCurrencyRate())});
            }

            salesPreformaChargesList = salesPreforma.getSalesPreformaCharges();

            if (commercialInvoice == null) {
                for (SalesPreformaCharge salesPreformaCharge : salesPreforma.getSalesPreformaCharges()) {
                    // salesPreformaItemTableModel.addRow(new Object[]{null});
                    CommercialInvoiceCharge commercialInvoiceCharge = new CommercialInvoiceCharge();
                    commercialInvoiceCharge.setAmount(salesPreformaCharge.getAmount());
                    commercialInvoiceCharge.setCharges(salesPreformaCharge.getCharges());
                    commercialInvoiceCharge.setCompany(salesPreformaCharge.getCompany());
                    commercialInvoiceChargesList.add(commercialInvoiceCharge);
                }
            } else {
                commercialInvoiceChargesList = commercialInvoice.getCommercialInvoiceCharges();
            }

            for (CommercialInvoiceCharge commercialInvoiceCharge : commercialInvoiceChargesList) {
                salesPreformaItemTableModel.addRow(new Object[]{"", "", "\t" + commercialInvoiceCharge.getCharges().getName(), "", "", "", salesPreforma.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString(commercialInvoiceCharge.getAmount() / salesPreforma.getCurrentCurrencyRate())});
            }

            //discount
            if (discount > 0) {

                salesPreformaItemTableModel.addRow(new Object[]{"", "", "Discount", "", "", "", salesPreforma.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString(discount)});
            }

            // Combo boxes initialization
            //jTable rows & columns alignment
            ((DefaultTableCellRenderer) salePreformaItemTable.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);

            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(JLabel.CENTER);
            salePreformaItemTable.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
            salePreformaItemTable.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
            salePreformaItemTable.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
            salePreformaItemTable.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
            //jTable rows & columns alignment
            setFocusOrder();

        } catch (Exception e) {
            log.error("Purchase:", e);
        }

    }

    void setFocusOrder() {
        try {
            billDateFormattedText.setNextFocusableComponent(invoiceNumberText);
            invoiceNumberText.setNextFocusableComponent(customerText);
            customerText.setNextFocusableComponent(deliveryText);
            deliveryText.setNextFocusableComponent(paymentTermsText);
            paymentTermsText.setNextFocusableComponent(deleveryTermsText);
            deleveryTermsText.setNextFocusableComponent(packingText);
            //packingText.setNextFocusableComponent(multiCurrencyComboBox);
            // multiCurrencyComboBox.setNextFocusableComponent(addressTextArea);
            addressTextArea.setNextFocusableComponent(salesTaxText);
        } catch (Exception e) {
            log.error("setFocusOrder:", e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tableBorderPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        salePreformaItemTable = new javax.swing.JTable();
        bottomPanel = new javax.swing.JPanel();
        grandTotalPanel = new javax.swing.JPanel();
        netAmountlLabel = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        discountText = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        totDiscountAddButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        grossAmountLabel = new javax.swing.JLabel();
        grandTotalLabel = new javax.swing.JLabel();
        currencyLabel = new javax.swing.JLabel();
        InvoiceDetailsPanel = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        salesTaxText = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        addressTextArea = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        customerText = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        paymentTermsText = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        deleveryTermsText = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        packingText = new javax.swing.JTextField();
        deliveryText = new javax.swing.JTextField();
        invoiceNumberText = new javax.swing.JTextField();
        chargesPanel = new javax.swing.JPanel();
        chargesComboBox = new javax.swing.JComboBox();
        chargesText = new javax.swing.JFormattedTextField();
        chargesAddButton = new javax.swing.JButton();
        chargesMinusButton = new javax.swing.JButton();
        billDateFormattedText = new com.toedter.calendar.JDateChooser();
        buttonPanel = new javax.swing.JPanel();
        saveButton = new javax.swing.JButton();
        printButton = new javax.swing.JButton();
        pdfButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        tableBorderPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("CommercialInvoice"));
        tableBorderPanel.setOpaque(false);

        jScrollPane1.setName(""); // NOI18N
        jScrollPane1.setOpaque(false);

        salePreformaItemTable.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        salePreformaItemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        salePreformaItemTable.setOpaque(false);
        salePreformaItemTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                salePreformaItemTableMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                salePreformaItemTableMouseEntered(evt);
            }
        });
        jScrollPane1.setViewportView(salePreformaItemTable);

        bottomPanel.setOpaque(false);

        grandTotalPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Grand Total", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N
        grandTotalPanel.setOpaque(false);

        netAmountlLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        netAmountlLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        netAmountlLabel.setText("0.00");
        netAmountlLabel.setFocusable(false);
        netAmountlLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setText("Net Amount:");

        discountText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                discountTextActionPerformed(evt);
            }
        });
        discountText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                discountTextFocusGained(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel22.setText("Discount:");

        totDiscountAddButton.setText("+");
        totDiscountAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totDiscountAddButtonActionPerformed(evt);
            }
        });

        grossAmountLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        grossAmountLabel.setText("Grand Total:");
        grossAmountLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        grandTotalLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        grandTotalLabel.setText("0.00");

        javax.swing.GroupLayout grandTotalPanelLayout = new javax.swing.GroupLayout(grandTotalPanel);
        grandTotalPanel.setLayout(grandTotalPanelLayout);
        grandTotalPanelLayout.setHorizontalGroup(
            grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(grandTotalPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(grandTotalPanelLayout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addGap(18, 18, 18)
                        .addComponent(netAmountlLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(grandTotalPanelLayout.createSequentialGroup()
                        .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(grandTotalPanelLayout.createSequentialGroup()
                                .addComponent(jLabel22)
                                .addGap(20, 20, 20)
                                .addComponent(currencyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(discountText, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(totDiscountAddButton)
                                .addGap(0, 5, Short.MAX_VALUE))
                            .addGroup(grandTotalPanelLayout.createSequentialGroup()
                                .addComponent(grossAmountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(grandTotalLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(15, 15, 15)))
                        .addGap(14, 14, 14)))
                .addContainerGap())
        );
        grandTotalPanelLayout.setVerticalGroup(
            grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(grandTotalPanelLayout.createSequentialGroup()
                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(netAmountlLabel)
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(currencyLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(discountText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel22)
                        .addComponent(totDiscountAddButton)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(grossAmountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(grandTotalLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23))
        );

        InvoiceDetailsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Invoice Details"));
        InvoiceDetailsPanel.setOpaque(false);

        jLabel6.setText("Bill Date");

        jLabel5.setText("Invoice Number");

        jLabel13.setText("Customer");

        jLabel14.setText("Delivery");

        jLabel15.setText("Sales Tax No: ");

        addressTextArea.setColumns(20);
        addressTextArea.setLineWrap(true);
        addressTextArea.setRows(5);
        jScrollPane3.setViewportView(addressTextArea);

        jLabel1.setText("Address :");

        customerText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customerTextActionPerformed(evt);
            }
        });
        customerText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                customerTextKeyPressed(evt);
            }
        });

        jLabel11.setText("Delivery Terms");

        paymentTermsText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                paymentTermsTextKeyPressed(evt);
            }
        });

        jLabel12.setText("Payment Terms");

        deleveryTermsText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleveryTermsTextActionPerformed(evt);
            }
        });
        deleveryTermsText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                deleveryTermsTextKeyPressed(evt);
            }
        });

        jLabel18.setText("Packing");

        packingText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                packingTextKeyPressed(evt);
            }
        });

        deliveryText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deliveryTextActionPerformed(evt);
            }
        });
        deliveryText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                deliveryTextKeyPressed(evt);
            }
        });

        invoiceNumberText.setEditable(false);
        invoiceNumberText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                invoiceNumberTextActionPerformed(evt);
            }
        });
        invoiceNumberText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                invoiceNumberTextKeyPressed(evt);
            }
        });

        chargesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Charges"));
        chargesPanel.setOpaque(false);

        chargesComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                chargesComboBoxMouseClicked(evt);
            }
        });
        chargesComboBox.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesComboBoxFocusGained(evt);
            }
        });

        chargesText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        chargesText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        chargesText.setText("0.00");
        chargesText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesTextFocusGained(evt);
            }
        });
        chargesText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                chargesTextKeyTyped(evt);
            }
        });

        chargesAddButton.setText("+");
        chargesAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesAddButtonActionPerformed(evt);
            }
        });

        chargesMinusButton.setText("-");
        chargesMinusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesMinusButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout chargesPanelLayout = new javax.swing.GroupLayout(chargesPanel);
        chargesPanel.setLayout(chargesPanelLayout);
        chargesPanelLayout.setHorizontalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, chargesPanelLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chargesComboBox, 0, 130, Short.MAX_VALUE)
                    .addComponent(chargesText))
                .addGap(18, 18, 18)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        chargesPanelLayout.setVerticalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chargesPanelLayout.createSequentialGroup()
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chargesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chargesText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        billDateFormattedText.setDateFormatString("dd/MM/yyyy");

        javax.swing.GroupLayout InvoiceDetailsPanelLayout = new javax.swing.GroupLayout(InvoiceDetailsPanel);
        InvoiceDetailsPanel.setLayout(InvoiceDetailsPanelLayout);
        InvoiceDetailsPanelLayout.setHorizontalGroup(
            InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(paymentTermsText, javax.swing.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE)
                    .addComponent(invoiceNumberText)
                    .addComponent(customerText)
                    .addComponent(deliveryText)
                    .addComponent(billDateFormattedText, javax.swing.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE))
                .addGap(15, 15, 15)
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(packingText, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(deleveryTermsText)
                            .addComponent(salesTaxText, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE))
                        .addGap(28, 28, 28)
                        .addComponent(chargesPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane3))
                .addGap(2, 2, 2))
        );
        InvoiceDetailsPanelLayout.setVerticalGroup(
            InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addComponent(billDateFormattedText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(11, 11, 11)
                                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(invoiceNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel13)
                                    .addComponent(customerText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(12, 12, 12)
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(deliveryText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(paymentTermsText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12)))
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel11)
                                    .addComponent(deleveryTermsText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel18)
                                    .addComponent(packingText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(8, 8, 8)
                                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel15)
                                    .addComponent(salesTaxText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(chargesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
        );

        buttonPanel.setLayout(new java.awt.GridLayout(1, 0));

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveButton.setText("<HTML><U>S</U>ave<HTML>");
        saveButton.setToolTipText("");
        saveButton.setOpaque(false);
        saveButton.setPreferredSize(new java.awt.Dimension(57, 35));
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(saveButton);

        printButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/print.png"))); // NOI18N
        printButton.setText("Print");
        printButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(printButton);

        pdfButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/pdf-icon.png"))); // NOI18N
        pdfButton.setText(" PDF");
        pdfButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdfButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(pdfButton);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/cancel.png"))); // NOI18N
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(cancelButton);

        javax.swing.GroupLayout bottomPanelLayout = new javax.swing.GroupLayout(bottomPanel);
        bottomPanel.setLayout(bottomPanelLayout);
        bottomPanelLayout.setHorizontalGroup(
            bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bottomPanelLayout.createSequentialGroup()
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(InvoiceDetailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(grandTotalPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );
        bottomPanelLayout.setVerticalGroup(
            bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bottomPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(grandTotalPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(InvoiceDetailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout tableBorderPanelLayout = new javax.swing.GroupLayout(tableBorderPanel);
        tableBorderPanel.setLayout(tableBorderPanelLayout);
        tableBorderPanelLayout.setHorizontalGroup(
            tableBorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tableBorderPanelLayout.createSequentialGroup()
                .addGroup(tableBorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bottomPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(tableBorderPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1)))
                .addContainerGap())
        );
        tableBorderPanelLayout.setVerticalGroup(
            tableBorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tableBorderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bottomPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        add(tableBorderPanel, java.awt.BorderLayout.CENTER);
        tableBorderPanel.getAccessibleContext().setAccessibleName("Commercial Invoice");
    }// </editor-fold>//GEN-END:initComponents

    void fillSalesPreformaItemTable() {
        try {
            discount = Double.parseDouble(discountText.getText());
            //boolean flag = false;
            netamount = 0;
            subTotal = 0.00;

            salesPreformaItemTableModel.setRowCount(0);
            String currencySymbol = (null == currency) ? "" : currency.getSymbol();
            salesPreformaItemList = salesPreforma.getSalesPreformaItems();

            for (SalesPreformaItem pItem : salesPreformaItemList) {
                netamount += (((pItem.getUnitPrice() / salesPreforma.getCurrentCurrencyRate()) - (pItem.getDiscount() / salesPreforma.getCurrentCurrencyRate())) * pItem.getQuantity());
                salesPreformaItemTableModel.addRow(new Object[]{"", " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), salesPreforma.getMultiCurrency().getSymbol() + "" + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / salesPreforma.getCurrentCurrencyRate()), salesPreforma.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getDiscount() / salesPreforma.getCurrentCurrencyRate()), pItem.getQuantity(), salesPreforma.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString(((pItem.getUnitPrice() / salesPreforma.getCurrentCurrencyRate()) - (pItem.getDiscount() / salesPreforma.getCurrentCurrencyRate())) * pItem.getQuantity())});
                subTotal = netamount;
            }

            for (SalesPreformaTax salesPreformaTax : salesPreforma.getSalesPreformaTaxs()) {
                ///salesPreformaItemTableModel.addRow(new Object[]{null});
                salesPreformaItemTableModel.addRow(new Object[]{"", "", "\t" + salesPreformaTax.getTax().getName(), "", "", "", salesPreforma.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString((salesPreformaTax.getAmount()) / salesPreforma.getCurrentCurrencyRate())});
                netamount += (salesPreformaTax.getAmount() / salesPreforma.getCurrentCurrencyRate());
            }

            for (CommercialInvoiceCharge commercialInvoiceCharge : commercialInvoiceChargesList) {
                /*if (!flag) {
                 salesPreformaItemTableModel.addRow(new Object[]{null});
                 flag = true;
                 }*/
                if (salesPreforma == null) {
                    salesPreformaItemTableModel.addRow(new Object[]{"", "", "\t" + commercialInvoiceCharge.getCharges().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(commercialInvoiceCharge.getAmount())});
                    netamount += (commercialInvoiceCharge.getAmount());
                } else {
                    salesPreformaItemTableModel.addRow(new Object[]{"", "", "\t" + commercialInvoiceCharge.getCharges().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(commercialInvoiceCharge.getAmount() / salesPreforma.getCurrentCurrencyRate())});
                    netamount += (commercialInvoiceCharge.getAmount() / salesPreforma.getCurrentCurrencyRate());
                }
            }

            grandtotal = netamount - discount;
            if (discount > 0) {
                salesPreformaItemTableModel.addRow(new Object[]{"", "", "Discount", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(discount)});
            }

            netAmountlLabel.setText(salesPreforma.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString(netamount));
            grandTotalLabel.setText(salesPreforma.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString(grandtotal));
        } catch (Exception e) {
            log.error("fillPurchaseItemTable:", e);
        }
    }

    private void salePreformaItemTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salePreformaItemTableMouseClicked
        try {
            ArrayList row = CommonService.getTableRowData(salePreformaItemTable);
            String itemDesc = row.get(2) + "";
            itemDesc = itemDesc.trim();
            String itemCode = row.get(1) + "";
            int count = chargesComboBox.getItemCount();
            count = chargesComboBox.getItemCount();
            for (int i = 0; i < count; i++) {
                ComboKeyValue ckv = (ComboKeyValue) chargesComboBox.getItemAt(i);
                if (ckv.getKey().trim().equalsIgnoreCase(itemDesc)) {
                    chargesComboBox.setSelectedItem(ckv);
                    chargesText.setText(row.get(6) + "");
                }
            }
            if (itemDesc.equalsIgnoreCase("discount")) {
                totDiscountAddButton.setText("-");
            }
        } catch (Exception e) {

        }
    }//GEN-LAST:event_salePreformaItemTableMouseClicked

    private void salePreformaItemTableMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salePreformaItemTableMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_salePreformaItemTableMouseEntered

    void saveOrUpdateCommercialInvoice() {
        try {
            commercialInvoice = salesPreforma.getCommercialInvoice();

            if (commercialInvoice == null) {
                commercialInvoice = new CommercialInvoice();
            }

            commercialInvoice.setInvoiceNumber(invoiceNumberText.getText().trim());
            commercialInvoice.setBillDate(commonService.utilDateToSqlDate(billDateFormattedText.getDate()));
            commercialInvoice.setDiscount(discount * salesPreforma.getCurrentCurrencyRate());
            commercialInvoice.setGrandTotal(grandtotal * salesPreforma.getCurrentCurrencyRate());
            commercialInvoice.setCompany(GlobalProperty.getCompany());
            commercialInvoice.setAddress(addressTextArea.getText());
            for (CommercialInvoiceCharge commercialInvoiceCharge : commercialInvoiceChargesList) {
                commercialInvoiceCharge.setAmount(commercialInvoiceCharge.getAmount() / salesPreforma.getCurrentCurrencyRate());

            }
            commercialInvoice.setCommercialInvoiceCharges(commercialInvoiceChargesList);
            if (commercialInvoice == null) {
                cRUDServices.saveOrUpdateModel(commercialInvoice);
                numberProperty.setNumber(numberProperty.getNumber() + 1);
                cRUDServices.saveOrUpdateModel(numberProperty);
            } else {
                cRUDServices.saveOrUpdateModel(commercialInvoice);
                numberProperty.setNumber(numberProperty.getNumber() + 1);
                cRUDServices.saveOrUpdateModel(numberProperty);
            }
            salesPreforma.setCommercialInvoice(commercialInvoice);
            cRUDServices.saveOrUpdateModel(salesPreforma);
            
            //Updating availableQty of Customer Ledger
            if(customerLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")){
                customerLedger.setAvailableBalance(customerLedger.getAvailableBalance()+(grandtotal* salesPreforma.getCurrentCurrencyRate()));
                cRUDServices.saveOrUpdateModel(customerLedger);
            }else if(customerLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")){
                customerLedger.setAvailableBalance(customerLedger.getAvailableBalance()-(grandtotal* salesPreforma.getCurrentCurrencyRate()));
                cRUDServices.saveOrUpdateModel(customerLedger);
            }

            for (SalesPreformaItem pi : salesPreformaItemList) {
                Item item = pi.getItem();
                item.setAvailableQty(item.getAvailableQty() - pi.getQuantity());
                cRUDServices.saveOrUpdateModel(item);
            }

            //Updating Commercial Invoice Ledger available balance
            Ledger ledger = ledgerDAO.findByLedgerName("Commercial Invoice");
            ledger.setAvailableBalance(ledger.getAvailableBalance() + subTotal);
            cRUDServices.saveOrUpdateModel(ledger);

            ledger = ledgerDAO.findByLedgerName("Discount Allowed");
            ledger.setAvailableBalance(ledger.getAvailableBalance() + discount);

            //tax
            for (SalesPreformaTax salesPreformaTax : salesPreformaTaxList) {
                ledger = ledgerDAO.findByLedgerName(salesPreformaTax.getTax().getName());
                ledger.setAvailableBalance(ledger.getAvailableBalance() - salesPreformaTax.getAmount());
                cRUDServices.saveOrUpdateModel(ledger);
            }
            //charges
            for (SalesPreformaCharge salesPreformaCharge : salesPreformaChargesList) {
                ledger = ledgerDAO.findByLedgerName(salesPreformaCharge.getCharges().getName());
                ledger.setAvailableBalance(ledger.getAvailableBalance() + salesPreformaCharge.getAmount());
                cRUDServices.saveOrUpdateModel(ledger);

            }
            tableBorderPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Saved CommercialInvoice"));
        } catch (Exception e) {
            log.error("saveOrUpdateCommercialInvoice:", e);
        }
    }
    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        try {
            if (billDateFormattedText.getDate() != null && invoiceNumberText.getText().length() > 0) {
                if (MsgBox.confirm("Are you sure you want to save?")) {
                    saveOrUpdateCommercialInvoice();
                    MsgBox.success("Commercial Invoice Saved Successfully");
                }
            } else {
                MsgBox.warning("Required fields are not entered");
            }
        } catch (Exception ex) {
            log.error("saveButtonActionPerformed:", ex);
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        CommonService.removeJpanel(mainForm.getContentPanel());
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void packingTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_packingTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_packingTextKeyPressed

    private void deleveryTermsTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_deleveryTermsTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleveryTermsTextKeyPressed

    private void deleveryTermsTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleveryTermsTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleveryTermsTextActionPerformed

    private void paymentTermsTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_paymentTermsTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_paymentTermsTextKeyPressed

    private void customerTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_customerTextKeyPressed
    }//GEN-LAST:event_customerTextKeyPressed

    private void customerTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customerTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customerTextActionPerformed

    private void deliveryTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deliveryTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deliveryTextActionPerformed

    private void deliveryTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_deliveryTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_deliveryTextKeyPressed

    private void invoiceNumberTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_invoiceNumberTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_invoiceNumberTextActionPerformed

    private void invoiceNumberTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_invoiceNumberTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_invoiceNumberTextKeyPressed

    private void discountTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_discountTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_discountTextActionPerformed

    private void totDiscountAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totDiscountAddButtonActionPerformed

        if (totDiscountAddButton.getText().trim().equalsIgnoreCase("+")) {
            discount = Double.parseDouble(discountText.getText().trim());
        } else {
            discount = 0;
            discountText.setText("0.00");
            totDiscountAddButton.setText("+");
        }

        fillSalesPreformaItemTable();
    }//GEN-LAST:event_totDiscountAddButtonActionPerformed

    private void printButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printButtonActionPerformed
        try {
            if (billDateFormattedText.getDate() != null && invoiceNumberText.getText().length() > 0) {
                if (MsgBox.confirm("Are you sure you want to save and print?")) {
                    saveOrUpdateCommercialInvoice();
                    CommercialInvoiceReport report = new CommercialInvoiceReport();
                    if (report.createCommercialInvoiceReport(salesPreforma, "print")) {
                        clearAllFields();
                    } else {
                        MsgBox.warning("Something went wrong, please re-click the button again!");
                    }
                }
            } else {
                MsgBox.warning("Required fields are not entered");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(ex);
        }
    }//GEN-LAST:event_printButtonActionPerformed

    private void chargesComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chargesComboBoxMouseClicked
        // TODO add your handling code here:
        chargesComboBox.removeAllItems();
        for (Charges charges : chargesDAO.findAll()) {
            chargesComboBox.addItem(new ComboKeyValue(charges.getName(), charges));
        }
    }//GEN-LAST:event_chargesComboBoxMouseClicked

    private void chargesComboBoxFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesComboBoxFocusGained
        // TODO add your handling code here:

    }//GEN-LAST:event_chargesComboBoxFocusGained

    private void chargesTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesTextFocusGained
        chargesText.selectAll();
    }//GEN-LAST:event_chargesTextFocusGained

    private void chargesTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chargesTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_chargesTextKeyTyped

    private void chargesAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesAddButtonActionPerformed

        try {
            //commercialInvoiceChargesList=salesPreformaCommercialInnvoice.getSalesPreformaCharges()
            Object chargesItem = chargesComboBox.getSelectedItem();
            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();
            double chargeAmt = Double.parseDouble(chargesText.getText());
            if (Double.parseDouble(chargesText.getText()) > 0 && !charges.getName().equalsIgnoreCase("N/A")) {
                CommercialInvoiceCharge commercialInvoiceCharge = new CommercialInvoiceCharge();

                Iterator<CommercialInvoiceCharge> iter = commercialInvoiceChargesList.iterator();
                while (iter.hasNext()) {
                    if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                        iter.remove();
                    }
                }

                commercialInvoiceCharge.setCharges(charges);
                commercialInvoiceCharge.setAmount(chargeAmt * salesPreforma.getCurrentCurrencyRate());
                commercialInvoiceChargesList.add(commercialInvoiceCharge);
                chargesComboBox.setSelectedIndex(0);
                chargesText.setText("0.00");
                fillSalesPreformaItemTable();
            }

        } catch (Exception e) {
            log.error("chargesAddButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesAddButtonActionPerformed

    private void chargesMinusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesMinusButtonActionPerformed
        try {
            Object chargesItem = chargesComboBox.getSelectedItem();
            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();

            Iterator<CommercialInvoiceCharge> iter = commercialInvoiceChargesList.iterator();
            while (iter.hasNext()) {
                if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                    iter.remove();
                }
            }

            chargesComboBox.setSelectedIndex(0);
            chargesText.setText("0.00");
            fillSalesPreformaItemTable();

        } catch (Exception e) {
            log.error("chargesMinusButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesMinusButtonActionPerformed

    private void pdfButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdfButtonActionPerformed
        try {
            if (billDateFormattedText.getDate() != null && invoiceNumberText.getText().length() > 0) {
                if (MsgBox.confirm("Are you sure you want to save and create pdf?")) {
                    saveOrUpdateCommercialInvoice();
                    CommercialInvoiceReport report = new CommercialInvoiceReport();
                    if (report.createCommercialInvoiceReport(salesPreforma, "pdf")) {
                        clearAllFields();
                    } else {
                        MsgBox.warning("Something went wrong, please re-click the button again!");
                    }
                }
            } else {
                MsgBox.warning("Required fields are not entered");
            }
        } catch (Exception e) {
            log.error("printButtonActionPerformed", e);
        }
    }//GEN-LAST:event_pdfButtonActionPerformed

    private void discountTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_discountTextFocusGained
        discountText.selectAll();
    }//GEN-LAST:event_discountTextFocusGained

    void clearAllFields() {
        try {
            billDateFormattedText.setDate(new java.util.Date());
            CommonService.clearTextFields(new JTextField[]{deliveryText, customerText, discountText, deliveryText, paymentTermsText, deleveryTermsText, packingText, invoiceNumberText, salesTaxText});
            CommonService.clearCurrencyFields(new JTextField[]{});
            //multiCurrencyComboBox.setSelectedIndex(0);
            addressTextArea.setText("");
            salesPreformaItemList = null;
            salesPreformaTaxList = null;
            salesPreformaChargesList = null;
            customerLedger = null;
            grandTotalLabel.setText("0.00");
            grandtotal = 0.00;
            netamount = 0.00;
            chargesText.setText("0.00");
            salesPreformaItemTableModel.setRowCount(0);
            //Object currencyItem = multiCurrencyComboBox.getSelectedItem();
            //currency = (MultiCurrency) ((ComboKeyValue) currencyItem).getValue();
            String currencySymbol = (null == currency) ? "" : currency.getSymbol();
            netAmountlLabel.setText(currencySymbol + " " + grandtotal);
            numberProperty = numberPropertyDAO.findInvoiceNo("CommercialInvoice");
            invoiceNumberText.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
            commercialInvoice = null;
            discount = 0.00;
            discountText.setText("0.00");
        } catch (Exception e) {
            log.error("clearAllFields:", e);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel InvoiceDetailsPanel;
    private javax.swing.JTextArea addressTextArea;
    private com.toedter.calendar.JDateChooser billDateFormattedText;
    private javax.swing.JPanel bottomPanel;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton chargesAddButton;
    private javax.swing.JComboBox chargesComboBox;
    private javax.swing.JButton chargesMinusButton;
    private javax.swing.JPanel chargesPanel;
    private javax.swing.JFormattedTextField chargesText;
    private javax.swing.JLabel currencyLabel;
    private javax.swing.JTextField customerText;
    private javax.swing.JTextField deleveryTermsText;
    private javax.swing.JTextField deliveryText;
    private javax.swing.JTextField discountText;
    private javax.swing.JLabel grandTotalLabel;
    private javax.swing.JPanel grandTotalPanel;
    private javax.swing.JLabel grossAmountLabel;
    private javax.swing.JTextField invoiceNumberText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel netAmountlLabel;
    private javax.swing.JTextField packingText;
    private javax.swing.JTextField paymentTermsText;
    private javax.swing.JButton pdfButton;
    private javax.swing.JButton printButton;
    private javax.swing.JTable salePreformaItemTable;
    private javax.swing.JTextField salesTaxText;
    private javax.swing.JButton saveButton;
    private javax.swing.JPanel tableBorderPanel;
    private javax.swing.JButton totDiscountAddButton;
    // End of variables declaration//GEN-END:variables
}
