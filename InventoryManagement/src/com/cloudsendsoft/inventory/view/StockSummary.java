/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.CTableModel;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.PurchaseDAO;
import com.cloudsendsoft.inventory.dao.PurchaseItemDAO;
import com.cloudsendsoft.inventory.dao.PurchaseReturnDAO;
import com.cloudsendsoft.inventory.dao.PurchaseReturnItemDAO;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesItemDAO;
import com.cloudsendsoft.inventory.dao.SalesReturnDAO;
import com.cloudsendsoft.inventory.dao.SalesReturnItemDAO;
import com.cloudsendsoft.inventory.dao.StockGroupDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseReturn;
import com.cloudsendsoft.inventory.model.PurchaseReturnItem;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesReturn;
import com.cloudsendsoft.inventory.model.SalesReturnItem;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.report.JTableReport;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import static com.cloudsendsoft.inventory.utilities.GlobalProperty.company;
import static com.cloudsendsoft.inventory.view.MainForm.jMainFrame;
import static com.cloudsendsoft.inventory.view.PurchaseView.log;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class StockSummary extends javax.swing.JPanel {

    static final Logger log = Logger.getLogger(StockSummary.class.getName());

    CommonService commonService = new CommonService();

    CTableModel stockSummaryTableModel = null;

    List<StockGroup> listOfStockGroup = null;
    List<Item> listOfItem = null;
    
    List<PurchaseItem> listOfPurchaseItem = null;
    List<Purchase> listOfPurchase = null;

    List<PurchaseReturnItem> listOfPurchaseReturnItem = null;
    List<PurchaseReturn> listOfPurchaseReturn = null;
    
    List<SalesItem> listOfSalesItem = null;
    List<Sales> listOfSales = null;
    
    List<SalesReturnItem> listOfSalesReturnItem = null;
    List<SalesReturn> listOfSalesReturn = null;

    StockGroup stockGroup = null;
    Item item = null;
    String month = null;

    StockGroupDAO stockGroupDAO = new StockGroupDAO();
    ItemDAO itemDAO = new ItemDAO();
    PurchaseDAO purchaseDAO = new PurchaseDAO();
    SalesDAO salesDAO = new SalesDAO();

    SalesReturnDAO salesReturnDAO=new SalesReturnDAO();
    PurchaseReturnDAO purchaseReturnDAO=new PurchaseReturnDAO();
    
    PurchaseItemDAO purchaseItemDAO = new PurchaseItemDAO();
    SalesItemDAO salesItemDAO = new SalesItemDAO();

    PurchaseReturnItemDAO purchaseReturnItemDAO=new PurchaseReturnItemDAO();
    SalesReturnItemDAO salesReturnItemDAO=new SalesReturnItemDAO();
    
    int screen = 1;
    int index = 0;

    String[] months = new String[13];

    String invoiceDate = null;
    String invoiceType = null;
    String invoiceLedger = null;
    String invoiceNumber = null;

    PurchaseView purchaseView = null;
    SalesView salesView=null;

    boolean isDuplicateColumnsVisible = false;
    
    public StockSummary() {
        initComponents();
        System.out.println("${app.root}/:"+System.getProperty("java.io.tmpdir"));
        try {
            //shortcut
            // HERE ARE THE KEY BINDINGS
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_W,
                                    java.awt.event.InputEvent.SHIFT_DOWN_MASK
                                    | java.awt.event.InputEvent.CTRL_DOWN_MASK), "forward");
            this.getActionMap().put("forward", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (isDuplicateColumnsVisible) {
                        isDuplicateColumnsVisible = false;
                    } else {
                        isDuplicateColumnsVisible = true;
                    }
                    fillStockSummaryTable();
                }
            });
            // END OF KEY BINDINGS
            //shortcut

            //table background color settings
            jScrollPane1.setOpaque(false);
            jScrollPane1.getViewport().setOpaque(false);
            /*       stockSummaryTable.setBackground(this.getBackground());
             stockSummaryTable.getTableHeader().setBackground(this.getBackground());
             stockSummaryTable.getTableHeader().setForeground(this.getBackground());
             */
            //set table design
            stockSummaryTable.setRowHeight(GlobalProperty.tableRowHieght);

            stockSummaryTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {

                //alternate row color jtable
                @Override
                public Component getTableCellRendererComponent(JTable table,
                        Object value, boolean isSelected, boolean hasFocus,
                        int row, int column) {
                    Component c = super.getTableCellRendererComponent(table,
                            value, isSelected, hasFocus, row, column);
                    c.setBackground(row % 2 == 0 ? Color.GRAY : Color.LIGHT_GRAY);
                    return c;
                }
            ;
            });

        listOfStockGroup = stockGroupDAO.findAllByNameASC();
            //month from company start month to end month
            String monthNames[] = new DateFormatSymbols().getMonths();
            int startIndex = GlobalProperty.company.getStartDate().getMonth();
            int j = 0, i = 0, k = 0;
            for (i = startIndex, j = 0; i < monthNames.length; i++, j++) {
                months[j] = monthNames[i];
            }
            j--;
            for (k = 0; k < startIndex; k++, j++) {
                months[j] = monthNames[k];
            }
            fillStockSummaryTable();
        } catch (Exception e) {
            log.error("StockSummary:", e);
        }
    }

    void fillStockSummaryTable() {
        try {
            int slNo = 1;

            double openingQty = 0.00;
            double openingBal = 0.00;

            double inwardsQty = 0.00;
            double inwardsValue = 0.00;
            double outwardsQty = 0.00;
            double outwardsValue = 0.00;

            double closingQty = 0.00;
            double closingBal = 0.00;

            double totalAmount = 0.00;
            double totalQuantity = 0.00;
            double totalSalesQty = 0.00;
            //double totalSalesReturnsQty = 0.00;

            switch (screen) {
                case 1:
                    //Stock Groups Page
                    leftHeadingLabel.setText("Stock Summary");
                    //table columns,model & size
                    stockSummaryTable.setShowGrid(false);
                    stockSummaryTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                            null,
                            new String[]{"Sr. No.", "Stock Group", "Quantity", "Rate", "Value"}
                    ));
                    stockSummaryTableModel = (CTableModel) stockSummaryTable.getModel();
                    CommonService.setWidthAsPercentages(stockSummaryTable, .05, .75, .20, .20, .20);

                    stockSummaryTableModel.setRowCount(0);
                    // double totalAmount = 0.00;//(clos.bal+purch)-salesReturn

                    Iterator<StockGroup> iter = listOfStockGroup.iterator();

                    while (iter.hasNext()) {
                        totalAmount = 0.00;
                        totalQuantity = 0.00;
                        totalSalesQty = 0.00;
                        stockGroup = iter.next();
                        listOfItem = itemDAO.findAllByStockGroup(stockGroup);
                        if (listOfItem.size() > 0) {
                            for (Item item : listOfItem) {
                                double purchaseQuantity = item.getOpeningQty();
                                double purchaseAmount = item.getOpeningBal();

                                if (isDuplicateColumnsVisible) {
                                    purchaseQuantity += item.getOpeningQty1();
                                    purchaseAmount += item.getOpeningBal1();
                                    
                                    purchaseAmount += purchaseItemDAO.totalAmountByItem1(item);
                                    purchaseQuantity += purchaseItemDAO.totalQuantityByItem1(item);
                                    
                                    purchaseAmount -= purchaseReturnItemDAO.totalAmountByItem1(item);
                                    purchaseQuantity -= purchaseReturnItemDAO.totalQuantityByItem1(item);
                                    
                                    totalSalesQty = salesItemDAO.totalQuantityByItem1(item);
                                    
                                    totalSalesQty -= salesReturnItemDAO.totalQuantityByItem1(item);
                                } else {
                                    purchaseAmount += purchaseItemDAO.totalAmountByItem(item);
                                    purchaseQuantity += purchaseItemDAO.totalQuantityByItem(item);
                                    
                                    purchaseAmount -= purchaseReturnItemDAO.totalAmountByItem(item);
                                    purchaseQuantity -= purchaseReturnItemDAO.totalQuantityByItem(item);
                                    
                                    totalSalesQty = salesItemDAO.totalQuantityByItem(item);
                                    
                                    totalSalesQty -= salesReturnItemDAO.totalQuantityByItem(item);
                                }
                                if(purchaseQuantity>0){
                                    totalAmount += (purchaseAmount / purchaseQuantity) * (purchaseQuantity - totalSalesQty);
                                    totalQuantity += (purchaseQuantity - totalSalesQty);
                                }
                            }
                        }
                        if (totalAmount > 0) {
                            stockSummaryTableModel.addRow(new Object[]{slNo++, stockGroup.getName(), totalQuantity, commonService.formatIntoCurrencyAsString((totalAmount / totalQuantity)), commonService.formatIntoCurrencyAsString(totalAmount)});
                        } else {
                            //iter.remove();
                        }
                    }
                    if (listOfStockGroup.size() == 0) {
                        stockSummaryTableModel.addRow(new Object[]{"", "There is no stock group created or no current stock.. Please create a group or purchase stock first!"});
                    }
                    break;
                case 2:
                    //Items Page
                    if (listOfStockGroup.size() > 0) {
                        //stockGroup = listOfStockGroup.get(index);
                        leftHeadingLabel.setText("Stock Summary");
                        //table columns,model & size
                        rightHeadingLabel.setText("Closing Balance - " + stockGroup.getName());
                        stockSummaryTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                                null,
                                new String[]{"Sr. No.", "Particulars", "Quantity", "Avg.Rate", "Value", "Godown"}
                        ));
                        stockSummaryTableModel = (CTableModel) stockSummaryTable.getModel();
                        CommonService.setWidthAsPercentages(stockSummaryTable, .05, .60, .10, .10, .15, .15);

                        stockSummaryTableModel.setRowCount(0);
                        listOfItem = itemDAO.findAllByStockGroup(stockGroup);

                        if (listOfItem.size() > 0) {
                            for (Item item : listOfItem) {
                                totalAmount = 0.00;
                                totalQuantity = 0.00;
                                totalSalesQty = 0.00;

                                double purchaseQuantity = item.getOpeningQty();
                                double purchaseAmount = item.getOpeningBal();

                                if (isDuplicateColumnsVisible) {
                                    purchaseQuantity += item.getOpeningQty1();
                                    purchaseAmount += item.getOpeningBal1();
                                    
                                    purchaseAmount += purchaseItemDAO.totalAmountByItem1(item);
                                    purchaseQuantity += purchaseItemDAO.totalQuantityByItem1(item);
                                    
                                    purchaseAmount -= purchaseReturnItemDAO.totalAmountByItem1(item);
                                    purchaseQuantity -= purchaseReturnItemDAO.totalQuantityByItem1(item);
                                    
                                    totalSalesQty = salesItemDAO.totalQuantityByItem1(item);
                                    
                                    totalSalesQty -= salesReturnItemDAO.totalQuantityByItem1(item);
                                } else {
                                    purchaseAmount += purchaseItemDAO.totalAmountByItem(item);
                                    purchaseQuantity += purchaseItemDAO.totalQuantityByItem(item);
                                    
                                    purchaseAmount -= purchaseReturnItemDAO.totalAmountByItem(item);
                                    purchaseQuantity -= purchaseReturnItemDAO.totalQuantityByItem(item);
                                    
                                    totalSalesQty = salesItemDAO.totalQuantityByItem(item);
                                    
                                    totalSalesQty -= salesReturnItemDAO.totalQuantityByItem(item);
                                }

                                if (purchaseQuantity > 0) {
                                    totalAmount += (purchaseAmount / purchaseQuantity) * (purchaseQuantity - totalSalesQty);
                                    totalQuantity += (purchaseQuantity - totalSalesQty);
                                    stockSummaryTableModel.addRow(new Object[]{slNo++, item.getDescription(), totalQuantity, commonService.formatIntoCurrencyAsString((totalAmount / totalQuantity)), commonService.formatIntoCurrencyAsString(totalAmount), item.getGodown().getName()});
                                }
         
                            }
                        } else {
                            stockSummaryTableModel.addRow(new Object[]{"", "There is no item created under this stock group..!"});
                        }
                    }
                    break;
                case 3:
                    //Month List Page
                    if (listOfItem.size() > 0) {
                        //item = listOfItem.get(index);
                        leftHeadingLabel.setText("Item Monthly Summary");
                        //table columns,model & size
                        rightHeadingLabel.setText("Item: "+item.getDescription());
                        stockSummaryTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                                null,
                                new String[]{"Particulars", "Inwards Qty", "Inwards Value", "Outwards Qty", "Outwards Value", "Closing Bal. Qty", "Closing Bal. Value"}
                        ));
                        stockSummaryTableModel = (CTableModel) stockSummaryTable.getModel();
                        CommonService.setWidthAsPercentages(stockSummaryTable, .70, .10, .10, .10, .10, .10, .10);

                        stockSummaryTableModel.setRowCount(0);

                        if (item != null) {
                            openingQty = item.getOpeningQty();
                            openingBal = item.getOpeningBal();
                            if (isDuplicateColumnsVisible) {
                                openingQty += item.getOpeningQty1();
                                openingBal += item.getOpeningBal1();
                            }
                        }
                        closingQty = openingQty;
                        closingBal = openingBal;

                        stockSummaryTableModel.addRow(new Object[]{"Opening Balance", "", "", "", "", commonService.blankWhenZero(closingQty),(closingBal>0)?commonService.formatIntoCurrencyAsString(closingBal):commonService.blankWhenZero(closingBal)});
                        totalAmount = 0.00;
                        for (int i = 0; i < months.length - 1; i++) {
                            month = months[i];
                            inwardsQty = 0.00;
                            inwardsValue = 0.00;
                            outwardsQty = 0.00;
                            outwardsValue = 0.00;
                            //purchase items
                            listOfPurchase = purchaseDAO.findAllByMonthName(month);
                            for (Purchase purchase : listOfPurchase) {
                                listOfPurchaseItem = purchase.getPurchaseItems();
                                for (PurchaseItem purchaseItem : listOfPurchaseItem) {
                                    if (purchaseItem.getItem().getItemCode().equalsIgnoreCase(item.getItemCode())) {
                                        inwardsQty += purchaseItem.getQuantity();
                                        inwardsValue += (purchaseItem.getQuantity() * purchaseItem.getUnitPrice());
                                        if (isDuplicateColumnsVisible) {
                                            inwardsQty += purchaseItem.getQuantity1();
                                            inwardsValue += (purchaseItem.getQuantity1() * purchaseItem.getUnitPrice1());
                                        }
                                        break;
                                    }
                                }
                            }
                            
                             //purchase returns items
                            listOfPurchaseReturn = purchaseReturnDAO.findAllByMonthName(month);
                            for (PurchaseReturn purchaseReturn : listOfPurchaseReturn) {
                                listOfPurchaseReturnItem = purchaseReturn.getPurchaseReturnItems();
                                for (PurchaseReturnItem purchaseReturnItem : listOfPurchaseReturnItem) {
                                    if (purchaseReturnItem.getItem().getItemCode().equalsIgnoreCase(item.getItemCode())) {
                                        inwardsQty -= purchaseReturnItem.getQuantity();
                                        inwardsValue -= (purchaseReturnItem.getQuantity() * purchaseReturnItem.getUnitPrice());
                                        if (isDuplicateColumnsVisible) {
                                            inwardsQty -= purchaseReturnItem.getQuantity1();
                                            inwardsValue -= (purchaseReturnItem.getQuantity1() * purchaseReturnItem.getUnitPrice1());
                                        }
                                        break;
                                    }
                                }
                            }
                            
                            closingQty += inwardsQty;
                            closingBal += inwardsValue;

                            //sales items
                            listOfSales = salesDAO.findAllByMonthName(month);
                            for (Sales sales : listOfSales) {
                                listOfSalesItem = sales.getSalesItems();
                                for (SalesItem salesItem : listOfSalesItem) {
                                    if (salesItem.getItem().getItemCode().equalsIgnoreCase(item.getItemCode())) {
                                        outwardsQty += salesItem.getQuantity();
                                        outwardsValue += (salesItem.getQuantity() * salesItem.getUnitPrice());
                                        if (isDuplicateColumnsVisible) {
                                            outwardsQty += salesItem.getQuantity1();
                                            outwardsValue += (salesItem.getQuantity1() * salesItem.getUnitPrice());
                                        }
                                        break;
                                    }
                                }
                            }
                            
                            //sales returns items
                            listOfSalesReturn = salesReturnDAO.findAllByMonthName(month);
                            for (SalesReturn salesReturn : listOfSalesReturn) {
                                listOfSalesReturnItem = salesReturn.getSalesReturnItems();
                                for (SalesReturnItem salesReturnItem : listOfSalesReturnItem) {
                                    if (salesReturnItem.getItem().getItemCode().equalsIgnoreCase(item.getItemCode())) {
                                        outwardsQty -= salesReturnItem.getQuantity();
                                        outwardsValue -= (salesReturnItem.getQuantity() * salesReturnItem.getUnitPrice());
                                        if (isDuplicateColumnsVisible) {
                                            outwardsQty -= salesReturnItem.getQuantity1();
                                            outwardsValue -= (salesReturnItem.getQuantity1() * salesReturnItem.getUnitPrice());
                                        }
                                        break;
                                    }
                                }
                            }
                            
                            closingQty -= outwardsQty;
                            if (inwardsQty > 0) {
                                totalAmount = (closingBal / (openingQty + inwardsQty)) * (closingQty);
                            }
                            stockSummaryTableModel.addRow(new Object[]{month, commonService.blankWhenZero(inwardsQty), (inwardsValue>0)?commonService.formatIntoCurrencyAsString(inwardsValue):commonService.blankWhenZero(inwardsValue),
                                commonService.blankWhenZero(outwardsQty), (outwardsValue>0)?commonService.formatIntoCurrencyAsString(outwardsValue):commonService.blankWhenZero(outwardsValue),
                                commonService.blankWhenZero(closingQty), (totalAmount>0)?commonService.formatIntoCurrencyAsString(totalAmount):commonService.blankWhenZero(totalAmount)});
                        }
                    }
                    break;
                case 4:
                    //Stock Vouchers

                    try {
                        //month = months[index];
                        leftHeadingLabel.setText("Stock Vouchers");
                        //table columns,model & size
                        rightHeadingLabel.setText(item.getDescription() +" "+commonService.sqlDateToString(company.getStartDate()) + " To " + commonService.sqlDateToString(company.getEndDate()));
                        stockSummaryTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                                null,
                                new String[]{"Date", "Particulars", "Invoice Type", "Invoice No.", "Inwards Qty", "Inwards Value", "Outwards Qty", "Outwards Value", "Closing Qty", "Closing Value"}
                        ));
                        stockSummaryTableModel = (CTableModel) stockSummaryTable.getModel();
                        CommonService.setWidthAsPercentages(stockSummaryTable, .05, .35, .10, .10, .10, .10, .10, .10, .10, .10);

                        stockSummaryTableModel.setRowCount(0);

                        if (item != null) {
                            openingQty = item.getOpeningQty();
                            openingBal = item.getOpeningBal();
                            if (isDuplicateColumnsVisible) {
                                openingQty = item.getOpeningQty1();
                                openingBal = item.getOpeningBal1();
                            }
                        }

                        
                        //all purchases for the month
                        List<Purchase> purchaseList = purchaseDAO.findAllByMonthName(month);

                        for (Purchase purchase : purchaseList) {
                            for (PurchaseItem purchaseItem : purchase.getPurchaseItems()) {
                                if (purchaseItem.getItem().getItemCode().equals(item.getItemCode())) {
                                    if (isDuplicateColumnsVisible) {
                                        stockSummaryTableModel.addRow(new Object[]{purchase.getBillDate(), purchase.getSupplier().getLedgerName(),
                                            "Purchase", purchase.getInvoiceNumber(), purchaseItem.getQuantity() + purchaseItem.getQuantity1(), (purchaseItem.getQuantity() * purchaseItem.getUnitPrice()) + (purchaseItem.getQuantity1() * purchaseItem.getUnitPrice1()), (double) 0.00, (double) 0.00});
                                    } else {
                                        stockSummaryTableModel.addRow(new Object[]{purchase.getBillDate(), purchase.getSupplier().getLedgerName(),
                                            "Purchase", purchase.getInvoiceNumber(), purchaseItem.getQuantity(), purchaseItem.getQuantity() * purchaseItem.getUnitPrice(), (double) 0.00, (double) 0.00});
                                    }
                                    break;
                                }
                            }
                        }

                        //all purchase returns for the month
                        List<PurchaseReturn> purchaseReturnList = purchaseReturnDAO.findAllByMonthName(month);
                        for (PurchaseReturn purchaseReturn : purchaseReturnList) {
                            for (PurchaseReturnItem purchaseReturnItem : purchaseReturn.getPurchaseReturnItems()) {
                                if (purchaseReturnItem.getItem().getItemCode().equals(item.getItemCode())) {
                                    if (isDuplicateColumnsVisible) {
                                        stockSummaryTableModel.addRow(new Object[]{purchaseReturn.getBillDate(), purchaseReturn.getSupplier().getLedgerName(),
                                            "Purchase Return", purchaseReturn.getInvoiceNumber(), (double) 0.00, (double) 0.00, purchaseReturnItem.getQuantity() + purchaseReturnItem.getQuantity1(), (purchaseReturnItem.getQuantity()* purchaseReturnItem.getUnitPrice()) +(purchaseReturnItem.getQuantity1() * purchaseReturnItem.getUnitPrice1())});
                                    } else {
                                        stockSummaryTableModel.addRow(new Object[]{purchaseReturn.getBillDate(), purchaseReturn.getSupplier().getLedgerName(),
                                            "Purchase Return", purchaseReturn.getInvoiceNumber(), (double) 0.00, (double) 0.00, purchaseReturnItem.getQuantity(), purchaseReturnItem.getQuantity() * purchaseReturnItem.getUnitPrice()});
                                    }
                                    break;
                                }
                            }
                        }
                        
                        //all sales for the month
                        List<Sales> salesList = salesDAO.findAllByMonthName(month);
                        for (Sales sales : salesList) {
                            for (SalesItem salesItem : sales.getSalesItems()) {
                                if (salesItem.getItem().getItemCode().equals(item.getItemCode())) {
                                    if (isDuplicateColumnsVisible) {
                                        stockSummaryTableModel.addRow(new Object[]{sales.getBillDate(), sales.getCustomer().getLedgerName(),
                                            "Sales", sales.getInvoiceNumber(), (double) 0.00, (double) 0.00, salesItem.getQuantity() + salesItem.getQuantity1(), (salesItem.getQuantity() + salesItem.getQuantity1()) * salesItem.getUnitPrice()});
                                    } else {
                                        stockSummaryTableModel.addRow(new Object[]{sales.getBillDate(), sales.getCustomer().getLedgerName(),
                                            "Sales", sales.getInvoiceNumber(), (double) 0.00, (double) 0.00, salesItem.getQuantity(), salesItem.getQuantity() * salesItem.getUnitPrice()});
                                    }
                                    break;
                                }
                            }
                        }
                        
                        //all sales Return return for the month
                        List<SalesReturn> salesReturnList = salesReturnDAO.findAllByMonthName(month);

                        for (SalesReturn salesReturn : salesReturnList) {
                            for (SalesReturnItem salesReturnItem : salesReturn.getSalesReturnItems()) {
                                if (salesReturnItem.getItem().getItemCode().equals(item.getItemCode())) {
                                    if (isDuplicateColumnsVisible) {
                                        stockSummaryTableModel.addRow(new Object[]{salesReturn.getBillDate(), salesReturn.getCustomer().getLedgerName(),
                                            "Sales Return", salesReturn.getInvoiceNumber(), salesReturnItem.getQuantity() + salesReturnItem.getQuantity1(), (salesReturnItem.getQuantity() * salesReturnItem.getUnitPrice()) + (salesReturnItem.getQuantity1() * salesReturnItem.getUnitPrice()), (double) 0.00, (double) 0.00});
                                    } else {
                                        stockSummaryTableModel.addRow(new Object[]{salesReturn.getBillDate(), salesReturn.getCustomer().getLedgerName(),
                                            "Sales Return", salesReturn.getInvoiceNumber(), salesReturnItem.getQuantity(), salesReturnItem.getQuantity() * salesReturnItem.getUnitPrice(), (double) 0.00, (double) 0.00});
                                    }
                                    break;
                                }
                            }
                        }
                        
                        // Sort all the rows in ascending order based on the
                        // values in the second column of the model
                        commonService.sortAllRowsBy(stockSummaryTableModel, 0, true);

                        stockSummaryTableModel.insertRow(0, new Object[]{GlobalProperty.company.getStartDate(), "Opening Balance", "", "", openingQty, openingBal, 0, 0, 0, 0});

                        closingQty = 0.00;
                        closingBal = openingBal;

                        double totalInwardQty = 0.00;
                        double totalInwardValue = 0.00;

                        inwardsValue = 0.00;

                        outwardsValue = 0.00;
                        inwardsQty = 0.00;
                        outwardsQty = 0.00;
                        //get all rows from jtable
                        for (int count = 0; count < stockSummaryTableModel.getRowCount(); count++) {
                            inwardsQty = Double.parseDouble(stockSummaryTableModel.getValueAt(count, 4) + "");
                            inwardsValue = Double.parseDouble(stockSummaryTableModel.getValueAt(count, 5) + "");
                            totalInwardQty += inwardsQty;
                            totalInwardValue += inwardsValue;
                            double rate =0;
                            if(totalInwardQty>0 && !(stockSummaryTableModel.getValueAt(count, 2) + "").equalsIgnoreCase("Sales Return")){
                                rate=totalInwardValue / totalInwardQty;
                            }
                            
                            closingQty += inwardsQty;

                            outwardsQty = Double.parseDouble(stockSummaryTableModel.getValueAt(count, 6) + "");
                            outwardsValue = Double.parseDouble(stockSummaryTableModel.getValueAt(count, 7) + "");
                            closingQty -= outwardsQty;

                            Date sqlDate = (Date) stockSummaryTableModel.getValueAt(count, 0);
                            stockSummaryTableModel.setValueAt(CommonService.sqlDateToString(sqlDate), count, 0);

                            stockSummaryTableModel.setValueAt(commonService.blankWhenZero(inwardsQty), count, 4);
                            stockSummaryTableModel.setValueAt((inwardsValue>0)?commonService.formatIntoCurrencyAsString(inwardsValue):commonService.blankWhenZero(inwardsValue), count, 5);
                            stockSummaryTableModel.setValueAt(commonService.blankWhenZero(outwardsQty), count, 6);
                            stockSummaryTableModel.setValueAt((outwardsValue>0)?commonService.formatIntoCurrencyAsString(outwardsValue):commonService.blankWhenZero(outwardsValue), count, 7);

                            //if (!(stockSummaryTableModel.getValueAt(count, 2) + "").equalsIgnoreCase("Sales") || (stockSummaryTableModel.getRowCount()-1)== count) {
                                stockSummaryTableModel.setValueAt(commonService.blankWhenZero(closingQty), count, 8);
                                stockSummaryTableModel.setValueAt(((closingQty * rate)>0)?commonService.formatIntoCurrencyAsString(closingQty * rate):commonService.blankWhenZero((closingQty * rate)), count, 9);
                            //}
                        }
                    } catch (Exception e) {
                        log.error("case 4:", e);
                    }
                    break;
                case 5:
                    try {
                       /* if (invoiceType.equalsIgnoreCase("Purchase")) {
                            purchaseView = new PurchaseView(GlobalProperty.mainForm);
                                                        
                            this.add(purchaseView);
                            purchaseView.generateAllList(invoiceDate,invoiceNumber, invoiceLedger);
                            this.revalidate();
                            this.repaint();
                        }else if (invoiceType.equalsIgnoreCase("Sales")) {
                            salesView = new SalesView(GlobalProperty.mainForm);
                            java.sql.Date invDate = new java.sql.Date(commonService.convertToDate(invoiceDate).getTime());
                            Sales salesReturn = salesDAO.findByDateInvNumCustomer(invDate, invoiceNumber, invoiceLedger);
                            this.add(salesView);
                            salesView.listSalesItemHistory(salesReturn);
                        }*/
                    } catch (Exception e) {
                        log.error("case 5:", e);
                    }
                    break;
            }
        } catch (Exception e) {
            log.error("fillStockSummaryTable:", e);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headingPanel = new javax.swing.JPanel();
        leftHeadingLabel = new javax.swing.JLabel();
        rightHeadingLabel = new javax.swing.JLabel();
        pdfButton = new javax.swing.JButton();
        xlsButton = new javax.swing.JButton();
        bodyPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        stockSummaryTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        headingPanel.setBackground(new java.awt.Color(134, 0, 0));
        headingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        headingPanel.setPreferredSize(new java.awt.Dimension(1124, 50));

        leftHeadingLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        leftHeadingLabel.setForeground(new java.awt.Color(255, 255, 153));
        leftHeadingLabel.setText("Stock Summary");

        rightHeadingLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        pdfButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/pdf-icon.png"))); // NOI18N
        pdfButton.setText("PDF");
        pdfButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdfButtonActionPerformed(evt);
            }
        });

        xlsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/xls-icon.png"))); // NOI18N
        xlsButton.setText("XLS");
        xlsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xlsButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout headingPanelLayout = new javax.swing.GroupLayout(headingPanel);
        headingPanel.setLayout(headingPanelLayout);
        headingPanelLayout.setHorizontalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addComponent(leftHeadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 345, Short.MAX_VALUE)
                .addComponent(pdfButton, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xlsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(rightHeadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        headingPanelLayout.setVerticalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, headingPanelLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(leftHeadingLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                        .addComponent(xlsButton)
                        .addComponent(pdfButton))
                    .addComponent(rightHeadingLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        add(headingPanel, java.awt.BorderLayout.PAGE_START);

        bodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        bodyPanel.setOpaque(false);

        stockSummaryTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        stockSummaryTable.setOpaque(false);
        stockSummaryTable.setSelectionBackground(new java.awt.Color(153, 153, 153));
        stockSummaryTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stockSummaryTableMouseClicked(evt);
            }
        });
        stockSummaryTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                stockSummaryTableKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(stockSummaryTable);

        javax.swing.GroupLayout bodyPanelLayout = new javax.swing.GroupLayout(bodyPanel);
        bodyPanel.setLayout(bodyPanelLayout);
        bodyPanelLayout.setHorizontalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1124, Short.MAX_VALUE)
        );
        bodyPanelLayout.setVerticalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 397, Short.MAX_VALUE)
        );

        add(bodyPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void stockSummaryTableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_stockSummaryTableKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE && screen > 1) {
            if(screen==5){
               /* if (invoiceType.equalsIgnoreCase("Purchase")) {
                    this.remove(purchaseView);
                }else if(invoiceType.equalsIgnoreCase("Sales")){
                    this.remove(salesView);
                }
                this.revalidate();
                this.repaint();*/
            }
            screen--;
            fillStockSummaryTable();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER && screen<5) {
            ////////////screen++;
            ////////fillStockSummaryTable();
            index = stockSummaryTable.getSelectedRow();
            screen++;
            clickAction();
        }
    }//GEN-LAST:event_stockSummaryTableKeyPressed

    private void stockSummaryTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stockSummaryTableMouseClicked
        try {
            if (evt.getClickCount() == 2) {
                index = stockSummaryTable.rowAtPoint(evt.getPoint());
                screen++;
                clickAction();
            }
        } catch (Exception e) {
            log.error("stockSummaryTableMouseClicked", e);
        }
    }//GEN-LAST:event_stockSummaryTableMouseClicked

    private void pdfButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdfButtonActionPerformed
        try {
            JTableReport jTableReport = new JTableReport();
            if (screen == 1) {
                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_StockGroup", 5, "pdf");
            } else if (screen == 2) {
                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_Item", 6, "pdf");
            } else if (screen == 3) {
                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_Monthly", 7, "pdf");
            } else if (screen == 4) {
                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_StockVoucher", 10, "pdf");
            }
        } catch (Exception ex) {
            log.error("pdfButtonActionPerformed:", ex);
        }
    }//GEN-LAST:event_pdfButtonActionPerformed

    private void xlsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xlsButtonActionPerformed
        try {
            JTableReport jTableReport = new JTableReport();
            if (screen == 1) {
                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_StockGroup", 5, "xls");
            } else if (screen == 2) {
                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_Item", 6, "xls");
            } else if (screen == 3) {
                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_Monthly", 7, "xls");
            } else if (screen == 4) {
                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_StockVoucher", 10, "xls");
            }
        } catch (Exception ex) {
            log.error("xlsButtonActionPerformed:", ex);
        }
    }//GEN-LAST:event_xlsButtonActionPerformed

    void clickAction() {
        try {
            switch (screen) {
                case 2:
                    stockGroup = listOfStockGroup.get(index);
                    fillStockSummaryTable();
                    break;
                case 3:
                    item = listOfItem.get(index);
                    fillStockSummaryTable();
                    break;
                case 4:
                    if (index > 0) {
                        month = months[index - 1];
                        fillStockSummaryTable();
                    } else {
                        screen--;
                    }
                    break;
                case 5:
                    ArrayList row = CommonService.getTableRowData(stockSummaryTable);
                    if (row.size() > 0) {
                        invoiceDate = (String) row.get(0);
                        invoiceLedger = (String) row.get(1);
                        invoiceType = (String) row.get(2);
                        invoiceNumber = (String) row.get(3);
                        fillStockSummaryTable();
                    }

                    break;

            }
        } catch (Exception e) {
            log.error("clickAction:", e);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bodyPanel;
    private javax.swing.JPanel headingPanel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel leftHeadingLabel;
    private javax.swing.JButton pdfButton;
    private javax.swing.JLabel rightHeadingLabel;
    private javax.swing.JTable stockSummaryTable;
    private javax.swing.JButton xlsButton;
    // End of variables declaration//GEN-END:variables
}
