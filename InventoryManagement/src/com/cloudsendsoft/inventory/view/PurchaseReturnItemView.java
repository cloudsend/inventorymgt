/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.GodownDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.MultiCurrencyDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.dao.PackingTypeDAO;
import com.cloudsendsoft.inventory.dao.PurchaseReturnDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.dao.ShipmentModeDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.PackingList;
import com.cloudsendsoft.inventory.model.PackingListItem;
import com.cloudsendsoft.inventory.model.PackingType;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseCharge;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseReturn;
import com.cloudsendsoft.inventory.model.PurchaseReturnCharge;
import com.cloudsendsoft.inventory.model.PurchaseReturnItem;
import com.cloudsendsoft.inventory.model.PurchaseTax;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaCharge;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesPreformaTax;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class PurchaseReturnItemView extends javax.swing.JPanel {

    /**
     * Creates new form PackingListView
     */
    static final Logger log = Logger.getLogger(PurchaseReturnItemView.class.getName());
    SalesPreforma salesPreforma = null;
    SalesPreformaItem salesPreformaItem = null;
    PackingListItem packingListItem = null;
    PackingList packingList = null;
    DefaultTableModel purchaseItemTableModel = null;
    DefaultTableModel purchaseReturnItemTableModel = null;
    List<SalesPreformaItem> salesPreformaItemList = null;
    //MultiCurrency currency=null;
    List<SalesPreformaTax> salesPreformaTaxList = null;
    List<SalesPreformaCharge> salesPreformaChargesList = null;
    List<PackingListItem> packingListItemList = new ArrayList<PackingListItem>(0);
    NumberProperty numberPropertyPackingListInvoice = null;
    NumberProperty numberPropertyPackingListNumberOfPack = null;
    NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
    CommonService commonService = new CommonService();
    PackingTypeDAO packingDAO = new PackingTypeDAO();
    PackingType packing = null;
    CRUDServices cRUDServices = new CRUDServices();
    //boolean isPackingListTableClicked=false;
    boolean isPackingListNew = false;
    List<PurchaseReturn> purchaseReturnList = new ArrayList<PurchaseReturn>();
    List<PurchaseItem> purchaseItemList = new ArrayList<PurchaseItem>();
    List<PurchaseReturnItem> purchaseReturnItemList = new ArrayList<PurchaseReturnItem>();
    List<PurchaseReturnItem> purchaseReturnItemRemovedList = new ArrayList<PurchaseReturnItem>();
    //List<PurchaseReturnItems>
    MultiCurrency currency = null;
    List<PurchaseTax> purchaseTaxList = null;
    List<PurchaseCharge> purchaseChargesList = null;
    //MultiCurrency currency=null;
    List<PurchaseTax> purchaseRetuenTaxList = new ArrayList<PurchaseTax>();
    List<PurchaseReturnCharge> purchaseReturnChargesList = new ArrayList<PurchaseReturnCharge>();
    int selectedIndex = -1;
    Purchase purchase = null;
    PurchaseReturn purchaseReturn = null;
    boolean isPackingListTableClicked = false;
    PurchaseItem purchaseItem = null;
    PurchaseReturnItem purchaseReturnItem = null;

    NumberProperty numberProperty = null;
    TaxDAO taxDAO = new TaxDAO();
    GodownDAO godownDAO = new GodownDAO();
    ChargesDAO chargesDAO = new ChargesDAO();
    MultiCurrencyDAO multiCurrencyDAO = new MultiCurrencyDAO();
    ShipmentModeDAO shipmentModeDAO = new ShipmentModeDAO();
    PurchaseReturnDAO purchaseReturnDAO = new PurchaseReturnDAO();
    // double subTotal = 0.00;
    Item item = null;
    double itemQty = 0.0;
    double itemQty1 = 0.0;
    double netTotal = 0.00;
    double netTotal1 = 0.00;
    double totDiscount = 0.00;
    double grandTotal = 0.00;
    double totDiscount1 = 0.00;
    double grandTotal1 = 0.00;

    double subTotal = 0.00;
    double subTotal1 = 0.00;

    double netReturnTotal = 0.00;
    double totReturnDiscount = 0.00;
    double grandReturnTotal = 0.00;

    double netReturnTotal1 = 0.00;
    double totReturnDiscount1 = 0.00;
    double grandReturnTotal1 = 0.00;

    int selectedRowIndex = -1;
    int check = 0;

    boolean isDuplicateColumnsVisible = false;

    Ledger purchaseReturnsLedger = null;
    //Ledger discountLedger = null;

    LedgerDAO ledgerDAO = new LedgerDAO();

    public PurchaseReturnItemView() {
        /* initComponents();

         qtyPanel.remove(itemQtyText1);
         unitPricePanel.remove(itemUnitPriceText1);
         discountPanel.remove(itemDiscountText1);
         qtyPanel.revalidate();
         unitPricePanel.revalidate();
         discountPanel.revalidate();
         //shortcut
         // HERE ARE THE KEY BINDINGS
         this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
         put(KeyStroke.getKeyStroke(KeyEvent.VK_W,
         java.awt.event.InputEvent.SHIFT_DOWN_MASK
         | java.awt.event.InputEvent.CTRL_DOWN_MASK), "forward");
         this.getActionMap().put("forward", new AbstractAction() {
         @Override
         public void actionPerformed(ActionEvent e) {
         if(isDuplicateColumnsVisible){
         isDuplicateColumnsVisible = false; 
         qtyPanel.add(itemQtyText1);
         unitPricePanel.add(itemUnitPriceText1);
         discountPanel.add(itemDiscountText1);
         qtyPanel.revalidate();
         unitPricePanel.revalidate();
         discountPanel.revalidate();
                       
         //set table design
         purchaseItemTable.setRowHeight(GlobalProperty.tableRowHieght);
         purchaseItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
         null,
         new String[]{"Sr. No.", "Item Code", "Description", "Godown", "Qty.", "Unit Price", "Qty1", "Unit Price1", "Total Amount"}
         ));
         purchaseItemTableModel = (DefaultTableModel) purchaseItemTable.getModel();
         CommonService.setWidthAsPercentages(purchaseItemTable, .03, .10, .20, .08, .08, .08, .08, .08, .08);
         }else{
         isDuplicateColumnsVisible = true; 
         qtyPanel.remove(itemQtyText1);
         unitPricePanel.remove(itemUnitPriceText1);
         discountPanel.remove(itemDiscountText1);
         qtyPanel.revalidate();
         unitPricePanel.revalidate();
         discountPanel.revalidate();
                        
         //set table design
         purchaseItemTable.setRowHeight(GlobalProperty.tableRowHieght);
         purchaseItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
         null,
         new String[]{"Sr. No.", "Item Code", "Description", "Godown", "Qty.", "Unit Price", "Total Amount"}
         ));
         purchaseItemTableModel = (DefaultTableModel) purchaseItemTable.getModel();
         CommonService.setWidthAsPercentages(purchaseItemTable, .03, .10, .20, .08, .08, .08, .08);
         }
         fillPurchaseItemTable();
         fillPurchaseReturnTable();
         }
         });
         // END OF KEY BINDINGS
         //shortcut
            
         purchaseReturnItemTable.setRowHeight(GlobalProperty.tableRowHieght);
         purchaseReturnItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
         null,
         new String[]{"Sr.No.", "Item Code", "Description", "Unit Price", "Qty.", "Discount", "Total Amount"}
         ));
         purchaseReturnItemTableModel = (DefaultTableModel) purchaseReturnItemTable.getModel();
         CommonService.setWidthAsPercentages(purchaseReturnItemTable, .03, .10, .20, .08, .08, .08, .08);
         */
    }

    public PurchaseReturnItemView(Purchase purchase) {

        initComponents();

        qtyPanel.remove(itemQtyText1);
        unitPricePanel.remove(itemUnitPriceText1);
        discountPanel.remove(itemDiscountText1);
        chrgsPanelHD.remove(chargesText1);
        qtyPanel.revalidate();
        unitPricePanel.revalidate();
        discountPanel.revalidate();
        chrgsPanelHD.revalidate();

        //shortcut
        // HERE ARE THE KEY BINDINGS
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                put(KeyStroke.getKeyStroke(KeyEvent.VK_W,
                                java.awt.event.InputEvent.SHIFT_DOWN_MASK
                                | java.awt.event.InputEvent.CTRL_DOWN_MASK), "forward");
        this.getActionMap().put("forward", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isDuplicateColumnsVisible) {
                    isDuplicateColumnsVisible = false;
                    qtyPanel.remove(itemQtyText1);
                    unitPricePanel.remove(itemUnitPriceText1);
                    discountPanel.remove(itemDiscountText1);
                    chrgsPanelHD.remove(chargesText1);
                    qtyPanel.revalidate();
                    unitPricePanel.revalidate();
                    discountPanel.revalidate();
                    chrgsPanelHD.revalidate();

                    //set table design
                    purchaseItemTable.setRowHeight(GlobalProperty.tableRowHieght);
                    purchaseItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                            null,
                            new String[]{"Sr. No.", "Item Code", "Description", "Godown", "Qty.", "Unit Price", "Total Amount"}
                    ));
                    purchaseItemTableModel = (DefaultTableModel) purchaseItemTable.getModel();
                    CommonService.setWidthAsPercentages(purchaseItemTable, .03, .10, .20, .08, .08, .08, .08);

                    //set table design-2
                    purchaseReturnItemTable.setRowHeight(GlobalProperty.tableRowHieght);
                    purchaseReturnItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                            null,
                            new String[]{"Sr.No.", "Item Code", "Description", "Unit Price", "Qty.", "Total Amount"}
                    ));
                    purchaseReturnItemTableModel = (DefaultTableModel) purchaseReturnItemTable.getModel();
                    CommonService.setWidthAsPercentages(purchaseReturnItemTable, .03, .10, .20, .08, .08, .08);
                } else {
                    isDuplicateColumnsVisible = true;
                    qtyPanel.add(itemQtyText1);
                    unitPricePanel.add(itemUnitPriceText1);
                    discountPanel.add(itemDiscountText1);
                    chrgsPanelHD.add(chargesText1);
                    qtyPanel.revalidate();
                    unitPricePanel.revalidate();
                    discountPanel.revalidate();
                    chrgsPanelHD.revalidate();
                    //set table design
                    purchaseItemTable.setRowHeight(GlobalProperty.tableRowHieght);
                    purchaseItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                            null,
                            new String[]{"Sr. No.", "Item Code", "Description", "Godown", "Qty.", "Unit Price", "Qty1", "Unit Price1", "Total Amount"}
                    ));
                    purchaseItemTableModel = (DefaultTableModel) purchaseItemTable.getModel();
                    CommonService.setWidthAsPercentages(purchaseItemTable, .03, .10, .20, .08, .08, .08, .08, .08, .08);

                    //set table design-2
                    purchaseReturnItemTable.setRowHeight(GlobalProperty.tableRowHieght);
                    purchaseReturnItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                            null,
                            new String[]{"Sr.No.", "Item Code", "Description", "Qty.", "Unit Price", "Qty1", "Unit Price1", "Total Amount"}
                    ));
                    purchaseReturnItemTableModel = (DefaultTableModel) purchaseReturnItemTable.getModel();
                    CommonService.setWidthAsPercentages(purchaseReturnItemTable, .03, .10, .08, .08, .08, .08, .08, .08);
                }
                fillPurchaseItemTable();
                fillPurchaseReturnTable();
            }
        });
        // END OF KEY BINDINGS
        //shortcut

        purchaseReturnsLedger = ledgerDAO.findByLedgerName("Purchase Returns");
        //discountLedger = ledgerDAO.findByLedgerName("Discount Received");

        commonService.setCurrentPeriodOnCalendar(pInvoiceDateFormattedText);

        if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
            saveButton.setEnabled(false);
        }

        for (Charges charges : chargesDAO.findAll()) {
            chargesComboBox.addItem(new ComboKeyValue(charges.getName(), charges));
        }

        //for (MultiCurrency currency : multiCurrencyDAO.findAll()) {
        // if (currency == null) {
        // this.currency = currency;
        //}
        //multiCurrencyComboBox.addItem(new ComboKeyValue(currency.getName(), currency));
        //}
        for (ShipmentMode shipmentMode : shipmentModeDAO.findAll()) {
            shipmentComboBox.addItem(new ComboKeyValue(shipmentMode.getName(), shipmentMode));
        }
        pInvoiceDateFormattedText.setDate(new java.util.Date());
        numberProperty = numberPropertyDAO.findInvoiceNo("PurchaseReturn");
        pInvoiceNumberText.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
        this.purchase = purchase;
        //this.purchaseReturn=purchase.get
        //table background color removed
        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);
        jScrollPane2.setOpaque(false);
        jScrollPane2.getViewport().setOpaque(false);

        //this.salesPreforma=salesPreforma;
        //System.out.println("InvoiceNumber:"+salesPreforma.getInvoiceNumber());
        //sales prefoma table design
        purchaseItemTable.setRowHeight(GlobalProperty.tableRowHieght);
        purchaseItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{"Sr.No.", "Item Code", "Description", "Godown", "Qty.", "Unit Price", "Total Amount"}
        ));
        purchaseItemTableModel = (DefaultTableModel) purchaseItemTable.getModel();
        CommonService.setWidthAsPercentages(purchaseItemTable, .03, .10, .20, .08, .08, .08, .08);

        //salesPreformaItemList=salesPreforma.getSalesPreformaItems();
        purchaseItemList = purchase.getPurchaseItems();
        purchaseTaxList = purchase.getPurchaseTaxes();
        purchaseChargesList = purchase.getPurchaseCharges();
        //=salesPreforma.getSalesPreformaTaxs();
        //salesPreformaChargesList=salesPreforma.getSalesPreformaCharges();
        fillPurchaseItemTable();

        //packing list table design
        purchaseReturnItemTable.setRowHeight(GlobalProperty.tableRowHieght);
        purchaseReturnItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{"Sr.No.", "Item Code", "Description", "Qty.", "Unit Price", "Total Amount"}
        ));
        purchaseReturnItemTableModel = (DefaultTableModel) purchaseReturnItemTable.getModel();
        CommonService.setWidthAsPercentages(purchaseReturnItemTable, .03, .10, .20, .08, .08, .08);

    }

    public void listPurchaseReturnItemHistory(PurchaseReturn purchaseReturn) {
        //Hide the fields
        // leftSidePanel.setVisible(false);
        // purchaseItemTable.setVisible(false);

        this.purchaseReturn = purchaseReturn;
        this.purchase = purchaseReturn.getPurchase();
        //this.PurchaseReturnItemView(purchase);
        //this.SupplierLedger=purchase.getSupplier();
        purchaseReturnItemList = purchaseReturn.getPurchaseReturnItems();
        //purchaseReturnTaxList=purchase.getPurchaseTaxes();
        purchaseReturnChargesList = purchaseReturn.getPurchaseReturnCharges();
        //invoiceNumberText.setText(purchaseReturn.getInvoiceNumber());
        billDateFormattedText.setText(commonService.sqlDateToString(purchaseReturn.getBillDate()));
        invoiceNumberText.setText(purchaseReturn.getPurchase().getInvoiceNumber());
        //.setText(purchaseReturn.getDeliveryDate()+"");
        customerText.setText(purchaseReturn.getSupplier().getLedgerName());
        // System.out.println("salesPreforma.getCustomer():"+purchase.getSupplier().getName());
        deliveryText.setText(purchaseReturn.getDeliveryDate() + "");
        addressTextArea.setText(purchaseReturn.getSupplier().getAddress());
        telNoText.setText(purchaseReturn.getSupplier().getTelephone());
        pInvoiceNumberText.setText(purchaseReturn.getInvoiceNumber());
        //paymentTermsText.setText(purchaseReturn.get);
        //addressTextArea.setText(purchase.getSupplier().getAddress());
        //Updating availableQty of Supplier Ledger
        if (purchase.getSupplier().getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
            purchase.getSupplier().setAvailableBalance(purchase.getSupplier().getAvailableBalance() - purchaseReturn.getGrandTotal());
        } else if (purchase.getSupplier().getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
            purchase.getSupplier().setAvailableBalance(purchase.getSupplier().getAvailableBalance() + purchaseReturn.getGrandTotal());
        }

        int count1 = shipmentComboBox.getItemCount();
        if (purchaseReturn.getShipmentMode() != null) {
            for (int i = 0; i < count1; i++) {
                ComboKeyValue ckv = (ComboKeyValue) shipmentComboBox.getItemAt(i);
                if (ckv.getKey().trim().equalsIgnoreCase(purchaseReturn.getShipmentMode().getName())) {
                    shipmentComboBox.setSelectedItem(ckv);
                }
            }
        }
        saveButton.setText("Update");
        check = 1;
        fillPurchaseReturnTable();

    }

    void fillPurchaseReturnTable() {
        int slNo = 1;
        netReturnTotal = 0.0;
        netReturnTotal1 = 0.0;
        subTotal = 0.00;
        subTotal1 = 0.00;
        //System.out.println("purchaseReturn:"+purchaseReturn.getInvoiceNumber());
        currency = purchase.getMultiCurrency();
        purchaseReturnItemTableModel.setRowCount(0);
        String currencySymbol = (null == currency) ? "" : currency.getSymbol();
        for (PurchaseReturnItem pItem : purchaseReturnItemList) {
            double itemReturnTotal = ((pItem.getUnitPrice() / purchase.getCurrentCurrencyRate()) - (pItem.getDiscount() / purchase.getCurrentCurrencyRate())) * pItem.getQuantity();
            double itemReturnTotal1 = ((pItem.getUnitPrice1() / purchase.getCurrentCurrencyRate()) * pItem.getQuantity1());

            if (isDuplicateColumnsVisible) {
                if (itemReturnTotal > 0 || itemReturnTotal1 > 0) {
                    purchaseReturnItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getQuantity(), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / purchase.getCurrentCurrencyRate()), pItem.getQuantity1(), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice1() / purchase.getCurrentCurrencyRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemReturnTotal + itemReturnTotal1)});
                }
            } else {
                if (itemReturnTotal > 0) {
                    purchaseReturnItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getQuantity(), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / purchase.getCurrentCurrencyRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemReturnTotal)});
                }
            }

            netReturnTotal += itemReturnTotal;
            netReturnTotal1 += itemReturnTotal1;

            subTotal += itemReturnTotal;
            subTotal1 += itemReturnTotal1;
        }
        //adding tax,chrgs,etc
        if (netReturnTotal > 0 || netReturnTotal1 > 0) {
            for (PurchaseReturnCharge purchaseReturnCharge : purchaseReturnChargesList) {
                if (isDuplicateColumnsVisible) {
                    purchaseReturnItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseReturnCharge.getCharges().getName(), "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString((purchaseReturnCharge.getAmount1() / purchase.getCurrentCurrencyRate()) + (purchaseReturnCharge.getAmount() / purchase.getCurrentCurrencyRate()))});
                } else {
                    purchaseReturnItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseReturnCharge.getCharges().getName(), "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(purchaseReturnCharge.getAmount() / purchase.getCurrentCurrencyRate())});
                }

                netReturnTotal += purchaseReturnCharge.getAmount() / purchase.getCurrentCurrencyRate();
                netReturnTotal1 += purchaseReturnCharge.getAmount1() / purchase.getCurrentCurrencyRate();
            }
        }
        grandReturnTotal = netReturnTotal; //- totReturnDiscount;
        grandReturnTotal1 = netReturnTotal1;
        //adding netTotal & grandTotal
        if (netReturnTotal > 0 || netReturnTotal1 > 0 || grandReturnTotal > 0 || grandReturnTotal1 > 0) {
            if (isDuplicateColumnsVisible) {
                purchaseReturnItemTableModel.addRow(new Object[]{"", "", "Net Total", "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(netReturnTotal + netReturnTotal1)});
                purchaseReturnItemTableModel.addRow(new Object[]{"", "", "<html><b>Grand Total</b></html>", "", "", "", "", "<html><b>" + currencySymbol + " " + commonService.formatIntoCurrencyAsString(grandReturnTotal + grandReturnTotal1) + "</b></html>"});

            } else {
                purchaseReturnItemTableModel.addRow(new Object[]{"", "", "Net Total", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(netReturnTotal)});
                purchaseReturnItemTableModel.addRow(new Object[]{"", "", "<html><b>Grand Total</b></html>", "", "", "<html><b>" + currencySymbol + " " + commonService.formatIntoCurrencyAsString(grandReturnTotal) + "</b></html>"});

            }
        }

        CommonService.clearTextFields(new JTextField[]{pItemDescriptionText});
        CommonService.clearNumberFields(new JTextField[]{pItemWeightText, itemQtyText,itemQtyText1, itemUnitPriceText,itemUnitPriceText1,itemDiscountText,itemDiscountText1});

    }

    void fillPurchaseItemTable() {
        try {

            netTotal = 0.00;
            netTotal1 = 0.00;

            //adding items on prefoma table
            purchaseItemTableModel.setRowCount(0);
            int slNo = 1;
            currency = purchase.getMultiCurrency();
            String currencySymbol = (null == currency) ? "" : currency.getSymbol();
            for (PurchaseItem pItem : purchaseItemList) {
                double itemTotal = ((pItem.getUnitPrice() / purchase.getCurrentCurrencyRate()) - (pItem.getDiscount() / purchase.getCurrentCurrencyRate())) * pItem.getQuantity();
                double itemTotal1 = (pItem.getUnitPrice1() / purchase.getCurrentCurrencyRate()) * pItem.getQuantity1();
                if (isDuplicateColumnsVisible) {
                    if (itemTotal1 > 0 || itemTotal > 0) { //only add to jtable if tota is >0
                        purchaseItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getItem().getGodown().getName(), pItem.getQuantity(), currency.getSymbol() + " " + (pItem.getUnitPrice() / currency.getRate()), pItem.getQuantity1(), currency.getSymbol() + " " + (pItem.getUnitPrice1() / currency.getRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal + itemTotal1)});
                    }
                } else {
                    if (itemTotal > 0) {
                        purchaseItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getItem().getGodown().getName(), pItem.getQuantity(), currency.getSymbol() + " " + (pItem.getUnitPrice() / currency.getRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal)});
                    }
                }

                netTotal += itemTotal;
                netTotal1 += itemTotal1;
            }
            //adding tax,chrgs,etc
            if (netTotal > 0 || netTotal1 > 0) {
                for (PurchaseTax purchaseTax : purchaseTaxList) {

                    if (isDuplicateColumnsVisible) {
                        purchaseItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseTax.getTax().getName(), "", "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(purchaseTax.getAmount() / purchase.getCurrentCurrencyRate())});
                    } else {
                        purchaseItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseTax.getTax().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(purchaseTax.getAmount() / purchase.getCurrentCurrencyRate())});
                    }

                    netTotal += purchaseTax.getAmount() / purchase.getCurrentCurrencyRate();
                }

                for (PurchaseCharge purchaseCharge : purchaseChargesList) {

                    if (isDuplicateColumnsVisible) {
                        purchaseItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseCharge.getCharges().getName(), "", "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString((purchaseCharge.getAmount() / purchase.getCurrentCurrencyRate()) + (purchaseCharge.getAmount1() / purchase.getCurrentCurrencyRate()))});
                    } else {
                        purchaseItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseCharge.getCharges().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(purchaseCharge.getAmount() / purchase.getCurrentCurrencyRate())});
                    }

                    netTotal += purchaseCharge.getAmount() / purchase.getCurrentCurrencyRate();
                    netTotal1 += purchaseCharge.getAmount1() / purchase.getCurrentCurrencyRate();
                }
            }
            totDiscount = purchase.getDiscount() / purchase.getCurrentCurrencyRate();
            grandTotal = netTotal - totDiscount;
            totDiscount1 = purchase.getDiscount1() / purchase.getCurrentCurrencyRate();
            grandTotal1 = netTotal1 - totDiscount1;
            if (totDiscount > 0 || totDiscount1 > 0) {
                if (isDuplicateColumnsVisible) {
                    purchaseItemTableModel.addRow(new Object[]{"", "", "Discount", "", "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(totDiscount + totDiscount1)});
                } else {
                    purchaseItemTableModel.addRow(new Object[]{"", "", "Discount", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(totDiscount)});
                }

            }

            //adding netTotal & grandTotal
            if (isDuplicateColumnsVisible) {
                purchaseItemTableModel.addRow(new Object[]{"", "", "Net Total", "", "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(netTotal + netTotal1)});
                purchaseItemTableModel.addRow(new Object[]{"", "", "<html><b>Grand Total</b></html>", "", "", "", "", "", "<html><b>" + currencySymbol + " " + commonService.formatIntoCurrencyAsString(grandTotal + grandTotal1) + "</b></html>"});

            } else {
                purchaseItemTableModel.addRow(new Object[]{"", "", "Net Total", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(netTotal)});
                purchaseItemTableModel.addRow(new Object[]{"", "", "<html><b>Grand Total</b></html>", "", "", "", "<html><b>" + currencySymbol + " " + commonService.formatIntoCurrencyAsString(grandTotal) + "</b></html>"});

            }

            billDateFormattedText.setText(commonService.sqlDateToString(purchase.getBillDate()));
            invoiceNumberText.setText(purchase.getInvoiceNumber());
            customerText.setText(purchase.getSupplier().getLedgerName());
            deleveryTermsText.setText(purchase.getDeliveryDate() + "");
            //paymentTermsText.setText(purchase.());
            addressTextArea.setText(purchase.getSupplier().getAddress());
            telNoText.setText(purchase.getSupplier().getTelephone());
            //telNoText.setText(currencySymbol);
        } catch (Exception e) {
            log.error("fillPurchaseItemTable:", e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headingPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        bodyPanel = new javax.swing.JPanel();
        leftSidePanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        purchaseItemTable = new javax.swing.JTable();
        maxMinPreformaButton = new javax.swing.JButton();
        salesPrefomaDetailPanel = new javax.swing.JPanel();
        billDateFormattedText = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        invoiceNumberText = new javax.swing.JTextField();
        customerText = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        deliveryText = new javax.swing.JTextField();
        paymentTermsText = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        deleveryTermsText = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        addressTextArea = new javax.swing.JTextArea();
        jLabel15 = new javax.swing.JLabel();
        telNoText = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        backButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        rightSidePanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        purchaseReturnItemTable = new javax.swing.JTable();
        maxMinPackingListButton = new javax.swing.JButton();
        packingDetailsPanel = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        pItemWeightText = new javax.swing.JFormattedTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        pInvoiceNumberText = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        pInvoiceDateFormattedText = new com.toedter.calendar.JDateChooser();
        pItemDescriptionText = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        pRtrndeliveryText = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        chargesPanel = new javax.swing.JPanel();
        chargesComboBox = new javax.swing.JComboBox();
        chargesAddButton = new javax.swing.JButton();
        chargesMinusButton = new javax.swing.JButton();
        chrgsPanelHD = new javax.swing.JPanel();
        chargesText = new javax.swing.JFormattedTextField();
        chargesText1 = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        shipmentComboBox = new javax.swing.JComboBox();
        discountPanel = new javax.swing.JPanel();
        itemDiscountText = new javax.swing.JFormattedTextField();
        itemDiscountText1 = new javax.swing.JFormattedTextField();
        jPanel2 = new javax.swing.JPanel();
        pItemAddButton = new javax.swing.JButton();
        pItemMinusButton1 = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        qtyPanel = new javax.swing.JPanel();
        itemQtyText = new javax.swing.JFormattedTextField();
        itemQtyText1 = new javax.swing.JFormattedTextField();
        unitPricePanel = new javax.swing.JPanel();
        itemUnitPriceText = new javax.swing.JFormattedTextField();
        itemUnitPriceText1 = new javax.swing.JFormattedTextField();

        setBackground(new java.awt.Color(102, 102, 102));
        setLayout(new java.awt.BorderLayout());

        headingPanel.setBackground(new java.awt.Color(134, 0, 0));
        headingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        headingPanel.setLayout(new javax.swing.BoxLayout(headingPanel, javax.swing.BoxLayout.LINE_AXIS));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 153));
        jLabel1.setText("Purchase Return Items");
        headingPanel.add(jLabel1);

        add(headingPanel, java.awt.BorderLayout.PAGE_START);

        bodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        bodyPanel.setLayout(new java.awt.GridLayout(1, 0));

        leftSidePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        leftSidePanel.setOpaque(false);

        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder("Purchased Items"));
        jScrollPane1.setOpaque(false);

        purchaseItemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        purchaseItemTable.setOpaque(false);
        purchaseItemTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                purchaseItemTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(purchaseItemTable);

        maxMinPreformaButton.setText(">>");
        maxMinPreformaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maxMinPreformaButtonActionPerformed(evt);
            }
        });

        salesPrefomaDetailPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("User Information"));
        salesPrefomaDetailPanel.setOpaque(false);

        billDateFormattedText.setEditable(false);
        billDateFormattedText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));

        jLabel6.setText("Bill Date");

        jLabel5.setText("Invoice Number");

        invoiceNumberText.setEditable(false);
        invoiceNumberText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                invoiceNumberTextActionPerformed(evt);
            }
        });
        invoiceNumberText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                invoiceNumberTextKeyPressed(evt);
            }
        });

        customerText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customerTextActionPerformed(evt);
            }
        });
        customerText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                customerTextKeyPressed(evt);
            }
        });

        jLabel13.setText("Customer");

        jLabel14.setText("Delivery");

        deliveryText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deliveryTextActionPerformed(evt);
            }
        });
        deliveryText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                deliveryTextKeyPressed(evt);
            }
        });

        paymentTermsText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                paymentTermsTextKeyPressed(evt);
            }
        });

        jLabel12.setText("Payment Terms");

        jLabel11.setText("Delivery Terms");

        deleveryTermsText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleveryTermsTextActionPerformed(evt);
            }
        });
        deleveryTermsText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                deleveryTermsTextKeyPressed(evt);
            }
        });

        jLabel2.setText("Address :");

        addressTextArea.setColumns(20);
        addressTextArea.setLineWrap(true);
        addressTextArea.setRows(5);
        jScrollPane3.setViewportView(addressTextArea);

        jLabel15.setText("Tel No:");

        telNoText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                telNoTextActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel1.setOpaque(false);

        backButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/back.png"))); // NOI18N
        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/cancel.png"))); // NOI18N
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cancelButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(backButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );

        javax.swing.GroupLayout salesPrefomaDetailPanelLayout = new javax.swing.GroupLayout(salesPrefomaDetailPanel);
        salesPrefomaDetailPanel.setLayout(salesPrefomaDetailPanelLayout);
        salesPrefomaDetailPanelLayout.setHorizontalGroup(
            salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(deliveryText, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(customerText, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(invoiceNumberText, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(paymentTermsText)
                    .addComponent(billDateFormattedText, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 217, Short.MAX_VALUE)
                    .addComponent(deleveryTermsText, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(telNoText))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        salesPrefomaDetailPanelLayout.setVerticalGroup(
            salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                            .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(deleveryTermsText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                                    .addGap(14, 14, 14)
                                    .addComponent(jLabel2))
                                .addGroup(salesPrefomaDetailPanelLayout.createSequentialGroup()
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGap(39, 39, 39))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, salesPrefomaDetailPanelLayout.createSequentialGroup()
                            .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(billDateFormattedText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(11, 11, 11)
                            .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(invoiceNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(customerText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(deliveryText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel14))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(paymentTermsText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel12))))
                    .addGroup(salesPrefomaDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(telNoText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout leftSidePanelLayout = new javax.swing.GroupLayout(leftSidePanel);
        leftSidePanel.setLayout(leftSidePanelLayout);
        leftSidePanelLayout.setHorizontalGroup(
            leftSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(leftSidePanelLayout.createSequentialGroup()
                .addGroup(leftSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addComponent(salesPrefomaDetailPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(maxMinPreformaButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(1, 1, 1))
        );
        leftSidePanelLayout.setVerticalGroup(
            leftSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(leftSidePanelLayout.createSequentialGroup()
                .addComponent(maxMinPreformaButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(salesPrefomaDetailPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        bodyPanel.add(leftSidePanel);

        rightSidePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        rightSidePanel.setOpaque(false);

        jScrollPane2.setBorder(javax.swing.BorderFactory.createTitledBorder("Items to be Returned"));
        jScrollPane2.setOpaque(false);

        purchaseReturnItemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        purchaseReturnItemTable.setOpaque(false);
        purchaseReturnItemTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                purchaseReturnItemTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(purchaseReturnItemTable);

        maxMinPackingListButton.setText("<<");
        maxMinPackingListButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maxMinPackingListButtonActionPerformed(evt);
            }
        });

        packingDetailsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Item to return"));
        packingDetailsPanel.setOpaque(false);

        jPanel3.setOpaque(false);

        pItemWeightText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        pItemWeightText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        pItemWeightText.setText("0.00");
        pItemWeightText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                pItemWeightTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                pItemWeightTextFocusLost(evt);
            }
        });
        pItemWeightText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                pItemWeightTextKeyTyped(evt);
            }
        });

        jLabel21.setText("Invoice Date*");

        jLabel7.setText("Invoice Number*");

        jLabel20.setText("Item Weight");

        jLabel22.setText("Unit Price");

        jLabel16.setText("Item Quandity");

        jLabel8.setText("Item Descreption");

        pInvoiceDateFormattedText.setDateFormatString("dd/MM/yyyy");

        jPanel5.setOpaque(false);

        jPanel4.setOpaque(false);

        jLabel23.setText("Discount");

        pRtrndeliveryText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pRtrndeliveryTextActionPerformed(evt);
            }
        });
        pRtrndeliveryText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pRtrndeliveryTextKeyPressed(evt);
            }
        });

        jLabel19.setText("Delivery");

        chargesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Charges"));
        chargesPanel.setOpaque(false);

        chargesAddButton.setText("+");
        chargesAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesAddButtonActionPerformed(evt);
            }
        });

        chargesMinusButton.setText("-");
        chargesMinusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesMinusButtonActionPerformed(evt);
            }
        });

        chrgsPanelHD.setLayout(new java.awt.GridLayout(1, 0));

        chargesText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        chargesText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        chargesText.setText("0.00");
        chargesText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesTextFocusGained(evt);
            }
        });
        chargesText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                chargesTextKeyTyped(evt);
            }
        });
        chrgsPanelHD.add(chargesText);

        chargesText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        chargesText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        chargesText1.setText("0.00");
        chargesText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesText1FocusGained(evt);
            }
        });
        chargesText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                chargesText1KeyTyped(evt);
            }
        });
        chrgsPanelHD.add(chargesText1);

        javax.swing.GroupLayout chargesPanelLayout = new javax.swing.GroupLayout(chargesPanel);
        chargesPanel.setLayout(chargesPanelLayout);
        chargesPanelLayout.setHorizontalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, chargesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chargesComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chrgsPanelHD, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        chargesPanelLayout.setVerticalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chargesPanelLayout.createSequentialGroup()
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chargesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chrgsPanelHD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel3.setText("Shipment Mode");

        discountPanel.setLayout(new java.awt.GridLayout(1, 0));

        itemDiscountText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        itemDiscountText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemDiscountText.setText("0.00");
        itemDiscountText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemDiscountTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemDiscountTextFocusLost(evt);
            }
        });
        itemDiscountText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemDiscountTextKeyTyped(evt);
            }
        });
        discountPanel.add(itemDiscountText);

        itemDiscountText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        itemDiscountText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemDiscountText1.setText("0.00");
        itemDiscountText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemDiscountText1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemDiscountText1FocusLost(evt);
            }
        });
        itemDiscountText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemDiscountText1KeyTyped(evt);
            }
        });
        discountPanel.add(itemDiscountText1);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(discountPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE)
                    .addComponent(pRtrndeliveryText))
                .addContainerGap())
            .addComponent(chargesPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(shipmentComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(1, 1, 1))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(discountPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pRtrndeliveryText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chargesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(shipmentComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(6, 6, 6))
        );

        jPanel2.setLayout(new java.awt.GridLayout(1, 0));

        pItemAddButton.setText("+");
        pItemAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pItemAddButtonActionPerformed(evt);
            }
        });
        jPanel2.add(pItemAddButton);

        pItemMinusButton1.setText("-");
        pItemMinusButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pItemMinusButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(pItemMinusButton1);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(saveButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(saveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        qtyPanel.setLayout(new java.awt.GridLayout(1, 0));

        itemQtyText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        itemQtyText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemQtyText.setText("0.00");
        itemQtyText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemQtyTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemQtyTextFocusLost(evt);
            }
        });
        itemQtyText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemQtyTextKeyTyped(evt);
            }
        });
        qtyPanel.add(itemQtyText);

        itemQtyText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        itemQtyText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemQtyText1.setText("0.00");
        itemQtyText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemQtyText1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemQtyText1FocusLost(evt);
            }
        });
        itemQtyText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemQtyText1KeyTyped(evt);
            }
        });
        qtyPanel.add(itemQtyText1);

        unitPricePanel.setLayout(new java.awt.GridLayout(1, 0));

        itemUnitPriceText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        itemUnitPriceText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemUnitPriceText.setText("0.00");
        itemUnitPriceText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemUnitPriceTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemUnitPriceTextFocusLost(evt);
            }
        });
        itemUnitPriceText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemUnitPriceTextKeyTyped(evt);
            }
        });
        unitPricePanel.add(itemUnitPriceText);

        itemUnitPriceText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        itemUnitPriceText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemUnitPriceText1.setText("0.00");
        itemUnitPriceText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemUnitPriceText1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemUnitPriceText1FocusLost(evt);
            }
        });
        itemUnitPriceText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemUnitPriceText1KeyTyped(evt);
            }
        });
        unitPricePanel.add(itemUnitPriceText1);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(90, 90, 90)
                                .addComponent(pItemDescriptionText))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(8, 8, 8)
                                .addComponent(pInvoiceNumberText)))
                        .addGap(2, 2, 2))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(pInvoiceDateFormattedText, javax.swing.GroupLayout.DEFAULT_SIZE, 176, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(8, 8, 8)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(pItemWeightText)
                                        .addGap(2, 2, 2))
                                    .addComponent(qtyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(8, 8, 8)
                                .addComponent(unitPricePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pInvoiceDateFormattedText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pInvoiceNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pItemDescriptionText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pItemWeightText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(qtyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(unitPricePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout packingDetailsPanelLayout = new javax.swing.GroupLayout(packingDetailsPanel);
        packingDetailsPanel.setLayout(packingDetailsPanelLayout);
        packingDetailsPanelLayout.setHorizontalGroup(
            packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        packingDetailsPanelLayout.setVerticalGroup(
            packingDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(packingDetailsPanelLayout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout rightSidePanelLayout = new javax.swing.GroupLayout(rightSidePanel);
        rightSidePanel.setLayout(rightSidePanelLayout);
        rightSidePanelLayout.setHorizontalGroup(
            rightSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rightSidePanelLayout.createSequentialGroup()
                .addGroup(rightSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(maxMinPackingListButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(packingDetailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        rightSidePanelLayout.setVerticalGroup(
            rightSidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rightSidePanelLayout.createSequentialGroup()
                .addComponent(maxMinPackingListButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 344, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(packingDetailsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        bodyPanel.add(rightSidePanel);

        add(bodyPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void maxMinPackingListButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maxMinPackingListButtonActionPerformed
        try {
            if (maxMinPackingListButton.getText().equalsIgnoreCase("<<")) {
                bodyPanel.removeAll();
                bodyPanel.add(rightSidePanel);
                bodyPanel.revalidate();
                bodyPanel.repaint();
                maxMinPackingListButton.setText(">>");
            } else {

                bodyPanel.removeAll();
                bodyPanel.add(leftSidePanel);
                bodyPanel.add(rightSidePanel);
                bodyPanel.revalidate();
                bodyPanel.repaint();
                maxMinPackingListButton.setText("<<");
            }

        } catch (Exception e) {
            log.error("maxMinPackingListButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_maxMinPackingListButtonActionPerformed

    private void maxMinPreformaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maxMinPreformaButtonActionPerformed
        try {
            if (maxMinPreformaButton.getText().equalsIgnoreCase(">>")) {
                bodyPanel.removeAll();
                bodyPanel.add(leftSidePanel);
                bodyPanel.revalidate();
                bodyPanel.repaint();
                maxMinPreformaButton.setText("<<");
            } else {
                bodyPanel.removeAll();
                bodyPanel.add(leftSidePanel);
                bodyPanel.add(rightSidePanel);
                bodyPanel.revalidate();
                bodyPanel.repaint();
                maxMinPreformaButton.setText(">>");
            }
        } catch (Exception e) {
            log.error("maxMinPreformaButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_maxMinPreformaButtonActionPerformed

    private void invoiceNumberTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_invoiceNumberTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_invoiceNumberTextActionPerformed

    private void invoiceNumberTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_invoiceNumberTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_invoiceNumberTextKeyPressed

    private void customerTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customerTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customerTextActionPerformed

    private void customerTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_customerTextKeyPressed

    }//GEN-LAST:event_customerTextKeyPressed

    private void deliveryTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deliveryTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deliveryTextActionPerformed

    private void deliveryTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_deliveryTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_deliveryTextKeyPressed

    private void paymentTermsTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_paymentTermsTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_paymentTermsTextKeyPressed

    private void deleveryTermsTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleveryTermsTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleveryTermsTextActionPerformed

    private void deleveryTermsTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_deleveryTermsTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleveryTermsTextKeyPressed

    private void purchaseItemTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purchaseItemTableMouseClicked
//       try {
//            if (evt.getClickCount() == 2) {
//                selectedIndex= purchaseItemTable.rowAtPoint(evt.getPoint());
//                clickAction();
//            }
//        } catch (Exception e) {
//            log.error("stockSummaryTableMouseClicked", e);
//        }
        isPackingListTableClicked = false;
        // purchaseReturn.getPurchaseReturnItems()
        purchaseReturnList = purchaseReturnDAO.findAllPurchaseReturn(purchase);

        int selectedRowIndex = purchaseItemTable.rowAtPoint(evt.getPoint());
        if (purchaseItemList.size() >= selectedRowIndex) {
            purchaseItem = purchaseItemList.get(selectedRowIndex);

            itemQty = purchaseItem.getQuantity();
            itemQty1 = purchaseItem.getQuantity1();

            pItemDescriptionText.setText(purchaseItem.getItem().getDescription());
            pItemWeightText.setText(purchaseItem.getItem().getWeight() + "");
            for (PurchaseReturn purchaseReturn : purchaseReturnList) {

                for (PurchaseReturnItem purchaseReturnItem : purchaseReturn.getPurchaseReturnItems()) {
                    if (purchaseReturnItem.getItem().getId() == purchaseItem.getItem().getId()) {
                        //purchaseItem.setQuantity(purchaseItem.getQuantity()-purchaseReturnItem.getQuantity());
                        itemQty -= purchaseReturnItem.getQuantity();
                        //if(isDuplicateColumnsVisible){
                        itemQty1 -= purchaseReturnItem.getQuantity1();
                        //}
                    }
                }
            }
            itemQtyText.setText(itemQty + "");
            itemUnitPriceText.setText(commonService.formatIntoCurrencyAsString(purchaseItem.getUnitPrice() / purchase.getCurrentCurrencyRate()));
            itemDiscountText.setText(commonService.formatIntoCurrencyAsString(purchase.getDiscount() / purchase.getCurrentCurrencyRate()));

            if (isDuplicateColumnsVisible) {
                itemQtyText1.setText(itemQty1 + "");
                itemUnitPriceText1.setText(commonService.formatIntoCurrencyAsString(purchaseItem.getUnitPrice1() / purchase.getCurrentCurrencyRate()));
                itemDiscountText1.setText(commonService.formatIntoCurrencyAsString(purchase.getDiscount1() / purchase.getCurrentCurrencyRate()));

            }

            item = purchaseItem.getItem();
            //pNetWeightText.setText(Double.parseDouble(pContentOfPackageQtyText.getText()) * Double.parseDouble(pItemWeightText.getText()) + "");
            //pGrossWeightText.setText(Double.parseDouble(pNetWeightText.getText()) + Double.parseDouble(pPackageWeightText.getText()) + "");
            //fillPackingListTable();
        } else {
            commonService.clearTextFields(new JTextField[]{pItemDescriptionText, pInvoiceNumberText});
            commonService.clearCurrencyFields(new JTextField[]{pItemWeightText, pItemWeightText, itemQtyText,itemQtyText1, itemUnitPriceText,itemUnitPriceText1,itemDiscountText,itemDiscountText1});
            // pUnitLabel.setText("");
            purchaseReturnItem = null;
        }
    }//GEN-LAST:event_purchaseItemTableMouseClicked

    private void pItemWeightTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pItemWeightTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_pItemWeightTextKeyTyped

    private void pItemWeightTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pItemWeightTextFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_pItemWeightTextFocusLost

    private void pItemWeightTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pItemWeightTextFocusGained
        pItemWeightText.selectAll();
    }//GEN-LAST:event_pItemWeightTextFocusGained

    private void itemQtyTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_itemQtyTextKeyTyped

    private void itemQtyTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyTextFocusLost
        //pNetWeightText.setText(Double.parseDouble(itemQtyText.getText())*Double.parseDouble(pItemWeightText.getText())+"");
        //pGrossWeightText.setText(Double.parseDouble(pNetWeightText.getText())+Double.parseDouble(itemUnitPriceText.getText())+"");
    }//GEN-LAST:event_itemQtyTextFocusLost

    private void itemQtyTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyTextFocusGained
        itemQtyText.selectAll();
    }//GEN-LAST:event_itemQtyTextFocusGained

    private void pItemAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pItemAddButtonActionPerformed
        try {
            if (CommonService.stringValidator(new String[]{pItemDescriptionText.getText()}) && pItemDescriptionText.getText().trim().length() > 0) {
                double itemQtyNow = Double.parseDouble(itemQtyText.getText());
                double itemUnitPrice = Double.parseDouble(itemUnitPriceText.getText());
                double itemQtyNow1 = Double.parseDouble(itemQtyText1.getText());
                double itemUnitPrice1 = Double.parseDouble(itemUnitPriceText1.getText());
                if (itemQtyNow <= (itemQty) && itemQty > 0 || itemQtyNow1 <= (itemQty1) && itemQty1 > 0) {
                    if (itemQtyNow > 0) {
                        purchaseItem.setQuantity(itemQty);
                    }
                    if (itemQtyNow1 > 0) {
                        purchaseItem.setQuantity1(itemQty1);
                    }
                    PurchaseReturnItem purchaseReturnItem = null;
                    for (PurchaseReturnItem pItem : purchaseReturnItemList) {
                        if (pItem.getItem().getItemCode().equalsIgnoreCase(item.getItemCode()) && pItem.getItem().getGodown().getName().equalsIgnoreCase(item.getGodown().getName())) {
                            purchaseReturnItem = pItem;
                            break;
                        }
                    }

                    if (null == purchaseReturnItem) {
                        purchaseReturnItem = new PurchaseReturnItem();
                        purchaseReturnItemList.add(purchaseReturnItem);
                    }

                    purchaseReturnItem.setItem(item);
                    purchaseReturnItem.setQuantity(itemQtyNow);
                    purchaseReturnItem.setUnitPrice(purchaseItem.getUnitPrice());
                    purchaseReturnItem.setDiscount(purchaseItem.getDiscount() * purchase.getCurrentCurrencyRate());

                    purchaseReturnItem.setQuantity1(itemQtyNow1);
                    purchaseReturnItem.setUnitPrice1(purchaseItem.getUnitPrice1());
                    fillPurchaseReturnTable();
                    //purchaseItem=null;
                } else {
                    MsgBox.warning("You do not have enough quandity to return");
                }

            } else {
                MsgBox.warning("No Item Selected.");
            }
        } catch (Exception e) {
            log.error("addItemButtonActionPerformed:", e);
        }

    }//GEN-LAST:event_pItemAddButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed

        try {
            if (purchase != null) {
                if (MsgBox.confirm("Are you sure you want to save?")) {
                    if (purchaseReturn == null) {
                        purchaseReturn = new PurchaseReturn();
                        check = 0;
                    }
                    purchaseReturn.setSupplier(purchase.getSupplier());
                    purchaseReturn.setBillDate(commonService.utilDateToSqlDate(pInvoiceDateFormattedText.getDate()));
                    purchaseReturn.setInvoiceNumber(pInvoiceNumberText.getText());
                    purchaseReturn.setGrandTotal(grandReturnTotal * purchase.getCurrentCurrencyRate());
                    purchaseReturn.setGrandTotal1(grandReturnTotal1 * purchase.getCurrentCurrencyRate());
                    purchaseReturn.setMultiCurrency(purchase.getMultiCurrency());
                    Object shipmentMod = shipmentComboBox.getSelectedItem();
                    ShipmentMode shipmentMode = (ShipmentMode) ((ComboKeyValue) shipmentMod).getValue();
                    if (shipmentMode.getName().equalsIgnoreCase("N/A")) {
                        purchaseReturn.setShipmentMode(null);
                    } else {
                        purchaseReturn.setShipmentMode(shipmentMode);
                    }
                    purchaseReturn.setDeliveryDate(pRtrndeliveryText.getText());

                    purchaseReturn.setPurchase(purchase);

                    purchaseReturn.setPurchaseReturnItems(purchaseReturnItemList);

                    purchaseReturn.setPurchaseReturnCharges(purchaseReturnChargesList);
                    purchaseReturn.setCompany(GlobalProperty.getCompany());
                    if (check == 0) {
                        cRUDServices.saveOrUpdateModel(purchaseReturn);
                        numberProperty.setNumber(numberProperty.getNumber() + 1);
                        cRUDServices.saveOrUpdateModel(numberProperty);
                    } else {
                        cRUDServices.saveOrUpdateModel(purchaseReturn);
                    }

                    //Updating availableQty of Supplier Ledger
                    if (purchase.getSupplier().getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                        purchase.getSupplier().setAvailableBalance(purchase.getSupplier().getAvailableBalance() + grandReturnTotal * purchase.getCurrentCurrencyRate());
                        purchase.getSupplier().setAvailableBalance1(purchase.getSupplier().getAvailableBalance1() + grandReturnTotal1 * purchase.getCurrentCurrencyRate());
                    } else if (purchase.getSupplier().getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                        purchase.getSupplier().setAvailableBalance(purchase.getSupplier().getAvailableBalance() - grandReturnTotal * purchase.getCurrentCurrencyRate());
                        purchase.getSupplier().setAvailableBalance1(purchase.getSupplier().getAvailableBalance1() - grandReturnTotal1 * purchase.getCurrentCurrencyRate());
                    }
                    for (PurchaseReturnItem purchaseReturnItem : purchaseReturnItemList) {
                        double newavalQty = purchaseReturnItem.getItem().getAvailableQty();
                        purchaseReturnItem.getItem().setAvailableQty(newavalQty - purchaseReturnItem.getQuantity());
                        double newavalQty1 = purchaseReturnItem.getItem().getAvailableQty1();
                        purchaseReturnItem.getItem().setAvailableQty1(newavalQty1 - purchaseReturnItem.getQuantity1());

                        cRUDServices.saveOrUpdateModel(purchaseReturnItem.getItem());
                    }
                    //when delete the item completely
                    for (PurchaseReturnItem purchaseReturnItem : purchaseReturnItemRemovedList) {
                        double newavalQty = purchaseReturnItem.getItem().getAvailableQty();
                        purchaseReturnItem.getItem().setAvailableQty(newavalQty + purchaseReturnItem.getQuantity());
                        double newavalQty1 = purchaseReturnItem.getItem().getAvailableQty1();
                        purchaseReturnItem.getItem().setAvailableQty1(newavalQty1 + purchaseReturnItem.getQuantity1());

                        cRUDServices.saveOrUpdateModel(purchaseReturnItem.getItem());
                    }

                    //Updating Purchase Ledger available balance
                    purchaseReturnsLedger.setAvailableBalance((purchaseReturnsLedger.getAvailableBalance()) + subTotal);
                    purchaseReturnsLedger.setAvailableBalance1((purchaseReturnsLedger.getAvailableBalance1()) + subTotal1);
                    cRUDServices.saveOrUpdateModel(purchaseReturnsLedger);

                    //charges
                    //for (Ledger ledger : chargesLedgerList) {
                    //   cRUDServices.saveOrUpdateModel(ledger);
                    //}
                    for (PurchaseReturnCharge purchaseReturnCharge : purchaseReturnChargesList) {
                        Ledger ledger = ledgerDAO.findByLedgerName(purchaseReturnCharge.getCharges().getName());
                        ledger.setAvailableBalance(ledger.getAvailableBalance() + purchaseReturnCharge.getAmount());
                        ledger.setAvailableBalance1(ledger.getAvailableBalance1() + purchaseReturnCharge.getAmount1());
                        cRUDServices.saveOrUpdateModel(ledger);

                    }

                    MsgBox.success("Successfylly Saved PurchaseReturn!");
                    PurchaseReturnView createPurchaseReturn = new PurchaseReturnView();
                    GlobalProperty.mainForm.getContentPanel().removeAll();
                    GlobalProperty.mainForm.getContentPanel().repaint();
                    GlobalProperty.mainForm.getContentPanel().revalidate();
                    GlobalProperty.mainForm.getContentPanel().add(createPurchaseReturn);
                }
            }
            listPurchaseReturnItemHistory(purchaseReturn);
        } catch (Exception e) {
            log.error("saveButtonActionPerformed", e);
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void purchaseReturnItemTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purchaseReturnItemTableMouseClicked
//        isPackingListTableClicked=true;
        selectedRowIndex = purchaseReturnItemTable.rowAtPoint(evt.getPoint());
        //fillPurchaseReturnTable();

//        
    }//GEN-LAST:event_purchaseReturnItemTableMouseClicked

    private void pItemMinusButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pItemMinusButton1ActionPerformed
        if (purchaseReturnItemList.size() > selectedRowIndex) {

            PurchaseReturnItem purchaseReturnItem = purchaseReturnItemList.get(selectedRowIndex);
            //purchaseItem.setQuantity(purchaseItem.getQuantity() + purchaseReturnItem.getQuantity());
            purchaseReturnItemRemovedList.add(purchaseReturnItem);
            purchaseReturnItemList.remove(purchaseReturnItem);
            fillPurchaseReturnTable();
        }
    }//GEN-LAST:event_pItemMinusButton1ActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        PendingCommercialInvoiceView pendingCommercialInvoiceView = new PendingCommercialInvoiceView(GlobalProperty.mainForm.getContentPanel());
        GlobalProperty.mainForm.getContentPanel().removeAll();
        GlobalProperty.mainForm.getContentPanel().repaint();
        GlobalProperty.mainForm.getContentPanel().revalidate();
        GlobalProperty.mainForm.getContentPanel().add(pendingCommercialInvoiceView);
    }//GEN-LAST:event_backButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        CommonService.removeJpanel(GlobalProperty.mainForm.getContentPanel());
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void chargesTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesTextFocusGained
        chargesText.selectAll();
    }//GEN-LAST:event_chargesTextFocusGained

    private void chargesTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chargesTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_chargesTextKeyTyped

    private void chargesAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesAddButtonActionPerformed

        try {
            Object chargesItem = chargesComboBox.getSelectedItem();
            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();
            double chargeAmt = Double.parseDouble(chargesText.getText());
            double chargeAmt1 = Double.parseDouble(chargesText1.getText());
            if (((Double.parseDouble(chargesText.getText()) > 0) || (Double.parseDouble(chargesText.getText()) > 0)) && !charges.getName().equalsIgnoreCase("N/A")) {
                PurchaseReturnCharge purchaseReturnCharge = new PurchaseReturnCharge();
                Iterator<PurchaseReturnCharge> iter = purchaseReturnChargesList.iterator();
                while (iter.hasNext()) {
                    if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                        iter.remove();
                    }
                }
                purchaseReturnCharge.setCharges(charges);
                purchaseReturnCharge.setAmount(chargeAmt * purchase.getCurrentCurrencyRate());
                purchaseReturnCharge.setAmount1(chargeAmt1 * purchase.getCurrentCurrencyRate());
                purchaseReturnChargesList.add(purchaseReturnCharge);
                chargesComboBox.setSelectedIndex(0);
                chargesText.setText("0.00");
                chargesText1.setText("0.00");
                fillPurchaseReturnTable();
            }

        } catch (Exception e) {
            log.error("chargesAddButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesAddButtonActionPerformed

    private void chargesMinusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesMinusButtonActionPerformed
        try {
            Object chargesItem = chargesComboBox.getSelectedItem();
            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();
            Iterator<PurchaseReturnCharge> iter = purchaseReturnChargesList.iterator();
            while (iter.hasNext()) {
                if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                    iter.remove();
                }
            }
            chargesComboBox.setSelectedIndex(0);
            chargesText.setText("0.00");
            chargesText1.setText("0.00");
            fillPurchaseReturnTable();

        } catch (Exception e) {
            log.error("chargesMinusButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesMinusButtonActionPerformed

    private void itemUnitPriceTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemUnitPriceTextFocusGained
        itemUnitPriceText.selectAll();
    }//GEN-LAST:event_itemUnitPriceTextFocusGained

    private void itemUnitPriceTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemUnitPriceTextFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_itemUnitPriceTextFocusLost

    private void itemUnitPriceTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemUnitPriceTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemUnitPriceTextKeyTyped

    private void itemDiscountTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemDiscountTextFocusGained
        itemDiscountText.selectAll();
    }//GEN-LAST:event_itemDiscountTextFocusGained

    private void itemDiscountTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemDiscountTextFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_itemDiscountTextFocusLost

    private void itemDiscountTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemDiscountTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemDiscountTextKeyTyped

    private void telNoTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_telNoTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_telNoTextActionPerformed

    private void pRtrndeliveryTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pRtrndeliveryTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pRtrndeliveryTextActionPerformed

    private void pRtrndeliveryTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pRtrndeliveryTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pRtrndeliveryTextKeyPressed

    private void itemQtyText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyText1FocusGained
        itemQtyText1.selectAll();
    }//GEN-LAST:event_itemQtyText1FocusGained

    private void itemQtyText1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyText1FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_itemQtyText1FocusLost

    private void itemQtyText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyText1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemQtyText1KeyTyped

    private void itemUnitPriceText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemUnitPriceText1FocusGained
        itemUnitPriceText1.selectAll();
    }//GEN-LAST:event_itemUnitPriceText1FocusGained

    private void itemUnitPriceText1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemUnitPriceText1FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_itemUnitPriceText1FocusLost

    private void itemUnitPriceText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemUnitPriceText1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemUnitPriceText1KeyTyped

    private void chargesText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesText1FocusGained
        chargesText1.selectAll();
    }//GEN-LAST:event_chargesText1FocusGained

    private void chargesText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chargesText1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_chargesText1KeyTyped

    private void itemDiscountText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemDiscountText1FocusGained
        itemDiscountText1.selectAll();
    }//GEN-LAST:event_itemDiscountText1FocusGained

    private void itemDiscountText1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemDiscountText1FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_itemDiscountText1FocusLost

    private void itemDiscountText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemDiscountText1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemDiscountText1KeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea addressTextArea;
    private javax.swing.JButton backButton;
    private javax.swing.JFormattedTextField billDateFormattedText;
    private javax.swing.JPanel bodyPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton chargesAddButton;
    private javax.swing.JComboBox chargesComboBox;
    private javax.swing.JButton chargesMinusButton;
    private javax.swing.JPanel chargesPanel;
    private javax.swing.JFormattedTextField chargesText;
    private javax.swing.JFormattedTextField chargesText1;
    private javax.swing.JPanel chrgsPanelHD;
    private javax.swing.JTextField customerText;
    private javax.swing.JTextField deleveryTermsText;
    private javax.swing.JTextField deliveryText;
    private javax.swing.JPanel discountPanel;
    private javax.swing.JPanel headingPanel;
    private javax.swing.JTextField invoiceNumberText;
    private javax.swing.JFormattedTextField itemDiscountText;
    private javax.swing.JFormattedTextField itemDiscountText1;
    private javax.swing.JFormattedTextField itemQtyText;
    private javax.swing.JFormattedTextField itemQtyText1;
    private javax.swing.JFormattedTextField itemUnitPriceText;
    private javax.swing.JFormattedTextField itemUnitPriceText1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPanel leftSidePanel;
    private javax.swing.JButton maxMinPackingListButton;
    private javax.swing.JButton maxMinPreformaButton;
    private com.toedter.calendar.JDateChooser pInvoiceDateFormattedText;
    private javax.swing.JTextField pInvoiceNumberText;
    private javax.swing.JButton pItemAddButton;
    private javax.swing.JTextField pItemDescriptionText;
    private javax.swing.JButton pItemMinusButton1;
    private javax.swing.JFormattedTextField pItemWeightText;
    private javax.swing.JTextField pRtrndeliveryText;
    private javax.swing.JPanel packingDetailsPanel;
    private javax.swing.JTextField paymentTermsText;
    private javax.swing.JTable purchaseItemTable;
    private javax.swing.JTable purchaseReturnItemTable;
    private javax.swing.JPanel qtyPanel;
    private javax.swing.JPanel rightSidePanel;
    private javax.swing.JPanel salesPrefomaDetailPanel;
    private javax.swing.JButton saveButton;
    private javax.swing.JComboBox shipmentComboBox;
    private javax.swing.JTextField telNoText;
    private javax.swing.JPanel unitPricePanel;
    // End of variables declaration//GEN-END:variables

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {

                    SalesPreformaDAO salesPreformaDAO = new SalesPreformaDAO();
                    List<SalesPreforma> listOfSalesPreforma = salesPreformaDAO.findAll();

                    MainForm jMainFrame = new MainForm();
                    jMainFrame.setVisible(true);
                    jMainFrame.setExtendedState(MainForm.MAXIMIZED_BOTH);

                    //PurchaseReturnItemView packingListView=new PurchaseReturnItemView(listOfSalesPreforma.get(0));
                    jMainFrame.getContentPanel().removeAll();
                    jMainFrame.getContentPanel().repaint();
                    jMainFrame.getContentPanel().revalidate();
                    //jMainFrame.getContentPanel().add(packingListView);

                } catch (Exception e) {
                    log.error("MainForm run :", e);
                }
            }
        });
    }
}
