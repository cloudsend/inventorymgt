/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.report;

///import com.cloudsendsoft.inventory.bean.JTableReportBean;
import com.cloudsendsoft.inventory.bean.SalesBean;
import com.cloudsendsoft.inventory.bean.SalesPreformaBean;
import com.cloudsendsoft.inventory.bean.TrialBalanceReportBean;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
//import com.cloudsendsoft.inventory.datasource.JTableDataSource;
import com.cloudsendsoft.inventory.datasource.SalesDataSource;
import com.cloudsendsoft.inventory.datasource.SalesPreformaDataSource;
import com.cloudsendsoft.inventory.datasource.TrialBalanceDataSource;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesCharge;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaCharge;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesPreformaTax;
import com.cloudsendsoft.inventory.model.SalesTax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class TrialBalanceReport {

    static final Logger log = Logger.getLogger(TrialBalanceReport.class.getName());
    CommonService commonService = new CommonService();

    public boolean createTrialBalanceReport(javax.swing.JTable itemTable, String typeOfOperation, String format) {
        JasperPrint jasperPrint;
        JasperReport jasperReport;
        DefaultTableModel model = (DefaultTableModel) itemTable.getModel();
        HashMap<String, Object> jasperParameter = new HashMap<String, Object>();
        //System.out.println("Table row count is"+model.getRowCount());
        List<TrialBalanceReportBean> listOfJTableResult = new ArrayList<TrialBalanceReportBean>(0);
        try {
            String[] fields = null;
            fields = new String[]{"Particulars", "Debit", "Credit"};
            
            for (int count = 0; count < model.getRowCount(); count++) {
                TrialBalanceReportBean bean = new TrialBalanceReportBean();
                if(model.getValueAt(count, 0)!=null){
               // System.out.println("value is"+model.getValueAt(count, 0).toString());
                
                String tmpVal1=model.getValueAt(count, 0).toString();
                tmpVal1=tmpVal1.replaceAll("<html><b>", "");
                tmpVal1=tmpVal1.replaceAll("</b></html>", "");
                
                String tmpVal2=model.getValueAt(count, 1).toString();
                tmpVal2=tmpVal2.replaceAll("<html><b>", "");
                tmpVal2=tmpVal2.replaceAll("</b></html>", "");
                
                String tmpVal3=model.getValueAt(count, 2).toString();
                tmpVal3=tmpVal3.replaceAll("<html><b>", "");
                tmpVal3=tmpVal3.replaceAll("</b></html>", "");
                
                bean.setVal1(tmpVal1);
                bean.setVal2(tmpVal2);
                bean.setVal3(tmpVal3);
                }
                listOfJTableResult.add(bean);
                
                

            }
                TrialBalanceDataSource datasource = new TrialBalanceDataSource(listOfJTableResult, fields);
                String applicationPath = new File(".").getAbsolutePath();
                InputStream layoutFile=null;
                
                    //layoutFile = applicationPath + "\\jrxml\\trialBalance.jrxml";
                     layoutFile = this.getClass().getClassLoader().getResourceAsStream("trialBalance.jrxml");
                
                jasperParameter.put("operationType","TRIAL BALANCE");
                jasperReport = JasperCompileManager.compileReport(layoutFile);
                jasperPrint = JasperFillManager.fillReport(jasperReport, jasperParameter, datasource);

            switch (format) {
                case "print":
                    JasperPrintManager.printReport(jasperPrint, false);
                    break;
                case "pdf":
                    String outputFile = System.getProperty("java.io.tmpdir") + "\\TrialBalance.pdf";
                    commonService.forceDeleteFile(outputFile);
                    JasperExportManager.exportReportToPdfFile(jasperPrint, outputFile);
                    commonService.openPDF(new File(outputFile));
                    break;
            }

            
            return true;
        } catch (Exception e) {
            log.error("Trial Balance Report:", e);
            return false;
        }

    }

    public static void main(String args[]) {

        HibernateUtil hibernateUtil = new HibernateUtil();
        System.out.println("Report");
        TrialBalanceReport report = new TrialBalanceReport();

        SalesDAO salesDAO = new SalesDAO();
        List<Sales> listOfSales = salesDAO.findAll();

      //  report.createSalesReport(listOfSales.get(0));
    }
}
