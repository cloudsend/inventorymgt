/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.report;

import com.cloudsendsoft.inventory.bean.JTableReportBean;
import com.cloudsendsoft.inventory.bean.SalesBean;
import com.cloudsendsoft.inventory.bean.SalesPreformaBean;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.datasource.JTableDataSource;
import com.cloudsendsoft.inventory.datasource.SalesDataSource;
import com.cloudsendsoft.inventory.datasource.SalesPreformaDataSource;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesCharge;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaCharge;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesPreformaTax;
import com.cloudsendsoft.inventory.model.SalesTax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class JTableReport {

    static final Logger log = Logger.getLogger(JTableReport.class.getName());
    CommonService commonService = new CommonService();

    //methode to create report based on jtable
    public boolean createJTableReport(javax.swing.JTable itemTable, String typeOfOperation, Integer columnNo, String format) {
        JasperPrint jasperPrint;
        JasperReport jasperReport;
        DefaultTableModel model = (DefaultTableModel) itemTable.getModel();
        HashMap<String, Object> jasperParameter = new HashMap<String, Object>();
        List<JTableReportBean> listOfJTableResult = new ArrayList<JTableReportBean>(0);
        String title = "";

        try {
            String[] fields = null;
            if ("PurchaseOrder".equalsIgnoreCase(typeOfOperation.trim()) || "CommercialInvoice".equalsIgnoreCase(typeOfOperation.trim()) || "Purchase".equalsIgnoreCase(typeOfOperation.trim()) || "Sales".equalsIgnoreCase(typeOfOperation.trim()) || "SalesPreforma".equalsIgnoreCase(typeOfOperation.trim())) {
                fields = new String[]{"srNo", "InvoiceNo", "Name", "BillDate", "Currency", "Discount", "GrandTotal"};
            }
            if ("Journal".equalsIgnoreCase(typeOfOperation.trim()) || "Contra".equalsIgnoreCase(typeOfOperation.trim()) || "Payment".equalsIgnoreCase(typeOfOperation.trim()) || "Receipt".equalsIgnoreCase(typeOfOperation.trim())) {
                fields = new String[]{"srNo", "JournalNo", "Date", "LedgerBy", "LedgerTo", "Narration", "Amount"};
            }
            if ("PurchaseReturn".equalsIgnoreCase(typeOfOperation.trim()) || "SalesReturn".equalsIgnoreCase(typeOfOperation.trim())) {
                fields = new String[]{"srNo", "InvoiceNo", "Name", "BillDate", "Currency", "GrandTotal"};
            }
            if ("StockSummary_StockVoucher".equalsIgnoreCase(typeOfOperation.trim())) {
                fields = new String[]{"Date", "Particulars", "InvoiceType", "InvoiceNo", "InwardsQty", "InwardsValue", "OutwardsQty", "OutwardsValue", "ClosingQty"};
            }
            if ("StockSummary_Monthly".equalsIgnoreCase(typeOfOperation.trim())) {
                fields = new String[]{"Particulars", "InwardsQty", "InwardsValue", "OutwardsQty", "OutwardsValue", "ClosingBalQty", "ClosingBalValue"};
            }
            if ("StockSummary_Item".equalsIgnoreCase(typeOfOperation.trim())) {
                fields = new String[]{"SrNo", "Particulars","Type", "Quantity", "AvgRate", "Value", "Godown"};
            }
            if ("StockSummary_StockGroup".equalsIgnoreCase(typeOfOperation.trim())) {
                fields = new String[]{"SrNo", "StockGroup", "Quantity", "Rate", "Value"};
            }
            for (int count = 0; count < model.getRowCount(); count++) {
                JTableReportBean bean = new JTableReportBean();
                for (int j = 0; j < columnNo; j++) {
                    switch (j) {
                        case 0:
                            bean.setVal1((model.getValueAt(count, j) == null) ? "" : model.getValueAt(count, j) + "");
                            break;
                        case 1:
                            bean.setVal2((model.getValueAt(count, j) == null) ? "" : model.getValueAt(count, j) + "");
                            break;
                        case 2:
                            bean.setVal3((model.getValueAt(count, j) == null) ? "" : model.getValueAt(count, j) + "");
                            break;
                        case 3:
                            bean.setVal4((model.getValueAt(count, j) == null) ? "" : model.getValueAt(count, j) + "");
                            break;
                        case 4:
                            bean.setVal5((model.getValueAt(count, j) == null) ? "" : model.getValueAt(count, j) + "");
                            break;
                        case 5:
                            bean.setVal6((model.getValueAt(count, j) == null) ? "" : model.getValueAt(count, j) + "");
                            break;
                        case 6:
                            bean.setVal7((model.getValueAt(count, j) == null) ? "" : model.getValueAt(count, j) + "");
                            break;
                        case 7:
                            bean.setVal8((model.getValueAt(count, j) == null) ? "" : model.getValueAt(count, j) + "");
                            break;
                        case 8:
                            bean.setVal9((model.getValueAt(count, j) == null) ? "" : model.getValueAt(count, j) + "");
                            break;
                        case 9:
                            bean.setVal10((model.getValueAt(count, j) == null) ? "" : model.getValueAt(count, j) + "");
                            break;

                    }
                }
                listOfJTableResult.add(bean);

            }
            JTableDataSource datasource = new JTableDataSource(listOfJTableResult, fields);
            String applicationPath = new File(".").getAbsolutePath();
            InputStream layoutFile = null;
            if ("PurchaseOrder".equalsIgnoreCase(typeOfOperation.trim()) || "CommercialInvoice".equalsIgnoreCase(typeOfOperation.trim()) || "Purchase".equalsIgnoreCase(typeOfOperation.trim()) || "Sales".equalsIgnoreCase(typeOfOperation.trim()) || "SalesPreforma".equalsIgnoreCase(typeOfOperation.trim())) {
                //layoutFile = applicationPath + "\\jrxml\\jTableColumn9.jrxml";
                layoutFile = this.getClass().getClassLoader().getResourceAsStream("jTableColumns7.jrxml");
                title = typeOfOperation + " History";
            }
            if ("Journal".equalsIgnoreCase(typeOfOperation.trim()) || "Contra".equalsIgnoreCase(typeOfOperation.trim()) || "Payment".equalsIgnoreCase(typeOfOperation.trim()) || "Receipt".equalsIgnoreCase(typeOfOperation.trim())) {
                //layoutFile = applicationPath + "\\jrxml\\jTableColumn7.jrxml";
                layoutFile = this.getClass().getClassLoader().getResourceAsStream("jTableColumn7.jrxml");
                title = typeOfOperation + " History";
            }
            if ("PurchaseReturn".equalsIgnoreCase(typeOfOperation.trim()) || "SalesReturn".equalsIgnoreCase(typeOfOperation.trim())) {
                //layoutFile = applicationPath + "\\jrxml\\jTableColumn8.jrxml";
                layoutFile = this.getClass().getClassLoader().getResourceAsStream("jTableColumn6.jrxml");
                title = typeOfOperation + " History";
            }
            if ("StockSummary_StockVoucher".equalsIgnoreCase(typeOfOperation.trim())) {
                //layoutFile = applicationPath + "\\jrxml\\StockSummary_StockVoucher.jrxml";
                layoutFile = this.getClass().getClassLoader().getResourceAsStream("StockSummary_StockVoucher.jrxml");
                title = "StockSummary StockVoucher";
            }
            if ("StockSummary_Monthly".equalsIgnoreCase(typeOfOperation.trim())) {
                //layoutFile = applicationPath + "\\jrxml\\StockSummary_Monthly.jrxml";
                layoutFile = this.getClass().getClassLoader().getResourceAsStream("StockSummary_Monthly.jrxml");
                title = "StockSummary - Item Monthly Summary";
            }
            if ("StockSummary_Item".equalsIgnoreCase(typeOfOperation.trim())) {
                //layoutFile = applicationPath + "\\jrxml\\StockSummary_Item.jrxml";
                layoutFile = this.getClass().getClassLoader().getResourceAsStream("StockSummary_Item.jrxml");
                title = "StockSummary - Stock Group Items";
            }
            if ("StockSummary_StockGroup".equalsIgnoreCase(typeOfOperation.trim())) {
                //layoutFile = applicationPath + "\\jrxml\\StockSummary_StockGroup.jrxml";
                layoutFile = this.getClass().getClassLoader().getResourceAsStream("StockSummary_StockGroup.jrxml");
                title = "StockSummary - Stock Group";
            }
            jasperParameter.put("operationType", title);
            jasperReport = JasperCompileManager.compileReport(layoutFile);
            jasperPrint = JasperFillManager.fillReport(jasperReport, jasperParameter, datasource);

            switch (format) {
                case "print":
                    JasperPrintManager.printReport(jasperPrint, false);
                    break;
                case "pdf":
                    String outputFile = System.getProperty("java.io.tmpdir") + "\\StockSummary_StockVoucher.pdf";
                    commonService.forceDeleteFile(outputFile);
                    JasperExportManager.exportReportToPdfFile(jasperPrint, outputFile);
                    commonService.openPDF(new File(outputFile));
                    break;
                case "xls":
                    outputFile = System.getProperty("java.io.tmpdir") + "\\StockSummary_StockVoucher.xls";
                    commonService.forceDeleteFile(outputFile);

                    JRXlsExporter export = new JRXlsExporter();
                    export.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			//export.setParameter(JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED,Boolean.TRUE);
                    //export.setParameter(JRXlsExporterParameter.IS_AUTO_DETECT_CELL_TYPE,Boolean.FALSE);
                    //export.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE);
                    //export.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,Boolean.FALSE);
                    //export.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    export.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, outputFile);
                    export.exportReport();

                    commonService.openXls(new File(outputFile));
                    break;
            }

            return true;
        } catch (Exception e) {
            log.error("JTable Report:", e);
            return false;
        }

    }

    public static void main(String args[]) {

        HibernateUtil hibernateUtil = new HibernateUtil();
        System.out.println("Report");
        JTableReport report = new JTableReport();

        SalesDAO salesDAO = new SalesDAO();
        List<Sales> listOfSales = salesDAO.findAll();

        //  report.createSalesReport(listOfSales.get(0));
    }
}
