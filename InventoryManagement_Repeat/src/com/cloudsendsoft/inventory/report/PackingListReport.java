/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.report;

import com.cloudsendsoft.inventory.bean.PackingListBean;
import com.cloudsendsoft.inventory.bean.SalesPreformaBean;
import com.cloudsendsoft.inventory.dao.PackingListDAO;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.datasource.PackingListDataSource;
import com.cloudsendsoft.inventory.datasource.SalesPreformaDataSource;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.PackingList;
import com.cloudsendsoft.inventory.model.PackingListItem;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaCharge;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesPreformaTax;
import com.cloudsendsoft.inventory.model.SalesTax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class PackingListReport {

    static final Logger log = Logger.getLogger(PackingListReport.class.getName());
    CommonService commonService = new CommonService();

    public boolean createPackingListReport(SalesPreforma salesPreforma, String format) {
        PackingList packingList = salesPreforma.getPackingList();
        JasperPrint jasperPrint;
        JasperReport jasperReport;
        HashMap<String, Object> jasperParameter = new HashMap<String, Object>();
        List<PackingListBean> listOfPackingList = new ArrayList<PackingListBean>(0);
        try {
            String[] fields = new String[]{"srNo", "itemCode", "itemDescription", "numberOfPack", "kindOfPack", "contentOfPack", "netWeight", "grossWeight"};

            Integer srNo = 1;
            double grandTotal = 0.00;

            //adding items
            for (PackingListItem packingListItem : packingList.getPackingListItems()) {
                PackingListBean bean = new PackingListBean();
                bean.setSrNo(srNo);
                bean.setItemCode(packingListItem.getItem().getItemCode());
                bean.setDescription(packingListItem.getItem().getDescription());
                bean.setNumberOfPack(packingListItem.getNumberOfPack());
                bean.setKindOfPack(packingListItem.getKindOfPackage().getName());
                bean.setContentOfPack(packingListItem.getContentsOfPackageQty());
                bean.setNetWeight(packingListItem.getNetWeight());
                bean.setGrossWeight(packingListItem.getGrossWeight());
                listOfPackingList.add(bean);
                //grandTotal+=(bean.getAmount());
                srNo++;
            }

            PackingListDataSource datasource = new PackingListDataSource(listOfPackingList, fields);
            String companyDetails = "";
            jasperParameter.put("billDate", commonService.sqlDateToString(packingList.getInvoiceDate()));
            jasperParameter.put("invoiceNumber", packingList.getInvoiceNumber());
            jasperParameter.put("address", salesPreforma.getPackingList().getAddress());
            jasperParameter.put("customerName", salesPreforma.getCustomer().getLedgerName());
            jasperParameter.put("fax", packingList.getCustomer().getFax());
            jasperParameter.put("email", packingList.getCustomer().getEmail());
            jasperParameter.put("telephone", packingList.getCustomer().getTelephone());
            jasperParameter.put("totalNumberOfPackages", packingList.getTotalNumberOfPackages());
            jasperParameter.put("casesList", "(" + packingList.getCasesList() + ")");
            jasperParameter.put("netWeight", packingList.getNetWeight());
            jasperParameter.put("grossWeight", packingList.getGrossWeight());

            if (GlobalProperty.getCompany().getName() != null) {
                if (GlobalProperty.getCompany().getName().trim().length() > 0) {
                    companyDetails += GlobalProperty.getCompany().getName() + "\n";
                }
            }
            if (GlobalProperty.getCompany().getMailingAddress() != null) {
                if (GlobalProperty.getCompany().getMailingAddress().trim().length() > 0) {
                    companyDetails += GlobalProperty.getCompany().getMailingAddress() + "\n";
                }
            }
            if (GlobalProperty.getCompany().getPinCode() != null) {
                if (GlobalProperty.getCompany().getPinCode().trim().length() > 0) {
                    companyDetails += GlobalProperty.getCompany().getPinCode() + "\n";
                }
            }
            if (GlobalProperty.getCompany().getEmail() != null) {
                if (GlobalProperty.getCompany().getEmail().trim().length() > 0) {
                    companyDetails += "Email:" + GlobalProperty.getCompany().getEmail() + "\n";
                }
            }
            if (GlobalProperty.getCompany().getPhone() != null) {
                if (GlobalProperty.getCompany().getPhone().trim().length() > 0) {
                    companyDetails += "Phone:" + GlobalProperty.getCompany().getPhone() + "\n";
                }
            }
            if (GlobalProperty.getCompany().getFax() != null) {
                if (GlobalProperty.getCompany().getFax().trim().length() > 0) {
                    companyDetails += "Fax:" + GlobalProperty.getCompany().getFax();
                }
            }
            jasperParameter.put("companyDetails", companyDetails);
            jasperParameter.put("customerDetails", salesPreforma.getPackingList().getAddress());

            String applicationPath = new File(".").getAbsolutePath();
            //String layoutFile = applicationPath + "\\jrxml\\PackingList.jrxml";
            InputStream layoutFile = this.getClass().getClassLoader().getResourceAsStream("PackingList.jrxml");
            jasperReport = JasperCompileManager.compileReport(layoutFile);
            jasperPrint = JasperFillManager.fillReport(jasperReport, jasperParameter, datasource);

            switch (format) {
                case "print":
                    JasperPrintManager.printReport(jasperPrint, false);
                    break;
                case "pdf":
                    String outputFile = System.getProperty("java.io.tmpdir")+"\\packingList.pdf";
                    commonService.forceDeleteFile(outputFile);
                    JasperExportManager.exportReportToPdfFile(jasperPrint, outputFile);
                    commonService.openPDF(new File(outputFile));
                    break;
            }
            return true;
        } catch (Exception e) {
            log.error("createSalesPreformaReport:", e);
            return false;
        }
    }

    public static void main(String args[]) {

        HibernateUtil hibernateUtil = new HibernateUtil();
        System.out.println("Report");
        PackingListReport report = new PackingListReport();

        PackingListDAO packingListDAO = new PackingListDAO();
        List<PackingList> listOfPackingList = packingListDAO.findAll();
        File file = new File("2M_letter_logo.png");
        System.out.println("..." + file.getAbsolutePath());
        //report.createPackingListReport(listOfPackingList.get(0));

    }
}
