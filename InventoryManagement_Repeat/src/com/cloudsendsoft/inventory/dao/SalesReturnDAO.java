/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import static com.cloudsendsoft.inventory.dao.SalesDAO.log;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesReturn;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.sql.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class SalesReturnDAO {
    
    static final Logger log = Logger.getLogger(SalesReturnDAO.class.getName());
    
    public List<SalesReturn> findAllSalesReturn() {
        Session session = null;
         List<SalesReturn> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesReturn i where i.company = :comp ORDER BY i.billDate DESC,i.id DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllSalesReturn:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<SalesReturn> findAllSalesReturn(Date fromDate,Date toDate) {
        Session session = null;
         List<SalesReturn> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesReturn i where i.company = :comp and i.billDate>=:key1 AND i.billDate<=:key2 ORDER BY i.billDate DESC,i.id DESC");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllSalesReturn:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<SalesReturn> findAllSalesReturnTill(java.util.Date date) {
        Session session = null;
         List<SalesReturn> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesReturn i where i.company = :comp and i.billDate<:date ORDER BY i.billDate DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            query.setDate("date", date);
            list = query.list();
        } catch (Exception e) {
            log.error("findAllSalesReturnTill:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<SalesReturn> findAllSalesReturn(Sales sales) {
        Session session = null;
         List<SalesReturn> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesReturn i where i.company = :comp and i.sales= :key");
            query.setParameter("key", sales);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllSalesReturn:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
               
  public List<SalesReturn> findAllByInvoiceNo(String invoiceNo) {
        Session session = null;
         List<SalesReturn> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesReturn i where i.company = :comp and i.invoiceNumber LIKE :key");
            query.setParameter("key", invoiceNo + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByInvoiceNo:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<SalesReturn> findAllByDate(Date  fromDate,Date toDate) {
        Session session = null;
         List<SalesReturn> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesReturn i where i.company = :comp and i.billDate >= :key1 AND i.billDate<=:key2");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp", GlobalProperty.getCompany());
            log.info("Sales Return query begins....................."+System.currentTimeMillis());
            list = query.list();
            log.info("Sales Return query begins....................."+System.currentTimeMillis());
        } catch (Exception e) {
            log.error("findAllByDate:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

    public List<SalesReturn> findAllByMonthName(String month) {
        Session session = null;
         List<SalesReturn> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesReturn i where i.company = :comp and monthname(i.billDate) = :month");
            query.setString("month", month);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByMonthName:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public double findTotalSalesReturn(Date fromDate,Date toDate){
    Session session= null;
    double salesReturnTotal=0.00;
    try{ 
        session = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Query query = session.createQuery("select sum(grandTotal) from SalesReturn s where s.company = :comp and s.billDate >= :key1 AND s.billDate <=:key2");
        query.setDate("key1", fromDate);
        query.setDate("key2", toDate);
        query.setParameter("comp",GlobalProperty.getCompany());
        if(query.uniqueResult()!=null)
        {
                salesReturnTotal=(double)query.uniqueResult();
        }
        else
        {
                salesReturnTotal=0.00;
        }
    }catch (Exception e){
         log.error("Find Total Sales Return:",e);
    }
    finally{
        if(session != null && session.isOpen()){
            session.close();
        }
    }
    return salesReturnTotal;
    }
    public double findTotalSalesReturnWithId(int id,Date fromDate , Date toDate)
    {
            Session session= null;
            double salesReturnTotal=0.00;
            try{ 
                session = HibernateUtil.getSessionFactory().openSession();
                org.hibernate.Query query = session.createQuery("select sum(grandTotal) from SalesReturn s where s.company = :comp and s.billDate >= :key1 AND s.billDate <=:key2 AND s.customer.id =:key3");
                query.setDate("key1", fromDate);
                query.setDate("key2", toDate);
                query.setParameter("key3", id);
                query.setParameter("comp",GlobalProperty.getCompany());
                if(query.uniqueResult()!=null)
                {
                        salesReturnTotal=(double)query.uniqueResult();
                }
                else
                {
                        salesReturnTotal=0.00;
                }
            }catch (Exception e){
                 log.error("Find Total Sales Return With Id:",e);
            }
            finally{
                if(session != null && session.isOpen()){
                    session.close();
                }
            }
            return salesReturnTotal;
            }
    public void deleteSalesReturn(SalesReturn salesReturn)
    {
        Session session =HibernateUtil.getSessionFactory().openSession();
        Transaction  transaction=session.beginTransaction();
        session.delete(salesReturn);
        transaction.commit();
        session.close();  
    }
}
