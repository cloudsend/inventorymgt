/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class SalesItemDAO {
    
    static final Logger log = Logger.getLogger(SalesItemDAO.class.getName());
    
    public List<SalesItem> findAllByItem(Item item) {
        Session session = null;
         List<SalesItem> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesItem i where i.item = :item");
            query.setParameter("item", item);
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByItem:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<SalesItem> findAllByItemCodeDESC(String itemCode) {
        Session session = null;
         List<SalesItem> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesItem i where i.item.itemCode = :itemCode");
            query.setParameter("itemCode", itemCode);
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByItemCode:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
     public Double totalAmountByItem(Item item) {
        Session session = null;
         Double amt = 0.00;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(quantity*unitPrice) from SalesItem i where i.item = :item");
            query.setParameter("item", item);
            amt = (Double) query.uniqueResult();
        } catch (Exception e) {
            log.error("totalAmountByItem:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return (amt==null)?0.00:amt;
    }

     public Double totalQuantityByItem(Item item) {
        Session session = null;
         Double amt = 0.0;
        try {    
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(i.quantity) from SalesItem i where i.item = :item");
            query.setParameter("item", item);
            amt = (Double) query.uniqueResult();
        } catch (Exception e) {
            log.error("totalQuantityByItem:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return (amt==null)?0:amt;
    }
     public Double totalQuantityByItem1(Item item) {
        Session session = null;
         Double amt = 0.0;
        try {    
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(i.quantity+i.quantity1) from SalesItem i where i.item = :item");
            query.setParameter("item", item);
            amt = (Double) query.uniqueResult();
        } catch (Exception e) {
            log.error("totalQuantityByItem1:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return (amt==null)?0:amt;
    }
}
