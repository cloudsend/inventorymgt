/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import static com.cloudsendsoft.inventory.dao.PurchaseDAO.log;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.sql.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class SalesDAO {
    
    static final Logger log = Logger.getLogger(SalesDAO.class.getName());
    
    public List<Sales> findAll() {
        Session session = null;
         List<Sales> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Sales i where i.company = :comp order by id");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<Sales> findAllByDate(Date  fromDate,Date toDate) {
        Session session = null;
         List<Sales> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Sales i where i.company = :comp and i.billDate >= :key1 AND i.billDate<=:key2");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp", GlobalProperty.getCompany());
            log.info("Sales query begins....................."+System.currentTimeMillis());
            list = query.list();
            log.info("Sales query ends....................."+System.currentTimeMillis());            
        } catch (Exception e) {
            log.error("findAllByDate:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Sales> findAllSales() {
        Session session = null;
         List<Sales> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Sales i where i.company = :comp ORDER BY i.billDate DESC,i.id DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllSales:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Sales> findAllSales(Date fromDate,Date toDate) {
        Session session = null;
         List<Sales> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Sales i where i.company = :comp and i.billDate>= :key1 AND i.billDate<= :key2 ORDER BY i.billDate DESC,i.id DESC");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllSales:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<Sales> findAllSalesTill(java.util.Date date) {
        Session session = null;
         List<Sales> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Sales i where i.company = :comp and i.billDate<:date ORDER BY i.billDate DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            query.setDate("date", date);
            list = query.list();
        } catch (Exception e) {
            log.error("findAllSalesTill:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<Sales> findAllByMonthName(String month) {
        Session session = null;
         List<Sales> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Sales i where i.company = :comp and monthname(i.billDate) = :month");
            query.setString("month", month);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByMonthName:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<Sales> findAllByCustomer(Ledger ledger) {
        Session session = null;
         List<Sales> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Sales i where i.company = :comp and i.customer = :key");
            query.setParameter("key", ledger);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByCustomer:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Sales> findAllByInvoiceNo(String invoiceNo) {
        Session session = null;
         List<Sales> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Sales i where i.company = :comp and i.invoiceNumber LIKE :key");
            query.setParameter("key", invoiceNo + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByInvoiceNo:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<Sales> findAllBySalesItem(SalesItem item) {
        Session session = null;
         List<Sales> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Sales i where i.company = :comp and i.salesItems = :item");
            query.setParameter("item", item);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByPurchaseItem:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public Sales findByDateInvNumCustomer(java.sql.Date billDate,String invoiceNumber,String ledgerName) {
        Session session = null;
         Sales sales= null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Sales i where i.company = :comp and i.billDate = :billDate and i.invoiceNumber = :invoiceNumber and customer.ledgerName = :ledgerName");
            query.setDate("billDate",billDate);
            query.setString("invoiceNumber",invoiceNumber);
            query.setString("ledgerName", ledgerName);
            query.setParameter("comp", GlobalProperty.getCompany());
            sales = (Sales) query.uniqueResult();
        } catch (Exception e) {
            log.error("findByDateInvNumCustomer:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return sales;
    }
     public double findTotalSales(Date fromDate,Date toDate){
        Session session= null;
        double salesTotal=0.00;
        try{ 
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select (sum(grandTotal)+sum(discount)) from Sales s where s.company = :comp and s.billDate >= :key1 AND s.billDate <=:key2");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp",GlobalProperty.getCompany());
            if(query.uniqueResult()!=null)
            {
                    salesTotal=(double)query.uniqueResult();
            }
            else
            {
                    salesTotal=0.00;
            }
        }catch (Exception e){
             log.error("Find Total Purchase:",e);
        }
        finally{
            if(session != null && session.isOpen()){
                session.close();
            }
        }
        return salesTotal;
    }
     public double findTotalSalesWithId(int id,Date fromDate,Date toDate){
        Session session= null;
        double salesTotal=0.00;
        try{ 
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(grandTotal) from Sales s where s.company = :comp and s.billDate >= :key1 AND s.billDate <=:key2 AND s.customer.id=:key3");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("key3", id);
            query.setParameter("comp",GlobalProperty.getCompany());
            if(query.uniqueResult()!=null)
            {
                    salesTotal=(double)query.uniqueResult();
            }
            else
            {
                    salesTotal=0.00;
            }
        }catch (Exception e){
             log.error("Find Total Purchase:",e);
        }
        finally{
            if(session != null && session.isOpen()){
                session.close();
            }
        }
        return salesTotal;
    }
     public double findTotalDiscount(Date fromDate,Date toDate){
        Session session= null;
        double discountTotal=0.00;
        try{ 
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(discount) from Sales s where s.company = :comp and s.billDate >= :key1 AND s.billDate <=:key2");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp",GlobalProperty.getCompany());
            if(query.uniqueResult()!=null)
            {
                    discountTotal=(double)query.uniqueResult();
            }
            else
            {
                    discountTotal=0.00;
            }
        }catch (Exception e){
             log.error("Find Total Purchase:",e);
        }
        finally{
            if(session != null && session.isOpen()){
                session.close();
            }
        }
        return discountTotal;
    }
public void deleteSales(Sales sales)
 {
        Session session =HibernateUtil.getSessionFactory().openSession();
        Transaction  transaction=session.beginTransaction();
        session.delete(sales);
        transaction.commit();
        session.close();            

 }
}
