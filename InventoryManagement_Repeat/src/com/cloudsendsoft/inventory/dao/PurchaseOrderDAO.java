/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
//import com.cloudsendsoft.inventory.model.Purchase;
//import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseOrder;
import com.cloudsendsoft.inventory.model.PurchaseOrderItem;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.sql.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class PurchaseOrderDAO {
    
    static final Logger log = Logger.getLogger(PurchaseOrderDAO.class.getName());
    
    public List<PurchaseOrder> findAllByMonthName(String month) {
        Session session = null;
         List<PurchaseOrder> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseOrder i where i.company = :comp and monthname(i.billDate) =  :month");
            query.setString("month", month);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByMonthName:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public PurchaseOrder findByDateInvNumSupplier(java.sql.Date billDate,String invoiceNumber,String ledgerName) {
        Session session = null;
         PurchaseOrder purchaseOrder= null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseOrder i where i.company = :comp and i.billDate = :billDate and i.invoiceNumber = :invoiceNumber and supplier.ledgerName = :ledgerName");
            query.setDate("billDate",billDate);
            query.setString("invoiceNumber",invoiceNumber);
            query.setString("ledgerName", ledgerName);
            query.setParameter("comp", GlobalProperty.getCompany());
            purchaseOrder = (PurchaseOrder) query.uniqueResult();
        } catch (Exception e) {
            log.error("findItemByItemCode:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return purchaseOrder;
    }
    public List<PurchaseOrder> findAllByPurchaseItem(PurchaseOrderItem item) {
        Session session = null;
         List<PurchaseOrder> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseOrder i where i.company = :comp and i.purchaseItems = :item");
            query.setParameter("item", item);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByPurchaseItem:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<PurchaseOrder> findAllBySupplier(Ledger ledger) {
        Session session = null;
         List<PurchaseOrder> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseOrder i where i.company = :comp and i.supplier = :key");
            query.setParameter("key", ledger);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllBySupplier:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<PurchaseOrder> findAllPurchase() {
        Session session = null;
         List<PurchaseOrder> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseOrder i where i.company = :comp ORDER BY i.billDate DESC,i.id DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllPurchaseOrder:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<PurchaseOrder> findAllByBillDateASC() {
        Session session = null;
         List<PurchaseOrder> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseOrder i where i.company = :comp ORDER BY i.billDate ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByBillDateASC:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<PurchaseOrder> findAllByInvoiceNo(String invoiceNo) {
        Session session = null;
         List<PurchaseOrder> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseOrder i where i.company = :comp and i.invoiceNumber LIKE :key");
            query.setParameter("key", invoiceNo + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByInvoiceNo:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<PurchaseOrder> findAllByDate(Date  fromDate,Date toDate) {
        Session session = null;
         List<PurchaseOrder> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseOrder i where i.company = :comp and i.billDate >= :key1 AND i.billDate<=:key2");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByDate:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
}
