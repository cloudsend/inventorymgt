/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Company;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class ChargesDAO {
    
    static final Logger log = Logger.getLogger(ChargesDAO.class.getName());   
    public List<Charges> findAllByDESC() {
        Session session = null;
        List<Charges> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Charges c where c.company = :key ORDER BY id DESC");
            query.setParameter("key", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByDESC:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Charges> findAll() {
        Session session = null;
        List<Charges> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Charges c where c.company = :key ORDER BY id");
            query.setParameter("key", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Charges> findAll(Company comp) {
        Session session = null;
        List<Charges> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Charges c where c.company = :key ORDER BY id");
            query.setParameter("key", comp);
            //comp = (Company) query.uniqueResult();
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

    public Charges findByName(String name) {
        Session session = null;
         Charges charge= null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Charges i where i.name = :key and i.company = :key1");
            query.setParameter("key1", GlobalProperty.getCompany());
            query.setString("key", name);
            charge = (Charges) query.uniqueResult();
        } catch (Exception e) {
            log.error("findByName:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return charge;
    }
    
    public Charges findByNameAndCompany(String name,Company company) {
        Session session = null;
         Charges charge= null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Charges i where i.name = :key and i.company = :key1");
            query.setParameter("key1", company);
            query.setString("key", name);
            charge = (Charges) query.uniqueResult();
        } catch (Exception e) {
            log.error("findByNameAndCompany:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return charge;
    }
    public List<Charges> findAllStartsWithCharges(String charge) {
        Session session = null;
        List<Charges> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Charges c where c.company = :comp and c.name LIKE :key ORDER BY c.name ASC");
            query.setString("key", charge + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllStartsWithTax:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
           
}
