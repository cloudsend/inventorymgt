/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.LedgerView;
import com.cloudsendsoft.inventory.view.MainForm;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class LedgerDAO {
    
    static final Logger log = Logger.getLogger(LedgerDAO.class.getName());
    
    /*public Ledger findLedgerById(Long item_id) {
        Session session = null;
        Ledger ledger = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction trans = session.getTransaction();
            trans.begin();
            ledger = (Ledger) session.get(Ledger.class, Long.parseLong(item_id + ""));
            trans.commit();
        } catch (Exception e) {
            log.error("findLedgerById:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return ledger;
    }*/

    public List<Ledger> findAllByDESC() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l WHERE l.company = :comp ORDER BY id DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByDESC:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }
    public List<Ledger> findAll() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l WHERE l.company = :comp ORDER BY id");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }

   /*public List<Ledger> findAllName() {
        Session session = null;
        List<Item> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Item ORDER BY itemName ASC");
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return list;
    }*/    
     public Ledger findByLedgerName(String name) {
        Session session = null;
         Ledger ledger= null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.ledgerName = :key and l.company = :comp");
            query.setString("key", name);
            query.setParameter("comp", GlobalProperty.getCompany());
            ledger = (Ledger) query.uniqueResult();
        } catch (Exception e) {
            log.error("findByLedgerName:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return ledger;
    }
   
    public boolean isLedgerNameExist(String name) {
        Query query = HibernateUtil.getSessionFactory().openSession().
                createQuery("select 1 from Ledger l where l.ledgerName = :key and l.company = :comp");
        query.setString("key", name);
        query.setParameter("comp", GlobalProperty.getCompany());
        return (query.uniqueResult() != null);
    }

    public List<Ledger> findAllStartsWithSupplier(String supplier) {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id=1 or l.ledgerGroup.id=31 or l.ledgerGroup.id=6) and l.ledgerName LIKE :key ORDER BY l.ledgerName ASC");
            query.setString("key", supplier + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllStartsWithSupplier:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllSupplier() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id=1 or l.ledgerGroup.id=31 or l.ledgerGroup.id=6)  ORDER BY l.ledgerName ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllSupplier:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllExpenses() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and ((l.ledgerGroup.accountType='Indirect Expense'OR l.ledgerGroup.accountType='Direct Expense') AND l.ledgerName!='Discount Allowed' AND l.ledgerGroup.groupName!='Purchase Accounts' )  ORDER BY l.ledgerName ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllExpenses:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllIncomes() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and ((l.ledgerGroup.accountType='Indirect Income' OR l.ledgerGroup.accountType='Direct Income') AND l.ledgerName!='Discount Received' AND l.ledgerGroup.groupName!='Sales Accounts' )  ORDER BY l.ledgerName ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllExpenses:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllStartsWithCustomer(String customer) {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id=1 or l.ledgerGroup.id=32 or l.ledgerGroup.id=6) and l.ledgerName LIKE :key ORDER BY l.ledgerName ASC");
            query.setString("key", customer + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllStartsWithCustomer:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllCustomer() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id=1 or l.ledgerGroup.id=32 or l.ledgerGroup.id=6) ORDER BY l.ledgerName ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllCustomer:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllCustomerForTable() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and l.ledgerGroup.id=32  ORDER BY l.ledgerName ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllCustomer:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllCustomerPaymentBy() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id !=1 and l.ledgerGroup.id !=6 and l.ledgerGroup.accountType!='Direct Income' and l.ledgerGroup.accountType!='Indirect Income') ORDER BY l.ledgerName ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllCustomerPaymentBy:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllCustomerForPurchaseReturnBy() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id =1 or l.ledgerGroup.id =6 or l.ledgerGroup.id =31) ORDER BY l.ledgerName ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllCustomerForPurchaseReturnBy:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllCustomerForSalesReturnBy() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id =1 or l.ledgerGroup.id =6 or l.ledgerGroup.id =32) ORDER BY l.ledgerName ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllCustomerForSalesReturnBy:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<Ledger> findAllCustomerJournalBy() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id !=1 and l.ledgerGroup.id !=6) ORDER BY l.ledgerName ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllCustomerJournalBy:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllCustomerContraBy() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id =1 or l.ledgerGroup.id =6) ORDER BY l.ledgerName ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllCustomerContraBy:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<Ledger> findAllStartsWithCustomerPaymentBy(String customer) {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id !=1 and l.ledgerGroup.id !=6 and l.ledgerGroup.accountType!='Direct Income' and l.ledgerGroup.accountType!='Indirect Income') and l.ledgerName LIKE :key ORDER BY l.ledgerName ASC");
            query.setString("key", customer + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllStartsWithCustomerPaymentBy:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllStartsWithCustomerJournalBy(String customer) {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id !=1 and l.ledgerGroup.id !=6) and l.ledgerName LIKE :key ORDER BY l.ledgerName ASC");
            query.setString("key", customer + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllStartsWithCustomerJournalBy:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllStartsWithCustomerContraBy(String customer) {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id =1 or l.ledgerGroup.id =6) and l.ledgerName LIKE :key ORDER BY l.ledgerName ASC");
            query.setString("key", customer + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllStartsWithCustomerContraBy:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllCustomerPaymentTo() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id =1 or l.ledgerGroup.id =6) ORDER BY l.ledgerName ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllCustomerPaymentTo:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllCustomerJournalTo() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id !=1 and l.ledgerGroup.id !=6) ORDER BY l.ledgerName ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllCustomerJournalTo:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllCustomerContraTo() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id =1 or l.ledgerGroup.id =6) ORDER BY l.ledgerName ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllCustomerContraTo:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllStartsWithCustomerPaymentTo(String customer) {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and l.ledgerName LIKE :key and (l.ledgerGroup.id =1 or l.ledgerGroup.id =6) ORDER BY l.ledgerName ASC");
            query.setString("key", customer + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllStartsWithCustomerPaymentTo:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllStartsWithCustomerJournalTo(String customer) {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id !=1 and l.ledgerGroup.id !=6) and l.ledgerName LIKE :key ORDER BY l.ledgerName ASC");
            query.setString("key", customer + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllStartsWithCustomerJournalTo:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllStartsWithCustomerContraTo(String customer) {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id =1 or l.ledgerGroup.id =6) and l.ledgerName LIKE :key ORDER BY l.ledgerName ASC");
            query.setString("key", customer + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllStartsWithCustomerContraTo:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<Ledger> findAllCustomerReceiptTo() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id !=1 and l.ledgerGroup.id !=6 and l.ledgerGroup.accountType!='Direct Expense' and l.ledgerGroup.accountType!='Indirect Expense') ORDER BY l.ledgerName ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllCustomerReceiptTo:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllStartsWithCustomerReceiptTo(String customer) {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and (l.ledgerGroup.id !=1 and l.ledgerGroup.id !=6 and l.ledgerGroup.accountType!='Direct Expense' and l.ledgerGroup.accountType!='Indirect Expense') and l.ledgerName LIKE :key ORDER BY l.ledgerName ASC");
            query.setString("key", customer + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllStartsWithCustomerReceiptTo:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
//    public List<Ledger> findAllCustomerReceiptBy() {
//        Session session = null;
//        List<Ledger> list = null;
//        try {
//            session = HibernateUtil.getSessionFactory().openSession();
//           // org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp ORDER BY l.ledgerName ASC");
//             org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp ORDER BY l.ledgerName ASC");
//            query.setParameter("comp", GlobalProperty.getCompany());
//            list = query.list();
//        } catch (Exception e) {
//            log.error("findAllCustomerReceiptBy:", e);
//        } finally {
//            if (session != null && session.isOpen()) {
//                session.close();
//            }
//        }
//        return list;
//    }
    public List<Ledger> findAllCustomerReceiptBy() {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
           // org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp ORDER BY l.ledgerName ASC");
             org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and ( l.ledgerGroup.id=1 or l.ledgerGroup.id=6)  ORDER BY l.ledgerName ASC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllCustomerReceiptBy:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<Ledger> findAllStartsWithCustomerReceiptBy(String customer) {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and l.ledgerName  and LIKE :key and(l.ledgerGroup.id =1 or l.ledgerGroup.id =6) ORDER BY l.ledgerName ASC");
            query.setString("key", customer + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllStartsWithCustomerReceiptBy:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
  public List<Ledger> findAllByLedgerGroup(LedgerGroup ledgerGroup) {
        Session session = null;
        List<Ledger> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Ledger l where l.company = :comp and l.ledgerGroup=:ledgerGroup order by l.ledgerName ASC");
            query.setParameter("ledgerGroup", ledgerGroup);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByLedgerGroup:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
  public List<LedgerGroup> findDistinctLedgerGroupFromLedger() {
        Session session = null;
        List<LedgerGroup> list = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select distinct lc from Ledger p inner join p.ledgerGroup lc where p.company = :comp order by lc.groupName asc");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findDistinctLedgerGroupFromLedger:", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
}
