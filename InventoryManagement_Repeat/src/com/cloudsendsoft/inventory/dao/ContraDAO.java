/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import com.cloudsendsoft.inventory.model.Contra;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Journal;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseReturn;
import com.cloudsendsoft.inventory.model.Receipt;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesReturn;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.sql.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class ContraDAO {
    
    static final Logger log = Logger.getLogger(ContraDAO.class.getName());
    
    public List<Contra> findAllContra() {
        Session session = null;
         List<Contra> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Contra i where i.company = :comp ORDER BY i.contraDate DESC,i.id DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllContra:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
               
  public List<Contra> findAllByInvoiceNo(String invoiceNo) {
        Session session = null;
         List<Contra> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Contra i where i.company = :comp and i.contraNo LIKE :key");
            query.setParameter("key", invoiceNo + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByInvoiceNo:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<Contra> findAllByDate(Date  fromDate,Date toDate) {
        Session session = null;
         List<Contra> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from Contra i where i.contraDate >= :key1 AND i.contraDate<=:key2 and i.company = :comp");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByDate:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
     public double findAllToWithDateAndId(int id,Date  fromDate,Date toDate) {
        Session session = null;
        double total=0;
         List<Receipt> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(amount)from Contra i where i.company = :comp and i.ledgerTo.id=:key3 AND i.contraDate >= :key1 AND i.contraDate<=:key2");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp", GlobalProperty.getCompany());
            query.setParameter("key3", id);
            if(query.uniqueResult()!=null)
            {
                total= (double)query.uniqueResult();
            }
            else
            {
                total=0;
            }
        } catch (Exception e) {
            log.error("findAllByDate:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return total;
    }
      public double findAllByWithDateAndId(int id,Date  fromDate,Date toDate) {
        Session session = null;
        double total=0;
         List<Receipt> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(amount)from Contra i where i.company = :comp and i.ledgerBy.id=:key3 AND i.contraDate >= :key1 AND i.contraDate<=:key2");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp", GlobalProperty.getCompany());
            query.setParameter("key3", id);
            if(query.uniqueResult()!=null)
            {
                total= (double)query.uniqueResult();
            }
            else
            {
                total=0;
            }
        } catch (Exception e) {
            log.error("findAllByDate:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return total;
    }
public List<Contra> findAllByLedgerAndDate(Ledger ledger,Date  fromDate,Date toDate) {
    Session session = null;
     List<Contra> list = null;
    try {           
        session = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Query query = session.createQuery("from Contra i where i.company = :comp AND (i.ledgerBy=:ledger OR i.ledgerTo=:ledger) AND i.contraDate >= :key1 AND i.contraDate<=:key2");
        query.setDate("key1", fromDate);
        query.setDate("key2", toDate);
        query.setParameter("comp", GlobalProperty.getCompany());
        query.setParameter("ledger",ledger);
        list = query.list();
    } catch (Exception e) {
        log.error("findAllByDate:",e);
    } finally {
        if (session != null && session.isOpen()) {
            session.close();
        }
    }
    return list;
}      
}
