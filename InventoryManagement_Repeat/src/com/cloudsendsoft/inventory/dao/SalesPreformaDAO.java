/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.sql.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class SalesPreformaDAO {
    
    static final Logger log = Logger.getLogger(SalesPreformaDAO.class.getName());
    
    public List<SalesPreforma> findAll() {
        Session session = null;
         List<SalesPreforma> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesPreforma where company = :comp order by id");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAll:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<SalesPreforma> findAllPendingCommercialInvoice() {
        Session session = null;
         List<SalesPreforma> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesPreforma sp where sp.company = :comp and sp.expiryDate>=DATE(NOW()) order by sp.billDate");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllPendingCommercialInvoice:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<SalesPreforma> findAllSalesPreforma() {
        Session session = null;
         List<SalesPreforma> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesPreforma i where i.company = :comp ORDER BY i.billDate DESC,i.id DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllSalesPreforma:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<SalesPreforma> findAllSalesPreformaCommercialInvoice() {
        Session session = null;
         List<SalesPreforma> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesPreforma i WHERE i.company = :comp and commercialInvoice_id !=NULL ORDER BY i.billDate DESC,i.id DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllSalesPreforma:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
     public List<SalesPreforma> findAllSalesPreformaCommercialInvoice(Date fromDate,Date toDate) {
        Session session = null;
         List<SalesPreforma> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesPreforma i WHERE i.company = :comp and commercialInvoice_id !=NULL and i.billDate>=:key1 AND i.billDate<=:key2 ORDER BY i.billDate DESC,i.id DESC");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllSalesPreforma:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<SalesPreforma> findAllSalesPreformaCommercialInvoiceTill(java.util.Date date) {
        Session session = null;
         List<SalesPreforma> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesPreforma i WHERE i.company = :comp and i.billDate<:date and commercialInvoice_id !=NULL ORDER BY i.billDate DESC");
            query.setParameter("comp", GlobalProperty.getCompany());
            query.setDate("date", date);
            list = query.list();
        } catch (Exception e) {
            log.error("findAllSalesPreforma:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public List<SalesPreforma> findAllByInvoiceNo(String invoiceNo) {
        Session session = null;
         List<SalesPreforma> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesPreforma i where i.company = :comp and i.invoiceNumber LIKE :key");
            query.setParameter("key", invoiceNo + "%");
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByInvoiceNo:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<SalesPreforma> findAllByDate(Date  fromDate,Date toDate) {
        Session session = null;
         List<SalesPreforma> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from SalesPreforma i where i.company = :comp and i.billDate >= :key1 AND i.billDate<=:key2");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp", GlobalProperty.getCompany());
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByDate:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    public double findTotalCommercialInvoice(Date fromDate , Date toDate)
    {
        Session session = null;
       double totalAmount=0.0;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(grandTotal)from SalesPreforma i where i.company = :comp and i.billDate >= :key1 AND i.billDate<=:key2 AND commercialInvoice_id!=NULL");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp", GlobalProperty.getCompany());
            if(query.uniqueResult()!=null)
            {
                totalAmount = (double) query.uniqueResult();
            }
           
        } catch (Exception e) {
            log.error("findTotalCommercialInvoice:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return totalAmount;
    }
    public double findTotalDiscount(Date fromDate , Date toDate)
    {
        Session session = null;
       double totalAmount=0.0;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(discount)from CommercialInvoice i where i.company = :comp and i.billDate >= :key1 AND i.billDate<=:key2");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp", GlobalProperty.getCompany());
            if(query.uniqueResult()!=null)
            {
                totalAmount = (double) query.uniqueResult();
            }
           
        } catch (Exception e) {
            log.error("findTotalCommercialInvoice:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return totalAmount;
    }
}
