/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.dao;

import static com.cloudsendsoft.inventory.dao.SalesPreformaDAO.log;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.sql.Date;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author LOGON SOFT 1
 */
public class CommercialInvoiceDAO {
     static final Logger log = Logger.getLogger(CommercialInvoiceDAO.class.getName());
     public double findTotalDiscount(Date fromDate , Date toDate)
    {
        Session session = null;
       double totalAmount=0.0;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(discount)from CommercialInvoice i where i.company = :comp and i.billDate >= :key1 AND i.billDate<=:key2");
            query.setDate("key1", fromDate);
            query.setDate("key2", toDate);
            query.setParameter("comp", GlobalProperty.getCompany());
            if(query.uniqueResult()!=null)
            {
                totalAmount = (double) query.uniqueResult();
            }
           
        } catch (Exception e) {
            log.error("findTotalCommercialInvoice:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return totalAmount;
    }
}
