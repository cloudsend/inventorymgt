/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.dao;

import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.sql.Date;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;

/**
 *
 * @author LOGON SOFT 1
 */
public class ClosingStockDAO {
    static final Logger log = Logger.getLogger(ClosingStockDAO.class.getName());
    
    public List getClosingStock(StockGroup stockGroup)
    {
        Session session =null;
        session = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Query query = session.createSQLQuery("CALL closingStockSan(:key)");
        query.setParameter("key", stockGroup.getId());
       
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List result=query.list();
        
        return result;
    }
    public List getStockList(StockGroup stockGroup)
    {
        Session session =null;
        session = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Query query = session.createSQLQuery("CALL individualStockList(:key)");
        query.setParameter("key", stockGroup.getId());
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List result=query.list();
        return result;
    }
    public List getClosingStockBasedOnDateRange(StockGroup stockGroup, Date fromDate, Date toDate)
    {
        Session session =null;
        session = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Query query = session.createSQLQuery("CALL closingStockBasedOnDateRange(:key,:fromDate,:toDate,:comp)");
        query.setParameter("key", stockGroup.getId());
        query.setDate("fromDate", fromDate);
        query.setDate("toDate", toDate);
        query.setParameter("comp", GlobalProperty.getCompany().getId());
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List result=query.list();
        
        return result;
    }
    public List getStockListBasedOnDateRange(StockGroup stockGroup, Date fromDate, Date toDate)
    {
        Session session =null;
        session = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Query query = session.createSQLQuery("CALL individualStockListBasedOnDate(:key,:fromDate,:toDate,:comp)");
        query.setParameter("key", stockGroup.getId());
        query.setDate("fromDate", fromDate);
        query.setDate("toDate", toDate);
        query.setParameter("comp", GlobalProperty.getCompany().getId());
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List result=query.list();
        return result;
    }
     public List getClosingStockBasedOnEndDate(StockGroup stockGroup, Date toDate)
    {
        Session session =null;
        session = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Query query = session.createSQLQuery("CALL closingStockUsingOnlyEndDate(:key,:toDate,:comp)");
        query.setParameter("key", stockGroup.getId());
        query.setDate("toDate", toDate);
        query.setParameter("comp", GlobalProperty.getCompany().getId());
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List result=query.list();
        
        return result;
    }
     public List getStockListBasedOnEndDate(StockGroup stockGroup, Date toDate)
    {
        Session session =null;
        session = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Query query = session.createSQLQuery("CALL individualStockListUsingOnlyEndDate(:key,:toDate,:comp)");
        query.setParameter("key", stockGroup.getId());
        query.setDate("toDate", toDate);
        query.setParameter("comp", GlobalProperty.getCompany().getId());
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List result=query.list();
        return result;
    }
}
