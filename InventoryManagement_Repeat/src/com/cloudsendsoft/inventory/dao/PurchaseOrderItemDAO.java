/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.dao;

import com.cloudsendsoft.inventory.model.Item;
//import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseOrderItem;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.MainForm;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class PurchaseOrderItemDAO {
    
    static final Logger log = Logger.getLogger(PurchaseOrderItemDAO.class.getName());
    
    public List<PurchaseOrderItem> findAllByItem(Item item) {
        Session session = null;
         List<PurchaseOrderItem> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseOrderItem i where i.item = :item");
            query.setParameter("item", item);
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByItem:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<PurchaseOrderItem> findAllByItemCodeDESC(String itemCode) {
        Session session = null;
         List<PurchaseOrderItem> list = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("from PurchaseOrderItem i where i.item.itemCode = :itemCode");
            query.setParameter("itemCode", itemCode);
            list = query.list();
        } catch (Exception e) {
            log.error("findAllByItemCode:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
     public Double totalAmountByItem(Item item) {
        Session session = null;
         Double amt = 0.00;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum((quantity*unitPrice)-discount) from PurchaseOrderItem i where i.item = :item");
            query.setParameter("item", item);
            amt = (Double) query.uniqueResult();
        } catch (Exception e) {
            log.error("TotalAmountByItem:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return (amt==null)?0.00:amt;
    }
    
     /*public Long totalQuantityByItem(Item item,String month) {
        Session session = null;
         Long amt = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select p from Purchase p join p.purchaseItems pi"
                    + " where monthname(p.billDate) = :month and pi.item = :item");
            query.setParameter("month", month);
            query.setParameter("item", item);
          //  amt = (Long) query.uniqueResult();
            
                    System.out.println("amt:"+query.list());
        } catch (Exception e) {
            System.out.println("Exception item:"+item.getDescription());
            log.error("totalQuantityByItem:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return amt;
    }*/
  
    /* public Double totalAmountByItemByMonth(String month) {
        Session session = null;
         Double amt = null;
        try {           
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(quantity*unitPrice) from PurchaseItem i where i.item = :item");
            query.setParameter("item", item);
            amt = (Double) query.uniqueResult();
        } catch (Exception e) {
            log.error("TotalAmountByItem:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return amt;
    }*/
    
     public Double totalQuantityByItem(Item item) {
        Session session = null;
         Double amt = 0.0;
        try {    
            session = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = session.createQuery("select sum(i.quantity) from PurchaseOrderItem i where i.item = :item");
            query.setParameter("item", item);
            amt = (Double) query.uniqueResult();
        } catch (Exception e) {
            log.error("totalQuantityByItem:",e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return (amt==null)?0:amt;
    }
}
