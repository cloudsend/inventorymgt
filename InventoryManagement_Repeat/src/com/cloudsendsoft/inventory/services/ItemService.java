package com.cloudsendsoft.inventory.services;

import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.StockGroupDAO;
import com.cloudsendsoft.inventory.model.Godown;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseOrderItem;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.PopupTableDialog;
import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author user
 */
public class ItemService {

    CommonService commonService = new CommonService();

    public void fillAddItemTable(javax.swing.JTable itemTable,List<Item> items,boolean isDuplicateColumnsVisible) {
        DefaultTableModel model = (DefaultTableModel) itemTable.getModel();
        model.setRowCount(0);
        
        int i = 0;
        for (Item itm : items) {
            if(isDuplicateColumnsVisible){
                ((DefaultTableModel) itemTable.getModel()).insertRow(i, new Object[]{itm.getItemCode(), itm.getDescription(), itm.getUnit().getName(), itm.getSellingPrice(), itm.getAvailableQty(),itm.getAvailableQty1(),itm.isVisible()});
            }else{
                ((DefaultTableModel) itemTable.getModel()).insertRow(i, new Object[]{itm.getItemCode(), itm.getDescription(), itm.getUnit().getName(), itm.getSellingPrice(), itm.getAvailableQty(), itm.isVisible()});
            }
            i++;
        }
        //hide color column
        if (isDuplicateColumnsVisible) {
            TableColumnModel m = itemTable.getColumnModel();
            if (m.getColumnCount() > 7) {
                TableColumn col7 = m.getColumn(7);
                TableColumn col8 = m.getColumn(8);
                m.removeColumn(col7);
                m.removeColumn(col8);
            }
        } else {
            TableColumnModel m = itemTable.getColumnModel();
            if (m.getColumnCount() > 6) {
                TableColumn col6 = m.getColumn(6);
                TableColumn col7 = m.getColumn(7);
                m.removeColumn(col6);
                m.removeColumn(col7);
            }
        }
        
        if(isDuplicateColumnsVisible){
            itemTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
            //alternate row color jtable
            @Override
            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int column) {
                Component c = super.getTableCellRendererComponent(table,
                        value, isSelected, hasFocus, row, column);
                if (table.getModel().getRowCount() > 0) {
                    if (isSelected) {
                        c.setBackground(Color.DARK_GRAY);
                    } else {
                       if((Boolean) table.getModel().getValueAt(row, 6)){
                           // c.setBackground(new Color(Integer.parseInt((String) table.getModel().getValueAt(row, 7))));
                            c.setForeground(Color.BLACK);
                       }else{
                           c.setBackground(Color.LIGHT_GRAY);
                          // c.setForeground(new Color(Integer.parseInt((String) table.getModel().getValueAt(row, 7))));
                       }
                        
                    }
                }
                return c;
            }
        ;
    }

    );     
   
        }else{
            itemTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
            //alternate row color jtable
            @Override
            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int column) {
                Component c = super.getTableCellRendererComponent(table,
                        value, isSelected, hasFocus, row, column);
                if (table.getModel().getRowCount() > 0) {
                    if (isSelected) {
                        c.setBackground(Color.DARK_GRAY);
                    } else {
                       if((Boolean) table.getModel().getValueAt(row, 5)){
                            //c.setBackground(new Color(Integer.parseInt((String) table.getModel().getValueAt(row, 6))));
                            c.setBackground(Color.WHITE); 
                           c.setForeground(Color.BLACK);
                       }else{
                           c.setBackground(Color.LIGHT_GRAY);
                           //c.setForeground(new Color(Integer.parseInt((String) table.getModel().getValueAt(row, 6))));
                       }
                        
                    }
                }
                return c;
            }
        ;
    }

    );
        }
        
        //itemTable.setSelectionBackground(Color.black);
    }
    
    public List<Item> populatePurchasePopupTableByProductId(String productId, PopupTableDialog popupTableDialog, List<PurchaseItem> purchaseItemList, MultiCurrency currecny, boolean isDuplicateColumnsVisible,List<PurchaseItem> purchaseItemListHistory) throws Exception {
        ItemDAO itemDAO = new ItemDAO();
        List<Item> items = null;
        if (productId.length() <= 0) {
            items = itemDAO.findAllOrderByDescription(true);
        } else {
            items = itemDAO.findAllStartsWithItemCode(productId, true);
        }
        ArrayList<Object[]> tableData = new ArrayList<>();
        int slNo = 1;
        for (Item itm : items) {
            double avalQty = itm.getAvailableQty();
            double avalQty1 = itm.getAvailableQty1();
            for (PurchaseItem pi : purchaseItemList) {
                if (pi.getItem().getItemCode().equalsIgnoreCase(itm.getItemCode()) && pi.getItem().getGodown().getName().equalsIgnoreCase(itm.getGodown().getName())) {
                    
                    if(purchaseItemListHistory !=null)
                    {
                        
                        for(PurchaseItem item:purchaseItemListHistory)
                        {
                            if(item.getItem().getId()==pi.getItem().getId())
                            {
                                if(item.getQuantity()==pi.getQuantity())
                                {
                               avalQty = itm.getAvailableQty(); 
                               avalQty1 = itm.getAvailableQty1();
                               break;
                                }
                                else
                                {
                                 avalQty -= (pi.getQuantity()-item.getQuantity());
                                 avalQty1 -= (pi.getQuantity1()-item.getQuantity());
                                 break;   
                                }
                            }
                        }
                    }
                    else
                    {
                    avalQty += pi.getQuantity();
                    avalQty1 += pi.getQuantity1();
                    break;
                     }
                }
            }
            if (isDuplicateColumnsVisible) {
                tableData.add(new Object[]{slNo++, " " + (itm.getItemCode()), " " + (itm.getDescription()), itm.getWeight(), itm.getUnit().getName(), itm.getSellingPrice() / currecny.getRate(), avalQty, avalQty1, itm.getGodown().getName(), itm.getGodown().getColor()});
            } else {
                tableData.add(new Object[]{slNo++, " " + (itm.getItemCode()), " " + (itm.getDescription()), itm.getWeight(), itm.getUnit().getName(), itm.getSellingPrice(), avalQty, itm.getGodown().getName(), itm.getGodown().getColor()});
            }

        }
        if (isDuplicateColumnsVisible) {
            popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Weight", "Unit", "Rate", "Available Quantity", "Available Quantity1", "Godown", ""});
        } else {
            popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Weight", "Unit", "Rate", "Available Quantity", "Godown", ""});
        }

        popupTableDialog.setTableData(tableData);
        //setup godown color
        popupTableDialog.setTableWithGodownColor();
        return items;
    }

    public List<StockGroup> populatePurchasePopupTableByCategory(String categoryName, PopupTableDialog popupTableDialog, List<PurchaseItem> purchaseItemList, MultiCurrency currecny, boolean isDuplicateColumnsVisible,List<PurchaseItem> purchaseItemListHistory) throws Exception {
        
        StockGroupDAO stockGroupDAO = new StockGroupDAO();
        List<StockGroup> stockGroups = null;
        if (categoryName.length() <= 0) {
            stockGroups = stockGroupDAO.findAllOrderByCategory(true);
        } else {
            stockGroups = stockGroupDAO.findAllStartsWithStockGroup(categoryName, true);
        }
        ArrayList<Object[]> tableData = new ArrayList<>();
        int slNo = 1;
        for (StockGroup stockGroup : stockGroups) {
            
//            for (PurchaseItem pi : purchaseItemList) {
//                if (pi.getItem().getItemCode().equalsIgnoreCase(itm.getItemCode()) && pi.getItem().getGodown().getName().equalsIgnoreCase(itm.getGodown().getName())) {
//                    
//                    if(purchaseItemListHistory !=null)
//                    {
//                        
//                        for(PurchaseItem item:purchaseItemListHistory)
//                        {
//                            if(item.getItem().getId()==pi.getItem().getId())
//                            {
//                                if(item.getQuantity()==pi.getQuantity())
//                                {
//                               avalQty = itm.getAvailableQty(); 
//                               avalQty1 = itm.getAvailableQty1();
//                               break;
//                                }
//                                else
//                                {
//                                 avalQty -= (pi.getQuantity()-item.getQuantity());
//                                 avalQty1 -= (pi.getQuantity1()-item.getQuantity());
//                                 break;   
//                                }
//                            }
//                        }
//                    }
//                    else
//                    {
//                    avalQty += pi.getQuantity();
//                    avalQty1 += pi.getQuantity1();
//                    break;
//                     }
//                }
//            }
           // if (isDuplicateColumnsVisible) {
                if(stockGroup.getUnderGroup()!=null)
                {
                    tableData.add(new Object[]{slNo++, " " + (stockGroup.getName()), " " + (stockGroup.getUnderGroup().getName())});
                }
                else
                {
                    tableData.add(new Object[]{slNo++, " " + (stockGroup.getName()), " General"});
                }
           // } else {
              //  tableData.add(new Object[]{slNo++, " " + (itm.getItemCode()), " " + (itm.getDescription()), itm.getWeight(), itm.getUnit().getName(), itm.getSellingPrice() / currecny.getRate(), avalQty, itm.getGodown().getName(), itm.getGodown().getColor()});
          //  }

        }
       // if (isDuplicateColumnsVisible) {
       //     popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Weight", "Unit", "Rate", "Available Quantity", "Available Quantity1", "Godown", ""});
        //} else {
            popupTableDialog.setTitle(new String[]{"Sl No.", "Category Name", "Under"});
       // }

        popupTableDialog.setTableData(tableData);
        //setup godown color
        //popupTableDialog.setTableWithGodownColor();
        return stockGroups;
    }
    public List<Item> populatePurchasePopupTableByItemName(String itemName, PopupTableDialog popupTableDialog, List<PurchaseItem> purchaseItemList, MultiCurrency currency, boolean isDuplicateColumnsVisible,List<PurchaseItem> purchaseItemListHistory) throws Exception {
        ItemDAO itemDAO = new ItemDAO();
        List<Item> items = null;
        if (itemName.length() <= 0) {
            items = itemDAO.findAllOrderByDescription(true);
        } else {
            items = itemDAO.findAllStartsWithDescription(itemName, true);
        }
        ArrayList<Object[]> tableData = new ArrayList<>();
        int slNo = 1;
        for (Item itm : items) {
            double avalQty = itm.getAvailableQty();
            double avalQty1 = itm.getAvailableQty1();
            for (PurchaseItem pi : purchaseItemList) {
                if (pi.getItem().getItemCode().equalsIgnoreCase(itm.getItemCode()) && pi.getItem().getGodown().getName().equalsIgnoreCase(itm.getGodown().getName())) {
                    
                    if(purchaseItemListHistory !=null)
                    {
                        
                        for(PurchaseItem item:purchaseItemListHistory)
                        {
                            if(item.getItem().getId()==pi.getItem().getId())
                            {
                                if(item.getQuantity()==pi.getQuantity())
                                {
                               avalQty = itm.getAvailableQty(); 
                               avalQty1 = itm.getAvailableQty1();
                               break;
                                }
                                else
                                {
                                 avalQty -= (pi.getQuantity()-item.getQuantity());
                                 avalQty1 -= (pi.getQuantity1()-item.getQuantity());
                                 break;   
                                }
                            }
                        }
                    }
                    else
                    {
                    avalQty += pi.getQuantity();
                    avalQty1 += pi.getQuantity1();
                    break;
                    }
                }
            }
            if (isDuplicateColumnsVisible) {
                tableData.add(new Object[]{slNo++, " " + (itm.getItemCode()), " " + (itm.getDescription()), itm.getWeight(), itm.getUnit().getName(), itm.getSellingPrice() / currency.getRate(), avalQty, avalQty1, itm.getGodown().getName(), itm.getGodown().getColor()});
            } else {
                tableData.add(new Object[]{slNo++, " " + (itm.getItemCode()), " " + (itm.getDescription()), itm.getWeight(), itm.getUnit().getName(), itm.getSellingPrice(), avalQty, itm.getGodown().getName(), itm.getGodown().getColor()});
            }

        }
        if (isDuplicateColumnsVisible) {
            popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Weight", "Unit", "Rate", "Available Quantity", "Available Quantity1", "Godown", ""});
        } else {
            popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Weight", "Unit", "Rate", "Available Quantity", "Godown", ""});
        }

        popupTableDialog.setTableData(tableData);

        //setup godown color
        popupTableDialog.setTableWithGodownColor();
        return items;
    }

    public List<Item> populatePurchaseOrderPopupTableByProductId(String productId, PopupTableDialog popupTableDialog, List<PurchaseOrderItem> purchaseOrderItemList, MultiCurrency currency,List<PurchaseOrderItem> purchaseOrderItemListHistory) throws Exception {
        ItemDAO itemDAO = new ItemDAO();
        List<Item> items = null;
        if (productId.length() <= 0) {
            items = itemDAO.findAllOrderByDescription(true);
        } else {
            items = itemDAO.findAllStartsWithItemCode(productId, true);
        }
        ArrayList<Object[]> tableData = new ArrayList<>();
        int slNo = 1;
        for (Item itm : items) {
            double avalQty = itm.getAvailableQty();
            for (PurchaseOrderItem pi : purchaseOrderItemList) {
                if (pi.getItem().getItemCode().equalsIgnoreCase(itm.getItemCode()) && pi.getItem().getGodown().getName().equalsIgnoreCase(itm.getGodown().getName())) {
                    
                    if(purchaseOrderItemListHistory !=null)
                    {
                        
                        for(PurchaseOrderItem item:purchaseOrderItemListHistory)
                        {
                            if(item.getItem().getId()==pi.getItem().getId())
                            {
                                if(item.getQuantity()==pi.getQuantity())
                                {
                               avalQty = itm.getAvailableQty(); 
                               //avalQty1 = itm.getAvailableQty1();
                               break;
                                }
                                else
                                {
                                 avalQty -= (pi.getQuantity()-item.getQuantity());
                                 //avalQty1 -= (pi.getQuantity1()-item.getQuantity());
                                 break;   
                                }
                            }
                        }
                    }
                    else
                    {
                    avalQty += pi.getQuantity();
                    break;
                     }
                }
            }
            tableData.add(new Object[]{slNo++, " " + (itm.getItemCode()), " " + (itm.getDescription()), itm.getWeight(), itm.getUnit().getName(), itm.getSellingPrice() / currency.getRate(), avalQty, itm.getGodown().getName(), itm.getGodown().getColor()});
        }

        popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Weight", "Unit", "Rate", "Available Quantity", "Godown", ""});
        popupTableDialog.setTableData(tableData);
        //setup godown color
        popupTableDialog.setTableWithGodownColor();
        return items;
    }

    public List<Item> populatePurchaseOrderPopupTableByItemName(String itemName, PopupTableDialog popupTableDialog, List<PurchaseOrderItem> purchaseOrderItemList, MultiCurrency currency,List<PurchaseOrderItem> purchaseOrderItemListHistory) throws Exception {
        ItemDAO itemDAO = new ItemDAO();
        List<Item> items = null;
        if (itemName.length() <= 0) {
            items = itemDAO.findAllOrderByDescription(true);
        } else {
            items = itemDAO.findAllStartsWithDescription(itemName, true);
        }
        ArrayList<Object[]> tableData = new ArrayList<>();
        int slNo = 1;
        for (Item itm : items) {
            double avalQty = itm.getAvailableQty();
            for (PurchaseOrderItem pi : purchaseOrderItemList) {
                if (pi.getItem().getItemCode().equalsIgnoreCase(itm.getItemCode()) && pi.getItem().getGodown().getName().equalsIgnoreCase(itm.getGodown().getName())) {
                    
                     if(purchaseOrderItemListHistory !=null)
                    {
                        
                        for(PurchaseOrderItem item:purchaseOrderItemListHistory)
                        {
                            if(item.getItem().getId()==pi.getItem().getId())
                            {
                                if(item.getQuantity()==pi.getQuantity())
                                {
                               avalQty = itm.getAvailableQty(); 
                               //avalQty1 = itm.getAvailableQty1();
                               break;
                                }
                                else
                                {
                                 avalQty -= (pi.getQuantity()-item.getQuantity());
                                 //avalQty1 -= (pi.getQuantity1()-item.getQuantity());
                                 break;   
                                }
                            }
                        }
                    }
                    else
                    {
                    avalQty += pi.getQuantity();
                    break;
                    }
                }
            }
            tableData.add(new Object[]{slNo++, " " + (itm.getItemCode()), " " + (itm.getDescription()), itm.getWeight(), itm.getUnit().getName(), itm.getSellingPrice() / currency.getRate(), avalQty, itm.getGodown().getName(), itm.getGodown().getColor()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Weight", "Unit", "Rate", "Available Quantity", "Godown", ""});
        popupTableDialog.setTableData(tableData);

        //setup godown color
        popupTableDialog.setTableWithGodownColor();
        return items;
    }

    public List<Item> populateSalesPopupTableByProductId(String productId, PopupTableDialog popupTableDialog, List<SalesItem> salesItemList, MultiCurrency currency, boolean isDuplicateColumnsVisible,List<SalesItem> salesItemListHistory) throws Exception {
        ItemDAO itemDAO = new ItemDAO();
        List<Item> items = null;
        if (productId.length() <= 0) {
            items = itemDAO.findAllOrderByDescription(true);
        } else {
            items = itemDAO.findAllStartsWithItemCode(productId, true);
        }
        ArrayList<Object[]> tableData = new ArrayList<>();
        int slNo = 1;
        for (Item itm : items) {
            double avalQty = itm.getAvailableQty();
            double avalQty1 = itm.getAvailableQty1();
            for (SalesItem pi : salesItemList) {
                if (pi.getItem().getItemCode().equalsIgnoreCase(itm.getItemCode())  &&(pi.getItem().getDescription().equalsIgnoreCase(itm.getDescription()))) {
                    if(salesItemListHistory !=null)
                    {
                        
                        for(SalesItem item:salesItemListHistory)
                        {
                             
                            if(item.getItem().getId()==pi.getItem().getId())
                            {
                                if(item.getQuantity()==pi.getQuantity())
                                {
                               avalQty = itm.getAvailableQty(); 
                               avalQty1 = itm.getAvailableQty1();
                               break;
                                }
                                else
                                {
                                 avalQty -= (pi.getQuantity()-item.getQuantity());
                                 avalQty1 -= (pi.getQuantity1()-item.getQuantity());
                                 break;   
                                }
                            }
                        }
                    }
                    else
                    {
                    avalQty -= pi.getQuantity();
                    avalQty1 -= pi.getQuantity1();
                    break;
                    }
                }
            }
            if (isDuplicateColumnsVisible) {
                tableData.add(new Object[]{slNo++, " " + (itm.getItemCode()), " " + (itm.getDescription()), itm.getStockGroup().getName(), itm.getUnit().getName(), (itm.getSellingPrice() / currency.getRate()), avalQty,avalQty1});
            }else{
                tableData.add(new Object[]{slNo++, " " + (itm.getItemCode()), " " + (itm.getDescription()), itm.getStockGroup().getName(), itm.getUnit().getName(), (itm.getSellingPrice() / currency.getRate()), avalQty});
            }
            
        }
        if (isDuplicateColumnsVisible) {
            popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Stock Group", "Unit", "Rate", "Available Quantity", "Available Quantity1"});
        }else{
            popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Stock Group", "Unit", "Rate", "Available Quantity"});
        }
        
        popupTableDialog.setTableData(tableData);

        //setup godown color
        //popupTableDialog.setTableWithGodownColor();
        return items;
    }
    public List<Item> populateItemTableByProductId(String productId, PopupTableDialog popupTableDialog, MultiCurrency currency) throws Exception {
        ItemDAO itemDAO = new ItemDAO();
        List<Item> items = null;
        if (productId.length() <= 0) {
            items = itemDAO.findAllOrderByDescription(true);
        } else {
            items = itemDAO.findAllStartsWithItemCode(productId, true);
        }
        ArrayList<Object[]> tableData = new ArrayList<>();
        int slNo = 1;
        for (Item itm : items) {
            double avalQty = itm.getAvailableQty();           
           tableData.add(new Object[]{slNo++, " " + (itm.getItemCode()), " " + (itm.getDescription()), itm.getWeight(), itm.getUnit().getName(), itm.getSellingPrice(), avalQty, itm.getGodown().getName(), itm.getGodown().getColor()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Weight", "Unit", "Rate", "Available Quantity", "Godown", ""});
        popupTableDialog.setTableData(tableData);

        //setup godown color
        popupTableDialog.setTableWithGodownColor();
        return items;
    }

    public List<Item> populateSalesPopupTableByItemName(String itemName, PopupTableDialog popupTableDialog, List<SalesItem> salesItemList, MultiCurrency currency, boolean isDuplicateColumnsVisible,List<SalesItem> salesItemListHistory) throws Exception {
        ItemDAO itemDAO = new ItemDAO();
        List<Item> items = null;
        
         
        if (itemName.length() <= 0) {
            items = itemDAO.findAllOrderByDescription(true);
        } else {
            items = itemDAO.findAllStartsWithDescription(itemName, true);
        }
        ArrayList<Object[]> tableData = new ArrayList<>();
        int slNo = 1;
        for (Item itm : items) {
            double avalQty = itm.getAvailableQty();
            double avalQty1 = itm.getAvailableQty1();
            
            for (SalesItem pi : salesItemList) {
                if (pi.getItem().getItemCode().equalsIgnoreCase(itm.getItemCode()) && pi.getItem().getGodown().getName().equalsIgnoreCase(itm.getGodown().getName())&&(pi.getItem().getDescription().equalsIgnoreCase(itm.getDescription()))) {
                  
                    if(salesItemListHistory !=null)
                    {
                        
                        for(SalesItem item:salesItemListHistory)
                        {
                            if(item.getItem().getId()==pi.getItem().getId())
                            {
                                if(item.getQuantity()==pi.getQuantity())
                                {
                               avalQty = itm.getAvailableQty(); 
                               avalQty1 = itm.getAvailableQty1();
                               break;
                                }
                                else
                                {
                                 avalQty -= (pi.getQuantity()-item.getQuantity());
                                 avalQty1 -= (pi.getQuantity1()-item.getQuantity());
                                 break;   
                                }
                            }
                        }
                    }
                    else
                    {
                   avalQty -= pi.getQuantity();
                   avalQty1 -= pi.getQuantity1();
                    break;
                     }
                }
            }
            if (isDuplicateColumnsVisible) {
                tableData.add(new Object[]{slNo++, " " + (itm.getItemCode()), " " + (itm.getDescription()), itm.getWeight(), itm.getUnit().getName(), (itm.getSellingPrice() / currency.getRate()), avalQty, avalQty1, itm.getGodown().getName(), itm.getGodown().getColor()});
            }else{
                tableData.add(new Object[]{slNo++, " " + (itm.getItemCode()), " " + (itm.getDescription()), itm.getWeight(), itm.getUnit().getName(), (itm.getSellingPrice() / currency.getRate()), avalQty, itm.getGodown().getName(), itm.getGodown().getColor()});
            }
        }
        if (isDuplicateColumnsVisible) {
            popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Weight", "Unit", "Rate", "Available Quantity", "Available Quantity1","Godown", ""});
        }else{
            popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Weight", "Unit", "Rate", "Available Quantity", "Godown", ""});
        }
        
        popupTableDialog.setTableData(tableData);

        //setup godown color
        popupTableDialog.setTableWithGodownColor();
        return items;
    }
    public List<Item> populateItemPopupTableByItemName(String itemName, PopupTableDialog popupTableDialog) throws Exception {
        ItemDAO itemDAO = new ItemDAO();
        List<Item> items = null;
        if (itemName.length() <= 0) {
            items = itemDAO.findAllOrderByDescription(true);
        } else {
            items = itemDAO.findAllStartsWithDescription(itemName, true);
        }
        ArrayList<Object[]> tableData = new ArrayList<>();
        int slNo = 1;
        for (Item itm : items) {
            double avalQty = itm.getAvailableQty();           
            tableData.add(new Object[]{slNo++, " " + (itm.getItemCode()), " " + (itm.getDescription()), itm.getWeight(), itm.getUnit().getName(), itm.getSellingPrice(), avalQty, itm.getGodown().getName(), itm.getGodown().getColor()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Weight", "Unit", "Rate", "Available Quantity", "Godown", ""});
        popupTableDialog.setTableData(tableData);

        //setup godown color
        popupTableDialog.setTableWithGodownColor();
        return items;
    }
    
        public List<Item> populateItemPopupTableByItemCode(String itemCode,String itemName, PopupTableDialog popupTableDialog) throws Exception {
        ItemDAO itemDAO = new ItemDAO();
        List<Item> items = null;
        if (itemName.length() <= 0) {
            items = itemDAO.findAllOrderByDescription(true);
        } else {
           items = itemDAO.findDistinctItemCodeWithDescription(itemCode,itemName, true);
        }
        ArrayList<Object[]> tableData = new ArrayList<>();
        int slNo = 1;
        for (Item itm : items) {
            tableData.add(new Object[]{slNo++, " "+ itm.getItemCode(),itm.getGodown().getColor()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code",""});
        popupTableDialog.setTableData(tableData);

        //setup godown color
        popupTableDialog.setTableWithGodownColor();
        return items;
    }
    

    public List<Item> populateSalesPreformaPopupTableByProductId(String productId, PopupTableDialog popupTableDialog, List<SalesPreformaItem> salesPreformaItemList, MultiCurrency currency,List<SalesPreformaItem> salesPreformaItemListHistory) throws Exception {
        ItemDAO itemDAO = new ItemDAO();
        List<Item> items = null;
        if (productId.length() <= 0) {
            items = itemDAO.findAllOrderByDescription(true);
        } else {
            items = itemDAO.findAllStartsWithItemCode(productId, true);
        }
        ArrayList<Object[]> tableData = new ArrayList<>();
        int slNo = 1;
        for (Item itm : items) {
            double avalQty = itm.getAvailableQty();
            for (SalesPreformaItem pi : salesPreformaItemList) {
                if (pi.getItem().getItemCode().equalsIgnoreCase(itm.getItemCode()) && pi.getItem().getGodown().getName().equalsIgnoreCase(itm.getGodown().getName())) {
                    
                    
                    if(salesPreformaItemListHistory !=null)
                    {
                        
                        for(SalesPreformaItem item:salesPreformaItemListHistory)
                        {
                            if(item.getItem().getId()==pi.getItem().getId())
                            {
                                if(item.getQuantity()==pi.getQuantity())
                                {
                               avalQty = itm.getAvailableQty(); 
                              // avalQty1 = itm.getAvailableQty1();
                               break;
                                }
                                else
                                {
                                 avalQty -= (pi.getQuantity()-item.getQuantity());
                                // avalQty1 -= (pi.getQuantity1()-item.getQuantity());
                                 break;   
                                }
                            }
                        }
                    }
                    else
                    {
                    avalQty -= pi.getQuantity();
                    break;
                    }
                }
            }
            tableData.add(new Object[]{slNo++, " " + (itm.getItemCode()), " " + (itm.getDescription()), itm.getWeight(), itm.getUnit().getName(), itm.getSellingPrice() / currency.getRate(), avalQty, itm.getGodown().getName(), itm.getGodown().getColor()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Weight", "Unit", "Rate", "Available Quantity", "Godown", ""});
        popupTableDialog.setTableData(tableData);

        //setup godown color
        popupTableDialog.setTableWithGodownColor();
        return items;
    }

    public List<Item> populateSalesPreformaPopupTableByItemName(String itemName, PopupTableDialog popupTableDialog, List<SalesPreformaItem> salesPreformaItemList, MultiCurrency currency,List<SalesPreformaItem> salesPreformaItemListHistory) throws Exception {
        ItemDAO itemDAO = new ItemDAO();
        List<Item> items = null;
        if (itemName.length() <= 0) {
            items = itemDAO.findAllOrderByDescription(true);
        } else {
            items = itemDAO.findAllStartsWithDescription(itemName, true);
        }
        ArrayList<Object[]> tableData = new ArrayList<>();
        int slNo = 1;
        for (Item itm : items) {
            double avalQty = itm.getAvailableQty();
            for (SalesPreformaItem pi : salesPreformaItemList) {
                if (pi.getItem().getItemCode().equalsIgnoreCase(itm.getItemCode()) && pi.getItem().getGodown().getName().equalsIgnoreCase(itm.getGodown().getName())) {
                    
                    if(salesPreformaItemListHistory !=null)
                    {
                        
                        for(SalesPreformaItem item:salesPreformaItemListHistory)
                        {
                            if(item.getItem().getId()==pi.getItem().getId())
                            {
                                if(item.getQuantity()==pi.getQuantity())
                                {
                               avalQty = itm.getAvailableQty(); 
                              // avalQty1 = itm.getAvailableQty1();
                               break;
                                }
                                else
                                {
                                 avalQty -= (pi.getQuantity()-item.getQuantity());
                                // avalQty1 -= (pi.getQuantity1()-item.getQuantity());
                                 break;   
                                }
                            }
                        }
                    }
                    else
                    {
                    avalQty -= pi.getQuantity();
                    break;
                    }
                }
            }
            tableData.add(new Object[]{slNo++, " " + (itm.getItemCode()), " " + (itm.getDescription()), itm.getWeight(), itm.getUnit().getName(), itm.getSellingPrice() / currency.getRate(), avalQty, itm.getGodown().getName(), itm.getGodown().getColor()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Weight", "Unit", "Rate", "Available Quantity", "Godown", ""});
        popupTableDialog.setTableData(tableData);

        //setup godown color
        popupTableDialog.setTableWithGodownColor();
        return items;
    }
     public List<Godown> populateStockMoveViewByGodownList(String godown,PopupTableDialog popTable)throws Exception
   {  
         List<Godown> godowns=null;
         ItemDAO ItemDAO=new ItemDAO();
         if (godown.length() <= 0)
            godowns = ItemDAO.findAllGodown(true);
         else {
                godowns = ItemDAO.findAllStartsWithGodown(godown, true);
            }
         ArrayList<Object[]> tableData = new ArrayList<>();
         int slNo = 1;
         for (Godown itm : godowns) {
            
             tableData.add(new Object[]{slNo++,itm.getName(),itm.getColor()});
             
         } 
         popTable.setTitle(new String[]{"Sl No.","Godown Name",""});
         popTable.setTableData(tableData);
         popTable.setTableWithGodownColor();
         return godowns; 
   }  
     public boolean barcodeDetector(String barcode)
     {
         System.out.println("Barcode :"+barcode);
         List<Item> items = null;
         ItemDAO itemDAO = new ItemDAO();
         items = itemDAO.findAllByItemCode(barcode.trim());
         if(!items.isEmpty())
                return true;
         else
                return false;
     }
}
