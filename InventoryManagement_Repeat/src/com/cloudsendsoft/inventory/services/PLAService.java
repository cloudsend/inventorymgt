/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.services;

import com.cloudsendsoft.inventory.components.CTableModel;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.ClosingStockDAO;
import com.cloudsendsoft.inventory.dao.CommercialInvoiceDAO;
import com.cloudsendsoft.inventory.dao.ContraDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.JournalDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.LedgerGroupDAO;
import com.cloudsendsoft.inventory.dao.PaymentDAO;
import com.cloudsendsoft.inventory.dao.PurchaseDAO;
import com.cloudsendsoft.inventory.dao.PurchaseItemDAO;
import com.cloudsendsoft.inventory.dao.PurchaseReturnDAO;
import com.cloudsendsoft.inventory.dao.ReceiptDAO;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.dao.SalesReturnDAO;
import com.cloudsendsoft.inventory.dao.StockGroupDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Contra;
import com.cloudsendsoft.inventory.model.Godown;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Journal;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.model.Payment;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseCharge;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseReturn;
import com.cloudsendsoft.inventory.model.PurchaseReturnItem;
import com.cloudsendsoft.inventory.model.PurchaseTax;
import com.cloudsendsoft.inventory.model.Receipt;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesCharge;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesReturn;
import com.cloudsendsoft.inventory.model.SalesReturnItem;
import com.cloudsendsoft.inventory.model.SalesTax;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.view.PLAccountView;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.apache.log4j.Logger;

/**
 *
 * @author LOGON SOFT 1
 */
public class PLAService {

    CommonService commonService = new CommonService();

    LedgerGroupDAO ledgerGroupDAO = new LedgerGroupDAO();
    List<LedgerGroup> listOfLedgerGroup = null;

    LedgerDAO ledgerDAO = new LedgerDAO();
    List<Ledger> listOfLedger = null;

    ChargesDAO chargesDAO = new ChargesDAO();
    List<Charges> listOfCharges = null;

    ItemDAO itemDAO = new ItemDAO();
    List<Item> listOfItem = null;
    StockGroupDAO stockGroupDAO = new StockGroupDAO();
    StockGroup stockGroup = null;
    List<StockGroup> listOfStockGroup = null;

    PurchaseDAO purchaseDAO = new PurchaseDAO();
    PurchaseReturnDAO purchaseReturnDAO = new PurchaseReturnDAO();
    List<PurchaseItem> listofPurchaseItem = new ArrayList<>(0);
    List<SalesItem> listOfSalesItem = new ArrayList<>(0);

    SalesDAO salesDAO = new SalesDAO();
    SalesReturnDAO salesReturnDAO = new SalesReturnDAO();

    //using for Commercial Invoice
    SalesPreformaDAO salesPreformaDAO = new SalesPreformaDAO();

    LedgerGroup ledgerGroup = null;
    double closingStock = 0;
    List<Object[]> closingStockList = new ArrayList<>(0);
    List<Object[]> closingStockListDetailed = new ArrayList<>(0);

    ReceiptDAO receiptDAO = new ReceiptDAO();
    PaymentDAO paymentDAO = new PaymentDAO();
    ContraDAO contraDAO = new ContraDAO();
    JournalDAO journalDAO = new JournalDAO();
    CommercialInvoiceDAO commercialInvoiceDAO = new CommercialInvoiceDAO();

    List<PurchaseReturn> listOfPurchaseReturns1 = purchaseReturnDAO.findAllPurchaseReturn();
    List<Sales> listOfSales1 = salesDAO.findAllSales();
    List<SalesReturn> listOfSalesReturns1 = salesReturnDAO.findAllSalesReturn();
    List<SalesPreforma> listOfCommercialInvoice1 = salesPreformaDAO.findAllSalesPreformaCommercialInvoice();

    ClosingStockDAO closingStockDAO = new ClosingStockDAO();
    List closingQtyValue = null;
    static final Logger log = Logger.getLogger(PLAService.class.getName());

    public PLAService() {
        listOfStockGroup = stockGroupDAO.findAllByNameASC();
        stockGroup = stockGroupDAO.findByName("General");
    }

    public double findClosingStockByDateRange(Date fromDate, Date toDate) {
        double closingValue = 0.00;
        closingQtyValue = closingStockDAO.getClosingStockBasedOnDateRange(stockGroup, fromDate, toDate);
        Iterator<Object> resultIter = closingQtyValue.iterator();
        while (resultIter.hasNext()) {
            Object object = resultIter.next();
            Map row = (Map) object;
            if (row.get("cqnty") != null && row.get("finalStock") != null) {
                closingValue = ((BigDecimal) row.get("finalStock")).doubleValue();
            }
        }

        return closingValue;
    }
    public double findClosingStockByEndDate(Date toDate) {
        double closingValue = 0.00;
        closingQtyValue = closingStockDAO.getClosingStockBasedOnEndDate(stockGroup, toDate);
        Iterator<Object> resultIter = closingQtyValue.iterator();
        while (resultIter.hasNext()) {
            Object object = resultIter.next();
            Map row = (Map) object;
            if (row.get("cqnty") != null && row.get("finalStock") != null) {
                closingValue = ((BigDecimal) row.get("finalStock")).doubleValue();
            }
        }

        return closingValue;
    }

    public double findAvailableBalance(Ledger ledger, Date fromDate, Date toDate) {
        log.info("findAvailableBalance started at: " + Calendar.getInstance().getTime());
        List<Purchase> samplePurchaseList = purchaseDAO.findAllByDate(fromDate, toDate);
        List<Sales> salesList = salesDAO.findAllByDate(fromDate, toDate);
        double netBalance = 0, result = 0;
        result = ledger.getOpeningBalance();
        netBalance += result;
        //Checking in Receipt
        // double result =receiptDAO.findAllByDateAndId(GlobalProperty.getCompany().getStartDate(), GlobalProperty.getCompany().getEndDate(), ledger.getId());
        result = receiptDAO.findAllByDateAndId(fromDate, toDate, ledger.getId());
        log.info("receiptDAO completed at: " + Calendar.getInstance().getTime());
        if (result != 0) {
            if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                netBalance -= result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                netBalance -= result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                netBalance += result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                netBalance += result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                netBalance += result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                netBalance += result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                netBalance -= result;
            }
        }
        //Checking in Payment
        result = paymentDAO.findAllByDateAndId(fromDate, toDate, ledger.getId());
        log.info("paymentDAO completed at: " + Calendar.getInstance().getTime());
        if (result != 0) {
            if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                netBalance += result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                netBalance += result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                netBalance -= result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                netBalance -= result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                netBalance -= result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                netBalance -= result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                netBalance += result;
            }
        }
        //Checking in JournalTo
        result = journalDAO.findAllToByDateAndId(fromDate, toDate, ledger.getId());
        log.info("journalDAO-To completed at: " + Calendar.getInstance().getTime());
        if (result != 0) {
            if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                netBalance -= result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                netBalance -= result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                netBalance += result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                netBalance += result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                netBalance += result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                netBalance += result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                netBalance -= result;
            }
        }
        //Checking in JournalBy
        result = journalDAO.findAllByWithDateAndId(ledger.getId(), fromDate, toDate);
        log.info("journalDAO-By completed at: " + Calendar.getInstance().getTime());
        if (result != 0) {
            if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                netBalance += result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                netBalance += result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                netBalance -= result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                netBalance -= result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                netBalance -= result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                netBalance -= result;
            } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                netBalance += result;
            }
        }
        //Checking in Contra
        result = contraDAO.findAllByWithDateAndId(ledger.getId(), GlobalProperty.getCompany().getStartDate(), GlobalProperty.getCompany().getEndDate());
        log.info("contraDAO completed at: " + Calendar.getInstance().getTime());
        if (result != 0) {
            if (ledger.getLedgerGroup().getGroupName().equalsIgnoreCase("Cash-in-hand") || ledger.getLedgerGroup().getGroupName().equalsIgnoreCase("Bank Accounts")) {
                netBalance += result;
            }
        }
        result = contraDAO.findAllToWithDateAndId(ledger.getId(), GlobalProperty.getCompany().getStartDate(), GlobalProperty.getCompany().getEndDate());
        log.info("contraDAO completed at: " + Calendar.getInstance().getTime());
        if (result != 0) {
            if (ledger.getLedgerGroup().getGroupName().equalsIgnoreCase("Cash-in-hand") || ledger.getLedgerGroup().getGroupName().equalsIgnoreCase("Bank Accounts")) {
                netBalance -= result;
            }
        }
        //Discount Received
        if (ledger.getLedgerName().equalsIgnoreCase("discount received")) {
            //netBalance=ledger.getAvailableBalance();
            netBalance += purchaseDAO.findTotalDiscount(fromDate, toDate);
        }
        //Discount Allowed
        if (ledger.getLedgerName().equalsIgnoreCase("discount allowed")) {
            //netBalance=ledger.getAvailableBalance(); 
            netBalance += salesDAO.findTotalDiscount(fromDate, toDate);
            netBalance += commercialInvoiceDAO.findTotalDiscount(fromDate, toDate);
        }
//        //Purchase Account
//        if(ledger.getLedgerGroup().getGroupName().equalsIgnoreCase("Purchase Accounts"))
//        {
//            if(ledger.getLedgerName().equalsIgnoreCase("Purchase Account"))
//            {
//                result=purchaseDAO.findTotalPurchase(fromDate, toDate);
//                netBalance+=result;
//            }
//            else if(ledger.getLedgerName().equalsIgnoreCase("Purchase Returns"))
//            {
//                result=purchaseReturnDAO.findTotalPurchaseReturn(fromDate, toDate);
//                netBalance=result;
//            }
//        }
//        //Sales Account
//        if(ledger.getLedgerGroup().getGroupName().equalsIgnoreCase("Sales Accounts"))
//        {
//            if(ledger.getLedgerName().equalsIgnoreCase("Sales Account"))
//            {
//                result=salesDAO.findTotalSales(fromDate, toDate);
//                netBalance+=result;
//            }
//            else if(ledger.getLedgerName().equalsIgnoreCase("Sales Returns"))
//            {
//                result=salesReturnDAO.findTotalSalesReturn(fromDate, toDate);
//                netBalance=result;
//            }
//            else if(ledger.getLedgerName().equalsIgnoreCase("Commercial Invoice"))
//            {
//                result=salesPreformaDAO.findTotalCommercialInvoice(fromDate, toDate);
//                netBalance=result;
//            }
//        }
//        //Checking in Purchase
//        result=purchaseDAO.findTotalPurchaseWithId(ledger.getId(), fromDate, toDate);
//        log.info("purchaseDAO completed at: "+Calendar.getInstance().getTime());
//        
//        netBalance-=result;
//        //Checking in Sales
//        result=salesDAO.findTotalSalesWithId(ledger.getId(), fromDate, toDate);
//        log.info("salesDAO completed at: "+Calendar.getInstance().getTime());
//        
//        netBalance+=result;
//        if(netBalance==0&&(ledger.getLedgerGroup().getGroupName().equalsIgnoreCase("Duties & Taxes")))
//        {
//            List<PurchaseTax> purchaseTaxList =new ArrayList<>();
//            for(Purchase purchase : samplePurchaseList)
//            {
//               purchaseTaxList.addAll(purchase.getPurchaseTaxes());
//            }
//            for(PurchaseTax purchaseTax : purchaseTaxList)
//            {
//                if(ledger.getLedgerName().equalsIgnoreCase(purchaseTax.getTax().getName()))
//                {    
//                    netBalance+=purchaseTax.getAmount();
//                }
//            }
//        }
//        if(netBalance==0&&(ledger.getLedgerGroup().getGroupName().equalsIgnoreCase("Duties & Taxes")))
//        {
//            List<SalesTax> salesTaxList =new ArrayList<>();
//            for(Sales sales : salesList)
//            {
//               salesTaxList.addAll(sales.getSalesTaxs());
//            }
//            for(SalesTax salesTax : salesTaxList)
//            {
//                if(ledger.getLedgerName().equalsIgnoreCase(salesTax.getTax().getName()))
//                {    
//                    netBalance-=salesTax.getAmount();
//                }
//            }
//        }
        if (netBalance == 0 && (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense"))) {
            List<PurchaseCharge> purchaseChargesList = new ArrayList<>();
            for (Purchase purchase : samplePurchaseList) {
                purchaseChargesList.addAll(purchase.getPurchaseCharges());
            }
            for (PurchaseCharge purchaseCharge : purchaseChargesList) {
                if (ledger.getLedgerName().equalsIgnoreCase(purchaseCharge.getCharges().getName())) {
                    netBalance += purchaseCharge.getAmount();
                }
            }
        }
        if (/*netBalance==0&&*/(ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense"))) {
            List<SalesCharge> salesChargesList = new ArrayList<>();
            for (Sales sales : salesList) {
                salesChargesList.addAll(sales.getSalesCharges());
            }
            for (SalesCharge salesCharge : salesChargesList) {
                if (ledger.getLedgerName().equalsIgnoreCase(salesCharge.getCharges().getName())) {
                    netBalance += salesCharge.getAmount();
                }
            }
        }
        return netBalance;
    }

    public double findPurchaseTax(Ledger ledger, Date fromDate, Date toDate) {
        List<Purchase> purchaseList = purchaseDAO.findAllByDate(fromDate, toDate);
        double netBalance = 0.00;
        if (ledger.getLedgerGroup().getGroupName().equalsIgnoreCase("Duties & Taxes")) {
            List<PurchaseTax> purchaseTaxList = new ArrayList<>();
            for (Purchase purchase : purchaseList) {
                purchaseTaxList.addAll(purchase.getPurchaseTaxes());
            }
            for (PurchaseTax purchaseTax : purchaseTaxList) {
                if (ledger.getLedgerName().equalsIgnoreCase(purchaseTax.getTax().getName())) {
                    netBalance += purchaseTax.getAmount();
                }
            }
        }
        return netBalance;
    }

    public double findSalesTax(Ledger ledger, Date fromDate, Date toDate) {
        List<Sales> salesList = salesDAO.findAllByDate(fromDate, toDate);
        double netBalance = 0.00;
        if (ledger.getLedgerGroup().getGroupName().equalsIgnoreCase("Duties & Taxes")) {
            List<SalesTax> salesTaxList = new ArrayList<>();
            for (Sales sales : salesList) {
                salesTaxList.addAll(sales.getSalesTaxs());
            }
            for (SalesTax salesTax : salesTaxList) {
                if (ledger.getLedgerName().equalsIgnoreCase(salesTax.getTax().getName())) {
                    netBalance += salesTax.getAmount();
                }
            }
        }
        return netBalance;
    }

    public double testFindAvailableBalanceForBalanceSheet(Ledger ledger, Date fromDate, Date toDate) {
        List<Purchase> purchaseList = purchaseDAO.findAllByDate(fromDate, toDate);
        List<Sales> salesList = salesDAO.findAllByDate(fromDate, toDate);
        List<Receipt> receiptList = receiptDAO.findAllByLedgerAndDate(ledger, fromDate, toDate);
        List<Payment> paymentList = paymentDAO.findAllByLedgerAndDate(ledger, fromDate, toDate);
        List<Journal> journalList = journalDAO.findAllByLedgerAndDate(ledger, fromDate, toDate);
        List<Contra> contraList = contraDAO.findAllByLedgerAndDate(ledger, fromDate, toDate);
        double netBalance = 0, result = 0;
        //opeing Balance
        result = ledger.getOpeningBalance();
        netBalance += result;
        //Checking in Receipt
        for (Receipt receipt : receiptList) {
            System.out.println("Inside loop::::::::" + receipt.getLedgerBy().getName());
            result = receipt.getAmount();
            if (receipt.getLedgerTo().getId() == ledger.getId()) {

                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    //netBalance+=result;
                    netBalance -= result;

                } 
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    netBalance += result;
                } 
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                    netBalance -= result;
                }
            } else if (receipt.getLedgerBy().getId() == ledger.getId()) {
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    netBalance += result;
                } 
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    netBalance -= result;
                }
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                    netBalance += result;
                }
            }

        }
        log.info("receipt completed at: " + Calendar.getInstance().getTime());

        //Checking in Payment
        for (Payment payment : paymentList) {
            result = payment.getAmount();
            if (payment.getLedgerBy().getId() == ledger.getId()) {
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    netBalance += result;
                }
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    netBalance -= result;
                }
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                    netBalance += result;
                }
            } else if (payment.getLedgerTo().getId() == ledger.getId()) {
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    netBalance -= result;
                }
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    netBalance += result;
                }
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                    netBalance -= result;
                }
            }
        }
        log.info("payment completed at: " + Calendar.getInstance().getTime());
        //Checking in JournalTo
        for (Journal journal : journalList) {
            result = journal.getAmount();
            if (journal.getLedgerBy().getId() == ledger.getId()) {
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    netBalance -= result;
                } 
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    netBalance -= result;
                }
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                    netBalance += result;
                }
            } else if (journal.getLedgerTo().getId() == ledger.getId()) {
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    netBalance += result;

                } 
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    netBalance += result;
                }
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                    netBalance -= result;
                }
            }
        }
        log.info("Journal completed at: " + Calendar.getInstance().getTime());

       // Checking in Contra
        for (Contra contra : contraList) {
            result = contra.getAmount();
            if (contra.getLedgerBy().getId() == ledger.getId()) {
                netBalance += result;
            } else if (contra.getLedgerTo().getId() == ledger.getId()) {
                netBalance -= result;
            }
        }

        if (ledger.getLedgerName().equalsIgnoreCase("discount received")) {
            
            netBalance += purchaseDAO.findTotalDiscount(fromDate, toDate);
        }
        if (ledger.getLedgerName().equalsIgnoreCase("discount allowed")) {
             log.info("discount started at: " + Calendar.getInstance().getTime());
            netBalance += salesDAO.findTotalDiscount(fromDate, toDate);
            log.info("discount completed at: " + Calendar.getInstance().getTime());
        }
        

        if ((ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability") || ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset"))&&!ledger.getLedgerGroup().getGroupName().equalsIgnoreCase("Capital")) {
            //Checking in Purchase
            for (Purchase purchase : purchaseList) {
                result = purchase.getGrandTotal();
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    netBalance += result;
                }
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    netBalance -= result;
                }
            }

            //Checking in Purchase returns
            result = purchaseReturnDAO.findTotalPurchaseReturnWithId(ledger.getId(), fromDate, toDate);
            log.info("purchaseReturnDAO completed at: " + Calendar.getInstance().getTime());
            if (result != 0) {
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    netBalance -= result;
                }
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    netBalance += result;
                }
            }
            //Checking in Sales
            for (Sales sales : salesList) {
                result = sales.getGrandTotal();
                if (result != 0) {
                    if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                        netBalance += result;
                    }
                    if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                        netBalance -= result;
                    }
                }
            }

            //Checking in Sales returns
            result = salesReturnDAO.findTotalSalesReturnWithId(ledger.getId(), fromDate, toDate);
            log.info("salesReturnDAO completed at: " + Calendar.getInstance().getTime());
            if (result != 0) {
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    netBalance -= result;
                }
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    netBalance += result;
                }
            }
        }
        
        if (netBalance == 0 && (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense"))) {
            List<PurchaseCharge> purchaseChargesList = new ArrayList<>();
            for (Purchase purchase : purchaseList) {
                purchaseChargesList.addAll(purchase.getPurchaseCharges());
            }
            for (PurchaseCharge purchaseCharge : purchaseChargesList) {
                if (!purchaseCharge.getCharges().getName().isEmpty()) {
                    if (ledger.getLedgerName().equalsIgnoreCase(purchaseCharge.getCharges().getName())) {
                        netBalance += purchaseCharge.getAmount();
                    }
                }
            }
            //}
            if (/*netBalance==0&&*/(ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense"))) {
                List<SalesCharge> salesChargesList = new ArrayList<>();
                for (Sales sales : salesList) {
                    salesChargesList.addAll(sales.getSalesCharges());
                }
                for (SalesCharge salesCharge : salesChargesList) {
                    if (ledger.getLedgerName().equalsIgnoreCase(salesCharge.getCharges().getName())) {
                        netBalance += salesCharge.getAmount();
                    }
                }
            }
        }

        return netBalance;
    }
    public double findAvailableBalanceForBalanceSheet(Ledger ledger, Date fromDate, Date toDate) {
        log.info("DAO set started at: " + Calendar.getInstance().getTime());                
        List<Purchase> purchaseList = purchaseDAO.findAllByDate(fromDate, toDate);
        List<PurchaseReturn> purchaseReturnList=purchaseReturnDAO.findAllByDate(fromDate, toDate);
        List<Sales> salesList = salesDAO.findAllByDate(fromDate, toDate);
        List<SalesReturn> salesReturnList=salesReturnDAO.findAllByDate(fromDate, toDate);
        log.info("DAO set ended at: " + Calendar.getInstance().getTime()); 
        log.info("Accounts DAO set started at: " + Calendar.getInstance().getTime());    
        List<Receipt> receiptList = receiptDAO.findAllByLedgerAndDate(ledger, fromDate, toDate);
        List<Payment> paymentList = paymentDAO.findAllByLedgerAndDate(ledger, fromDate, toDate);
        List<Journal> journalList = journalDAO.findAllByLedgerAndDate(ledger, fromDate, toDate);
        List<Contra> contraList = contraDAO.findAllByLedgerAndDate(ledger, fromDate, toDate);
        log.info("Accounts DAO set ended at: " + Calendar.getInstance().getTime());                
        double netBalance = 0, result = 0;
        //opeing Balance
        result = ledger.getOpeningBalance();
        netBalance += result;
        //Checking in Receipt
        for (Receipt receipt : receiptList) {
            result = receipt.getAmount();
            if (receipt.getLedgerTo().getId() == ledger.getId()) {

                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    //netBalance+=result;
                    netBalance -= result;

                } 
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    netBalance += result;
                } 
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                    netBalance -= result;
                }
            } else if (receipt.getLedgerBy().getId() == ledger.getId()) {
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    netBalance += result;
                } 
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    netBalance -= result;
                }
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                    netBalance += result;
                }
            }

        }

        //Checking in Payment
        for (Payment payment : paymentList) {
            result = payment.getAmount();
            if (payment.getLedgerBy().getId() == ledger.getId()) {
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    netBalance += result;
                }
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    netBalance -= result;
                }
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                    netBalance += result;
                }
            } else if (payment.getLedgerTo().getId() == ledger.getId()) {
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    netBalance -= result;
                }
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    netBalance += result;
                }
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                    netBalance -= result;
                }
            }
        }

        //Checking in JournalTo
        for (Journal journal : journalList) {
            result = journal.getAmount();
            if (journal.getLedgerBy().getId() == ledger.getId()) {
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    netBalance -= result;
                } 
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    netBalance -= result;
                }
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                    netBalance += result;
                }
            } else if (journal.getLedgerTo().getId() == ledger.getId()) {
                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Expense")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense")) {
                    netBalance -= result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Indirect Income")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Income")) {
                    netBalance += result;
                } else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    netBalance += result;

                } 
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    netBalance += result;
                }
                else if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Capital")) {
                    netBalance -= result;
                }
            }
        }

       // Checking in Contra
        for (Contra contra : contraList) {
            result = contra.getAmount();
            if (contra.getLedgerBy().getId() == ledger.getId()) {
                netBalance += result;
            } else if (contra.getLedgerTo().getId() == ledger.getId()) {
                netBalance -= result;
            }
        }

        if (ledger.getLedgerName().equalsIgnoreCase("discount received")) {
            for (Purchase purchase : purchaseList) 
            {
                result=purchase.getDiscount();
                netBalance += result;
            }
        }
        if (ledger.getLedgerName().equalsIgnoreCase("discount allowed")) {
            for (Sales sales : salesList) 
            {
                result=sales.getDiscount();
                netBalance += result;
            }
        }


        if ((ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability") || ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset"))) {
            //Checking in Purchase
            for (Purchase purchase : purchaseList) {
                result = purchase.getGrandTotal();//+purchase.getDiscount();
                if(purchase.getSupplier().getId()==ledger.getId())
                {
                    if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                        netBalance += result;
                    }
                    if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                        netBalance -= result;
                    }
                }
            }

            //Checking in Purchase returns
//            result = purchaseReturnDAO.findTotalPurchaseReturnWithId(ledger.getId(), fromDate, toDate);
            //log.info("purchaseReturnDAO completed at: " + Calendar.getInstance().getTime());
            for(PurchaseReturn purchaseReturn : purchaseReturnList)
            {
                result=purchaseReturn.getGrandTotal();
                if(purchaseReturn.getPurchase().getSupplier().getId()==ledger.getId())
                {
                    if (result != 0) {
                        if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                            netBalance -= result;
                        }
                        if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                            netBalance += result;
                        }
                    }
                }
            }
            //Checking in Sales
            for (Sales sales : salesList) {
                result = sales.getGrandTotal();//+sales.getDiscount();
                if (result != 0) {
                    if(sales.getCustomer().getId()==ledger.getId())
                    {
                        if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                            netBalance += result;
                        }
                        if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                            netBalance -= result;
                        }
                    }
                }
            }

            //Checking in Sales returns
//            result = salesReturnDAO.findTotalSalesReturnWithId(ledger.getId(), fromDate, toDate);
//            log.info("salesReturnDAO completed at: " + Calendar.getInstance().getTime());
//            if (result != 0) {
//                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
//                    netBalance -= result;
//                }
//                if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
//                    netBalance += result;
//                }
//            }
            for(SalesReturn salesReturn : salesReturnList)
            {
                result=salesReturn.getGrandTotal();
                if(salesReturn.getSales().getCustomer().getId()==ledger.getId())
                {
                    if (result != 0) {
                        if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                            netBalance += result;
                        }
                        if (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                            netBalance -= result;
                        }
                    }
                }
            }
        }
        
        if (netBalance == 0 && (ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense"))) {
            List<PurchaseCharge> purchaseChargesList = new ArrayList<>();
            for (Purchase purchase : purchaseList) {
                purchaseChargesList.addAll(purchase.getPurchaseCharges());
            }
            for (PurchaseCharge purchaseCharge : purchaseChargesList) {
                if (!purchaseCharge.getCharges().getName().isEmpty()) {
                    if (ledger.getLedgerName().equalsIgnoreCase(purchaseCharge.getCharges().getName())) {
                        netBalance += purchaseCharge.getAmount();
                    }
                }
            }
            //}
            if (/*netBalance==0&&*/(ledger.getLedgerGroup().getAccountType().equalsIgnoreCase("Direct Expense"))) {
                List<SalesCharge> salesChargesList = new ArrayList<>();
                for (Sales sales : salesList) {
                    salesChargesList.addAll(sales.getSalesCharges());
                }
                for (SalesCharge salesCharge : salesChargesList) {
                    if (ledger.getLedgerName().equalsIgnoreCase(salesCharge.getCharges().getName())) {
                        netBalance += salesCharge.getAmount();
                    }
                }
            }
        }

        return netBalance;
    }
}
