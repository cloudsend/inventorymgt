package com.cloudsendsoft.inventory.services;

import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.MultiCurrencyDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.PopupTableDialog;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author user
 */
public class MultiCurrencyService {

    public void fillCurrencyListTable(javax.swing.JTable itemTable) {
        DefaultTableModel model = (DefaultTableModel) itemTable.getModel();
        model.setRowCount(0);
        MultiCurrencyDAO multiCurrencyDAO = new MultiCurrencyDAO();
        List<MultiCurrency> items = multiCurrencyDAO.findAllByDESC();
        int i = 0;
        for (MultiCurrency itm : items) {
            ((DefaultTableModel) itemTable.getModel()).insertRow(i, new Object[]{itm.getName(), itm.getSymbol(), itm.getRate()});
            i++;
        }
    }
    public List<MultiCurrency> populatePopupTableByCurrency(String currency,PopupTableDialog popupTableDialog) throws Exception{
       MultiCurrencyDAO currencyDAO=new MultiCurrencyDAO();
        List<MultiCurrency> currencyList=null;
        if(currency.length()<=0){
            currencyList = currencyDAO.findAllByDESC();
        }else{
           // taxList=taxDAO.findAllByDESC();
            currencyList=currencyDAO.findAllStartsWithCurrency(currency);
        }
        ArrayList<Object[]> tableData=new ArrayList<>();
        int slNo=1;
        for(MultiCurrency itm:currencyList){
               tableData.add(new Object [] {slNo++,itm.getName(), itm.getRate(), itm.getSymbol()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.","Currency","Rate","Symbol"});
        popupTableDialog.setTableData(tableData);
        return currencyList;
    }
    
}
