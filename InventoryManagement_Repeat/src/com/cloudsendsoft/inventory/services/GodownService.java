package com.cloudsendsoft.inventory.services;

import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.GodownDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Godown;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import com.cloudsendsoft.inventory.view.PopupTableDialog;
import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author user
 */
public class GodownService {

    public void fillGodownListTable(javax.swing.JTable itemTable) {
        DefaultTableModel model = (DefaultTableModel) itemTable.getModel();
        model.setRowCount(0);

        GodownDAO godownDAO = new GodownDAO();
        List<Godown> items = godownDAO.findAllByDESC();
        int i = 0;
        for (Godown itm : items) {
            ((DefaultTableModel) itemTable.getModel()).insertRow(i, new Object[]{itm.getName(), itm.getColor()});
            i++;
        }

        //hide color column
        TableColumnModel m = itemTable.getColumnModel();
        if (m.getColumnCount() > 1) {
            TableColumn col = m.getColumn(1);
            List<TableColumn> removed = new ArrayList<>();
            removed.add(col);
            m.removeColumn(col);
        }

        itemTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
            //alternate row color jtable
            @Override
            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int column) {
                Component c = super.getTableCellRendererComponent(table,
                        value, isSelected, hasFocus, row, column);
                if (table.getModel().getRowCount() > 0) {
                    
                    if (isSelected) {
                        c.setBackground(Color.LIGHT_GRAY);
                    } else {
                        c.setBackground(new Color(Integer.parseInt((String) table.getModel().getValueAt(row, 1))));
                        c.setForeground(Color.BLACK);
                    }
                }
                return c;
            }
        ;
        });
    }

}
