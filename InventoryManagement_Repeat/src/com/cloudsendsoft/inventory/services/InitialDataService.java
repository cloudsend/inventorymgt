/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.services;

import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.LedgerGroupDAO;
import com.cloudsendsoft.inventory.dao.PrivilegesDAO;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Company;
import com.cloudsendsoft.inventory.model.Godown;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.PackingType;
import com.cloudsendsoft.inventory.model.Privileges;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.model.Unit;
import com.cloudsendsoft.inventory.model.User;
import com.cloudsendsoft.inventory.view.LedgerView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class InitialDataService {

    CRUDServices cRUDServices = new CRUDServices();
    static final Logger log = Logger.getLogger(InitialDataService.class.getName());

    public void createDefaultNumberProperties(Company company) {
        try {
            NumberProperty numberProperty = new NumberProperty();
            numberProperty.setCategory("Sales");
            numberProperty.setCategoryPrefix("SA");
            numberProperty.setCompany(company);
            numberProperty.setNumber(1L);
            cRUDServices.saveModel(numberProperty);
            numberProperty = new NumberProperty();
            numberProperty.setCategory("SalesPreforma");
            numberProperty.setCategoryPrefix("SP");
            numberProperty.setCompany(company);
            numberProperty.setNumber(1L);
            cRUDServices.saveModel(numberProperty);
            numberProperty = new NumberProperty();
            numberProperty.setCategory("Payment");
            numberProperty.setCategoryPrefix("PY");
            numberProperty.setCompany(company);
            numberProperty.setNumber(1L);
            cRUDServices.saveModel(numberProperty);
            numberProperty = new NumberProperty();
            numberProperty.setCategory("Receipt");
            numberProperty.setCategoryPrefix("RC");
            numberProperty.setCompany(company);
            numberProperty.setNumber(1L);
            cRUDServices.saveModel(numberProperty);
            numberProperty = new NumberProperty();
            numberProperty.setCategory("Contra");
            numberProperty.setCategoryPrefix("CR");
            numberProperty.setCompany(company);
            numberProperty.setNumber(1L);
            cRUDServices.saveModel(numberProperty);
            numberProperty = new NumberProperty();
            numberProperty.setCategory("Journal");
            numberProperty.setCategoryPrefix("JN");
            numberProperty.setCompany(company);
            numberProperty.setNumber(1L);
            cRUDServices.saveModel(numberProperty);
            numberProperty = new NumberProperty();
            numberProperty.setCategory("CommercialInvoice");
            numberProperty.setCategoryPrefix("CI");
            numberProperty.setCompany(company);
            numberProperty.setNumber(1L);
            cRUDServices.saveModel(numberProperty);
            numberProperty = new NumberProperty();
            numberProperty.setCategory("PurchaseReturn");
            numberProperty.setCategoryPrefix("PR");
            numberProperty.setCompany(company);
            numberProperty.setNumber(1L);
            cRUDServices.saveModel(numberProperty);
            numberProperty = new NumberProperty();
            numberProperty.setCategory("SalesReturn");
            numberProperty.setCategoryPrefix("SR");
            numberProperty.setCompany(company);
            numberProperty.setNumber(1L);
            cRUDServices.saveModel(numberProperty);
            numberProperty = new NumberProperty();
            numberProperty.setCategory("Purchase");
            numberProperty.setCategoryPrefix("PUR");
            numberProperty.setCompany(company);
            numberProperty.setNumber(1L);
            cRUDServices.saveModel(numberProperty);
            numberProperty = new NumberProperty();
            numberProperty.setCategory("PackingListInvoice");
            numberProperty.setCategoryPrefix("PLI");
            numberProperty.setCompany(company);
            numberProperty.setNumber(1L);
            cRUDServices.saveModel(numberProperty);
            numberProperty = new NumberProperty();
            numberProperty.setCategory("PackingListNumberOfPack");
            numberProperty.setCategoryPrefix("");
            numberProperty.setCompany(company);
            numberProperty.setNumber(1L);
            cRUDServices.saveModel(numberProperty);
            numberProperty = new NumberProperty();
            numberProperty.setCategory("PurchaseOrder");
            numberProperty.setCategoryPrefix("PO");
            numberProperty.setCompany(company);
            numberProperty.setNumber(1L);
            cRUDServices.saveModel(numberProperty);
        } catch (Exception e) {
            log.error("createNumberProperties", e);
        }
    }

    public void createDefaultLedgers(Company company) {
        try {
            LedgerGroupDAO ledgerGroupDAO = new LedgerGroupDAO();

            Ledger ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Cash");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(false);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Cash-in-hand"));
            cRUDServices.saveModel(ledger);

            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Purchase Account");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(false);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Purchase Accounts"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Purchase Returns");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(false);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Purchase Accounts"));
            cRUDServices.saveModel(ledger);

            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Sales Account");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(false);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Sales Accounts"));
            cRUDServices.saveModel(ledger);

            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Sales Returns");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(false);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Sales Accounts"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Commercial Invoice");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(false);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Sales Accounts"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Discount Received");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(false);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Incomes"));
            cRUDServices.saveModel(ledger);

            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Discount Allowed");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(false);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Salary");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(false);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Manufacturing Expense");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(true);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Direct Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Advertisement");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(true);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Electricity");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(true);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Water");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(true);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Rent");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(true);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Internet");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(true);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Bank Charges");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(true);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Commission Allowed");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(true);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Interest on loan");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(true);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Insurance Premium");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(true);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Legal Charges");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(true);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Printing and Stationery");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(true);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Postage and courier");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(true);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Bad Debts");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(true);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);
            
            ledger = new Ledger();
            ledger.setAvailableBalance(0.00);
            ledger.setLedgerName("Rates and taxes");
            ledger.setOpeningBalance(0.00);
            ledger.setCompany(company);
            ledger.setEditable(true);
            ledger.setLedgerGroup(ledgerGroupDAO.findByGroupName("Indirect Expenses"));
            cRUDServices.saveModel(ledger);

        } catch (Exception e) {
            log.error("createLedgers:", e);
        }
    }
    
    public void loadPrivilegesTable(){
        try{
            ArrayList<String> privilegesStringList = new ArrayList(Arrays.asList("PurchaseMenuItem","SalesMenuItem","StockMenuItem", "AccountsMenuItem",
                    "OptionsMenuItem","CompanyMenuItem","HistoryMenuItem","UserMenuItem","PurchaseSidePanel","SalesPreformaSidePanel",
                    "PackingListSidePanel","SalesSidePanel","PaymentSidePanel","ReceiptSidePanel","ContraSidePanel","JournalSidePanel",
                    "PurchaseReturnsSidePanel","SalesReturnsSidePanel","TrialBalanceSidePanel","PLAccountSidePanel","BalanceSheetSidePanel",
                    "Edit","StoreKeeper","Delete"));
            for(String string:privilegesStringList){
                Privileges privileges=new Privileges();
                privileges.setName(string);
                cRUDServices.saveModel(privileges);
            }
        }catch(Exception e){
            log.error("loadInitialPrivileges:", e);
        }
    }
    
    public void loadLedgerGroupTable(){
        try{
            
        ArrayList<String> groupNameStringList = new ArrayList(Arrays.asList("Bank Accounts", "Bank OCC A/c","Bank OD A/c","Branch / Divisions"
                    ,"Capital Account","Cash-in-hand","Current Assets","Current Liabilities","Deposits (Assets)","Direct Expenses","Direct Incomes"
            ,"Duties & Taxes","Expenses (Direct)","Expenses (Indirect)","Fixed Assets","Income (Direct)","Income (Indirect)","Indirect Expenses"
        ,"Indirect Incomes","Investments","Loan & Advances (Asset)","Loans (Liability)","Misc. Expenses (Asset)","Provisions","Purchase Accounts"
        ,"Reserves & Surplus","Retained Earnings","Sales Accounts","Secured Loans","Stock-in-hand","Sundry Creditors","Sundry Debtors"
        ,"Suspense A/c","Unsecured Loans","Commercial Invoice"));
            
            ArrayList<String> accountTypeStringList = new ArrayList(Arrays.asList("Asset", "Liability","Liability","Branch"
                    ,"Liability","Asset","Asset","Liability","Asset","Direct Expense","Direct Income"
            ,"Liability","Direct Expense","Indirect Expence","Asset","Direct Income","Indirect Income","Indirect Expense"
            ,"Indirect Income","Asset","Asset","Liability","Indirect Expense","Liability","Direct Expense"
            ,"Liability","Liability","Direct Income","Liability","Asset","Liability","Asset"
            ,"Suspense","Liability","Direct Income"));
            
            for(int i=0;i<groupNameStringList.size();i++){
                LedgerGroup ledgerGroup=new LedgerGroup();
                ledgerGroup.setGroupName(groupNameStringList.get(i));
                ledgerGroup.setAccountType(accountTypeStringList.get(i));
                cRUDServices.saveModel(ledgerGroup);
            }
        }catch(Exception e){
            log.error("loadLedgerGroupTable:",e);
        }
    }
    
    public void loadUserTable() {
        try {
            User user = new User();
            user.setName("Administrator");
            user.setPassword("admin");
            user.setUserName("admin");
            user.setUserType("Administrator");
            user.setDesignation("Default User");
            
            PrivilegesDAO privilegesDAO=new PrivilegesDAO();
            user.setListOfPrivileges(privilegesDAO.findAllExceptStoreKeeper());
            
            cRUDServices.saveModel(user);
        } catch (Exception e) {
            log.error("loadUserTable:", e);
        }
    }
    
    public void loadOtherTables(Company company){
        try{
            Charges charges=new Charges();
            charges.setCompany(company);
            charges.setName("N/A");
            cRUDServices.saveModel(charges);
            
            Godown godown=new Godown();
            godown.setCompany(company);
            godown.setColor("-10027162");
            godown.setName("Main Godown");
            cRUDServices.saveModel(godown);
            
            PackingType packingType=new PackingType();
            packingType.setCompany(company);
            packingType.setName("N/A");
            packingType.setWeight(0.00);
            cRUDServices.saveModel(packingType);
            
            ShipmentMode shipmentMode=new ShipmentMode();
            shipmentMode.setCompany(company);
            shipmentMode.setName("N/A");
            cRUDServices.saveModel(shipmentMode);
            
            StockGroup stockGroup=new StockGroup();
            stockGroup.setCompany(company);
            stockGroup.setName("General");
            //List<StockGroup> listOfStockGroup=new ArrayList<StockGroup>();
            //listOfStockGroup.add(stockGroup);
            //stockGroup.setStockGroups(listOfStockGroup);
            //stockGroup.setUnderGroup(stockGroup);
            cRUDServices.saveModel(stockGroup);
            
            Tax tax=new Tax();
            tax.setCompany(company);
            tax.setName("N/A");
            tax.setTaxRate(0.00);
            cRUDServices.saveModel(tax);
            
            Unit unit=new Unit();
            unit.setCompany(company);
            unit.setName("Nos.");
            cRUDServices.saveModel(unit);
            
        }catch(Exception e){
            log.error("loadOtherTable:",e);
        }
    }
    public void loadMultiCurrencyTable() {
        try {
            MultiCurrency multiCurrency=new MultiCurrency();
            multiCurrency.setName("Indian Rupee");
            multiCurrency.setRate(1.00);
            multiCurrency.setSymbol("Rs.");
            cRUDServices.saveModel(multiCurrency);
        } catch (Exception e) {
            log.error("loadMultiCurrencyTable:", e);
        }
    }
}
