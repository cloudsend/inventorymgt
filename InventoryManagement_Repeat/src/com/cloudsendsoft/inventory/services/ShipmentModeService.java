/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.services;

import com.cloudsendsoft.inventory.dao.ShipmentModeDAO;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.view.PopupTableDialog;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LOGON SOFT 1
 */
public class ShipmentModeService {
    
    public List<ShipmentMode> populatePopupTableByShipmentMode(String shipmentMode,PopupTableDialog popupTableDialog) throws Exception{
       ShipmentModeDAO shipmentmodeDAO=new ShipmentModeDAO();
        List<ShipmentMode> shipmentModeList=null;
        if(shipmentMode.length()<=0){
            shipmentModeList = shipmentmodeDAO.findAllByDESC();
        }else{
           // taxList=taxDAO.findAllByDESC();
            shipmentModeList=shipmentmodeDAO.findAllStartsWithShipmentMode(shipmentMode);
        }
        ArrayList<Object[]> tableData=new ArrayList<>();
        int slNo=1;
        for(ShipmentMode itm:shipmentModeList){
               tableData.add(new Object [] {slNo++,itm.getName()});
        }
        popupTableDialog.setTitle(new String[]{"Sl No.","Shipment Mode"});
        popupTableDialog.setTableData(tableData);
        return shipmentModeList;
    }
    
}
