/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.PurchaseDAO;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.StockGroupDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import static com.cloudsendsoft.inventory.view.PurchaseView.log;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class SalesReturnView extends javax.swing.JPanel {

    static final Logger log = Logger.getLogger(SalesReturnView.class.getName());

    CommonService commonService = new CommonService();

    DefaultTableModel ledgerTableModel = null;

    List<StockGroup> listOfStockGroup = null;
    List<Item> listOfItem = null;
    List<PurchaseItem> listOfPurchaseItem = null;
    List<Sales> listOfSales = null;

    StockGroup stockGroup = null;
    Item item = null;
    String month = null;

    StockGroupDAO stockGroupDAO = new StockGroupDAO();
    ItemDAO itemDAO = new ItemDAO();
    SalesDAO salesDAO = new SalesDAO();
    //SalesItemDAO salesItemDAO=new SalesItemDAO();
    //SalesDAO salesDAO=new SalesDAO();

    List<Ledger> listOfLedger = null;
    Ledger ledgerList = null;
    Sales sales = null;
    LedgerDAO ledgerDAO = new LedgerDAO();
    Sales salesList = null;

    int selectedIndex = -1;
    int screen = 1;
    int index = 0;

    String[] months = new DateFormatSymbols().getMonths();

    public SalesReturnView() {
        initComponents();
        //table background color settings
        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);
        salesReturnTable.setBackground(this.getBackground());
//        salesReturnTable.getTableHeader().setBackground(this.getBackground());
//        salesReturnTable.getTableHeader().setForeground(this.getBackground());

        //set table design
        salesReturnTable.setRowHeight(GlobalProperty.tableRowHieght);

        listOfLedger = ledgerDAO.findAllCustomerForSalesReturnBy();

        fillStockSummaryTable();
    }

    void fillStockSummaryTable() {
        try {
            int slNo = 1;

            double openingQty = 0.00;
            double openingBal = 0.00;

            double inwardsQty = 0.00;
            double inwardsValue = 0.00;
            double outwardsQty = 0.00;
            double outwardsValue = 0.00;

            double closingQty = 0.00;
            double closingBal = 0.00;

            switch (screen) {
                case 1:
                    //Stock Groups Page
                    leftHeadingLabel.setText("Sales Return");
                    //table columns,model & size
                    salesReturnTable.setShowGrid(false);
                    salesReturnTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                            null,
                            new String[]{"Sr. No.", "Ledger Name", "Leder Group"}
                    ));
                    ledgerTableModel = (DefaultTableModel) salesReturnTable.getModel();
                    CommonService.setWidthAsPercentages(salesReturnTable, .05, .75, .20);

                    ledgerTableModel.setRowCount(0);
                    for (Ledger ledgerList : listOfLedger) {
                        ledgerTableModel.addRow(new Object[]{slNo++, ledgerList.getLedgerName(), ledgerList.getLedgerGroup().getGroupName()});
                    }
                    break;
                case 2:
                    leftHeadingLabel.setText("Sales Return");
                    //table columns,model & size
                    //rightHeadingLabel.setText("Items Purchased by the User");
                    salesReturnTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                            null,
                            new String[]{"Sr. No.", "Invoice No", "Name", "Bill Date",  "Currency", "Grand Total"}
                    ));
                    ledgerTableModel = (DefaultTableModel) salesReturnTable.getModel();
                    CommonService.setWidthAsPercentages(salesReturnTable, .05, .4, .6, .3,.5,.5);

                    ledgerTableModel.setRowCount(0);

                    for (Sales salesList : listOfSales) {

                        ledgerTableModel.addRow(new Object[]{slNo++, salesList.getInvoiceNumber(), salesList.getCustomer().getLedgerName(), commonService.sqlDateToString(salesList.getBillDate()),  salesList.getMultiCurrency().getName(), salesList.getMultiCurrency().getSymbol()+salesList.getGrandTotal()});
                    }
                    break;
                case 3:

                    break;
                case 4:

                    break;

            }

            //String currencySymbol = (null == currency) ? "" : currency.getSymbol();
            //{"Sr. No.", "Item Code","Description", "Unit Price", "Qty.", "Total Amount"}
          /*  for (PurchaseItem pItem : purchaseItemList) {
             double itemTotal = pItem.getUnitPrice() * pItem.getQuantity();
             purchaseItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), currency.getSymbol() + ". " + pItem.getUnitPrice(), pItem.getQuantity(), currency.getSymbol() + " " + itemTotal});
             //grandTotal.add(BigDecimal.pa itemTotal);
             subTotal += itemTotal;
             grandTotal = subTotal;
             }*/
            //selected first row of table by default
            // stockSummaryTable.setRowSelectionInterval(0, 0);
            // stockSummaryTable.requestFocusInWindow();
        } catch (Exception e) {
            log.error("fillStockSummaryTable:", e);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headingPanel = new javax.swing.JPanel();
        leftHeadingLabel = new javax.swing.JLabel();
        searchInvoiceText = new javax.swing.JTextField();
        searchButton = new javax.swing.JButton();
        bodyPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        salesReturnTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        headingPanel.setBackground(new java.awt.Color(47, 105, 142));
        headingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        headingPanel.setPreferredSize(new java.awt.Dimension(1124, 50));

        leftHeadingLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        leftHeadingLabel.setForeground(new java.awt.Color(255, 255, 255));
        leftHeadingLabel.setText("Purchase Return");

        searchInvoiceText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchInvoiceTextActionPerformed(evt);
            }
        });

        searchButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/search.png"))); // NOI18N
        searchButton.setText("Search");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout headingPanelLayout = new javax.swing.GroupLayout(headingPanel);
        headingPanel.setLayout(headingPanelLayout);
        headingPanelLayout.setHorizontalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addComponent(leftHeadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 601, Short.MAX_VALUE)
                .addComponent(searchInvoiceText, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        headingPanelLayout.setVerticalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, headingPanelLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(leftHeadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchInvoiceText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchButton))
                .addContainerGap())
        );

        add(headingPanel, java.awt.BorderLayout.PAGE_START);

        bodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        bodyPanel.setOpaque(false);

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));

        salesReturnTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        salesReturnTable.setOpaque(false);
        salesReturnTable.setSelectionBackground(new java.awt.Color(153, 153, 153));
        salesReturnTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                salesReturnTableMouseClicked(evt);
            }
        });
        salesReturnTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                salesReturnTableKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(salesReturnTable);

        javax.swing.GroupLayout bodyPanelLayout = new javax.swing.GroupLayout(bodyPanel);
        bodyPanel.setLayout(bodyPanelLayout);
        bodyPanelLayout.setHorizontalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1124, Short.MAX_VALUE)
        );
        bodyPanelLayout.setVerticalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 397, Short.MAX_VALUE)
        );

        add(bodyPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void salesReturnTableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_salesReturnTableKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE && screen > 0) {
            screen--;
            fillStockSummaryTable();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            selectedIndex=salesReturnTable.getSelectedRow();
            clickAction();
        }
    }//GEN-LAST:event_salesReturnTableKeyPressed

    void clickAction() {
        try {
            //ArrayList row = CommonService.getTableRowData(purchaseReturnTable);
            //String selectedFirstCol=row.get(3) + "";
            //if(!selectedFirstCol.equalsIgnoreCase("Closing Balance")){
            switch (screen) {
                case 1:
                    // index=Integer.parseInt(selectedFirstCol);
                    ledgerList = listOfLedger.get(selectedIndex);
                    listOfSales = salesDAO.findAllByCustomer(ledgerList);
                    break;
                case 2:
                    //index=Integer.parseInt(selectedFirstCol);
                    salesList = listOfSales.get(selectedIndex);
                    SalesReturnItemView salesReturnItemView = new SalesReturnItemView(salesList);
                    GlobalProperty.mainForm.getContentPanel().removeAll();
                    GlobalProperty.mainForm.getContentPanel().repaint();
                    GlobalProperty.mainForm.getContentPanel().revalidate();
                    GlobalProperty.mainForm.getContentPanel().add(salesReturnItemView);

                    //item=listOfItem.get(index-1);
                    break;
                case 3:
                    //month=selectedFirstCol.trim();
                    break;

            }
            screen++;
            fillStockSummaryTable();
            //}

        } catch (Exception e) {
            log.error("clickAction:", e);
        }
    }
    private void salesReturnTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salesReturnTableMouseClicked
        try {
            if (evt.getClickCount() == 2) {
                selectedIndex = salesReturnTable.rowAtPoint(evt.getPoint());
                clickAction();
            }
        } catch (Exception e) {
            log.error("stockSummaryTableMouseClicked", e);
        }

    }//GEN-LAST:event_salesReturnTableMouseClicked

    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed
        screen = 2;
        listOfSales = salesDAO.findAllByInvoiceNo(searchInvoiceText.getText().trim());
        if (listOfSales.size() == 0) {
            MsgBox.warning("Invoice No not found");
        } else {
            fillStockSummaryTable();
        }
    }//GEN-LAST:event_searchButtonActionPerformed

    private void searchInvoiceTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchInvoiceTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchInvoiceTextActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bodyPanel;
    private javax.swing.JPanel headingPanel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel leftHeadingLabel;
    private javax.swing.JTable salesReturnTable;
    private javax.swing.JButton searchButton;
    private javax.swing.JTextField searchInvoiceText;
    // End of variables declaration//GEN-END:variables
}
