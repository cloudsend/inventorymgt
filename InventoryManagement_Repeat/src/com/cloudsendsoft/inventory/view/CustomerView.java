/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.LedgerGroupDAO;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.LedgerService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import static com.cloudsendsoft.inventory.view.LedgerView.log;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author LOGON SOFT 1
 */
public class CustomerView extends javax.swing.JInternalFrame {

    /**
     * Creates new form CustomerView
     */
    static final Logger log = Logger.getLogger(CustomerView.class.getName());
    LedgerService ledgerService = new LedgerService();
    Ledger ledger = null;
    LedgerDAO ledgerDAO = new LedgerDAO();
    LedgerGroupDAO ledgerGroupDAO = new LedgerGroupDAO(); 
    CRUDServices cRUDServices = null;
    List<Ledger> ledgerList = null;
    CommonService commonService = new CommonService();            
    public CustomerView() {
        initComponents();
        
        if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
            saveButton.setEnabled(false);
            newButton.setEnabled(false);
        }
        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);

        this.getRootPane().setDefaultButton(saveButton);
        //customersTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(null, new String[]{"Name","Address","Phone","E-mail"}));
        customersTable.setRowHeight(25);
        ledgerList = ledgerService.fillCustomersTable(customersTable);
        cRUDServices = new CRUDServices();
        SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        nameText.requestFocus();
      }
        });
          //save shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "save");
            this.getActionMap().put("save", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveButtonActionPerformed(e);
                }
            });
         //new Shortcut
           this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_N,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "new");
            this.getActionMap().put("new", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    newButtonActionPerformed(e);
                }
            });
            //Delete shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_D,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "delete");
            this.getActionMap().put("delete", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    deleteButtonActionPerformed(e);
                }
            });
           // Cancel shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_W,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "cancel");
            this.getActionMap().put("cancel", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveButtonActionPerformed(e);
                }
            });
            setFocusOrder();
    }
    void setFocusOrder()
    {
            nameText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    telephoneText.requestFocusInWindow();
                }
            });
            telephoneText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    emailText.requestFocusInWindow();
                }
            });
           emailText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveButton.requestFocusInWindow();
                }
            });
           
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        detailsPanel = new javax.swing.JPanel();
        nameText = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        telephoneText = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        emailText = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        addressTextPane = new javax.swing.JTextPane();
        buttonPanel = new javax.swing.JPanel();
        cancelButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        newButton = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        customersTable = new javax.swing.JTable();

        setClosable(true);
        setTitle("Customers");
        setPreferredSize(new java.awt.Dimension(569, 690));
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setPreferredSize(new java.awt.Dimension(395, 247));

        detailsPanel.setBackground(new java.awt.Color(255, 255, 255));
        detailsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Details"));
        detailsPanel.setOpaque(false);

        jLabel6.setText("Address");

        jLabel7.setText("Name");

        jLabel8.setText("Telephone");

        telephoneText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                telephoneTextKeyTyped(evt);
            }
        });

        jLabel9.setText("E-mail");

        jScrollPane3.setViewportView(addressTextPane);

        javax.swing.GroupLayout detailsPanelLayout = new javax.swing.GroupLayout(detailsPanel);
        detailsPanel.setLayout(detailsPanelLayout);
        detailsPanelLayout.setHorizontalGroup(
            detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(detailsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9))
                .addGap(32, 32, 32)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(emailText)
                    .addComponent(telephoneText)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                    .addComponent(nameText))
                .addContainerGap())
        );
        detailsPanelLayout.setVerticalGroup(
            detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, detailsPanelLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(nameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(telephoneText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(emailText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        buttonPanel.setBackground(new java.awt.Color(255, 255, 255));
        buttonPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new java.awt.GridLayout(6, 1, 0, 8));

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(cancelButton);

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/delete-icon.png"))); // NOI18N
        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(deleteButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveButton.setText("Save/Update");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(saveButton);

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("New");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(newButton);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(detailsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(detailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(17, 17, 17))
        );

        jPanel1.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Customers List"));

        customersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        customersTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                customersTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(customersTable);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 541, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 541, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 364, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 364, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)))
        );

        jPanel1.add(jPanel3, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel1, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
             ledger=null;
             CommonService.clearTextFields(new JTextField[]{nameText, telephoneText, emailText});
             CommonService.clearTextPane(new JTextPane[]{addressTextPane});
             ledger=null;
            
    }//GEN-LAST:event_newButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        try {
            String ledgerName = nameText.getText().trim();
            boolean isEditable = true;
            if (ledger != null) {
                if (!ledger.isEditable()) {
                    isEditable = false;
                }
            }
            if (isEditable) {
                if (!ledgerDAO.isLedgerNameExist(ledgerName)) {
                    LedgerGroup ledgerGroup = ledgerGroupDAO.findByGroupName("Sundry Debtors");
                    double balanceAmount = 0.00;
                   if (CommonService.stringValidator(new String[]{ledgerName})) {
                        if (MsgBox.confirm("Are you sure you want to save or update?")) {
                            if (ledger == null) {
                                ledger = new Ledger();
                            }
                            ledger.setCompany(GlobalProperty.getCompany());
                            ledger.setLedgerName(ledgerName);
                            ledger.setLedgerGroup(ledgerGroup);
                            if (nameText.getText().trim().length() > 0) {
                                ledger.setName(ledgerName);
                            }
                            if (addressTextPane.getText().trim().length() > 0) {
                                ledger.setAddress(addressTextPane.getText());
                            }
                            if (telephoneText.getText().trim().length() > 0) {
                                ledger.setTelephone(telephoneText.getText().trim());
                            }
                            if (emailText.getText().trim().length() > 0) {
                                ledger.setEmail(emailText.getText().trim());
                            }
                            ledger.setAvailableBalance(balanceAmount);
                            ledger.setOpeningBalance(balanceAmount);
                            ledger.setEditable(true);
                            cRUDServices.saveOrUpdateModel(ledger);
                            MsgBox.success("Successfully Added " + ledgerName);
                            CommonService.clearTextFields(new JTextField[]{ nameText, telephoneText, emailText});
                            CommonService.clearTextPane(new JTextPane[]{addressTextPane});
                            ledgerList=ledgerService.fillCustomersTable(customersTable);
                            
                        }
                    } else {
                        MsgBox.warning("Ledger Name is mandatory");
                        nameText.requestFocusInWindow();
                        ledger=null;
                    }
                } else {
                    MsgBox.abort("Ledger Name '" + ledgerName + "' is already exists.");
                    nameText.requestFocusInWindow();
                }
            } else {
                MsgBox.warning("You can't edit auto generated ledger");
            }
        } catch (Exception e) {
            log.error("addButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
         try {
            Session session = null;
            if (null != ledger) {
                String ledgerName = ledger.getLedgerName();
                if (ledger.isEditable()) {
                    if (MsgBox.confirm("Are you sure you want to delete?")) {
                        try {
                            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
                            session = sessionFactory.openSession();
                            Transaction transaction = session.beginTransaction();
                            session.delete(ledger);
                            transaction.commit();

                            MsgBox.success("Successfully Deleted " + ledgerName);
                            CommonService.clearTextFields(new JTextField[]{ nameText, telephoneText, emailText});
                            CommonService.clearTextPane(new JTextPane[]{addressTextPane});
                            ledgerList = ledgerService.fillCustomersTable(customersTable);
                            nameText.requestFocusInWindow();

                        } catch (Exception e) {
                            MsgBox.warning("Ledger '" + ledgerName + "' is already used for transactions");
                            nameText.requestFocusInWindow();
                        } finally {
                            if (session != null) {
                                session.clear();
                                session.close();
                            }
                        }
                    }
                } else {
                    MsgBox.warning("You can't delete auto generated ledger");
                }
            }
        } catch (Exception e) {
            log.error("deleteButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void customersTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_customersTableMouseClicked
            ledger= ledgerList.get(customersTable.getSelectedRow());
            int column = customersTable.getSelectedColumn();
            nameText.setText(ledger.getName()); 
            addressTextPane.setText(ledger.getAddress());
            telephoneText.setText(ledger.getTelephone());
            emailText.setText(ledger.getEmail());
            switch(column)
            {
                case 0: nameText.requestFocusInWindow();
                        nameText.selectAll();
                        break;
                case 1: addressTextPane.requestFocusInWindow();
                        addressTextPane.selectAll();
                        break;
                case 2: telephoneText.requestFocusInWindow();
                        telephoneText.selectAll();
                        break;
                case 3: emailText.requestFocusInWindow();
                        emailText.selectAll();
                        break;
           }
          
    }//GEN-LAST:event_customersTableMouseClicked

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void telephoneTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_telephoneTextKeyTyped
       CommonService.numberValidator(evt);
    }//GEN-LAST:event_telephoneTextKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextPane addressTextPane;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTable customersTable;
    private javax.swing.JButton deleteButton;
    private javax.swing.JPanel detailsPanel;
    private javax.swing.JTextField emailText;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField nameText;
    private javax.swing.JButton newButton;
    private javax.swing.JButton saveButton;
    private javax.swing.JTextField telephoneText;
    // End of variables declaration//GEN-END:variables
}
