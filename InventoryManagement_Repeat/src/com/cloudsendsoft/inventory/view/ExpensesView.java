/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.LedgerGroupDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.model.Contra;
import com.cloudsendsoft.inventory.model.Journal;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.Payment;
import com.cloudsendsoft.inventory.model.Receipt;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.LedgerService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import static com.cloudsendsoft.inventory.view.JournalEntryView.log;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;
import sun.applet.resources.MsgAppletViewer;

/**
 *
 * @author LOGON SOFT 1
 */
public class ExpensesView extends javax.swing.JInternalFrame {

    /**
     * Creates new form ExpensesView
     */
    static final Logger log = Logger.getLogger(ExpensesView.class.getName());
    LedgerService ledgerService = new LedgerService();
    Ledger ledger = null;
    LedgerDAO ledgerDAO = new LedgerDAO();
    LedgerGroupDAO ledgerGroupDAO = new LedgerGroupDAO(); 
    CRUDServices cRUDServices = null;
    List<Ledger> ledgerList = null;
    int delRow =-1;
    Payment payment = null;
    int check=0;
    CommonService commonService = new CommonService();
    NumberProperty numberProperty = null;
    NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
    public ExpensesView() {
        initComponents();
        expensesLabel.setVisible(false);
        //expensesTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(null, new String[]{"Expense","Amount"}));
        expensesTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(null, new String[]{"Expense","Amount","By","Details","Date"}));
      //  ledgerService.fillExpensesTable(expensesTable);
        //Add Items To ExpensesCombo
        for (Ledger ledgerItem : ledgerDAO.findAllExpenses()) 
        { //for (Ledger ledgerItem : ledgerDAO.findAllCustomerPaymentBy()){
                //expenseComboBox.addItem(new ComboKeyValue((ledgerItem.getLedgerName()), ledgerItem));
             //expenseComboBox.addItem(ledgerItem);
             expenseComboBox.addItem(ledgerItem.getLedgerName());
        }
        cRUDServices = new CRUDServices();
        for (Ledger ledgerItem : ledgerDAO.findAllCustomerPaymentTo())
        {
                byComboBox.addItem(ledgerItem.getLedgerName());
        }
        //creditorComboBox.add();
        expensesLabel.setText("Payment");
        numberProperty = numberPropertyDAO.findInvoiceNo("Payment");
        expensesLabel.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
        commonService.setCurrentPeriodOnCalendar(expensesDate);
        expensesDate.setDate(new java.util.Date());    
        //set combo box selected onload
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
              expenseComboBox.requestFocus();
            }
        });
        //save shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "save");
            this.getActionMap().put("save", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveButtonActionPerformed(e);
                }
            });
            //Add Short cut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "add");
            this.getActionMap().put("add", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addButtonActionPerformed(e);
                }
            });
            //Delete short cut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_D,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "delete");
            this.getActionMap().put("delete", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    deleteButtonActionPerformed(e);
                }
            });
            //cancel short cut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_W,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "cancel");
            this.getActionMap().put("cancel", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    cancelButtonActionPerformed(e);
                }
            });
            setFocusOrder();
    }
    void setFocusOrder()
    {
         expenseComboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    amountText.requestFocusInWindow();
                }
            });
          amountText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    byComboBox.requestFocusInWindow();
                }
            });
           byComboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(detailsText.isVisible())
                        detailsText.requestFocusInWindow();
                    else
                        addButton.requestFocusInWindow();
                }
            });
           addButton.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e){
                    saveButton.requestFocusInWindow();
                }   
           });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        mainPanel = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        amountText = new javax.swing.JTextField();
        expensesDate = new com.toedter.calendar.JDateChooser();
        byComboBox = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        expenseComboBox = new javax.swing.JComboBox();
        chequeLabel = new javax.swing.JLabel();
        detailsText = new javax.swing.JTextField();
        expensesLabel = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        addButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        expensesTable = new javax.swing.JTable();

        setClosable(true);
        setTitle("Expenses");
        setPreferredSize(new java.awt.Dimension(650, 608));
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel1.setLayout(new java.awt.BorderLayout());

        mainPanel.setBackground(new java.awt.Color(255, 255, 255));
        mainPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        mainPanel.setPreferredSize(new java.awt.Dimension(300, 165));
        mainPanel.setLayout(new java.awt.GridLayout(1, 0));

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Expense");

        jLabel4.setText("By");

        amountText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                amountTextKeyTyped(evt);
            }
        });

        expensesDate.setDateFormatString("dd/MM/yyyy");

        byComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                byComboBoxItemStateChanged(evt);
            }
        });

        jLabel3.setText("Date");

        jLabel2.setText("Amount");

        chequeLabel.setText("Cheque Number");

        detailsText.setAutoscrolls(false);

        expensesLabel.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        expensesLabel.setText("Label");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(expensesLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4)
                            .addComponent(chequeLabel))
                        .addGap(55, 55, 55)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(detailsText, javax.swing.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)
                            .addComponent(byComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(expenseComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(amountText)
                            .addComponent(expensesDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(expensesLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addComponent(expensesDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(expenseComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(amountText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(byComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(detailsText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chequeLabel))
                .addGap(0, 0, 0))
        );

        mainPanel.add(jPanel5);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setLayout(new java.awt.GridLayout(5, 1));

        addButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        addButton.setText("Add");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });
        jPanel7.add(addButton);

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/delete-icon.png"))); // NOI18N
        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        jPanel7.add(deleteButton);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(212, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        mainPanel.add(jPanel2);

        jPanel1.add(mainPanel, java.awt.BorderLayout.PAGE_START);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.setLayout(new java.awt.GridLayout(1, 0));

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        jPanel6.add(saveButton);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/cancel.png"))); // NOI18N
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        jPanel6.add(cancelButton);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(152, 152, 152)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel1.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        expensesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        expensesTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                expensesTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(expensesTable);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 672, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 672, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 346, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 346, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel4, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel1, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
      
         
         if(amountText.getText().equals("")||expensesDate.getDate()==null)
         {
              MsgBox.warning("Please fill in required fields");
         }
         else
         {
             //String paymentId = expensesLabel.getText();
             String expenseName = expenseComboBox.getSelectedItem().toString();
             double amount = Double.parseDouble(amountText.getText());
             String byName = byComboBox.getSelectedItem().toString();
             Date payDate = commonService.utilDateToSqlDate(expensesDate.getDate());
             
             String details = detailsText.getText();
             int test =0;
             for(int i= 0; i<expensesTable.getRowCount();i++){
                 if(expenseName==expensesTable.getValueAt(i, 0))
                 {
                     test=1;
                 }
             }
             if(test==0){
                 fillExpensesTable(expensesTable,expenseName ,amount,byName,details,payDate, expensesTable.getRowCount());
             }
             else
             {
                 MsgBox.warning("Already exists");
             }
         }
        
    }//GEN-LAST:event_addButtonActionPerformed

    private void expensesTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_expensesTableMouseClicked
        delRow = expensesTable.getSelectedRow();
    }//GEN-LAST:event_expensesTableMouseClicked

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        if(delRow!=-1)
        {
            deleteExpensesTable(expensesTable, delRow);
            delRow=-1;
        } else {
            MsgBox.warning("Select any row to delete!!!");
        }
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
      if(expensesTable.getRowCount()==0)
        {
            MsgBox.warning("No Payments to Save!!!");
        }
        else
        {
            Ledger byLedger = null;
            Ledger toLedger = null;
            for(int i= 0; i<expensesTable.getRowCount();i++)
            {
                try {
                    byLedger = ledgerDAO.findByLedgerName((String) expensesTable.getValueAt(i, 0));
                    toLedger = ledgerDAO.findByLedgerName((String)expensesTable.getValueAt(i, 2));
                    // ledgerSave.setAvailableBalance((Double)expensesTable.getValueAt(i, 1));
                    if (payment == null) {
                        payment = new Payment();
                        check = 0;
                    }
                    payment.setPaymentDate(commonService.utilDateToSqlDate((Date) expensesTable.getValueAt(i,4)));
                    expensesLabel.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
                    payment.setPaymentNo(expensesLabel.getText());
                    payment.setAmount((Double) expensesTable.getValueAt(i, 1));
                    payment.setNarration((String) expensesTable.getValueAt(i,3));
                    payment.setLedgerBy(byLedger);
                    payment.setLedgerTo(toLedger);
                    payment.setCompany(GlobalProperty.getCompany());
                    if (check == 1) {
                        cRUDServices.saveOrUpdateModel(payment);
                    } else {
                        cRUDServices.saveModel(payment);
                    }
                    if (byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Liability") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Direct Income") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Indirect Income") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Capital")) {
                        byLedger.setAvailableBalance(byLedger.getAvailableBalance() - (Double) expensesTable.getValueAt(i, 1));
                        cRUDServices.saveOrUpdateModel(byLedger);
                    } else {
                        byLedger.setAvailableBalance(byLedger.getAvailableBalance() + (Double) expensesTable.getValueAt(i, 1));
                        cRUDServices.saveOrUpdateModel(byLedger);
                    }
                    if (toLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Asset") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Direct Expense") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Indirect Expence")) {
                    toLedger.setAvailableBalance(toLedger.getAvailableBalance() - (Double) expensesTable.getValueAt(i, 1));
                    cRUDServices.saveOrUpdateModel(toLedger);
                    } else {
                    toLedger.setAvailableBalance(toLedger.getAvailableBalance() + (Double) expensesTable.getValueAt(i, 1));
                    cRUDServices.saveOrUpdateModel(toLedger);
                    }
                    numberProperty.setNumber(numberProperty.getNumber() + 1);
                    cRUDServices.saveOrUpdateModel(numberProperty);


                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(ExpensesView.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
                    MsgBox.success("Payment(s) Saved Successfully");
                    clearAllFields();
                    this.dispose();
        }
      
    }//GEN-LAST:event_saveButtonActionPerformed
    public void clearAllFields(){
            expensesDate.setDate(new java.util.Date());
            commonService.clearCurrencyFields(new JTextField[]{amountText});
            detailsText.setText("");
            numberProperty = numberPropertyDAO.findInvoiceNo("Payment");
            expensesLabel.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
    }     
    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void byComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_byComboBoxItemStateChanged
       Ledger selectedMode = new Ledger();
        selectedMode=ledgerDAO.findByLedgerName((String)byComboBox.getSelectedItem()) ;
        if(selectedMode.getLedgerGroup().getGroupName().equalsIgnoreCase("Bank Accounts"))   
        {
            chequeLabel.setVisible(true);
            detailsText.setVisible(true);
        }
        else{
            chequeLabel.setVisible(false);
            detailsText.setVisible(false);
        }
    }//GEN-LAST:event_byComboBoxItemStateChanged

    private void amountTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_amountTextKeyTyped
       CommonService.currencyValidator(evt);
    }//GEN-LAST:event_amountTextKeyTyped
    public void fillExpensesTable(javax.swing.JTable expensesTable ,String expenseName  , double amount ,String byName ,String details,Date date, int row){
        ((DefaultTableModel) expensesTable.getModel()).insertRow(row, new Object [] {expenseName, amount,byName,details,date});
    }
    public void deleteExpensesTable(javax.swing.JTable expensesTable , int row){
        ((DefaultTableModel) expensesTable.getModel()).removeRow(row);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JTextField amountText;
    private javax.swing.JComboBox byComboBox;
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel chequeLabel;
    private javax.swing.JButton deleteButton;
    private javax.swing.JTextField detailsText;
    private javax.swing.JComboBox expenseComboBox;
    private com.toedter.calendar.JDateChooser expensesDate;
    private javax.swing.JLabel expensesLabel;
    private javax.swing.JTable expensesTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton saveButton;
    // End of variables declaration//GEN-END:variables
}
