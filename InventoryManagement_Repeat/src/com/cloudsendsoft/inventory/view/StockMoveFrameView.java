

/**
 *
 * @author LogOn
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.GodownDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.MultiCurrencyDAO;
import com.cloudsendsoft.inventory.dao.PurchaseDAO;
import com.cloudsendsoft.inventory.dao.ShipmentModeDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.ItemService;
import com.cloudsendsoft.inventory.services.LedgerService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Godown;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseCharge;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseTax;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.model.StockMovement;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import static com.cloudsendsoft.inventory.view.StockMoveFrameView.log;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger; 

public class StockMoveFrameView extends javax.swing.JInternalFrame {
     static double totalAmount = 0;
    double subTotal = 0.00;
    double grandTotal = 0.00;

    static final Logger log = Logger.getLogger(StockMoveFrameView.class.getName());

    ItemDAO itemDAO = new ItemDAO();
    CommonService commonService = new CommonService();
    MainForm mainForm = null;
    LedgerService ledgerService = new LedgerService();
    TaxDAO taxDAO = new TaxDAO();
    GodownDAO godownDAO = new GodownDAO();
    ChargesDAO chargesDAO = new ChargesDAO();
    MultiCurrencyDAO multiCurrencyDAO = new MultiCurrencyDAO();
    ShipmentModeDAO shipmentModeDAO = new ShipmentModeDAO();
    PurchaseDAO purchaseDAO = new PurchaseDAO();
    //global usage 
    Ledger supplierLedger = null;
    //double itemQty = 0;
    List<PurchaseItem> purchaseItemList = new ArrayList<PurchaseItem>();
    List<PurchaseItem> purchaseItemListHistory = null;
    Item fromItem = null;
    Item toItem = null;
    List<PurchaseTax> purchaseTaxList = new ArrayList<PurchaseTax>();
    List<PurchaseCharge> purchaseChargesList = new ArrayList<PurchaseCharge>();
    Item item=null;
    MultiCurrency currency=null;
    Purchase purchase = null;
    int check = 0;
    List<Item> itemList = new ArrayList<Item>();

    PurchaseItem purchaseItem = null;
    DefaultTableModel purchaseItemTableModel = null;
    boolean isDupplicateColumnsVisible=false;
    CRUDServices cRUDServices = new CRUDServices();
    StockMovement stockMove=new StockMovement();
    Godown godown=null;
    
    boolean isDuplicateColumnsVisible = false;


    /**
     * Creates new form StockMoveFrameView
     */
    public StockMoveFrameView() {
      try {
            initComponents();
            SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        itemCodeText.requestFocus();
      }
            });
            qtyToMovePanel.remove(qtyToMoveText1);
            availQtyPanel.remove(itemAvailQtyText1); 
         
                if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
                buttonPanel.remove(saveButton);
                buttonPanel.remove(newButton);
                buttonPanel.repaint();
                buttonPanel.revalidate();
            }
            
          
           billDateFormattedText.setDate(new java.util.Date());
           billDateFormattedText.setEnabled(false); 
           setFocusOrder();
           
       } catch (Exception e) {
            log.error("Purchase:", e);
        }
      
       this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_W,
                                    java.awt.event.InputEvent.SHIFT_DOWN_MASK
                                    | java.awt.event.InputEvent.CTRL_DOWN_MASK), "forward");
            this.getActionMap().put("forward", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(itemAvailQtyText1.getParent()==availQtyPanel)
                    {
                    availQtyPanel.remove(itemAvailQtyText1);
                    availQtyPanel.revalidate();
                    qtyToMovePanel.remove(qtyToMoveText1);
                    qtyToMovePanel.revalidate();
                    isDuplicateColumnsVisible=false;
                    }
                    else
                    {
                       availQtyPanel.add(itemAvailQtyText1);
                       availQtyPanel.revalidate();
                       qtyToMovePanel.add(qtyToMoveText1);
                       qtyToMovePanel.revalidate();
                    }
                }
          });
                
    this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "save");
            this.getActionMap().put("save", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveButtonActionPerformed(e);
                   }
            });   
            
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_N,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "new");
            this.getActionMap().put("new", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                   clearAllFields();
                }
            });
           this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_C,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "cancel");
            this.getActionMap().put("cancel", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    cancelButtonActionPerformed(e);
                }
            });

    }
     void generateAllList(String billDate, String ledgerName, String invoiceNumber) {
        try {

        } catch (Exception e) {
            log.error("generateAllList:", e);
        }
    }
     
     void addStockMoveDuplicateColumns()
     {
       availQtyPanel.add(itemAvailQtyText1);
       availQtyPanel.revalidate();
       qtyToMovePanel.add(qtyToMoveText1);
       qtyToMovePanel.revalidate();
     }
    void setFocusOrder() {
        try {
             itemCodeText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    qtyToMoveText.requestFocusInWindow();
                }
            });
            qtyToMoveText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(isDuplicateColumnsVisible=true)                    
                         qtyToMoveText1.requestFocusInWindow();
                    else
                       destinationText.requestFocusInWindow();
                }
            });
            
            qtyToMoveText1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    destinationText.requestFocusInWindow();
                }
            });
            
            qtyToMoveText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    destinationText.requestFocusInWindow();
                }
            });
            
            destinationText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveButton.requestFocusInWindow();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel1 = new javax.swing.JPanel();
        inputPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        itemCodeText = new javax.swing.JTextField();
        availQtyPanel = new javax.swing.JPanel();
        itemAvailQtyText = new javax.swing.JTextField();
        itemAvailQtyText1 = new javax.swing.JTextField();
        itemDescriptionText = new javax.swing.JTextField();
        godownText = new javax.swing.JTextField();
        billDateFormattedText = new com.toedter.calendar.JDateChooser();
        destinationText = new javax.swing.JTextField();
        qtyToMovePanel = new javax.swing.JPanel();
        qtyToMoveText = new javax.swing.JTextField();
        qtyToMoveText1 = new javax.swing.JTextField();
        buttonPanel = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();

        setClosable(true);
        setTitle("Stock Move");

        mainPanel1.setBackground(new java.awt.Color(255, 255, 255));

        inputPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        inputPanel1.setOpaque(false);

        jLabel2.setText("Item Code");

        jLabel11.setText("Item Description");

        jLabel12.setText("Available Quantity");

        jLabel13.setText("Destination Godown");

        jLabel14.setText("Current Godown");

        jLabel16.setText("Quantity to move");

        jLabel17.setText("Date");

        itemCodeText.setToolTipText("Press Enter");
        itemCodeText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemCodeTextActionPerformed(evt);
            }
        });
        itemCodeText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemCodeTextKeyPressed(evt);
            }
        });

        availQtyPanel.setLayout(new java.awt.GridLayout(1, 0));

        itemAvailQtyText.setEditable(false);
        itemAvailQtyText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemAvailQtyText.setText("0");
        itemAvailQtyText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemAvailQtyTextFocusGained(evt);
            }
        });
        itemAvailQtyText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemAvailQtyTextKeyTyped(evt);
            }
        });
        availQtyPanel.add(itemAvailQtyText);

        itemAvailQtyText1.setEditable(false);
        itemAvailQtyText1.setText("0.00");
        availQtyPanel.add(itemAvailQtyText1);

        itemDescriptionText.setToolTipText("Press Enter");
        itemDescriptionText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemDescriptionTextActionPerformed(evt);
            }
        });
        itemDescriptionText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemDescriptionTextKeyPressed(evt);
            }
        });

        godownText.setEditable(false);

        billDateFormattedText.setDateFormatString("dd/MM/yyyy");

        destinationText.setToolTipText("Press Enter");
        destinationText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                destinationTextKeyPressed(evt);
            }
        });

        qtyToMovePanel.setLayout(new java.awt.GridLayout(1, 0));

        qtyToMoveText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        qtyToMoveText.setText("0");
        qtyToMoveText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                qtyToMoveTextFocusGained(evt);
            }
        });
        qtyToMoveText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                qtyToMoveTextKeyTyped(evt);
            }
        });
        qtyToMovePanel.add(qtyToMoveText);

        qtyToMoveText1.setText("0.00");
        qtyToMovePanel.add(qtyToMoveText1);

        javax.swing.GroupLayout inputPanel1Layout = new javax.swing.GroupLayout(inputPanel1);
        inputPanel1.setLayout(inputPanel1Layout);
        inputPanel1Layout.setHorizontalGroup(
            inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel16)
                    .addComponent(jLabel12)
                    .addComponent(jLabel17)
                    .addComponent(jLabel2))
                .addGap(30, 30, 30)
                .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(inputPanel1Layout.createSequentialGroup()
                        .addComponent(itemCodeText, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(61, 61, 61)
                        .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13)
                            .addGroup(inputPanel1Layout.createSequentialGroup()
                                .addGap(16, 16, 16)
                                .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel11)
                                    .addComponent(jLabel14))))
                        .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(inputPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                                .addComponent(jLabel15)
                                .addGap(218, 218, 218))
                            .addGroup(inputPanel1Layout.createSequentialGroup()
                                .addGap(54, 54, 54)
                                .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(godownText, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
                                    .addComponent(itemDescriptionText, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
                                    .addComponent(destinationText))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(inputPanel1Layout.createSequentialGroup()
                        .addComponent(billDateFormattedText, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inputPanel1Layout.createSequentialGroup()
                    .addContainerGap(125, Short.MAX_VALUE)
                    .addComponent(availQtyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(400, Short.MAX_VALUE)))
            .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(inputPanel1Layout.createSequentialGroup()
                    .addGap(124, 124, 124)
                    .addComponent(qtyToMovePanel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(401, Short.MAX_VALUE)))
        );
        inputPanel1Layout.setVerticalGroup(
            inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(billDateFormattedText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(itemDescriptionText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(itemCodeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(inputPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(inputPanel1Layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(godownText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                        .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16)
                            .addComponent(jLabel13)
                            .addComponent(destinationText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31))))
            .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inputPanel1Layout.createSequentialGroup()
                    .addContainerGap(72, Short.MAX_VALUE)
                    .addComponent(availQtyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(73, Short.MAX_VALUE)))
            .addGroup(inputPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inputPanel1Layout.createSequentialGroup()
                    .addContainerGap(113, Short.MAX_VALUE)
                    .addComponent(qtyToMovePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(32, 32, 32)))
        );

        buttonPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new java.awt.GridLayout(1, 0));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("New");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(newButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(saveButton);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/cancel.png"))); // NOI18N
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(cancelButton);

        javax.swing.GroupLayout mainPanel1Layout = new javax.swing.GroupLayout(mainPanel1);
        mainPanel1.setLayout(mainPanel1Layout);
        mainPanel1Layout.setHorizontalGroup(
            mainPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(inputPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        mainPanel1Layout.setVerticalGroup(
            mainPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanel1Layout.createSequentialGroup()
                .addComponent(inputPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 684, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mainPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 278, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(mainPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    void clearAllFields() {
        try {
            
          CommonService.clearTextFields(new JTextField[]{itemCodeText, itemDescriptionText,itemAvailQtyText,itemAvailQtyText1,godownText,qtyToMoveText,qtyToMoveText1,destinationText});
          itemCodeText.setEditable(true);
          itemDescriptionText.setEditable(true);

        } catch (Exception e) {
            log.error("clearAllFields:", e);
        }
    }
    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        clearAllFields();
    }//GEN-LAST:event_newButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        try {
              if(destinationText.getText().toString().equalsIgnoreCase(godownText.getText()))
              {      
                MsgBox.warning("Cannot Add items to same godown....!!");
              }
              else{
              String itemCode=itemCodeText.getText();
              String itemDesc=itemDescriptionText.getText();
              toItem=itemDAO.findItemByItemCode(itemCode, itemDesc, destinationText.getText());
              if (MsgBox.confirm("Are you sure you want to save?")) {
                toItem.setAvailableQty(toItem.getAvailableQty() + Double.parseDouble(qtyToMoveText.getText()));
                toItem.setAvailableQty1(toItem.getAvailableQty1() + Double.parseDouble(qtyToMoveText1.getText()));
                cRUDServices.saveOrUpdateModel(toItem);
                fromItem.setAvailableQty(fromItem.getAvailableQty() - Double.parseDouble(qtyToMoveText.getText()));
                fromItem.setAvailableQty1(fromItem.getAvailableQty1() - Double.parseDouble(qtyToMoveText1.getText()));
                cRUDServices.saveOrUpdateModel(fromItem);
                stockMove.setName(itemDesc);
                stockMove.setAvailableQty(fromItem.getAvailableQty());
                stockMove.setAvailableQty1(fromItem.getAvailableQty1());
                stockMove.setItemCode(itemCode);
                stockMove.setQtyToMove(Double.parseDouble(qtyToMoveText.getText())); 
                stockMove.setQtyToMove1(Double.parseDouble(qtyToMoveText1.getText())); 
                stockMove.setFromGodown(godownText.getText());
                stockMove.setToGodown(destinationText.getText().toString());
                stockMove.setCompany(GlobalProperty.getCompany()); 
                stockMove.setDate(commonService.utilDateToSqlDate(billDateFormattedText.getDate()));   
                cRUDServices.saveOrUpdateModel(stockMove); 
                MsgBox.success("Items moved Successfully");
               }
             }
              clearAllFields();
         }
        catch (Exception ex) {
            ex.printStackTrace();
            log.error(ex);
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void itemDescriptionTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemDescriptionTextKeyPressed
        try {
            ItemService itemService = new ItemService();
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                String proName = itemDescriptionText.getText();
                String proId = itemCodeText.getText();
                popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Weight", "Unit", "Rate", "Available Quantity", "Godown",""});
                List<Item> items = itemService.populatePurchasePopupTableByItemName(proName, popupTableDialog, purchaseItemList, currency,false, purchaseItemListHistory);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();

                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    fromItem = items.get(index);
                    itemCodeText.setText(fromItem.getItemCode());
                    itemDescriptionText.setText(fromItem.getDescription());
                    itemCodeText.setEditable(false);
                    itemDescriptionText.setEditable(false); 
                    itemAvailQtyText.setText(fromItem.getAvailableQty() + "");
                    godownText.setText(fromItem.getGodown().getName());
                } else {
                    MsgBox.warning("No Item Selected....");
                    itemAvailQtyText.requestFocusInWindow();
                }
            }
        } catch (Exception e) {
            log.error("productIdTextKeyPressed:", e);
        }
    }//GEN-LAST:event_itemDescriptionTextKeyPressed

    private void itemDescriptionTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemDescriptionTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemDescriptionTextActionPerformed

    private void itemCodeTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemCodeTextKeyPressed
        try {
            ItemService itemService = new ItemService();
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                String productId = itemCodeText.getText();
                popupTableDialog.setTitle(new String[]{"Sl No.", "Item Code", "Description", "Weight", "Unit", "Rate", "Available Quantity", "Godown" ,"" });
                List<Item> items = itemService.populatePurchasePopupTableByProductId(productId, popupTableDialog, purchaseItemList, currency,false, purchaseItemListHistory);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();

                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    fromItem = items.get(index);
                    itemCodeText.setText(fromItem.getItemCode());
                    itemCodeText.setEditable(false); 
                    itemDescriptionText.setText(fromItem.getDescription());
                    itemDescriptionText.setEditable(false); 
                    itemAvailQtyText.setText(fromItem.getAvailableQty().toString());
                    itemAvailQtyText1.setText(fromItem.getAvailableQty1().toString()); 
                    godownText.setText(fromItem.getGodown().getName());

                } else {
                    MsgBox.warning("No Item Selected.");
                    itemAvailQtyText.requestFocusInWindow();
                }
            }
        } catch (Exception e) {
            log.error("productIdTextKeyPressed:", e);
        }
    }//GEN-LAST:event_itemCodeTextKeyPressed

    private void itemCodeTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemCodeTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemCodeTextActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
         this.hide();
         //CommonService.removeJpanel(mainForm.getContentPanel()); 
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void destinationTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_destinationTextKeyPressed
        try
        {  
            ItemService itemService = new ItemService();
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                String productId = itemCodeText.getText();
                String proName=itemDescriptionText.getText();
               // popupTableDialog.setTitle(new String[]{"Sl No.", "Godown Name","" });
                List<Godown> items=itemService.populateStockMoveViewByGodownList(destinationText.getText(),popupTableDialog);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();

                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    godown=items.get(index);
                    destinationText.setText(godown.getName());

                } else {
                    MsgBox.warning("No Item Selected.");
                    itemAvailQtyText.requestFocusInWindow();
                }
            }
        } 
        catch (Exception e) {
            log.error("productIdTextKeyPressed:", e);
        }
    }//GEN-LAST:event_destinationTextKeyPressed

    private void itemAvailQtyTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemAvailQtyTextFocusGained
        itemAvailQtyText.selectAll();
    }//GEN-LAST:event_itemAvailQtyTextFocusGained

    private void itemAvailQtyTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemAvailQtyTextKeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_itemAvailQtyTextKeyTyped

    private void qtyToMoveTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_qtyToMoveTextFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_qtyToMoveTextFocusGained

    private void qtyToMoveTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_qtyToMoveTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_qtyToMoveTextKeyTyped
 public void listStockMoveHistory(StockMovement stockMove)
 {
   saveButton.setVisible(false);
   buttonPanel.setVisible(false); 
   billDateFormattedText.setEnabled(false);
   itemCodeText.setEditable(false); 
   qtyToMoveText.setEditable(false);
   qtyToMoveText1.setEditable(false);
   destinationText.setEditable(false); 
   itemDescriptionText.setEditable(false); 
   this.stockMove=stockMove;
   billDateFormattedText.setDate(stockMove.getDate()); 
   itemCodeText.setText(stockMove.getItemCode());
   itemDescriptionText.setText(stockMove.getName());
   itemAvailQtyText.setText(stockMove.getAvailableQty()+"");  
   itemAvailQtyText1.setText(stockMove.getAvailableQty1()+"");
   qtyToMoveText.setText(stockMove.getQtyToMove()+""); 
   qtyToMoveText1.setText(stockMove.getQtyToMove1()+"");
   godownText.setText(stockMove.getFromGodown());
   destinationText.setText(stockMove.getToGodown());
 }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel availQtyPanel;
    private com.toedter.calendar.JDateChooser billDateFormattedText;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField destinationText;
    private javax.swing.JTextField godownText;
    private javax.swing.JPanel inputPanel1;
    private javax.swing.JTextField itemAvailQtyText;
    private javax.swing.JTextField itemAvailQtyText1;
    private javax.swing.JTextField itemCodeText;
    private javax.swing.JTextField itemDescriptionText;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel mainPanel1;
    private javax.swing.JButton newButton;
    private javax.swing.JPanel qtyToMovePanel;
    private javax.swing.JTextField qtyToMoveText;
    private javax.swing.JTextField qtyToMoveText1;
    private javax.swing.JButton saveButton;
    // End of variables declaration//GEN-END:variables
}
