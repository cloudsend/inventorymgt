package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.StockGroupDAO;
import com.cloudsendsoft.inventory.services.ChargesService;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.services.StockGroupService;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class StockGroupView extends javax.swing.JInternalFrame {

    /**
     * Creates new form AddItem
     */
    static final Logger log = Logger.getLogger(StockGroupView.class.getName());
    CRUDServices cRUDServices = new CRUDServices();
    ;
    StockGroupDAO stockGroupDAO = new StockGroupDAO();
    StockGroupService stockGroupService = new StockGroupService();
    CommonService commonService = new CommonService();
    StockGroup stockGroup = null;

    public StockGroupView() {
        initComponents();

        //delete button temporarily hiding because of foriehn key issue
        buttonPanel.remove(deleteButton);
        
        if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
            mainPanel.remove(buttonPanel);
            mainPanel.repaint();
            mainPanel.revalidate();
        }

        this.getRootPane().setDefaultButton(addButton);
        KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);

        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);

        stockGroupListTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{
                    "Stock Group","Under"
                }
        ));
        stockGroupListTable.setRowHeight(GlobalProperty.tableRowHieght);

        stockGroupService.fillStockGroupListTable(stockGroupListTable);
        
        for (StockGroup stockGroup : stockGroupDAO.findAll()) {
            groupCombo.addItem(new ComboKeyValue((stockGroup.getName()), stockGroup));
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        stockGroupListTable = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        groupNameText = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        groupCombo = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        buttonPanel = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();
        updateButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();

        setClosable(true);
        setTitle("Stock Group");
        getContentPane().setLayout(new java.awt.CardLayout());

        mainPanel.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("List Of Stock Group"));
        jPanel2.setOpaque(false);

        stockGroupListTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        stockGroupListTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stockGroupListTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(stockGroupListTable);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 527, Short.MAX_VALUE)
                .addGap(1, 1, 1))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 421, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel4.setOpaque(false);

        jLabel1.setText("Group Name");

        groupCombo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                groupComboMouseClicked(evt);
            }
        });

        jLabel2.setText("Under");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(groupCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(groupNameText, javax.swing.GroupLayout.DEFAULT_SIZE, 439, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(groupNameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(groupCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buttonPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new java.awt.GridLayout(1, 0));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("New");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(newButton);

        addButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        addButton.setText("Save");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(addButton);

        updateButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/update.png"))); // NOI18N
        updateButton.setText("Update");
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(updateButton);

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/delete-icon.png"))); // NOI18N
        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(deleteButton);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(mainPanel, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        try {
            String groupName = groupNameText.getText().trim();
            stockGroup = stockGroupDAO.findByName(groupName);
            if (null == stockGroup) {
                if (CommonService.stringValidator(new String[]{groupName})) {
                    if (MsgBox.confirm("Are you sure you want to save?")) {
                        
                        Object stockUnder = groupCombo.getSelectedItem();
                        StockGroup stockGroupUnder = (StockGroup) ((ComboKeyValue) stockUnder).getValue();
                        
                        stockGroup = new StockGroup();
                        stockGroup.setName(groupName);
                        stockGroup.setCompany(GlobalProperty.getCompany());
                        stockGroup.setUnderGroup(stockGroupUnder);
                        cRUDServices.saveModel(stockGroup);
                        MsgBox.success("Successfully Added " + groupName);
                        CommonService.clearTextFields(new JTextField[]{groupNameText});
                        stockGroupService.fillStockGroupListTable(stockGroupListTable);
                        groupNameText.requestFocusInWindow();
                    }
                } else {
                    MsgBox.warning("StockGroup Name is mandatory.");
                    groupNameText.requestFocusInWindow();
                }
            } else {
                MsgBox.warning("StockGroup Name '" + groupName + "' is already exists.");
                groupNameText.requestFocusInWindow();
            }

        } catch (Exception e) {
            log.error("addButtonActionPerformed:", e);
        }

    }//GEN-LAST:event_addButtonActionPerformed

    private void stockGroupListTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stockGroupListTableMouseClicked
        try {
            ArrayList selectedRow = commonService.getTableRowData(stockGroupListTable);
            stockGroup = stockGroupDAO.findByName(selectedRow.get(0) + "");
            groupNameText.setText(stockGroup.getName());
            for (int i = 0; i < stockGroupListTable.getRowCount(); i++) {
                    ComboKeyValue ckv = (ComboKeyValue) groupCombo.getItemAt(i);
                    if (ckv.getKey().trim().equalsIgnoreCase(stockGroup.getUnderGroup().getName())) {
                        groupCombo.setSelectedItem(ckv);
                        break;
                    }
                }
        } catch (Exception e) {
            log.error("StockGroupListTableMouseClicked", e);
        }
    }//GEN-LAST:event_stockGroupListTableMouseClicked

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
        try {
            String groupName = groupNameText.getText().trim();
            if (null != stockGroup) {
                if (CommonService.stringValidator(new String[]{groupName})) {
                    if (MsgBox.confirm("Are you sure you want to update?")) {
                        stockGroup.setName(groupName);
                        
                        Object stockUnder = groupCombo.getSelectedItem();
                        StockGroup stockGroupUnder = (StockGroup) ((ComboKeyValue) stockUnder).getValue();
                        stockGroup.setUnderGroup(stockGroupUnder);
                        
                        cRUDServices.saveOrUpdateModel(stockGroup);
                        MsgBox.success("Successfully Updated " + groupName);
                        CommonService.clearTextFields(new JTextField[]{groupNameText});
                        stockGroupService.fillStockGroupListTable(stockGroupListTable);
                        groupNameText.requestFocusInWindow();
                    }
                } else {
                    MsgBox.warning("StockGroup Name is mandatory.");
                    groupNameText.requestFocusInWindow();
                }
                stockGroup = null;
            } else {
                MsgBox.warning("StockGroup Name '" + groupName + "' Not Found.");
                groupNameText.requestFocusInWindow();
            }

        } catch (Exception e) {
            log.error("updateButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_updateButtonActionPerformed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        CommonService.clearTextFields(new JTextField[]{groupNameText});
    }//GEN-LAST:event_newButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        Session session = null;
        try {
            String groupName = groupNameText.getText().trim();
            if (null != stockGroup) {
                if (!stockGroup.getName().equalsIgnoreCase("General")) {
                    if (MsgBox.confirm("Are you sure you want to delete?")) {
                        try {
                            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
                            session = sessionFactory.openSession();
                            Transaction transaction = session.beginTransaction();
                            session.delete(stockGroup);
                            transaction.commit();

                            MsgBox.success("Successfully Deleted " + groupName);
                            CommonService.clearTextFields(new JTextField[]{groupNameText});
                            stockGroupService.fillStockGroupListTable(stockGroupListTable);
                            groupNameText.requestFocusInWindow();
                            stockGroup = null;
                        } catch (Exception e) {
                            MsgBox.warning("Stock Group '" + groupName + "' is already used for transactions!");
                            groupNameText.requestFocusInWindow();
                        } finally {
                            session.clear();
                            session.close();
                        }
                    }
                } else {
                    MsgBox.warning("You can't delete default Stock Group");
                }
            } else {
                MsgBox.warning("StockGroup Name '" + groupName + "' Not Found.");
                groupNameText.requestFocusInWindow();
            }
        } catch (Exception e) {
            log.error("deleteButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void groupComboMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_groupComboMouseClicked
        groupCombo.removeAllItems();
        for (StockGroup stockGroup : stockGroupDAO.findAll()) {
            groupCombo.addItem(new ComboKeyValue((stockGroup.getName()), stockGroup));
        }
    }//GEN-LAST:event_groupComboMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton deleteButton;
    private javax.swing.JComboBox groupCombo;
    private javax.swing.JTextField groupNameText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton newButton;
    private javax.swing.JTable stockGroupListTable;
    private javax.swing.JButton updateButton;
    // End of variables declaration//GEN-END:variables
}
