/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.LedgerGroupDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.Receipt;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.LedgerService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.util.List;
import java.util.logging.Level;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author LOGON SOFT 1
 */
public class IncomeView extends javax.swing.JInternalFrame {

    /**
     * Creates new form IncomeView
     */
      static final Logger log = Logger.getLogger(ExpensesView.class.getName());
    LedgerService ledgerService = new LedgerService();
    Ledger ledger = null;
    LedgerDAO ledgerDAO = new LedgerDAO();
    LedgerGroupDAO ledgerGroupDAO = new LedgerGroupDAO(); 
    CRUDServices cRUDServices = null;
    List<Ledger> ledgerList = null;
    CommonService commonService = new CommonService();
    NumberProperty numberProperty = null;
    NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
    Receipt receipt = null;
    int check=0;
    int delRow =-1;
    public IncomeView() {
        initComponents();
        incomeTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(null, new String[]{"Income","Amount","By","Details","Date"}));
        for (Ledger ledgerItem : ledgerDAO.findAllIncomes()) 
        {
            incomeComboBox.addItem(new ComboKeyValue((ledgerItem.getLedgerName()), ledgerItem));
        }
        for (Ledger ledgerItem : ledgerDAO.findAllCustomerPaymentTo())
        {
            byComboBox.addItem(ledgerItem.getLedgerName());
        }
        cRUDServices = new CRUDServices();
        incomeLabel.setVisible(false);
        incomeLabel.setText("Receipt");
        numberProperty = numberPropertyDAO.findInvoiceNo("Receipt");
        incomeLabel.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
        commonService.setCurrentPeriodOnCalendar(incomeDate);
        incomeDate.setDate(new java.util.Date());
        
        //Set combo Box Selected
        SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        incomeComboBox.requestFocus();
      }
        });
        //save shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "save");
            this.getActionMap().put("save", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveButtonActionPerformed(e);
                }
            });
            //Add Short cut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "add");
            this.getActionMap().put("add", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addButtonActionPerformed(e);
                }
            });
            //Delete short cut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_D,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "delete");
            this.getActionMap().put("delete", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    deleteButtonActionPerformed(e);
                }
            });
            //cancel short cut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_W,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "cancel");
            this.getActionMap().put("cancel", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    cancelButtonActionPerformed(e);
                }
            });
            setFocusOrder();
    }
    void setFocusOrder()
    {
         incomeComboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    amountText.requestFocusInWindow();
                }
            });
          amountText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    byComboBox.requestFocusInWindow();
                }
            });
           byComboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(chequeText.isVisible())
                        chequeText.requestFocusInWindow();
                    else
                        addButton.requestFocusInWindow();
                }
            });
           addButton.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e){
                    saveButton.requestFocusInWindow();
                }   
           });
    
    
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        headerPanel = new javax.swing.JPanel();
        detailsPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        amountText = new javax.swing.JTextField();
        incomeComboBox = new javax.swing.JComboBox();
        incomeLabel = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        incomeDate = new com.toedter.calendar.JDateChooser();
        jLabel4 = new javax.swing.JLabel();
        byComboBox = new javax.swing.JComboBox();
        chequeLabel = new javax.swing.JLabel();
        chequeText = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        buttonPanel = new javax.swing.JPanel();
        addButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        bottomButtonPanel = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        tablePanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        incomeTable = new javax.swing.JTable();

        setClosable(true);
        setTitle("Income");
        setToolTipText("");
        getContentPane().setLayout(new java.awt.CardLayout());

        mainPanel.setBackground(new java.awt.Color(255, 255, 255));
        mainPanel.setLayout(new java.awt.BorderLayout());

        headerPanel.setBackground(new java.awt.Color(255, 255, 255));
        headerPanel.setPreferredSize(new java.awt.Dimension(510, 190));

        detailsPanel.setBackground(new java.awt.Color(255, 255, 255));
        detailsPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Income");

        jLabel2.setText("Amount");

        amountText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                amountTextKeyTyped(evt);
            }
        });

        incomeLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        incomeLabel.setText("Label");

        jLabel3.setText("Date");

        incomeDate.setDateFormatString("dd/MM/yyyy");

        jLabel4.setText("By");

        byComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                byComboBoxItemStateChanged(evt);
            }
        });

        chequeLabel.setText("Cheque Number");

        chequeText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                chequeTextKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout detailsPanelLayout = new javax.swing.GroupLayout(detailsPanel);
        detailsPanel.setLayout(detailsPanelLayout);
        detailsPanelLayout.setHorizontalGroup(
            detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(detailsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(incomeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(amountText, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, detailsPanelLayout.createSequentialGroup()
                            .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel4)
                                .addComponent(chequeLabel))
                            .addGap(64, 64, 64)
                            .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(chequeText, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(byComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(detailsPanelLayout.createSequentialGroup()
                        .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(incomeComboBox, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(incomeDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(57, Short.MAX_VALUE))
        );
        detailsPanelLayout.setVerticalGroup(
            detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, detailsPanelLayout.createSequentialGroup()
                .addComponent(incomeLabel)
                .addGap(16, 16, 16)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addComponent(incomeDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(incomeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(amountText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(byComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(detailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chequeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chequeLabel))
                .addGap(16, 16, 16))
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        buttonPanel.setBackground(new java.awt.Color(255, 255, 255));

        addButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        addButton.setText("Add");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/delete-icon.png"))); // NOI18N
        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout buttonPanelLayout = new javax.swing.GroupLayout(buttonPanel);
        buttonPanel.setLayout(buttonPanelLayout);
        buttonPanelLayout.setHorizontalGroup(
            buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(buttonPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(deleteButton, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(0, 0, 0))
        );
        buttonPanelLayout.setVerticalGroup(
            buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(buttonPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addButton, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(deleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout headerPanelLayout = new javax.swing.GroupLayout(headerPanel);
        headerPanel.setLayout(headerPanelLayout);
        headerPanelLayout.setHorizontalGroup(
            headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headerPanelLayout.createSequentialGroup()
                .addGap(275, 275, 275)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(headerPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(detailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );
        headerPanelLayout.setVerticalGroup(
            headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headerPanelLayout.createSequentialGroup()
                .addGroup(headerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(headerPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(detailsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(headerPanelLayout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49))
        );

        mainPanel.add(headerPanel, java.awt.BorderLayout.PAGE_START);

        bottomButtonPanel.setBackground(new java.awt.Color(255, 255, 255));

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.setLayout(new java.awt.GridLayout(1, 0));

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        jPanel6.add(saveButton);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/cancel.png"))); // NOI18N
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        jPanel6.add(cancelButton);

        javax.swing.GroupLayout bottomButtonPanelLayout = new javax.swing.GroupLayout(bottomButtonPanel);
        bottomButtonPanel.setLayout(bottomButtonPanelLayout);
        bottomButtonPanelLayout.setHorizontalGroup(
            bottomButtonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bottomButtonPanelLayout.createSequentialGroup()
                .addGap(152, 152, 152)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        bottomButtonPanelLayout.setVerticalGroup(
            bottomButtonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bottomButtonPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        mainPanel.add(bottomButtonPanel, java.awt.BorderLayout.PAGE_END);

        tablePanel.setBackground(new java.awt.Color(255, 255, 255));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));

        incomeTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        incomeTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                incomeTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(incomeTable);

        javax.swing.GroupLayout tablePanelLayout = new javax.swing.GroupLayout(tablePanel);
        tablePanel.setLayout(tablePanelLayout);
        tablePanelLayout.setHorizontalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 515, Short.MAX_VALUE)
        );
        tablePanelLayout.setVerticalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 389, Short.MAX_VALUE)
        );

        mainPanel.add(tablePanel, java.awt.BorderLayout.CENTER);

        getContentPane().add(mainPanel, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        if(amountText.getText().equals("")||incomeDate.getDate()==null)
        {
             MsgBox.warning("Please fill in required fields");
        }
        else 
        {
            
            String incomeName = incomeComboBox.getSelectedItem().toString();
            double amount = Double.parseDouble(amountText.getText());
            String byName = byComboBox.getSelectedItem().toString();
            Date inDate = commonService.utilDateToSqlDate(incomeDate.getDate());
            String cheque = chequeText.getText();
            int test =0;
            for(int i= 0; i<incomeTable.getRowCount();i++){
                if(incomeName==incomeTable.getValueAt(i, 0))
                {
                    test=1;
                }
            }
            if(test==0){
                fillIncomeTable(incomeTable,incomeName ,amount,byName,cheque,inDate, incomeTable.getRowCount());
            }
            else
            {
                MsgBox.warning("Already exists");
            }
        }

    }//GEN-LAST:event_addButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        if(delRow!=-1)
        {
            deleteIncomeTable(incomeTable, delRow);
            delRow=-1;
        } else {
            MsgBox.warning("Select any row to delete!!!");
        }
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        if(incomeTable.getRowCount()==0)
        {
            MsgBox.warning("No Incomes to Save!!!");
        }
        else
        {
            Ledger byLedger = null;
            Ledger toLedger = null;
             for(int i= 0; i<incomeTable.getRowCount();i++)
            {
                try {
                    byLedger = ledgerDAO.findByLedgerName((String) incomeTable.getValueAt(i, 2));
                    toLedger = ledgerDAO.findByLedgerName((String) incomeTable.getValueAt(i, 0));
                    
                    if (receipt == null) 
                    {
                        receipt = new Receipt();
                        check = 0;
                    }
                    receipt.setReceiptDate(commonService.utilDateToSqlDate(incomeDate.getDate()));
                    incomeLabel.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
                    receipt.setReceiptNo(incomeLabel.getText());
                    receipt.setAmount((Double) incomeTable.getValueAt(i, 1));
                    receipt.setNarration((String) incomeTable.getValueAt(i,3));
                    receipt.setLedgerBy(byLedger);
                    receipt.setLedgerTo(toLedger);
                    receipt.setCompany(GlobalProperty.getCompany());
                    if (check == 1) {
                        try {
                            cRUDServices.saveOrUpdateModel(receipt);
                        } catch (Exception ex) {
                            java.util.logging.Logger.getLogger(IncomeView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        try {
                            cRUDServices.saveModel(receipt);
                        } catch (Exception ex) {
                            java.util.logging.Logger.getLogger(IncomeView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    if (byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Liability") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Direct Income") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Indirect Income") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Capital")) {
                        try {
                            byLedger.setAvailableBalance(byLedger.getAvailableBalance() - (Double) incomeTable.getValueAt(i, 1));
                            cRUDServices.saveOrUpdateModel(byLedger);
                        } catch (Exception ex) {
                            java.util.logging.Logger.getLogger(IncomeView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        try {
                            byLedger.setAvailableBalance(byLedger.getAvailableBalance() + (Double) incomeTable.getValueAt(i, 1));
                            cRUDServices.saveOrUpdateModel(byLedger);
                        } catch (Exception ex) {
                            java.util.logging.Logger.getLogger(IncomeView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    if (toLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Asset") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Direct Expense") || byLedger.getLedgerGroup().getAccountType().trim().equalsIgnoreCase("Indirect Expense")) {
                        try {
                            toLedger.setAvailableBalance(toLedger.getAvailableBalance() - (Double) incomeTable.getValueAt(i, 1));
                            cRUDServices.saveOrUpdateModel(toLedger);
                        } catch (Exception ex) {
                            java.util.logging.Logger.getLogger(IncomeView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        try {
                            toLedger.setAvailableBalance(toLedger.getAvailableBalance() + (Double) incomeTable.getValueAt(i, 1));
                            cRUDServices.saveOrUpdateModel(toLedger);
                        } catch (Exception ex) {
                            java.util.logging.Logger.getLogger(IncomeView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    numberProperty.setNumber(numberProperty.getNumber() + 1);
                    cRUDServices.saveOrUpdateModel(numberProperty);
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(IncomeView.class.getName()).log(Level.SEVERE, null, ex);
                }
                   
        }
                    MsgBox.success("Income(s) Saved Successfully");
                    clearAllFields();
                    this.dispose();
        }

    }//GEN-LAST:event_saveButtonActionPerformed
    public void clearAllFields(){
            incomeDate.setDate(new java.util.Date());
            commonService.clearCurrencyFields(new JTextField[]{amountText});
            chequeText.setText("");
            numberProperty = numberPropertyDAO.findInvoiceNo("Receipt");
            incomeLabel.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
            
    }      
    
    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void incomeTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_incomeTableMouseClicked
       delRow = incomeTable.getSelectedRow();
    }//GEN-LAST:event_incomeTableMouseClicked

    private void byComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_byComboBoxItemStateChanged
        //if("Cheque".equalsIgnoreCase((String)byComboBox.getSelectedItem()))
        Ledger selectedMode = new Ledger();
        selectedMode=ledgerDAO.findByLedgerName((String)byComboBox.getSelectedItem()) ;
        if(selectedMode.getLedgerGroup().getGroupName().equalsIgnoreCase("Bank Accounts"))   
        {
            chequeLabel.setVisible(true);
            chequeText.setVisible(true);
        }
        else{
            chequeLabel.setVisible(false);
            chequeText.setVisible(false);
        }
    }//GEN-LAST:event_byComboBoxItemStateChanged

    private void amountTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_amountTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_amountTextKeyTyped

    private void chequeTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chequeTextKeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_chequeTextKeyTyped
      public void fillIncomeTable(javax.swing.JTable expensesTable , String expenseName , double amount ,String byName , String cheque,Date inDate, int row){
        ((DefaultTableModel) incomeTable.getModel()).insertRow(row, new Object [] {expenseName, amount,byName,cheque,inDate});
    }
       public void deleteIncomeTable(javax.swing.JTable expensesTable , int row){
        ((DefaultTableModel) incomeTable.getModel()).removeRow(row);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JTextField amountText;
    private javax.swing.JPanel bottomButtonPanel;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JComboBox byComboBox;
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel chequeLabel;
    private javax.swing.JTextField chequeText;
    private javax.swing.JButton deleteButton;
    private javax.swing.JPanel detailsPanel;
    private javax.swing.JPanel headerPanel;
    private javax.swing.JComboBox incomeComboBox;
    private com.toedter.calendar.JDateChooser incomeDate;
    private javax.swing.JLabel incomeLabel;
    private javax.swing.JTable incomeTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton saveButton;
    private javax.swing.JPanel tablePanel;
    // End of variables declaration//GEN-END:variables
}
