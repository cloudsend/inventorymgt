/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.GodownDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.MultiCurrencyDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.dao.PurchaseDAO;
import com.cloudsendsoft.inventory.dao.ShipmentModeDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.ItemService;
import com.cloudsendsoft.inventory.services.LedgerService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Godown;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseCharge;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseTax;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class PurchaseView extends javax.swing.JPanel {

    /**
     * Creates new form PurchaseViewPanel
     */
    static double totalAmount = 0;
    double subTotal = 0.00;
    double grandTotal = 0.00;
    double netTotal = 0.00;
    double totDiscount = 0.00;

    double subTotal1 = 0.00;
    double grandTotal1 = 0.00;
    double netTotal1 = 0.00;
    double totDiscount1 = 0.00;

    static final Logger log = Logger.getLogger(PurchaseView.class.getName());
    Ledger ledger = null;

    ItemDAO itemDAO = new ItemDAO();
    CommonService commonService = new CommonService();
    MainForm mainForm = null;
    LedgerService ledgerService = new LedgerService();
    TaxDAO taxDAO = new TaxDAO();
    GodownDAO godownDAO = new GodownDAO();
    ChargesDAO chargesDAO = new ChargesDAO();
    MultiCurrencyDAO multiCurrencyDAO = new MultiCurrencyDAO();
    ShipmentModeDAO shipmentModeDAO = new ShipmentModeDAO();
    PurchaseDAO purchaseDAO = new PurchaseDAO();
    NumberProperty numberProperty = null;
    NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
    //global usage
    Ledger supplierLedger = null;
    double itemQty = 0;
    List<PurchaseItem> purchaseItemList = new ArrayList<PurchaseItem>();
     List<PurchaseItem> purchaseItemRemovedList = new ArrayList<PurchaseItem>();
    List<PurchaseItem> purchaseItemListHistory = null;
    Item item = null;
    List<PurchaseTax> purchaseTaxList = new ArrayList<PurchaseTax>();
    List<PurchaseCharge> purchaseChargesList = new ArrayList<PurchaseCharge>();

    MultiCurrency currency = null;
    Purchase purchase = null;
    int check = 0;

    PurchaseItem purchaseItem = null;
    DefaultTableModel purchaseItemTableModel = null;

    LedgerDAO ledgerDAO = new LedgerDAO();

    CRUDServices cRUDServices = new CRUDServices();

    //Double previousPurchaseAccount=0.00;
    //Double previousDiscountReceived=0.00;
    Ledger purchaseLedger = null;
    Ledger discountLedger = null;
    List<Ledger> taxLedgerList = new ArrayList<Ledger>();
    List<Ledger> chargesLedgerList = new ArrayList<Ledger>();

    boolean isDuplicateColumnsVisible = false;

    public PurchaseView(MainForm mainForm) {

        try {
            initComponents();

            //shortcut
            // HERE ARE THE KEY BINDINGS
            //removed additional qty & rate textboxes by default
            qtyPanel.remove(itemQtyText1);
            unitPricePanel.remove(itemUnitPriceText1);
            chargesPanel1.remove(chargesText1);
            discountPanel.remove(discountText1);

            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_W,
                                    java.awt.event.InputEvent.SHIFT_DOWN_MASK
                                    | java.awt.event.InputEvent.CTRL_DOWN_MASK), "forward");
            this.getActionMap().put("forward", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (itemQtyText1.getParent() == qtyPanel) {
                        qtyPanel.remove(itemQtyText1);
                        qtyPanel.revalidate();
                        unitPricePanel.remove(itemUnitPriceText1);
                        unitPricePanel.revalidate();
                        chargesPanel1.remove(chargesText1);
                        chargesPanel1.revalidate();
                        discountPanel.remove(discountText1);
                        discountPanel.revalidate();
                        isDuplicateColumnsVisible = false;

                        //set table design
                        purchaseItemTable.setRowHeight(GlobalProperty.tableRowHieght);
                        purchaseItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                                null,
                                new String[]{"Sr. No.", "Item Code", "Description", "Godown", "Qty.", "Unit Price", "Total Amount"}
                        ));
                        purchaseItemTableModel = (DefaultTableModel) purchaseItemTable.getModel();
                        CommonService.setWidthAsPercentages(purchaseItemTable, .03, .10, .20, .08, .08, .08, .08);
                        
                        fillPurchaseItemTable();
                    } else {
                        qtyPanel.add(itemQtyText1);
                        qtyPanel.revalidate();
                        unitPricePanel.add(itemUnitPriceText1);
                        unitPricePanel.revalidate();
                        chargesPanel1.add(chargesText1);
                        chargesPanel1.revalidate();
                        discountPanel.add(discountText1);
                        discountPanel.revalidate();
                        isDuplicateColumnsVisible = true;

                        //set table design
                        purchaseItemTable.setRowHeight(GlobalProperty.tableRowHieght);
                        purchaseItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                                null,
                                new String[]{"Sr. No.", "Item Code", "Description", "Godown", "Qty.", "Unit Price", "Qty1", "Unit Price1", "Total Amount"}
                        ));
                        purchaseItemTableModel = (DefaultTableModel) purchaseItemTable.getModel();
                        CommonService.setWidthAsPercentages(purchaseItemTable, .03, .10, .20, .08, .08, .08, .08, .08, .08);
                        
                        fillPurchaseItemTable();
                    }
                }
            });
            // END OF KEY BINDINGS
            //shortcut

             //save shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "save");
            this.getActionMap().put("save", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveInvoice();
                }
            });
            
            purchaseLedger = ledgerDAO.findByLedgerName("Purchase Account");
            discountLedger = ledgerDAO.findByLedgerName("Discount Received");
            // Combo boxes initialization && get all tax Ledgers to update availableQty
            for (Tax tacs : taxDAO.findAll()) {
                taxComboBox.addItem(new ComboKeyValue((tacs.getName()), tacs));
                //              taxLedgerList.add(ledgerDAO.findByLedgerName(tacs.getName()));
            }

            // Combo boxes initialization && get all charges Ledgers to update availableQty
            for (Charges charges : chargesDAO.findAll()) {
                chargesComboBox.addItem(new ComboKeyValue(charges.getName(), charges));
                //             chargesLedgerList.add(ledgerDAO.findByLedgerName(charges.getName()));
            }
            commonService.setCurrentPeriodOnCalendar(billDateFormattedText);

            if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
                saveButton.setEnabled(false);
                newButton.setEnabled(false);
            }
            //table background color removed
            jScrollPane1.setOpaque(false);
            jScrollPane1.getViewport().setOpaque(false);

            billDateFormattedText.setDate(new java.util.Date());
            this.mainForm = mainForm;

            numberProperty = numberPropertyDAO.findInvoiceNo("Purchase");
            invoiceNumberText.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());

            //set table design
            purchaseItemTable.setRowHeight(GlobalProperty.tableRowHieght);
            purchaseItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                    null,
                    new String[]{"Sr. No.", "Item Code", "Description", "Godown", "Qty.", "Unit Price", "Total Amount"}
            ));
            purchaseItemTableModel = (DefaultTableModel) purchaseItemTable.getModel();
            CommonService.setWidthAsPercentages(purchaseItemTable, .03, .10, .20, .08, .08, .08, .08);

            for (MultiCurrency currency : multiCurrencyDAO.findAll()) {
                if (currency == null) {
                    this.currency = currency;
                }
                multiCurrencyComboBox.addItem(new ComboKeyValue(currency.getName(), currency));
            }

            for (ShipmentMode shipmentMode : shipmentModeDAO.findAll()) {
                shipmentComboBox.addItem(new ComboKeyValue(shipmentMode.getName(), shipmentMode));
            }

            //jTable rows & columns alignment
            ((DefaultTableCellRenderer) purchaseItemTable.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);

            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(JLabel.CENTER);
            purchaseItemTable.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
            purchaseItemTable.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
            purchaseItemTable.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
            purchaseItemTable.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
            //jTable rows & columns alignment
            setFocusOrder();

        } catch (Exception e) {
            log.error("Purchase:", e);
        }

    }

    public void listPurchaseItemHistory(Purchase purchase) {
        this.purchase = purchase;
        this.supplierLedger = purchase.getSupplier();

        //to update available Qty of every Item
        purchaseItemList = purchase.getPurchaseItems();
        purchaseItemListHistory = new ArrayList<PurchaseItem>();
        for (PurchaseItem purchaseItem : purchaseItemList) {

            //copy sales items 
            PurchaseItem purchaseItem1 = new PurchaseItem();
            purchaseItem1.setItem(purchaseItem.getItem());
            purchaseItem1.setQuantity(purchaseItem.getQuantity());
            purchaseItem1.setQuantity1(purchaseItem.getQuantity1());
            purchaseItemListHistory.add(purchaseItem1);

            Item item = purchaseItem.getItem();
            item.setAvailableQty(item.getAvailableQty() - purchaseItem.getQuantity());
            purchaseLedger.setAvailableBalance(purchaseLedger.getAvailableBalance() - (purchaseItem.getUnitPrice() * purchaseItem.getQuantity()));

            purchaseLedger.setAvailableBalance1(purchaseLedger.getAvailableBalance1() - (purchaseItem.getUnitPrice1() * purchaseItem.getQuantity1()));
            purchaseItem.setUnitPrice(purchaseItem.getUnitPrice());
        }

        purchaseTaxList = purchase.getPurchaseTaxes();
        for (PurchaseTax purchaseTax : purchaseTaxList) {
            ledger = ledgerDAO.findByLedgerName(purchaseTax.getTax().getName());
            ledger.setAvailableBalance(ledger.getAvailableBalance() - purchaseTax.getAmount());
            taxLedgerList.add(ledger);
            purchaseTax.setAmount(purchaseTax.getAmount() / purchase.getCurrentCurrencyRate());
        }

        purchaseChargesList = purchase.getPurchaseCharges();
        for (PurchaseCharge purchaseCharge : purchaseChargesList) {
            ledger = ledgerDAO.findByLedgerName(purchaseCharge.getCharges().getName());
            ledger.setAvailableBalance(ledger.getAvailableBalance() - purchaseCharge.getAmount());
            ledger.setAvailableBalance1(ledger.getAvailableBalance1() - purchaseCharge.getAmount1());
            chargesLedgerList.add(ledger);
            purchaseCharge.setAmount(purchaseCharge.getAmount() / purchase.getCurrentCurrencyRate());
            purchaseCharge.setAmount1(purchaseCharge.getAmount1() / purchase.getCurrentCurrencyRate());
        }
        billDateFormattedText.setDate(purchase.getBillDate());
        invoiceNumberText.setText(purchase.getInvoiceNumber());
        supplierText.setText(purchase.getSupplier().getName());
        deliveryDateFormattedText.setText(purchase.getDeliveryDate() + "");
        addressTextArea.setText(purchase.getAddress());

        discountLedger.setAvailableBalance(discountLedger.getAvailableBalance() - purchase.getDiscount());
        discountLedger.setAvailableBalance1(discountLedger.getAvailableBalance1() - purchase.getDiscount1());

        //Updating availableQty of Supplier Ledger
        if (supplierLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
            supplierLedger.setAvailableBalance(supplierLedger.getAvailableBalance() + purchase.getGrandTotal());
            supplierLedger.setAvailableBalance1(supplierLedger.getAvailableBalance1() + purchase.getGrandTotal1());
        } else if (supplierLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
            supplierLedger.setAvailableBalance(supplierLedger.getAvailableBalance() - purchase.getGrandTotal());
            supplierLedger.setAvailableBalance1(supplierLedger.getAvailableBalance1() - purchase.getGrandTotal1());
        }

        totDiscount = purchase.getDiscount() / purchase.getCurrentCurrencyRate();
        totDiscount1 = purchase.getDiscount1() / purchase.getCurrentCurrencyRate();

        saveButton.setText("Update");
        currency = purchase.getMultiCurrency();
        currency.setRate(purchase.getCurrentCurrencyRate());
        //currency.setRate(1.0);
        check = 1;

        /* for(PurchaseItem purchaseItem:purchaseItemList){
         previousPurchaseAccount+=(purchaseItem.getQuantity()*purchaseItem.getUnitPrice());
         $$$$$$$$$$$$$$$$$$$$$     }
         previousDiscountReceived=totDiscount;
         */
        int count = multiCurrencyComboBox.getItemCount();
        for (int i = 0; i < count; i++) {
            ComboKeyValue ckv = (ComboKeyValue) multiCurrencyComboBox.getItemAt(i);
            if (ckv.getKey().trim().equalsIgnoreCase(purchase.getMultiCurrency().getName())) {
                multiCurrencyComboBox.setSelectedItem(ckv);
            }
        }

        int count1 = shipmentComboBox.getItemCount();
        if (purchase.getShipmentMode() != null) {
            for (int i = 0; i < count1; i++) {
                ComboKeyValue ckv = (ComboKeyValue) shipmentComboBox.getItemAt(i);
                if (ckv.getKey().trim().equalsIgnoreCase(purchase.getShipmentMode().getName())) {
                    shipmentComboBox.setSelectedItem(ckv);
                }
            }
        }
        // to avoid increasing number property while updating repeatingly
        numberProperty.setNumber(numberProperty.getNumber() - 1);
        fillPurchaseItemTable();
    }

    void generateAllList(String billDate, String invoiceNumber, String ledgerName) {
        try {
            java.sql.Date invDate = new java.sql.Date(commonService.convertToDate(billDate).getTime());
            Purchase purchase = purchaseDAO.findByDateInvNumSupplier(invDate, invoiceNumber, ledgerName);
            purchaseItemList = purchase.getPurchaseItems();
            purchaseTaxList = purchase.getPurchaseTaxes();
            purchaseChargesList = purchase.getPurchaseCharges();
            saveButton.setEnabled(false);
            newButton.setEnabled(false);
            cancelButton.setEnabled(false);
            addItemButton.setEnabled(false);
            itemRemoveButton.setEnabled(false);
            taxAddButton.setEnabled(false);
            chargesAddButton.setEnabled(false);
            chargesMinusButton.setEnabled(false);
            fillPurchaseItemTable();
        } catch (Exception e) {
            log.error("generateAllList:", e);
        }
    }

    void setFocusOrder() {
        try {

            supplierText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    deliveryDateFormattedText.requestFocusInWindow();
                }
            });
            deliveryDateFormattedText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    multiCurrencyComboBox.requestFocusInWindow();
                }
            });
            
            multiCurrencyComboBox.getEditor().addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    itemCodeText.requestFocusInWindow();
                }
            });
            
            itemCodeText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    itemQtyText.requestFocusInWindow();
                }
            });
            itemQtyText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    itemUnitPriceText.requestFocusInWindow();
                }
            });
            itemUnitPriceText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addItemButton.requestFocusInWindow();
                }
            });
            addItemButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    taxComboBox.requestFocusInWindow();
                }
            });
            taxComboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    taxAddButton.requestFocusInWindow();
                }
            });
            taxAddButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chargesComboBox.requestFocusInWindow();
                }
            });
            chargesComboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chargesText.requestFocusInWindow();
                }
            });
            chargesText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chargesAddButton.requestFocusInWindow();
                }
            });
            chargesAddButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    shipmentComboBox.requestFocusInWindow();
                }
            });
            shipmentComboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    discountText.requestFocusInWindow();
                }
            });
            discountText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    totDiscountAddButton.requestFocusInWindow();
                }
            });
            totDiscountAddButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveButton.requestFocusInWindow();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
     void setFocusOrder1() {
        try {

            itemQtyText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    itemQtyText1.requestFocusInWindow();
                }
            });
            itemQtyText1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    itemUnitPriceText.requestFocusInWindow();
                }
            });
            
           
            
            itemUnitPriceText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    itemQtyText.requestFocusInWindow();
                }
            });
            itemQtyText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    itemUnitPriceText1.requestFocusInWindow();
                }
            });
            itemUnitPriceText1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addItemButton.requestFocusInWindow();
                }
            });
            chargesText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chargesText1.requestFocusInWindow();
                }
            });
            chargesText1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chargesAddButton.requestFocusInWindow();
                }
            });
            discountText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    discountText1.requestFocusInWindow();
                }
            });
            discountText1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    totDiscountAddButton.requestFocusInWindow();
                }
            });
            
            
   
        }catch (Exception e) {
            e.printStackTrace();
        }

    }
     /** This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        bottomPanel = new javax.swing.JPanel();
        itemInfoPanel = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        itemDescriptionText = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        itemCodeText = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        totavailableQtyLabel = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        availableQtyLabel = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        itemCategoryText = new javax.swing.JTextField();
        godownText = new javax.swing.JTextField();
        buttonPanel = new javax.swing.JPanel();
        addItemButton = new javax.swing.JButton();
        itemRemoveButton = new javax.swing.JButton();
        qtyPanel = new javax.swing.JPanel();
        itemQtyText = new javax.swing.JTextField();
        itemQtyText1 = new javax.swing.JTextField();
        unitPricePanel = new javax.swing.JPanel();
        itemUnitPriceText = new javax.swing.JFormattedTextField();
        itemUnitPriceText1 = new javax.swing.JFormattedTextField();
        chargesPanel = new javax.swing.JPanel();
        chargesComboBox = new javax.swing.JComboBox();
        chargesAddButton = new javax.swing.JButton();
        chargesMinusButton = new javax.swing.JButton();
        chargesPanel1 = new javax.swing.JPanel();
        chargesText = new javax.swing.JFormattedTextField();
        chargesText1 = new javax.swing.JFormattedTextField();
        InvoiceDetailsPanel = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        salesTaxText = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        addressTextArea = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        multiCurrencyComboBox = new javax.swing.JComboBox();
        jLabel16 = new javax.swing.JLabel();
        invoiceNumberText = new javax.swing.JTextField();
        supplierText = new javax.swing.JTextField();
        billDateFormattedText = new com.toedter.calendar.JDateChooser();
        deliveryDateFormattedText = new javax.swing.JTextField();
        taxPanel = new javax.swing.JPanel();
        taxComboBox = new javax.swing.JComboBox();
        taxAddButton = new javax.swing.JButton();
        shipmentModePanel = new javax.swing.JPanel();
        shipmentComboBox = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        grandTotalPanel1 = new javax.swing.JPanel();
        grandTotalLabel = new javax.swing.JLabel();
        shipmentModePanel1 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        totDiscountAddButton = new javax.swing.JButton();
        netAmountLabel = new javax.swing.JLabel();
        discountPanel = new javax.swing.JPanel();
        discountText = new javax.swing.JFormattedTextField();
        discountText1 = new javax.swing.JFormattedTextField();
        jPanel1 = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        tableBorderPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        purchaseItemTable = new javax.swing.JTable();

        jTextField1.setText("jTextField1");

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        bottomPanel.setOpaque(false);

        itemInfoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Item Info*"));
        itemInfoPanel.setOpaque(false);
        itemInfoPanel.setPreferredSize(new java.awt.Dimension(382, 179));

        jLabel10.setText("Unit Price");

        jLabel9.setText("Quantity");

        itemDescriptionText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemDescriptionTextKeyPressed(evt);
            }
        });

        jLabel7.setText("Item Code");

        itemCodeText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemCodeTextKeyPressed(evt);
            }
        });

        jLabel8.setText("Description");

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel6.setOpaque(false);
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Total Aval Quantity");
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel6.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 120, 20));

        totavailableQtyLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        totavailableQtyLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel6.add(totavailableQtyLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 90, 40));

        jLabel12.setText("Available Quantity:");
        jLabel12.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel6.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 100, 20));

        availableQtyLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        availableQtyLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel6.add(availableQtyLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 50, 30));

        jLabel4.setText("Category");

        jLabel11.setText("Godown");

        itemCategoryText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemCategoryText.setText("0");
        itemCategoryText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemCategoryTextFocusGained(evt);
            }
        });
        itemCategoryText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemCategoryTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemCategoryTextKeyTyped(evt);
            }
        });

        godownText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        godownText.setText("0");
        godownText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                godownTextFocusGained(evt);
            }
        });
        godownText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                godownTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                godownTextKeyTyped(evt);
            }
        });

        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new java.awt.GridLayout(1, 0));

        addItemButton.setText("+");
        addItemButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addItemButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(addItemButton);

        itemRemoveButton.setText("-");
        itemRemoveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemRemoveButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(itemRemoveButton);

        qtyPanel.setLayout(new java.awt.GridLayout(1, 0));

        itemQtyText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemQtyText.setText("0");
        itemQtyText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemQtyTextFocusGained(evt);
            }
        });
        itemQtyText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemQtyTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemQtyTextKeyTyped(evt);
            }
        });
        qtyPanel.add(itemQtyText);

        itemQtyText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemQtyText1.setText("0");
        itemQtyText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemQtyText1FocusGained(evt);
            }
        });
        itemQtyText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemQtyText1KeyTyped(evt);
            }
        });
        qtyPanel.add(itemQtyText1);

        unitPricePanel.setLayout(new java.awt.GridLayout(1, 0));

        itemUnitPriceText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        itemUnitPriceText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemUnitPriceText.setText("0.00");
        itemUnitPriceText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemUnitPriceTextFocusGained(evt);
            }
        });
        itemUnitPriceText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemUnitPriceTextKeyTyped(evt);
            }
        });
        unitPricePanel.add(itemUnitPriceText);

        itemUnitPriceText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        itemUnitPriceText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemUnitPriceText1.setText("0.00");
        itemUnitPriceText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemUnitPriceText1FocusGained(evt);
            }
        });
        itemUnitPriceText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemUnitPriceText1KeyTyped(evt);
            }
        });
        unitPricePanel.add(itemUnitPriceText1);

        javax.swing.GroupLayout itemInfoPanelLayout = new javax.swing.GroupLayout(itemInfoPanel);
        itemInfoPanel.setLayout(itemInfoPanelLayout);
        itemInfoPanelLayout.setHorizontalGroup(
            itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(itemCategoryText, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(itemDescriptionText, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(itemCodeText)
                    .addComponent(godownText)
                    .addComponent(qtyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(unitPricePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        itemInfoPanelLayout.setVerticalGroup(
            itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, itemInfoPanelLayout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE))
                    .addGroup(itemInfoPanelLayout.createSequentialGroup()
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(itemCodeText))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(itemDescriptionText)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(qtyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(itemInfoPanelLayout.createSequentialGroup()
                                        .addGap(55, 55, 55)
                                        .addComponent(godownText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(unitPricePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(itemInfoPanelLayout.createSequentialGroup()
                                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(7, 7, 7)
                                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(itemCategoryText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                        .addContainerGap(25, Short.MAX_VALUE))))
        );

        chargesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Charges"));
        chargesPanel.setOpaque(false);

        chargesComboBox.setEditable(true);
        chargesComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                chargesComboBoxMouseClicked(evt);
            }
        });
        chargesComboBox.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesComboBoxFocusGained(evt);
            }
        });

        chargesAddButton.setText("+");
        chargesAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesAddButtonActionPerformed(evt);
            }
        });

        chargesMinusButton.setText("-");
        chargesMinusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesMinusButtonActionPerformed(evt);
            }
        });

        chargesPanel1.setLayout(new java.awt.GridLayout(1, 0));

        chargesText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        chargesText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        chargesText.setText("0.00");
        chargesText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesTextFocusGained(evt);
            }
        });
        chargesText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                chargesTextKeyTyped(evt);
            }
        });
        chargesPanel1.add(chargesText);

        chargesText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        chargesText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        chargesText1.setText("0.00");
        chargesText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesText1FocusGained(evt);
            }
        });
        chargesText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                chargesText1KeyTyped(evt);
            }
        });
        chargesPanel1.add(chargesText1);

        javax.swing.GroupLayout chargesPanelLayout = new javax.swing.GroupLayout(chargesPanel);
        chargesPanel.setLayout(chargesPanelLayout);
        chargesPanelLayout.setHorizontalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, chargesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chargesComboBox, 0, 138, Short.MAX_VALUE)
                    .addComponent(chargesPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        chargesPanelLayout.setVerticalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chargesPanelLayout.createSequentialGroup()
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chargesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(chargesPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        InvoiceDetailsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Invoice Details"));
        InvoiceDetailsPanel.setOpaque(false);

        jLabel6.setText("Bill Date*");

        jLabel5.setText("Invoice Number*");

        jLabel13.setText("Supplier*");

        jLabel14.setText("Delivery Date");

        jLabel15.setText("Sales Tax No: ");

        addressTextArea.setColumns(20);
        addressTextArea.setLineWrap(true);
        addressTextArea.setRows(5);
        jScrollPane3.setViewportView(addressTextArea);

        jLabel1.setText("Address :");

        multiCurrencyComboBox.setEditable(true);
        multiCurrencyComboBox.setNextFocusableComponent(itemCodeText);
        multiCurrencyComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                multiCurrencyComboBoxMouseClicked(evt);
            }
        });
        multiCurrencyComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                multiCurrencyComboBoxItemStateChanged(evt);
            }
        });
        multiCurrencyComboBox.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                multiCurrencyComboBoxFocusGained(evt);
            }
        });
        multiCurrencyComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                multiCurrencyComboBoxKeyTyped(evt);
            }
        });

        jLabel16.setText("Multi Currency*");

        invoiceNumberText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                invoiceNumberTextKeyPressed(evt);
            }
        });

        supplierText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supplierTextActionPerformed(evt);
            }
        });
        supplierText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                supplierTextKeyPressed(evt);
            }
        });

        billDateFormattedText.setDateFormatString("dd/MM/yyyy");

        javax.swing.GroupLayout InvoiceDetailsPanelLayout = new javax.swing.GroupLayout(InvoiceDetailsPanel);
        InvoiceDetailsPanel.setLayout(InvoiceDetailsPanelLayout);
        InvoiceDetailsPanelLayout.setHorizontalGroup(
            InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                        .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
                                        .addGap(4, 4, 4)))
                                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(supplierText, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(billDateFormattedText, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                    .addComponent(deliveryDateFormattedText)))
                            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(invoiceNumberText)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(multiCurrencyComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                    .addComponent(salesTaxText))
                .addContainerGap())
        );
        InvoiceDetailsPanelLayout.setVerticalGroup(
            InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(multiCurrencyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16))
                        .addGap(12, 12, 12)
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(billDateFormattedText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(12, 12, 12)
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(invoiceNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addGap(16, 16, 16)
                                .addComponent(jLabel13))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(supplierText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(2, 2, 2)))))
                .addGap(9, 9, 9)
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(salesTaxText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel14)
                        .addComponent(deliveryDateFormattedText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        taxPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Tax"));
        taxPanel.setOpaque(false);

        taxComboBox.setEditable(true);
        taxComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                taxComboBoxMouseClicked(evt);
            }
        });
        taxComboBox.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
                taxComboBoxPopupMenuWillBecomeVisible(evt);
            }
        });
        taxComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                taxComboBoxItemStateChanged(evt);
            }
        });
        taxComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                taxComboBoxActionPerformed(evt);
            }
        });
        taxComboBox.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                taxComboBoxFocusGained(evt);
            }
        });

        taxAddButton.setText("+");
        taxAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                taxAddButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout taxPanelLayout = new javax.swing.GroupLayout(taxPanel);
        taxPanel.setLayout(taxPanelLayout);
        taxPanelLayout.setHorizontalGroup(
            taxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(taxPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(taxComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(taxAddButton)
                .addContainerGap())
        );
        taxPanelLayout.setVerticalGroup(
            taxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(taxPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(taxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(taxAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(taxComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        shipmentModePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        shipmentModePanel.setOpaque(false);

        shipmentComboBox.setEditable(true);
        shipmentComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                shipmentComboBoxMouseClicked(evt);
            }
        });
        shipmentComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                shipmentComboBoxActionPerformed(evt);
            }
        });
        shipmentComboBox.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                shipmentComboBoxFocusGained(evt);
            }
        });

        jLabel3.setText("Shipment Mode");

        javax.swing.GroupLayout shipmentModePanelLayout = new javax.swing.GroupLayout(shipmentModePanel);
        shipmentModePanel.setLayout(shipmentModePanelLayout);
        shipmentModePanelLayout.setHorizontalGroup(
            shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, shipmentModePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(shipmentComboBox, 0, 116, Short.MAX_VALUE)
                .addContainerGap())
        );
        shipmentModePanelLayout.setVerticalGroup(
            shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(shipmentModePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(shipmentComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        grandTotalPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Grand Total", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N
        grandTotalPanel1.setOpaque(false);
        grandTotalPanel1.setLayout(new java.awt.BorderLayout());

        grandTotalLabel.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        grandTotalLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        grandTotalLabel.setText("0.00");
        grandTotalLabel.setFocusable(false);
        grandTotalLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        grandTotalPanel1.add(grandTotalLabel, java.awt.BorderLayout.CENTER);

        shipmentModePanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        shipmentModePanel1.setOpaque(false);

        jLabel17.setText("Discount");

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel18.setText("Net Amount");

        totDiscountAddButton.setText("+");
        totDiscountAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totDiscountAddButtonActionPerformed(evt);
            }
        });

        netAmountLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        discountPanel.setLayout(new java.awt.GridLayout(1, 0));

        discountText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        discountText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        discountText.setText("0.00");
        discountText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                discountTextFocusGained(evt);
            }
        });
        discountText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                discountTextKeyTyped(evt);
            }
        });
        discountPanel.add(discountText);

        discountText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        discountText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        discountText1.setText("0.00");
        discountText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                discountText1FocusGained(evt);
            }
        });
        discountText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                discountText1KeyTyped(evt);
            }
        });
        discountPanel.add(discountText1);

        javax.swing.GroupLayout shipmentModePanel1Layout = new javax.swing.GroupLayout(shipmentModePanel1);
        shipmentModePanel1.setLayout(shipmentModePanel1Layout);
        shipmentModePanel1Layout.setHorizontalGroup(
            shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(shipmentModePanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(shipmentModePanel1Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(netAmountLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(shipmentModePanel1Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(totDiscountAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE)
                            .addComponent(discountPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        shipmentModePanel1Layout.setVerticalGroup(
            shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, shipmentModePanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE)
                    .addComponent(netAmountLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(discountPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(totDiscountAddButton)
                .addContainerGap())
        );

        jPanel1.setOpaque(false);
        jPanel1.setLayout(new java.awt.GridLayout(1, 0));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("New");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        jPanel1.add(newButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveButton.setText("<HTML><U>S</U>ave<HTML>");
        saveButton.setToolTipText("");
        saveButton.setOpaque(false);
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        jPanel1.add(saveButton);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/cancel.png"))); // NOI18N
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        jPanel1.add(cancelButton);

        javax.swing.GroupLayout bottomPanelLayout = new javax.swing.GroupLayout(bottomPanel);
        bottomPanel.setLayout(bottomPanelLayout);
        bottomPanelLayout.setHorizontalGroup(
            bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bottomPanelLayout.createSequentialGroup()
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(InvoiceDetailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(itemInfoPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 316, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(shipmentModePanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chargesPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(taxPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(shipmentModePanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(grandTotalPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(11, 11, 11))
        );
        bottomPanelLayout.setVerticalGroup(
            bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bottomPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(bottomPanelLayout.createSequentialGroup()
                        .addComponent(InvoiceDetailsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(itemInfoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bottomPanelLayout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(shipmentModePanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(grandTotalPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(bottomPanelLayout.createSequentialGroup()
                        .addComponent(taxPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chargesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(shipmentModePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(bottomPanel, java.awt.BorderLayout.PAGE_END);

        tableBorderPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Purchase Invoice"));
        tableBorderPanel.setOpaque(false);

        jScrollPane1.setName(""); // NOI18N
        jScrollPane1.setOpaque(false);

        purchaseItemTable.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        purchaseItemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        purchaseItemTable.setOpaque(false);
        purchaseItemTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                purchaseItemTableMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                purchaseItemTableMouseEntered(evt);
            }
        });
        jScrollPane1.setViewportView(purchaseItemTable);

        javax.swing.GroupLayout tableBorderPanelLayout = new javax.swing.GroupLayout(tableBorderPanel);
        tableBorderPanel.setLayout(tableBorderPanelLayout);
        tableBorderPanelLayout.setHorizontalGroup(
            tableBorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tableBorderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1228, Short.MAX_VALUE)
                .addContainerGap())
        );
        tableBorderPanelLayout.setVerticalGroup(
            tableBorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tableBorderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE))
        );

        add(tableBorderPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void itemCodeTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemCodeTextKeyPressed
        try {
            ItemService itemService = new ItemService();
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                String productId = itemCodeText.getText().trim();
                List<Item> items = itemService.populatePurchasePopupTableByProductId(productId, popupTableDialog, purchaseItemList, currency, isDuplicateColumnsVisible, purchaseItemListHistory);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();

                itemQtyText.requestFocusInWindow();

                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    item = items.get(index);
                    itemCodeText.setText(item.getItemCode());
                    itemDescriptionText.setText(item.getDescription());
                    itemCategoryText.setText(Double.toString(item.getWeight()));
                    godownText.setText(item.getGodown().getName());
                    if (isDuplicateColumnsVisible) {
                        itemQty = itemDAO.findTotItemQty1(item.getItemCode());
                        availableQtyLabel.setText(Double.toString(item.getAvailableQty() + item.getAvailableQty1()));
                    } else {
                        itemQty = itemDAO.findTotItemQty(item.getItemCode());
                        availableQtyLabel.setText(Double.toString(item.getAvailableQty()));
                    }
                    totavailableQtyLabel.setText(itemQty + "");
                } else {
                    MsgBox.warning("No Item Selected.");
                    itemQtyText.requestFocusInWindow();
                }
            }
        } catch (Exception e) {
            log.error("productIdTextKeyPressed:", e);
        }
    }//GEN-LAST:event_itemCodeTextKeyPressed

    void fillPurchaseItemTable() {
        try {
            //boolean flag = false;

            purchaseItemTableModel.setRowCount(0);
            //BigDecimal grandTotal = new BigDecimal(0.00) ;
            //grandTotal.setScale(1);
            subTotal = 0.00;
            netTotal = 0.00;

            subTotal1 = 0.00;
            netTotal1 = 0.00;

            int slNo = 1;
            String currencySymbol = (null == currency) ? "" : currency.getSymbol();

            //{"Sr. No.", "Item Code","Description", "Unit Price", "Qty.", "Total Amount"}
            for (PurchaseItem pItem : purchaseItemList) {
                double itemTotal = 0;
                double itemTotal1 = 0;

                if (purchase == null) {
                    itemTotal = ((pItem.getUnitPrice() / currency.getRate()) * pItem.getQuantity());
                    itemTotal1 = ((pItem.getUnitPrice1() / currency.getRate()) * pItem.getQuantity1());
                    if (isDuplicateColumnsVisible) {
                        //itemTotal1 = ((pItem.getUnitPrice1() / currency.getRate()) * pItem.getQuantity1());
                        if (itemTotal1 > 0 || itemTotal > 0) { //only add to jtable if tota is >0
                            purchaseItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getItem().getGodown().getName(), pItem.getQuantity(), currency.getSymbol() + " " + (pItem.getUnitPrice() / currency.getRate()), pItem.getQuantity1(), currency.getSymbol() + " " + (pItem.getUnitPrice1() / currency.getRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal + itemTotal1)});
                        }
                    } else {
                        if (itemTotal > 0) {
                            purchaseItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getItem().getGodown().getName(), pItem.getQuantity(), currency.getSymbol() + " " + (pItem.getUnitPrice() / currency.getRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal)});
                        }
                    }
                } else {
                    itemTotal = ((pItem.getUnitPrice() / currency.getRate()) * pItem.getQuantity());
                    itemTotal1 = ((pItem.getUnitPrice1() / currency.getRate()) * pItem.getQuantity1());
                    if (isDuplicateColumnsVisible) {
                        // itemTotal1 = ((pItem.getUnitPrice1() / currency.getRate()) * pItem.getQuantity1());
                        if (itemTotal1 > 0 || itemTotal > 0) {
                            purchaseItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getItem().getGodown().getName(), pItem.getQuantity(), currency.getSymbol() + " " + (pItem.getUnitPrice() / currency.getRate()), pItem.getQuantity1(), currency.getSymbol() + " " + (pItem.getUnitPrice1() / currency.getRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal + itemTotal1)});
                        }
                    } else {
                        if (itemTotal > 0) {
                            purchaseItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getItem().getGodown().getName(), pItem.getQuantity(), currency.getSymbol() + " " + (pItem.getUnitPrice() / currency.getRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal)});
                        }
                    }

                }
                subTotal += itemTotal;
                netTotal = subTotal;

                subTotal1 += itemTotal1;
                netTotal1 = subTotal1;
            }

            if (subTotal > 0 || subTotal1 > 0) {
                for (PurchaseTax purchaseTax : purchaseTaxList) {
                    if (isDuplicateColumnsVisible) {
                        purchaseItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseTax.getTax().getName(), "", "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(purchaseTax.getAmount() / currency.getRate())});
                    } else {
                        purchaseItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseTax.getTax().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(purchaseTax.getAmount() / currency.getRate())});
                    }
                    netTotal += (purchaseTax.getAmount());
                }
                for (PurchaseCharge purchaseCharge : purchaseChargesList) {
                    if (isDuplicateColumnsVisible) {
                        purchaseItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseCharge.getCharges().getName(), "", "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(purchaseCharge.getAmount() + purchaseCharge.getAmount1())});
                    } else {
                        purchaseItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseCharge.getCharges().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(purchaseCharge.getAmount())});
                    }

                    netTotal += (purchaseCharge.getAmount());
                    netTotal1 += (purchaseCharge.getAmount1());
                }
            }
            grandTotal = netTotal - totDiscount;
            grandTotal1 = netTotal1 - totDiscount1;
            if (totDiscount > 0 || totDiscount1 > 0) {
                if (isDuplicateColumnsVisible) {
                    purchaseItemTableModel.addRow(new Object[]{"", "", "Discount", "", "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(totDiscount + totDiscount1)});
                } else {
                    purchaseItemTableModel.addRow(new Object[]{"", "", "Discount", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(totDiscount)});
                }
            }
            if (isDuplicateColumnsVisible) {
                netAmountLabel.setText(currencySymbol + " " + commonService.formatIntoCurrencyAsString(netTotal + netTotal1));
                grandTotalLabel.setText(currencySymbol + "" + commonService.formatIntoCurrencyAsString(grandTotal + grandTotal1));
            } else {
                netAmountLabel.setText(currencySymbol + " " + commonService.formatIntoCurrencyAsString(netTotal));
                grandTotalLabel.setText(currencySymbol + "" + commonService.formatIntoCurrencyAsString(grandTotal));
            }

            //clear fields
            CommonService.clearCurrencyFields(new JTextField[]{itemUnitPriceText, itemUnitPriceText1});
            CommonService.clearNumberFields(new JTextField[]{itemQtyText, itemQtyText1, itemCategoryText});
            CommonService.clearTextFields(new JTextField[]{itemCodeText, itemDescriptionText, godownText});
            totavailableQtyLabel.setText("");
            availableQtyLabel.setText("");

        } catch (Exception e) {
            log.error("fillPurchaseItemTable:", e);
        }

    }

    private void addItemButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addItemButtonActionPerformed

        try {
            if (CommonService.stringValidator(new String[]{itemCodeText.getText(), itemDescriptionText.getText()})) {

                if (Double.parseDouble(itemQtyText.getText()) > 0 || Double.parseDouble(itemQtyText1.getText()) > 0) {

                    double itemQty = Double.parseDouble(itemQtyText.getText());
                    double itemUnitPrice = Double.parseDouble(itemUnitPriceText.getText());
                    double itemQty1 = Double.parseDouble(itemQtyText1.getText());
                    double itemUnitPrice1 = Double.parseDouble(itemUnitPriceText1.getText());

                    PurchaseItem purchaseItem = null;
                    for (PurchaseItem pItem : purchaseItemList) {
                        if (pItem.getItem().getItemCode().equalsIgnoreCase(item.getItemCode()) && pItem.getItem().getGodown().getName().equalsIgnoreCase(item.getGodown().getName())) {
                            purchaseItem = pItem;
                            break;
                        }
                    }

                    if (null == purchaseItem) {
                        purchaseItem = new PurchaseItem();
                        purchaseItemList.add(purchaseItem);
                    }

                    purchaseItem.setItem(item);
                    purchaseItem.setQuantity(itemQty);
                    purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());

                    //saving duplicated qty & price
                    purchaseItem.setQuantity1(itemQty1);
                    purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText())) * currency.getRate());

                    purchaseItem.setDiscount(0.00);
                    fillPurchaseItemTable();
                }
            } else {
                MsgBox.warning("No Item Selected.");
            }
        } catch (Exception e) {
            log.error("addItemButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_addItemButtonActionPerformed

    private void itemRemoveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemRemoveButtonActionPerformed
        if (null != purchaseItem) {
            purchaseItemRemovedList.add(purchaseItem);
            purchaseItemList.remove(purchaseItem);
            fillPurchaseItemTable();
            purchaseItem = null;
        }
    }//GEN-LAST:event_itemRemoveButtonActionPerformed

    private void invoiceNumberTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_invoiceNumberTextKeyPressed

    }//GEN-LAST:event_invoiceNumberTextKeyPressed

    private void itemQtyTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyTextFocusGained
        itemQtyText.selectAll();
    }//GEN-LAST:event_itemQtyTextFocusGained

    private void itemQtyTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyTextKeyPressed

    }//GEN-LAST:event_itemQtyTextKeyPressed

    private void itemQtyTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyTextKeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_itemQtyTextKeyTyped

    private void itemUnitPriceTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemUnitPriceTextFocusGained
        itemUnitPriceText.selectAll();
    }//GEN-LAST:event_itemUnitPriceTextFocusGained

    private void itemUnitPriceTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemUnitPriceTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_itemUnitPriceTextKeyTyped

    private void chargesTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesTextFocusGained
        chargesText.selectAll();
    }//GEN-LAST:event_chargesTextFocusGained

    private void chargesTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chargesTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_chargesTextKeyTyped

    private void chargesAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesAddButtonActionPerformed

        try {
            Object chargesItem = chargesComboBox.getSelectedItem();
            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();
            double chargeAmt = Double.parseDouble(chargesText.getText());
            double chargeAmt1 = Double.parseDouble(chargesText1.getText());
            if ((Double.parseDouble(chargesText.getText()) > 0 || Double.parseDouble(chargesText1.getText()) > 0) && !charges.getName().equalsIgnoreCase("N/A")) {
                PurchaseCharge purchaseCharge = new PurchaseCharge();

                Iterator<PurchaseCharge> iter = purchaseChargesList.iterator();
                while (iter.hasNext()) {
                    if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                        iter.remove();
                    }
                }

                purchaseCharge.setCharges(charges);
                purchaseCharge.setAmount(chargeAmt);
                purchaseCharge.setAmount1(chargeAmt1);
                purchaseChargesList.add(purchaseCharge);
                chargesComboBox.setSelectedIndex(0);
                chargesText.setText("0.00");
                chargesText1.setText("0.00");
                fillPurchaseItemTable();
            }

        } catch (Exception e) {
            log.error("chargesAddButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesAddButtonActionPerformed

    private void itemDescriptionTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemDescriptionTextKeyPressed
        try {
            ItemService itemService = new ItemService();
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                String itemName = itemDescriptionText.getText().trim();
                List<Item> items = itemService.populatePurchasePopupTableByItemName(itemName, popupTableDialog, purchaseItemList, currency, isDuplicateColumnsVisible, purchaseItemListHistory);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();

                itemQtyText.requestFocusInWindow();

                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    item = items.get(index);
                    itemCodeText.setText(item.getItemCode());
                    itemDescriptionText.setText(item.getDescription());
                    itemCategoryText.setText(Double.toString(item.getWeight()));
                    godownText.setText(item.getGodown().getName());
                    if (isDuplicateColumnsVisible) {
                        itemQty = itemDAO.findTotItemQty1(item.getItemCode());
                        availableQtyLabel.setText(Double.toString(item.getAvailableQty() + item.getAvailableQty1()));
                    } else {
                        itemQty = itemDAO.findTotItemQty(item.getItemCode());
                        availableQtyLabel.setText(Double.toString(item.getAvailableQty()));
                    }
                    totavailableQtyLabel.setText(itemQty + "");
                } else {
                    MsgBox.warning("No Item Selected.");
                    itemDescriptionText.requestFocusInWindow();
                }
            }
        } catch (Exception e) {
            log.error("itemNameTextKeyPressed:", e);
        }
    }//GEN-LAST:event_itemDescriptionTextKeyPressed

    private void multiCurrencyComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_multiCurrencyComboBoxItemStateChanged
        Object currencyItem = multiCurrencyComboBox.getSelectedItem();

        if (((ComboKeyValue) currencyItem) != null) {
     //       MultiCurrency tempCurrency = (MultiCurrency) ((ComboKeyValue) currencyItem).getValue();
            //      if (purchase != null) {
            //          if (purchase.getMultiCurrency().getId() != tempCurrency.getId()) {
            currency = (MultiCurrency) ((ComboKeyValue) currencyItem).getValue();
       //         }
            //    } else {
            //          currency = (MultiCurrency) ((ComboKeyValue) currencyItem).getValue();
            //     }
            //System.out.println("currency:"+currency.getName());
            fillPurchaseItemTable();
        }

    }//GEN-LAST:event_multiCurrencyComboBoxItemStateChanged

    private void purchaseItemTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purchaseItemTableMouseClicked
        try {
            ArrayList row = CommonService.getTableRowData(purchaseItemTable);
            String itemCode = row.get(1) + "";

            if (itemCode.trim().length() > 0) {
                purchaseItem = purchaseItemList.get(Integer.parseInt(row.get(0) + "") - 1);
                item = purchaseItem.getItem();
                itemCodeText.setText(purchaseItem.getItem().getItemCode());
                itemDescriptionText.setText(purchaseItem.getItem().getDescription());
                itemQtyText.setText(purchaseItem.getQuantity() + "");
                itemQtyText1.setText(purchaseItem.getQuantity1() + "");
                if (purchase == null) {
                    itemUnitPriceText.setText(purchaseItem.getUnitPrice() / currency.getRate() + "");
                    itemUnitPriceText1.setText(purchaseItem.getUnitPrice1() / currency.getRate() + "");
                } else {
                    itemUnitPriceText.setText(purchaseItem.getUnitPrice() / purchase.getCurrentCurrencyRate() + "");
                    itemUnitPriceText1.setText(purchaseItem.getUnitPrice1() / purchase.getCurrentCurrencyRate() + "");
                }
                //totavailableQtyLabel.setText(purchaseItem.getItem().getAvailableQty() + "");
                //showing total available qty from all godown
                if (isDuplicateColumnsVisible) {
                    itemQty = itemDAO.findTotItemQty1(purchaseItem.getItem().getItemCode());
                    availableQtyLabel.setText(Double.toString(purchaseItem.getItem().getAvailableQty() + purchaseItem.getItem().getAvailableQty1()));
                } else {
                    itemQty = itemDAO.findTotItemQty(purchaseItem.getItem().getItemCode());
                    availableQtyLabel.setText(Double.toString(purchaseItem.getItem().getAvailableQty()));
                }
                totavailableQtyLabel.setText(itemQty + "");
                //showing total available qty 

                godownText.setText(purchaseItem.getItem().getGodown().getName());

            } else {
                //this part will treat as charges,discount or tax
                String itemDesc = row.get(2) + "";
                itemDesc = itemDesc.trim();
                int count = taxComboBox.getItemCount();

                if (itemDesc.equalsIgnoreCase("discount")) {
                    totDiscountAddButton.setText("-");
                }

                for (int i = 0; i < count; i++) {
                    ComboKeyValue ckv = (ComboKeyValue) taxComboBox.getItemAt(i);
                    if (ckv.getKey().trim().equalsIgnoreCase(itemDesc)) {
                        taxComboBox.setSelectedItem(ckv);
                        taxAddButton.setText("-");
                    }
                }

                count = chargesComboBox.getItemCount();
                for (int i = 0; i < count; i++) {
                    ComboKeyValue ckv = (ComboKeyValue) chargesComboBox.getItemAt(i);
                    if (ckv.getKey().trim().equalsIgnoreCase(itemDesc)) {
                        chargesComboBox.setSelectedItem(ckv);
                        for (PurchaseCharge purchaseCharge : purchaseChargesList) {
                            if (purchaseCharge.getCharges().getName().equalsIgnoreCase(itemDesc)) {
                                chargesText.setText(purchaseCharge.getAmount() + "");
                                chargesText1.setText(purchaseCharge.getAmount1() + "");
                                break;
                            }
                        }
                    }
                }
            }
            discountText.setText(totDiscount + "");
            discountText1.setText(totDiscount1 + "");

        } catch (Exception e) {
            log.error("purchaseItemTableMouseClicked:", e);
        }
    }//GEN-LAST:event_purchaseItemTableMouseClicked

    private void taxComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_taxComboBoxItemStateChanged


    }//GEN-LAST:event_taxComboBoxItemStateChanged

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        CommonService.removeJpanel(mainForm.getContentPanel());
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void chargesMinusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesMinusButtonActionPerformed
        try {
            Object chargesItem = chargesComboBox.getSelectedItem();
            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();

            Iterator<PurchaseCharge> iter = purchaseChargesList.iterator();
            while (iter.hasNext()) {
                if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                    iter.remove();
                }
            }

            chargesComboBox.setSelectedIndex(0);
            chargesText.setText("0.00");
            fillPurchaseItemTable();

        } catch (Exception e) {
            log.error("chargesMinusButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesMinusButtonActionPerformed

    private void taxAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_taxAddButtonActionPerformed
        Object taxItem = taxComboBox.getSelectedItem();
        Tax tax = (Tax) ((ComboKeyValue) taxItem).getValue();

        if (tax.getTaxRate() > 0 && subTotal > 0 && !tax.getName().equalsIgnoreCase("N/A")) {
            if (taxAddButton.getText().trim().equalsIgnoreCase("+")) {
                PurchaseTax purchaseTax = new PurchaseTax();
                Iterator<PurchaseTax> iter = purchaseTaxList.iterator();
                while (iter.hasNext()) {
                    if (tax.getName().equalsIgnoreCase(iter.next().getTax().getName())) {
                        iter.remove();
                    }
                }

                purchaseTax.setTax(tax);
                purchaseTax.setTaxRate(tax.getTaxRate());
                double taxTotal = (subTotal * tax.getTaxRate()) / 100;
                purchaseTax.setAmount(taxTotal);
                purchaseTaxList.add(purchaseTax);
                taxComboBox.setSelectedIndex(0);
            } else {
                Iterator<PurchaseTax> iter = purchaseTaxList.iterator();
                while (iter.hasNext()) {
                    if (tax.getName().equalsIgnoreCase(iter.next().getTax().getName())) {
                        iter.remove();
                    }
                }
                taxAddButton.setText("+");
            }

            fillPurchaseItemTable();
        }
        taxComboBox.setSelectedIndex(0);
    }//GEN-LAST:event_taxAddButtonActionPerformed

    private void purchaseItemTableMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purchaseItemTableMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_purchaseItemTableMouseEntered

    void savePurchase() {
        try {
            if (purchase == null) {
                check = 0;
                purchase = new Purchase();
            }

            purchase.setBillDate(commonService.utilDateToSqlDate(billDateFormattedText.getDate()));
            purchase.setInvoiceNumber(invoiceNumberText.getText());
            purchase.setAddress(addressTextArea.getText().trim());
            purchase.setSupplier(supplierLedger);
            purchase.setDeliveryDate(deliveryDateFormattedText.getText());

            Object shipmentMod = shipmentComboBox.getSelectedItem();
            ShipmentMode shipmentMode = (ShipmentMode) ((ComboKeyValue) shipmentMod).getValue();
            if (shipmentMode.getName().equalsIgnoreCase("N/A")) {
                purchase.setShipmentMode(null);
            } else {
                purchase.setShipmentMode(shipmentMode);
            }

            purchase.setDiscount(totDiscount * currency.getRate());
            purchase.setDiscount1(totDiscount1 * currency.getRate());

            /*for (PurchaseItem purchaseItem : purchaseItemList) {
             purchaseItem.setUnitPrice(purchaseItem.getUnitPrice());

             }*/
            purchase.setPurchaseItems(purchaseItemList);

            for (PurchaseTax purchaseTax : purchaseTaxList) {
                purchaseTax.setAmount(purchaseTax.getAmount() * currency.getRate());
            }
            purchase.setPurchaseTaxes(purchaseTaxList);

            for (PurchaseCharge purchaseCharge : purchaseChargesList) {
                purchaseCharge.setAmount(purchaseCharge.getAmount() * currency.getRate());
                purchaseCharge.setAmount1(purchaseCharge.getAmount1() * currency.getRate());
            }
            purchase.setPurchaseCharges(purchaseChargesList);
            purchase.setCurrentCurrencyRate(currency.getRate());
            purchase.setMultiCurrency(currency);

            purchase.setGrandTotal(grandTotal * currency.getRate());

            purchase.setGrandTotal1(grandTotal1 * currency.getRate());

            purchase.setCompany(GlobalProperty.getCompany());
            //if (check == 0) {
            //    cRUDServices.saveModel(purchase);
            //} else {
            cRUDServices.saveOrUpdateModel(purchase);
            //}

            //Updating availableQty of Supplier Ledger
            if (supplierLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                supplierLedger.setAvailableBalance(supplierLedger.getAvailableBalance() - (grandTotal * currency.getRate()));
                supplierLedger.setAvailableBalance1(supplierLedger.getAvailableBalance1() - (grandTotal1 * currency.getRate()));
                cRUDServices.saveOrUpdateModel(supplierLedger);

            } else if (supplierLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                supplierLedger.setAvailableBalance(supplierLedger.getAvailableBalance() + (grandTotal * currency.getRate()));
                supplierLedger.setAvailableBalance1(supplierLedger.getAvailableBalance1() + (grandTotal1 * currency.getRate()));
                cRUDServices.saveOrUpdateModel(supplierLedger);

            }

            //Adding available quantity on each item
            for (PurchaseItem pi : purchaseItemList) {
                Item item = pi.getItem();
                item.setAvailableQty(item.getAvailableQty() + pi.getQuantity());
                item.setAvailableQty1(item.getAvailableQty1() + pi.getQuantity1());
                cRUDServices.saveOrUpdateModel(item);
            }
            
            //Adding available quantity on each item
            for (PurchaseItem pi : purchaseItemRemovedList) {
                Item item = pi.getItem();
                item.setAvailableQty(item.getAvailableQty());
                item.setAvailableQty1(item.getAvailableQty1());
                cRUDServices.saveOrUpdateModel(item);
            }

            //Updating Purchase Ledger available balance
            purchaseLedger.setAvailableBalance((purchaseLedger.getAvailableBalance()) + subTotal);
            purchaseLedger.setAvailableBalance1((purchaseLedger.getAvailableBalance1()) + subTotal1);
            cRUDServices.saveOrUpdateModel(purchaseLedger);

            //Discount Received
            discountLedger.setAvailableBalance((discountLedger.getAvailableBalance()) + totDiscount);
            discountLedger.setAvailableBalance1((discountLedger.getAvailableBalance1()) + totDiscount1);
            cRUDServices.saveOrUpdateModel(discountLedger);

            numberProperty.setNumber(numberProperty.getNumber() + 1);
            cRUDServices.saveOrUpdateModel(numberProperty);

            //tax
            for (Ledger ledger : taxLedgerList) {
                cRUDServices.saveOrUpdateModel(ledger);
            }
            for (PurchaseTax purchaseTax : purchaseTaxList) {
                ledger = ledgerDAO.findByLedgerName(purchaseTax.getTax().getName());
                ledger.setAvailableBalance(ledger.getAvailableBalance() + (purchaseTax.getAmount()));
                cRUDServices.saveOrUpdateModel(ledger);
            }

            //charges
            for (Ledger ledger : chargesLedgerList) {
                cRUDServices.saveOrUpdateModel(ledger);
            }
            for (PurchaseCharge purchaseCharge : purchaseChargesList) {
                ledger = ledgerDAO.findByLedgerName(purchaseCharge.getCharges().getName());
                ledger.setAvailableBalance(ledger.getAvailableBalance() + purchaseCharge.getAmount());
                ledger.setAvailableBalance1(ledger.getAvailableBalance1() + purchaseCharge.getAmount1());
                cRUDServices.saveOrUpdateModel(ledger);

            }
            listPurchaseItemHistory(purchase);
        } catch (Exception e) {
            log.error("savePurchase:", e);
        }
    }
    void saveInvoice(){
        try {
            
            if (billDateFormattedText.getDate() != null && supplierLedger != null && purchaseItemList != null) {
                if (MsgBox.confirm("Are you sure you want to save?")) {
                    savePurchase();
                    MsgBox.success("Purchase Invoice Saved Successfully");
                    //clearAllFields();
                }
            } else {
                MsgBox.warning("Required fields are not entered");
            }
            

        } catch (Exception ex) {
            log.error("saveButtonActionPerformed:", ex);
        }
    }
    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        saveInvoice();
    }//GEN-LAST:event_saveButtonActionPerformed

    private void supplierTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_supplierTextKeyPressed
        try {

            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                //String productId = productIdText.getText().trim();
                List<Ledger> suppliers = ledgerService.populatePopupTableBySupplier(supplierText.getText().trim(), popupTableDialog);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();
                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    supplierLedger = suppliers.get(index);
                    supplierText.setText(supplierLedger.getLedgerName());
                    addressTextArea.setText(supplierLedger.getAddress());
                    salesTaxText.setText(supplierLedger.getSalesTaxNo());
                }
            }

        } catch (Exception e) {
            log.error("supplierTextKeyPressed:", e);
        }
    }//GEN-LAST:event_supplierTextKeyPressed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        clearAllFields();
       
    }//GEN-LAST:event_newButtonActionPerformed

    private void itemCategoryTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemCategoryTextFocusGained
        itemCategoryText.selectAll();
    }//GEN-LAST:event_itemCategoryTextFocusGained

    private void itemCategoryTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemCategoryTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemCategoryTextKeyPressed

    private void itemCategoryTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemCategoryTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemCategoryTextKeyTyped

    private void godownTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_godownTextFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_godownTextFocusGained

    private void godownTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_godownTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_godownTextKeyPressed

    private void godownTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_godownTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_godownTextKeyTyped

    private void taxComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_taxComboBoxActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_taxComboBoxActionPerformed

    private void supplierTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supplierTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_supplierTextActionPerformed

    private void totDiscountAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totDiscountAddButtonActionPerformed
        // TODO add your handling code here:
        if (totDiscountAddButton.getText().trim().equalsIgnoreCase("+")) {
            totDiscount = Double.parseDouble(discountText.getText().trim());
            totDiscount1 = Double.parseDouble(discountText1.getText().trim());
            
        } else {
            totDiscount = 0;
            totDiscount1 = 0;
            
            totDiscountAddButton.setText("+");
        }
        discountText.setText("0.00");
        discountText1.setText("0.00");
        fillPurchaseItemTable();
    }//GEN-LAST:event_totDiscountAddButtonActionPerformed

    private void taxComboBoxFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_taxComboBoxFocusGained
        // TODO add your handling code here:
        //taxComboBox.setSelectedIndex(0);
        //taxComboBox.removeAllItems();

    }//GEN-LAST:event_taxComboBoxFocusGained

    private void chargesComboBoxFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesComboBoxFocusGained
        // TODO add your handling code here:

    }//GEN-LAST:event_chargesComboBoxFocusGained

    private void multiCurrencyComboBoxFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_multiCurrencyComboBoxFocusGained
        // TODO add your handling code here:

    }//GEN-LAST:event_multiCurrencyComboBoxFocusGained

    private void shipmentComboBoxFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_shipmentComboBoxFocusGained
        // TODO add your handling code here:

    }//GEN-LAST:event_shipmentComboBoxFocusGained

    private void taxComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_taxComboBoxMouseClicked
        // TODO add your handling code here:
        taxComboBox.removeAllItems();
        for (Tax tacs : taxDAO.findAll()) {
            taxComboBox.addItem(new ComboKeyValue((tacs.getName()), tacs));
        }
    }//GEN-LAST:event_taxComboBoxMouseClicked

    private void taxComboBoxPopupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_taxComboBoxPopupMenuWillBecomeVisible
        // TODO add your handling code here:

    }//GEN-LAST:event_taxComboBoxPopupMenuWillBecomeVisible

    private void shipmentComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_shipmentComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_shipmentComboBoxActionPerformed

    private void shipmentComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_shipmentComboBoxMouseClicked
        // TODO add your handling code here:
        shipmentComboBox.removeAllItems();
        for (ShipmentMode shipmentMode : shipmentModeDAO.findAll()) {
            shipmentComboBox.addItem(new ComboKeyValue(shipmentMode.getName(), shipmentMode));
        }
    }//GEN-LAST:event_shipmentComboBoxMouseClicked

    private void multiCurrencyComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_multiCurrencyComboBoxMouseClicked
        // TODO add your handling code here:
        multiCurrencyComboBox.removeAllItems();
        for (MultiCurrency currency : multiCurrencyDAO.findAll()) {
            if (currency == null) {
                this.currency = currency;
            }
            multiCurrencyComboBox.addItem(new ComboKeyValue(currency.getName(), currency));
        }
    }//GEN-LAST:event_multiCurrencyComboBoxMouseClicked

    private void chargesComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chargesComboBoxMouseClicked
        // TODO add your handling code here:
        chargesComboBox.removeAllItems();
        for (Charges charges : chargesDAO.findAll()) {
            chargesComboBox.addItem(new ComboKeyValue(charges.getName(), charges));
        }
    }//GEN-LAST:event_chargesComboBoxMouseClicked

    private void itemUnitPriceText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemUnitPriceText1FocusGained
        itemUnitPriceText1.selectAll();
    }//GEN-LAST:event_itemUnitPriceText1FocusGained

    private void itemUnitPriceText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemUnitPriceText1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemUnitPriceText1KeyTyped

    private void itemQtyText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyText1FocusGained
        itemQtyText1.selectAll();
    }//GEN-LAST:event_itemQtyText1FocusGained

    private void itemQtyText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyText1KeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_itemQtyText1KeyTyped

    private void chargesText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesText1FocusGained
        chargesText1.selectAll();
    }//GEN-LAST:event_chargesText1FocusGained

    private void chargesText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chargesText1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_chargesText1KeyTyped

    private void discountTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_discountTextFocusGained
        discountText.selectAll();
    }//GEN-LAST:event_discountTextFocusGained

    private void discountTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_discountTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_discountTextKeyTyped

    private void discountText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_discountText1FocusGained
        discountText1.selectAll();
    }//GEN-LAST:event_discountText1FocusGained

    private void discountText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_discountText1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_discountText1KeyTyped

    private void multiCurrencyComboBoxKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_multiCurrencyComboBoxKeyTyped
       
    }//GEN-LAST:event_multiCurrencyComboBoxKeyTyped

    //methode to clear all fields
    void clearAllFields() {
        try {
            saveButton.setText("<HTML><U>S</U>ave<HTML>");
            billDateFormattedText.setDate(new java.util.Date());
            CommonService.clearTextFields(new JTextField[]{invoiceNumberText, supplierText, deliveryDateFormattedText, salesTaxText});
            CommonService.clearTextArea(new JTextArea[]{addressTextArea});
            CommonService.clearCurrencyFields(new JTextField[]{chargesText});
            multiCurrencyComboBox.setSelectedIndex(0);
            taxComboBox.setSelectedIndex(0);
            chargesComboBox.setSelectedIndex(0);
            shipmentComboBox.setSelectedIndex(0);
            purchaseItemList = new ArrayList<PurchaseItem>();
            purchaseTaxList = new ArrayList<PurchaseTax>();
            purchaseChargesList = new ArrayList<PurchaseCharge>();
            supplierLedger = null;

            grandTotal = 0.00;
            subTotal = 0.00;
            numberProperty = numberPropertyDAO.findInvoiceNo("Purchase");
            invoiceNumberText.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
            Object currencyItem = multiCurrencyComboBox.getSelectedItem();
            currency = (MultiCurrency) ((ComboKeyValue) currencyItem).getValue();
            String currencySymbol = (null == currency) ? "" : currency.getSymbol();
            grandTotalLabel.setText(currencySymbol + " " + grandTotal);
            netAmountLabel.setText(currencySymbol + "" + netTotal);
            discountText.setText(grandTotal + "");
            purchaseItemTableModel.setRowCount(0);

            availableQtyLabel.setText("");
            netAmountLabel.setText("");
            purchase = null;
             saveButton.setText("Save");
        } catch (Exception e) {
            log.error("clearAllFields:", e);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel InvoiceDetailsPanel;
    private javax.swing.JButton addItemButton;
    private javax.swing.JTextArea addressTextArea;
    private javax.swing.JLabel availableQtyLabel;
    private com.toedter.calendar.JDateChooser billDateFormattedText;
    private javax.swing.JPanel bottomPanel;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton chargesAddButton;
    private javax.swing.JComboBox chargesComboBox;
    private javax.swing.JButton chargesMinusButton;
    private javax.swing.JPanel chargesPanel;
    private javax.swing.JPanel chargesPanel1;
    private javax.swing.JFormattedTextField chargesText;
    private javax.swing.JFormattedTextField chargesText1;
    private javax.swing.JTextField deliveryDateFormattedText;
    private javax.swing.JPanel discountPanel;
    private javax.swing.JFormattedTextField discountText;
    private javax.swing.JFormattedTextField discountText1;
    private javax.swing.JTextField godownText;
    private javax.swing.JLabel grandTotalLabel;
    private javax.swing.JPanel grandTotalPanel1;
    private javax.swing.JTextField invoiceNumberText;
    private javax.swing.JTextField itemCategoryText;
    private javax.swing.JTextField itemCodeText;
    private javax.swing.JTextField itemDescriptionText;
    private javax.swing.JPanel itemInfoPanel;
    private javax.swing.JTextField itemQtyText;
    private javax.swing.JTextField itemQtyText1;
    private javax.swing.JButton itemRemoveButton;
    private javax.swing.JFormattedTextField itemUnitPriceText;
    private javax.swing.JFormattedTextField itemUnitPriceText1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JComboBox multiCurrencyComboBox;
    private javax.swing.JLabel netAmountLabel;
    private javax.swing.JButton newButton;
    private javax.swing.JTable purchaseItemTable;
    private javax.swing.JPanel qtyPanel;
    private javax.swing.JTextField salesTaxText;
    private javax.swing.JButton saveButton;
    private javax.swing.JComboBox shipmentComboBox;
    private javax.swing.JPanel shipmentModePanel;
    private javax.swing.JPanel shipmentModePanel1;
    private javax.swing.JTextField supplierText;
    private javax.swing.JPanel tableBorderPanel;
    private javax.swing.JButton taxAddButton;
    private javax.swing.JComboBox taxComboBox;
    private javax.swing.JPanel taxPanel;
    private javax.swing.JButton totDiscountAddButton;
    private javax.swing.JLabel totavailableQtyLabel;
    private javax.swing.JPanel unitPricePanel;
    // End of variables declaration//GEN-END:variables
}
