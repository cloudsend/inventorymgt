/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.CTableModel;
import com.cloudsendsoft.inventory.dao.CompanyDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.LedgerGroupDAO;
import com.cloudsendsoft.inventory.dao.PurchaseDAO;
import com.cloudsendsoft.inventory.dao.PurchaseReturnDAO;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.dao.SalesReturnDAO;
import com.cloudsendsoft.inventory.dao.StockGroupDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.report.BalanceSheetReport;
import com.cloudsendsoft.inventory.report.JTableReport;
import com.cloudsendsoft.inventory.report.TrialBalanceReport;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.services.PLAService;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import static com.cloudsendsoft.inventory.view.MainForm.jMainFrame;
import static com.cloudsendsoft.inventory.view.PurchaseView.log;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;
import sun.reflect.generics.tree.Tree;

/**
 *
 * @author Sangeeth
 */
public class TrialBalanceView extends javax.swing.JPanel {

    static final Logger log = Logger.getLogger(TrialBalanceView.class.getName());

    CommonService commonService = new CommonService();

    CTableModel trialBalanceTableModel = null;

    int screen = 1;
    int index = 0;
    int pointer = 0;
    boolean isDetailedView = false;

    LedgerGroupDAO ledgerGroupDAO = new LedgerGroupDAO();
    List<LedgerGroup> listOfLedgerGroup = null;
    List<LedgerGroup> listOfLedgerGroupNew = new ArrayList<>();

    LedgerDAO ledgerDAO = new LedgerDAO();
    List<Ledger> listOfLedger = null;

    LedgerGroup ledgerGroup = null;

    double totalDebitAmt = 0.00;
    double totalCreditAmt = 0.00;

    ItemDAO itemDAO = new ItemDAO();

    List<StockGroup> listOfStockGroup = null;

    Date fromDate = GlobalProperty.getCompany().getStartDate();
    Date toDate = GlobalProperty.getCompany().getEndDate();
    PLAService plaService = new PLAService();
    SalesDAO salesDAO = new SalesDAO();
    SalesReturnDAO salesReturnDAO = new SalesReturnDAO();
    SalesPreformaDAO salesPreformaDAO = new SalesPreformaDAO();
    PurchaseReturnDAO purchaseReturnDAO = new PurchaseReturnDAO();
    PurchaseDAO purchaseDAO = new PurchaseDAO();
    HashMap<String, Double> ledgerBalanceHashMap = new HashMap<>();
    HashMap<String, Double> ledgerGroupBalanceHashMap = new HashMap<>();

    public TrialBalanceView() {
        initComponents();
        centerHeadingLabel.setText("Trial Balance as at " + commonService.sqlDateToString(GlobalProperty.company.getEndDate()));
        //table background color settings
        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);

        //set table design
        trialBalanceTable.setRowHeight(GlobalProperty.tableRowHieght);

        trialBalanceTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {

            //alternate row color jtable
            @Override
            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int column) {
                Component c = super.getTableCellRendererComponent(table,
                        value, isSelected, hasFocus, row, column);
                c.setBackground(row % 2 == 0 ? Color.GRAY : Color.LIGHT_GRAY);
                return c;
            }
        ;
        });

        trialBalanceTable.setShowGrid(false);
        trialBalanceTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{"Particulars", "Debit", "Credit"}
        ));
        trialBalanceTableModel = (CTableModel) trialBalanceTable.getModel();
        CommonService.setWidthAsPercentages(trialBalanceTable, .80, .20, .20);

        trialBalanceTableModel.setRowCount(0);

        //fetch all ledger groups
        //listOfLedgerGroup = ledgerGroupDAO.findAllByAsc();
        listOfLedgerGroup = ledgerDAO.findDistinctLedgerGroupFromLedger();

        //fetching all stockgroups
        listOfStockGroup = itemDAO.findDistinctStockGroupFromItem();
        commonService.setCurrentPeriodOnCalendar(fromDateChooser);
        commonService.setCurrentPeriodOnCalendar(toDateChooser);
        fromDateChooser.setDate(GlobalProperty.getCompany().getStartDate());
        toDateChooser.setDate(GlobalProperty.getCompany().getEndDate());
        fillTrialBalanceTable();
    }

    void fillTrialBalanceTableOld(int pointerValue) {
        try {
            trialBalanceTableModel.setRowCount(0);
            totalDebitAmt = 0.00;
            totalCreditAmt = 0.00;
            double openingBalanceTot = itemDAO.findOpeningBalanceTotal();
            if (pointer == 0) {
                //Opening Stock
                totalDebitAmt += openingBalanceTot;

                //double purchaseTotal = 0;
                //double salesTotal = 0;
                if (openingBalanceTot != 0) {
                    trialBalanceTableModel.addRow(new Object[]{"<html><b>Opening Stock</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(openingBalanceTot) + "</b></html>"});
                    //Detailed View of Op Bal.
                    if (detailViewCheckBox.isSelected()) {
                        for (StockGroup stockGroup : listOfStockGroup) {
                            List<Object[]> itemStockGroupList = itemDAO.findOpeningBalanceByItemStockGroup(stockGroup);
                            boolean isGroupNameFirst = true;
                            for (Object[] objects : itemStockGroupList) {
                                if (isGroupNameFirst && Double.parseDouble(String.valueOf(objects[1])) != 0) {
                                    trialBalanceTableModel.addRow(new Object[]{"<html>&nbsp;&nbsp;&nbsp;&nbsp;<u><b>" + stockGroup.getName() + "</b></u></html>"});
                                    isGroupNameFirst = false;
                                }
                                if (Double.parseDouble(String.valueOf(objects[1])) != 0) {
                                    trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + objects[0] + "\t\t" + commonService.formatIntoCurrencyAsString(Double.parseDouble(String.valueOf(objects[1]))) + "</i></html>"});
                                }
                            }
                        }
                    }
                }
                //pointer++;
            }
            switch (screen) {
                case 1:

                    //detailViewCheckBox.setVisible(true);
                    //totalDebitAmt = 0.00;
                    //totalCreditAmt = 0.00;
                    for (LedgerGroup ledgerGroup : listOfLedgerGroup) {
                        listOfLedger = ledgerDAO.findAllByLedgerGroup(ledgerGroup);
                        double ledgerGroupTotal = 0.00;
                        for (Ledger ledger : listOfLedger) {
                            ledgerGroupTotal += ledger.getAvailableBalance();
                        }
                        if (ledgerGroupTotal > 0 || ledgerGroupTotal < 0) {
                            if (ledgerGroup.getAccountType().equalsIgnoreCase("Asset")
                                    || ledgerGroup.getAccountType().equalsIgnoreCase("Expense")) {
                                //Debit Balance - LedgerGroup
                                if (ledgerGroupTotal > 0) {
                                    listOfLedgerGroupNew.add(ledgerGroup);
                                    trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                        + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>", ""});
                                    totalDebitAmt += ledgerGroupTotal;
                                } else if (ledgerGroupTotal < 0) {
                                    listOfLedgerGroupNew.add(ledgerGroup);
                                    trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                        + "</b></html>", "", "<html><b>" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerGroupTotal)) + "</b></html>"});
                                    totalCreditAmt += Math.abs(ledgerGroupTotal);
                                }
                                //Detailed View of Ledger
                                if (detailViewCheckBox.isSelected()) {
                                    for (Ledger ledger : listOfLedger) {
                                        if (ledger.getAvailableBalance() > 0) {
                                            trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                                + "</i></html>", "<html><i>" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>", ""});
                                        } else if (ledger.getAvailableBalance() < 0) {
                                            trialBalanceTableModel.addRow(new Object[]{"<html><i>" + ledger.getLedgerName()
                                                + "</i></html>", "", "<html><i>" + commonService.formatIntoCurrencyAsString(Math.abs(ledger.getAvailableBalance())) + "</i></html>"});
                                        }
                                    }
                                }

                            } else {
                                //Debit Balance - LedgerGroup
                                if (ledgerGroupTotal > 0) {
                                    listOfLedgerGroupNew.add(ledgerGroup);
                                    trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                        + "</b></html>", "", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                    totalCreditAmt += ledgerGroupTotal;
                                } else if (ledgerGroupTotal < 0) {
                                    listOfLedgerGroupNew.add(ledgerGroup);
                                    trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                        + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerGroupTotal)) + "</b></html>", ""});
                                    totalDebitAmt += Math.abs(ledgerGroupTotal);
                                }
                                //Detailed View of Ledger
                                if (detailViewCheckBox.isSelected()) {
                                    for (Ledger ledger : listOfLedger) {
                                        if (ledger.getAvailableBalance() > 0) {
                                            trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                                + "</i></html>", "", "<html><i>" + commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()) + "</i></html>"});
                                        } else if (ledger.getAvailableBalance() < 0) {
                                            trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                                + "</i></html>", "<html><i>" + commonService.formatIntoCurrencyAsString(Math.abs(ledger.getAvailableBalance())) + "</i></html>", ""});
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //total amount calculation
                    double diffAmount = 0.00;
                    trialBalanceTableModel.addRow(new Object[]{null});
                    if (totalDebitAmt > totalCreditAmt) {
                        diffAmount = totalDebitAmt - totalCreditAmt;
                        trialBalanceTableModel.addRow(new Object[]{"<html><b>" + "Difference" + "</b></html>", "", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmount) + "</b></html>"});
                        trialBalanceTableModel.addRow(new Object[]{null});
                        trialBalanceTableModel.addRow(new Object[]{"<html><b>" + " Total " + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(totalDebitAmt) + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(totalDebitAmt) + "</b></html>"});
                    } else {
                        diffAmount = totalCreditAmt - totalDebitAmt;
                        trialBalanceTableModel.addRow(new Object[]{"<html><b>" + "Difference" + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmount) + "</b></html>", ""});
                        trialBalanceTableModel.addRow(new Object[]{null});
                        trialBalanceTableModel.addRow(new Object[]{"<html><b>" + " Total ", "<html><b>" + commonService.formatIntoCurrencyAsString(totalCreditAmt) + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(totalCreditAmt) + "</b></html>"});
                    }
                    break;
                case 2:
                    if (index == 0) {
                        if (openingBalanceTot != 0) {
                            trialBalanceTableModel.addRow(new Object[]{"<html><b>Opening Stock</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(openingBalanceTot) + "</b></html>"});
                            //Detailed View of Op Bal.
                            //if (detailViewCheckBox.isSelected()) {
                            for (StockGroup stockGroup : listOfStockGroup) {
                                List<Object[]> itemStockGroupList = itemDAO.findOpeningBalanceByItemStockGroup(stockGroup);
                                boolean isGroupNameFirst = true;
                                for (Object[] objects : itemStockGroupList) {
                                    if (isGroupNameFirst && Double.parseDouble(String.valueOf(objects[1])) != 0) {
                                        trialBalanceTableModel.addRow(new Object[]{"<html>&nbsp;&nbsp;&nbsp;&nbsp;<u><b>" + stockGroup.getName() + "</b></u></html>"});
                                        isGroupNameFirst = false;
                                    }
                                    if (Double.parseDouble(String.valueOf(objects[1])) != 0) {
                                        trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + objects[0] + "\t\t" + commonService.formatIntoCurrencyAsString(Double.parseDouble(String.valueOf(objects[1]))) + "</i></html>"});
                                    }
                                }
                            }
                            //}
                        }
                    } //detailViewCheckBox.setVisible(false);
                    else if (index > 0)//if (null != ledgerGroup)
                    {
                        ledgerGroup = listOfLedgerGroupNew.get(index - 1);
                        listOfLedger = ledgerDAO.findAllByLedgerGroup(ledgerGroup);
                        trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName() + "</b></html>"});
                        for (Ledger ledger : listOfLedger) {
                            if (ledgerGroup.getAccountType().equalsIgnoreCase("Asset")
                                    || ledgerGroup.getAccountType().equalsIgnoreCase("Expense")) {
                                //Debit Balance
                                if (ledger.getAvailableBalance() > 0) {
                                    trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                        + "</i></html>", commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance()), ""});
                                } else if (ledger.getAvailableBalance() < 0) {
                                    trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                        + "</i></html>", "", commonService.formatIntoCurrencyAsString(Math.abs(ledger.getAvailableBalance()))});
                                }
                            } else {
                                if (ledger.getAvailableBalance() > 0) {
                                    trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                        + "</i></html>", "", commonService.formatIntoCurrencyAsString(ledger.getAvailableBalance())});
                                } else if (ledger.getAvailableBalance() < 0) {
                                    trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                        + "</i></html>", commonService.formatIntoCurrencyAsString(Math.abs(ledger.getAvailableBalance())), ""});
                                }
                            }
                        }
                    }

                    break;
            }
        } catch (Exception e) {
            log.error("fillTrialBalanceTable:", e);
        }
    }

    void fillTrialBalanceTable() {
        try {
            double subResult = 0.00;
            double salesTotal = 0.00;
            double purchaseTotal = 0.00;
            double salesReturnTotal = 0.00;
            double purchaseReturnTotal = 0.00;
            double purchaseTaxTotal = 0.00;
            double salesTaxTotal = 0.00;

            trialBalanceTableModel.setRowCount(0);
            totalDebitAmt = 0.00;
            totalCreditAmt = 0.00;
            Calendar cal = Calendar.getInstance();
            cal.setTime(fromDate);
            cal.add(Calendar.DATE, -1);
            java.util.Date prevDate = cal.getTime();
            double openingBalanceTot = 0.00;
            openingBalanceTot = plaService.findClosingStockByEndDate(commonService.utilDateToSqlDate(prevDate));
            //double closingBalanceTot = plaService.findClosingStockByDateRange(GlobalProperty.getCompany().getStartDate(), toDate);
            double closingBalanceTot = plaService.findClosingStockByEndDate(toDate);
            //double openingBalanceTot = itemDAO.findOpeningBalanceTotal();
            if (pointer == 0) {
                //Opening Stock
                totalDebitAmt += openingBalanceTot;

                //double purchaseTotal = 0;
                //double salesTotal = 0;
                if (openingBalanceTot != 0) {
                    trialBalanceTableModel.addRow(new Object[]{"<html><b>Opening Stock</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(openingBalanceTot) + "</b></html>", ""});
                    //Detailed View of Op Bal.
                    if (detailViewCheckBox.isSelected()) {
                        trialBalanceTableModel.addRow(new Object[]{"<html>&nbsp&nbsp&nbsp&nbsp<i>Please view Stock Summary for further details [Ctrl+Shift+U]</i></html>"});
                    }

                }

            }
            switch (screen) {
                case 1:
                    ledgerBalanceHashMap.clear();
                    ledgerGroupBalanceHashMap.clear();
                    listOfLedgerGroupNew.clear();
                    checkBoxPanel.setVisible(true);
                    for (LedgerGroup ledgerGroup : listOfLedgerGroup) {
                        listOfLedger = ledgerDAO.findAllByLedgerGroup(ledgerGroup);
                        double ledgerGroupTotal = 0.00;
                        for (Ledger ledger : listOfLedger) {
                            if (ledger.getLedgerGroup().getGroupName().equalsIgnoreCase("Purchase Accounts")) {
                                if (ledger.getLedgerName().equalsIgnoreCase("Purchase Account")) {
                                    subResult = purchaseDAO.findTotalPurchase(fromDate, toDate);
                                    ledgerGroupTotal += subResult;
                                } else if (ledger.getLedgerName().equalsIgnoreCase("Purchase Returns")) {
                                    subResult = purchaseReturnDAO.findTotalPurchaseReturn(fromDate, toDate);
                                    purchaseReturnTotal = subResult;
                                }
                            } else if (ledger.getLedgerGroup().getGroupName().equalsIgnoreCase("Sales Accounts")) {
                                if (ledger.getLedgerName().equalsIgnoreCase("Sales Account")) {
                                    subResult = salesDAO.findTotalSales(fromDate, toDate);
                                    ledgerGroupTotal += subResult;
                                } else if (ledger.getLedgerName().equalsIgnoreCase("Sales Returns")) {
                                    subResult = salesReturnDAO.findTotalSalesReturn(fromDate, toDate);
                                    salesReturnTotal = subResult;
                                }
                            } else {
                                if (ledgerGroup.getGroupName().equalsIgnoreCase("Duties & Taxes")) {
                                    purchaseTaxTotal = plaService.findPurchaseTax(ledger, fromDate, toDate);
                                    salesTaxTotal = plaService.findSalesTax(ledger, fromDate, toDate);
                                    subResult = purchaseTaxTotal - salesTaxTotal;
                                } else {
                                    subResult = plaService.findAvailableBalanceForBalanceSheet(ledger, fromDate, toDate);
                                }
                                ledgerGroupTotal += subResult;
                            }
                            if (ledgerBalanceHashMap.get(ledger.getLedgerName()) == null) {
                                ledgerBalanceHashMap.put(ledger.getLedgerName(), subResult);
                            } else {
                                ledgerBalanceHashMap.put(ledger.getLedgerName(), ledgerBalanceHashMap.get(ledger.getLedgerName()) + subResult);
                            }
                            subResult = 0;
                        }
                        if (ledgerGroupBalanceHashMap.get(ledgerGroup.getGroupName()) == null) {
                            if (ledgerGroup.getGroupName().equalsIgnoreCase("Purchase Accounts")) {
                                ledgerGroupBalanceHashMap.put(ledgerGroup.getGroupName(), ledgerGroupTotal - purchaseReturnTotal);
                            } else if (ledgerGroup.getGroupName().equalsIgnoreCase("Sales Accounts")) {
                                ledgerGroupBalanceHashMap.put(ledgerGroup.getGroupName(), ledgerGroupTotal - salesReturnTotal);
                            } else {
                                ledgerGroupBalanceHashMap.put(ledgerGroup.getGroupName(), ledgerGroupTotal);
                            }
                        } else {
                            if (ledgerGroup.getGroupName().equalsIgnoreCase("Purchase Accounts")) {
                                ledgerGroupBalanceHashMap.put(ledgerGroup.getGroupName(), ledgerGroupBalanceHashMap.get(ledgerGroup.getGroupName()) + (ledgerGroupTotal - purchaseReturnTotal));
                            } else if (ledgerGroup.getGroupName().equalsIgnoreCase("Sales Accounts")) {
                                ledgerGroupBalanceHashMap.put(ledgerGroup.getGroupName(), ledgerGroupBalanceHashMap.get(ledgerGroup.getGroupName()) + (ledgerGroupTotal - salesReturnTotal));
                            } else if (!ledgerGroup.getGroupName().equalsIgnoreCase("Duties & Taxes")) {
                                ledgerGroupBalanceHashMap.put(ledgerGroup.getGroupName(), ledgerGroupBalanceHashMap.get(ledgerGroup.getGroupName()) + ledgerGroupTotal);
                            }
                        }
                        if (ledgerGroupTotal > 0 || ledgerGroupTotal < 0) {
                            if (ledgerGroup.getAccountType().equalsIgnoreCase("Asset")
                                    || ledgerGroup.getAccountType().equalsIgnoreCase("Direct Expense") || ledgerGroup.getAccountType().equalsIgnoreCase("Indirect Expense")) {
                                //Debit Balance - LedgerGroup
                                if (ledgerGroupTotal > 0) {
                                    listOfLedgerGroupNew.add(ledgerGroup);
                                    if (ledgerGroup.getGroupName().trim().equalsIgnoreCase("Purchase Accounts")) {
                                        ledgerGroupTotal -= purchaseReturnTotal;
                                    }
                                    trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                        + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>", ""});
                                    totalDebitAmt += ledgerGroupTotal;
                                } else if (ledgerGroupTotal < 0) {
                                    listOfLedgerGroupNew.add(ledgerGroup);
                                    if (ledgerGroup.getGroupName().trim().equalsIgnoreCase("Purchase Accounts")) {
                                        ledgerGroupTotal += purchaseReturnTotal;
                                    }
                                    trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                        + "</b></html>", "", "<html><b>" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerGroupTotal)) + "</b></html>"});
                                    totalCreditAmt += Math.abs(ledgerGroupTotal);
                                }
                                //Detailed View of Ledger
                                if (detailViewCheckBox.isSelected()) {
                                    for (Ledger ledger : listOfLedger) {
                                        if (ledgerBalanceHashMap.get(ledger.getLedgerName()) != null) {
                                            if (ledgerBalanceHashMap.get(ledger.getLedgerName()) > 0) {
                                                trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                                    + "&nbsp;&nbsp;&nbsp;&nbsp; " + commonService.formatIntoCurrencyAsString(Math.abs(ledgerBalanceHashMap.get(ledger.getLedgerName()))) + "</i></html>", "", ""});
                                            } else if (ledgerBalanceHashMap.get(ledger.getLedgerName()) < 0) {
                                                trialBalanceTableModel.addRow(new Object[]{"<html><i>" + ledger.getLedgerName()
                                                    + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerBalanceHashMap.get(ledger.getLedgerName()))) + "</i></html>", "", ""});
                                            }
                                        }
                                    }
                                }
                            } else {
                                //Debit Balance - LedgerGroup
                                if (ledgerGroup.getGroupName().equalsIgnoreCase("Duties & Taxes")) {
                                    listOfLedgerGroupNew.add(ledgerGroup);
                                    if (ledgerGroupTotal > 0) {
                                        trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                            + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>", ""});
                                        totalDebitAmt += ledgerGroupTotal;
                                    } else if (ledgerGroupTotal < 0) {
                                        trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                            + "</b></html>", "", "<html><b>" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerGroupTotal)) + "</b></html>"});
                                        totalCreditAmt += ledgerGroupTotal;
                                    }
                                } else if (ledgerGroupTotal > 0) {
                                    listOfLedgerGroupNew.add(ledgerGroup);
                                    if (ledgerGroup.getGroupName().trim().equalsIgnoreCase("Sales Accounts")) {
                                        ledgerGroupTotal -= salesReturnTotal;
                                    }
                                    trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                        + "</b></html>", "", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                    totalCreditAmt += ledgerGroupTotal;
                                } else if (ledgerGroupTotal < 0) {
                                    listOfLedgerGroupNew.add(ledgerGroup);
                                    if (ledgerGroup.getGroupName().trim().equalsIgnoreCase("Sales Accounts")) {
                                        ledgerGroupTotal += salesReturnTotal;
                                    }
                                    trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                        + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerGroupTotal)) + "</b></html>", ""});
                                    totalDebitAmt += Math.abs(ledgerGroupTotal);
                                }
                                //Detailed View of Ledger
                                if (detailViewCheckBox.isSelected()) {
                                    for (Ledger ledger : listOfLedger) {
                                        if (ledgerBalanceHashMap.get(ledger.getLedgerName()) != null) {
                                            if (ledgerBalanceHashMap.get(ledger.getLedgerName()) > 0) {
                                                trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                                    + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledgerBalanceHashMap.get(ledger.getLedgerName())) + "</i></html>", "", ""});
                                            } else if (ledgerBalanceHashMap.get(ledger.getLedgerName()) < 0) {
                                                trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                                    + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerBalanceHashMap.get(ledger.getLedgerName()))) + "</i></html>", "", ""});
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //closing stock
                    //clsing stock in trial balance --- as adjustments
//                    totalDebitAmt += closingBalanceTot;
//                    if (closingBalanceTot != 0) {
//                        trialBalanceTableModel.addRow(new Object[]{"<html><b>Closing Stock</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(closingBalanceTot) + "</b></html>", ""});
//                        if (detailViewCheckBox.isSelected()) {
//                            trialBalanceTableModel.addRow(new Object[]{"<html>&nbsp&nbsp&nbsp&nbsp<i>Please view Stock Summary for further details [Ctrl+Shift+U]</i></html>"});
//                        }
//
//                    }
                    //total amount calculation
                    double diffAmount = 0.00;
                    trialBalanceTableModel.addRow(new Object[]{null});
                    if (totalDebitAmt > totalCreditAmt) {
                        diffAmount = totalDebitAmt - totalCreditAmt;
                        trialBalanceTableModel.addRow(new Object[]{"<html><b>" + "Difference" + "</b></html>", "", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmount) + "</b></html>"});
                        trialBalanceTableModel.addRow(new Object[]{null});
                        trialBalanceTableModel.addRow(new Object[]{"<html><b>" + " Total " + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(totalDebitAmt) + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(totalDebitAmt) + "</b></html>"});
                    } else {
                        diffAmount = totalCreditAmt - totalDebitAmt;
                        trialBalanceTableModel.addRow(new Object[]{"<html><b>" + "Difference" + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmount) + "</b></html>", ""});
                        trialBalanceTableModel.addRow(new Object[]{null});
                        trialBalanceTableModel.addRow(new Object[]{"<html><b>" + " Total ", "<html><b>" + commonService.formatIntoCurrencyAsString(totalCreditAmt) + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(totalCreditAmt) + "</b></html>"});
                    }
                    if (closingBalanceTot != 0) {
                        trialBalanceTableModel.addRow(new Object[]{""});
                        trialBalanceTableModel.addRow(new Object[]{"<html><u><b>Adjustments</b></u></html>"});
                        trialBalanceTableModel.addRow(new Object[]{"<html><b>Closing Stock</b>&nbsp;&nbsp;&nbsp;&nbsp;<b>" + commonService.formatIntoCurrencyAsString(closingBalanceTot) + "</b></html>", "",""});
                    if (detailViewCheckBox.isSelected()) {
                        trialBalanceTableModel.addRow(new Object[]{"<html>&nbsp&nbsp&nbsp&nbsp<i>Please view Stock Summary for further details [Ctrl+Shift+U]</i></html>"});
                    }

                    }
                    break;
                case 2:
                    checkBoxPanel.setVisible(false);
                    if (index == 0) {
                        if (openingBalanceTot != 0) {
                            trialBalanceTableModel.addRow(new Object[]{"<html><b>Opening Stock</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(openingBalanceTot) + "</b></html>", ""});
                            trialBalanceTableModel.addRow(new Object[]{"<html>&nbsp&nbsp&nbsp&nbsp<i>Please view Stock Summary for further details [Ctrl+Shift+U]</i></html>"});
                        }
                    } //detailViewCheckBox.setVisible(false);
                    else if (index > 0 && index <= listOfLedgerGroupNew.size())//if (null != ledgerGroup)
                    {
                        ledgerGroup = listOfLedgerGroupNew.get(index - 1);
                        listOfLedger = ledgerDAO.findAllByLedgerGroup(ledgerGroup);
                        double ledgerGroupTotal = 0.00;

                        if (ledgerGroupBalanceHashMap.get("Purchase Accounts") != null) {
                            ledgerGroupTotal = ledgerGroupBalanceHashMap.get(ledgerGroup.getGroupName());
                        }
                        if (ledgerGroupTotal > 0) {
                            trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>", ""});
                        } else if (ledgerGroupTotal < 0) {
                            trialBalanceTableModel.addRow(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                + "</b></html>", "", "<html><b>" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerGroupTotal)) + "</b></html>"});
                        }
                        for (Ledger ledger : listOfLedger) {
                            if (ledgerGroup.getAccountType().equalsIgnoreCase("Asset")
                                    || ledgerGroup.getAccountType().equalsIgnoreCase("Expense")) {
                                //Debit Balance
                                if (ledgerBalanceHashMap.get(ledger.getLedgerName()) > 0) {
                                    trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName() + "&nbsp;&nbsp; "
                                        + commonService.formatIntoCurrencyAsString(ledgerBalanceHashMap.get(ledger.getLedgerName())) + "</i></html>"});
                                } else if (ledgerBalanceHashMap.get(ledger.getLedgerName()) < 0) {

                                    trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName() + "&nbsp;&nbsp; "
                                        + commonService.formatIntoCurrencyAsString(Math.abs(ledgerBalanceHashMap.get(ledger.getLedgerName()))) + "</i></html>"});
                                }
                            } else {
                                if (ledgerBalanceHashMap.get(ledger.getLedgerName()) > 0) {

                                    trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName() + "&nbsp;&nbsp;"
                                        + commonService.formatIntoCurrencyAsString(ledgerBalanceHashMap.get(ledger.getLedgerName())) + "</i></html>"});
                                } else if (ledgerBalanceHashMap.get(ledger.getLedgerName()) < 0) {

                                    trialBalanceTableModel.addRow(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName() + "&nbsp;&nbsp;"
                                        + commonService.formatIntoCurrencyAsString(Math.abs(ledgerBalanceHashMap.get(ledger.getLedgerName()))) + "</i></html>"});
                                }
                            }
                        }
                    } else {
                        if (closingBalanceTot != 0) {
                            trialBalanceTableModel.addRow(new Object[]{"<html><b>Closing Stock</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(closingBalanceTot) + "</b></html>", ""});
                            trialBalanceTableModel.addRow(new Object[]{"<html>&nbsp&nbsp&nbsp&nbsp<i>Please view Stock Summary for further details [Ctrl+Shift+U]</i></html>"});
                        }
                    }
                    break;
            }
        } catch (Exception e) {
            log.error("fillTrialBalanceTable:", e);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headingPanel = new javax.swing.JPanel();
        centerHeadingLabel = new javax.swing.JLabel();
        printPanel = new javax.swing.JPanel();
        printButton = new javax.swing.JButton();
        pdfButton = new javax.swing.JButton();
        checkBoxPanel = new javax.swing.JPanel();
        detailViewCheckBox = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        fromDateChooser = new com.toedter.calendar.JDateChooser();
        toDateChooser = new com.toedter.calendar.JDateChooser();
        jLabel2 = new javax.swing.JLabel();
        filterButton = new javax.swing.JButton();
        bodyPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        trialBalanceTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        headingPanel.setBackground(new java.awt.Color(47, 105, 142));
        headingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        headingPanel.setPreferredSize(new java.awt.Dimension(1124, 50));

        centerHeadingLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        centerHeadingLabel.setForeground(new java.awt.Color(255, 255, 255));
        centerHeadingLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        centerHeadingLabel.setText("Trial Balance ");

        printPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        printPanel.setOpaque(false);
        printPanel.setLayout(new java.awt.GridLayout(1, 0));

        printButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/print.png"))); // NOI18N
        printButton.setText("Print");
        printButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printButtonActionPerformed(evt);
            }
        });
        printPanel.add(printButton);

        pdfButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/pdf-icon.png"))); // NOI18N
        pdfButton.setText("PDF");
        pdfButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdfButtonActionPerformed(evt);
            }
        });
        printPanel.add(pdfButton);

        checkBoxPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        checkBoxPanel.setOpaque(false);

        detailViewCheckBox.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        detailViewCheckBox.setForeground(new java.awt.Color(255, 255, 255));
        detailViewCheckBox.setText("Detailed View");
        detailViewCheckBox.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        detailViewCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        detailViewCheckBox.setOpaque(false);
        detailViewCheckBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                detailViewCheckBoxMouseEntered(evt);
            }
        });
        detailViewCheckBox.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                detailViewCheckBoxStateChanged(evt);
            }
        });
        detailViewCheckBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                detailViewCheckBoxItemStateChanged(evt);
            }
        });
        detailViewCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                detailViewCheckBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout checkBoxPanelLayout = new javax.swing.GroupLayout(checkBoxPanel);
        checkBoxPanel.setLayout(checkBoxPanelLayout);
        checkBoxPanelLayout.setHorizontalGroup(
            checkBoxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(detailViewCheckBox, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
        );
        checkBoxPanelLayout.setVerticalGroup(
            checkBoxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(checkBoxPanelLayout.createSequentialGroup()
                .addComponent(detailViewCheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("From");

        fromDateChooser.setDateFormatString("dd/MM/yyyy");
        fromDateChooser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fromDateChooserMouseClicked(evt);
            }
        });
        fromDateChooser.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                fromDateChooserFocusLost(evt);
            }
        });
        fromDateChooser.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                fromDateChooserPropertyChange(evt);
            }
        });
        fromDateChooser.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                fromDateChooserInputMethodTextChanged(evt);
            }
        });

        toDateChooser.setDateFormatString("dd/MM/yyyy");
        toDateChooser.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                toDateChooserFocusGained(evt);
            }
        });
        toDateChooser.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                toDateChooserPropertyChange(evt);
            }
        });

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("To");

        filterButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/search.png"))); // NOI18N
        filterButton.setText("Filter");
        filterButton.setOpaque(false);
        filterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filterButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout headingPanelLayout = new javax.swing.GroupLayout(headingPanel);
        headingPanel.setLayout(headingPanelLayout);
        headingPanelLayout.setHorizontalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addComponent(checkBoxPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(centerHeadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fromDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(toDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(filterButton)
                .addGap(45, 45, 45)
                .addComponent(printPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        headingPanelLayout.setVerticalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(printPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(checkBoxPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(centerHeadingLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(headingPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(fromDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)
                            .addComponent(toDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(filterButton))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(headingPanel, java.awt.BorderLayout.PAGE_START);

        bodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        bodyPanel.setOpaque(false);

        trialBalanceTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        trialBalanceTable.setOpaque(false);
        trialBalanceTable.setSelectionBackground(new java.awt.Color(153, 153, 153));
        trialBalanceTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                trialBalanceTableMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                trialBalanceTableMouseEntered(evt);
            }
        });
        trialBalanceTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                trialBalanceTableKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(trialBalanceTable);

        javax.swing.GroupLayout bodyPanelLayout = new javax.swing.GroupLayout(bodyPanel);
        bodyPanel.setLayout(bodyPanelLayout);
        bodyPanelLayout.setHorizontalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1128, Short.MAX_VALUE)
        );
        bodyPanelLayout.setVerticalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 397, Short.MAX_VALUE)
        );

        add(bodyPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void trialBalanceTableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_trialBalanceTableKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE && screen > 1) {
            screen--;
            pointer = 0;
            fillTrialBalanceTable();

        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            pointer++;
            index = trialBalanceTable.getSelectedRow();
            if (trialBalanceTable.getValueAt(index, 0).toString().equalsIgnoreCase("<html><b> Total ") || trialBalanceTable.getValueAt(index, 0).toString().equalsIgnoreCase("Difference") || trialBalanceTable.getValueAt(index, 0).toString().equalsIgnoreCase("")) {
                pointer = 0;
                MsgBox.warning("Invalid Selection");
            } else if (detailViewCheckBox.isSelected()) {
                pointer = 0;
                MsgBox.warning("Detailed View is Selected");
            } else if (screen < 2) {
                if (trialBalanceTable.getValueAt(0, 0).toString().equalsIgnoreCase("<html><b>Opening Stock</b></html>")) {
                    screen++;
                    fillTrialBalanceTable();
                } else {
                    index++;
                    screen++;
                    fillTrialBalanceTable();
                }

            }
        }
    }//GEN-LAST:event_trialBalanceTableKeyPressed

    private void trialBalanceTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_trialBalanceTableMouseClicked

        try {
            if (evt.getClickCount() == 2) {
                pointer++;
                index = trialBalanceTable.rowAtPoint(evt.getPoint());
                if (trialBalanceTable.getValueAt(index, 0) == null || trialBalanceTable.getValueAt(index, 0).toString().equalsIgnoreCase("<html><b> Total ") || trialBalanceTable.getValueAt(index, 0).toString().equalsIgnoreCase("<html><b>Difference</b></html>")) {
                    pointer = 0;
                    MsgBox.warning("Invalid Selection");
                } else if (detailViewCheckBox.isSelected()) {
                    pointer = 0;
                    MsgBox.warning("Detailed View is Selected");
                } else if (screen < 2) {
                    if (trialBalanceTable.getValueAt(0, 0).toString().equalsIgnoreCase("<html><b>Opening Stock</b></html>")) {
                        screen++;
                        clickAction();
                        //screen++;
                        //fillStockSummaryTable();
                    } else {
                        index++;
                        screen++;

                        clickAction();
                    }
                }
            }
        } catch (Exception e) {
            log.error("stockSummaryTableMouseClicked", e);
        }
    }//GEN-LAST:event_trialBalanceTableMouseClicked

    private void detailViewCheckBoxStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_detailViewCheckBoxStateChanged

    }//GEN-LAST:event_detailViewCheckBoxStateChanged

    private void printButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printButtonActionPerformed
        try {
            TrialBalanceReport trialBalanceReport = new TrialBalanceReport();
            if (MsgBox.confirm("Are you sure you want to Print?")) {
                if (trialBalanceReport.createTrialBalanceReport(trialBalanceTable, "TrialBalance", "print")) {

                } else {
                    MsgBox.warning("Something went wrong, please re-click the button again!");
                }
            }
        } catch (Exception ex) {
            log.error("printButtonActionPerformed:", ex);
        }
    }//GEN-LAST:event_printButtonActionPerformed

    private void pdfButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdfButtonActionPerformed
        try {
            TrialBalanceReport trialBalanceReport = new TrialBalanceReport();
            if (MsgBox.confirm("Are you sure you want to create pdf?")) {
                if (trialBalanceReport.createTrialBalanceReport(trialBalanceTable, "TrialBalance", "pdf")) {

                } else {
                    MsgBox.warning("Something went wrong, please re-click the button again!");
                }
            }
        } catch (Exception ex) {
            log.error("pdfButtonActionPerformed:", ex);
        }
    }//GEN-LAST:event_pdfButtonActionPerformed

    private void trialBalanceTableMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_trialBalanceTableMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_trialBalanceTableMouseEntered

    private void fromDateChooserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fromDateChooserMouseClicked

    }//GEN-LAST:event_fromDateChooserMouseClicked

    private void fromDateChooserFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fromDateChooserFocusLost

    }//GEN-LAST:event_fromDateChooserFocusLost

    private void fromDateChooserInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_fromDateChooserInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_fromDateChooserInputMethodTextChanged

    private void toDateChooserFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_toDateChooserFocusGained

    }//GEN-LAST:event_toDateChooserFocusGained

    private void filterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filterButtonActionPerformed
        try {
            fromDate = commonService.jDateChooserToSqlDate(fromDateChooser);
            toDate = commonService.jDateChooserToSqlDate(toDateChooser);

        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(TrialBalanceView.class.getName()).log(Level.SEVERE, null, ex);
        }
        listOfLedgerGroupNew.clear();
        fillTrialBalanceTable();
    }//GEN-LAST:event_filterButtonActionPerformed

    private void detailViewCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_detailViewCheckBoxActionPerformed
        pointer = 0;
        fillTrialBalanceTable();
    }//GEN-LAST:event_detailViewCheckBoxActionPerformed

    private void fromDateChooserPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_fromDateChooserPropertyChange
        toDateChooser.setMinSelectableDate(fromDateChooser.getDate());
    }//GEN-LAST:event_fromDateChooserPropertyChange

    private void toDateChooserPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_toDateChooserPropertyChange
        fromDateChooser.setMaxSelectableDate(toDateChooser.getDate());
    }//GEN-LAST:event_toDateChooserPropertyChange

    private void detailViewCheckBoxMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_detailViewCheckBoxMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_detailViewCheckBoxMouseEntered

    private void detailViewCheckBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_detailViewCheckBoxItemStateChanged

    }//GEN-LAST:event_detailViewCheckBoxItemStateChanged

    void clickAction() {
        try {
            switch (screen) {
                case 2:
//                    if(index==0)
//                    {
//                        MsgBox.warning("Opening Balance");
//                        break;
//                    }
//                    else
//                    {
                    //ledgerGroup = listOfLedgerGroup.get(index);
                    // ledgerGroup = listOfLedgerGroupNew.get(index);
                    //listOfLedgerGroupNew.add(ledgerGroup);
                    // System.out.println("listOfLedgerGroupNew  :"+listOfLedgerGroupNew.get(index).getGroupName());
                    //System.out.println("ledgerGroup: "+ledgerGroup.getGroupName());
                    fillTrialBalanceTable();

                    break;
//                    }
                case 3:
                    // item = listOfItem.get(index);
                    //fillStockSummaryTable();
                    break;
                case 4:
                    // month = selectedFirstCol.trim();
                    if (index > 0) {
//                        month = months[index - 1];
                        // fillStockSummaryTable();
                    } else {
                        screen--;
                    }
                    break;
                case 5:
                    ArrayList row = CommonService.getTableRowData(trialBalanceTable);
                    if (row.size() > 0) {
                        //   invoiceDate = (String) row.get(0);
                        //  invoiceType = (String) row.get(2);
                        //  invoiceNumber = (String) row.get(3);
                        //fillStockSummaryTable();
                    }

                    break;

            }
            //screen++;

        } catch (Exception e) {
            log.error("clickAction:", e);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bodyPanel;
    private javax.swing.JLabel centerHeadingLabel;
    private javax.swing.JPanel checkBoxPanel;
    private javax.swing.JCheckBox detailViewCheckBox;
    private javax.swing.JButton filterButton;
    private com.toedter.calendar.JDateChooser fromDateChooser;
    private javax.swing.JPanel headingPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton pdfButton;
    private javax.swing.JButton printButton;
    private javax.swing.JPanel printPanel;
    private com.toedter.calendar.JDateChooser toDateChooser;
    private javax.swing.JTable trialBalanceTable;
    // End of variables declaration//GEN-END:variables

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    //HibernateUtil hibernateUtil=new HibernateUtil();

                    CompanyDAO companyDAO = new CompanyDAO();
                    GlobalProperty.setCompany(companyDAO.getDefaultCompany());
                    MainForm jMainFrame = new MainForm();
                    jMainFrame.setVisible(true);
                    jMainFrame.setExtendedState(MainForm.MAXIMIZED_BOTH);

                    TrialBalanceView trialBalanceView = new TrialBalanceView();
                    jMainFrame.getContentPanel().removeAll();
                    jMainFrame.getContentPanel().repaint();
                    jMainFrame.getContentPanel().revalidate();
                    jMainFrame.getContentPanel().add(trialBalanceView);

                } catch (Exception e) {
                    log.error("MainForm run :", e);
                }
            }
        });
    }
}
