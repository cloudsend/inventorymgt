/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ButtonColumn;
import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.components.PendingSalesTableModal;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.MultiCurrencyDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.dao.ShipmentModeDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.ItemService;
import com.cloudsendsoft.inventory.services.LedgerService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseCharge;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseTax;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesCharge;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreformaTax;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaCharge;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesTax;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class PendingCommercialInvoiceView extends javax.swing.JPanel {

    /**
     * Creates new form PurchaseViewPanel
     */
    static double totalAmount = 0;
    double subTotal = 0.00;
    double netTotal = 0.00;
    double totDiscount = 0.00;
    double grandTotal = 0.00;
    static final Logger log = Logger.getLogger(PendingCommercialInvoiceView.class.getName());
    PendingSalesTableModal salesPreformaItemTableModel = null;

    ItemDAO itemDAO = new ItemDAO();
    CommonService commonService = new CommonService();
    JPanel contentPanel = null;
    LedgerService ledgerService = new LedgerService();
    TaxDAO taxDAO = new TaxDAO();
    ChargesDAO chargesDAO = new ChargesDAO();
    MultiCurrencyDAO multiCurrencyDAO = new MultiCurrencyDAO();
    ShipmentModeDAO shipmentModeDAO = new ShipmentModeDAO();
    SalesPreformaDAO salesPreformaDAO = new SalesPreformaDAO();
    List<SalesPreforma> salesPreformaList = null;
    //global usage
    Ledger customerLedger = null;
    long itemQty = 0;
    ArrayList<SalesItem> salesItemList = new ArrayList<SalesItem>();
    Item item = null;
    ArrayList<SalesTax> salesTaxList = new ArrayList<SalesTax>();
    ArrayList<SalesCharge> salesChargesList = new ArrayList<SalesCharge>();

    CRUDServices cRUDServices = new CRUDServices();
    MultiCurrency currency = null;
    NumberProperty numberProperty = null;
    NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
    SalesItem salesItem = null;
    
    boolean isStoreKeeper=false;

    public PendingCommercialInvoiceView(final JPanel contentPanel) {

        try {
            initComponents();

            if(GlobalProperty.listOfPrivileges.contains("StoreKeeper")){
                isStoreKeeper=true;
            }
            //table background color removed
            jScrollPane1.setOpaque(false);
            jScrollPane1.getViewport().setOpaque(false);

            //billDateFormattedText.setText("hhhhhhh");
            this.contentPanel = contentPanel;
            salesItemTable.setRowHeight(25);

            if (!isStoreKeeper) {
                salesItemTable.setModel(new com.cloudsendsoft.inventory.components.PendingSalesTableModal(
                        null,
                        new String[]{"Sr.No.", "Invoice No", "Bill Date", "Customer", "Expiry Date", "Delivery", "Packing", "ShipmentMode", "Packing List", "ComercialInvoice"}
                ));
                salesPreformaItemTableModel = (PendingSalesTableModal) (DefaultTableModel) salesItemTable.getModel();
                CommonService.setWidthAsPercentages(salesItemTable, .02, .03, .10, .04, .04, .04, .04, .04, .05, .06);
            }else{//Store keeper - list without commercial invoice
                salesItemTable.setModel(new com.cloudsendsoft.inventory.components.PendingSalesTableModal(
                        null,
                        new String[]{"Sr.No.", "Invoice No", "Bill Date", "Customer", "Expiry Date", "Delivery", "Packing", "ShipmentMode", "Packing List"}
                ));
                salesPreformaItemTableModel = (PendingSalesTableModal) (DefaultTableModel) salesItemTable.getModel();
                CommonService.setWidthAsPercentages(salesItemTable, .02, .03, .10, .04, .04, .04, .04, .04, .05);
            }
            
            
            
            //salesPreformaList=salesPreformaDAO.findAll();
            salesPreformaItemTableModel.setRowCount(0);
            salesPreformaList = salesPreformaDAO.findAllPendingCommercialInvoice();

            if (salesPreformaList.size() > 0) {
                int slNo=1;
                for (SalesPreforma salesPreforma : salesPreformaList) {
                    salesPreformaItemTableModel.addRow(new Object[]{slNo++, salesPreforma.getInvoiceNumber(), commonService.sqlDateToString(salesPreforma.getBillDate()), ((salesPreforma.getAddress() == null) ? "" :  salesPreforma.getAddress()),
                        commonService.sqlDateToString(salesPreforma.getExpiryDate()), salesPreforma.getDelivery(), salesPreforma.getPacking(),
                        (salesPreforma.getShipmentMode()==null)?"":salesPreforma.getShipmentMode().getName(), (salesPreforma.getPackingList() == null) ? "Create Packing List" : "Packing List",
                        (salesPreforma.getCommercialInvoice() == null) ? "Deliver ComercialInvoice" : "Delivered ComercialInvoice"});
                }
            } else {
                salesItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                        null,
                        new String[]{""}
                ));
                DefaultTableModel defaultTableModel = (DefaultTableModel) salesItemTable.getModel();
                DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
                centerRenderer.setHorizontalAlignment(JLabel.CENTER);
                salesItemTable.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
                defaultTableModel.addRow(new Object[]{"You don't have any Pending Sales Proforma Invoices !"});
            }

            //packing list button action
            final Action packingListAction = new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    //System.out.println("Clicked!"); // not fired
                    JTable table = (JTable) e.getSource();
                    int modelRow = Integer.valueOf(e.getActionCommand());
                    SalesPreforma salesPreforma = salesPreformaList.get(modelRow);

                    PackingListView packingListView = new PackingListView(salesPreforma);
                    contentPanel.removeAll();
                    contentPanel.repaint();
                    contentPanel.revalidate();
                    contentPanel.add(packingListView);
                }
            };

            //commercialInvoiceAction button action
            final Action commercialInvoiceAction = new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    //System.out.println("Clicked!"); // not fired
                    JTable table = (JTable) e.getSource();
                    int modelRow = Integer.valueOf(e.getActionCommand());
                    SalesPreforma salesPreforma = salesPreformaList.get(modelRow);

                    CommercialInvoiceView commercialInvoiceView = new CommercialInvoiceView(salesPreforma);
                    contentPanel.removeAll();
                    contentPanel.repaint();
                    contentPanel.revalidate();
                    contentPanel.add(commercialInvoiceView);
                }
            };

            if (salesItemTable.getColumnCount() > 7) {
                ButtonColumn buttonColumn = new ButtonColumn(salesItemTable, packingListAction, 8);
                if(!isStoreKeeper){
                    ButtonColumn buttonColumn1 = new ButtonColumn(salesItemTable, commercialInvoiceAction, 9);
                }
            }

        } catch (Exception e) {
            log.error("PendingCommercialInvoiceView:", e);
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tableBorderPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        salesItemTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        tableBorderPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("PendingCommercialInvoiceView"));
        tableBorderPanel.setOpaque(false);

        jScrollPane1.setName(""); // NOI18N
        jScrollPane1.setOpaque(false);

        salesItemTable.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        salesItemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        salesItemTable.setOpaque(false);
        salesItemTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                salesItemTableMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                salesItemTableMouseEntered(evt);
            }
        });
        jScrollPane1.setViewportView(salesItemTable);

        javax.swing.GroupLayout tableBorderPanelLayout = new javax.swing.GroupLayout(tableBorderPanel);
        tableBorderPanel.setLayout(tableBorderPanelLayout);
        tableBorderPanelLayout.setHorizontalGroup(
            tableBorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tableBorderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1493, Short.MAX_VALUE)
                .addContainerGap())
        );
        tableBorderPanelLayout.setVerticalGroup(
            tableBorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tableBorderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 542, Short.MAX_VALUE)
                .addContainerGap())
        );

        add(tableBorderPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents
    private void salesItemTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salesItemTableMouseClicked
    }//GEN-LAST:event_salesItemTableMouseClicked
    private void salesItemTableMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salesItemTableMouseEntered
    }//GEN-LAST:event_salesItemTableMouseEntered
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable salesItemTable;
    private javax.swing.JPanel tableBorderPanel;
    // End of variables declaration//GEN-END:variables
}
