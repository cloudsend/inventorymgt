package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.dao.StockGroupDAO;
import com.cloudsendsoft.inventory.dao.UnitDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.ItemService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.model.Unit;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.awt.TextField;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author sangeeth
 */
public class NumberPropertyView extends javax.swing.JInternalFrame {

    /**
     * Creates new form ItemInfo
     */
    static final Logger log = Logger.getLogger(NumberPropertyView.class.getName());
    CRUDServices cRUDServices = new CRUDServices();
    ;
    NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
    List<NumberProperty> listOfNumberProperty = null;

    public NumberPropertyView() {
        initComponents();

        if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
            mainPanel.remove(buttonPanel);
            mainPanel.repaint();
            mainPanel.revalidate();
        }

        listOfNumberProperty = numberPropertyDAO.findAll();

        salesPrefixTextField.setText(listOfNumberProperty.get(0).getCategoryPrefix());
        salesNoTextField.setText(String.valueOf(listOfNumberProperty.get(0).getNumber()));
        salesPreformaPrefixTextField.setText(listOfNumberProperty.get(1).getCategoryPrefix());
        salesPreformaNoTextField.setText(String.valueOf(listOfNumberProperty.get(1).getNumber()));
        paymentPrefixTextField.setText(listOfNumberProperty.get(2).getCategoryPrefix());
        paymentNoTextField.setText(String.valueOf(listOfNumberProperty.get(2).getNumber()));
        receiptPrefixTextField.setText(listOfNumberProperty.get(3).getCategoryPrefix());
        receiptNoTextField.setText(String.valueOf(listOfNumberProperty.get(3).getNumber()));
        contraPrefixTextField.setText(listOfNumberProperty.get(4).getCategoryPrefix());
        contraNoTextField.setText(String.valueOf(listOfNumberProperty.get(4).getNumber()));
        journalPrefixTextField.setText(listOfNumberProperty.get(5).getCategoryPrefix());
        journalNoTextField.setText(String.valueOf(listOfNumberProperty.get(5).getNumber()));
        comInvoicePrefixTextField.setText(listOfNumberProperty.get(6).getCategoryPrefix());
        comInvoiceNoTextField.setText(String.valueOf(listOfNumberProperty.get(6).getNumber()));
        purchaseReturnPrefixTextField.setText(listOfNumberProperty.get(7).getCategoryPrefix());
        purchaseReturnNoTextField.setText(String.valueOf(listOfNumberProperty.get(7).getNumber()));
        salesReturnPrefixTextField.setText(listOfNumberProperty.get(8).getCategoryPrefix());
        salesReturnNoTextField.setText(String.valueOf(listOfNumberProperty.get(8).getNumber()));
        purchasePrefixTextField.setText(listOfNumberProperty.get(9).getCategoryPrefix());
        purchaseNoTextField.setText(String.valueOf(listOfNumberProperty.get(9).getNumber()));
        purchaseOrderPrefixTextField.setText(listOfNumberProperty.get(12).getCategoryPrefix());
        purchaseOrderNoTextField.setText(String.valueOf(listOfNumberProperty.get(12).getNumber()));
        /*this.getRootPane().setDefaultButton(hideShowButton);
         for(Unit unit:UnitDAO.findAll()){
         unitCombo.addItem(unit.getName());
         }

         for(StockGroup stockGroup:stockGroupDAO.findAll()){
         groupCombo.addItem(stockGroup.getName());
         }
        
         itemCodeText.setNextFocusableComponent(descriptionText);
         descriptionText.setNextFocusableComponent(unitCombo);
         unitCombo.setNextFocusableComponent(openingQtyText);
         openingQtyText.setNextFocusableComponent(openingBalText);
         openingBalText.setNextFocusableComponent(hideShowButton);
         hideShowButton.setNextFocusableComponent(itemCodeText);
       
         itemTable.setModel(new com.cloudsend.inventory.components.CTableModel(
         null,
         new String[]{
         "Product ID", "Item Name", "Unit","Selling Price","Available Quantity"
         }
         ));
         itemTable.setRowHeight(GlobalProperty.tableRowHieght);
        
         itemService.fillAddItemTable(itemTable);*/
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        salesLabel = new javax.swing.JLabel();
        salesPreformaLabel = new javax.swing.JLabel();
        paymentLabel = new javax.swing.JLabel();
        receiptLabel = new javax.swing.JLabel();
        salesPrefixTextField = new javax.swing.JTextField();
        salesNoTextField = new javax.swing.JTextField();
        salesPreformaPrefixTextField = new javax.swing.JTextField();
        salesPreformaNoTextField = new javax.swing.JTextField();
        paymentPrefixTextField = new javax.swing.JTextField();
        paymentNoTextField = new javax.swing.JTextField();
        receiptPrefixTextField = new javax.swing.JTextField();
        receiptNoTextField = new javax.swing.JTextField();
        contraPrefixTextField = new javax.swing.JTextField();
        contraNoTextField = new javax.swing.JTextField();
        receiptLabel1 = new javax.swing.JLabel();
        journalNoTextField = new javax.swing.JTextField();
        journalPrefixTextField = new javax.swing.JTextField();
        receiptLabel2 = new javax.swing.JLabel();
        comInvoiceNoTextField = new javax.swing.JTextField();
        comInvoicePrefixTextField = new javax.swing.JTextField();
        receiptLabel3 = new javax.swing.JLabel();
        purchaseReturnPrefixTextField = new javax.swing.JTextField();
        salesReturnPrefixTextField = new javax.swing.JTextField();
        purchaseReturnNoTextField = new javax.swing.JTextField();
        salesReturnNoTextField = new javax.swing.JTextField();
        receiptLabel4 = new javax.swing.JLabel();
        receiptLabel5 = new javax.swing.JLabel();
        purchaseNoTextField = new javax.swing.JTextField();
        purchasePrefixTextField = new javax.swing.JTextField();
        receiptLabel6 = new javax.swing.JLabel();
        purchaseOrderNoTextField = new javax.swing.JTextField();
        purchaseOrderPrefixTextField = new javax.swing.JTextField();
        receiptLabel7 = new javax.swing.JLabel();
        buttonPanel = new javax.swing.JPanel();
        saveNoPropertyButton = new javax.swing.JButton();

        setClosable(true);
        setTitle("NumberProperty");
        getContentPane().setLayout(new java.awt.CardLayout());

        mainPanel.setBackground(new java.awt.Color(255, 255, 255));

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel4.setOpaque(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Category");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Prefix");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Number");

        salesLabel.setText("Sales");

        salesPreformaLabel.setText("SalesPreforma");

        paymentLabel.setText("Payment");

        receiptLabel.setText("Receipt");

        receiptLabel1.setText("Contra");

        receiptLabel2.setText("Journal");

        receiptLabel3.setText("Commercial Invoice");

        salesReturnNoTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salesReturnNoTextFieldActionPerformed(evt);
            }
        });

        receiptLabel4.setText("Purchase Return");

        receiptLabel5.setText("Sales Return");

        purchaseNoTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                purchaseNoTextFieldActionPerformed(evt);
            }
        });

        purchasePrefixTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                purchasePrefixTextFieldActionPerformed(evt);
            }
        });

        receiptLabel6.setText("Purchase");

        purchaseOrderNoTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                purchaseOrderNoTextFieldActionPerformed(evt);
            }
        });

        purchaseOrderPrefixTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                purchaseOrderPrefixTextFieldActionPerformed(evt);
            }
        });

        receiptLabel7.setText("Purchase Order");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(salesLabel)
                            .addComponent(salesPreformaLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(receiptLabel)
                            .addComponent(paymentLabel)
                            .addComponent(receiptLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(receiptLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(receiptLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(receiptLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(receiptLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(receiptLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(receiptLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(46, 46, 46)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(salesPrefixTextField)
                    .addComponent(salesPreformaPrefixTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 71, Short.MAX_VALUE)
                    .addComponent(paymentPrefixTextField)
                    .addComponent(receiptPrefixTextField)
                    .addComponent(contraPrefixTextField)
                    .addComponent(journalPrefixTextField)
                    .addComponent(comInvoicePrefixTextField)
                    .addComponent(purchaseReturnPrefixTextField)
                    .addComponent(salesReturnPrefixTextField)
                    .addComponent(purchasePrefixTextField)
                    .addComponent(purchaseOrderPrefixTextField))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(68, 68, 68)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(97, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(72, 72, 72)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(salesPreformaNoTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(salesNoTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(paymentNoTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(receiptNoTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(contraNoTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(journalNoTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comInvoiceNoTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(purchaseReturnNoTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(salesReturnNoTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(purchaseNoTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(purchaseOrderNoTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(43, 43, 43))))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSeparator1)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(salesPrefixTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(salesNoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(salesLabel))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(salesPreformaPrefixTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(salesPreformaNoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(salesPreformaLabel))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(paymentPrefixTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(paymentNoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(paymentLabel))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(receiptPrefixTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(receiptLabel)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(receiptNoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(contraPrefixTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(contraNoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(receiptLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(journalNoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(journalPrefixTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(receiptLabel2)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comInvoiceNoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comInvoicePrefixTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(receiptLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(purchaseReturnPrefixTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(receiptLabel4))
                    .addComponent(purchaseReturnNoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(salesReturnPrefixTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(salesReturnNoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(receiptLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(purchasePrefixTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(receiptLabel6))
                    .addComponent(purchaseNoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(purchaseOrderNoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(receiptLabel7)
                        .addComponent(purchaseOrderPrefixTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buttonPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        buttonPanel.setOpaque(false);

        saveNoPropertyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveNoPropertyButton.setText("Save");
        saveNoPropertyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveNoPropertyButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout buttonPanelLayout = new javax.swing.GroupLayout(buttonPanel);
        buttonPanel.setLayout(buttonPanelLayout);
        buttonPanelLayout.setHorizontalGroup(
            buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(buttonPanelLayout.createSequentialGroup()
                .addGap(183, 183, 183)
                .addComponent(saveNoPropertyButton, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        buttonPanelLayout.setVerticalGroup(
            buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, buttonPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(saveNoPropertyButton, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        getContentPane().add(mainPanel, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void saveNoPropertyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveNoPropertyButtonActionPerformed
        try {
            int sflag1 = 1, sflag2 = 1, sflag3 = 1, sflag4 = 1, sflag5 = 1, sflag6 = 1, sflag7 = 1, sflag8 = 1, sflag9 = 1, sflag10 = 1, sflag11=1;
            //int fflag1,fflag2,fflag3,fflag4;
            if (salesNoTextField.getText().length() > 0 && salesPreformaNoTextField.getText().length() > 0 && paymentNoTextField.getText().length() > 0 && receiptNoTextField.getText().length() > 0 && contraNoTextField.getText().length() > 0 && journalNoTextField.getText().length() > 0 && purchaseNoTextField.getText().length() > 0 && purchaseOrderNoTextField.getText().length() > 0) {
                if (MsgBox.confirm("Are you sure you want to save?")) {
                    Boolean valSales, valSalesPreforma, valPayment, valReceipt, valContra, valJournal, valComInvoice, valPurchaseReturn, valSalesReturn, valPurchase, valPurchaseOrder;
                    NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
                    valSales = numberPropertyDAO.isSalesExist(salesPrefixTextField.getText().trim() + salesNoTextField.getText().trim());
                    if (valSales == false) {
                        listOfNumberProperty.get(0).setCategoryPrefix(salesPrefixTextField.getText());
                        listOfNumberProperty.get(0).setNumber(Long.parseLong(salesNoTextField.getText()));
                        cRUDServices.saveOrUpdateModel(listOfNumberProperty.get(0));
                        sflag1 = 0;
                    } else {
                        MsgBox.warning("Sales Invoice Already Exist");
                    }
                    valSalesPreforma = numberPropertyDAO.isSalesPreformaExist(salesPreformaPrefixTextField.getText().trim() + salesPreformaNoTextField.getText().trim());
                    if (valSalesPreforma == false) {
                        listOfNumberProperty.get(1).setCategoryPrefix(salesPreformaPrefixTextField.getText());
                        listOfNumberProperty.get(1).setNumber(Long.parseLong(salesPreformaNoTextField.getText()));
                        cRUDServices.saveOrUpdateModel(listOfNumberProperty.get(1));
                        sflag2 = 0;
                    } else {
                        MsgBox.warning("SalesPreforma Invoice Already Exist");
                    }
                    valPayment = numberPropertyDAO.isPaymentExist(paymentPrefixTextField.getText().trim() + paymentNoTextField.getText().trim());
                    if (valPayment == false) {
                        listOfNumberProperty.get(2).setCategoryPrefix(paymentPrefixTextField.getText());
                        listOfNumberProperty.get(2).setNumber(Long.parseLong(paymentNoTextField.getText()));
                        cRUDServices.saveOrUpdateModel(listOfNumberProperty.get(2));
                        sflag3 = 0;
                    } else {
                        MsgBox.warning("PaymentNo Already Exist");
                    }
                    valReceipt = numberPropertyDAO.isReceiptExist(receiptPrefixTextField.getText().trim() + receiptNoTextField.getText().trim());
                    if (valReceipt == false) {
                        listOfNumberProperty.get(3).setCategoryPrefix(receiptPrefixTextField.getText());
                        listOfNumberProperty.get(3).setNumber(Long.parseLong(receiptNoTextField.getText()));
                        cRUDServices.saveOrUpdateModel(listOfNumberProperty.get(3));
                        sflag4 = 0;
                    } else {
                        MsgBox.warning("ReceiptNo Already Exist");
                    }
                    valContra = numberPropertyDAO.isContraExist(contraPrefixTextField.getText().trim() + contraNoTextField.getText().trim());
                    if (valContra == false) {
                        listOfNumberProperty.get(4).setCategoryPrefix(contraPrefixTextField.getText());
                        listOfNumberProperty.get(4).setNumber(Long.parseLong(contraNoTextField.getText()));
                        cRUDServices.saveOrUpdateModel(listOfNumberProperty.get(4));
                        sflag5 = 0;
                    } else {
                        MsgBox.warning("Contra Already Exist");
                    }
                    valJournal = numberPropertyDAO.isJournalExist(journalPrefixTextField.getText().trim() + journalNoTextField.getText().trim());
                    if (valJournal == false) {
                        listOfNumberProperty.get(5).setCategoryPrefix(journalPrefixTextField.getText());
                        listOfNumberProperty.get(5).setNumber(Long.parseLong(journalNoTextField.getText()));
                        cRUDServices.saveOrUpdateModel(listOfNumberProperty.get(5));
                        sflag6 = 0;
                    } else {
                        MsgBox.warning("Journal Already Exist");
                    }
                    valComInvoice = numberPropertyDAO.isComInvoiceExist(comInvoicePrefixTextField.getText().trim() + comInvoiceNoTextField.getText().trim());
                    if (valComInvoice == false) {
                        listOfNumberProperty.get(6).setCategoryPrefix(comInvoicePrefixTextField.getText());
                        listOfNumberProperty.get(6).setNumber(Long.parseLong(comInvoiceNoTextField.getText()));
                        cRUDServices.saveOrUpdateModel(listOfNumberProperty.get(6));
                        sflag7 = 0;
                    } else {
                        MsgBox.warning("Commercial Invoice Already Exist");
                    }
                    valPurchaseReturn = numberPropertyDAO.isPurchaseReturnExist(purchaseReturnPrefixTextField.getText().trim() + purchaseReturnNoTextField.getText().trim());
                    if (valPurchaseReturn == false) {
                        listOfNumberProperty.get(7).setCategoryPrefix(purchaseReturnPrefixTextField.getText());
                        listOfNumberProperty.get(7).setNumber(Long.parseLong(purchaseReturnNoTextField.getText()));
                        cRUDServices.saveOrUpdateModel(listOfNumberProperty.get(7));
                        sflag8 = 0;
                    } else {
                        MsgBox.warning("Purchase Return Already Exist");
                    }
                    valSalesReturn = numberPropertyDAO.isSalesReturnExist(salesReturnPrefixTextField.getText().trim() + salesReturnNoTextField.getText().trim());
                    if (valSalesReturn == false) {
                        listOfNumberProperty.get(8).setCategoryPrefix(salesReturnPrefixTextField.getText());
                        listOfNumberProperty.get(8).setNumber(Long.parseLong(salesReturnNoTextField.getText()));
                        cRUDServices.saveOrUpdateModel(listOfNumberProperty.get(8));
                        sflag9 = 0;
                    } else {
                        MsgBox.warning("Sales Return Already Exist");
                    }
                    valPurchase = numberPropertyDAO.isPurchaseExist(purchasePrefixTextField.getText().trim() + purchaseNoTextField.getText().trim());
                    if (valPurchase == false) {
                        listOfNumberProperty.get(9).setCategoryPrefix(purchasePrefixTextField.getText());
                        listOfNumberProperty.get(9).setNumber(Long.parseLong(purchaseNoTextField.getText()));
                        cRUDServices.saveOrUpdateModel(listOfNumberProperty.get(9));
                        sflag10 = 0;
                    } else {
                        MsgBox.warning("Purchase Already Exist");
                    }
                    valPurchaseOrder = numberPropertyDAO.isPurchaseOrderExist(purchaseOrderPrefixTextField.getText().trim() + purchaseOrderNoTextField.getText().trim());
                    if (valPurchaseOrder == false) {
                        listOfNumberProperty.get(12).setCategoryPrefix(purchaseOrderPrefixTextField.getText());
                        listOfNumberProperty.get(12).setNumber(Long.parseLong(purchaseOrderNoTextField.getText()));
                        cRUDServices.saveOrUpdateModel(listOfNumberProperty.get(12));
                        sflag11 = 0;
                    } else {
                        MsgBox.warning("PurchaseOrder Already Exist");
                    }
                    if (sflag1 == 0 && sflag2 == 0 && sflag3 == 0 && sflag4 == 0 && sflag5 == 0 && sflag6 == 0 && sflag7 == 0 && sflag8 == 0 && sflag9 == 0 && sflag10 == 0 && sflag11 == 0) {
                        MsgBox.success("Successfully Updated");
                    }
                }
            } else {
                MsgBox.warning("Number cannot be empty");
            }
            /*UnitDAO unitDAO = new UnitDAO();
             String productId = itemCodeText.getText().trim();
             if (!itemDAO.isItemCodeExist(productId)) {
             String itemName = descriptionText.getText().trim();
             Unit unit = unitDAO.findByName(unitCombo.getSelectedItem().toString());
             StockGroup stockGroup = stockGroupDAO.findByName(groupCombo.getSelectedItem().toString());
             if (CommonService.stringValidator(new String[]{productId, itemName})) {
             Item item = new Item();
             item.setVisible(true);
             item.setCompany(GlobalProperty.getCompany());
             item.setItemCode(productId);
             item.setDescription(itemName);
             item.setStockGroup(stockGroup);
             item.setUnit(unit);
             item.setWeight(Double.parseDouble(weightText.getText()));
             if (sellingPriceText.getText().length() > 0) {
             item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
             }
             item.setOpeningDate(CommonService.strToSqlDate(openingDateFormattedText.getText()));
                    
             if (openingQtyText.getText().length() > 0) {
             item.setOpeningQty(Integer.parseInt(openingQtyText.getText()));
             item.setAvailableQty(Integer.parseInt(openingQtyText.getText()));
             }
                    
             if (openingBalText.getText().length() > 0) {
             item.setOpeningBal(Double.parseDouble(openingBalText.getText()));
             }
                    
             cRUDServices.saveModel(item);
             MsgBox.success("Successfully Added " + productId);
             CommonService.clearTextFields(new JTextField[]{itemCodeText, descriptionText,openingQtyText});
             CommonService.clearCombo(new JComboBox[]{groupCombo,unitCombo});
             CommonService.clearCurrencyFields(new JTextField[]{weightText,sellingPriceText,openingQtyText,openingBalText});
             itemService.fillAddItemTable(itemTable);
             itemCodeText.requestFocusInWindow();
             } else {
             MsgBox.warning("Product Id and Item Name are mandatory.");
             itemCodeText.requestFocusInWindow();
             }
             } else {
             MsgBox.abort("Product Id '" + productId + "' is already exists.");
             itemCodeText.requestFocusInWindow();
             }*/

        } catch (Exception e) {
            e.printStackTrace();
            log.error("saveButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_saveNoPropertyButtonActionPerformed

    private void salesReturnNoTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salesReturnNoTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_salesReturnNoTextFieldActionPerformed

    private void purchaseNoTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_purchaseNoTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_purchaseNoTextFieldActionPerformed

    private void purchasePrefixTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_purchasePrefixTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_purchasePrefixTextFieldActionPerformed

    private void purchaseOrderNoTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_purchaseOrderNoTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_purchaseOrderNoTextFieldActionPerformed

    private void purchaseOrderPrefixTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_purchaseOrderPrefixTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_purchaseOrderPrefixTextFieldActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JTextField comInvoiceNoTextField;
    private javax.swing.JTextField comInvoicePrefixTextField;
    private javax.swing.JTextField contraNoTextField;
    private javax.swing.JTextField contraPrefixTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField journalNoTextField;
    private javax.swing.JTextField journalPrefixTextField;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JLabel paymentLabel;
    private javax.swing.JTextField paymentNoTextField;
    private javax.swing.JTextField paymentPrefixTextField;
    private javax.swing.JTextField purchaseNoTextField;
    private javax.swing.JTextField purchaseOrderNoTextField;
    private javax.swing.JTextField purchaseOrderPrefixTextField;
    private javax.swing.JTextField purchasePrefixTextField;
    private javax.swing.JTextField purchaseReturnNoTextField;
    private javax.swing.JTextField purchaseReturnPrefixTextField;
    private javax.swing.JLabel receiptLabel;
    private javax.swing.JLabel receiptLabel1;
    private javax.swing.JLabel receiptLabel2;
    private javax.swing.JLabel receiptLabel3;
    private javax.swing.JLabel receiptLabel4;
    private javax.swing.JLabel receiptLabel5;
    private javax.swing.JLabel receiptLabel6;
    private javax.swing.JLabel receiptLabel7;
    private javax.swing.JTextField receiptNoTextField;
    private javax.swing.JTextField receiptPrefixTextField;
    private javax.swing.JLabel salesLabel;
    private javax.swing.JTextField salesNoTextField;
    private javax.swing.JTextField salesPrefixTextField;
    private javax.swing.JLabel salesPreformaLabel;
    private javax.swing.JTextField salesPreformaNoTextField;
    private javax.swing.JTextField salesPreformaPrefixTextField;
    private javax.swing.JTextField salesReturnNoTextField;
    private javax.swing.JTextField salesReturnPrefixTextField;
    private javax.swing.JButton saveNoPropertyButton;
    // End of variables declaration//GEN-END:variables
}
