package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.GodownDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.StockGroupDAO;
import com.cloudsendsoft.inventory.dao.UnitDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.ItemService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Godown;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.model.Unit;
import com.cloudsendsoft.inventory.report.BarcodeReport;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import static com.cloudsendsoft.inventory.utilities.GlobalProperty.mainForm;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.awt.Color;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.InputStream;
import static java.lang.Double.NaN;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class ItemInfo extends javax.swing.JInternalFrame {

    /**
     * Creates new form ItemInfo
     */
    static final Logger log = Logger.getLogger(ItemInfo.class.getName());
    CRUDServices cRUDServices = new CRUDServices();
    
    ItemDAO itemDAO = new ItemDAO();
    ItemService itemService = new ItemService();
    UnitDAO unitDAO = new UnitDAO();
    GodownDAO godownDAO = new GodownDAO();
    StockGroupDAO stockGroupDAO = new StockGroupDAO();
    ArrayList<Godown> godownList = new ArrayList<Godown>();
    List<Godown> godownListAll = null;
    MultiCurrency currency = null;
    Item item=null;
    List<Item> items;
            
    boolean isDuplicateColumnsVisible = false;

    public ItemInfo() {
        initComponents();
        //Godown hide
        godownLabel.setVisible(false);
        godownComboBox.setVisible(false);
        
        items = itemDAO.findAllByDESC();
        barcodeRowText.setText("1");
        
        itemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{
                    "Product ID", "Item Name", "Unit", "Selling Price", "Available Quantity",""
                }
        )); 
        
        //shortcut
        // HERE ARE THE KEY BINDINGS
        //removed additional qty & rate textboxes by default
        openingQtyPanel.remove(openingQtyText1);
        openingUnitPricePanel.remove(openingUnitPriceText1);
        openingBalancePanel.remove(openingBalText1);

        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                put(KeyStroke.getKeyStroke(KeyEvent.VK_W,
                                java.awt.event.InputEvent.SHIFT_DOWN_MASK
                                | java.awt.event.InputEvent.CTRL_DOWN_MASK), "forward");
        this.getActionMap().put("forward", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isDuplicateColumnsVisible) {
                    openingQtyPanel.add(openingQtyText1);
                    openingUnitPricePanel.add(openingUnitPriceText1);
                    openingBalancePanel.add(openingBalText1);

                    itemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                            null,
                            new String[]{
                                "Product ID", "Item Name", "Unit", "Selling Price", "Available Qty", "Available Qty1",""
                            }
                    ));
                    itemService.fillAddItemTable(itemTable,items,isDuplicateColumnsVisible);
                    
                    isDuplicateColumnsVisible = false;
                    
                } else {
                    openingQtyPanel.remove(openingQtyText1);
                    openingUnitPricePanel.remove(openingUnitPriceText1);
                    openingBalancePanel.remove(openingBalText1);

                    itemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                            null,
                            new String[]{
                                "Product ID", "Item Name", "Unit", "Selling Price", "Available Quantity",""
                            }
                    ));
                    itemService.fillAddItemTable(itemTable,items,isDuplicateColumnsVisible);
                    
                    isDuplicateColumnsVisible = true;
                }
                openingPanel.revalidate();
                
            }
        });
            // END OF KEY BINDINGS
        //shortcut
        setFocusOrder();

        if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
            saveItemButton.setEnabled(false);
            newButton.setEnabled(false);
            updateButton.setEnabled(false);
            hideShowButton.setEnabled(false);
        }

        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);

        //load combo box
        for (Godown godown : godownDAO.findAll()) {
            //System.out.println("godown.getName():" + godown.getName());
            godownComboBox.addItem(new ComboKeyValue((godown.getName().trim()), godown));
        }
        godownComboBox.setSelectedIndex(0);
        this.getRootPane().setDefaultButton(hideShowButton);
        for (Unit unit : UnitDAO.findAll()) {
            unitCombo.addItem(unit.getName());
        }

        for (StockGroup stockGroup : stockGroupDAO.findAll()) {
            groupCombo.addItem(stockGroup.getName());
        }

        itemTable.setRowHeight(GlobalProperty.tableRowHieght);

        
        itemService.fillAddItemTable(itemTable,items,isDuplicateColumnsVisible);

    }

    void setFocusOrder() {
        try {
            itemCodeText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    descriptionText.requestFocusInWindow();
                }
            });
            descriptionText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    groupCombo.requestFocusInWindow();
                }
            });
            groupCombo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    unitCombo.requestFocusInWindow();
                }
            });
            unitCombo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    weightText.requestFocusInWindow();
                }
            });
            weightText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    sellingPriceText.requestFocusInWindow();
                }
            });
            sellingPriceText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    godownComboBox.requestFocusInWindow();
                }
            });
            godownComboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveItemButton.requestFocusInWindow();
                }
            });
            
            
            barcodeRowText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    barcodePrintButton.requestFocusInWindow();
                }
            });
        } catch (Exception e) {
            log.error("setFocusOrder:", e);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        tablePanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        itemTable = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        unitCombo = new javax.swing.JComboBox();
        descriptionText = new javax.swing.JTextField();
        itemCodeText = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        sellingPriceText = new javax.swing.JFormattedTextField();
        groupCombo = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        weightText = new javax.swing.JFormattedTextField();
        godownComboBox = new javax.swing.JComboBox();
        godownLabel = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        buttonPanel = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        saveItemButton = new javax.swing.JButton();
        updateButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        hideShowButton = new javax.swing.JButton();
        barcodePanel = new javax.swing.JPanel();
        barcodeRowText = new javax.swing.JFormattedTextField();
        barcodePrintButton = new javax.swing.JButton();
        openingPanel = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        openingQtyPanel = new javax.swing.JPanel();
        openingQtyText = new javax.swing.JTextField();
        openingQtyText1 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        openingUnitPricePanel = new javax.swing.JPanel();
        openingUnitPriceText = new javax.swing.JFormattedTextField();
        openingUnitPriceText1 = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        openingBalancePanel = new javax.swing.JPanel();
        openingBalText = new javax.swing.JFormattedTextField();
        openingBalText1 = new javax.swing.JFormattedTextField();

        setClosable(true);
        setResizable(true);
        setTitle("Inventory");
        setPreferredSize(new java.awt.Dimension(650, 700));

        mainPanel.setBackground(new java.awt.Color(255, 255, 255));
        mainPanel.setPreferredSize(new java.awt.Dimension(566, 653));

        tablePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Item List"));
        tablePanel.setOpaque(false);

        itemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        itemTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                itemTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(itemTable);

        javax.swing.GroupLayout tablePanelLayout = new javax.swing.GroupLayout(tablePanel);
        tablePanel.setLayout(tablePanelLayout);
        tablePanelLayout.setHorizontalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tablePanelLayout.createSequentialGroup()
                .addComponent(jScrollPane1)
                .addGap(0, 0, 0))
        );
        tablePanelLayout.setVerticalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE)
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel4.setOpaque(false);

        descriptionText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                descriptionTextKeyPressed(evt);
            }
        });

        itemCodeText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemCodeTextKeyPressed(evt);
            }
        });

        jLabel1.setText("Item Code");

        jLabel2.setText("Description");

        jLabel3.setText("Unit");

        jLabel6.setText("Selling Price");

        sellingPriceText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        sellingPriceText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        sellingPriceText.setText("0.00");
        sellingPriceText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                sellingPriceTextFocusGained(evt);
            }
        });
        sellingPriceText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                sellingPriceTextKeyTyped(evt);
            }
        });

        jLabel7.setText("Group");

        jLabel8.setText("Weight");

        weightText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        weightText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        weightText.setText("0.00");
        weightText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                weightTextFocusGained(evt);
            }
        });
        weightText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                weightTextKeyTyped(evt);
            }
        });

        godownComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                godownComboBoxItemStateChanged(evt);
            }
        });

        godownLabel.setText("Godown");

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("KG");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel7))
                        .addGap(28, 28, 28)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(descriptionText, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(groupCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(itemCodeText)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(jLabel3))
                            .addComponent(godownLabel))
                        .addGap(42, 42, 42)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(unitCombo, 0, 92, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(weightText, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel11))
                            .addComponent(sellingPriceText, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(godownComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(33, 33, 33))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(83, 83, 83)
                        .addComponent(jLabel3))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(80, 80, 80)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(jLabel8)
                                .addComponent(weightText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(unitCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(itemCodeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(descriptionText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(groupCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(sellingPriceText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(godownLabel)
                    .addComponent(godownComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buttonPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new java.awt.GridLayout(0, 1));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("New");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(newButton);

        saveItemButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveItemButton.setText("Save");
        saveItemButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveItemButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(saveItemButton);

        updateButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/update.png"))); // NOI18N
        updateButton.setText("Update");
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(updateButton);

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/delete-icon.png"))); // NOI18N
        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(deleteButton);

        hideShowButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/hideShow.png"))); // NOI18N
        hideShowButton.setText("Hide Stock");
        hideShowButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hideShowButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(hideShowButton);

        barcodePanel.setLayout(new java.awt.GridLayout(1, 0));

        barcodeRowText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        barcodeRowText.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        barcodeRowText.setText("1");
        barcodeRowText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                barcodeRowTextFocusGained(evt);
            }
        });
        barcodeRowText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                barcodeRowTextKeyTyped(evt);
            }
        });
        barcodePanel.add(barcodeRowText);

        barcodePrintButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/195-barcode-icon.png"))); // NOI18N
        barcodePrintButton.setText("Print Barcode");
        barcodePrintButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                barcodePrintButtonActionPerformed(evt);
            }
        });
        barcodePanel.add(barcodePrintButton);

        buttonPanel.add(barcodePanel);

        openingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Opening Bal [Optional]"));
        openingPanel.setOpaque(false);

        jLabel4.setText("Qty :");

        openingQtyPanel.setLayout(new java.awt.GridLayout(1, 0));

        openingQtyText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        openingQtyText.setText("0");
        openingQtyText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                openingQtyTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                openingQtyTextFocusLost(evt);
            }
        });
        openingQtyText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                openingQtyTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                openingQtyTextKeyTyped(evt);
            }
        });
        openingQtyPanel.add(openingQtyText);

        openingQtyText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        openingQtyText1.setText("0");
        openingQtyText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                openingQtyText1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                openingQtyText1FocusLost(evt);
            }
        });
        openingQtyText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                openingQtyText1KeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                openingQtyText1KeyTyped(evt);
            }
        });
        openingQtyPanel.add(openingQtyText1);

        jLabel10.setText("Unit Price");

        openingUnitPricePanel.setLayout(new java.awt.GridLayout(1, 0));

        openingUnitPriceText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        openingUnitPriceText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        openingUnitPriceText.setText("0.00");
        openingUnitPriceText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openingUnitPriceTextActionPerformed(evt);
            }
        });
        openingUnitPriceText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                openingUnitPriceTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                openingUnitPriceTextFocusLost(evt);
            }
        });
        openingUnitPriceText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                openingUnitPriceTextKeyTyped(evt);
            }
        });
        openingUnitPricePanel.add(openingUnitPriceText);

        openingUnitPriceText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        openingUnitPriceText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        openingUnitPriceText1.setText("0.00");
        openingUnitPriceText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                openingUnitPriceText1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                openingUnitPriceText1FocusLost(evt);
            }
        });
        openingUnitPriceText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                openingUnitPriceText1KeyTyped(evt);
            }
        });
        openingUnitPricePanel.add(openingUnitPriceText1);

        jLabel5.setText("Opening Bal");

        openingBalancePanel.setLayout(new java.awt.GridLayout(1, 0));

        openingBalText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        openingBalText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        openingBalText.setText("0.00");
        openingBalText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                openingBalTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                openingBalTextFocusLost(evt);
            }
        });
        openingBalText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                openingBalTextKeyTyped(evt);
            }
        });
        openingBalancePanel.add(openingBalText);

        openingBalText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        openingBalText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        openingBalText1.setText("0.00");
        openingBalText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                openingBalText1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                openingBalText1FocusLost(evt);
            }
        });
        openingBalText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                openingBalText1KeyTyped(evt);
            }
        });
        openingBalancePanel.add(openingBalText1);

        javax.swing.GroupLayout openingPanelLayout = new javax.swing.GroupLayout(openingPanel);
        openingPanel.setLayout(openingPanelLayout);
        openingPanelLayout.setHorizontalGroup(
            openingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(openingPanelLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(openingQtyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(openingUnitPricePanel, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(openingBalancePanel, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        openingPanelLayout.setVerticalGroup(
            openingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(openingQtyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(openingUnitPricePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(openingBalancePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tablePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, mainPanelLayout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE))
                    .addComponent(openingPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(openingPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tablePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 634, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 671, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void openingBalTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_openingBalTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_openingBalTextKeyTyped

    private void openingQtyTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_openingQtyTextKeyPressed

    }//GEN-LAST:event_openingQtyTextKeyPressed

    private void openingQtyTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_openingQtyTextKeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_openingQtyTextKeyTyped

    private void openingBalTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_openingBalTextFocusGained
        openingBalText.selectAll();
    }//GEN-LAST:event_openingBalTextFocusGained

    private void openingQtyTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_openingQtyTextFocusGained
        openingQtyText.selectAll();
    }//GEN-LAST:event_openingQtyTextFocusGained

    private void itemTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_itemTableMouseClicked
        try {
            CommonService commonService = new CommonService();
            ArrayList selectedRow = commonService.getTableRowData(itemTable);
            item = itemDAO.findItemByItemCode(selectedRow.get(0) + "",selectedRow.get(1) + "","Main Godown");
            itemCodeText.setText(item.getItemCode());
            descriptionText.setText(item.getDescription());
            openingQtyText.setText(item.getOpeningQty() + "");
            openingQtyText1.setText(item.getOpeningQty1() + "");
            openingUnitPriceText.setText(item.getOpeningUnitPrice() + "");
            openingUnitPriceText1.setText(item.getOpeningUnitPrice1() + "");
            openingBalText.setText(item.getOpeningBal()+"");
            openingBalText1.setText(item.getOpeningBal1()+"");
            unitCombo.setSelectedItem(item.getUnit().getName() + "");
            weightText.setText(item.getWeight() + "");
            groupCombo.setSelectedItem(item.getStockGroup().getName() + "");
            sellingPriceText.setText(item.getSellingPrice() + "");

            int count = godownComboBox.getItemCount();
            for (int i = 0; i < count; i++) {
                ComboKeyValue ckv = (ComboKeyValue) godownComboBox.getItemAt(i);
                if (ckv.getKey().trim().equalsIgnoreCase(item.getGodown().getName())) {
                    godownComboBox.setSelectedItem(ckv);
                }
            }

            ////// ComboKeyValue ckv = (ComboKeyValue) item.getGodown();
            /////godownComboBox.setSelectedItem(item.getGodown());
            godownComboBox.setEnabled(false);
            //openingDateFormattedText.setText(CommonService.sqlDateToString(item.getOpeningDate()));
            //openingQtyText.setText(item.getOpeningQty() + "");
            //openingBalText.setText(item.getOpeningBal() + "");
            if (item.isVisible() == true) {
                hideShowButton.setText("Hide Stock");
            } else {
                hideShowButton.setText("Show Stock");
            }
        } catch (Exception e) {
            log.error("itemTableMouseClicked", e);
        }
    }//GEN-LAST:event_itemTableMouseClicked

    private void sellingPriceTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_sellingPriceTextFocusGained
        sellingPriceText.selectAll();
    }//GEN-LAST:event_sellingPriceTextFocusGained

    private void sellingPriceTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_sellingPriceTextKeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_sellingPriceTextKeyTyped

    private void weightTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_weightTextFocusGained
        weightText.selectAll();
    }//GEN-LAST:event_weightTextFocusGained

    private void weightTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_weightTextKeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_weightTextKeyTyped

    private void godownComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_godownComboBoxItemStateChanged

    }//GEN-LAST:event_godownComboBoxItemStateChanged

    private void openingUnitPriceTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_openingUnitPriceTextFocusGained
        openingUnitPriceText.selectAll();
    }//GEN-LAST:event_openingUnitPriceTextFocusGained

    private void openingUnitPriceTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_openingUnitPriceTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_openingUnitPriceTextKeyTyped

    private void openingUnitPriceTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_openingUnitPriceTextFocusLost
        if (openingUnitPriceText.getText().trim().length() > 0 && openingQtyText.getText().trim().length() > 0 && Double.parseDouble(openingUnitPriceText.getText())>0.0) {
            openingBalText.setText((Double.parseDouble(openingQtyText.getText()) * Double.parseDouble(openingUnitPriceText.getText())) + "");
        }
    }//GEN-LAST:event_openingUnitPriceTextFocusLost

    private void openingBalTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_openingBalTextFocusLost
        if (openingQtyText.getText().trim().length() > 0 && openingBalText.getText().trim().length() > 0 &&Double.parseDouble(openingQtyText.getText())>0.0 ) {
            openingUnitPriceText.setText((Double.parseDouble(openingBalText.getText()) / Double.parseDouble(openingQtyText.getText())) + "");
            
        }
        else
        {
            openingUnitPriceText.setText("0.00");
        }
    }//GEN-LAST:event_openingBalTextFocusLost

    private void saveItemButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveItemButtonActionPerformed
        try {
            UnitDAO unitDAO = new UnitDAO();
            String productId = itemCodeText.getText().trim();
            if(productId.length()>=3){
                if (!itemDAO.isItemCodeExist(productId)) {
                String itemName = descriptionText.getText().trim();
                Unit unit = unitDAO.findByName(unitCombo.getSelectedItem().toString());
                StockGroup stockGroup = stockGroupDAO.findByName(groupCombo.getSelectedItem().toString());
                godownListAll = godownDAO.findAll();
                Object godown = godownComboBox.getSelectedItem();
                Godown godown1 = (Godown) ((ComboKeyValue) godown).getValue();
                if (CommonService.stringValidator(new String[]{productId, itemName})) {
                    if (MsgBox.confirm("Are you sure you want to save?")) {

                        for (Godown godownObj : godownListAll) {
                            Item item = new Item();
                            item.setOpeningQty(0.00);
                            item.setAvailableQty(0.00);
                            item.setOpeningBal(0.00);
                            item.setVisible(false);
                            if (godownObj.getName().trim().equalsIgnoreCase(godown1.getName().trim())) {
                                if (openingQtyText.getText().length() > 0) {
                                    item.setOpeningQty(Double.parseDouble(openingQtyText.getText()));
                                    item.setAvailableQty(Double.parseDouble(openingQtyText.getText()));
                                }
                                if (openingBalText.getText().length() > 0) {
                                    item.setOpeningBal(Double.parseDouble(openingBalText.getText()));
                                }
                                if (openingUnitPriceText.getText().length() > 0) {
                                    item.setOpeningUnitPrice(Double.parseDouble(openingUnitPriceText.getText()));
                                }
                                
                                if (openingQtyText1.getText().length() > 0) {
                                    item.setOpeningQty1(Double.parseDouble(openingQtyText1.getText()));
                                    item.setAvailableQty1(Double.parseDouble(openingQtyText1.getText()));
                                }
                                if (openingBalText1.getText().length() > 0) {
                                    item.setOpeningBal1(Double.parseDouble(openingBalText1.getText()));
                                }
                                if (openingUnitPriceText1.getText().length() > 0) {
                                    item.setOpeningUnitPrice1(Double.parseDouble(openingUnitPriceText1.getText()));
                                }
                                item.setVisible(true);
                            }
                            item.setGodown(godownObj);
                            item.setCompany(GlobalProperty.getCompany());
                            item.setItemCode(productId);
                            item.setDescription(itemName);
                            item.setStockGroup(stockGroup);
                            item.setUnit(unit);
                            item.setWeight(Double.parseDouble(weightText.getText()));
                            if (sellingPriceText.getText().length() > 0) {
                                item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                            }
                            cRUDServices.saveModel(item);
                            barcodeRowText.setText("1");
                            item=null;
                        }
                        MsgBox.success("Successfully Saved " + productId);
                        CommonService.clearTextFields(new JTextField[]{itemCodeText, descriptionText, openingQtyText,openingQtyText1});
                        CommonService.clearCombo(new JComboBox[]{groupCombo, unitCombo});
                        CommonService.clearCurrencyFields(new JTextField[]{weightText, sellingPriceText, openingQtyText,openingQtyText1, openingBalText,openingBalText1, openingUnitPriceText,openingUnitPriceText1});
                        
                        items = itemDAO.findAllByDESC();
                        itemService.fillAddItemTable(itemTable,items,isDuplicateColumnsVisible);
                        
                        itemCodeText.requestFocusInWindow();
                    }
                } else {
                    MsgBox.warning("Product Id and Item Name are mandatory.");
                    itemCodeText.requestFocusInWindow();
                }
            } else {
                MsgBox.abort("Product Id '" + productId + "' is already exists.");
                itemCodeText.requestFocusInWindow();
            }
            }else{
                MsgBox.warning("Item Code must have minimum 3 characters");
                itemCodeText.requestFocusInWindow();
            }
            

        } catch (Exception e) {
            log.error("addItemButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_saveItemButtonActionPerformed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        CommonService.clearTextFields(new JTextField[]{itemCodeText, descriptionText});
        CommonService.clearCombo(new JComboBox[]{groupCombo, unitCombo});
        CommonService.clearCurrencyFields(new JTextField[]{weightText, sellingPriceText, openingQtyText, openingBalText, openingUnitPriceText,openingQtyText1, openingBalText1, openingUnitPriceText1});
        item=null;
        barcodeRowText.setText("1");
    }//GEN-LAST:event_newButtonActionPerformed

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
        try {
            String productId = itemCodeText.getText().trim();
            CommonService commonService = new CommonService();
            //ArrayList selectedRow = commonService.getTableRowData(itemTable);
         //   if (selectedRow != null) {
                //Item item = itemDAO.findItemByItemCode(productId, selectedRow.get(5) + "");
            if(productId.length()>=3){
               if (null != item) {
                    //List<Item> itemList = itemDAO.findAllByItemCode(item.getItemCode());
                    String itemName = descriptionText.getText().trim();
                    Unit unit = unitDAO.findByName(unitCombo.getSelectedItem().toString());
                    StockGroup stockGroup = stockGroupDAO.findByName(groupCombo.getSelectedItem().toString());
                    if (CommonService.stringValidator(new String[]{productId, itemName})) {
                        if (MsgBox.confirm("Are you sure you want to update?")) {
                            item.setItemCode(productId);
                            item.setDescription(itemName);
                            item.setStockGroup(stockGroup);
                            item.setUnit(unit);
                            item.setWeight(Double.parseDouble(weightText.getText()));
                            if (sellingPriceText.getText().length() > 0) {
                                item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                            }
                            //item.setGodown((Godown) godownComboBox.getSelectedItem());
                            Object godown = godownComboBox.getSelectedItem();
                            item.setGodown((Godown) ((ComboKeyValue) godown).getValue());

                            if (openingQtyText.getText().length() > 0) {
                                double oldOpeningQty = item.getOpeningQty();
                                double newOpeningQty = Double.parseDouble(openingQtyText.getText());
                                double availableQty = item.getAvailableQty();
                                item.setOpeningQty(newOpeningQty);
                                item.setAvailableQty((availableQty - oldOpeningQty) + newOpeningQty);
                            }
                            if (openingQtyText1.getText().length() > 0) {
                                double oldOpeningQty1 = item.getOpeningQty1();
                                double newOpeningQty1 = Double.parseDouble(openingQtyText1.getText());
                                double availableQty1 = item.getAvailableQty1();
                                item.setOpeningQty1(newOpeningQty1);
                                item.setAvailableQty1((availableQty1 - oldOpeningQty1) + newOpeningQty1);
                            }

                            if (openingBalText.getText().length() > 0) {
                                item.setOpeningBal(Double.parseDouble(openingBalText.getText()));
                            }
                            if (openingBalText1.getText().length() > 0) {
                                item.setOpeningBal1(Double.parseDouble(openingBalText1.getText()));
                            }
                            if (openingUnitPriceText.getText().length() > 0) {
                                item.setOpeningUnitPrice(Double.parseDouble(openingUnitPriceText.getText()));
                            }
                            if (openingUnitPriceText1.getText().length() > 0) {
                                item.setOpeningUnitPrice1(Double.parseDouble(openingUnitPriceText1.getText()));
                            }
                            cRUDServices.saveOrUpdateModel(item);
                            
//                            for (Item item1 : itemList) {
//                                if (item.getId() != item1.getId()) {
//                                    item1.setItemCode(item.getItemCode());
//                                    item1.setDescription(item.getDescription());
//                                    item1.setStockGroup(item.getStockGroup());
//                                    item1.setUnit(item.getUnit());
//                                    item1.setWeight(item.getWeight());
//                                    item1.setSellingPrice(item.getSellingPrice());
//                                    cRUDServices.saveOrUpdateModel(item1);
//                                }
//                            }
                            MsgBox.success("Successfully Updated " + productId);
                            CommonService.clearCombo(new JComboBox[]{groupCombo, unitCombo});
                            CommonService.clearCurrencyFields(new JTextField[]{weightText, sellingPriceText, openingQtyText, openingBalText, openingUnitPriceText,openingQtyText1, openingBalText1, openingUnitPriceText1});
                            items = itemDAO.findAllByDESC();
                            itemService.fillAddItemTable(itemTable,items,isDuplicateColumnsVisible);
                            itemCodeText.requestFocusInWindow();
                            item=null;
                            barcodeRowText.setText("1");
                        }
                    } else {
                        MsgBox.warning("Product Id and Item Name are mandatory.");
                        itemCodeText.requestFocusInWindow();
                    }
                } else {
                    MsgBox.warning("Product Id '" + productId + "' Not Found.");
                    itemCodeText.requestFocusInWindow();
                } 
            }else{
                MsgBox.warning("Item Code must have minimum 3 characters");
                itemCodeText.requestFocusInWindow();
            }
                
           // }

        } catch (Exception e) {
            log.error("updateButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_updateButtonActionPerformed

    private void hideShowButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hideShowButtonActionPerformed
        try {
          //  String productId = itemCodeText.getText().trim();
           // CommonService commonService = new CommonService();
            //ArrayList selectedRow = commonService.getTableRowData(itemTable);

            //if (selectedRow != null) {
                //Item item = itemDAO.findItemByItemCode(productId, selectedRow.get(5) + "");
                if (null != item) {
                    if (item.isVisible() == true) {
                        item.setVisible(false);
                        hideShowButton.setText("Show Stock");
                    } else {
                        item.setVisible(true);
                        hideShowButton.setText("Hide Stock");
                    }
                    cRUDServices.saveOrUpdateModel(item);
                    //items = itemDAO.findAllByDESC();
                    itemService.fillAddItemTable(itemTable,items,isDuplicateColumnsVisible);
                }
           // }
            /* if(selectedRow!=null){
             Item item = itemDAO.findItemByItemCode(productId, selectedRow.get(5) + "");
             if (null != item) {
             if (item.isVisible() == true) {
             item.setVisible(false);
             hideShowButton.setText("Show Stock");
             } else {
             MsgBox.warning("Product Id '" + productId + "' Not Found.");
             itemCodeText.requestFocusInWindow();
             }
             }

             }*/

        } catch (Exception e) {
            log.error("hideShowButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_hideShowButtonActionPerformed

    private void barcodePrintButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_barcodePrintButtonActionPerformed
        try {
            int numberOfRow=Integer.parseInt(barcodeRowText.getText());
            for(int i=0;i<numberOfRow;i++){
                BarcodeReport barcodeReport = new BarcodeReport();
                barcodeReport.createBarcodeReport(itemCodeText.getText(), descriptionText.getText(),sellingPriceText.getText(),itemCodeText.getText(), descriptionText.getText(),sellingPriceText.getText(),"print");
            }
        } catch (Exception e) {
            log.error("barcodePrintButtonActionPerformed", e);
        }
    }//GEN-LAST:event_barcodePrintButtonActionPerformed

    private void openingQtyText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_openingQtyText1FocusGained
        openingQtyText1.selectAll();
    }//GEN-LAST:event_openingQtyText1FocusGained

    private void openingQtyText1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_openingQtyText1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_openingQtyText1KeyPressed

    private void openingQtyText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_openingQtyText1KeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_openingQtyText1KeyTyped

    private void openingUnitPriceText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_openingUnitPriceText1FocusGained
       openingUnitPriceText1.selectAll();
    }//GEN-LAST:event_openingUnitPriceText1FocusGained

    private void openingUnitPriceText1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_openingUnitPriceText1FocusLost
       if (openingUnitPriceText1.getText().trim().length() > 0 && openingQtyText1.getText().trim().length() > 0) {
            openingBalText1.setText((Double.parseDouble(openingQtyText1.getText()) * Double.parseDouble(openingUnitPriceText1.getText())) + "");
        }
    }//GEN-LAST:event_openingUnitPriceText1FocusLost

    private void openingUnitPriceText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_openingUnitPriceText1KeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_openingUnitPriceText1KeyTyped

    private void openingBalText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_openingBalText1FocusGained
        openingBalText1.selectAll();
    }//GEN-LAST:event_openingBalText1FocusGained

    private void openingBalText1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_openingBalText1FocusLost
        if (openingQtyText1.getText().trim().length() > 0 && openingBalText1.getText().trim().length() > 0) {
            openingUnitPriceText1.setText((Double.parseDouble(openingBalText1.getText()) / Double.parseDouble(openingQtyText1.getText())) + "");
        }
    }//GEN-LAST:event_openingBalText1FocusLost

    private void openingBalText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_openingBalText1KeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_openingBalText1KeyTyped

    private void openingUnitPriceTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openingUnitPriceTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_openingUnitPriceTextActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        Session session = null;
        try {
            String productId = itemCodeText.getText().trim();
            String description = descriptionText.getText().trim();
            CommonService commonService = new CommonService();
            ArrayList selectedRow = commonService.getTableRowData(itemTable);
            if (selectedRow != null) {
                Item item = itemDAO.findItemByItemCode(productId,description, selectedRow.get(5) + "");
                if (null != item) {
                    if (MsgBox.confirm("Are you sure you want to delete?")) {
                        try {
                            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
                            session = sessionFactory.openSession();
                            Transaction transaction = session.beginTransaction();
                            session.delete(item);
                            transaction.commit();

                            MsgBox.success("Successfully deleted " + productId);
                            CommonService.clearCombo(new JComboBox[]{groupCombo, unitCombo});
                            CommonService.clearCurrencyFields(new JTextField[]{weightText, sellingPriceText, openingQtyText, openingBalText, openingUnitPriceText,openingQtyText1, openingBalText1, openingUnitPriceText1});
                            
                            items = itemDAO.findAllByDESC();
                            itemService.fillAddItemTable(itemTable,items,isDuplicateColumnsVisible);
                            itemCodeText.requestFocusInWindow();
                            item=null;
                            barcodeRowText.setText("1");
                        } catch (Exception e) {
                            MsgBox.warning("Item '" + productId + "' is already used for transactions!");
                            itemCodeText.requestFocusInWindow();
                        }
                    }
                } else {
                    MsgBox.warning("Product Id '" + productId + "' Not Found.");
                    itemCodeText.requestFocusInWindow();
                }
            }
        } catch (Exception e) {
            log.error("deleteButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void openingQtyTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_openingQtyTextFocusLost
        if (openingUnitPriceText.getText().trim().length() > 0 && openingQtyText.getText().trim().length() > 0) {
            openingBalText.setText((Double.parseDouble(openingQtyText.getText()) * Double.parseDouble(openingUnitPriceText.getText())) + "");
        }
    }//GEN-LAST:event_openingQtyTextFocusLost

    private void openingQtyText1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_openingQtyText1FocusLost
        if (openingUnitPriceText1.getText().trim().length() > 0 && openingQtyText1.getText().trim().length() > 0) {
            openingBalText1.setText((Double.parseDouble(openingQtyText1.getText()) * Double.parseDouble(openingUnitPriceText1.getText())) + "");
        }
    }//GEN-LAST:event_openingQtyText1FocusLost

    private void barcodeRowTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_barcodeRowTextFocusGained
        barcodeRowText.selectAll();
    }//GEN-LAST:event_barcodeRowTextFocusGained

    private void barcodeRowTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_barcodeRowTextKeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_barcodeRowTextKeyTyped

    private void itemCodeTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemCodeTextKeyPressed
         try {
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                String productId = itemCodeText.getText().trim();
                items=itemDAO.findAllStartsWithItemCode(productId, true);
                itemService.fillAddItemTable(itemTable,items,isDuplicateColumnsVisible);
            }
        } catch (Exception e) {
            log.error("itemCodeTextKeyPressed:", e);
        }
    }//GEN-LAST:event_itemCodeTextKeyPressed

    private void descriptionTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_descriptionTextKeyPressed
         try {
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                String description = descriptionText.getText().trim();
                items=itemDAO.findAllStartsWithDescription(description, true);
                itemService.fillAddItemTable(itemTable,items,isDuplicateColumnsVisible);
            }
        } catch (Exception e) {
            log.error("descriptionTextKeyPressed:", e);
        }
    }//GEN-LAST:event_descriptionTextKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel barcodePanel;
    private javax.swing.JButton barcodePrintButton;
    private javax.swing.JFormattedTextField barcodeRowText;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton deleteButton;
    private javax.swing.JTextField descriptionText;
    private javax.swing.JComboBox godownComboBox;
    private javax.swing.JLabel godownLabel;
    private javax.swing.JComboBox groupCombo;
    private javax.swing.JButton hideShowButton;
    private javax.swing.JTextField itemCodeText;
    private javax.swing.JTable itemTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton newButton;
    private javax.swing.JFormattedTextField openingBalText;
    private javax.swing.JFormattedTextField openingBalText1;
    private javax.swing.JPanel openingBalancePanel;
    private javax.swing.JPanel openingPanel;
    private javax.swing.JPanel openingQtyPanel;
    private javax.swing.JTextField openingQtyText;
    private javax.swing.JTextField openingQtyText1;
    private javax.swing.JPanel openingUnitPricePanel;
    private javax.swing.JFormattedTextField openingUnitPriceText;
    private javax.swing.JFormattedTextField openingUnitPriceText1;
    private javax.swing.JButton saveItemButton;
    private javax.swing.JFormattedTextField sellingPriceText;
    private javax.swing.JPanel tablePanel;
    private javax.swing.JComboBox unitCombo;
    private javax.swing.JButton updateButton;
    private javax.swing.JFormattedTextField weightText;
    // End of variables declaration//GEN-END:variables

    private void If(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
