package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.LedgerGroupDAO;
import com.cloudsendsoft.inventory.dao.UnitDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.ItemService;
import com.cloudsendsoft.inventory.services.LedgerService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.model.Unit;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import static com.cloudsendsoft.inventory.view.ChargesView.log;
import java.awt.TextField;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class LedgerView extends javax.swing.JInternalFrame {

    /**
     * Creates new form AddItem
     */
    static final Logger log = Logger.getLogger(LedgerView.class.getName());
    CRUDServices cRUDServices = null;
    LedgerDAO ledgerDAO = new LedgerDAO();
    LedgerService ledgerService = new LedgerService();
    LedgerGroupDAO ledgerGroupDAO = new LedgerGroupDAO();
    Ledger ledger = null;
    

    public LedgerView() {
        initComponents();

        if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
            addButton.setEnabled(false);
            newButton.setEnabled(false);
        }

        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);

        this.getRootPane().setDefaultButton(addButton);
        cRUDServices = new CRUDServices();
        for (LedgerGroup group : LedgerGroupDAO.findAll()) {
            underCombo.addItem(group.getGroupName());
        }

        ledgerListTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{
                    "Ledger Name", "Narration", "Ledger Type", "Available Balance"
                }
        ));
        ledgerListTable.setRowHeight(25);

        ledgerService.fillLedgerListTable(ledgerListTable);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ledgerMainPanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        ledgerListTable = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        nameText = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        telephoneText = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        emailText = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        addressTextPane = new javax.swing.JTextPane();
        jLabel4 = new javax.swing.JLabel();
        incomeTaxText = new javax.swing.JTextField();
        salesTaxText = new javax.swing.JTextField();
        faxText = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        underCombo = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        openingBalText = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        ledgerNameText = new javax.swing.JTextField();
        narrationText = new javax.swing.JTextField();

        setClosable(true);
        setTitle("Ledger Info");
        getContentPane().setLayout(new java.awt.CardLayout());

        ledgerMainPanel.setBackground(new java.awt.Color(255, 255, 255));
        ledgerMainPanel.setLayout(new java.awt.BorderLayout());

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Ledger List"));
        jPanel2.setOpaque(false);

        ledgerListTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        ledgerListTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ledgerListTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(ledgerListTable);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 959, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 345, Short.MAX_VALUE)
        );

        ledgerMainPanel.add(jPanel2, java.awt.BorderLayout.CENTER);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel4.setOpaque(false);

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel6.setOpaque(false);

        jLabel6.setText("Address");

        jLabel7.setText("Name");

        jLabel8.setText("Telephone");

        jLabel9.setText("E-mail");

        jLabel10.setText("Income Tax No");

        jLabel11.setText("Sales Tax No");

        jScrollPane3.setViewportView(addressTextPane);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel4.setText("Additional Details");

        incomeTaxText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                incomeTaxTextActionPerformed(evt);
            }
        });

        jLabel12.setText("Fax");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(salesTaxText, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(incomeTaxText, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(faxText, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(emailText)
                    .addComponent(telephoneText)
                    .addComponent(jScrollPane3)
                    .addComponent(nameText))
                .addContainerGap(24, Short.MAX_VALUE))
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(nameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(telephoneText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(emailText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(faxText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(incomeTaxText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(salesTaxText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel5.setOpaque(false);
        jPanel5.setLayout(new java.awt.GridLayout(6, 1, 0, 8));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("New");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        jPanel5.add(newButton);

        addButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        addButton.setText("Save/Update");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });
        jPanel5.add(addButton);

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/delete-icon.png"))); // NOI18N
        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        jPanel5.add(deleteButton);

        jPanel1.setOpaque(false);

        jLabel2.setText("Narration");

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder("Opening Bal [Optional]"));
        jPanel9.setOpaque(false);

        jLabel15.setText("Balance");

        openingBalText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        openingBalText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        openingBalText.setText("0.00");
        openingBalText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                openingBalTextFocusGained(evt);
            }
        });
        openingBalText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                openingBalTextKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel15)
                .addGap(28, 28, 28)
                .addComponent(openingBalText, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(openingBalText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addContainerGap(27, Short.MAX_VALUE))
        );

        jLabel3.setText("Under");

        jLabel1.setText("Ledger Name");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addComponent(jLabel3))
                            .addGap(36, 36, 36)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(narrationText, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(underCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addGap(18, 18, 18)
                            .addComponent(ledgerNameText, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ledgerNameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(narrationText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(underCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        ledgerMainPanel.add(jPanel4, java.awt.BorderLayout.PAGE_START);

        getContentPane().add(ledgerMainPanel, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        try {
            String ledgerName = ledgerNameText.getText().trim();
            boolean isEditable = true;
            if (ledger != null) {
                if (!ledger.isEditable()) {
                    isEditable = false;
                }
            }
            if (isEditable) {
                if (!ledgerDAO.isLedgerNameExist(ledgerName)) {
                    LedgerGroup ledgerGroup = ledgerGroupDAO.findByGroupName(underCombo.getSelectedItem().toString());
                    double balanceAmount = 0.00;
                    if (openingBalText.getText().length() > 0) {
                        balanceAmount = Double.parseDouble(openingBalText.getText());
                    }

                    if (CommonService.stringValidator(new String[]{ledgerName})) {
                        if (MsgBox.confirm("Are you sure you want to save or update?")) {
                            if (ledger == null) {
                                ledger = new Ledger();
                            }
                            ledger.setCompany(GlobalProperty.getCompany());
                            ledger.setLedgerName(ledgerName);
                            if (narrationText.getText().trim().length() > 0) {
                                ledger.setNarration(narrationText.getText().trim());
                            }
                            ledger.setLedgerGroup(ledgerGroup);
                            if (nameText.getText().trim().length() > 0) {
                                ledger.setName(nameText.getText().trim());
                            }
                            if (addressTextPane.getText().trim().length() > 0) {
                                ledger.setAddress(addressTextPane.getText());
                            }
                            if (telephoneText.getText().trim().length() > 0) {
                                ledger.setTelephone(telephoneText.getText().trim());
                            }
                            if (emailText.getText().trim().length() > 0) {
                                ledger.setEmail(emailText.getText().trim());
                            }
                            if (faxText.getText().trim().length() > 0) {
                                ledger.setFax(faxText.getText().trim());
                            }
                            if (salesTaxText.getText().trim().length() > 0) {
                                ledger.setSalesTaxNo(salesTaxText.getText().trim());
                            }
                            if (incomeTaxText.getText().trim().length() > 0) {
                                ledger.setIncomeTaxNo(incomeTaxText.getText().trim());
                            }
                            ledger.setAvailableBalance(balanceAmount);
                            ledger.setOpeningBalance(balanceAmount);
                            ledger.setEditable(true);
                            cRUDServices.saveOrUpdateModel(ledger);
                            MsgBox.success("Successfully Added " + ledgerName);
                            CommonService.clearTextFields(new JTextField[]{ledgerNameText, narrationText, nameText, telephoneText, emailText, faxText, salesTaxText, incomeTaxText});
                            CommonService.clearTextPane(new JTextPane[]{addressTextPane});
                            underCombo.setSelectedIndex(0);
                            openingBalText.setText("0.00");
                            ledgerService.fillLedgerListTable(ledgerListTable);
                            ledgerNameText.requestFocusInWindow();
                        }
                    } else {
                        MsgBox.warning("Ledger Name is mandatory");
                        ledgerNameText.requestFocusInWindow();
                        ledger=null;
                    }
                } else {
                    MsgBox.abort("Ledger Name '" + ledgerName + "' is already exists.");
                    ledgerNameText.requestFocusInWindow();
                }
            } else {
                MsgBox.warning("You can't edit auto generated ledger");
            }
        } catch (Exception e) {
            log.error("addButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_addButtonActionPerformed

    private void ledgerListTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ledgerListTableMouseClicked
        try {
            CommonService commonService = new CommonService();
            ArrayList selectedRow = commonService.getTableRowData(ledgerListTable);
            if (selectedRow != null) {
                ledger = ledgerDAO.findByLedgerName(selectedRow.get(0) + "");
                ledgerNameText.setText(ledger.getLedgerName());
                narrationText.setText(ledger.getNarration());
                underCombo.setSelectedItem(ledger.getLedgerGroup().getGroupName() + "");
                nameText.setText(ledger.getName());
                addressTextPane.setText(ledger.getAddress());
                telephoneText.setText(ledger.getTelephone());
                emailText.setText(ledger.getEmail());
                faxText.setText(ledger.getFax());
                incomeTaxText.setText(ledger.getIncomeTaxNo());
                salesTaxText.setText(ledger.getSalesTaxNo());
                openingBalText.setText(ledger.getOpeningBalance() + "");
            }
        } catch (Exception e) {
            log.error("ledgerListTableMouseClicked", e);
        }
    }//GEN-LAST:event_ledgerListTableMouseClicked


    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
        
    }//GEN-LAST:event_updateButtonActionPerformed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        ledger=null;
        CommonService.clearTextFields(new JTextField[]{ledgerNameText, narrationText, nameText, telephoneText, emailText, faxText, salesTaxText, incomeTaxText, openingBalText});
        CommonService.clearTextPane(new JTextPane[]{addressTextPane});
        ledger=null;
    }//GEN-LAST:event_newButtonActionPerformed

    private void incomeTaxTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_incomeTaxTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_incomeTaxTextActionPerformed

    private void openingBalTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_openingBalTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_openingBalTextKeyTyped

    private void openingBalTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_openingBalTextFocusGained
        openingBalText.selectAll();
    }//GEN-LAST:event_openingBalTextFocusGained

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        try {
            Session session = null;
            if (null != ledger) {
                String ledgerName = ledger.getLedgerName();
                if (ledger.isEditable()) {
                    if (MsgBox.confirm("Are you sure you want to delete?")) {
                        try {
                            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
                            session = sessionFactory.openSession();
                            Transaction transaction = session.beginTransaction();
                            session.delete(ledger);
                            transaction.commit();

                            MsgBox.success("Successfully Deleted " + ledgerName);
                            CommonService.clearTextFields(new JTextField[]{ledgerNameText, narrationText, nameText, telephoneText, emailText, faxText, salesTaxText, incomeTaxText});
                            CommonService.clearTextPane(new JTextPane[]{addressTextPane});
                            underCombo.setSelectedIndex(0);
                            openingBalText.setText("0.00");
                            ledgerService.fillLedgerListTable(ledgerListTable);
                            ledgerNameText.requestFocusInWindow();

                        } catch (Exception e) {
                            MsgBox.warning("Ledger '" + ledgerName + "' is already used for transactions");
                            ledgerNameText.requestFocusInWindow();
                        } finally {
                            if (session != null) {
                                session.clear();
                                session.close();
                            }
                        }
                    }
                } else {
                    MsgBox.warning("You can't delete auto generated ledger");
                }
            }
        } catch (Exception e) {
            log.error("deleteButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_deleteButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JTextPane addressTextPane;
    private javax.swing.JButton deleteButton;
    private javax.swing.JTextField emailText;
    private javax.swing.JTextField faxText;
    private javax.swing.JTextField incomeTaxText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable ledgerListTable;
    private javax.swing.JPanel ledgerMainPanel;
    private javax.swing.JTextField ledgerNameText;
    private javax.swing.JTextField nameText;
    private javax.swing.JTextField narrationText;
    private javax.swing.JButton newButton;
    private javax.swing.JFormattedTextField openingBalText;
    private javax.swing.JTextField salesTaxText;
    private javax.swing.JTextField telephoneText;
    private javax.swing.JComboBox underCombo;
    // End of variables declaration//GEN-END:variables
}
