/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.CTableModel;
import com.cloudsendsoft.inventory.dao.CompanyDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.LedgerGroupDAO;
import com.cloudsendsoft.inventory.dao.PurchaseDAO;
import com.cloudsendsoft.inventory.dao.PurchaseReturnDAO;
import com.cloudsendsoft.inventory.dao.PurchaseReturnItemDAO;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.dao.SalesReturnDAO;
import com.cloudsendsoft.inventory.dao.SalesReturnItemDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseReturn;
import com.cloudsendsoft.inventory.model.PurchaseReturnItem;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesReturn;
import com.cloudsendsoft.inventory.model.SalesReturnItem;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.report.BalanceSheetReport;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.services.PLAService;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.awt.Color;
import java.awt.Component;
import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class PLAccountView extends javax.swing.JPanel {

    static final Logger log = Logger.getLogger(PLAccountView.class.getName());

    CommonService commonService = new CommonService();

    CTableModel debitSideTableModel = null;
    CTableModel creditSideTableModel = null;

    int screen = 1;
    int index = 0;

    LedgerGroupDAO ledgerGroupDAO = new LedgerGroupDAO();
    List<LedgerGroup> listOfLedgerGroup = null;

    LedgerDAO ledgerDAO = new LedgerDAO();
    List<Ledger> listOfLedger = null;

    ItemDAO itemDAO = new ItemDAO();
    List<Item> listOfItem = null;
    StockGroup stockGroup = null;
    List<StockGroup> listOfStockGroup = null;

    PurchaseDAO purchaseDAO = new PurchaseDAO();
    PurchaseReturnDAO purchaseReturnDAO = new PurchaseReturnDAO();
    List<PurchaseItem> listofPurchaseItem = new ArrayList<>(0);

    SalesDAO salesDAO = new SalesDAO();
    SalesReturnDAO salesReturnDAO = new SalesReturnDAO();

    //using for Commercial Invoice
    SalesPreformaDAO salesPreformaDAO = new SalesPreformaDAO();

    LedgerGroup ledgerGroup = null;

    double tradingDebitAmt = 0.00;
    double tradingCreditAmt = 0.00;

    double profitLosssDebitAmt = 0.00;
    double profitLosssCreditAmt = 0.00;

    boolean isBalancesheet = false;

    double diffAmountPLandTrading = 0.00;

    boolean PL_DebitBalance = true;

    List<Object[]> closingStockList = new ArrayList<>(0);
    List<Object[]> closingStockListDetailed = new ArrayList<>(0);
    List<Object[]> closingStockListBuffer = new ArrayList<>(0);
    double closingStock = 0.00;
    Date fromDate = GlobalProperty.getCompany().getStartDate();
    Date toDate = GlobalProperty.getCompany().getEndDate();
    PLAService plaService = new PLAService();
    CharSequence income="Income";
    CharSequence expense="Expense";
    public PLAccountView(boolean isBalancesheet) {

        this.isBalancesheet = isBalancesheet;
        log.info("init started at: " + Calendar.getInstance().getTime());
        initComponents();
        log.info("init completed at: " + Calendar.getInstance().getTime());
        //centerHeadingLabel.setText("Trial Balance as at "+commonService.sqlDateToString(GlobalProperty.company.getEndDate()));
        //table background color settings
        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);
        jScrollPane2.setOpaque(false);
        jScrollPane2.getViewport().setOpaque(false);
        /*       stockSummaryTable.setBackground(this.getBackground());
         stockSummaryTable.getTableHeader().setBackground(this.getBackground());
         stockSummaryTable.getTableHeader().setForeground(this.getBackground());
         */
        //*******************set debit side table design*****************
        debitSideTable.setRowHeight(GlobalProperty.tableRowHieght);

        debitSideTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {

            //alternate row color jtable
            @Override
            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int column) {
                Component c = super.getTableCellRendererComponent(table,
                        value, isSelected, hasFocus, row, column);
                c.setBackground(row % 2 == 0 ? Color.GRAY : Color.LIGHT_GRAY);
                return c;
            }
        ;
        });

        debitSideTable.setShowGrid(false);
        debitSideTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{"Particulars", "Amount"}
        ));
        debitSideTableModel = (CTableModel) debitSideTable.getModel();
        CommonService.setWidthAsPercentages(debitSideTable, .80, .20);

        debitSideTableModel.setRowCount(0);

        //*******************set credit side table design**************************************
        creditSideTable.setRowHeight(GlobalProperty.tableRowHieght);

        creditSideTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {

            //alternate row color jtable
            @Override
            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int column) {
                Component c = super.getTableCellRendererComponent(table,
                        value, isSelected, hasFocus, row, column);
                c.setBackground(row % 2 == 0 ? Color.GRAY : Color.LIGHT_GRAY);
                return c;
            }
        ;
        });

        creditSideTable.setShowGrid(false);
        creditSideTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{"Particulars", "Amount"}
        ));
        creditSideTableModel = (CTableModel) creditSideTable.getModel();
        CommonService.setWidthAsPercentages(creditSideTable, .80, .20);

        creditSideTableModel.setRowCount(0);

        //fetch all ledger groups
        listOfLedgerGroup = ledgerDAO.findDistinctLedgerGroupFromLedger();
        commonService.setCurrentPeriodOnCalendar(fromDateChooser);
        commonService.setCurrentPeriodOnCalendar(toDateChooser);
        fromDateChooser.setDate(GlobalProperty.getCompany().getStartDate());
        toDateChooser.setDate(GlobalProperty.getCompany().getEndDate());
        
        fillPLAccountTableNew();

    }

    void fillBalanceSheetNew() {
        try {
            //BS table style
            
            centerHeadingLabel.setText("Balance Sheet");
            debitSideTableModel.setRowCount(0);
            creditSideTableModel.setRowCount(0);
            debitSideTable.getColumnModel().getColumn(0).setHeaderValue("Liabilities");
            creditSideTable.getColumnModel().getColumn(0).setHeaderValue("Assets");

            double liabilitySideAmt = 0.00;
            double assetSideAmt = 0.00;

            double ledgerTotal = 0.00;
            double purchaseTotal = 0.00;
            double salesTotal = 0.00;
            double subTotal=0.00;
            HashMap<Ledger, String> taxDetailHashMap = new HashMap<>();
            HashMap<String, Double> ledgerBalanceHashMap = new HashMap<>();
            List<Object[]> capitalListLiabilitySide = new ArrayList<>(0);
            List<Object[]> capitalListAssetSide = new ArrayList<>(0);

            List<Object[]> liabilityListLiabilitySide = new ArrayList<>(0);
            List<Object[]> liabilityListAssetSide = new ArrayList<>(0);

            List<Object[]> assetListLiabilitySide = new ArrayList<>(0);
            List<Object[]> assetListAssetSide = new ArrayList<>(0);

            /* List<Object[]> closingStockList = new ArrayList<>(0);
             List<Object[]> closingStockListDetailed = new ArrayList<>(0);*/
            //all records from ledger table
            for (LedgerGroup ledgerGroup : listOfLedgerGroup) {
                if(!ledgerGroup.getAccountType().contains(income)&&!ledgerGroup.getAccountType().contains(expense))
                {
                listOfLedger = ledgerDAO.findAllByLedgerGroup(ledgerGroup);
                double ledgerGroupTotal = 0.00;
                    for (Ledger ledger : listOfLedger) {
                    if (ledgerGroup.getGroupName().equalsIgnoreCase("Duties & Taxes")) {
                        salesTotal = plaService.findSalesTax(ledger, fromDate, toDate);
                        purchaseTotal = plaService.findPurchaseTax(ledger, fromDate, toDate);
                        taxDetailHashMap.put(ledger, "<html><i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "Input " + ledger.getLedgerName()
                                + "&nbsp;&nbsp;&nbsp;" + purchaseTotal + "\n" + "<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "Output " + ledger.getLedgerName()
                                + "&nbsp;&nbsp;&nbsp;" + salesTotal);
                        ledgerGroupTotal += purchaseTotal - salesTotal;
                        if(ledgerBalanceHashMap.get(ledger.getLedgerName())==null)
                        {
                            ledgerBalanceHashMap.put(ledger.getLedgerName(), purchaseTotal-salesTotal);
                        }
                        else
                        {
                            ledgerBalanceHashMap.put(ledger.getLedgerName(),ledgerBalanceHashMap.get(ledger.getLedgerName())+(purchaseTotal-salesTotal));
                        }
                    } else {
                        subTotal=plaService.findAvailableBalanceForBalanceSheet(ledger, fromDate, toDate);
                        ledgerGroupTotal += subTotal;
                        if(ledgerBalanceHashMap.get(ledger.getLedgerName())==null)
                        {
                            ledgerBalanceHashMap.put(ledger.getLedgerName(), subTotal);
                        }
                        else
                        {
                            ledgerBalanceHashMap.put(ledger.getLedgerName(),ledgerBalanceHashMap.get(ledger.getLedgerName())+subTotal);
                        }
                    }
                }

                if (ledgerGroupTotal > 0) {
                    if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Capital")) {

                        capitalListLiabilitySide.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                            + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                        liabilitySideAmt += ledgerGroupTotal;
                    } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Liability")) {
                        liabilityListLiabilitySide.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                            + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                        liabilitySideAmt += ledgerGroupTotal;
                    } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Asset")) {
                        assetListAssetSide.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                            + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                        assetSideAmt += ledgerGroupTotal;
                    }
                } else if (ledgerGroupTotal < 0) {
                    //ledgerGroupTotal=Math.abs(ledgerGroupTotal);
                    if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Capital")) {
                        capitalListAssetSide.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                            + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerGroupTotal)) + "</b></html>"});
                        assetSideAmt += Math.abs(ledgerGroupTotal);
                    } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Liability")) {
                        liabilityListAssetSide.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                            + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerGroupTotal)) + "</b></html>"});
                        assetSideAmt += Math.abs(ledgerGroupTotal);
                    } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Asset")) {
                        assetListLiabilitySide.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                            + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerGroupTotal)) + "</b></html>"});
                        liabilitySideAmt += Math.abs(ledgerGroupTotal);
                    }
                }
                //Debit Side - Detailed View of Ledger
                if (detailViewCheckBox.isSelected()) {
                    for (Ledger ledger : listOfLedger) {
                        ledgerTotal = ledgerBalanceHashMap.get(ledger.getLedgerName());
                        if (ledgerGroupTotal > 0 && ledgerTotal != 0) {
                            if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Capital")) {
                                capitalListLiabilitySide.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                    + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledgerTotal) + "</i></html>"});
                            } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Liability")) {
                                if (ledgerGroup.getGroupName().equalsIgnoreCase("Duties & Taxes")) {
                                    liabilityListLiabilitySide.add(new Object[]{"<html>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>" + ledger.getLedgerName()
                                        + "&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledgerTotal) + "</u></b></html>"});

                                    liabilityListLiabilitySide.add(new Object[]{taxDetailHashMap.get(ledger)});

                                } else {
                                    liabilityListLiabilitySide.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                        + "&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledgerTotal) + "</i></html>"});
                                }
                            } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Asset")) {
                                assetListAssetSide.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                    + "&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledgerTotal) + "</i></html>"});
                            }
                        } else if (ledgerGroupTotal < 0 && ledgerTotal != 0) {
                            //ledger.setAvailableBalance(ledger.getAvailableBalance());
                            if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Capital")) {
                                capitalListAssetSide.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                    + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(Math.abs(plaService.findAvailableBalanceForBalanceSheet(ledger, fromDate, toDate))) + "</i></html>"});
                            } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Liability")) {
                                if (ledgerGroup.getGroupName().equalsIgnoreCase("Duties & Taxes")) {
                                    liabilityListAssetSide.add(new Object[]{"<html>&nbsp;&nbsp;&nbsp;&nbsp;<b><u>" + ledger.getLedgerName()
                                        + "&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerTotal)) + "</u></b></html>"});
                                    liabilityListAssetSide.add(new Object[]{taxDetailHashMap.get(ledger)});
                                } else {
                                    liabilityListAssetSide.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                        + "&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(Math.abs(ledgerTotal)) + "</i></html>"});
                                }
                            } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Asset")) {
                                assetListLiabilitySide.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                    + "&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(Math.abs(plaService.findAvailableBalanceForBalanceSheet(ledger, fromDate, toDate))) + "</i></html>"});
                            }
                        }
                    }
                }
            }
            }

            //Liability - capital list
            for (Object[] objects : capitalListLiabilitySide) {
                debitSideTableModel.addRow(objects);
            }

            //Profit & Loss Balance
            if (diffAmountPLandTrading > 0 && PL_DebitBalance) {
                liabilitySideAmt -= diffAmountPLandTrading;
            } else {
                liabilitySideAmt += diffAmountPLandTrading;
            }
            debitSideTableModel.addRow(new Object[]{"<html><b>" + "Profit & Loss A/C"
                + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmountPLandTrading) + "</b></html>"});

            //Liability - liability list
            for (Object[] objects : liabilityListLiabilitySide) {
                debitSideTableModel.addRow(objects);
            }
            //Minus(-) - asset list
            for (Object[] objects : assetListLiabilitySide) {
                debitSideTableModel.addRow(objects);
            }

            //Asset - asset list
            for (Object[] objects : assetListAssetSide) {
                creditSideTableModel.addRow(objects);
            }
            //Minus(-) - liability list
            for (Object[] objects : liabilityListAssetSide) {
                creditSideTableModel.addRow(objects);
            }
            //Minus(-) - capital list
            for (Object[] objects : capitalListAssetSide) {
                creditSideTableModel.addRow(objects);
            }

            //closing stock
            for (Object[] objects : closingStockList) {
                creditSideTableModel.addRow(objects);
            }
            assetSideAmt += closingStock;

            //closing stock detailed
            if (detailViewCheckBox.isSelected()) {
                creditSideTableModel.addRow(new Object[]{"<html>&nbsp&nbsp&nbsp&nbsp <i>Please view Stock Summary for further details [Ctrl+Shift+U]</i></html>"});
            }
            //total Trading A/c amount calculation
            double totalAmount = 0.00;
            double balSheetDiffAmount = 0.00;
            boolean debitBalance = true;

            if (liabilitySideAmt > assetSideAmt) {
                balSheetDiffAmount = liabilitySideAmt - assetSideAmt;
                creditSideTableModel.addRow(new Object[]{null});
                creditSideTableModel.addRow(new Object[]{"<html><b>" + "Diff. in opening balance" + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(balSheetDiffAmount) + "</b></html>"});
                creditSideTableModel.addRow(new Object[]{null});
                totalAmount = liabilitySideAmt;
            } else {
                balSheetDiffAmount = assetSideAmt - liabilitySideAmt;
                debitSideTableModel.addRow(new Object[]{null});
                debitSideTableModel.addRow(new Object[]{"<html><b>" + "Diff. in opening balance" + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(balSheetDiffAmount) + "</b></html>"});
                debitSideTableModel.addRow(new Object[]{null});
                totalAmount = assetSideAmt;
                debitBalance = false;
            }

            //equalize rows in both debit & credit table
            int row = debitSideTable.getRowCount();
            if (creditSideTable.getRowCount() > row) {
                row = creditSideTable.getRowCount();
            }
            for (int i = debitSideTable.getRowCount(); i < row; i++) {
                debitSideTableModel.addRow(new Object[]{null});
            }
            for (int i = creditSideTable.getRowCount(); i < row; i++) {
                creditSideTableModel.addRow(new Object[]{null});
            }

            debitSideTableModel.addRow(new Object[]{"<html><b><u Style=”border-bottom:1px solid black;”>" + " Total" + "</u></b></html>", "<html><b><u Style=”border-bottom:1px solid black;”>" + commonService.formatIntoCurrencyAsString(totalAmount) + "</u></b></html>"});
            creditSideTableModel.addRow(new Object[]{"<html><b><u Style=”border-bottom:1px solid black;”>" + " Total" + "</u></b></html>", "<html><b><u Style=”border-bottom:1px solid black;”>" + commonService.formatIntoCurrencyAsString(totalAmount) + "</u></b></html>"});

        } catch (Exception e) {
            log.error("fillBalanceSheetNew:", e);
        }
    }

    void fillPLAccountTableNew() {
        try {
            double subResult = 0.00;
            double salesTotal = 0.00;
            double purchaseTotal = 0.00;
            double salesReturnTotal = 0.00;
            double purchaseReturnTotal = 0.00;

            HashMap<String, Double> ledgerBalanceHashMap = new HashMap<>();
            debitSideTableModel.setRowCount(0);
            creditSideTableModel.setRowCount(0);
            listofPurchaseItem = new ArrayList<>(0);
            detailViewCheckBox.setVisible(true);
            tradingDebitAmt = 0.00;
            profitLosssDebitAmt = 0.00;
            profitLosssCreditAmt = 0.00;
            tradingCreditAmt = 0.00;

            List<Object[]> openingStockList = new ArrayList<>(0);
            closingStockList = new ArrayList<>(0);
            closingStockListDetailed = new ArrayList<>(0);

            List<Object[]> directExpenseList = new ArrayList<>(0);
            List<Object[]> indirectExpenseList = new ArrayList<>(0);
            List<Object[]> directIncomeList = new ArrayList<>(0);
            List<Object[]> indirectIncomeList = new ArrayList<>(0);

            //Opening Stock
            //double openingBalanceTot = itemDAO.findOpeningBalanceTotal();
//            Calendar cal = Calendar.getInstance();
//            cal.setTime(fromDate);
//            cal.add(Calendar.DATE, -1);
//            java.util.Date prevDate = cal.getTime();
            java.util.Date prevDate = null;
            prevDate=commonService.addDaysToDate(fromDate, -1);
            log.info("____________opening: " + Calendar.getInstance().getTime());
            double openingBalanceTot = plaService.findClosingStockByDateRange(GlobalProperty.getCompany().getStartDate(), commonService.utilDateToSqlDate(prevDate));
            log.info("____________opening Ends: " + Calendar.getInstance().getTime());
            tradingDebitAmt += openingBalanceTot;

            if (openingBalanceTot != 0) {
                openingStockList.add(new Object[]{"<html><b>Opening Stock</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(openingBalanceTot) + "</b></html>"});
            }
            //all records from ledger table
            for (LedgerGroup ledgerGroup : listOfLedgerGroup) {
                log.info("ledger group loop started at: " + Calendar.getInstance().getTime());
                if(ledgerGroup.getAccountType().contains(expense)||ledgerGroup.getAccountType().contains(income))
                {
                listOfLedger = ledgerDAO.findAllByLedgerGroup(ledgerGroup);
                log.info("ledger dao complted  at: " + Calendar.getInstance().getTime());
                double ledgerGroupTotal = 0.00;
                for (Ledger ledger : listOfLedger) {
                    if (ledger.getLedgerGroup().getGroupName().equalsIgnoreCase("Purchase Accounts")) {
                        if (ledger.getLedgerName().equalsIgnoreCase("Purchase Account")) {
                            subResult = purchaseDAO.findTotalPurchase(fromDate, toDate);
                            ledgerGroupTotal += subResult;
                        } else if (ledger.getLedgerName().equalsIgnoreCase("Purchase Returns")) {
                            subResult = purchaseReturnDAO.findTotalPurchaseReturn(fromDate, toDate);
                            purchaseReturnTotal = subResult;
                        }
                    }
                    else if (ledger.getLedgerGroup().getGroupName().equalsIgnoreCase("Sales Accounts")) {
                        if (ledger.getLedgerName().equalsIgnoreCase("Sales Account")) {
                            subResult = salesDAO.findTotalSales(fromDate, toDate);
                            ledgerGroupTotal += subResult;
                        } else if (ledger.getLedgerName().equalsIgnoreCase("Sales Returns")) {
                            subResult = salesReturnDAO.findTotalSalesReturn(fromDate, toDate);
                            salesReturnTotal = subResult;
                        }
                    } 
                    else 
                    {
                        subResult = plaService.findAvailableBalanceForBalanceSheet(ledger, fromDate, toDate);
                        ledgerGroupTotal += subResult;
                    }
                    if (ledgerBalanceHashMap.get(ledger.getLedgerName()) == null) {
                        ledgerBalanceHashMap.put(ledger.getLedgerName(), subResult);
                    } else {
                        ledgerBalanceHashMap.put(ledger.getLedgerName(), ledgerBalanceHashMap.get(ledger.getLedgerName()) + subResult);
                    }
                    subResult=0;
                    
                }

                if (ledgerGroupTotal > 0 || ledgerGroupTotal < 0) {//|| ledgerGroupTotal == 0) {
                    //Debit Balance - LedgerGroup
                    if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Expense")
                            || ledgerGroup.getAccountType().trim().equalsIgnoreCase("Indirect Expense")) {
                        if (ledgerGroupTotal >= 0) {
                            if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Expense")) {
                                if (ledgerGroup.getGroupName().trim().equalsIgnoreCase("Purchase Accounts")) {
                                    ledgerGroupTotal -= purchaseReturnTotal;
                                }
                                if (ledgerGroupTotal != 0) {
                                    directExpenseList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName() + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                }
                                tradingDebitAmt += ledgerGroupTotal;
                            } else if (ledgerGroupTotal != 0) {
                                indirectExpenseList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName()
                                    + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                profitLosssDebitAmt += ledgerGroupTotal;
                            }

                        } else if (ledgerGroupTotal <= 0) {
                            if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Expense")) {
                                if (ledgerGroup.getGroupName().trim().equalsIgnoreCase("Purchase Accounts")) {
                                    ledgerGroupTotal += purchaseReturnTotal;
                                }
                                if (ledgerGroupTotal != 0) {
                                    directIncomeList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName() + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                }
                                tradingCreditAmt += Math.abs(ledgerGroupTotal);
                            } else if (ledgerGroupTotal != 0) {
                                indirectIncomeList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName() + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                profitLosssCreditAmt += Math.abs(ledgerGroupTotal);

                            }

                        }
                        //Debit Side - Detailed View of Ledger
                        if (detailViewCheckBox.isSelected()) {
                            for (Ledger ledger : listOfLedger) {
                              if (ledgerBalanceHashMap.get(ledger.getLedgerName())!=null){
                                if (ledgerBalanceHashMap.get(ledger.getLedgerName()) > 0) {
                                    if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Expense")) {
                                        directExpenseList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledgerBalanceHashMap.get(ledger.getLedgerName())) + "</i></html>"});
                                    } else {
                                        indirectExpenseList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledgerBalanceHashMap.get(ledger.getLedgerName())) + "</i></html>"});
                                    }
                                } else if(ledgerBalanceHashMap.get(ledger.getLedgerName()) < 0) {
                                    if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Expense")) {
                                        directIncomeList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledgerBalanceHashMap.get(ledger.getLedgerName())) + "</i></html>"});
                                    } else {
                                        indirectIncomeList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledgerBalanceHashMap.get(ledger.getLedgerName())) + "</i></html>"});
                                    }
                                }
                            }
                            }
                        }

                    } else if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Income")
                            || ledgerGroup.getAccountType().trim().equalsIgnoreCase("Indirect Income")) {
                        //Credit Side Balance - LedgerGroup
                        if (ledgerGroupTotal >= 0) {
                            if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Income")) {
                                if (ledgerGroup.getGroupName().trim().equalsIgnoreCase("Sales Accounts")) {
                                    ledgerGroupTotal -= salesReturnTotal;
                                }
                                if (ledgerGroupTotal != 0) {
                                    directIncomeList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName() + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                }
                                tradingCreditAmt += ledgerGroupTotal;
                            } else if (ledgerGroupTotal != 0) {
                                indirectIncomeList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName() + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                profitLosssCreditAmt += ledgerGroupTotal;
                            }

                        } else if (ledgerGroupTotal < 0) {

                            if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Income")) {
                                if (ledgerGroup.getGroupName().trim().equalsIgnoreCase("Sales Accounts")) {
                                    ledgerGroupTotal += salesReturnTotal;
                                }
                                if (ledgerGroupTotal != 0) {
                                    directExpenseList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName() + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                }
                                tradingDebitAmt += Math.abs(ledgerGroupTotal);
                            } else if (ledgerGroupTotal != 0) {
                                indirectExpenseList.add(new Object[]{"<html><b>" + ledgerGroup.getGroupName() + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(ledgerGroupTotal) + "</b></html>"});
                                profitLosssDebitAmt += Math.abs(ledgerGroupTotal);

                            }

                        }

                        //Credit Side - Detailed View of Ledger
                        if (detailViewCheckBox.isSelected()) {
                            for (Ledger ledger : listOfLedger) {
                               if (ledgerBalanceHashMap.get(ledger.getLedgerName())!=null){
                                if (ledgerBalanceHashMap.get(ledger.getLedgerName()) > 0) {
                                    if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Income")) {
                                        directIncomeList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledgerBalanceHashMap.get(ledger.getLedgerName())) + "</i></html>"});
                                    } else {
                                        indirectIncomeList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledgerBalanceHashMap.get(ledger.getLedgerName())) + "</i></html>"});
                                    }
                                } else if (ledgerBalanceHashMap.get(ledger.getLedgerName())< 0) {
                                    if (ledgerGroup.getAccountType().trim().equalsIgnoreCase("Direct Income")) {
                                        directExpenseList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledgerBalanceHashMap.get(ledger.getLedgerName())) + "</i></html>"});
                                    } else {
                                        indirectExpenseList.add(new Object[]{"<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + ledger.getLedgerName()
                                            + "&nbsp;&nbsp;&nbsp;&nbsp;" + commonService.formatIntoCurrencyAsString(ledgerBalanceHashMap.get(ledger.getLedgerName())) + "</i></html>"});
                                    }
                                }
                            }
                            }
                        }
                    }//direct or indirect income closed
                }
            }
            }
            log.info("/////////closing started at: " + Calendar.getInstance().getTime());
            closingStock = plaService.findClosingStockByDateRange(GlobalProperty.getCompany().getStartDate(), toDate);
            log.info("/////////closing End at: " + Calendar.getInstance().getTime());
            //closingStock=openingBalanceTot+plaService.findClosingStockByDateRange(fromDate, toDate);
            //System.out.println("OPENING .........."+openingBalanceTot+"CLOSING.........."+plaService.findClosingStockByDateRange(commonService.utilDateToSqlDate(commonService.addDaysToDate(fromDate,-1)), toDate));
            tradingCreditAmt += closingStock;
            if (closingStock != 0) {
                closingStockList.add(new Object[]{"<html><b>Closing Stock</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(closingStock) + "</b></html>"});
            }

            //opening Stock
            if (detailViewCheckBox.isSelected() && openingBalanceTot != 0) {
                openingStockList = new ArrayList<>(0);
                openingStockList.add(new Object[]{"<html><b>Opening Stock</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(openingBalanceTot) + "</b></html>"});
                openingStockList.add(new Object[]{"<html><nbsp>&nbsp&nbsp&nbsp&nbsp<i>Please view Stock Summary for further details [Ctrl+Shift+U] </i></html>"});
            }
            for (Object[] objects : openingStockList) {
                debitSideTableModel.addRow(objects);
            }

            //direct expenses
            for (Object[] objects : directExpenseList) {
                debitSideTableModel.addRow(objects);
            }

            //Direct Income
            for (Object[] objects : directIncomeList) {
                creditSideTableModel.addRow(objects);
            }

            //closing stock
            for (Object[] objects : closingStockList) {
                creditSideTableModel.addRow(objects);
            }

            //closing stock detailed
            if (detailViewCheckBox.isSelected()) {
                creditSideTableModel.addRow(new Object[]{"<html>&nbsp&nbsp&nbsp&nbsp<i>Please view Stock Summary for further details [Ctrl+Shift+U] </i></html>"});
            }
            //total Trading A/c amount calculation
            double totalAmount = 0.00;
            diffAmountPLandTrading = 0.00;
            PL_DebitBalance = true;
            if (tradingDebitAmt > tradingCreditAmt) {
                diffAmountPLandTrading = tradingDebitAmt - tradingCreditAmt;
                creditSideTableModel.addRow(new Object[]{null});
                creditSideTableModel.addRow(new Object[]{"<html><b>" + "Gross Loss c/d" + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmountPLandTrading) + "</b></html>"});
                creditSideTableModel.addRow(new Object[]{null});
                totalAmount = tradingDebitAmt;
            } else {
                diffAmountPLandTrading = tradingCreditAmt - tradingDebitAmt;
                debitSideTableModel.addRow(new Object[]{null});
                debitSideTableModel.addRow(new Object[]{"<html><b>" + "Gross Profit c/d" + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmountPLandTrading) + "</b></html>"});
                debitSideTableModel.addRow(new Object[]{null});
                totalAmount = tradingCreditAmt;
                PL_DebitBalance = false;
            }

            //equalize rows in both debit & credit table
            int row = debitSideTable.getRowCount();
            if (creditSideTable.getRowCount() > row) {
                row = creditSideTable.getRowCount();
            }
            for (int i = debitSideTable.getRowCount(); i < row; i++) {
                debitSideTableModel.addRow(new Object[]{null});
            }
            for (int i = creditSideTable.getRowCount(); i < row; i++) {
                creditSideTableModel.addRow(new Object[]{null});
            }

            debitSideTableModel.addRow(new Object[]{"<html><b><u Style=”border-bottom:1px solid black;”>" + " Total" + "</u></b></html>", "<html><b><u Style=”border-bottom:1px solid black;”>" + commonService.formatIntoCurrencyAsString(totalAmount) + "</u></b></html>"});
            creditSideTableModel.addRow(new Object[]{"<html><b><u Style=”border-bottom:1px solid black;”>" + " Total" + "</u></b></html>", "<html><b><u Style=”border-bottom:1px solid black;”>" + commonService.formatIntoCurrencyAsString(totalAmount) + "</u></b></html>"});

            // Profit & Loss Calculations
            debitSideTableModel.addRow(new Object[]{null});
            creditSideTableModel.addRow(new Object[]{null});

            if (PL_DebitBalance) {
                debitSideTableModel.addRow(new Object[]{"<html><b>" + "Gross Loss b/d", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmountPLandTrading) + "</b></html>"});
                profitLosssDebitAmt += diffAmountPLandTrading;
            } else {
                creditSideTableModel.addRow(new Object[]{"<html><b>" + "Gross Profit b/d", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmountPLandTrading) + "</b></html>"});
                profitLosssCreditAmt += diffAmountPLandTrading;
            }

            //indirect Expenses
            for (Object[] objects : indirectExpenseList) {
                debitSideTableModel.addRow(objects);
            }

            //indirect Income
            for (Object[] objects : indirectIncomeList) {
                creditSideTableModel.addRow(objects);
            }

            //total Profit & Loss A/c amount calculation
            totalAmount = 0.00;
            diffAmountPLandTrading = 0.00;
            PL_DebitBalance = true;
            debitSideTableModel.addRow(new Object[]{null});
            if (profitLosssDebitAmt > profitLosssCreditAmt) {
                diffAmountPLandTrading = profitLosssDebitAmt - profitLosssCreditAmt;
                creditSideTableModel.addRow(new Object[]{null});
                creditSideTableModel.addRow(new Object[]{"<html><b>" + "Net Loss c/d" + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmountPLandTrading) + "</b></html>"});
                creditSideTableModel.addRow(new Object[]{null});
                totalAmount = profitLosssDebitAmt;
            } else {
                diffAmountPLandTrading = profitLosssCreditAmt - profitLosssDebitAmt;
                debitSideTableModel.addRow(new Object[]{null});
                debitSideTableModel.addRow(new Object[]{"<html><b>" + "Net Profit c/d" + "</b></html>", "<html><b>" + commonService.formatIntoCurrencyAsString(diffAmountPLandTrading) + "</b></html>"});
                debitSideTableModel.addRow(new Object[]{null});
                totalAmount = profitLosssCreditAmt;
                PL_DebitBalance = false;
            }

            //equalize rows in both debit & credit table after trading
            row = debitSideTable.getRowCount();
            if (creditSideTable.getRowCount() > row) {
                row = creditSideTable.getRowCount();
            }
            for (int i = debitSideTable.getRowCount(); i < row; i++) {
                debitSideTableModel.addRow(new Object[]{null});
            }
            for (int i = creditSideTable.getRowCount(); i < row; i++) {
                creditSideTableModel.addRow(new Object[]{null});
            }

            debitSideTableModel.addRow(new Object[]{"<html><b><u Style=”border-bottom:1px solid black;”>" + " Total" + "</u></b></html>", "<html><b><u Style=”border-bottom:1px solid black;”>" + commonService.formatIntoCurrencyAsString(totalAmount) + "</u></b></html>"});
            creditSideTableModel.addRow(new Object[]{"<html><b><u Style=”border-bottom:1px solid black;”>" + " Total" + "</u></b></html>", "<html><b><u Style=”border-bottom:1px solid black;”>" + commonService.formatIntoCurrencyAsString(totalAmount) + "</u></b></html>"});

            //Balace Sheet Section
            if (isBalancesheet) {
                fillBalanceSheetNew();
            }
        } catch (Exception e) {
            log.error("fillPLAccountTableNew:", e);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headingPanel = new javax.swing.JPanel();
        centerHeadingLabel = new javax.swing.JLabel();
        printPanel = new javax.swing.JPanel();
        printButton = new javax.swing.JButton();
        pdfButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        detailViewCheckBox = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        fromDateChooser = new com.toedter.calendar.JDateChooser();
        toDateChooser = new com.toedter.calendar.JDateChooser();
        jLabel2 = new javax.swing.JLabel();
        filterButton = new javax.swing.JButton();
        bodyPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        debitSideTable = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        creditSideTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        headingPanel.setBackground(new java.awt.Color(47, 105, 142));
        headingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        headingPanel.setPreferredSize(new java.awt.Dimension(1124, 50));

        centerHeadingLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        centerHeadingLabel.setForeground(new java.awt.Color(255, 255, 255));
        centerHeadingLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        centerHeadingLabel.setText("Trading And Profit And Loss Account");

        printPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        printPanel.setOpaque(false);
        printPanel.setLayout(new java.awt.GridLayout(1, 0));

        printButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/print.png"))); // NOI18N
        printButton.setText("Print");
        printButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printButtonActionPerformed(evt);
            }
        });
        printPanel.add(printButton);

        pdfButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/pdf-icon.png"))); // NOI18N
        pdfButton.setText("PDF");
        pdfButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdfButtonActionPerformed(evt);
            }
        });
        printPanel.add(pdfButton);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel1.setOpaque(false);

        detailViewCheckBox.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        detailViewCheckBox.setForeground(new java.awt.Color(255, 255, 255));
        detailViewCheckBox.setText("Detailed View");
        detailViewCheckBox.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        detailViewCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        detailViewCheckBox.setOpaque(false);
        detailViewCheckBox.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                detailViewCheckBoxStateChanged(evt);
            }
        });
        detailViewCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                detailViewCheckBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(detailViewCheckBox, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(detailViewCheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("From");

        fromDateChooser.setDateFormatString("dd/MM/yyyy");
        fromDateChooser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fromDateChooserMouseClicked(evt);
            }
        });
        fromDateChooser.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                fromDateChooserFocusLost(evt);
            }
        });
        fromDateChooser.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                fromDateChooserPropertyChange(evt);
            }
        });
        fromDateChooser.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                fromDateChooserInputMethodTextChanged(evt);
            }
        });

        toDateChooser.setDateFormatString("dd/MM/yyyy");
        toDateChooser.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                toDateChooserFocusGained(evt);
            }
        });
        toDateChooser.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                toDateChooserPropertyChange(evt);
            }
        });

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("To");

        filterButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/search.png"))); // NOI18N
        filterButton.setText("Filter");
        filterButton.setOpaque(false);
        filterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filterButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout headingPanelLayout = new javax.swing.GroupLayout(headingPanel);
        headingPanel.setLayout(headingPanelLayout);
        headingPanelLayout.setHorizontalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(centerHeadingLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 299, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fromDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(toDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(filterButton)
                .addGap(98, 98, 98)
                .addComponent(printPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        headingPanelLayout.setVerticalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(printPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(centerHeadingLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(headingPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(fromDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)
                            .addComponent(toDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(filterButton))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(headingPanel, java.awt.BorderLayout.PAGE_START);

        bodyPanel.setOpaque(false);
        bodyPanel.setLayout(new java.awt.GridLayout(1, 0));

        debitSideTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        debitSideTable.setOpaque(false);
        debitSideTable.setSelectionBackground(new java.awt.Color(153, 153, 153));
        debitSideTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                debitSideTableMouseClicked(evt);
            }
        });
        debitSideTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                debitSideTableKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(debitSideTable);

        bodyPanel.add(jScrollPane1);

        creditSideTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        creditSideTable.setOpaque(false);
        creditSideTable.setSelectionBackground(new java.awt.Color(153, 153, 153));
        creditSideTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                creditSideTableMouseClicked(evt);
            }
        });
        creditSideTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                creditSideTableKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(creditSideTable);

        bodyPanel.add(jScrollPane2);

        add(bodyPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void detailViewCheckBoxStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_detailViewCheckBoxStateChanged

    }//GEN-LAST:event_detailViewCheckBoxStateChanged

    private void debitSideTableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_debitSideTableKeyPressed

    }//GEN-LAST:event_debitSideTableKeyPressed

    private void debitSideTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_debitSideTableMouseClicked

    }//GEN-LAST:event_debitSideTableMouseClicked

    private void creditSideTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_creditSideTableMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_creditSideTableMouseClicked

    private void creditSideTableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_creditSideTableKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_creditSideTableKeyPressed

    private void detailViewCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_detailViewCheckBoxActionPerformed
        fillPLAccountTableNew();
    }//GEN-LAST:event_detailViewCheckBoxActionPerformed

    private void printButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printButtonActionPerformed
        try {
            BalanceSheetReport balanceSheetReport = new BalanceSheetReport();
            if (MsgBox.confirm("Are you sure you want to Print?")) {
                if (balanceSheetReport.createBalanceSheetReport(debitSideTable, creditSideTable, "BalanceSheet", "print")) {

                } else {
                    MsgBox.warning("Something went wrong, please re-click the button again!");
                }
            }
        } catch (Exception ex) {
            log.error("printButtonActionPerformed:", ex);
        }
    }//GEN-LAST:event_printButtonActionPerformed

    private void pdfButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdfButtonActionPerformed
        try {
            BalanceSheetReport balanceSheetReport = new BalanceSheetReport();
            if (MsgBox.confirm("Are you sure you want to create pdf?")) {
                if (balanceSheetReport.createBalanceSheetReport(debitSideTable, creditSideTable, "BalanceSheet", "pdf")) {

                } else {
                    MsgBox.warning("Something went wrong, please re-click the button again!");
                }
            }
        } catch (Exception ex) {
            log.error("pdfButtonActionPerformed:", ex);
        }
    }//GEN-LAST:event_pdfButtonActionPerformed

    private void filterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filterButtonActionPerformed
        try {
            fromDate = commonService.jDateChooserToSqlDate(fromDateChooser);
            toDate = commonService.jDateChooserToSqlDate(toDateChooser);

        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(PLAccountView.class.getName()).log(Level.SEVERE, null, ex);
        }
        fillPLAccountTableNew();

    }//GEN-LAST:event_filterButtonActionPerformed

    private void fromDateChooserFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fromDateChooserFocusLost

    }//GEN-LAST:event_fromDateChooserFocusLost

    private void toDateChooserFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_toDateChooserFocusGained

    }//GEN-LAST:event_toDateChooserFocusGained

    private void fromDateChooserInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_fromDateChooserInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_fromDateChooserInputMethodTextChanged

    private void fromDateChooserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fromDateChooserMouseClicked
        try {
            fromDate = commonService.jDateChooserToSqlDate(fromDateChooser);
            toDateChooser.setMinSelectableDate(fromDate);
            System.out.println("fromDate=" + fromDate);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(PLAccountView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_fromDateChooserMouseClicked

    private void fromDateChooserPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_fromDateChooserPropertyChange
        toDateChooser.setMinSelectableDate(fromDateChooser.getDate());
    }//GEN-LAST:event_fromDateChooserPropertyChange

    private void toDateChooserPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_toDateChooserPropertyChange
        fromDateChooser.setMaxSelectableDate(toDateChooser.getDate());
    }//GEN-LAST:event_toDateChooserPropertyChange


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bodyPanel;
    private javax.swing.JLabel centerHeadingLabel;
    private javax.swing.JTable creditSideTable;
    private javax.swing.JTable debitSideTable;
    private javax.swing.JCheckBox detailViewCheckBox;
    private javax.swing.JButton filterButton;
    private com.toedter.calendar.JDateChooser fromDateChooser;
    private javax.swing.JPanel headingPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton pdfButton;
    private javax.swing.JButton printButton;
    private javax.swing.JPanel printPanel;
    private com.toedter.calendar.JDateChooser toDateChooser;
    // End of variables declaration//GEN-END:variables

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    //HibernateUtil hibernateUtil=new HibernateUtil();

                    CompanyDAO companyDAO = new CompanyDAO();
                    GlobalProperty.setCompany(companyDAO.getDefaultCompany());
                    MainForm jMainFrame = new MainForm();
                    jMainFrame.setVisible(true);
                    jMainFrame.setExtendedState(MainForm.MAXIMIZED_BOTH);

                    PLAccountView trialBalanceView = new PLAccountView(false);
                    jMainFrame.getContentPanel().removeAll();
                    jMainFrame.getContentPanel().repaint();
                    jMainFrame.getContentPanel().revalidate();
                    jMainFrame.getContentPanel().add(trialBalanceView);

                } catch (Exception e) {
                    log.error("MainForm run :", e);
                }
            }
        });
    }
}
