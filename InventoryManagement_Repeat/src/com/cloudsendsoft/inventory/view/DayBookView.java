/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.CTableModel;
import com.cloudsendsoft.inventory.dao.ContraDAO;
import com.cloudsendsoft.inventory.dao.JournalDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.PaymentDAO;
import com.cloudsendsoft.inventory.dao.PurchaseDAO;
import com.cloudsendsoft.inventory.dao.PurchaseReturnDAO;
import com.cloudsendsoft.inventory.dao.ReceiptDAO;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesReturnDAO;
import com.cloudsendsoft.inventory.model.Contra;
import com.cloudsendsoft.inventory.model.Journal;
import com.cloudsendsoft.inventory.model.Payment;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseReturn;
import com.cloudsendsoft.inventory.model.Receipt;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesReturn;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.LedgerService;
import com.cloudsendsoft.inventory.services.PLAService;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author LOGON SOFT 1
 */
public class DayBookView extends javax.swing.JPanel {

    /**
     * Creates new form DayBookView
     */
    CommonService commonService = new CommonService();
    CTableModel dayBookTableModel = null;
    Date fromDate = GlobalProperty.getCompany().getStartDate();
    Date toDate = GlobalProperty.getCompany().getEndDate();
    
    List<Purchase> purchaseList =null;
    List<Purchase> purchaseDetailsList=null;
    List<PurchaseReturn>purchaseReturnList = null;
    List<PurchaseReturn>purchaseReturnDetailsList = null;
    List<Sales> salesList = null;
    List<Sales> salesDetailsList = null;
    List<SalesReturn>salesReturnList = null;
    List<SalesReturn>salesReturnDetailsList = null;
    List<Payment> paymentList = null;
    List<Receipt> receiptList = null;
    List<Journal> journalList = null;
    List<Contra> contraList = null;
    List<Payment> paymentDetails = null;
    List<Receipt> receiptDetails = null;
    List<Journal> journalDetails = null;
    List<Contra> contraDetails = null;
    
    PLAService plaService = new PLAService();
    LedgerService ledgerService = new LedgerService();
    
    LedgerDAO ledgerDAO = new LedgerDAO();
    PaymentDAO paymentDAO = new PaymentDAO();
    ReceiptDAO receiptDAO = new ReceiptDAO();
    JournalDAO journalDAO = new JournalDAO();
    PurchaseDAO purchaseDAO = new PurchaseDAO();
    PurchaseReturnDAO purchaseReturnDAO = new PurchaseReturnDAO();            
    SalesDAO salesDAO = new SalesDAO();
    SalesReturnDAO salesReturnDAO = new SalesReturnDAO();
    ContraDAO contraDAO = new ContraDAO();
    List tableRow=new ArrayList();
    String dayBookTableClickType,dayBookTableClickInvoice;
    int index=0;
    public DayBookView() {
        initComponents();
        dayBookTable.setRowHeight(GlobalProperty.tableRowHieght);
        dayBookTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
        //alternate row color jtable
            @Override
            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int column) {
                Component c = super.getTableCellRendererComponent(table,
                        value, isSelected, hasFocus, row, column);
                c.setBackground(row % 2 == 0 ? Color.GRAY : Color.LIGHT_GRAY);
                return c;
            }
        ;
        });

        dayBookTable.setShowGrid(false);
        dayBookTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{"Date", "Particulars","Invoice Type","Inv. No.","Debit", "Credit"}
        ));
        dayBookTableModel = (CTableModel) dayBookTable.getModel();
        CommonService.setWidthAsPercentages(dayBookTable,.20, .80, .20, .20,.20,.20);
        dayBookTableModel.setRowCount(0);
        fromDateChooser.setDate(fromDate);
        toDateChooser.setDate(toDate);
        commonService.setCurrentPeriodStartOnCalendar(fromDateChooser);
        commonService.setCurrentPeriodEndOnCalendar(toDateChooser);
        fillDayBookTable();
                SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                dayBookTable.requestFocus();
                if(dayBookTable.getRowCount()>0)
                {
                    dayBookTable.setRowSelectionInterval(0,0);
                }

              }
        });
    }
    public void fillDayBookTable()
    {
        dayBookTableModel.setRowCount(0);
        tableRow.clear();
        //Payments
        paymentList=paymentDAO.findAllByDate(fromDate, toDate);
        Iterator<Payment> payments = paymentList.iterator();
        while (payments.hasNext()) {
            Payment payment = payments.next();
            tableRow.add(new Object[]{payment.getPaymentDate(),payment.getLedgerBy().getLedgerName(),"Payment",payment.getPaymentNo(),"",payment.getAmount()});
        }
        //Receipts
        receiptList=receiptDAO.findAllByDate(fromDate, toDate);
        Iterator<Receipt> receipts = receiptList.iterator();
        while (receipts.hasNext()) {
            Receipt receipt = receipts.next();
            tableRow.add(new Object[]{receipt.getReceiptDate(),receipt.getLedgerTo().getLedgerName(),"Receipt",receipt.getReceiptNo(),receipt.getAmount(),""});
        }
        //Journals
        journalList = journalDAO.findAllByDate(fromDate, toDate);
        Iterator<Journal> journals = journalList.iterator();
        while (journals.hasNext()) {
            Journal journal = journals.next();
            tableRow.add(new Object[]{journal.getJournalDate(),journal.getLedgerTo().getLedgerName(),"Journal",journal.getJournalNo(),journal.getAmount(),""});
            tableRow.add(new Object[]{journal.getJournalDate(),journal.getLedgerBy().getLedgerName(),"Journal",journal.getJournalNo(),"",journal.getAmount()});
        }
        //Contra
        contraList = contraDAO.findAllByDate(fromDate, toDate);
        Iterator<Contra> contra = contraList.iterator();
        while (contra.hasNext()) {
            Contra contra1 = contra.next();
            tableRow.add(new Object[]{contra1.getContraDate(),contra1.getLedgerTo().getLedgerName(),"Contra",contra1.getContraNo(),contra1.getAmount(),""});
            tableRow.add(new Object[]{contra1.getContraDate(),contra1.getLedgerBy().getLedgerName(),"Contra",contra1.getContraNo(),"",contra1.getAmount()});
        }
        //Purchases
        purchaseList = purchaseDAO.findAllByDate(fromDate, toDate);
        Iterator<Purchase> purchases =purchaseList.iterator();
        while (purchases.hasNext()) {
            Purchase purchase = purchases.next();
            tableRow.add(new Object[]{purchase.getBillDate(),purchase.getSupplier().getLedgerName(),"Purchase",purchase.getInvoiceNumber(),"",purchase.getGrandTotal()});
        }
        //Sales
        salesList = salesDAO.findAllByDate(fromDate, toDate);
        Iterator<Sales> sales =salesList.iterator();
        while (sales.hasNext()) {
            Sales sales1 = sales.next();
            tableRow.add(new Object[]{sales1.getBillDate(),sales1.getCustomer().getLedgerName(),"Sales",sales1.getInvoiceNumber(),sales1.getGrandTotal(),""});
            
        }
        //Purchase Returns
        purchaseReturnList = purchaseReturnDAO.findAllByDate(fromDate, toDate);
        Iterator<PurchaseReturn> purchaseReturns =purchaseReturnList.iterator();
        while (purchaseReturns.hasNext()) {
            PurchaseReturn purchaseReturn = purchaseReturns.next();
            tableRow.add(new Object[]{purchaseReturn.getBillDate(),purchaseReturn.getSupplier().getLedgerName(),"Purchase Return",purchaseReturn.getInvoiceNumber(),purchaseReturn.getGrandTotal(),""});
        }
        //Sales Returns
        salesReturnList = salesReturnDAO.findAllByDate(fromDate, toDate);
        Iterator<SalesReturn> salesReturns =salesReturnList.iterator();
        while (salesReturns.hasNext()) {
            SalesReturn salesReturn = salesReturns.next();
            tableRow.add(new Object[]{salesReturn.getBillDate(),salesReturn.getCustomer().getLedgerName(),"Sales Return",salesReturn.getInvoiceNumber(),"",salesReturn.getGrandTotal()});
        }
        //Adding Rows to Table
        dayBookTableModel.setRowCount(0);
        int i=0;
        Iterator iter =tableRow.iterator();
        while (iter.hasNext()) {
            Object object = iter.next();
            dayBookTableModel.addRow((Object[]) object);
            Date sqlDate = (Date) dayBookTableModel.getValueAt(i, 0);
            dayBookTableModel.setValueAt(CommonService.sqlDateToString(sqlDate), i, 0);
            i++;
        }
        //Sorting Based on Date
        commonService.sortAllRowsBy(dayBookTableModel, 0, true);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headingPanel = new javax.swing.JPanel();
        centerHeadingLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        fromDateChooser = new com.toedter.calendar.JDateChooser();
        toDateChooser = new com.toedter.calendar.JDateChooser();
        jLabel2 = new javax.swing.JLabel();
        filterButton = new javax.swing.JButton();
        bodyPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        dayBookTable = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        headingPanel.setBackground(new java.awt.Color(47, 105, 142));
        headingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        headingPanel.setPreferredSize(new java.awt.Dimension(1124, 50));

        centerHeadingLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        centerHeadingLabel.setForeground(new java.awt.Color(255, 255, 255));
        centerHeadingLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        centerHeadingLabel.setText("Day Book");

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("From");

        fromDateChooser.setDateFormatString("dd/MM/yyyy");
        fromDateChooser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fromDateChooserMouseClicked(evt);
            }
        });
        fromDateChooser.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                fromDateChooserFocusLost(evt);
            }
        });
        fromDateChooser.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                fromDateChooserPropertyChange(evt);
            }
        });
        fromDateChooser.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                fromDateChooserInputMethodTextChanged(evt);
            }
        });

        toDateChooser.setDateFormatString("dd/MM/yyyy");
        toDateChooser.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                toDateChooserFocusGained(evt);
            }
        });
        toDateChooser.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                toDateChooserPropertyChange(evt);
            }
        });

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("To");

        filterButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/search.png"))); // NOI18N
        filterButton.setText("Filter");
        filterButton.setOpaque(false);
        filterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filterButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout headingPanelLayout = new javax.swing.GroupLayout(headingPanel);
        headingPanel.setLayout(headingPanelLayout);
        headingPanelLayout.setHorizontalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(centerHeadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 228, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fromDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(toDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(filterButton)
                .addContainerGap())
        );
        headingPanelLayout.setVerticalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(centerHeadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(headingPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(fromDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)
                            .addComponent(toDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(filterButton))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(headingPanel, java.awt.BorderLayout.PAGE_START);

        bodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        bodyPanel.setOpaque(false);

        dayBookTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        dayBookTable.setOpaque(false);
        dayBookTable.setSelectionBackground(new java.awt.Color(153, 153, 153));
        dayBookTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dayBookTableMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                dayBookTableMouseEntered(evt);
            }
        });
        dayBookTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dayBookTableKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(dayBookTable);

        javax.swing.GroupLayout bodyPanelLayout = new javax.swing.GroupLayout(bodyPanel);
        bodyPanel.setLayout(bodyPanelLayout);
        bodyPanelLayout.setHorizontalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 906, Short.MAX_VALUE)
        );
        bodyPanelLayout.setVerticalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 397, Short.MAX_VALUE)
        );

        add(bodyPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void fromDateChooserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fromDateChooserMouseClicked

    }//GEN-LAST:event_fromDateChooserMouseClicked

    private void fromDateChooserFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fromDateChooserFocusLost

    }//GEN-LAST:event_fromDateChooserFocusLost

    private void fromDateChooserPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_fromDateChooserPropertyChange
        toDateChooser.setMinSelectableDate(fromDateChooser.getDate());
    }//GEN-LAST:event_fromDateChooserPropertyChange

    private void fromDateChooserInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_fromDateChooserInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_fromDateChooserInputMethodTextChanged

    private void toDateChooserFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_toDateChooserFocusGained

    }//GEN-LAST:event_toDateChooserFocusGained

    private void toDateChooserPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_toDateChooserPropertyChange
        fromDateChooser.setMaxSelectableDate(toDateChooser.getDate());
    }//GEN-LAST:event_toDateChooserPropertyChange

    private void filterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filterButtonActionPerformed
        try {
            fromDate =commonService.jDateChooserToSqlDate(fromDateChooser);
            toDate = commonService.jDateChooserToSqlDate(toDateChooser);
            fillDayBookTable();
        } catch (ParseException ex) {
            Logger.getLogger(DayBookView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_filterButtonActionPerformed

    private void dayBookTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dayBookTableMouseClicked
        if(evt.getClickCount()==2)
         {
             DayBookView dayBookView=new DayBookView();
            index=dayBookTable.getSelectedRow();
            clickAction();
         }

        
    }//GEN-LAST:event_dayBookTableMouseClicked

    private void dayBookTableMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dayBookTableMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_dayBookTableMouseEntered

    private void dayBookTableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dayBookTableKeyPressed
             if (evt.getKeyCode() == KeyEvent.VK_ENTER ) {
            index = dayBookTable.getSelectedRow();
            clickAction();
        }
        
    }//GEN-LAST:event_dayBookTableKeyPressed
    void clickAction()
    {
        dayBookTableClickType=dayBookTable.getModel().getValueAt(index,2).toString();
            dayBookTableClickInvoice=dayBookTable.getModel().getValueAt(index,3).toString();
             if(dayBookTableClickType.equalsIgnoreCase("Purchase"))
             {
               PurchaseView purchaseView = new PurchaseView(MainForm.jMainFrame);
               purchaseDetailsList=purchaseDAO.findAllByInvoiceNo(dayBookTableClickInvoice);
               purchaseView.listPurchaseItemHistory(purchaseDetailsList.get(0));  
               purchaseView.fillPurchaseItemTable();
               GlobalProperty.mainForm.getContentPanel().removeAll();
               GlobalProperty.mainForm.getContentPanel().repaint();
               GlobalProperty.mainForm.getContentPanel().revalidate();
               GlobalProperty.mainForm.getContentPanel().add(purchaseView);
             }
             if(dayBookTableClickType.equalsIgnoreCase("Sales"))
             {
               SalesView salesView = new SalesView(MainForm.jMainFrame);
               salesDetailsList=salesDAO.findAllByInvoiceNo(dayBookTableClickInvoice);
               salesView.listSalesItemHistory(salesDetailsList.get(0));  
               salesView.fillSalesItemTable();
               GlobalProperty.mainForm.getContentPanel().removeAll();
               GlobalProperty.mainForm.getContentPanel().repaint();
               GlobalProperty.mainForm.getContentPanel().revalidate();
               GlobalProperty.mainForm.getContentPanel().add(salesView);
             }
             if(dayBookTableClickType.equalsIgnoreCase("Sales Return"))
             {
                salesReturnDetailsList=salesReturnDAO.findAllByInvoiceNo(dayBookTableClickInvoice);
               SalesReturnItemView salesReturnItemView = new SalesReturnItemView(salesReturnDetailsList.get(0).getSales()); 
               salesReturnItemView.listSalesReturnItemHistory(salesReturnDetailsList.get(0)); 
               salesReturnItemView.fillSalesItemTable();
               GlobalProperty.mainForm.getContentPanel().removeAll();
               GlobalProperty.mainForm.getContentPanel().repaint();
               GlobalProperty.mainForm.getContentPanel().revalidate();
               GlobalProperty.mainForm.getContentPanel().add(salesReturnItemView);
             }
             if(dayBookTableClickType.equalsIgnoreCase("Purchase Return"))
             {
                purchaseReturnDetailsList=purchaseReturnDAO.findAllByInvoiceNo(dayBookTableClickInvoice);
               PurchaseReturnItemView purchaseReturnItemView = new PurchaseReturnItemView(purchaseReturnDetailsList.get(0).getPurchase()); 
               purchaseReturnItemView.listPurchaseReturnItemHistory(purchaseReturnDetailsList.get(0)); 
               purchaseReturnItemView.fillPurchaseItemTable();
               GlobalProperty.mainForm.getContentPanel().removeAll();
               GlobalProperty.mainForm.getContentPanel().repaint();
               GlobalProperty.mainForm.getContentPanel().revalidate();
               GlobalProperty.mainForm.getContentPanel().add(purchaseReturnItemView);
             }
              if (dayBookTableClickType.equalsIgnoreCase("Payment")) {
                JournalEntryView journalEntryView = new JournalEntryView("Payment");
                paymentDetails=paymentDAO.findAllByInvoiceNo(dayBookTableClickInvoice);
                journalEntryView.listPaymentItemHistory(paymentDetails.get(0));
                GlobalProperty.mainForm.getContentPanel().removeAll();
                GlobalProperty.mainForm.getContentPanel().repaint();
                GlobalProperty.mainForm.getContentPanel().revalidate();
                GlobalProperty.mainForm.getContentPanel().add(journalEntryView);
            }
            if (dayBookTableClickType.equalsIgnoreCase("Receipt")) {
               
                JournalEntryView journalEntryView = new JournalEntryView("Receipt");
                receiptDetails=receiptDAO.findAllByInvoiceNo(dayBookTableClickInvoice);
                journalEntryView.listReceiptItemHistory(receiptDetails.get(0));
                GlobalProperty.mainForm.getContentPanel().removeAll();
                GlobalProperty.mainForm.getContentPanel().repaint();
                GlobalProperty.mainForm.getContentPanel().revalidate();
                GlobalProperty.mainForm.getContentPanel().add(journalEntryView);
            }
            if (dayBookTableClickType.equalsIgnoreCase("Journal")) {
              
                JournalEntryView journalEntryView = new JournalEntryView("Journal");
                journalDetails=journalDAO.findAllByInvoiceNo(dayBookTableClickInvoice);
                journalEntryView.listJournalItemHistory(journalDetails.get(0));
                GlobalProperty.mainForm.getContentPanel().removeAll();
                GlobalProperty.mainForm.getContentPanel().repaint();
                GlobalProperty.mainForm.getContentPanel().revalidate();
                GlobalProperty.mainForm.getContentPanel().add(journalEntryView);
            }
            if (dayBookTableClickType.equalsIgnoreCase("Contra")) {
                
                JournalEntryView journalEntryView = new JournalEntryView("Contra");
                contraDetails=contraDAO.findAllByInvoiceNo(dayBookTableClickInvoice);
                journalEntryView.listContraItemHistory(contraDetails.get(0));
                GlobalProperty.mainForm.getContentPanel().removeAll();
                GlobalProperty.mainForm.getContentPanel().repaint();
                GlobalProperty.mainForm.getContentPanel().revalidate();
                GlobalProperty.mainForm.getContentPanel().add(journalEntryView);
            }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bodyPanel;
    private javax.swing.JLabel centerHeadingLabel;
    private javax.swing.JTable dayBookTable;
    private javax.swing.JButton filterButton;
    private com.toedter.calendar.JDateChooser fromDateChooser;
    private javax.swing.JPanel headingPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private com.toedter.calendar.JDateChooser toDateChooser;
    // End of variables declaration//GEN-END:variables
}
