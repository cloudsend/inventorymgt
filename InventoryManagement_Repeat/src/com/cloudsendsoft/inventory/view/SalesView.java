/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.MultiCurrencyDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesReturnDAO;
import com.cloudsendsoft.inventory.dao.ShipmentModeDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.report.SalesReport;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.ItemService;
import com.cloudsendsoft.inventory.services.LedgerService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseCharge;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseTax;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesCharge;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreformaTax;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaCharge;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesReturn;
import com.cloudsendsoft.inventory.model.SalesReturnCharge;
import com.cloudsendsoft.inventory.model.SalesTax;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.services.ChargesService;
import com.cloudsendsoft.inventory.services.MultiCurrencyService;
import com.cloudsendsoft.inventory.services.ShipmentModeService;
import com.cloudsendsoft.inventory.services.TaxService;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class SalesView extends javax.swing.JPanel {

    /**
     * Creates new form PurchaseViewPanel
     */
    static double totalAmount = 0;
    double subTotal = 0.00;
    double netTotal = 0.00;
    double subTotal1 = 0.00;
    double netTotal1 = 0.00;
    double totDiscount = 0.00;
    double totDiscount1 = 0.00;
    double grandTotal = 0.00;
    double grandTotal1 = 0.00;
    static final Logger log = Logger.getLogger(SalesView.class.getName());

    ItemDAO itemDAO = new ItemDAO();
    CommonService commonService = new CommonService();
    TaxService taxService= new TaxService();
    MainForm mainForm = null;
    LedgerService ledgerService = new LedgerService();
    TaxDAO taxDAO = new TaxDAO();
    ChargesDAO chargesDAO = new ChargesDAO();
    MultiCurrencyDAO multiCurrencyDAO = new MultiCurrencyDAO();
    ShipmentModeDAO shipmentModeDAO = new ShipmentModeDAO();
    String customerDetails = "";

    //global usage
    Ledger customerLedger = null;
    double itemQty = 0;
    List<SalesItem> salesItemList = new ArrayList<SalesItem>();
    List<SalesItem> salesItemRemovedList = new ArrayList<SalesItem>();

    Item item = null;
    List<SalesTax> salesTaxList = new ArrayList<SalesTax>();
    List<SalesCharge> salesChargesList = new ArrayList<SalesCharge>();

    CRUDServices cRUDServices = new CRUDServices();
    MultiCurrency currency = null;
    NumberProperty numberProperty = null;
    NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
    SalesItem salesItem = null;
    DefaultTableModel salesItemTableModel = null;
    Sales sales = null;
    int check = 0;
    LedgerDAO ledgerDAO = new LedgerDAO();

    Ledger salesLedger = null;
    Ledger discountLedger = null;
    List<Ledger> taxLedgerList = new ArrayList<Ledger>();
    List<Ledger> chargesLedgerList = new ArrayList<Ledger>();
    Ledger ledger = null;
    List<SalesItem> salesItemListHistory = null;
    List<Tax> taxList=null;
    int taxCount=0,chargesCount=0;
    Tax tax=null;
    SalesTax saleTax=null;
    ChargesService chargesService = new ChargesService();
    List<Charges>chargesList=null;
    Charges charges=null;
    double chargesAmt=0.00;
    double chargesAmt1=0.00;
    SalesCharge saleCharge=null;
    MultiCurrencyService multiCurrencyService = new MultiCurrencyService();
    List<MultiCurrency> currencyList = null;
    double currencyCount=0;
    ShipmentModeService shipmentModeService = new ShipmentModeService();
    List<ShipmentMode> shipmentModesList =null;
    ShipmentMode shipmentMode =null;
    int shipmentModesCount =0;
    SalesDAO salesDAO=new SalesDAO();
    boolean isDuplicateColumnsVisible = false;

    public SalesView(MainForm mainForm) {

        try {
            initComponents();
            //hide shipment mode panel
            shipmentModePanel.setVisible(false);
            //hides delivery text
            deliveryLabel.setVisible(false);
            deliveryText.setVisible(false);
            //removed additional qty & rate textboxes by default
            qtyPanel.remove(itemQtyText1);
            chargesPanel1.remove(chargesText1);
            discountPanel1.remove(discountText1);
            //shortcut
            // HERE ARE THE KEY BINDINGS
            
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_W,
                                    java.awt.event.InputEvent.SHIFT_DOWN_MASK
                                    | java.awt.event.InputEvent.CTRL_DOWN_MASK), "forward");
            this.getActionMap().put("forward", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (itemQtyText1.getParent() == qtyPanel) {
                        qtyPanel.remove(itemQtyText1);
                        qtyPanel.revalidate();
                        chargesPanel1.remove(chargesText1);
                        chargesPanel1.revalidate();
                        discountPanel1.remove(discountText1);
                        discountPanel1.revalidate();
                        isDuplicateColumnsVisible = false;
                    } else {
                        qtyPanel.add(itemQtyText1);
                        qtyPanel.revalidate();
                        chargesPanel1.add(chargesText1);
                        chargesPanel1.revalidate();
                        discountPanel1.add(discountText1);
                        discountPanel1.revalidate();
                        isDuplicateColumnsVisible = true;
                    }
                    fillSalesItemTable();
                }
            });
            // END OF KEY BINDINGS
            //shortcut
            
            //print shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_P,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "print");
            this.getActionMap().put("print", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    printInvoice();
                }
            });
            
            //save shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "save");
            this.getActionMap().put("save", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveInvoice();
                }
            });
            //new shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_N,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "new");
            this.getActionMap().put("new", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    newButtonActionPerformed(e);
                }
            });
            
            
            salesLedger = ledgerDAO.findByLedgerName("Sales Account");
            discountLedger = ledgerDAO.findByLedgerName("Discount Allowed");

            commonService.setCurrentPeriodOnCalendar(billDateFormattedText);

            if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
                saveButton.setEnabled(false);
                newButton.setEnabled(false);
               
            }
            if(!GlobalProperty.listOfPrivileges.contains("Delete")){
                deleteButton.setEnabled(false);
            }
            //load invoice number
            numberProperty = numberPropertyDAO.findInvoiceNo("Sales");
            invoiceNumberText.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
            //table background color removed
            jScrollPane1.setOpaque(false);
            jScrollPane1.getViewport().setOpaque(false);
            billDateFormattedText.setDate(new java.util.Date());
            //billDateFormattedText.setText("hhhhhhh");
            this.mainForm = mainForm;
            salesItemTable.setRowHeight(25);

            salesItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                    null,
                    new String[]{"Sr. No.", "Item Code", "Description", "Unit Price", "Qty.", "Discount", "Total Amount"}
            ));
            salesItemTableModel = (DefaultTableModel) salesItemTable.getModel();
            CommonService.setWidthAsPercentages(salesItemTable, .03, .10, .20, .08, .08, .08, .08);
            //textbox initialization
            //currencyText.setText(multiCurrencyDAO.findAll().get(0).getName());
            currency=multiCurrencyDAO.findByName("Indian Rupee");
            taxText.setText("N/A");
            chargesTextBox.setText("N/A");
            shipmentModeText.setText("N/A");
            // Combo boxes initialization
            
//            for (Charges charges : chargesDAO.findAll()) {
//                chargesComboBox.addItem(new ComboKeyValue(charges.getName(), charges));
//            }

//            for (MultiCurrency currency : multiCurrencyDAO.findAll()) {
//                if (currency == null) {
//                    this.currency = currency;
//                }
//                multiCurrencyComboBox.addItem(new ComboKeyValue(currency.getName(), currency));
//            }

//            for (ShipmentMode shipmentMode : shipmentModeDAO.findAll()) {
//                shipmentComboBox.addItem(new ComboKeyValue(shipmentMode.getName(), shipmentMode));
//            }

            //jTable rows & columns alignment
            ((DefaultTableCellRenderer) salesItemTable.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);

            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(JLabel.CENTER);
            salesItemTable.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
            salesItemTable.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
            salesItemTable.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
            salesItemTable.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
            //jTable rows & columns alignment
            SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
              customerText.requestFocus();
            }
              });
            setFocusOrder();

        } catch (Exception e) {
            log.error("Purchase:", e);
        }

    }

    void setFocusOrder() {
        try {
            final Set<Character> pressed = new HashSet<Character>();
            customerText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    
                    itemCodeText.requestFocusInWindow();
                }
            });
            // Set of currently pressed keys
           
           itemCodeText.addKeyListener(new KeyAdapter() {
            
            @Override
            public synchronized void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_CONTROL||e.getKeyCode()==KeyEvent.VK_SPACE)
                    pressed.add(e.getKeyChar());
                if (pressed.size() ==2) {
                    taxText.requestFocusInWindow();
                    pressed.clear();
                }
            //}
          // public void keyPressed(KeyEvent e) {
           //int key = e.getKeyCode();
          // if (key == KeyEvent.VK_CONTROL) {
           //    taxText.requestFocusInWindow();
           //}
           else if(e.getKeyCode()==KeyEvent.VK_ENTER)
           {
            itemQtyText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(isDuplicateColumnsVisible)
                       itemQtyText1.requestFocusInWindow();
                    else
                       itemDiscountText.requestFocusInWindow();
                }
            });
            itemQtyText1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    itemDiscountText.requestFocusInWindow();
                }
            });
            itemDiscountText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addItemButton.requestFocusInWindow();
                }
            });
            addItemButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    itemCodeText.requestFocusInWindow();
                }
            });
          }
          }
         });
           taxText.addKeyListener(new KeyAdapter() {
                           

            @Override
            public synchronized void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_CONTROL||e.getKeyCode()==KeyEvent.VK_SPACE)
                    pressed.add(e.getKeyChar());
                if (pressed.size() ==2) {
                    chargesTextBox.requestFocusInWindow();
                    pressed.clear();
                }   
               
           // public void keyPressed(KeyEvent e) {
          // int key = e.getKeyCode();
         //       else if (key == KeyEvent.VK_SHIFT) {
         //      chargesTextBox.requestFocusInWindow();
          // }
           else if(e.getKeyCode()==KeyEvent.VK_ENTER)
           { 
               taxText.requestFocusInWindow();
           }
         }
         });
            taxText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    taxAddButton.requestFocusInWindow();
                }
            });
            taxAddButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    taxText.setText("");
                    taxText.requestFocusInWindow();
                }
            });
            chargesTextBox.addKeyListener(new KeyAdapter() {
                @Override
            public synchronized void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_CONTROL||e.getKeyCode()==KeyEvent.VK_SPACE)
                    pressed.add(e.getKeyChar());
                if (pressed.size() ==2) {
                    discountText.requestFocusInWindow();
                    pressed.clear();
                }   
         //   public void keyPressed(KeyEvent e) {
         //  int key = e.getKeyCode();
         // if (key == KeyEvent.VK_SHIFT) {
               //shipmentModeText.requestFocusInWindow();
         //      discountText.requestFocusInWindow();
         //  }
           else if(e.getKeyCode()==KeyEvent.VK_ENTER)
           { 
               chargesText.requestFocusInWindow();
           }
         }
         });
            chargesTextBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chargesText.requestFocusInWindow();
                }
            });
            chargesText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(isDuplicateColumnsVisible==true)
                       chargesText1.requestFocus();
                    else
                        chargesAddButton.requestFocusInWindow();
                }
            });
            chargesText1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chargesAddButton.requestFocusInWindow();
                }
            });
            chargesAddButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chargesTextBox.requestFocusInWindow();
                }
            });
//            shipmentModeText.addActionListener(new ActionListener() {
//                @Override
//                public void actionPerformed(ActionEvent e) {
//                    discountText.requestFocusInWindow();
//                }
//            });
            discountText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(isDuplicateColumnsVisible==true)
                        discountText1.requestFocus();
                    else
                      totDiscountAddButton.requestFocusInWindow();
                }
            });
            discountText1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    totDiscountAddButton.requestFocusInWindow();
                }
            });
            totDiscountAddButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveButton.requestFocusInWindow();
                }
            });
            saveButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    receivedCashText.requestFocusInWindow();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bottomPanel = new javax.swing.JPanel();
        itemInfoPanel = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        itemDescriptionText = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        itemCodeText = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        totAvalQtyLabel = new javax.swing.JLabel();
        avalQtytLabel = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        itemPriceText = new javax.swing.JTextField();
        stockGroupText = new javax.swing.JTextField();
        itemGodownText = new javax.swing.JTextField();
        itemDiscountText = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        itemButtonPanel = new javax.swing.JPanel();
        addItemButton = new javax.swing.JButton();
        itemRemoveButton = new javax.swing.JButton();
        qtyPanel = new javax.swing.JPanel();
        itemQtyText = new javax.swing.JTextField();
        itemQtyText1 = new javax.swing.JTextField();
        chargesPanel = new javax.swing.JPanel();
        chargesAddButton = new javax.swing.JButton();
        chargesMinusButton = new javax.swing.JButton();
        chargesPanel1 = new javax.swing.JPanel();
        chargesText = new javax.swing.JFormattedTextField();
        chargesText1 = new javax.swing.JFormattedTextField();
        chargesTextBox = new javax.swing.JTextField();
        InvoiceDetailsPanel = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        deliveryLabel = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        addressTextArea = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        customerText = new javax.swing.JTextField();
        invoiceNumberText = new javax.swing.JTextField();
        billDateFormattedText = new com.toedter.calendar.JDateChooser();
        deliveryText = new javax.swing.JTextField();
        taxPanel = new javax.swing.JPanel();
        taxAddButton = new javax.swing.JButton();
        taxText = new javax.swing.JTextField();
        shipmentModePanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        shipmentModeText = new javax.swing.JTextField();
        buttonPanel = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        printButton = new javax.swing.JButton();
        pdfButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        grandTotalPanel = new javax.swing.JPanel();
        netTotalLabel = new javax.swing.JLabel();
        netQtyLabel = new javax.swing.JLabel();
        availableQtyLabel2 = new javax.swing.JLabel();
        totDiscountAddButton = new javax.swing.JButton();
        grossAmountLabel = new javax.swing.JLabel();
        grandAmntLabel = new javax.swing.JLabel();
        discountPanel1 = new javax.swing.JPanel();
        discountText = new javax.swing.JFormattedTextField();
        discountText1 = new javax.swing.JFormattedTextField();
        receivedCashLabel = new javax.swing.JLabel();
        balanceAmtLabel = new javax.swing.JLabel();
        receivedCashText = new javax.swing.JTextField();
        balanceLabel1 = new javax.swing.JLabel();
        tableBorderPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        salesItemTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        bottomPanel.setOpaque(false);

        itemInfoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Item Info"));
        itemInfoPanel.setOpaque(false);
        itemInfoPanel.setPreferredSize(new java.awt.Dimension(382, 179));

        jLabel9.setText("Quantity");

        itemDescriptionText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemDescriptionTextKeyPressed(evt);
            }
        });

        jLabel7.setText("Item Code");

        itemCodeText.setBackground(new java.awt.Color(226, 247, 255));
        itemCodeText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemCodeTextActionPerformed(evt);
            }
        });
        itemCodeText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemCodeTextKeyPressed(evt);
            }
        });

        jLabel8.setText("Description");

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel6.setOpaque(false);
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Quantity");
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel6.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 90, 20));

        jLabel4.setText("Available Quantity:");
        jLabel4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel6.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 100, 20));

        totAvalQtyLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jPanel6.add(totAvalQtyLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 90, 30));

        avalQtytLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        avalQtytLabel.setText("0");
        avalQtytLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel6.add(avalQtytLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 90, 20));

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel15.setText("Total Available");
        jLabel15.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel6.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 90, 20));

        itemPriceText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemPriceText.setText("0");
        itemPriceText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemPriceTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemPriceTextFocusLost(evt);
            }
        });
        itemPriceText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemPriceTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemPriceTextKeyTyped(evt);
            }
        });

        stockGroupText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        stockGroupText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                stockGroupTextFocusGained(evt);
            }
        });
        stockGroupText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                stockGroupTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                stockGroupTextKeyTyped(evt);
            }
        });

        itemGodownText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemGodownText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemGodownTextActionPerformed(evt);
            }
        });
        itemGodownText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemGodownTextFocusGained(evt);
            }
        });
        itemGodownText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemGodownTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemGodownTextKeyTyped(evt);
            }
        });

        itemDiscountText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemDiscountText.setText("0");
        itemDiscountText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemDiscountTextActionPerformed(evt);
            }
        });
        itemDiscountText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemDiscountTextFocusGained(evt);
            }
        });
        itemDiscountText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemDiscountTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemDiscountTextKeyTyped(evt);
            }
        });

        jLabel10.setText("Unit Price");

        jLabel11.setText("StockGroup");

        jLabel12.setText("Godown");

        jLabel17.setText("Discount");

        itemButtonPanel.setOpaque(false);
        itemButtonPanel.setLayout(new java.awt.GridLayout(1, 0));

        addItemButton.setText("+");
        addItemButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addItemButtonActionPerformed(evt);
            }
        });
        itemButtonPanel.add(addItemButton);

        itemRemoveButton.setText("-");
        itemRemoveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemRemoveButtonActionPerformed(evt);
            }
        });
        itemButtonPanel.add(itemRemoveButton);

        qtyPanel.setLayout(new java.awt.GridLayout(1, 0));

        itemQtyText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemQtyText.setText("0");
        itemQtyText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemQtyTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemQtyTextFocusLost(evt);
            }
        });
        itemQtyText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemQtyTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemQtyTextKeyTyped(evt);
            }
        });
        qtyPanel.add(itemQtyText);

        itemQtyText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemQtyText1.setText("0");
        itemQtyText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemQtyText1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemQtyText1FocusLost(evt);
            }
        });
        itemQtyText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemQtyText1KeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemQtyText1KeyTyped(evt);
            }
        });
        qtyPanel.add(itemQtyText1);

        javax.swing.GroupLayout itemInfoPanelLayout = new javax.swing.GroupLayout(itemInfoPanel);
        itemInfoPanel.setLayout(itemInfoPanelLayout);
        itemInfoPanelLayout.setHorizontalGroup(
            itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, itemInfoPanelLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(itemDiscountText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE)
                    .addComponent(itemGodownText)
                    .addComponent(stockGroupText)
                    .addComponent(itemPriceText)
                    .addComponent(itemDescriptionText)
                    .addComponent(itemCodeText)
                    .addComponent(qtyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(itemButtonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        itemInfoPanelLayout.setVerticalGroup(
            itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(itemInfoPanelLayout.createSequentialGroup()
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(itemCodeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addGap(10, 10, 10)
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(itemDescriptionText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addGap(10, 10, 10)
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(qtyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(itemPriceText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(stockGroupText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(itemGodownText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(itemDiscountText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17)))
                    .addGroup(itemInfoPanelLayout.createSequentialGroup()
                        .addComponent(itemButtonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        chargesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Charges"));
        chargesPanel.setOpaque(false);

        chargesAddButton.setText("+");
        chargesAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesAddButtonActionPerformed(evt);
            }
        });

        chargesMinusButton.setText("-");
        chargesMinusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesMinusButtonActionPerformed(evt);
            }
        });

        chargesPanel1.setLayout(new java.awt.GridLayout(1, 0));

        chargesText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        chargesText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        chargesText.setText("0.00");
        chargesText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                chargesTextFocusLost(evt);
            }
        });
        chargesText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                chargesTextKeyTyped(evt);
            }
        });
        chargesPanel1.add(chargesText);

        chargesText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        chargesText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        chargesText1.setText("0.00");
        chargesText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesText1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                chargesText1FocusLost(evt);
            }
        });
        chargesText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                chargesText1KeyTyped(evt);
            }
        });
        chargesPanel1.add(chargesText1);

        chargesTextBox.setBackground(new java.awt.Color(226, 247, 255));
        chargesTextBox.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesTextBoxFocusGained(evt);
            }
        });
        chargesTextBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                chargesTextBoxKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout chargesPanelLayout = new javax.swing.GroupLayout(chargesPanel);
        chargesPanel.setLayout(chargesPanelLayout);
        chargesPanelLayout.setHorizontalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, chargesPanelLayout.createSequentialGroup()
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(chargesPanelLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(chargesPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE))
                    .addComponent(chargesTextBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        chargesPanelLayout.setVerticalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chargesPanelLayout.createSequentialGroup()
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chargesTextBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(chargesPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        InvoiceDetailsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Invoice Details"));
        InvoiceDetailsPanel.setOpaque(false);

        jLabel6.setText("Bill Date*");

        jLabel5.setText("Invoice Number*");

        jLabel13.setText("Customer*");

        deliveryLabel.setText("Delivery");

        addressTextArea.setColumns(20);
        addressTextArea.setLineWrap(true);
        addressTextArea.setRows(5);
        jScrollPane3.setViewportView(addressTextArea);

        jLabel1.setText("Address :");

        customerText.setBackground(new java.awt.Color(226, 247, 255));
        customerText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customerTextActionPerformed(evt);
            }
        });
        customerText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                customerTextKeyPressed(evt);
            }
        });

        invoiceNumberText.setEditable(false);
        invoiceNumberText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                invoiceNumberTextActionPerformed(evt);
            }
        });
        invoiceNumberText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                invoiceNumberTextKeyPressed(evt);
            }
        });

        billDateFormattedText.setDateFormatString("dd/MM/yyyy");

        javax.swing.GroupLayout InvoiceDetailsPanelLayout = new javax.swing.GroupLayout(InvoiceDetailsPanel);
        InvoiceDetailsPanel.setLayout(InvoiceDetailsPanelLayout);
        InvoiceDetailsPanelLayout.setHorizontalGroup(
            InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(deliveryLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(billDateFormattedText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(invoiceNumberText)
                    .addComponent(customerText)
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addComponent(deliveryText)
                        .addGap(1, 1, 1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        InvoiceDetailsPanelLayout.setVerticalGroup(
            InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(billDateFormattedText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addGap(7, 7, 7)
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addGap(41, 41, 41)
                                .addComponent(customerText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(deliveryLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(deliveryText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(invoiceNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel13))))
                    .addComponent(jLabel6)
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(34, 34, 34))
        );

        taxPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Tax"));
        taxPanel.setOpaque(false);

        taxAddButton.setText("+");
        taxAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                taxAddButtonActionPerformed(evt);
            }
        });

        taxText.setBackground(new java.awt.Color(226, 247, 255));
        taxText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                taxTextFocusGained(evt);
            }
        });
        taxText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                taxTextKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout taxPanelLayout = new javax.swing.GroupLayout(taxPanel);
        taxPanel.setLayout(taxPanelLayout);
        taxPanelLayout.setHorizontalGroup(
            taxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(taxPanelLayout.createSequentialGroup()
                .addComponent(taxText)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(taxAddButton)
                .addContainerGap())
        );
        taxPanelLayout.setVerticalGroup(
            taxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(taxPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(taxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(taxAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(taxText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        shipmentModePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        shipmentModePanel.setOpaque(false);

        jLabel3.setText("Shipment Mode");

        shipmentModeText.setBackground(new java.awt.Color(226, 247, 255));
        shipmentModeText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                shipmentModeTextKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout shipmentModePanelLayout = new javax.swing.GroupLayout(shipmentModePanel);
        shipmentModePanel.setLayout(shipmentModePanelLayout);
        shipmentModePanelLayout.setHorizontalGroup(
            shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, shipmentModePanelLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(shipmentModeText)
                .addContainerGap())
        );
        shipmentModePanelLayout.setVerticalGroup(
            shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(shipmentModePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(shipmentModeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        buttonPanel.setLayout(new java.awt.GridLayout(1, 0));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("<html><u>N</u>ew</html>");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(newButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveButton.setText("<HTML><U>S</U>ave<HTML>");
        saveButton.setToolTipText("");
        saveButton.setOpaque(false);
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(saveButton);

        printButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/print.png"))); // NOI18N
        printButton.setText("<HTML><U>P</U>rint<HTML>");
        printButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(printButton);

        pdfButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/pdf-icon.png"))); // NOI18N
        pdfButton.setText(" PDF");
        pdfButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdfButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(pdfButton);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/cancel.png"))); // NOI18N
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(cancelButton);

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/delete-icon.png"))); // NOI18N
        deleteButton.setText("<html><u>D</u>elete</html>");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(deleteButton);

        grandTotalPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Grand Total", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N
        grandTotalPanel.setOpaque(false);

        netTotalLabel.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        netTotalLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        netTotalLabel.setText("0.00");
        netTotalLabel.setFocusable(false);
        netTotalLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        netQtyLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        netQtyLabel.setText("Net Amount");
        netQtyLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        availableQtyLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        availableQtyLabel2.setText("Discount");
        availableQtyLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        totDiscountAddButton.setText("+");
        totDiscountAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totDiscountAddButtonActionPerformed(evt);
            }
        });

        grossAmountLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        grossAmountLabel.setText("Grand Total:");
        grossAmountLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        grandAmntLabel.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        grandAmntLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        grandAmntLabel.setText("0.00");

        discountPanel1.setLayout(new java.awt.GridLayout(1, 0));

        discountText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        discountText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        discountText.setText("0.00");
        discountText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                discountTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                discountTextFocusLost(evt);
            }
        });
        discountText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                discountTextKeyTyped(evt);
            }
        });
        discountPanel1.add(discountText);

        discountText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        discountText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        discountText1.setText("0.00");
        discountText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                discountText1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                discountText1FocusLost(evt);
            }
        });
        discountText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                discountText1KeyTyped(evt);
            }
        });
        discountPanel1.add(discountText1);

        receivedCashLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        receivedCashLabel.setText("Received Cash");

        balanceAmtLabel.setBackground(new java.awt.Color(255, 255, 0));
        balanceAmtLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        balanceAmtLabel.setText("0.00");

        receivedCashText.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        receivedCashText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                receivedCashTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                receivedCashTextKeyTyped(evt);
            }
        });

        balanceLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        balanceLabel1.setText("Balance :");

        javax.swing.GroupLayout grandTotalPanelLayout = new javax.swing.GroupLayout(grandTotalPanel);
        grandTotalPanel.setLayout(grandTotalPanelLayout);
        grandTotalPanelLayout.setHorizontalGroup(
            grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(grandTotalPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(grandTotalPanelLayout.createSequentialGroup()
                        .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(availableQtyLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(netQtyLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(totDiscountAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(grandTotalPanelLayout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(netTotalLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(discountPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)))
                    .addGroup(grandTotalPanelLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(grandTotalPanelLayout.createSequentialGroup()
                                .addComponent(grossAmountLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(grandAmntLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE))
                            .addGroup(grandTotalPanelLayout.createSequentialGroup()
                                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(receivedCashLabel)
                                    .addComponent(balanceLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(receivedCashText)
                                    .addComponent(balanceAmtLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addContainerGap())))
        );
        grandTotalPanelLayout.setVerticalGroup(
            grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(grandTotalPanelLayout.createSequentialGroup()
                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(netTotalLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(netQtyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(availableQtyLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(discountPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(totDiscountAddButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(receivedCashLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(receivedCashText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(grandTotalPanelLayout.createSequentialGroup()
                        .addComponent(balanceAmtLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(grandAmntLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(grossAmountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(balanceLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout bottomPanelLayout = new javax.swing.GroupLayout(bottomPanel);
        bottomPanel.setLayout(bottomPanelLayout);
        bottomPanelLayout.setHorizontalGroup(
            bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bottomPanelLayout.createSequentialGroup()
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(InvoiceDetailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(itemInfoPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(taxPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chargesPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(shipmentModePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(grandTotalPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        bottomPanelLayout.setVerticalGroup(
            bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bottomPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(itemInfoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(bottomPanelLayout.createSequentialGroup()
                        .addComponent(InvoiceDetailsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(grandTotalPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(bottomPanelLayout.createSequentialGroup()
                        .addComponent(taxPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chargesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(shipmentModePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(1, 1, 1))
        );

        add(bottomPanel, java.awt.BorderLayout.PAGE_END);

        tableBorderPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Sales"));
        tableBorderPanel.setOpaque(false);

        jScrollPane1.setName(""); // NOI18N
        jScrollPane1.setOpaque(false);

        salesItemTable.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        salesItemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        salesItemTable.setOpaque(false);
        salesItemTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                salesItemTableMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                salesItemTableMouseEntered(evt);
            }
        });
        jScrollPane1.setViewportView(salesItemTable);

        javax.swing.GroupLayout tableBorderPanelLayout = new javax.swing.GroupLayout(tableBorderPanel);
        tableBorderPanel.setLayout(tableBorderPanelLayout);
        tableBorderPanelLayout.setHorizontalGroup(
            tableBorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tableBorderPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1178, Short.MAX_VALUE))
        );
        tableBorderPanelLayout.setVerticalGroup(
            tableBorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tableBorderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
                .addGap(4, 4, 4))
        );

        add(tableBorderPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    void fillSalesItemTable() {
        try {

            salesItemTableModel.setRowCount(0);
            //BigDecimal grandTotal = new BigDecimal(0.00) ;
            //grandTotal.setScale(1);
            subTotal = 0.00;
            netTotal = 0.00;
            subTotal1 = 0.00;
            netTotal1 = 0.00;

            int slNo = 1;
            String currencySymbol = (null == currency) ? "" : currency.getSymbol();

            //{"Sr. No.", "Item Code","Description", "Unit Price", "Qty.", "Total Amount"}
            for (SalesItem pItem : salesItemList) {
                double itemTotal = 0.00;
                double itemTotal1 = 0.00;
                if (sales == null) {
                    itemTotal = (((pItem.getUnitPrice() / currency.getRate()) - (pItem.getDiscount())) * pItem.getQuantity());
                    itemTotal1 = (((pItem.getUnitPrice() / currency.getRate()) - (pItem.getDiscount())) * pItem.getQuantity1());
                    if (isDuplicateColumnsVisible) {
                        
                        if (itemTotal > 0 || itemTotal1 > 0) {
                            salesItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / currency.getRate()), (pItem.getQuantity() + pItem.getQuantity1()), commonService.formatIntoCurrencyAsString(pItem.getDiscount()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal + itemTotal1)});
                        }
                    } else {
                        if (itemTotal > 0) {
                            salesItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / currency.getRate()), pItem.getQuantity(), commonService.formatIntoCurrencyAsString(pItem.getDiscount()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal)});
                        }
                    }
                } else {
                    itemTotal = (((pItem.getUnitPrice() / currency.getRate()) - (pItem.getDiscount() / currency.getRate())) * pItem.getQuantity());
                    itemTotal1 = ((pItem.getUnitPrice() / currency.getRate()) * pItem.getQuantity1());
                    if (isDuplicateColumnsVisible) {
                        
                        if (itemTotal1 > 0 || itemTotal > 0) {
                            salesItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / currency.getRate()), (pItem.getQuantity()+pItem.getQuantity1()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getDiscount() / currency.getRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal + itemTotal1)});
                        }
                    } else {
                        if(itemTotal>0)
                        {
                         salesItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / currency.getRate()), pItem.getQuantity(), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getDiscount() / currency.getRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal)});   
                        }
                    }
                }
                subTotal += itemTotal;
                netTotal = subTotal;
                
                subTotal1 += itemTotal1;
                netTotal1 = subTotal1;
            }

            if (netTotal > 0) {
                for (SalesTax salesTax : salesTaxList) {

                    //double taxTotal = subTotal * (purchaseTax.getTax().getTaxRate() / 100);
                    //if (sales == null) {
                    salesItemTableModel.addRow(new Object[]{"", "", "\t" + salesTax.getTax().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(salesTax.getAmount() / currency.getRate())});
                    netTotal += (salesTax.getAmount());
//                    } else {
//                        salesItemTableModel.addRow(new Object[]{"", "", "\t" + salesTax.getTax().getName(), "", "", "", sales.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString(salesTax.getAmount() / sales.getCurrentCurrencyRate())});
//                        netTotal += (salesTax.getAmount() / sales.getCurrentCurrencyRate());
//                    }
                }

                for (SalesCharge salesCharge : salesChargesList) {
                    //double chargesTotal = Double.parseDouble(chargesText.getText());

                    //if (sales == null) {
                    if (isDuplicateColumnsVisible) {
                        salesItemTableModel.addRow(new Object[]{"", "", "\t" + salesCharge.getCharges().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString((salesCharge.getAmount()+salesCharge.getAmount1())/currency.getRate())});
                    } else {
                        salesItemTableModel.addRow(new Object[]{"", "", "\t" + salesCharge.getCharges().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(salesCharge.getAmount()/currency.getRate())});
                    }
                    
                    netTotal += (salesCharge.getAmount());
                     netTotal1 += (salesCharge.getAmount1());
//                    } else {
//                        salesItemTableModel.addRow(new Object[]{"", "", "\t" + salesCharge.getCharges().getName(), "", "", "", sales.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString(salesCharge.getAmount() / sales.getCurrentCurrencyRate())});
//                        netTotal += (salesCharge.getAmount() / sales.getCurrentCurrencyRate());
//                    }
                }
            }
            grandTotal = netTotal - totDiscount;
            grandTotal1 = netTotal1-totDiscount1;
            if (totDiscount > 0 || totDiscount1 > 0) {
                if (isDuplicateColumnsVisible) {
                    salesItemTableModel.addRow(new Object[]{"", "", "Discount", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString((totDiscount+totDiscount1)/currency.getRate())});
                } else {
                    salesItemTableModel.addRow(new Object[]{"", "", "Discount", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(totDiscount/currency.getRate())});
                }
            }
            if (isDuplicateColumnsVisible) {
              netTotalLabel.setText(currencySymbol + " " + commonService.formatIntoCurrencyAsString(netTotal+netTotal1));
            grandAmntLabel.setText(currencySymbol + "" + commonService.formatIntoCurrencyAsString(grandTotal+grandTotal1));
  
            }
            else
            {
             netTotalLabel.setText(currencySymbol + " " + commonService.formatIntoCurrencyAsString(netTotal));
            grandAmntLabel.setText(currencySymbol + "" + commonService.formatIntoCurrencyAsString(grandTotal));

            }
            
            //clear fields
            CommonService.clearNumberFields(new JTextField[]{itemQtyText, itemDiscountText,  itemPriceText});
            CommonService.clearTextFields(new JTextField[]{itemCodeText, itemDescriptionText, itemGodownText});
            avalQtytLabel.setText("");
            totAvalQtyLabel.setText("");

        } catch (Exception e) {
            log.error("fillSalesItemTable:", e);
        }

    }

    public void listSalesItemHistory(Sales sales) {
        this.sales = sales;
        //System.out.println("lllllllllllll"+sales.getPurchaseItems().size());
        // salesItemList = sales.getSalesItems();

        salesItemList = sales.getSalesItems();
        salesItemListHistory = new ArrayList<SalesItem>();
        
        for (SalesItem salesItem : salesItemList) {
            //copy sales items 
            SalesItem salesItem1=new SalesItem();
            salesItem1.setItem(salesItem.getItem());
            salesItem1.setQuantity(salesItem.getQuantity());
            salesItem1.setQuantity1(salesItem.getQuantity1());
            salesItemListHistory.add(salesItem1);
            //copy sales items 
            
            Item item = salesItem.getItem();
            item.setAvailableQty(item.getAvailableQty() + salesItem.getQuantity());
            item.setAvailableQty1(item.getAvailableQty1() + salesItem.getQuantity1());
            salesLedger.setAvailableBalance(salesLedger.getAvailableBalance() - (salesItem.getUnitPrice() * salesItem.getQuantity()));
            salesLedger.setAvailableBalance1(salesLedger.getAvailableBalance1() - (salesItem.getUnitPrice() * salesItem.getQuantity1()));
            salesItem.setUnitPrice(salesItem.getUnitPrice());
        }

        salesTaxList = sales.getSalesTaxs();
        for (SalesTax salesTax : salesTaxList) {
            ledger = ledgerDAO.findByLedgerName(salesTax.getTax().getName());
            ledger.setAvailableBalance(ledger.getAvailableBalance() - salesTax.getAmount());
            taxLedgerList.add(ledger);
            salesTax.setAmount(salesTax.getAmount() / sales.getCurrentCurrencyRate());
        }
        salesChargesList = sales.getSalesCharges();
        for (SalesCharge salesCharge : salesChargesList) {
            ledger = ledgerDAO.findByLedgerName(salesCharge.getCharges().getName());
            ledger.setAvailableBalance(ledger.getAvailableBalance() - salesCharge.getAmount());
            ledger.setAvailableBalance1(ledger.getAvailableBalance1() - salesCharge.getAmount1());
            chargesLedgerList.add(ledger);
            salesCharge.setAmount(salesCharge.getAmount() / sales.getCurrentCurrencyRate());
            salesCharge.setAmount1(salesCharge.getAmount1() / sales.getCurrentCurrencyRate());
        }

        //Updating availableQty of Customer Ledger
        this.customerLedger = sales.getCustomer();
        if (customerLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
            customerLedger.setAvailableBalance(customerLedger.getAvailableBalance() - sales.getGrandTotal());
            customerLedger.setAvailableBalance1(customerLedger.getAvailableBalance1() - sales.getGrandTotal1());
        } else if (customerLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
            customerLedger.setAvailableBalance(customerLedger.getAvailableBalance() + sales.getGrandTotal());
            customerLedger.setAvailableBalance1(customerLedger.getAvailableBalance1() + sales.getGrandTotal1());
        }

        this.customerLedger = sales.getCustomer();
        billDateFormattedText.setDate(sales.getBillDate());
        invoiceNumberText.setText(sales.getInvoiceNumber());
        customerText.setText(sales.getCustomer().getLedgerName());
        //currency=sales.getMultiCurrency();
        deliveryText.setText(sales.getDeliveryDate() + "");
        //paymentTermsText.setText(purchase.());
        addressTextArea.setText(sales.getAddress());

        discountLedger.setAvailableBalance(discountLedger.getAvailableBalance() - sales.getDiscount());
        discountLedger.setAvailableBalance1(discountLedger.getAvailableBalance1()- sales.getDiscount1());

        totDiscount = sales.getDiscount() / sales.getCurrentCurrencyRate();
        totDiscount1 = sales.getDiscount1() / sales.getCurrentCurrencyRate();
        
        saveButton.setText("Update");
        currency = sales.getMultiCurrency();
        currency.setRate(sales.getCurrentCurrencyRate());
        check = 1;

//        int count = multiCurrencyComboBox.getItemCount();
//        for (int i = 0; i < count; i++) {
//            ComboKeyValue ckv = (ComboKeyValue) multiCurrencyComboBox.getItemAt(i);
//            if (ckv.getKey().trim().equalsIgnoreCase(sales.getMultiCurrency().getName())) {
//                multiCurrencyComboBox.setSelectedItem(ckv);
//            }
//        }
        //currencyText.setText(sales.getMultiCurrency().getName());
        if (sales.getShipmentMode() != null) {
            shipmentModeText.setText(sales.getShipmentMode().getName());
        }
//        int count1 = shipmentComboBox.getItemCount();
//        if (sales.getShipmentMode() != null) {
//            for (int i = 0; i < count1; i++) {
//                ComboKeyValue ckv = (ComboKeyValue) shipmentComboBox.getItemAt(i);
//                if (ckv.getKey().trim().equalsIgnoreCase(sales.getShipmentMode().getName())) {
//                    shipmentComboBox.setSelectedItem(ckv);
//                }
//            }
//        }
        fillSalesItemTable();
    }

    private void salesItemTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salesItemTableMouseClicked
        try {
            ArrayList row = CommonService.getTableRowData(salesItemTable);
            String itemCode = row.get(1) + "";

            if (itemCode.trim().length() > 0) {
                salesItem = salesItemList.get(Integer.parseInt(row.get(0) + "") - 1);
                item = salesItem.getItem();
                itemCodeText.setText(salesItem.getItem().getItemCode());
                itemDescriptionText.setText(salesItem.getItem().getDescription());
                itemQtyText.setText(salesItem.getQuantity() + "");
                itemQtyText1.setText(salesItem.getQuantity1() + "");
                itemDiscountText.setText(salesItem.getDiscount() + "");
                stockGroupText.setText(salesItem.getItem().getStockGroup().getName());

                if (sales == null) {
                    itemPriceText.setText(salesItem.getUnitPrice() / currency.getRate() + "");
                } else {
                    itemPriceText.setText(salesItem.getUnitPrice() / sales.getCurrentCurrencyRate() + "");
                }

                itemGodownText.setText(salesItem.getItem().getGodown().getName());
                //avalQtytLabel.setText(salesItem.getItem().getAvailableQty() + "");
                //showing total available qty from all godown
                if (isDuplicateColumnsVisible) {
                    itemQty = itemDAO.findTotItemQty1(salesItem.getItem().getItemCode());
                    avalQtytLabel.setText(Double.toString(salesItem.getItem().getAvailableQty() + salesItem.getItem().getAvailableQty1()));
                } else {
                    itemQty = itemDAO.findTotItemQty(salesItem.getItem().getItemCode());
                    avalQtytLabel.setText(Double.toString(salesItem.getItem().getAvailableQty()));
                }
                totAvalQtyLabel.setText(itemQty + "");
                //showing total available qty 

            } else {
              /*  String itemDesc = row.get(2) + "";
                itemDesc = itemDesc.trim();
                int count = taxComboBox.getItemCount();

                if (itemDesc.equalsIgnoreCase("discount")) {
                    totDiscountAddButton.setText("-");
                }

                for (int i = 0; i < count; i++) {
                    ComboKeyValue ckv = (ComboKeyValue) taxComboBox.getItemAt(i);
                    if (ckv.getKey().trim().equalsIgnoreCase(itemDesc)) {
                        taxComboBox.setSelectedItem(ckv);
                        taxAddButton.setText("-");
                    }
                }

                count = chargesComboBox.getItemCount();
                for (int i = 0; i < count; i++) {
                    ComboKeyValue ckv = (ComboKeyValue) chargesComboBox.getItemAt(i);
                    if (ckv.getKey().trim().equalsIgnoreCase(itemDesc)) {
                        chargesComboBox.setSelectedItem(ckv);
                        for (SalesCharge salesCharge : salesChargesList) {
                            if (salesCharge.getCharges().getName().equalsIgnoreCase(itemDesc)) {
                                chargesText.setText(salesCharge.getAmount() + "");
                                chargesText1.setText(salesCharge.getAmount1() + "");
                                break;
                            }
                        }
                    }
                }*/
                 String itemDesc = row.get(2) + "";
                 itemDesc = itemDesc.trim();
                 ItemService itemService = new ItemService();
                 PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
                 if (itemDesc.equalsIgnoreCase("discount")) {
                    totDiscountAddButton.setText("-");
                 }
                if(taxList!=null)
                 {
                     int i=0;
                     Iterator<Tax> iter=taxList.iterator();
                     while(iter.hasNext()&& i<=(taxCount-1))
                     {
                         tax=taxList.get(i);
                         //System.out.println(""+tax.getName());
                         if(tax.getName().equalsIgnoreCase(itemDesc))
                         {
                             taxText.setText(tax.getName());
                             taxAddButton.setText("-");
                             break;
                         }
                         i++;
                     }
                 } 
                if(salesTaxList!=null)
                 {
                     int i=0;
                     Iterator<SalesTax> iter=salesTaxList.iterator();
                     while(iter.hasNext()&& i<=(salesTaxList.size()-1))
                     {
                         saleTax=salesTaxList.get(i);
                         //System.out.println(""+tax.getName());
                         if(saleTax.getTax().getName().equalsIgnoreCase(itemDesc))
                         {
                             taxText.setText(saleTax.getTax().getName());
                             taxAddButton.setText("-");
                             break;
                         }
                         i++;
                     }
                 } 
                 
                 if(chargesList!=null)
                 {
               
                 int j=0;
                 Iterator<Charges> ite=chargesList.iterator();
                 while(ite.hasNext()&& j<=1)
                 {
                    charges=chargesList.get(j);
                   
                     if(charges.getName().equalsIgnoreCase(itemDesc))
                     {
                       chargesTextBox.setText(charges.getName());
                       chargesText.setValue(chargesAmt);  
                       break;
                     }
                     j++;
                 }
                 }
                 
                  if(salesChargesList!=null)
                 {
                     
               
                 int j=0;
                 Iterator<SalesCharge> ite=salesChargesList.iterator();
                 while(ite.hasNext() && j<=salesChargesList.size()-1)
                 {
                    saleCharge=salesChargesList.get(j);
                    // System.out.println(""+charges.getName());
                     if(saleCharge.getCharges().getName().equalsIgnoreCase(itemDesc))
                     {
                        chargesTextBox.setText(saleCharge.getCharges().getName());
                       chargesText.setText(saleCharge.getAmount().toString());
                       chargesText1.setValue(saleCharge.getAmount1());  
                     //  chargesText.setText("");
                       break;
                     }
                     j++;
                 }
                 }
            }

            discountText.setText(totDiscount+"");
            discountText1.setText(totDiscount1+"");
            
        } catch (Exception e) {
            log.error("saleItemTableMouseClicked:", e);
        }
    }//GEN-LAST:event_salesItemTableMouseClicked

    private void salesItemTableMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salesItemTableMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_salesItemTableMouseEntered

    void saveOrUpdateSales() {
        try {
            if (sales == null) {
                sales = new Sales();
                check = 0;
            }
            sales.setBillDate(commonService.utilDateToSqlDate(billDateFormattedText.getDate()));
            sales.setInvoiceNumber(invoiceNumberText.getText());
            sales.setCustomer(customerLedger);
            sales.setAddress(addressTextArea.getText().trim());
            sales.setDeliveryDate(deliveryText.getText());
            sales.setMultiCurrency(currency);
//            Object shipmentMod = shipmentComboBox.getSelectedItem();
//            ShipmentMode shipmentMode = (ShipmentMode) ((ComboKeyValue) shipmentMod).getValue();
            if (shipmentModeText.getText().equalsIgnoreCase("N/A")) {
                sales.setShipmentMode(null);
            } else {
                sales.setShipmentMode(shipmentMode);
            }
            sales.setDiscount(totDiscount * currency.getRate());
            sales.setDiscount1(totDiscount1 * currency.getRate());
            for (SalesItem salesItem : salesItemList) {
                salesItem.setUnitPrice(salesItem.getUnitPrice());
                salesItem.setDiscount(salesItem.getDiscount() * currency.getRate());
            }
            sales.setSalesItems(salesItemList);
            for (SalesTax salesTax : salesTaxList) {
                salesTax.setAmount(salesTax.getAmount() * currency.getRate());
            }

            sales.setSalesTaxs(salesTaxList);
            for (SalesCharge salesCharge : salesChargesList) {
                salesCharge.setAmount(salesCharge.getAmount() * currency.getRate());
                salesCharge.setAmount1(salesCharge.getAmount1() * currency.getRate());
            }
            sales.setSalesCharges(salesChargesList);
            sales.setCurrentCurrencyRate(currency.getRate());
            sales.setGrandTotal(grandTotal * currency.getRate());
            sales.setGrandTotal1(grandTotal1 * currency.getRate());
            sales.setCompany(GlobalProperty.getCompany());
            //if (check == 0) {
            //      cRUDServices.saveModel(sales);
            // } else {
            cRUDServices.saveOrUpdateModel(sales);

            //Updating availableQty of Supplier Ledger
            if (customerLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                customerLedger.setAvailableBalance(customerLedger.getAvailableBalance() + (grandTotal * currency.getRate()));
                customerLedger.setAvailableBalance1(customerLedger.getAvailableBalance1() + (grandTotal1 * currency.getRate()));
                cRUDServices.saveOrUpdateModel(customerLedger);
            } else if (customerLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                customerLedger.setAvailableBalance(customerLedger.getAvailableBalance() - (grandTotal * currency.getRate()));
                customerLedger.setAvailableBalance1(customerLedger.getAvailableBalance1() - (grandTotal1 * currency.getRate()));
                cRUDServices.saveOrUpdateModel(customerLedger);
            }
            // }
            //Deducting available quantity on each item
            
            for (SalesItem pi : salesItemList) {
                Item item = pi.getItem();       
                item.setAvailableQty(item.getAvailableQty() - pi.getQuantity());
                item.setAvailableQty1(item.getAvailableQty1() - pi.getQuantity1());
                cRUDServices.saveOrUpdateModel(item);                
            }
            
            //
            //System.out.println("Removed item list size"+salesItemRemovedList.size());
           // System.out.println(" item list size"+salesItemList.size());
            for (SalesItem pi : salesItemRemovedList) {
                Item item = pi.getItem();               
                item.setAvailableQty(item.getAvailableQty());                
                item.setAvailableQty1(item.getAvailableQty1());
                cRUDServices.saveOrUpdateModel(item);
           }

            //Updating Sales Ledger available balance
            Ledger ledger = ledgerDAO.findByLedgerName("Sales Account");
            ledger.setAvailableBalance(ledger.getAvailableBalance() + subTotal);
            ledger.setAvailableBalance1(ledger.getAvailableBalance1() + subTotal1);
            cRUDServices.saveOrUpdateModel(ledger);
            //Update discount in leadger
            ledger = ledgerDAO.findByLedgerName("Discount Allowed");
            ledger.setAvailableBalance(ledger.getAvailableBalance() + totDiscount);
            ledger.setAvailableBalance1(ledger.getAvailableBalance1() + totDiscount1);
            cRUDServices.saveOrUpdateModel(ledger);

            numberProperty.setNumber(numberProperty.getNumber() + 1);
            cRUDServices.saveOrUpdateModel(numberProperty);

            //Updating Sales Ledger available balance
            salesLedger.setAvailableBalance((salesLedger.getAvailableBalance()) + subTotal);
            salesLedger.setAvailableBalance1((salesLedger.getAvailableBalance1()) + subTotal1);
            cRUDServices.saveOrUpdateModel(salesLedger);

            //Discount Allowed
            discountLedger.setAvailableBalance((discountLedger.getAvailableBalance()) + totDiscount);
            discountLedger.setAvailableBalance1((discountLedger.getAvailableBalance1()) + totDiscount1);
            cRUDServices.saveOrUpdateModel(discountLedger);

            //tax
            for (Ledger ledger1 : taxLedgerList) {
                cRUDServices.saveOrUpdateModel(ledger1);
            }
            for (SalesTax salesTax : salesTaxList) {
                ledger = ledgerDAO.findByLedgerName(salesTax.getTax().getName());
                ledger.setAvailableBalance(ledger.getAvailableBalance() + salesTax.getAmount());
                cRUDServices.saveOrUpdateModel(ledger);
            }

            //charges
            for (Ledger ledger1 : chargesLedgerList) {
                cRUDServices.saveOrUpdateModel(ledger1);
            }
            for (SalesCharge salesCharge : salesChargesList) {
                ledger = ledgerDAO.findByLedgerName(salesCharge.getCharges().getName());
                ledger.setAvailableBalance(ledger.getAvailableBalance() + salesCharge.getAmount());
                ledger.setAvailableBalance1(ledger.getAvailableBalance1() + salesCharge.getAmount1());
                cRUDServices.saveOrUpdateModel(ledger);
            }
            listSalesItemHistory(sales);
        } catch (Exception e) {
            log.error("saveOrUpdateSales:", e);
        }
    }
    void saveInvoice(){
        try {
            if (billDateFormattedText.getDate() != null && customerLedger != null && salesItemList != null && salesItemTable.getRowCount()!=0) {
                if (MsgBox.confirm("Are you sure you want to save?")) {
                    saveOrUpdateSales();
                    MsgBox.success("Sales Invoice Saved Successfully");
                    //clearAllFields();
                }
            } else {
                MsgBox.warning("Required fields are not entered");
            }
            
        } catch (Exception ex) {
            log.error(ex);
        }
    }
    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        SalesReturnDAO salesReturnDAO= new SalesReturnDAO();
        List<SalesReturn> salesReturnList = salesReturnDAO.findAllSalesReturn(sales);
        if(!salesReturnList.isEmpty())
        {
            MsgBox.warning("Cannot update this Sales");
        }
        else
        {
            saveInvoice();
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        CommonService.removeJpanel(mainForm.getContentPanel());
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void taxAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_taxAddButtonActionPerformed
//        Object taxItem = taxComboBox.getSelectedItem();
//        Tax tax = (Tax) ((ComboKeyValue) taxItem).getValue();
//
//        if (tax.getTaxRate() > 0 && netTotal > 0 && !tax.getName().equalsIgnoreCase("N/A")) {
//            if (taxAddButton.getText().trim().equalsIgnoreCase("+")) {
//                SalesTax salesTax = new SalesTax();
//
//                Iterator<SalesTax> iter = salesTaxList.iterator();
//                while (iter.hasNext()) {
//                    if (tax.getName().equalsIgnoreCase(iter.next().getTax().getName())) {
//                        iter.remove();
//                    }
//                }
//                salesTax.setTax(tax);
//                salesTax.setTaxRate(tax.getTaxRate());
//                double taxTotal = (netTotal * tax.getTaxRate()) / 100;
//                salesTax.setAmount(taxTotal);
//                salesTaxList.add(salesTax);
//                taxComboBox.setSelectedIndex(0);
//            } else {
//                Iterator<SalesTax> iter = salesTaxList.iterator();
//                while (iter.hasNext()) {
//                    if (tax.getName().equalsIgnoreCase(iter.next().getTax().getName())) {
//                        iter.remove();
//                    }
//                }
//                taxAddButton.setText("+");
//            }
//
//            fillSalesItemTable();
//        }
//        taxComboBox.setSelectedIndex(0);
        if(tax!=null)
       {
        if (tax.getTaxRate() > 0 && subTotal > 0 && !tax.getName().equalsIgnoreCase("N/A")) {
            if (taxAddButton.getText().trim().equalsIgnoreCase("+")) {
                SalesTax salesTax = new SalesTax();
                Iterator<SalesTax> iter = salesTaxList.iterator();
                while (iter.hasNext()) {
                    if (tax.getName().equalsIgnoreCase(iter.next().getTax().getName())) {
                        iter.remove();
                    }
                }
                salesTax.setTax(tax);
                salesTax.setTaxRate(tax.getTaxRate());
                double taxTotal = (subTotal*tax.getTaxRate()) / 100;
                salesTax.setAmount(taxTotal);
                salesTax.setCompany(GlobalProperty.getCompany());
                salesTaxList.add(salesTax);
            } else {
                Iterator<SalesTax> iter = salesTaxList.iterator();
                while (iter.hasNext()) {
                    if (tax.getName().equalsIgnoreCase(iter.next().getTax().getName())) {
                        iter.remove();
                    }
                }
                taxAddButton.setText("+");
            }
            fillSalesItemTable();
        }
        else if(salesItemList.size()==0)
        {
          MsgBox.warning("Sales Item List Empty......!!"); 
        }
       }
        else
        {
           Iterator<SalesTax> iter = salesTaxList.iterator();
            while (iter.hasNext()) {
               if (saleTax.getTax().getName().equalsIgnoreCase(iter.next().getTax().getName())) {
                        iter.remove();
                    }
                }    
            fillSalesItemTable();
        }
    }//GEN-LAST:event_taxAddButtonActionPerformed

    private void customerTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_customerTextKeyPressed
        try {

            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                //String productId = productIdText.getText().trim();
                List<Ledger> customers = ledgerService.populatePopupTableByCustomer(customerText.getText().trim(), popupTableDialog);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();
                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    customerLedger = customers.get(index);
                    customerText.setText(customerLedger.getLedgerName());
                    //
                    customerDetails = "";
                    if (customerLedger.getLedgerName() != null) {
                        if (customerLedger.getLedgerName().trim().length() > 0) {
                            customerDetails += customerLedger.getLedgerName() + "\n";
                        }
                    }
                    if (customerLedger.getAddress() != null) {
                        if (customerLedger.getAddress().trim().length() > 0) {
                            customerDetails += customerLedger.getAddress() + "\n";
                        }
                    }

                    if (customerLedger.getEmail() != null) {
                        if (customerLedger.getEmail().trim().length() > 0) {
                            customerDetails += "Email:" + customerLedger.getEmail() + "\n";
                        }
                    }
                    if (customerLedger.getTelephone() != null) {
                        if (customerLedger.getTelephone().trim().length() > 0) {
                            customerDetails += "Phone:" + customerLedger.getTelephone() + "\n";
                        }
                    }
                    if (customerLedger.getFax() != null) {
                        if (customerLedger.getFax().trim().length() > 0) {
                            customerDetails += "Fax:" + customerLedger.getFax();
                        }
                    }

                    //
                    addressTextArea.setText(customerDetails);
                    //salesTaxText.setText(customerLedger.getSalesTaxNo());
                }
            }

        } catch (Exception e) {
            log.error("productIdTextKeyPressed:", e);
        }
    }//GEN-LAST:event_customerTextKeyPressed

    private void chargesMinusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesMinusButtonActionPerformed
//        try {
//            Object chargesItem = chargesComboBox.getSelectedItem();
//            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();
//
//            Iterator<SalesCharge> iter = salesChargesList.iterator();
//            while (iter.hasNext()) {
//                if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
//                    iter.remove();
//                }
//            }
//            chargesComboBox.setSelectedIndex(0);
//            chargesText.setText("0.00");
//            fillSalesItemTable();
//
//        } catch (Exception e) {
//            log.error("chargesMinusButtonActionPerformed:", e);
//        }
        try {
            Iterator<SalesCharge> iter = salesChargesList.iterator();
            while (iter.hasNext()) {
                if(charges!=null)
                {
                if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                    iter.remove();
                }
              }
                 else if(saleCharge.getCharges().getName().equalsIgnoreCase(iter.next().getCharges().getName())) 
                   iter.remove();
            }
            chargesTextBox.setText("");
            chargesText.setText("0.00");
            fillSalesItemTable();

        } catch (Exception e) {
            log.error("chargesMinusButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesMinusButtonActionPerformed

    private void chargesAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesAddButtonActionPerformed

//        try {
//            Object chargesItem = chargesComboBox.getSelectedItem();
//            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();
//            double chargeAmt = Double.parseDouble(chargesText.getText());
//            double chargeAmt1 = Double.parseDouble(chargesText1.getText());
//            if ((Double.parseDouble(chargesText.getText()) > 0 || Double.parseDouble(chargesText1.getText()) > 0) && !charges.getName().equalsIgnoreCase("N/A")) {
//                SalesCharge salesCharge = new SalesCharge();
//
//                Iterator<SalesCharge> iter = salesChargesList.iterator();
//                while (iter.hasNext()) {
//                    if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
//                        iter.remove();
//                    }
//                }
//
//                salesCharge.setCharges(charges);
//                salesCharge.setAmount(chargeAmt);
//                salesCharge.setAmount1(chargeAmt1);
//                salesChargesList.add(salesCharge);
//                chargesComboBox.setSelectedIndex(0);
//                chargesText.setText("0.00");
//                chargesText1.setText("0.00");
//                fillSalesItemTable();
//            }
//
//        } catch (Exception e) {
//            log.error("chargesAddButtonActionPerformed:", e);
//        }
        try {
           if(salesItemList.size()>0)
           {
            chargesAmt = Double.parseDouble(chargesText.getText());
            chargesAmt1 = Double.parseDouble(chargesText1.getText());
            if ((Double.parseDouble(chargesText.getText()) > 0 || Double.parseDouble(chargesText1.getText()) > 0) && !charges.getName().equalsIgnoreCase("N/A")) {
                SalesCharge salesCharge = new SalesCharge();

                Iterator<SalesCharge> iter = salesChargesList.iterator();
                while (iter.hasNext()) {
                    if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                        iter.remove();
                    }
                }
                salesCharge.setCharges(charges);
                salesCharge.setAmount(chargesAmt);
                salesCharge.setAmount1(chargesAmt1);
                salesChargesList.add(salesCharge);
                chargesTextBox.setText("");
                chargesText.setText("0.00");
                chargesText1.setText("0.00");
                fillSalesItemTable();
            }
          }
          else
           {
            MsgBox.warning("Sales Item List Empty......!!"); 
           }

        } catch (Exception e) {
            log.error("chargesAddButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesAddButtonActionPerformed

    private void chargesTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chargesTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_chargesTextKeyTyped

    private void chargesTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesTextFocusGained
        chargesText.selectAll();
    }//GEN-LAST:event_chargesTextFocusGained

    private void itemQtyTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyTextKeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_itemQtyTextKeyTyped

    private void itemQtyTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyTextKeyPressed

    }//GEN-LAST:event_itemQtyTextKeyPressed

    private void itemQtyTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyTextFocusGained
        itemQtyText.selectAll();
    }//GEN-LAST:event_itemQtyTextFocusGained

    private void itemRemoveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemRemoveButtonActionPerformed
        if (null != salesItem) {
            salesItemRemovedList.add(salesItem);
            salesItemList.remove(salesItem);
            fillSalesItemTable();
            salesItem = null;
        }
    }//GEN-LAST:event_itemRemoveButtonActionPerformed

    private void addItemButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addItemButtonActionPerformed

        try {
            Boolean conf = true;
            if (item.getSellingPrice() <= (Double.parseDouble(itemPriceText.getText())) * currency.getRate()) {
                if ((Double.parseDouble(itemPriceText.getText().trim())) >= Double.parseDouble(itemDiscountText.getText().trim())) {
                    if (Double.parseDouble(itemQtyText.getText()) > item.getAvailableQty()) {
                        conf = MsgBox.confirm("No sufficient quantity ,Do you want to continue?");
                    }
                    if (conf == true) {
                        if (CommonService.stringValidator(new String[]{itemCodeText.getText(), itemDescriptionText.getText()})) {
                            Double itemQty = Double.parseDouble(itemQtyText.getText());
                            Double itemQty1 = Double.parseDouble(itemQtyText1.getText());

                            if (((Double.parseDouble(itemQtyText.getText()) > 0) && (itemQtyText.getText().length() > 0)) || ((Double.parseDouble(itemQtyText1.getText()) > 0) && (itemQtyText1.getText().length() > 0))) {
                                SalesItem salesItem = null;
                                for (SalesItem pItem : salesItemList) {
                                    if (pItem.getItem().getItemCode().equalsIgnoreCase(item.getItemCode()) && pItem.getItem().getGodown().getName().equalsIgnoreCase(item.getGodown().getName())&&(pItem.getItem().getDescription().equalsIgnoreCase(item.getDescription()))) {
                                        salesItem = pItem;
                                        break;
                                    }
                                }

                                if (null == salesItem) {
                                    salesItem = new SalesItem();
                                    salesItemList.add(salesItem);
                                }

                                salesItem.setItem(item);
                                salesItem.setQuantity(itemQty);
                                salesItem.setQuantity1(itemQty1);
                                salesItem.setUnitPrice((Double.parseDouble(itemPriceText.getText())) * currency.getRate());
                                salesItem.setDiscount((Double.parseDouble(itemDiscountText.getText())));

                                fillSalesItemTable();
                            }

                        } else {
                            MsgBox.warning("No Item Selected.");
                        }
                    } else {
                        //clearAllFields();
                        itemCodeText.selectAll();
                    }

                } else {
                    MsgBox.warning("Item discount is more than Item price");
                }
            } else {
                MsgBox.warning("Selling Price should be  greater than or Equal to Item Price ");
            }
        } catch (Exception e) {
            log.error("addItemButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_addItemButtonActionPerformed

    private void itemCodeTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemCodeTextKeyPressed
        try {
            ItemService itemService = new ItemService();
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                String productId = itemCodeText.getText().trim();
                List<Item> items = itemService.populateSalesPopupTableByProductId(productId, popupTableDialog, salesItemList, currency,isDuplicateColumnsVisible,salesItemListHistory);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();

                itemQtyText.requestFocusInWindow();

                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    item = items.get(index);
                    itemCodeText.setText(item.getItemCode());
                    itemDescriptionText.setText(item.getDescription());
                    stockGroupText.setText(item.getStockGroup().getName());
                    itemGodownText.setText(item.getGodown().getName());
                    itemPriceText.setText((item.getSellingPrice() / currency.getRate()) + "");

                    if (isDuplicateColumnsVisible) {
                        itemQty = itemDAO.findTotItemQty1(item.getItemCode());
                        avalQtytLabel.setText(Double.toString(item.getAvailableQty() + item.getAvailableQty1()));
                    } else {
                        itemQty = itemDAO.findTotItemQty(item.getItemCode());
                        avalQtytLabel.setText(Double.toString(item.getAvailableQty()));
                    }
                    totAvalQtyLabel.setText(itemQty + "");

                } else {
                    MsgBox.warning("No Item Selected.");
                    itemQtyText.requestFocusInWindow();
                }
            }
        } catch (Exception e) {
            log.error("productIdTextKeyPressed:", e);
        }
    }//GEN-LAST:event_itemCodeTextKeyPressed

    private void itemDescriptionTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemDescriptionTextKeyPressed
        try {
            ItemService itemService = new ItemService();
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                String itemName = itemDescriptionText.getText().trim();
                List<Item> items = itemService.populateSalesPopupTableByItemName(itemName, popupTableDialog, salesItemList, currency,isDuplicateColumnsVisible,salesItemListHistory);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();

                itemQtyText.requestFocusInWindow();

                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    item = items.get(index);
                    itemCodeText.setText(item.getItemCode());
                    itemDescriptionText.setText(item.getDescription());
                    //stockGroupText.setText(item.getWeight() + "");
                    stockGroupText.setText(item.getStockGroup().getName());
                    itemGodownText.setText(item.getGodown().getName());
                    itemPriceText.setText((item.getSellingPrice() / currency.getRate()) + "");

                    if (isDuplicateColumnsVisible) {
                        itemQty = itemDAO.findTotItemQty1(item.getItemCode());
                        avalQtytLabel.setText(Double.toString(item.getAvailableQty() + item.getAvailableQty1()));
                    } else {
                        itemQty = itemDAO.findTotItemQty(item.getItemCode());
                        avalQtytLabel.setText(Double.toString(item.getAvailableQty()));
                    }
                    totAvalQtyLabel.setText(itemQty + "");
                } else {
                    MsgBox.warning("No Item Selected.");
                    itemDescriptionText.requestFocusInWindow();
                }
            }
        } catch (Exception e) {
            log.error("itemNameTextKeyPressed:", e);
        }
    }//GEN-LAST:event_itemDescriptionTextKeyPressed

    private void customerTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customerTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customerTextActionPerformed

    private void invoiceNumberTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_invoiceNumberTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_invoiceNumberTextActionPerformed

    private void invoiceNumberTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_invoiceNumberTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_invoiceNumberTextKeyPressed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        clearAllFields();
       
    }//GEN-LAST:event_newButtonActionPerformed

    private void itemPriceTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemPriceTextFocusGained
        itemPriceText.selectAll();
    }//GEN-LAST:event_itemPriceTextFocusGained

    private void itemPriceTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemPriceTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemPriceTextKeyPressed

    private void itemPriceTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemPriceTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_itemPriceTextKeyTyped

    private void stockGroupTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_stockGroupTextFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_stockGroupTextFocusGained

    private void stockGroupTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_stockGroupTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_stockGroupTextKeyPressed

    private void stockGroupTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_stockGroupTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_stockGroupTextKeyTyped

    private void itemGodownTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemGodownTextFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_itemGodownTextFocusGained

    private void itemGodownTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemGodownTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemGodownTextKeyPressed

    private void itemGodownTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemGodownTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemGodownTextKeyTyped

    private void itemDiscountTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemDiscountTextFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_itemDiscountTextFocusGained

    private void itemDiscountTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemDiscountTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemDiscountTextKeyPressed

    private void itemDiscountTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemDiscountTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_itemDiscountTextKeyTyped

    private void itemGodownTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemGodownTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemGodownTextActionPerformed

    private void itemDiscountTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemDiscountTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemDiscountTextActionPerformed
    void printInvoice(){
        try {
            if (billDateFormattedText.getDate() != null && customerLedger != null && salesItemList != null) {
                if (MsgBox.confirm("Are you sure you want to save and print?")) {
                    saveOrUpdateSales();
                    SalesReport report = new SalesReport();
                    if (report.createSalesReport(sales, "print",isDuplicateColumnsVisible)) {
                        clearAllFields();
                    } else {
                        MsgBox.warning("Something went wrong, please re-click the button again!");
                    }
                }
            } else {
                MsgBox.warning("Required fields are not entered");
            }
        } catch (Exception ex) {
            log.error(ex);
        }
    }
    private void printButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printButtonActionPerformed
        printInvoice();
    }//GEN-LAST:event_printButtonActionPerformed

    private void pdfButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdfButtonActionPerformed
        try {
            if (billDateFormattedText.getDate() != null && customerLedger != null && salesItemList != null) {
                if (MsgBox.confirm("Are you sure you want to save and create pdf?")) {
                    saveOrUpdateSales();
                    SalesReport report = new SalesReport();
                    if (report.createSalesReport(sales, "pdf",isDuplicateColumnsVisible)) {
                        clearAllFields();
                    } else {
                        MsgBox.warning("Something went wrong, please re-click the button again!");
                    }
                }
            } else {
                MsgBox.warning("Required fields are not entered");
            }
        } catch (Exception e) {
            log.error("printButtonActionPerformed", e);
        }
    }//GEN-LAST:event_pdfButtonActionPerformed

    private void itemCodeTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemCodeTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemCodeTextActionPerformed

    private void itemQtyText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyText1FocusGained
        itemQtyText1.selectAll();
    }//GEN-LAST:event_itemQtyText1FocusGained

    private void itemQtyText1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyText1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemQtyText1KeyPressed

    private void itemQtyText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyText1KeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_itemQtyText1KeyTyped

    private void chargesText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesText1FocusGained
        chargesText1.selectAll();
    }//GEN-LAST:event_chargesText1FocusGained

    private void chargesText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chargesText1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_chargesText1KeyTyped

    private void totDiscountAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totDiscountAddButtonActionPerformed

        // TODO add your handling code here:
        if (totDiscountAddButton.getText().trim().equalsIgnoreCase("+")) {
            totDiscount = Double.parseDouble(discountText.getText().trim());
            totDiscount1 = Double.parseDouble(discountText1.getText().trim());
        } else {
            totDiscount = 0;

            totDiscount1 = 0;

            totDiscountAddButton.setText("+");
        }
        discountText.setText("0.00");
        discountText1.setText("0.00");
        fillSalesItemTable();
    }//GEN-LAST:event_totDiscountAddButtonActionPerformed

    private void discountTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_discountTextFocusGained
       discountText.selectAll();
    }//GEN-LAST:event_discountTextFocusGained

    private void discountTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_discountTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_discountTextKeyTyped

    private void discountText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_discountText1FocusGained
        discountText1.selectAll();
    }//GEN-LAST:event_discountText1FocusGained

    private void discountText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_discountText1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_discountText1KeyTyped

    private void receivedCashTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_receivedCashTextKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER)
        {
            if(receivedCashText.getText()!=null)
            {
                double bal=0.00;
                double received=0.00;
                received=Double.parseDouble(receivedCashText.getText());
                bal=(received-netTotal);
                balanceAmtLabel.setText(""+commonService.formatIntoCurrencyAsString(bal));
            }
        }
    }//GEN-LAST:event_receivedCashTextKeyPressed

    private void receivedCashTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_receivedCashTextKeyTyped

    }//GEN-LAST:event_receivedCashTextKeyTyped

    private void taxTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_taxTextKeyPressed
         try
        {
            ItemService itemService = new ItemService();
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                List<Tax> taxes = taxService.populatePopupTableByTax(taxText.getText().trim(), popupTableDialog);
                taxList=taxes;
                taxCount=taxList.size();
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();
                if(selectedRow!=null)
                {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    tax = taxes.get(index);
                    taxText.setText(tax.getName());
                }
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }//GEN-LAST:event_taxTextKeyPressed

    private void chargesTextBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chargesTextBoxKeyPressed
        try
        {
            ItemService itemService = new ItemService();
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                List<Charges> chargeList  = chargesService.populatePopupTableByCharges(chargesTextBox.getText().trim(), popupTableDialog);
                chargesList=chargeList;
                chargesCount=chargesList.size();
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();
                if(selectedRow!=null)
                {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    charges = chargeList.get(index);
                    chargesTextBox.setText(charges.getName());

                }
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }//GEN-LAST:event_chargesTextBoxKeyPressed

    private void shipmentModeTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_shipmentModeTextKeyPressed
        try
        {
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                List<ShipmentMode> shipmentModes = shipmentModeService.populatePopupTableByShipmentMode(shipmentModeText.getText().trim(), popupTableDialog);
                shipmentModesList=shipmentModes;
                popupTableDialog.setVisible(true);
                shipmentModesCount=popupTableDialog.getRowCount();
                ArrayList selectedRow = popupTableDialog.getSelectedRow();
                if(selectedRow!=null)
                {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    shipmentMode = shipmentModes.get(index);
                    shipmentModeText.setText(shipmentMode.getName());

                }
            }}catch(Exception e)
            {
                e.printStackTrace();
            }
    }//GEN-LAST:event_shipmentModeTextKeyPressed

    private void itemQtyTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyTextFocusLost
      if(itemQtyText.getText().isEmpty())
            itemQtyText.setText("0");
    }//GEN-LAST:event_itemQtyTextFocusLost

    private void itemQtyText1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyText1FocusLost
        if(itemQtyText1.getText().isEmpty())
            itemQtyText1.setText("0");
    }//GEN-LAST:event_itemQtyText1FocusLost

    private void itemPriceTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemPriceTextFocusLost
        if(itemQtyText.getText().isEmpty())
            itemQtyText.setText("0");
    }//GEN-LAST:event_itemPriceTextFocusLost

    private void chargesTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesTextFocusLost
        if(chargesText.getText().isEmpty())
           chargesText.setText("0.00");
    }//GEN-LAST:event_chargesTextFocusLost

    private void chargesText1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesText1FocusLost
        if(chargesText1.getText().isEmpty())
           chargesText1.setText("0.00");
    }//GEN-LAST:event_chargesText1FocusLost

    private void discountTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_discountTextFocusLost
       if(discountText.getText().isEmpty())
           discountText.setText("0.00");
    }//GEN-LAST:event_discountTextFocusLost

    private void discountText1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_discountText1FocusLost
         if(discountText1.getText().isEmpty())
           discountText1.setText("0.00");
    }//GEN-LAST:event_discountText1FocusLost

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        SalesReturnDAO salesReturnDAO= new SalesReturnDAO();
        List<SalesReturn> purchaseReturnList = salesReturnDAO.findAllSalesReturn(sales);
        if(!purchaseReturnList.isEmpty())
        {
            MsgBox.warning("Purchase return exists for this purchase. Hence this cannot be deleted");
        }
        else
        {
            deleteSales();
        }
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void taxTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_taxTextFocusGained
        taxText.selectAll();
    }//GEN-LAST:event_taxTextFocusGained

    private void chargesTextBoxFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesTextBoxFocusGained
       chargesText.selectAll();
    }//GEN-LAST:event_chargesTextBoxFocusGained

    void deleteSales()
    {
        double availableBal=0.00;
        double availableBal1=0.00;
        if(sales!=null)
        {
            try {
                for(SalesItem salesItem:sales.getSalesItems())
                {
                    cRUDServices.saveOrUpdateModel(salesItem.getItem());
                }
                //Updating Sales Ledger available balance
                cRUDServices.saveOrUpdateModel(salesLedger);
                
                //Update discount in leadger
                cRUDServices.saveOrUpdateModel(discountLedger);
                
                //Updating availableQty of Supplier Ledger
                if (customerLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    try {
                        cRUDServices.saveOrUpdateModel(customerLedger);
                    } catch (Exception ex) {
                        java.util.logging.Logger.getLogger(SalesView.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (customerLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    try {
                        cRUDServices.saveOrUpdateModel(customerLedger);
                    } catch (Exception ex) {
                        java.util.logging.Logger.getLogger(SalesView.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                //sales tax ledgers
                for(Ledger ledger : taxLedgerList)
                {
                    cRUDServices.saveOrUpdateModel(ledger);
                }
                //sales charges ledger
                for(Ledger ledger : chargesLedgerList)
                {
                    cRUDServices.saveOrUpdateModel(ledger);
                }
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(SalesView.class.getName()).log(Level.SEVERE, null, ex);
            }
            salesDAO.deleteSales(sales);
            MsgBox.success("Sales is deleted");
            clearAllFields();
            
        }
        else
        {
            MsgBox.warning("No Sales to delete");
        }
    }
    void clearAllFields() {
        try {
            saveButton.setText("<HTML><U>S</U>ave<HTML>");
            billDateFormattedText.setDate(new java.util.Date());
            deliveryText.setText("");
            CommonService.clearTextFields(new JTextField[]{ customerText});
            CommonService.clearCurrencyFields(new JTextField[]{chargesText});
           // currencyText.setText("");
            //multiCurrencyComboBox.setSelectedIndex(0);
//          taxComboBox.setSelectedIndex(0);
//          chargesComboBox.setSelectedIndex(0);
            //shipmentComboBox.setSelectedIndex(0);
           // currencyText.setText(multiCurrencyDAO.findAll().get(0).getName());
            taxText.setText("N/A");
            chargesTextBox.setText("N/A");
            shipmentModeText.setText("N/A");
            addressTextArea.setText("");
            salesItemList = new ArrayList<SalesItem>();        
            salesTaxList = new ArrayList<SalesTax>();
            salesChargesList = new ArrayList<SalesCharge>();
            customerLedger = null;
            salesLedger = ledgerDAO.findByLedgerName("Sales Account");
            
            netTotal = 0.00;
            subTotal = 0.00;
            grandTotal = 0.00;
            salesItemTableModel.setRowCount(0);
//            Object currencyItem = multiCurrencyComboBox.getSelectedItem();
//            currency = (MultiCurrency) ((ComboKeyValue) currencyItem).getValue();
            String currencySymbol = (null == currency) ? "" : currency.getSymbol();
            netTotalLabel.setText(currencySymbol + " " + subTotal);
            grandAmntLabel.setText(currencySymbol + " " + grandTotal);
            numberProperty = numberPropertyDAO.findInvoiceNo("Sales");
            invoiceNumberText.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
            CommonService.clearNumberFields(new JTextField[]{itemQtyText, itemDiscountText, stockGroupText, itemPriceText});
            CommonService.clearTextFields(new JTextField[]{itemCodeText, itemDescriptionText, itemGodownText});
            avalQtytLabel.setText("");
            totAvalQtyLabel.setText("");
            sales = null;
            totDiscount = 0.00;
            totDiscount1 = 0.00;
            discountText.setText("0.00");
            saveButton.setText("Save");
            customerText.requestFocusInWindow();
        } catch (Exception e) {
            log.error("clearAllFields:", e);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel InvoiceDetailsPanel;
    private javax.swing.JButton addItemButton;
    private javax.swing.JTextArea addressTextArea;
    private javax.swing.JLabel availableQtyLabel2;
    private javax.swing.JLabel avalQtytLabel;
    private javax.swing.JLabel balanceAmtLabel;
    private javax.swing.JLabel balanceLabel1;
    private com.toedter.calendar.JDateChooser billDateFormattedText;
    private javax.swing.JPanel bottomPanel;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton chargesAddButton;
    private javax.swing.JButton chargesMinusButton;
    private javax.swing.JPanel chargesPanel;
    private javax.swing.JPanel chargesPanel1;
    private javax.swing.JFormattedTextField chargesText;
    private javax.swing.JFormattedTextField chargesText1;
    private javax.swing.JTextField chargesTextBox;
    private javax.swing.JTextField customerText;
    private javax.swing.JButton deleteButton;
    private javax.swing.JLabel deliveryLabel;
    private javax.swing.JTextField deliveryText;
    private javax.swing.JPanel discountPanel1;
    private javax.swing.JFormattedTextField discountText;
    private javax.swing.JFormattedTextField discountText1;
    private javax.swing.JLabel grandAmntLabel;
    private javax.swing.JPanel grandTotalPanel;
    private javax.swing.JLabel grossAmountLabel;
    private javax.swing.JTextField invoiceNumberText;
    private javax.swing.JPanel itemButtonPanel;
    private javax.swing.JTextField itemCodeText;
    private javax.swing.JTextField itemDescriptionText;
    private javax.swing.JTextField itemDiscountText;
    private javax.swing.JTextField itemGodownText;
    private javax.swing.JPanel itemInfoPanel;
    private javax.swing.JTextField itemPriceText;
    private javax.swing.JTextField itemQtyText;
    private javax.swing.JTextField itemQtyText1;
    private javax.swing.JButton itemRemoveButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel netQtyLabel;
    private javax.swing.JLabel netTotalLabel;
    private javax.swing.JButton newButton;
    private javax.swing.JButton pdfButton;
    private javax.swing.JButton printButton;
    private javax.swing.JPanel qtyPanel;
    private javax.swing.JLabel receivedCashLabel;
    private javax.swing.JTextField receivedCashText;
    private javax.swing.JTable salesItemTable;
    private javax.swing.JButton saveButton;
    private javax.swing.JPanel shipmentModePanel;
    private javax.swing.JTextField shipmentModeText;
    private javax.swing.JTextField stockGroupText;
    private javax.swing.JPanel tableBorderPanel;
    private javax.swing.JButton taxAddButton;
    private javax.swing.JPanel taxPanel;
    private javax.swing.JTextField taxText;
    private javax.swing.JLabel totAvalQtyLabel;
    private javax.swing.JButton totDiscountAddButton;
    // End of variables declaration//GEN-END:variables
}
