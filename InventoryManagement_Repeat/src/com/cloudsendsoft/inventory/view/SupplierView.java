/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.LedgerGroupDAO;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.LedgerService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
//import static com.cloudsendsoft.inventory.view.CustomerView.log;
import static com.cloudsendsoft.inventory.view.LedgerView.log;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author LOGON SOFT 1
 */
public class SupplierView extends javax.swing.JInternalFrame {

    /**
     * Creates new form SupplierView
     */
    static final Logger log = Logger.getLogger(SupplierView.class.getName());
    LedgerService ledgerService = new LedgerService();
    Ledger ledger = null;
    LedgerDAO ledgerDAO = new LedgerDAO();
    LedgerGroupDAO ledgerGroupDAO = new LedgerGroupDAO(); 
    CRUDServices cRUDServices = null;
    List<Ledger> ledgerList = null;
    public SupplierView() {
        initComponents();
        if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
            saveButton.setEnabled(false);
            newButton.setEnabled(false);
        }
        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);
        cRUDServices = new CRUDServices();
        this.getRootPane().setDefaultButton(saveButton);
        ledgerList = ledgerService.fillSuppliersTable(suppliersTable);
        SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        nameText.requestFocus();
      }
        });
        //save shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "save");
            this.getActionMap().put("save", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveButtonActionPerformed(e);
                }
            });
         //new Shortcut
           this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_N,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "new");
            this.getActionMap().put("new", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    newButtonActionPerformed(e);
                }
            });
            //Delete shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_D,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "delete");
            this.getActionMap().put("delete", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    deleteButtonActionPerformed(e);
                }
            });
           // Cancel shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_W,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "cancel");
            this.getActionMap().put("cancel", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveButtonActionPerformed(e);
                }
            });
            setFocusOrder();
    }
    void setFocusOrder()
    {
            nameText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addressTextPane.requestFocusInWindow();
                }
            });
            telephoneText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    emailText.requestFocusInWindow();
                }
            });
           emailText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    faxText.requestFocusInWindow();
                }
            });
           faxText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    incomeTaxText.requestFocusInWindow();
                }
            });
           incomeTaxText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    salesTaxText.requestFocusInWindow();
                }
            });
           salesTaxText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveButton.requestFocusInWindow();
                }
            });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        nameText = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        telephoneText = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        emailText = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        addressTextPane = new javax.swing.JTextPane();
        incomeTaxText = new javax.swing.JTextField();
        salesTaxText = new javax.swing.JTextField();
        faxText = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        suppliersTable = new javax.swing.JTable();

        setClosable(true);
        setTitle("Suppliers");
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setPreferredSize(new java.awt.Dimension(394, 247));

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Details"));
        jPanel6.setOpaque(false);

        jLabel6.setText("Address");

        jLabel7.setText("Name");

        jLabel8.setText("Telephone");

        jLabel9.setText("E-mail");

        jLabel10.setText("Income Tax No");

        jLabel11.setText("Sales Tax No");

        jScrollPane3.setViewportView(addressTextPane);

        incomeTaxText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                incomeTaxTextActionPerformed(evt);
            }
        });

        jLabel12.setText("Fax");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(salesTaxText, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(incomeTaxText, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(faxText, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(emailText)
                    .addComponent(telephoneText)
                    .addComponent(jScrollPane3)
                    .addComponent(nameText))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(nameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(telephoneText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(emailText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(faxText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(incomeTaxText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(salesTaxText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel5.setOpaque(false);
        jPanel5.setLayout(new java.awt.GridLayout(6, 1, 0, 8));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("New");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        jPanel5.add(newButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveButton.setText("Save/Update");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        jPanel5.add(saveButton);

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/delete-icon.png"))); // NOI18N
        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        jPanel5.add(deleteButton);

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        jPanel5.add(cancelButton);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 248, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Suppliers Table"));

        suppliersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        suppliersTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                suppliersTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(suppliersTable);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 565, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 565, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 461, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                    .addContainerGap(17, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(17, Short.MAX_VALUE)))
        );

        jPanel1.add(jPanel3, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel1, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void incomeTaxTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_incomeTaxTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_incomeTaxTextActionPerformed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
             ledger = null;
             CommonService.clearTextFields(new JTextField[]{nameText, telephoneText, emailText,faxText,incomeTaxText,salesTaxText});
             CommonService.clearTextPane(new JTextPane[]{addressTextPane});
             ledger = null;
    }//GEN-LAST:event_newButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
             try {
            String ledgerName = nameText.getText().trim();
            boolean isEditable = true;
            if (ledger != null) {
                if (!ledger.isEditable()) {
                    isEditable = false;
                }
            }
            if (isEditable) {
                if (!ledgerDAO.isLedgerNameExist(ledgerName)) {
                    LedgerGroup ledgerGroup = ledgerGroupDAO.findByGroupName("Sundry Creditors");
                    double balanceAmount = 0.00;
                   if (CommonService.stringValidator(new String[]{ledgerName})) {
                        if (MsgBox.confirm("Are you sure you want to save or update?")) {
                            if (ledger == null) {
                                ledger = new Ledger();
                            }
                            ledger.setCompany(GlobalProperty.getCompany());
                            ledger.setLedgerName(ledgerName);
                            ledger.setLedgerGroup(ledgerGroup);
                            if (nameText.getText().trim().length() > 0) {
                                ledger.setName(ledgerName);
                            }
                            if (addressTextPane.getText().trim().length() > 0) {
                                ledger.setAddress(addressTextPane.getText());
                            }
                            if (telephoneText.getText().trim().length() > 0) {
                                ledger.setTelephone(telephoneText.getText().trim());
                            }
                            if (emailText.getText().trim().length() > 0) {
                                ledger.setEmail(emailText.getText().trim());
                            }
                            if (faxText.getText().trim().length() > 0) {
                                ledger.setFax(faxText.getText().trim());
                            }
                            if (salesTaxText.getText().trim().length() > 0) {
                                ledger.setSalesTaxNo(salesTaxText.getText().trim());
                            }
                            if (incomeTaxText.getText().trim().length() > 0) {
                                ledger.setIncomeTaxNo(incomeTaxText.getText().trim());
                            }
                            ledger.setAvailableBalance(balanceAmount);
                            ledger.setOpeningBalance(balanceAmount);
                            ledger.setEditable(true);
                            cRUDServices.saveOrUpdateModel(ledger);
                            MsgBox.success("Successfully Added " + ledgerName);
                            CommonService.clearTextFields(new JTextField[]{ nameText, telephoneText, emailText,faxText,incomeTaxText});
                            CommonService.clearTextPane(new JTextPane[]{addressTextPane});
                            ledgerList=ledgerService.fillSuppliersTable(suppliersTable);
                            
                        }
                    } else {
                        MsgBox.warning("Ledger Name is mandatory");
                        nameText.requestFocusInWindow();
                        ledger=null;
                    }
                } else {
                    MsgBox.abort("Ledger Name '" + ledgerName + "' is already exists.");
                    nameText.requestFocusInWindow();
                }
            } else {
                MsgBox.warning("You can't edit auto generated ledger");
            }
        } catch (Exception e) {
            log.error("addButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
             try {
            Session session = null;
            if (null != ledger) {
                String ledgerName = ledger.getLedgerName();
                if (ledger.isEditable()) {
                    if (MsgBox.confirm("Are you sure you want to delete?")) {
                        try {
                            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
                            session = sessionFactory.openSession();
                            Transaction transaction = session.beginTransaction();
                            session.delete(ledger);
                            transaction.commit();

                            MsgBox.success("Successfully Deleted " + ledgerName);
                            CommonService.clearTextFields(new JTextField[]{ nameText, telephoneText, emailText, faxText, salesTaxText, incomeTaxText,salesTaxText});
                            CommonService.clearTextPane(new JTextPane[]{addressTextPane});
                            ledgerList= ledgerService.fillSuppliersTable(suppliersTable);
                            nameText.requestFocusInWindow();

                        } catch (Exception e) {
                            MsgBox.warning("Ledger '" + ledgerName + "' is already used for transactions");
                            nameText.requestFocusInWindow();
                        } finally {
                            if (session != null) {
                                session.clear();
                                session.close();
                            }
                        }
                    }
                } else {
                    MsgBox.warning("You can't delete auto generated ledger");
                }
            }
        } catch (Exception e) {
            log.error("deleteButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void suppliersTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_suppliersTableMouseClicked
            ledger= ledgerList.get(suppliersTable.getSelectedRow());
            int column = suppliersTable.getSelectedColumn();
            nameText.setText(ledger.getName()); 
            addressTextPane.setText(ledger.getAddress());
            telephoneText.setText(ledger.getTelephone());
            emailText.setText(ledger.getEmail());
            faxText.setText(ledger.getFax());
            incomeTaxText.setText(ledger.getIncomeTaxNo());
            salesTaxText.setText(ledger.getSalesTaxNo());
            switch(column)
            {
                case 0: nameText.requestFocusInWindow();
                        break;
                case 1: addressTextPane.requestFocusInWindow();
                        break;
                case 2: telephoneText.requestFocusInWindow();
                        break;
                case 3: emailText.requestFocusInWindow();
                        break;
                case 4: faxText.requestFocusInWindow();
                        break;
                case 5: incomeTaxText.requestFocusInWindow();
                        break;
                case 6: salesTaxText.requestFocusInWindow();
                        break;
           }
    }//GEN-LAST:event_suppliersTableMouseClicked

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextPane addressTextPane;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton deleteButton;
    private javax.swing.JTextField emailText;
    private javax.swing.JTextField faxText;
    private javax.swing.JTextField incomeTaxText;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField nameText;
    private javax.swing.JButton newButton;
    private javax.swing.JTextField salesTaxText;
    private javax.swing.JButton saveButton;
    private javax.swing.JTable suppliersTable;
    private javax.swing.JTextField telephoneText;
    // End of variables declaration//GEN-END:variables
}
