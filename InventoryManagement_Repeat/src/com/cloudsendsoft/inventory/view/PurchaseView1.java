/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.bean.PrintBarcodeBean;
import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.GodownDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.MultiCurrencyDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.dao.PurchaseDAO;
import com.cloudsendsoft.inventory.dao.PurchaseItemDAO;
import com.cloudsendsoft.inventory.dao.PurchaseReturnDAO;
import com.cloudsendsoft.inventory.dao.ShipmentModeDAO;
import com.cloudsendsoft.inventory.dao.StockGroupDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.dao.UnitDAO;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.ItemService;
import com.cloudsendsoft.inventory.services.LedgerService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Godown;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseCharge;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseReturn;
import com.cloudsendsoft.inventory.model.PurchaseTax;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.model.Unit;
import com.cloudsendsoft.inventory.report.BarcodeReport;
import com.cloudsendsoft.inventory.services.ChargesService;
import com.cloudsendsoft.inventory.services.MultiCurrencyService;
import com.cloudsendsoft.inventory.services.ShipmentModeService;
import com.cloudsendsoft.inventory.services.TaxService;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import static com.cloudsendsoft.inventory.view.ItemInfo.log;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class PurchaseView1 extends javax.swing.JPanel {

    /**
     * Creates new form PurchaseViewPanel
     */
    static double totalAmount = 0;
    double subTotal = 0.00;
    double grandTotal = 0.00;
    double netTotal = 0.00;
    double totDiscount = 0.00;

    double subTotal1 = 0.00;
    double grandTotal1 = 0.00;
    double netTotal1 = 0.00;
    double totDiscount1 = 0.00;

    static final Logger log = Logger.getLogger(PurchaseView1.class.getName());
    Ledger ledger = null;

    ItemDAO itemDAO = new ItemDAO();
    CommonService commonService = new CommonService();
    MainForm mainForm = null;
    LedgerService ledgerService = new LedgerService();
    TaxDAO taxDAO = new TaxDAO();
    GodownDAO godownDAO = new GodownDAO();
    ChargesDAO chargesDAO = new ChargesDAO();
    MultiCurrencyDAO multiCurrencyDAO = new MultiCurrencyDAO();
    ShipmentModeDAO shipmentModeDAO = new ShipmentModeDAO();
    PurchaseDAO purchaseDAO = new PurchaseDAO();
    NumberProperty numberProperty = null;
    NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
    StockGroupDAO stockGroupDAO = null;
    //global usage
    Ledger supplierLedger = null;
    double itemQty = 0;
    List<PurchaseItem> purchaseItemList = new ArrayList<PurchaseItem>();
     List<PurchaseItem> purchaseItemRemovedList = new ArrayList<PurchaseItem>();
    List<PurchaseItem> purchaseItemListHistory = null;
    Item item = null;
    StockGroup stockGroup=null;
    Godown godown=null;
    List<PurchaseTax> purchaseTaxList = new ArrayList<PurchaseTax>();
    List<PurchaseCharge> purchaseChargesList = new ArrayList<PurchaseCharge>();

    MultiCurrency currency = null;
    Purchase purchase = null;
    int check = 0;

    PurchaseItem purchaseItem = null;
    DefaultTableModel purchaseItemTableModel = null;

    LedgerDAO ledgerDAO = new LedgerDAO();
   
    CRUDServices cRUDServices = new CRUDServices();

    //Double previousPurchaseAccount=0.00;
    //Double previousDiscountReceived=0.00;
    Ledger purchaseLedger = null;
    Ledger discountLedger = null;
    List<Ledger> taxLedgerList = new ArrayList<Ledger>();
    List<Ledger> chargesLedgerList = new ArrayList<Ledger>();
    TaxService taxService = new TaxService();
    List<Tax>taxList=null;
    Tax tax=null;
    PurchaseTax purchaseTax=null;
    int chargesCount=0,taxCount=0;
    ChargesService chargesService = new ChargesService();
    List<Charges>chargesList=null;
    Charges charges=null;
    double chargeAmt=0.00;
    double chargeAmt1=0.00;
    PurchaseCharge purchaseCharge=null;
    MultiCurrencyService multiCurrencyService = new MultiCurrencyService();
    List<MultiCurrency> currencyList = null;
    double currencyCount=0;
    ShipmentModeService shipmentModeService = new ShipmentModeService();
    List<ShipmentMode> shipmentModesList =null;
    ShipmentMode shipmentMode =null;
    int shipmentModesCount =0;
    UnitDAO unitDAO=new UnitDAO();
    
    boolean isDuplicateColumnsVisible = false;

    public PurchaseView1(MainForm mainForm) {

        try {
            initComponents();

           //hide delivery date
            deliveryLabel.setVisible(false);
            deliveryDateFormattedText.setVisible(false);
            //hide godown
            godownLabel.setVisible(false);
            godownText.setVisible(false);
            //hide shipmentMode panel
            shipmentModePanel.setVisible(false);
            //removed additional qty & rate textboxes by default
            qtyPanel.remove(itemQtyText1);
            unitPricePanel.remove(itemUnitPriceText1);
            chargesPanel1.remove(chargesText1);
            discountPanel.remove(discountText1);
            SPanel.remove(STextField1);
            MPanel.remove(MTextField1);
            LPanel.remove(LTextField1);
            XLPanel.remove(XLTextField1);
            XXLPanel.remove(XXLTextField1);
//          sellingPricePanel.remove(sellingPriceText1);

            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_W,
                                    java.awt.event.InputEvent.SHIFT_DOWN_MASK
                                    | java.awt.event.InputEvent.CTRL_DOWN_MASK), "forward");
            this.getActionMap().put("forward", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (itemQtyText1.getParent() == qtyPanel) {
                        qtyPanel.remove(itemQtyText1);
                        qtyPanel.revalidate();
                        SPanel.remove(STextField1);
                        SPanel.revalidate();
                        MPanel.remove(MTextField1);
                        MPanel.revalidate();
                        LPanel.remove(LTextField1);
                        LPanel.revalidate();
                        XLPanel.remove(XLTextField1);
                        XLPanel.revalidate();
                        XXLPanel.remove(XXLTextField1);
                        XXLPanel.revalidate();
                        unitPricePanel.remove(itemUnitPriceText1);
                        unitPricePanel.revalidate();
                        chargesPanel1.remove(chargesText1);
                        chargesPanel1.revalidate();
                        discountPanel.remove(discountText1);
                        discountPanel.revalidate();
//                      sellingPricePanel.remove(sellingPriceText1);
                        sellingPricePanel.revalidate();
                        isDuplicateColumnsVisible = false;

                        //set table design
                        purchaseItemTable.setRowHeight(GlobalProperty.tableRowHieght);
                        purchaseItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                                null,
                                new String[]{"Sr. No.", "Item Code", "Description",  "Qty.", "Unit Price", "Total Amount"}
                        ));
                        purchaseItemTableModel = (DefaultTableModel) purchaseItemTable.getModel();
                        CommonService.setWidthAsPercentages(purchaseItemTable, .03, .10, .20,  .08, .08, .08);
                        
                        fillPurchaseItemTable();
                    } else {
                        qtyPanel.add(itemQtyText1);
                        qtyPanel.revalidate();
                        SPanel.add(STextField1);
                        SPanel.revalidate();
                        MPanel.add(MTextField1);
                        MPanel.revalidate();
                        LPanel.add(LTextField1);
                        LPanel.revalidate();
                        XLPanel.add(XLTextField1);
                        XLPanel.revalidate();
                        XXLPanel.add(XXLTextField1);
                        XXLPanel.revalidate();
                        unitPricePanel.add(itemUnitPriceText1);
                        unitPricePanel.revalidate();
                        chargesPanel1.add(chargesText1);
                        chargesPanel1.revalidate();
                        discountPanel.add(discountText1);
                        discountPanel.revalidate();
//                      sellingPricePanel.add(sellingPriceText1);
                        sellingPricePanel.revalidate();
                        isDuplicateColumnsVisible = true;

                        //set table design
                        purchaseItemTable.setRowHeight(GlobalProperty.tableRowHieght);
                        purchaseItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                                null,
                                new String[]{"Sr. No.", "Item Code", "Description", "Qty.", "Unit Price", "Qty1", "Unit Price1", "Total Amount"}
                        ));
                        purchaseItemTableModel = (DefaultTableModel) purchaseItemTable.getModel();
                        CommonService.setWidthAsPercentages(purchaseItemTable, .03, .10, .20, .08, .08, .08, .08, .08);
                        fillPurchaseItemTable();
                    }
                }
            });
            // END OF KEY BINDINGS
      SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        supplierText.requestFocus();
      }
        });
            //shortcut

            //save shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "save");
            this.getActionMap().put("save", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveInvoice();
                }
            });
            //new shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_N,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "new");
            this.getActionMap().put("new", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                     clearAllFields();
                }
            });
            //cancel shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                     put(KeyStroke.getKeyStroke(KeyEvent.VK_C,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "cancel");
            this.getActionMap().put("cancel", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    cancelButtonActionPerformed(e);
                }
            });
            //Print barcode shortcut
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                     put(KeyStroke.getKeyStroke(KeyEvent.VK_B,
                                    java.awt.event.InputEvent.CTRL_DOWN_MASK), "barcode");
            this.getActionMap().put("barcode", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    printBarcodeButtonActionPerformed(e);
                }
            });
            purchaseLedger = ledgerDAO.findByLedgerName("Purchase Account");
            discountLedger = ledgerDAO.findByLedgerName("Discount Received");
            //currencyText.setText(multiCurrencyDAO.findAll().get(0).getName());
            currency=multiCurrencyDAO.findByName("Indian Rupee");
            taxText.setText("N/A");
            chargesTextBox.setText("N/A");
            shipmentModeText.setText("N/A");
            // Combo boxes initialization && get all tax Ledgers to update availableQty
//            for (Tax tacs : taxDAO.findAll()) {
//                taxComboBox.addItem(new ComboKeyValue((tacs.getName()), tacs));
//                //              taxLedgerList.add(ledgerDAO.findByLedgerName(tacs.getName()));
//            }

            // Combo boxes initialization && get all charges Ledgers to update availableQty
//            for (Charges charges : chargesDAO.findAll()) {
//                chargesComboBox.addItem(new ComboKeyValue(charges.getName(), charges));
//                //             chargesLedgerList.add(ledgerDAO.findByLedgerName(charges.getName()));
//            }
            commonService.setCurrentPeriodOnCalendar(billDateFormattedText);

            if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
                saveButton.setEnabled(false);
                newButton.setEnabled(false);
                
            }
            if(!GlobalProperty.listOfPrivileges.contains("Delete")){
                deleteButton.setEnabled(false);
            }
            //table background color removed
            jScrollPane1.setOpaque(false);
            jScrollPane1.getViewport().setOpaque(false);

            billDateFormattedText.setDate(new java.util.Date());
            this.mainForm = mainForm;

            numberProperty = numberPropertyDAO.findInvoiceNo("Purchase");
            invoiceNumberText.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());

            //set table design
            purchaseItemTable.setRowHeight(GlobalProperty.tableRowHieght);
            purchaseItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                    null,
                    new String[]{"Sr. No.", "Item Code", "Description", "Qty.", "Unit Price", "Total Amount"}
            ));
            purchaseItemTableModel = (DefaultTableModel) purchaseItemTable.getModel();
            CommonService.setWidthAsPercentages(purchaseItemTable, .03, .10, .20,  .08, .08, .08);

//            for (MultiCurrency currency : multiCurrencyDAO.findAll()) {
//                if (currency == null) {
//                    this.currency = currency;
//                }
//                multiCurrencyComboBox.addItem(new ComboKeyValue(currency.getName(), currency));
//            }

//            for (ShipmentMode shipmentMode : shipmentModeDAO.findAll()) {
//                shipmentComboBox.addItem(new ComboKeyValue(shipmentMode.getName(), shipmentMode));
//            }

            //jTable rows & columns alignment
            ((DefaultTableCellRenderer) purchaseItemTable.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);

            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(JLabel.CENTER);
            purchaseItemTable.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
            purchaseItemTable.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
            purchaseItemTable.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
            purchaseItemTable.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
            //jTable rows & columns alignment
            setFocusOrder();
            
            godown=godownDAO.findByName("Main Godown");
            godownText.setText(godown.getName());

        } catch (Exception e) {
            log.error("Purchase:", e);
        }

    }

    public void listPurchaseItemHistory(Purchase purchase) {
        this.purchase = purchase;
        this.supplierLedger = purchase.getSupplier();

        //to update available Qty of every Item
        purchaseItemList = purchase.getPurchaseItems();
        purchaseItemListHistory = new ArrayList<PurchaseItem>();
        for (PurchaseItem purchaseItem : purchaseItemList) {

            //copy sales items 
            PurchaseItem purchaseItem1 = new PurchaseItem();
            purchaseItem1.setItem(purchaseItem.getItem());
            purchaseItem1.setQuantity(purchaseItem.getQuantity());
            purchaseItem1.setQuantity1(purchaseItem.getQuantity1());
            purchaseItemListHistory.add(purchaseItem1);

            Item item = purchaseItem.getItem();
            item.setAvailableQty(item.getAvailableQty() - purchaseItem.getQuantity());
            item.setAvailableQty1(item.getAvailableQty1()-purchaseItem.getQuantity1());
            purchaseLedger.setAvailableBalance(purchaseLedger.getAvailableBalance() - (purchaseItem.getUnitPrice() * purchaseItem.getQuantity()));
            purchaseLedger.setAvailableBalance1(purchaseLedger.getAvailableBalance1() - (purchaseItem.getUnitPrice1() * purchaseItem.getQuantity1()));
            purchaseItem.setUnitPrice(purchaseItem.getUnitPrice());
        }

        purchaseTaxList = purchase.getPurchaseTaxes();
        for (PurchaseTax purchaseTax : purchaseTaxList) {
            ledger = ledgerDAO.findByLedgerName(purchaseTax.getTax().getName());
            ledger.setAvailableBalance(ledger.getAvailableBalance() - purchaseTax.getAmount());
            taxLedgerList.add(ledger);
            purchaseTax.setAmount(purchaseTax.getAmount() / purchase.getCurrentCurrencyRate());
        }

        purchaseChargesList = purchase.getPurchaseCharges();
        for (PurchaseCharge purchaseCharge : purchaseChargesList) {
            ledger = ledgerDAO.findByLedgerName(purchaseCharge.getCharges().getName());
            ledger.setAvailableBalance(ledger.getAvailableBalance() - purchaseCharge.getAmount());
            ledger.setAvailableBalance1(ledger.getAvailableBalance1() - purchaseCharge.getAmount1());
            chargesLedgerList.add(ledger);
            purchaseCharge.setAmount(purchaseCharge.getAmount() / purchase.getCurrentCurrencyRate());
            purchaseCharge.setAmount1(purchaseCharge.getAmount1() / purchase.getCurrentCurrencyRate());
        }
        billDateFormattedText.setDate(purchase.getBillDate());
        invoiceNumberText.setText(purchase.getInvoiceNumber());
        supplierText.setText(purchase.getSupplier().getLedgerName());
        deliveryDateFormattedText.setText(purchase.getDeliveryDate() + "");
        addressTextArea.setText(purchase.getAddress());

        discountLedger.setAvailableBalance(discountLedger.getAvailableBalance() - purchase.getDiscount());
        discountLedger.setAvailableBalance1(discountLedger.getAvailableBalance1() - purchase.getDiscount1());

        //Updating availableQty of Supplier Ledger
        if (supplierLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
            supplierLedger.setAvailableBalance(supplierLedger.getAvailableBalance() + purchase.getGrandTotal());
            supplierLedger.setAvailableBalance1(supplierLedger.getAvailableBalance1() + purchase.getGrandTotal1());
        } else if (supplierLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
            supplierLedger.setAvailableBalance(supplierLedger.getAvailableBalance() - purchase.getGrandTotal());
            supplierLedger.setAvailableBalance1(supplierLedger.getAvailableBalance1() - purchase.getGrandTotal1());
        }

        totDiscount = purchase.getDiscount() / purchase.getCurrentCurrencyRate();
        totDiscount1 = purchase.getDiscount1() / purchase.getCurrentCurrencyRate();

        saveButton.setText("Update");
        currency = purchase.getMultiCurrency();
        currency.setRate(purchase.getCurrentCurrencyRate());
        //currency.setRate(1.0);
        check = 1;

        /* for(PurchaseItem purchaseItem:purchaseItemList){
         previousPurchaseAccount+=(purchaseItem.getQuantity()*purchaseItem.getUnitPrice());
         $$$$$$$$$$$$$$$$$$$$$     }
         previousDiscountReceived=totDiscount;
         */
//        int count = multiCurrencyComboBox.getItemCount();
//        for (int i = 0; i < count; i++) {
//            ComboKeyValue ckv = (ComboKeyValue) multiCurrencyComboBox.getItemAt(i);
//            if (ckv.getKey().trim().equalsIgnoreCase(purchase.getMultiCurrency().getName())) {
//                multiCurrencyComboBox.setSelectedItem(ckv);
//            }
//        }
//        currencyText.setText(purchase.getMultiCurrency().getName());
        if (purchase.getShipmentMode() != null) {
            shipmentModeText.setText(purchase.getShipmentMode().getName());
        }
//        int count1 = shipmentComboBox.getItemCount();
//        if (purchase.getShipmentMode() != null) {
//            for (int i = 0; i < count1; i++) {
//                ComboKeyValue ckv = (ComboKeyValue) shipmentComboBox.getItemAt(i);
//                if (ckv.getKey().trim().equalsIgnoreCase(purchase.getShipmentMode().getName())) {
//                    shipmentComboBox.setSelectedItem(ckv);
//                }
//            }
//        }
        // to avoid increasing number property while updating repeatingly
        numberProperty.setNumber(numberProperty.getNumber() - 1);
        fillPurchaseItemTable();
    }

    void generateAllList(String billDate, String invoiceNumber, String ledgerName) {
        try {
            java.sql.Date invDate = new java.sql.Date(commonService.convertToDate(billDate).getTime());
            Purchase purchase = purchaseDAO.findByDateInvNumSupplier(invDate, invoiceNumber, ledgerName);
            purchaseItemList = purchase.getPurchaseItems();
            purchaseTaxList = purchase.getPurchaseTaxes();
            purchaseChargesList = purchase.getPurchaseCharges();
            saveButton.setEnabled(false);
            newButton.setEnabled(false);
            cancelButton.setEnabled(false);
            addItemButton.setEnabled(false);
            itemRemoveButton.setEnabled(false);
            taxAddButton.setEnabled(false);
            chargesAddButton.setEnabled(false);
            chargesMinusButton.setEnabled(false);
            fillPurchaseItemTable();
        } catch (Exception e) {
            log.error("generateAllList:", e);
        }
    }

    void setFocusOrder() {
        try {
             final Set<Character> pressed = new HashSet<Character>();
            supplierText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    //deliveryDateFormattedText.requestFocusInWindow();
                    salesTaxText.requestFocusInWindow();
                }
            });
            salesTaxText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    categoryText.requestFocusInWindow();
                }
            });
           categoryText .addKeyListener(new KeyAdapter() {
               
               @Override
            public synchronized void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_CONTROL||e.getKeyCode()==KeyEvent.VK_SPACE)
                    pressed.add(e.getKeyChar());
                if (pressed.size() ==2) {
                    taxText.requestFocusInWindow();
                    pressed.clear();
                }
          // public void keyPressed(KeyEvent e) {
          // int key = e.getKeyCode();
          // if (key == KeyEvent.VK_SHIFT) {
          //     taxText.requestFocusInWindow();
          // }
           else if(e.getKeyCode()==KeyEvent.VK_ENTER)
           {         
               itemDescriptionText.requestFocusInWindow();
            itemDescriptionText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    itemQtyText.requestFocusInWindow();
                }
            });
            
            itemQtyText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(isDuplicateColumnsVisible)
                        itemQtyText1.requestFocusInWindow();
                    else
                        itemUnitPriceText.requestFocusInWindow();
                }
            });
            itemQtyText1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    itemUnitPriceText.requestFocusInWindow();
                }
            });
            itemUnitPriceText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(isDuplicateColumnsVisible)
                        itemUnitPriceText1.requestFocusInWindow();
                    else
                        sellingPriceText.requestFocusInWindow();
                }
            });
            itemUnitPriceText1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    sellingPriceText.requestFocusInWindow();
                }
            });
            sellingPriceText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
//                    godownText.requestFocusInWindow();
//                }
//            });
//            godownText.addActionListener(new ActionListener() {
//                @Override
//                public void actionPerformed(ActionEvent e) {
                    STextField.requestFocusInWindow();
                }
            });
            STextField.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(isDuplicateColumnsVisible)
                        STextField1.requestFocusInWindow();
                    else
                        MTextField.requestFocusInWindow();
                }
            });
            STextField1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    MTextField.requestFocusInWindow();
                }
            });
            MTextField.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(isDuplicateColumnsVisible)
                        MTextField1.requestFocusInWindow();
                    else
                        LTextField.requestFocusInWindow();
                }
            });
            MTextField1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    LTextField.requestFocusInWindow();
                }
            });
            LTextField.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(isDuplicateColumnsVisible)
                        LTextField1.requestFocusInWindow();
                    else
                        XLTextField.requestFocusInWindow();
                }
            });
            LTextField1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    XLTextField.requestFocusInWindow();
                }
            });
            XLTextField.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(isDuplicateColumnsVisible)
                        XLTextField1.requestFocusInWindow();
                    else
                        XXLTextField.requestFocusInWindow();
                }
            });
            XLTextField1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    XXLTextField.requestFocusInWindow();
                }
            });
            XXLTextField.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(isDuplicateColumnsVisible)
                        XXLTextField1.requestFocusInWindow();
                    else
                        addItemButton.requestFocusInWindow();
                }
            });
            XXLTextField1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addItemButton.requestFocusInWindow();
                }
            });
            }
         }
         });
           
            addItemButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    categoryText.requestFocusInWindow();
                }
            });
            
            taxText.addKeyListener(new KeyAdapter() {
                
                @Override
            public synchronized void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_CONTROL||e.getKeyCode()==KeyEvent.VK_SPACE)
                    pressed.add(e.getKeyChar());
                if (pressed.size() ==2) {
                    chargesTextBox.requestFocusInWindow();
                    pressed.clear();
                }
         //   public void keyPressed(KeyEvent e) {
         //  int key = e.getKeyCode();
          // if (key == KeyEvent.VK_SHIFT) {
         //      chargesTextBox.requestFocusInWindow();
         //  }
           else if(e.getKeyCode()==KeyEvent.VK_ENTER)
           { 
               taxText.requestFocusInWindow();
           }
         }
         });
                taxText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    taxAddButton.requestFocusInWindow();
                }
            });
            taxAddButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    commonService.clearTextFields(new JTextField[]{taxText}); 
                    taxText.requestFocusInWindow();
                }
            });
            chargesTextBox.addKeyListener(new KeyAdapter() {
           
                
                @Override
            public synchronized void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_CONTROL||e.getKeyCode()==KeyEvent.VK_SPACE)
                    pressed.add(e.getKeyChar());
                if (pressed.size() ==2) {
                    discountText.requestFocusInWindow();
                    pressed.clear();
                }
                 //public void keyPressed(KeyEvent e) {
        //   int key = e.getKeyCode();
       //    if (key == KeyEvent.VK_SHIFT) {
               //shipmentModeText.requestFocusInWindow();
        //       discountText.requestFocusInWindow();
        //   }
           else if(e.getKeyCode()==KeyEvent.VK_ENTER)
           { 
               chargesText.requestFocusInWindow();
           }
         }
         });
            chargesTextBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chargesText.requestFocusInWindow();
                }
            });
            chargesText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(isDuplicateColumnsVisible==true)
                       chargesText1.requestFocus();
                    else
                        chargesAddButton.requestFocusInWindow();
                }
            });
            chargesText1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chargesAddButton.requestFocusInWindow();
                }
            });
            chargesAddButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chargesTextBox.requestFocusInWindow();
                }
            });
            shipmentModeText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    discountText.requestFocusInWindow();
                }
            });
           discountText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(isDuplicateColumnsVisible==true)
                        discountText1.requestFocus();
                    else
                      totDiscountAddButton.requestFocusInWindow();
                }
            });
            discountText1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    totDiscountAddButton.requestFocusInWindow();
                }
            });
            totDiscountAddButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    saveButton.requestFocusInWindow();
                }
            });

        } 
            catch (Exception e) {
            e.printStackTrace();
        }

    }
     
     /** This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        buttonGroup1 = new javax.swing.ButtonGroup();
        bottomPanel = new javax.swing.JPanel();
        itemInfoPanel = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        itemDescriptionText = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        categoryText = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        sizePanel = new javax.swing.JPanel();
        shirtRadioButton = new javax.swing.JRadioButton();
        pantRadioButton = new javax.swing.JRadioButton();
        SLabel = new javax.swing.JLabel();
        SPanel = new javax.swing.JPanel();
        STextField = new javax.swing.JTextField();
        STextField1 = new javax.swing.JTextField();
        MLabel = new javax.swing.JLabel();
        MPanel = new javax.swing.JPanel();
        MTextField = new javax.swing.JTextField();
        MTextField1 = new javax.swing.JTextField();
        LLabel = new javax.swing.JLabel();
        LPanel = new javax.swing.JPanel();
        LTextField = new javax.swing.JTextField();
        LTextField1 = new javax.swing.JTextField();
        XLLabel = new javax.swing.JLabel();
        XLPanel = new javax.swing.JPanel();
        XLTextField = new javax.swing.JTextField();
        XLTextField1 = new javax.swing.JTextField();
        XXLLabel = new javax.swing.JLabel();
        XXLPanel = new javax.swing.JPanel();
        XXLTextField = new javax.swing.JTextField();
        XXLTextField1 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        godownLabel = new javax.swing.JLabel();
        godownText = new javax.swing.JTextField();
        buttonPanel = new javax.swing.JPanel();
        addItemButton = new javax.swing.JButton();
        itemRemoveButton = new javax.swing.JButton();
        qtyPanel = new javax.swing.JPanel();
        itemQtyText = new javax.swing.JTextField();
        itemQtyText1 = new javax.swing.JTextField();
        unitPricePanel = new javax.swing.JPanel();
        itemUnitPriceText = new javax.swing.JFormattedTextField();
        itemUnitPriceText1 = new javax.swing.JFormattedTextField();
        sellingPricePanel = new javax.swing.JPanel();
        sellingPriceText = new javax.swing.JFormattedTextField();
        chargesPanel = new javax.swing.JPanel();
        chargesAddButton = new javax.swing.JButton();
        chargesMinusButton = new javax.swing.JButton();
        chargesPanel1 = new javax.swing.JPanel();
        chargesText = new javax.swing.JFormattedTextField();
        chargesText1 = new javax.swing.JFormattedTextField();
        chargesTextBox = new javax.swing.JTextField();
        InvoiceDetailsPanel = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        deliveryLabel = new javax.swing.JLabel();
        salesTaxText = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        addressTextArea = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        invoiceNumberText = new javax.swing.JTextField();
        supplierText = new javax.swing.JTextField();
        billDateFormattedText = new com.toedter.calendar.JDateChooser();
        deliveryDateFormattedText = new javax.swing.JTextField();
        taxPanel = new javax.swing.JPanel();
        taxAddButton = new javax.swing.JButton();
        taxText = new javax.swing.JTextField();
        shipmentModePanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        shipmentModeText = new javax.swing.JTextField();
        grandTotalPanel1 = new javax.swing.JPanel();
        grandTotalLabel = new javax.swing.JLabel();
        shipmentModePanel1 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        totDiscountAddButton = new javax.swing.JButton();
        netAmountLabel = new javax.swing.JLabel();
        discountPanel = new javax.swing.JPanel();
        discountText = new javax.swing.JFormattedTextField();
        discountText1 = new javax.swing.JFormattedTextField();
        jPanel1 = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        printBarcodeButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        tableBorderPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        purchaseItemTable = new javax.swing.JTable();

        jTextField1.setText("jTextField1");

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        bottomPanel.setOpaque(false);

        itemInfoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Item Info*"));
        itemInfoPanel.setOpaque(false);
        itemInfoPanel.setPreferredSize(new java.awt.Dimension(382, 179));

        jLabel10.setText("Unit Price");

        jLabel9.setText("Quantity");

        itemDescriptionText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemDescriptionTextKeyPressed(evt);
            }
        });

        jLabel7.setText("Category");

        categoryText.setBackground(new java.awt.Color(226, 247, 255));
        categoryText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                categoryTextKeyPressed(evt);
            }
        });

        jLabel8.setText("Description");

        sizePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Size"));
        sizePanel.setOpaque(false);
        sizePanel.setLayout(new java.awt.GridLayout(6, 2));

        buttonGroup1.add(shirtRadioButton);
        shirtRadioButton.setSelected(true);
        shirtRadioButton.setText("Shirt");
        shirtRadioButton.setOpaque(false);
        shirtRadioButton.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                shirtRadioButtonStateChanged(evt);
            }
        });
        sizePanel.add(shirtRadioButton);

        buttonGroup1.add(pantRadioButton);
        pantRadioButton.setText("Pant");
        pantRadioButton.setOpaque(false);
        pantRadioButton.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                pantRadioButtonStateChanged(evt);
            }
        });
        sizePanel.add(pantRadioButton);

        SLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        SLabel.setText("S");
        sizePanel.add(SLabel);

        SPanel.setLayout(new java.awt.GridLayout(1, 2));

        STextField.setText("0");
        STextField.setToolTipText("");
        STextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                STextFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                STextFieldFocusLost(evt);
            }
        });
        STextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                STextFieldKeyTyped(evt);
            }
        });
        SPanel.add(STextField);

        STextField1.setText("0");
        STextField1.setToolTipText("");
        STextField1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                STextField1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                STextField1FocusLost(evt);
            }
        });
        STextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                STextField1KeyTyped(evt);
            }
        });
        SPanel.add(STextField1);

        sizePanel.add(SPanel);

        MLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        MLabel.setText("M");
        sizePanel.add(MLabel);

        MPanel.setLayout(new java.awt.GridLayout(1, 2));

        MTextField.setText("0");
        MTextField.setToolTipText("");
        MTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                MTextFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                MTextFieldFocusLost(evt);
            }
        });
        MTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                MTextFieldKeyTyped(evt);
            }
        });
        MPanel.add(MTextField);

        MTextField1.setText("0");
        MTextField1.setToolTipText("");
        MTextField1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                MTextField1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                MTextField1FocusLost(evt);
            }
        });
        MTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                MTextField1KeyTyped(evt);
            }
        });
        MPanel.add(MTextField1);

        sizePanel.add(MPanel);

        LLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LLabel.setText("L");
        sizePanel.add(LLabel);

        LPanel.setLayout(new java.awt.GridLayout(1, 2));

        LTextField.setText("0");
        LTextField.setToolTipText("");
        LTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                LTextFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                LTextFieldFocusLost(evt);
            }
        });
        LTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                LTextFieldKeyTyped(evt);
            }
        });
        LPanel.add(LTextField);

        LTextField1.setText("0");
        LTextField1.setToolTipText("");
        LTextField1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                LTextField1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                LTextField1FocusLost(evt);
            }
        });
        LTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                LTextField1KeyTyped(evt);
            }
        });
        LPanel.add(LTextField1);

        sizePanel.add(LPanel);

        XLLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        XLLabel.setText("XL");
        sizePanel.add(XLLabel);

        XLPanel.setLayout(new java.awt.GridLayout(1, 2));

        XLTextField.setText("0");
        XLTextField.setToolTipText("");
        XLTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                XLTextFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                XLTextFieldFocusLost(evt);
            }
        });
        XLTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                XLTextFieldKeyTyped(evt);
            }
        });
        XLPanel.add(XLTextField);

        XLTextField1.setText("0");
        XLTextField1.setToolTipText("");
        XLTextField1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                XLTextField1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                XLTextField1FocusLost(evt);
            }
        });
        XLTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                XLTextField1KeyTyped(evt);
            }
        });
        XLPanel.add(XLTextField1);

        sizePanel.add(XLPanel);

        XXLLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        XXLLabel.setText("XXL");
        sizePanel.add(XXLLabel);

        XXLPanel.setLayout(new java.awt.GridLayout(1, 2));

        XXLTextField.setText("0");
        XXLTextField.setToolTipText("");
        XXLTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                XXLTextFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                XXLTextFieldFocusLost(evt);
            }
        });
        XXLTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                XXLTextFieldKeyTyped(evt);
            }
        });
        XXLPanel.add(XXLTextField);

        XXLTextField1.setText("0");
        XXLTextField1.setToolTipText("");
        XXLTextField1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                XXLTextField1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                XXLTextField1FocusLost(evt);
            }
        });
        XXLTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                XXLTextField1KeyTyped(evt);
            }
        });
        XXLPanel.add(XXLTextField1);

        sizePanel.add(XXLPanel);

        jLabel4.setText("Selling Price");

        godownLabel.setText("Godown");

        godownText.setBackground(new java.awt.Color(226, 247, 255));
        godownText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        godownText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                godownTextFocusGained(evt);
            }
        });
        godownText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                godownTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                godownTextKeyTyped(evt);
            }
        });

        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new java.awt.GridLayout(1, 0));

        addItemButton.setText("+");
        addItemButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addItemButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(addItemButton);

        itemRemoveButton.setText("-");
        itemRemoveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemRemoveButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(itemRemoveButton);

        qtyPanel.setLayout(new java.awt.GridLayout(1, 0));

        itemQtyText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemQtyText.setText("0");
        itemQtyText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemQtyTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemQtyTextFocusLost(evt);
            }
        });
        itemQtyText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemQtyTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemQtyTextKeyTyped(evt);
            }
        });
        qtyPanel.add(itemQtyText);

        itemQtyText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemQtyText1.setText("0");
        itemQtyText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemQtyText1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemQtyText1FocusLost(evt);
            }
        });
        itemQtyText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemQtyText1KeyTyped(evt);
            }
        });
        qtyPanel.add(itemQtyText1);

        unitPricePanel.setLayout(new java.awt.GridLayout(1, 0));

        itemUnitPriceText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        itemUnitPriceText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemUnitPriceText.setText("0.00");
        itemUnitPriceText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemUnitPriceTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemUnitPriceTextFocusLost(evt);
            }
        });
        itemUnitPriceText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemUnitPriceTextKeyTyped(evt);
            }
        });
        unitPricePanel.add(itemUnitPriceText);

        itemUnitPriceText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        itemUnitPriceText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemUnitPriceText1.setText("0.00");
        itemUnitPriceText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemUnitPriceText1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                itemUnitPriceText1FocusLost(evt);
            }
        });
        itemUnitPriceText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemUnitPriceText1KeyTyped(evt);
            }
        });
        unitPricePanel.add(itemUnitPriceText1);

        sellingPricePanel.setLayout(new java.awt.GridLayout(1, 0));

        sellingPriceText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        sellingPriceText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        sellingPriceText.setText("0.00");
        sellingPriceText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                sellingPriceTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                sellingPriceTextFocusLost(evt);
            }
        });
        sellingPriceText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                sellingPriceTextKeyTyped(evt);
            }
        });
        sellingPricePanel.add(sellingPriceText);

        javax.swing.GroupLayout itemInfoPanelLayout = new javax.swing.GroupLayout(itemInfoPanel);
        itemInfoPanel.setLayout(itemInfoPanelLayout);
        itemInfoPanelLayout.setHorizontalGroup(
            itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(godownLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(itemDescriptionText, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(categoryText)
                    .addComponent(godownText)
                    .addComponent(qtyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(unitPricePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
                    .addComponent(sellingPricePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0)
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(sizePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        itemInfoPanelLayout.setVerticalGroup(
            itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(itemInfoPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(categoryText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(itemInfoPanelLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sizePanel, javax.swing.GroupLayout.PREFERRED_SIZE, 136, Short.MAX_VALUE)
                    .addGroup(itemInfoPanelLayout.createSequentialGroup()
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(itemDescriptionText)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(qtyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(7, 7, 7)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                                .addComponent(unitPricePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(5, 5, 5)
                                .addComponent(sellingPricePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(godownLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(godownText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        chargesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Charges"));
        chargesPanel.setOpaque(false);

        chargesAddButton.setText("+");
        chargesAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesAddButtonActionPerformed(evt);
            }
        });

        chargesMinusButton.setText("-");
        chargesMinusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesMinusButtonActionPerformed(evt);
            }
        });

        chargesPanel1.setLayout(new java.awt.GridLayout(1, 0));

        chargesText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        chargesText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        chargesText.setText("0.00");
        chargesText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesTextFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                chargesTextFocusLost(evt);
            }
        });
        chargesText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                chargesTextKeyTyped(evt);
            }
        });
        chargesPanel1.add(chargesText);

        chargesText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        chargesText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        chargesText1.setText("0.00");
        chargesText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesText1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                chargesText1FocusLost(evt);
            }
        });
        chargesText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                chargesText1KeyTyped(evt);
            }
        });
        chargesPanel1.add(chargesText1);

        chargesTextBox.setBackground(new java.awt.Color(226, 247, 255));
        chargesTextBox.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesTextBoxFocusGained(evt);
            }
        });
        chargesTextBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                chargesTextBoxKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout chargesPanelLayout = new javax.swing.GroupLayout(chargesPanel);
        chargesPanel.setLayout(chargesPanelLayout);
        chargesPanelLayout.setHorizontalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, chargesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chargesPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE)
                    .addComponent(chargesTextBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        chargesPanelLayout.setVerticalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chargesPanelLayout.createSequentialGroup()
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chargesTextBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(chargesPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        InvoiceDetailsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Invoice Details"));
        InvoiceDetailsPanel.setOpaque(false);

        jLabel6.setText("Bill Date*");

        jLabel5.setText("Invoice Number*");

        jLabel13.setText("Supplier*");

        deliveryLabel.setText("Delivery Date");

        jLabel15.setText("Sales Tax No: ");

        addressTextArea.setColumns(20);
        addressTextArea.setLineWrap(true);
        addressTextArea.setRows(5);
        jScrollPane3.setViewportView(addressTextArea);

        jLabel1.setText("Address :");

        invoiceNumberText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                invoiceNumberTextKeyPressed(evt);
            }
        });

        supplierText.setBackground(new java.awt.Color(226, 247, 255));
        supplierText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supplierTextActionPerformed(evt);
            }
        });
        supplierText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                supplierTextKeyPressed(evt);
            }
        });

        billDateFormattedText.setDateFormatString("dd/MM/yyyy");

        javax.swing.GroupLayout InvoiceDetailsPanelLayout = new javax.swing.GroupLayout(InvoiceDetailsPanel);
        InvoiceDetailsPanel.setLayout(InvoiceDetailsPanelLayout);
        InvoiceDetailsPanelLayout.setHorizontalGroup(
            InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                        .addComponent(deliveryLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
                                        .addGap(4, 4, 4)))
                                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(supplierText, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(deliveryDateFormattedText)
                                    .addComponent(billDateFormattedText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(invoiceNumberText)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(salesTaxText))
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3)))
                .addContainerGap())
        );
        InvoiceDetailsPanelLayout.setVerticalGroup(
            InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(billDateFormattedText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(12, 12, 12)
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(invoiceNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addGap(16, 16, 16)
                                .addComponent(jLabel13))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, InvoiceDetailsPanelLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(supplierText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(2, 2, 2))))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(9, 9, 9)
                .addGroup(InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(salesTaxText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, InvoiceDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(deliveryLabel)
                        .addComponent(deliveryDateFormattedText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        taxPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Tax"));
        taxPanel.setOpaque(false);

        taxAddButton.setText("+");
        taxAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                taxAddButtonActionPerformed(evt);
            }
        });

        taxText.setBackground(new java.awt.Color(226, 247, 255));
        taxText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                taxTextFocusGained(evt);
            }
        });
        taxText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                taxTextKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout taxPanelLayout = new javax.swing.GroupLayout(taxPanel);
        taxPanel.setLayout(taxPanelLayout);
        taxPanelLayout.setHorizontalGroup(
            taxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(taxPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(taxText)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(taxAddButton)
                .addContainerGap())
        );
        taxPanelLayout.setVerticalGroup(
            taxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(taxPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(taxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(taxAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(taxText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        shipmentModePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        shipmentModePanel.setOpaque(false);

        jLabel3.setText("Shipment Mode");

        shipmentModeText.setBackground(new java.awt.Color(226, 247, 255));
        shipmentModeText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                shipmentModeTextKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout shipmentModePanelLayout = new javax.swing.GroupLayout(shipmentModePanel);
        shipmentModePanel.setLayout(shipmentModePanelLayout);
        shipmentModePanelLayout.setHorizontalGroup(
            shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, shipmentModePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(shipmentModeText)
                .addContainerGap())
        );
        shipmentModePanelLayout.setVerticalGroup(
            shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(shipmentModePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(shipmentModeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        grandTotalPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Grand Total", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N
        grandTotalPanel1.setOpaque(false);
        grandTotalPanel1.setLayout(new java.awt.BorderLayout());

        grandTotalLabel.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        grandTotalLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        grandTotalLabel.setText("0.00");
        grandTotalLabel.setFocusable(false);
        grandTotalLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        grandTotalPanel1.add(grandTotalLabel, java.awt.BorderLayout.CENTER);

        shipmentModePanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        shipmentModePanel1.setOpaque(false);

        jLabel17.setText("Discount");

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel18.setText("Net Amount");

        totDiscountAddButton.setText("+");
        totDiscountAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totDiscountAddButtonActionPerformed(evt);
            }
        });

        netAmountLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        discountPanel.setLayout(new java.awt.GridLayout(1, 0));

        discountText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        discountText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        discountText.setText("0.00");
        discountText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                discountTextFocusGained(evt);
            }
        });
        discountText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                discountTextKeyTyped(evt);
            }
        });
        discountPanel.add(discountText);

        discountText1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        discountText1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        discountText1.setText("0.00");
        discountText1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                discountText1FocusGained(evt);
            }
        });
        discountText1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                discountText1KeyTyped(evt);
            }
        });
        discountPanel.add(discountText1);

        javax.swing.GroupLayout shipmentModePanel1Layout = new javax.swing.GroupLayout(shipmentModePanel1);
        shipmentModePanel1.setLayout(shipmentModePanel1Layout);
        shipmentModePanel1Layout.setHorizontalGroup(
            shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(shipmentModePanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(shipmentModePanel1Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(netAmountLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(shipmentModePanel1Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(totDiscountAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(discountPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
                .addContainerGap())
        );
        shipmentModePanel1Layout.setVerticalGroup(
            shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, shipmentModePanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE)
                    .addComponent(netAmountLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(shipmentModePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(discountPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(totDiscountAddButton)
                .addContainerGap())
        );

        jPanel1.setOpaque(false);
        jPanel1.setLayout(new java.awt.GridLayout(1, 0));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("<html><u>N</u>ew</html>");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        jPanel1.add(newButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveButton.setText("<HTML><U>S</U>ave<HTML>");
        saveButton.setToolTipText("");
        saveButton.setOpaque(false);
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        jPanel1.add(saveButton);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/cancel.png"))); // NOI18N
        cancelButton.setText("<html><u>C</u>ancel</html>");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        jPanel1.add(cancelButton);

        printBarcodeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/195-barcode-icon.png"))); // NOI18N
        printBarcodeButton.setText("Print Barcode");
        printBarcodeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printBarcodeButtonActionPerformed(evt);
            }
        });
        jPanel1.add(printBarcodeButton);

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/delete-icon.png"))); // NOI18N
        deleteButton.setText("<html><u>D</u>elete</html>");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        jPanel1.add(deleteButton);

        javax.swing.GroupLayout bottomPanelLayout = new javax.swing.GroupLayout(bottomPanel);
        bottomPanel.setLayout(bottomPanelLayout);
        bottomPanelLayout.setHorizontalGroup(
            bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bottomPanelLayout.createSequentialGroup()
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(InvoiceDetailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(itemInfoPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(shipmentModePanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chargesPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(taxPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(shipmentModePanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(grandTotalPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(11, 11, 11))
        );
        bottomPanelLayout.setVerticalGroup(
            bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bottomPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(bottomPanelLayout.createSequentialGroup()
                        .addComponent(InvoiceDetailsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(itemInfoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bottomPanelLayout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(shipmentModePanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(grandTotalPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(bottomPanelLayout.createSequentialGroup()
                        .addComponent(taxPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chargesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(shipmentModePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, 0))
        );

        add(bottomPanel, java.awt.BorderLayout.PAGE_END);

        tableBorderPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Purchase Invoice"));
        tableBorderPanel.setOpaque(false);

        jScrollPane1.setName(""); // NOI18N
        jScrollPane1.setOpaque(false);

        purchaseItemTable.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        purchaseItemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        purchaseItemTable.setOpaque(false);
        purchaseItemTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                purchaseItemTableMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                purchaseItemTableMouseEntered(evt);
            }
        });
        jScrollPane1.setViewportView(purchaseItemTable);

        javax.swing.GroupLayout tableBorderPanelLayout = new javax.swing.GroupLayout(tableBorderPanel);
        tableBorderPanel.setLayout(tableBorderPanelLayout);
        tableBorderPanelLayout.setHorizontalGroup(
            tableBorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tableBorderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1289, Short.MAX_VALUE)
                .addContainerGap())
        );
        tableBorderPanelLayout.setVerticalGroup(
            tableBorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tableBorderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE))
        );

        add(tableBorderPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void categoryTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_categoryTextKeyPressed
        try {
            ItemService itemService = new ItemService();
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                String categoryName = categoryText.getText().trim();
                List<StockGroup> stockGroups = itemService.populatePurchasePopupTableByCategory(categoryName, popupTableDialog, purchaseItemList, currency, isDuplicateColumnsVisible, purchaseItemListHistory);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();

                itemQtyText.requestFocusInWindow();

                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    stockGroup = stockGroups.get(index);
                    categoryText.setText(stockGroup.getName());
                   // itemDescriptionText.setText(item.getDescription());
                   
                   // godownText.setText(item.getGodown().getName());
                    //if (isDuplicateColumnsVisible) {
                       // itemQty = itemDAO.findTotItemQty1(item.getItemCode());
                      //  availableQtyLabel.setText(Double.toString(item.getAvailableQty() + item.getAvailableQty1()));
                    //} else {
                     //   itemQty = itemDAO.findTotItemQty(item.getItemCode());
                     //   availableQtyLabel.setText(Double.toString(item.getAvailableQty()));
                    //}
                    //totavailableQtyLabel.setText(itemQty + "");
                } else {
                    MsgBox.warning("No Item Selected.");
                    itemQtyText.requestFocusInWindow();
                }
            }
        } catch (Exception e) {
            log.error("productIdTextKeyPressed:", e);
        }
    }//GEN-LAST:event_categoryTextKeyPressed

    void fillPurchaseItemTable() {
        try {
            //boolean flag = false;

            purchaseItemTableModel.setRowCount(0);
            //BigDecimal grandTotal = new BigDecimal(0.00) ;
            //grandTotal.setScale(1);
            subTotal = 0.00;
            netTotal = 0.00;

            subTotal1 = 0.00;
            netTotal1 = 0.00;

            int slNo = 1;
            String currencySymbol = (null == currency) ? "" : currency.getSymbol();

            //{"Sr. No.", "Item Code","Description", "Unit Price", "Qty.", "Total Amount"}
            for (PurchaseItem pItem : purchaseItemList) {
                double itemTotal = 0;
                double itemTotal1 = 0;

                if (purchase == null) {
                    itemTotal = ((pItem.getUnitPrice() / currency.getRate()) * pItem.getQuantity());
                    itemTotal1 = ((pItem.getUnitPrice1() / currency.getRate()) * pItem.getQuantity1());
                    if (isDuplicateColumnsVisible) {
                        //itemTotal1 = ((pItem.getUnitPrice1() / currency.getRate()) * pItem.getQuantity1());
                        if (itemTotal1 > 0 || itemTotal > 0) { //only add to jtable if tota is >0
                            purchaseItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()),  pItem.getQuantity(), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / currency.getRate()), pItem.getQuantity1(), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice1() / currency.getRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal + itemTotal1)});
                        }
                    } else {
                        if (itemTotal > 0) {
                            purchaseItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()),  pItem.getQuantity(), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / currency.getRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal)});
                        }
                    }
                } else {
                    itemTotal = ((pItem.getUnitPrice() / currency.getRate()) * pItem.getQuantity());
                    itemTotal1 = ((pItem.getUnitPrice1() / currency.getRate()) * pItem.getQuantity1());
                    if (isDuplicateColumnsVisible) {
                        // itemTotal1 = ((pItem.getUnitPrice1() / currency.getRate()) * pItem.getQuantity1());
                        if (itemTotal1 > 0 || itemTotal > 0) {
                            purchaseItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getQuantity(), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / currency.getRate()), pItem.getQuantity1(), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice1() / currency.getRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal + itemTotal1)});
                        }
                    } else {
                        if (itemTotal > 0) {
                            purchaseItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), pItem.getQuantity(), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getUnitPrice() / currency.getRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal)});
                        }
                    }

                }
                subTotal += itemTotal;
                netTotal = subTotal;

                subTotal1 += itemTotal1;
                netTotal1 = subTotal1;
            }

            if (subTotal > 0 || subTotal1 > 0) {
                for (PurchaseTax purchaseTax : purchaseTaxList) {
                    if (isDuplicateColumnsVisible) {
                        purchaseItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseTax.getTax().getName(),  "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(purchaseTax.getAmount() / currency.getRate())});
                    } else {
                        purchaseItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseTax.getTax().getName(),  "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(purchaseTax.getAmount() / currency.getRate())});
                    }
                    netTotal += (purchaseTax.getAmount());
                }
                for (PurchaseCharge purchaseCharge : purchaseChargesList) {
                    if (isDuplicateColumnsVisible) {
                        purchaseItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseCharge.getCharges().getName(),  "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString((purchaseCharge.getAmount() + purchaseCharge.getAmount1())/currency.getRate())});
                    } else {
                        purchaseItemTableModel.addRow(new Object[]{"", "", "\t" + purchaseCharge.getCharges().getName(),  "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString((purchaseCharge.getAmount())/currency.getRate())});
                    }

                    netTotal += (purchaseCharge.getAmount());
                    netTotal1 += (purchaseCharge.getAmount1());
                }
            }
            grandTotal = netTotal - totDiscount;
            grandTotal1 = netTotal1 - totDiscount1;
            if (totDiscount > 0 || totDiscount1 > 0) {
                if (isDuplicateColumnsVisible) {
                    purchaseItemTableModel.addRow(new Object[]{"", "", "Discount", "", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString((totDiscount + totDiscount1)/currency.getRate())});
                } else {
                    purchaseItemTableModel.addRow(new Object[]{"", "", "Discount", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString((totDiscount)/currency.getRate())});
                }
            }
            if (isDuplicateColumnsVisible) {
                netAmountLabel.setText(currencySymbol + " " + commonService.formatIntoCurrencyAsString(netTotal + netTotal1));
                grandTotalLabel.setText(currencySymbol + "" + commonService.formatIntoCurrencyAsString(grandTotal + grandTotal1));
            } else {
                netAmountLabel.setText(currencySymbol + " " + commonService.formatIntoCurrencyAsString(netTotal));
                grandTotalLabel.setText(currencySymbol + "" + commonService.formatIntoCurrencyAsString(grandTotal));
            }

            //clear fields
            CommonService.clearCurrencyFields(new JTextField[]{itemUnitPriceText, itemUnitPriceText1,sellingPriceText});
            CommonService.clearNumberFields(new JTextField[]{itemQtyText, itemQtyText1,STextField,MTextField,LTextField,XLTextField,XXLTextField,STextField1,MTextField1,LTextField1,XLTextField1,XXLTextField1});
            CommonService.clearTextFields(new JTextField[]{categoryText, itemDescriptionText});
           // totavailableQtyLabel.setText("");
           // availableQtyLabel.setText("");

        } catch (Exception e) {
            log.error("fillPurchaseItemTable:", e);
        }

    }

    private void addItemButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addItemButtonActionPerformed

        try {
            if (CommonService.stringValidator(new String[]{categoryText.getText(), itemDescriptionText.getText()})&&stockGroup!=null) {

                if (Double.parseDouble(itemQtyText.getText()) > 0 || Double.parseDouble(itemQtyText1.getText()) > 0) {
                    generateItemAndPurchaseItem();
                    fillPurchaseItemTable();
                }
            } else {
                MsgBox.warning("No Item Selected.");
            }
        } catch (Exception e) {
            log.error("addItemButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_addItemButtonActionPerformed

    private void itemRemoveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemRemoveButtonActionPerformed
        if (null != purchaseItem) {
            purchaseItemRemovedList.add(purchaseItem);
            purchaseItemList.remove(purchaseItem);
            fillPurchaseItemTable();
            purchaseItem = null;
        }
    }//GEN-LAST:event_itemRemoveButtonActionPerformed

    private void invoiceNumberTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_invoiceNumberTextKeyPressed

    }//GEN-LAST:event_invoiceNumberTextKeyPressed

    private void itemQtyTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyTextFocusGained
        itemQtyText.selectAll();
    }//GEN-LAST:event_itemQtyTextFocusGained

    private void itemQtyTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyTextKeyPressed

    }//GEN-LAST:event_itemQtyTextKeyPressed

    private void itemQtyTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyTextKeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_itemQtyTextKeyTyped

    private void itemUnitPriceTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemUnitPriceTextFocusGained
        itemUnitPriceText.selectAll();
    }//GEN-LAST:event_itemUnitPriceTextFocusGained

    private void itemUnitPriceTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemUnitPriceTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_itemUnitPriceTextKeyTyped

    private void chargesTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesTextFocusGained
        chargesText.selectAll();
    }//GEN-LAST:event_chargesTextFocusGained

    private void chargesTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chargesTextKeyTyped
       CommonService.currencyValidator(evt);
    }//GEN-LAST:event_chargesTextKeyTyped

    private void chargesAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesAddButtonActionPerformed

//        try {
//            Object chargesItem = chargesComboBox.getSelectedItem();
//            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();
//            double chargeAmt = Double.parseDouble(chargesText.getText());
//            double chargeAmt1 = Double.parseDouble(chargesText1.getText());
//            if ((Double.parseDouble(chargesText.getText()) > 0 || Double.parseDouble(chargesText1.getText()) > 0) && !charges.getName().equalsIgnoreCase("N/A")) {
//                PurchaseCharge purchaseCharge = new PurchaseCharge();
//
//                Iterator<PurchaseCharge> iter = purchaseChargesList.iterator();
//                while (iter.hasNext()) {
//                    if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
//                        iter.remove();
//                    }
//                }
//
//                purchaseCharge.setCharges(charges);
//                purchaseCharge.setAmount(chargeAmt);
//                purchaseCharge.setAmount1(chargeAmt1);
//                purchaseChargesList.add(purchaseCharge);
//                chargesComboBox.setSelectedIndex(0);
//                chargesText.setText("0.00");
//                chargesText1.setText("0.00");
//                fillPurchaseItemTable();
//            }
//
//        } catch (Exception e) {
//            log.error("chargesAddButtonActionPerformed:", e);
//        }
        try {
            //if(chargesTextBox.getText()==null)
            if(purchaseItemList.size()>0)
            {
            chargeAmt = Double.parseDouble(chargesText.getText())*currency.getRate();
            chargeAmt1 = Double.parseDouble(chargesText1.getText())*currency.getRate();
            if ((Double.parseDouble(chargesText.getText()) > 0 || Double.parseDouble(chargesText1.getText()) > 0) && !charges.getName().equalsIgnoreCase("N/A")) {
                PurchaseCharge purchaseCharge = new PurchaseCharge();
                Iterator<PurchaseCharge> iter = purchaseChargesList.iterator();
                while (iter.hasNext()) {
                    if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                        iter.remove();
                    }
                }
                purchaseCharge.setCharges(charges);
                purchaseCharge.setAmount(chargeAmt);
                purchaseCharge.setAmount1(chargeAmt1);
                purchaseCharge.setCompany(GlobalProperty.getCompany());
                purchaseChargesList.add(purchaseCharge);
                fillPurchaseItemTable();
            }
            chargesText.setText("0.00");
            chargesText1.setText("0.00");
           }
            else
            {
            MsgBox.warning("Purchase Item List Empty......!!"); 
            chargesText.setText("0.00");
            chargesText1.setText("0.00");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("chargesAddButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesAddButtonActionPerformed

    private void itemDescriptionTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemDescriptionTextKeyPressed
        try {
//            ItemService itemService = new ItemService();
//            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
//            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
//                String itemName = itemDescriptionText.getText().trim();
//                List<Item> items = itemService.populatePurchasePopupTableByItemName(itemName, popupTableDialog, purchaseItemList, currency, isDuplicateColumnsVisible, purchaseItemListHistory);
//                popupTableDialog.setVisible(true);
//                ArrayList selectedRow = popupTableDialog.getSelectedRow();
//
//                itemQtyText.requestFocusInWindow();
//
//                if (null != selectedRow) {
//                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
//                    item = items.get(index);
//                    categoryText.setText(item.getItemCode());
//                    itemDescriptionText.setText(item.getDescription());
//                    itemCategoryText.setText(Double.toString(item.getWeight()));
//                    godownText.setText(item.getGodown().getName());
//                    if (isDuplicateColumnsVisible) {
//                        itemQty = itemDAO.findTotItemQty1(item.getItemCode());
//                        availableQtyLabel.setText(Double.toString(item.getAvailableQty() + item.getAvailableQty1()));
//                    } else {
//                        itemQty = itemDAO.findTotItemQty(item.getItemCode());
//                        availableQtyLabel.setText(Double.toString(item.getAvailableQty()));
//                    }
//                    totavailableQtyLabel.setText(itemQty + "");
//                } else {
//                    MsgBox.warning("No Item Selected.");
//                    itemDescriptionText.requestFocusInWindow();
//                }
//            }
        } catch (Exception e) {
            log.error("itemNameTextKeyPressed:", e);
        }
    }//GEN-LAST:event_itemDescriptionTextKeyPressed

    private void purchaseItemTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purchaseItemTableMouseClicked
        try {
            ArrayList row = CommonService.getTableRowData(purchaseItemTable);
            String itemCode = row.get(1) + "";

            if (itemCode.trim().length() > 0) {
                purchaseItem = purchaseItemList.get(Integer.parseInt(row.get(0) + "") - 1);
                item = purchaseItem.getItem();
                categoryText.setText(purchaseItem.getItem().getStockGroup().getName());
                itemDescriptionText.setText(purchaseItem.getItem().getDescription());
                itemQtyText.setText(purchaseItem.getQuantity() + "");
                itemQtyText1.setText(purchaseItem.getQuantity1() + "");
                sellingPriceText.setText(purchaseItem.getItem().getSellingPrice().toString());
                if (purchase == null) {
                    itemUnitPriceText.setText(purchaseItem.getUnitPrice() / currency.getRate() + "");
                    itemUnitPriceText1.setText(purchaseItem.getUnitPrice1() / currency.getRate() + "");
                } else {
                    itemUnitPriceText.setText(purchaseItem.getUnitPrice() / purchase.getCurrentCurrencyRate() + "");
                    itemUnitPriceText1.setText(purchaseItem.getUnitPrice1() / purchase.getCurrentCurrencyRate() + "");
                }
                //totavailableQtyLabel.setText(purchaseItem.getItem().getAvailableQty() + "");
                //showing total available qty from all godown
    //            if (isDuplicateColumnsVisible) {
  //                  itemQty = itemDAO.findTotItemQty1(purchaseItem.getItem().getItemCode());
                    //availableQtyLabel.setText(Double.toString(purchaseItem.getItem().getAvailableQty() + purchaseItem.getItem().getAvailableQty1()));
      //          } else {
//                    itemQty = itemDAO.findTotItemQty(purchaseItem.getItem().getItemCode());
                    //availableQtyLabel.setText(Double.toString(purchaseItem.getItem().getAvailableQty()));
        //        }
               // totavailableQtyLabel.setText(itemQty + "");
                //showing total available qty 

                godownText.setText(purchaseItem.getItem().getGodown().getName());

            } else {
                //this part will treat as charges,discount or tax
//                String itemDesc = row.get(2) + "";
//                itemDesc = itemDesc.trim();
//                int count = taxComboBox.getItemCount();
//
//                if (itemDesc.equalsIgnoreCase("discount")) {
//                    totDiscountAddButton.setText("-");
//                }
//
//                for (int i = 0; i < count; i++) {
//                    ComboKeyValue ckv = (ComboKeyValue) taxComboBox.getItemAt(i);
//                    if (ckv.getKey().trim().equalsIgnoreCase(itemDesc)) {
//                        taxComboBox.setSelectedItem(ckv);
//                        taxAddButton.setText("-");
//                    }
//                }
//
//                count = chargesComboBox.getItemCount();
//                for (int i = 0; i < count; i++) {
//                    ComboKeyValue ckv = (ComboKeyValue) chargesComboBox.getItemAt(i);
//                    if (ckv.getKey().trim().equalsIgnoreCase(itemDesc)) {
//                        chargesComboBox.setSelectedItem(ckv);
//                        for (PurchaseCharge purchaseCharge : purchaseChargesList) {
//                            if (purchaseCharge.getCharges().getName().equalsIgnoreCase(itemDesc)) {
//                                chargesText.setText(purchaseCharge.getAmount() + "");
//                                chargesText1.setText(purchaseCharge.getAmount1() + "");
//                                break;
//                            }
//                        }
//                    }
//                }
                 String itemDesc = row.get(2) + "";
                 itemDesc = itemDesc.trim();
                 ItemService itemService = new ItemService();
                 PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
                 if (itemDesc.equalsIgnoreCase("discount")) {
                    totDiscountAddButton.setText("-");
                 }
                if(taxList!=null)
                 {
                     int i=0;
                     Iterator<Tax> iter=taxList.iterator();
                     while(iter.hasNext()&& i<=(taxCount-1))
                     {
                         tax=taxList.get(i);
                         //System.out.println(""+tax.getName());
                         if(tax.getName().equalsIgnoreCase(itemDesc))
                         {
                             taxText.setText(tax.getName());
                             taxAddButton.setText("-");
                             break;
                         }
                         i++;
                     }
                 } 
                
                  if(purchaseTaxList!=null)
                 {
                     int i=0;
                     Iterator<PurchaseTax> iter=purchaseTaxList.iterator();
                     while(iter.hasNext()&& i<=(purchaseTaxList.size()-1))
                     {
                         purchaseTax=purchaseTaxList.get(i);
                         //System.out.println(""+tax.getName());
                         if(purchaseTax.getTax().getName().equalsIgnoreCase(itemDesc))
                         {
                             taxText.setText(purchaseTax.getTax().getName());
                             taxAddButton.setText("-");
                             break;
                         }
                         i++;
                     }
                 } 
                 
                 if(chargesList!=null)
                 {
               
                 int j=0;
                 Iterator<Charges> ite=chargesList.iterator();
                 while(ite.hasNext()&& j<=1)
                 {
                    charges=chargesList.get(j);
                   
                     if(charges.getName().equalsIgnoreCase(itemDesc))
                     {
                       chargesTextBox.setText(charges.getName());
                       chargesText.setValue(chargeAmt);  
                       break;
                     }
                     j++;
                 }
                 }
                 
                  if(purchaseChargesList!=null)
                 {
                     
                // purchaseCharge=new PurchaseCharge();
                 int j=0;
                 Iterator<PurchaseCharge> ite=purchaseChargesList.iterator();
                 while(ite.hasNext() && j<=purchaseChargesList.size()-1)
                 {
                    purchaseCharge=purchaseChargesList.get(j);
                    // System.out.println(""+charges.getName());
                     if(purchaseCharge.getCharges().getName().equalsIgnoreCase(itemDesc))
                     {
                       chargesTextBox.setText(purchaseCharge.getCharges().getName());
                       chargesText.setText(purchaseCharge.getAmount().toString());
                       chargesText1.setValue(purchaseCharge.getAmount1());  
                     //  chargesText.setText("");
                       break;
                     }
                     j++;
                 }
                 }
            }
            discountText.setText(totDiscount + "");
            discountText1.setText(totDiscount1 + "");

        } catch (Exception e) {
            log.error("purchaseItemTableMouseClicked:", e);
        }
    }//GEN-LAST:event_purchaseItemTableMouseClicked

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        CommonService.removeJpanel(mainForm.getContentPanel());
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void chargesMinusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesMinusButtonActionPerformed
//        try {
//            Object chargesItem = chargesComboBox.getSelectedItem();
//            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();
//
//            Iterator<PurchaseCharge> iter = purchaseChargesList.iterator();
//            while (iter.hasNext()) {
//                if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
//                    iter.remove();
//                }
//            }
//
//            chargesComboBox.setSelectedIndex(0);
//            chargesText.setText("0.00");
//            fillPurchaseItemTable();
//
//        } catch (Exception e) {
//            log.error("chargesMinusButtonActionPerformed:", e);
//        }
         try {
            Iterator<PurchaseCharge> iter = purchaseChargesList.iterator();
            while (iter.hasNext()) {
             if(charges!=null) 
             {
                if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                    iter.remove();
                }
             }
               else if(purchaseCharge.getCharges().getName().equalsIgnoreCase(iter.next().getCharges().getName())) 
                   iter.remove();
            }
            
            chargesTextBox.setText("");
            chargesText.setText("0.00");
            fillPurchaseItemTable();
        } catch (Exception e) {
            log.error("chargesMinusButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesMinusButtonActionPerformed

    private void taxAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_taxAddButtonActionPerformed
//        Object taxItem = taxComboBox.getSelectedItem();
//        Tax tax = (Tax) ((ComboKeyValue) taxItem).getValue();
//
//        if (tax.getTaxRate() > 0 && subTotal > 0 && !tax.getName().equalsIgnoreCase("N/A")) {
//            if (taxAddButton.getText().trim().equalsIgnoreCase("+")) {
//                PurchaseTax purchaseTax = new PurchaseTax();
//                Iterator<PurchaseTax> iter = purchaseTaxList.iterator();
//                while (iter.hasNext()) {
//                    if (tax.getName().equalsIgnoreCase(iter.next().getTax().getName())) {
//                        iter.remove();
//                    }
//                }
//
//                purchaseTax.setTax(tax);
//                purchaseTax.setTaxRate(tax.getTaxRate());
//                double taxTotal = (subTotal * tax.getTaxRate()) / 100;
//                purchaseTax.setAmount(taxTotal);
//                purchaseTaxList.add(purchaseTax);
//                taxComboBox.setSelectedIndex(0);
//            } else {
//                Iterator<PurchaseTax> iter = purchaseTaxList.iterator();
//                while (iter.hasNext()) {
//                    if (tax.getName().equalsIgnoreCase(iter.next().getTax().getName())) {
//                        iter.remove();
//                    }
//                }
//                taxAddButton.setText("+");
//            }
//
//            fillPurchaseItemTable();
//        }
//        taxComboBox.setSelectedIndex(0);
        if(tax!=null)
        {
        if(tax.getTaxRate()==0)
        {
            MsgBox.warning("Please enter tax rate...!!"); 
        }
        if (tax.getTaxRate() > 0 && subTotal > 0 && !tax.getName().equalsIgnoreCase("N/A")&& purchaseItemList.size()>0) {
            if (taxAddButton.getText().trim().equalsIgnoreCase("+")) {
                PurchaseTax purchaseTax = new PurchaseTax();
                Iterator<PurchaseTax> iter = purchaseTaxList.iterator();
                while (iter.hasNext()) {
                    if (tax.getName().equalsIgnoreCase(iter.next().getTax().getName())) {
                        iter.remove();
                    }
                }
                purchaseTax.setTax(tax);
                purchaseTax.setTaxRate(tax.getTaxRate());
                double taxTotal = (subTotal * tax.getTaxRate()) / 100;
                purchaseTax.setAmount(taxTotal);
                purchaseTax.setCompany(GlobalProperty.getCompany());
                purchaseTaxList.add(purchaseTax);
            } else {
                Iterator<PurchaseTax> iter = purchaseTaxList.iterator();
                while (iter.hasNext()) {
                    if (tax.getName().equalsIgnoreCase(iter.next().getTax().getName())) {
                        iter.remove();
                    }
                }
                taxAddButton.setText("+");
            }
            fillPurchaseItemTable();
        }
        else if(purchaseItemList.size()==0)
        {
          MsgBox.warning("Purchase Item List Empty......!!"); 
        }
      }
        else
        {
           Iterator<PurchaseTax> iter = purchaseTaxList.iterator();
            while (iter.hasNext()) {
               if (purchaseTax.getTax().getName().equalsIgnoreCase(iter.next().getTax().getName())) {
                        iter.remove();
                    }
                }    
            fillPurchaseItemTable();
        }
    }//GEN-LAST:event_taxAddButtonActionPerformed

    private void purchaseItemTableMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purchaseItemTableMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_purchaseItemTableMouseEntered

    void savePurchase() {
//        try {
//            if (purchase == null) {
//                check = 0;
//                purchase = new Purchase();
//            }
//
//            purchase.setBillDate(commonService.utilDateToSqlDate(billDateFormattedText.getDate()));
//            purchase.setInvoiceNumber(invoiceNumberText.getText());
//            purchase.setAddress(addressTextArea.getText().trim());
//            purchase.setSupplier(supplierLedger);
//            purchase.setDeliveryDate(deliveryDateFormattedText.getText());
//
////            Object shipmentMod = shipmentComboBox.getSelectedItem();
////            ShipmentMode shipmentMode = (ShipmentMode) ((ComboKeyValue) shipmentMod).getValue();
//            if ((shipmentModeText.getText().equalsIgnoreCase("N/A"))) {
//                purchase.setShipmentMode(null);
//            } else {
//                purchase.setShipmentMode(shipmentMode);
//            }
//
//            purchase.setDiscount(totDiscount * currency.getRate());
//            purchase.setDiscount1(totDiscount1 * currency.getRate());
//
//            /*for (PurchaseItem purchaseItem : purchaseItemList) {
//             purchaseItem.setUnitPrice(purchaseItem.getUnitPrice());
//
//             }*/
//            purchase.setPurchaseItems(purchaseItemList);
//
//            for (PurchaseTax purchaseTax : purchaseTaxList) {
//                purchaseTax.setAmount(purchaseTax.getAmount() * currency.getRate());
//            }
//            purchase.setPurchaseTaxes(purchaseTaxList);
//
//            for (PurchaseCharge purchaseCharge : purchaseChargesList) {
//                purchaseCharge.setAmount(purchaseCharge.getAmount() * currency.getRate());
//                purchaseCharge.setAmount1(purchaseCharge.getAmount1() * currency.getRate());
//            }
//            purchase.setPurchaseCharges(purchaseChargesList);
//            purchase.setCurrentCurrencyRate(currency.getRate());
//            purchase.setMultiCurrency(currency);
//
//            purchase.setGrandTotal(grandTotal * currency.getRate());
//
//            purchase.setGrandTotal1(grandTotal1 * currency.getRate());
//
//            purchase.setCompany(GlobalProperty.getCompany());
//            if (check == 0) {
//                cRUDServices.saveModel(purchase);
//            } else {
//            cRUDServices.saveOrUpdateModel(purchase);
//            }
//
//            //Updating availableQty of Supplier Ledger
//            if (supplierLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
//                supplierLedger.setAvailableBalance(supplierLedger.getAvailableBalance() - (grandTotal * currency.getRate()));
//                supplierLedger.setAvailableBalance1(supplierLedger.getAvailableBalance1() - (grandTotal1 * currency.getRate()));
//                cRUDServices.saveOrUpdateModel(supplierLedger);
//
//            } else if (supplierLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
//                supplierLedger.setAvailableBalance(supplierLedger.getAvailableBalance() + (grandTotal * currency.getRate()));
//                supplierLedger.setAvailableBalance1(supplierLedger.getAvailableBalance1() + (grandTotal1 * currency.getRate()));
//                cRUDServices.saveOrUpdateModel(supplierLedger);
//
//            }
//
//            //Adding available quantity on each item
//            for (PurchaseItem pi : purchaseItemList) {
//                Item item = pi.getItem();
//                item.setAvailableQty(item.getAvailableQty()+ pi.getQuantity());
//                item.setAvailableQty1(item.getAvailableQty1()+pi.getQuantity1());
//                cRUDServices.saveOrUpdateModel(item);
//            }
//            
//            //removed items
//            for (PurchaseItem pi : purchaseItemRemovedList) {
//                Item item = pi.getItem();
//                item.setAvailableQty(item.getAvailableQty());
//                item.setAvailableQty1(item.getAvailableQty1());
//                cRUDServices.saveOrUpdateModel(item);
//               
//            }
//
//            //Updating Purchase Ledger available balance
//            purchaseLedger.setAvailableBalance((purchaseLedger.getAvailableBalance()) + subTotal);
//           
//            purchaseLedger.setAvailableBalance1((purchaseLedger.getAvailableBalance1()) + subTotal1);
//            cRUDServices.saveOrUpdateModel(purchaseLedger);
//
//            //Discount Received
//            discountLedger.setAvailableBalance((discountLedger.getAvailableBalance()) + totDiscount);
//            discountLedger.setAvailableBalance1((discountLedger.getAvailableBalance1()) + totDiscount1);
//            cRUDServices.saveOrUpdateModel(discountLedger);
//
//            numberProperty.setNumber(numberProperty.getNumber() + 1);
//            cRUDServices.saveOrUpdateModel(numberProperty);
//
//            //tax
//            for (Ledger ledger : taxLedgerList) {
//                cRUDServices.saveOrUpdateModel(ledger);
//            }
//            for (PurchaseTax purchaseTax : purchaseTaxList) {
//                ledger = ledgerDAO.findByLedgerName(purchaseTax.getTax().getName());
//                ledger.setAvailableBalance(ledger.getAvailableBalance() + (purchaseTax.getAmount()));
//                cRUDServices.saveOrUpdateModel(ledger);
//            }
//
//            //charges
//            for (Ledger ledger : chargesLedgerList) {
//                cRUDServices.saveOrUpdateModel(ledger);
//            }
//            for (PurchaseCharge purchaseCharge : purchaseChargesList) {
//                ledger = ledgerDAO.findByLedgerName(purchaseCharge.getCharges().getName());
//                ledger.setAvailableBalance(ledger.getAvailableBalance() + purchaseCharge.getAmount());
//                ledger.setAvailableBalance1(ledger.getAvailableBalance1() + purchaseCharge.getAmount1());
//                cRUDServices.saveOrUpdateModel(ledger);
//
//            }
//            listPurchaseItemHistory(purchase);
//        } catch (Exception e) {
//            log.error("savePurchase:", e);
//        }
                try {
            if (purchase == null) {
                check = 0;
                purchase = new Purchase();
            }

            purchase.setBillDate(commonService.utilDateToSqlDate(billDateFormattedText.getDate()));
            purchase.setInvoiceNumber(invoiceNumberText.getText());
            purchase.setAddress(addressTextArea.getText().trim());
            purchase.setSupplier(supplierLedger);
            purchase.setDeliveryDate(deliveryDateFormattedText.getText());

//            Object shipmentMod = shipmentComboBox.getSelectedItem();
//            ShipmentMode shipmentMode = (ShipmentMode) ((ComboKeyValue) shipmentMod).getValue();
            if ((shipmentModeText.getText().equalsIgnoreCase("N/A"))) {
                purchase.setShipmentMode(null);
            } else {
                purchase.setShipmentMode(shipmentMode);
            }

            purchase.setDiscount(totDiscount * currency.getRate());
            purchase.setDiscount1(totDiscount1 * currency.getRate());

            /*for (PurchaseItem purchaseItem : purchaseItemList) {
             purchaseItem.setUnitPrice(purchaseItem.getUnitPrice());

             }*/
            purchase.setPurchaseItems(purchaseItemList);

            for (PurchaseTax purchaseTax : purchaseTaxList) {
                purchaseTax.setAmount(purchaseTax.getAmount() * currency.getRate());
            }
            purchase.setPurchaseTaxes(purchaseTaxList);

            for (PurchaseCharge purchaseCharge : purchaseChargesList) {
                purchaseCharge.setAmount(purchaseCharge.getAmount() * currency.getRate());
                purchaseCharge.setAmount1(purchaseCharge.getAmount1() * currency.getRate());
            }
            purchase.setPurchaseCharges(purchaseChargesList);
            purchase.setCurrentCurrencyRate(currency.getRate());
            purchase.setMultiCurrency(currency);

            purchase.setGrandTotal(grandTotal * currency.getRate());

            purchase.setGrandTotal1(grandTotal1 * currency.getRate());

            purchase.setCompany(GlobalProperty.getCompany());
            //if (check == 0) {
            //    cRUDServices.saveModel(purchase);
            //} else {
            cRUDServices.saveOrUpdateModel(purchase);
            //}

            //Updating availableQty of Supplier Ledger
            if (supplierLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                supplierLedger.setAvailableBalance(supplierLedger.getAvailableBalance() - (grandTotal * currency.getRate()));
                supplierLedger.setAvailableBalance1(supplierLedger.getAvailableBalance1() - (grandTotal1 * currency.getRate()));
                cRUDServices.saveOrUpdateModel(supplierLedger);

            } else if (supplierLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                supplierLedger.setAvailableBalance(supplierLedger.getAvailableBalance() + (grandTotal * currency.getRate()));
                supplierLedger.setAvailableBalance1(supplierLedger.getAvailableBalance1() + (grandTotal1 * currency.getRate()));
                cRUDServices.saveOrUpdateModel(supplierLedger);

            }

            //Adding available quantity on each item
            for (PurchaseItem pi : purchaseItemList) {
                Item item = pi.getItem();
                item.setAvailableQty(item.getAvailableQty()+ pi.getQuantity());
                item.setAvailableQty1(item.getAvailableQty1()+pi.getQuantity1());
                cRUDServices.saveOrUpdateModel(item);
            }
            
            //removed items
            for (PurchaseItem pi : purchaseItemRemovedList) {
                Item item = pi.getItem();
                item.setAvailableQty(item.getAvailableQty());
                item.setAvailableQty1(item.getAvailableQty1());
                cRUDServices.saveOrUpdateModel(item);
               
            }

            //Updating Purchase Ledger available balance
            purchaseLedger.setAvailableBalance((purchaseLedger.getAvailableBalance()) + subTotal);
           
            purchaseLedger.setAvailableBalance1((purchaseLedger.getAvailableBalance1()) + subTotal1);
            cRUDServices.saveOrUpdateModel(purchaseLedger);

            //Discount Received
            discountLedger.setAvailableBalance((discountLedger.getAvailableBalance()) + totDiscount);
            discountLedger.setAvailableBalance1((discountLedger.getAvailableBalance1()) + totDiscount1);
            cRUDServices.saveOrUpdateModel(discountLedger);

            numberProperty.setNumber(numberProperty.getNumber() + 1);
            cRUDServices.saveOrUpdateModel(numberProperty);

            //tax
            for (Ledger ledger : taxLedgerList) {
                cRUDServices.saveOrUpdateModel(ledger);
            }
            for (PurchaseTax purchaseTax : purchaseTaxList) {
                ledger = ledgerDAO.findByLedgerName(purchaseTax.getTax().getName());
                ledger.setAvailableBalance(ledger.getAvailableBalance() + (purchaseTax.getAmount()));
                cRUDServices.saveOrUpdateModel(ledger);
            }

            //charges
            for (Ledger ledger : chargesLedgerList) {
                cRUDServices.saveOrUpdateModel(ledger);
            }
            for (PurchaseCharge purchaseCharge : purchaseChargesList) {
                ledger = ledgerDAO.findByLedgerName(purchaseCharge.getCharges().getName());
                ledger.setAvailableBalance(ledger.getAvailableBalance() + purchaseCharge.getAmount());
                ledger.setAvailableBalance1(ledger.getAvailableBalance1() + purchaseCharge.getAmount1());
                cRUDServices.saveOrUpdateModel(ledger);

            }
            listPurchaseItemHistory(purchase);
        } catch (Exception e) {
            log.error("savePurchase:", e);
        }
    }
    void saveInvoice(){
        try {
            
            if (billDateFormattedText.getDate() != null && supplierLedger != null && purchaseItemList != null&&purchaseItemTable.getRowCount()!=0) {
                if (MsgBox.confirm("Are you sure you want to save?")) {
                    savePurchase();
                    MsgBox.success("Purchase Invoice Saved Successfully");
                    //clearAllFields();
                }
            } else {
                MsgBox.warning("Required fields are not entered");
            }
            

        } catch (Exception ex) {
            log.error("saveButtonActionPerformed:", ex);
        }
    }
    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        PurchaseReturnDAO purchaseReturnDAO= new PurchaseReturnDAO();
        List<PurchaseReturn> purchaseReturnList = purchaseReturnDAO.findAllPurchaseReturn(purchase);
        if(!purchaseReturnList.isEmpty())
        {
            MsgBox.warning("Cannot update this purchase");
        }
        else
        {
            saveInvoice();
        }        
    }//GEN-LAST:event_saveButtonActionPerformed

    private void supplierTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_supplierTextKeyPressed
        try {
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                //String productId = productIdText.getText().trim();
                List<Ledger> suppliers = ledgerService.populatePopupTableBySupplier(supplierText.getText().trim(), popupTableDialog);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();
                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    supplierLedger = suppliers.get(index);
                    supplierText.setText(supplierLedger.getLedgerName());
                    addressTextArea.setText(supplierLedger.getAddress());
                    salesTaxText.setText(supplierLedger.getSalesTaxNo());
                }
            }

        } catch (Exception e) {
            log.error("supplierTextKeyPressed:", e);
        }
    }//GEN-LAST:event_supplierTextKeyPressed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        clearAllFields();
        
    }//GEN-LAST:event_newButtonActionPerformed

    private void godownTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_godownTextFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_godownTextFocusGained

    private void godownTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_godownTextKeyPressed
//        try {
//            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
//            
//            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
//                String godownName = godownText.getText().trim();
//                GodownDAO godownDAO = new GodownDAO();
//                List<Godown> godowns = null;
//                if (godownName.length() <= 0) {
//                    godowns = godownDAO.findAll();
//                } else {
//                    godowns = godownDAO.findAllStartsWithName(godownName);
//                }
//                ArrayList<Object[]> tableData = new ArrayList<>();
//                int slNo = 1;
//                for (Godown godown : godowns) {
//                    tableData.add(new Object[]{slNo++, " " + (godown.getName())});
//                }
//                popupTableDialog.setTitle(new String[]{"Sl No.", "Godown Name"});
//                popupTableDialog.setTableData(tableData);
//
//                popupTableDialog.setVisible(true);
//                ArrayList selectedRow = popupTableDialog.getSelectedRow();
//
//                if (null != selectedRow) {
//                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
//                    godown = godowns.get(index);
//                    godownText.setText(godown.getName());
//                } else {
//                    MsgBox.warning("No Item Selected.");
//                    itemQtyText.requestFocusInWindow();
//                }
//            }
//        } catch (Exception e) {
//            log.error("productIdTextKeyPressed:", e);
//        }
    }//GEN-LAST:event_godownTextKeyPressed

    private void godownTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_godownTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_godownTextKeyTyped

    private void supplierTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supplierTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_supplierTextActionPerformed

    private void totDiscountAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totDiscountAddButtonActionPerformed
        // TODO add your handling code here:
        if (totDiscountAddButton.getText().trim().equalsIgnoreCase("+")) {
            totDiscount = Double.parseDouble(discountText.getText().trim());
            totDiscount1 = Double.parseDouble(discountText1.getText().trim());
            
        } else {
            totDiscount = 0;
            totDiscount1 = 0;
            
            totDiscountAddButton.setText("+");
        }
        discountText.setText("0.00");
        discountText1.setText("0.00");
        fillPurchaseItemTable();
    }//GEN-LAST:event_totDiscountAddButtonActionPerformed

    private void itemUnitPriceText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemUnitPriceText1FocusGained
        itemUnitPriceText1.selectAll();
    }//GEN-LAST:event_itemUnitPriceText1FocusGained

    private void itemUnitPriceText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemUnitPriceText1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemUnitPriceText1KeyTyped

    private void itemQtyText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyText1FocusGained
        itemQtyText1.selectAll();
    }//GEN-LAST:event_itemQtyText1FocusGained

    private void itemQtyText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyText1KeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_itemQtyText1KeyTyped

    private void chargesText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesText1FocusGained
        chargesText1.selectAll();
    }//GEN-LAST:event_chargesText1FocusGained

    private void chargesText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chargesText1KeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_chargesText1KeyTyped

    private void discountTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_discountTextFocusGained
        discountText.selectAll();
    }//GEN-LAST:event_discountTextFocusGained

    private void discountTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_discountTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_discountTextKeyTyped

    private void discountText1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_discountText1FocusGained
        discountText1.selectAll();
    }//GEN-LAST:event_discountText1FocusGained

    private void discountText1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_discountText1KeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_discountText1KeyTyped

    private void sellingPriceTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_sellingPriceTextFocusGained
        sellingPriceText.selectAll();
    }//GEN-LAST:event_sellingPriceTextFocusGained

    private void sellingPriceTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_sellingPriceTextKeyTyped
        CommonService.currencyValidator(evt);
    }//GEN-LAST:event_sellingPriceTextKeyTyped

    private void printBarcodeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printBarcodeButtonActionPerformed
        try {
            if (MsgBox.confirm("Are you sure you want to print barcodes?")) {
                ArrayList<PrintBarcodeBean> barcodeBeans=new ArrayList<>();
                for (PurchaseItem purchaseItem : purchaseItemList) {
                    Item item = purchaseItem.getItem();
                    for (int i = 0; i < purchaseItem.getQuantity(); i++) {
                        PrintBarcodeBean barcodeBean=new PrintBarcodeBean();
                        barcodeBean.setItemCode(item.getItemCode());
                        barcodeBean.setDescription(item.getDescription());
                        barcodeBean.setSellingPrice(item.getSellingPrice());
                        barcodeBeans.add(barcodeBean);
                    }
                    for (int i = 0; i < purchaseItem.getQuantity1(); i++) {
                        PrintBarcodeBean barcodeBean=new PrintBarcodeBean();
                        barcodeBean.setItemCode(item.getItemCode());
                        barcodeBean.setDescription(item.getDescription());
                        barcodeBean.setSellingPrice(item.getSellingPrice());
                        barcodeBeans.add(barcodeBean);
                    }
                }
                for(int i=0;i<barcodeBeans.size();i++){
                    PrintBarcodeBean barcodeBean=barcodeBeans.get(i);
                    PrintBarcodeBean nextBarcodeBean=null;
                    if((i+1)!=barcodeBeans.size()){
                        nextBarcodeBean=barcodeBeans.get(i+1);
                        i++;
                    }else{
                        nextBarcodeBean=barcodeBeans.get(i);
                    }
                    
                    BarcodeReport barcodeReport = new BarcodeReport();
                        barcodeReport.createBarcodeReport(barcodeBean.getItemCode(),
                                barcodeBean.getDescription(), barcodeBean.getSellingPrice()+"",nextBarcodeBean.getItemCode(),
                                nextBarcodeBean.getDescription(), nextBarcodeBean.getSellingPrice() + "", "print");
                }
            }
        } catch (Exception e) {
            log.error("printBarcodeButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_printBarcodeButtonActionPerformed

    private void shirtRadioButtonStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_shirtRadioButtonStateChanged
        if(shirtRadioButton.isSelected()){
            SLabel.setText("S");
            MLabel.setText("M");
            LLabel.setText("L");
            XLLabel.setText("XL");
            XXLLabel.setText("XXL");
        }
    }//GEN-LAST:event_shirtRadioButtonStateChanged

    private void pantRadioButtonStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_pantRadioButtonStateChanged
        if(pantRadioButton.isSelected()){
            SLabel.setText("28");
            MLabel.setText("30");
            LLabel.setText("32");
            XLLabel.setText("34");
            XXLLabel.setText("36");
        }
    }//GEN-LAST:event_pantRadioButtonStateChanged

    private void STextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_STextFieldKeyTyped
      CommonService.numberValidator(evt);
    }//GEN-LAST:event_STextFieldKeyTyped

    private void STextField1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_STextField1KeyTyped
       CommonService.numberValidator(evt);
    }//GEN-LAST:event_STextField1KeyTyped

    private void MTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MTextFieldKeyTyped
       CommonService.numberValidator(evt);
    }//GEN-LAST:event_MTextFieldKeyTyped

    private void MTextField1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MTextField1KeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_MTextField1KeyTyped

    private void LTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_LTextFieldKeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_LTextFieldKeyTyped

    private void LTextField1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_LTextField1KeyTyped
       CommonService.numberValidator(evt);
    }//GEN-LAST:event_LTextField1KeyTyped

    private void XLTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_XLTextFieldKeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_XLTextFieldKeyTyped

    private void XLTextField1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_XLTextField1KeyTyped
       CommonService.numberValidator(evt);
    }//GEN-LAST:event_XLTextField1KeyTyped

    private void XXLTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_XXLTextFieldKeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_XXLTextFieldKeyTyped

    private void XXLTextField1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_XXLTextField1KeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_XXLTextField1KeyTyped

    private void taxTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_taxTextKeyPressed
       try
        {
           PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
           if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            List<Tax> taxes = taxService.populatePopupTableByTax(taxText.getText().trim(), popupTableDialog);
            taxList=taxes;
            popupTableDialog.setVisible(true);
            taxCount=popupTableDialog.getRowCount();
               
            ArrayList selectedRow = popupTableDialog.getSelectedRow();
            if(selectedRow!=null)
            {
            int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
            tax = taxes.get(index);
            taxText.setText(tax.getName());
            }
        }}catch(Exception e)
        {
          e.printStackTrace();
        }
    }//GEN-LAST:event_taxTextKeyPressed

    private void chargesTextBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chargesTextBoxKeyPressed
       try
        {
           ItemService itemService = new ItemService();
           PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
           if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            List<Charges> chargeList  = chargesService.populatePopupTableByCharges(chargesTextBox.getText().trim(), popupTableDialog);
            chargesList=chargeList;
            popupTableDialog.setVisible(true);
            ArrayList selectedRow = popupTableDialog.getSelectedRow();
            if(selectedRow!=null)
            {
            int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
            charges = chargeList.get(index);
            chargesTextBox.setText(charges.getName());
            }
          }
        }catch(Exception e)
        {
          e.printStackTrace();
        }        
    }//GEN-LAST:event_chargesTextBoxKeyPressed

    private void shipmentModeTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_shipmentModeTextKeyPressed
         try
        {
           PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
           if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            List<ShipmentMode> shipmentModes = shipmentModeService.populatePopupTableByShipmentMode(shipmentModeText.getText().trim(), popupTableDialog);
            shipmentModesList=shipmentModes;
            popupTableDialog.setVisible(true);
            shipmentModesCount=popupTableDialog.getRowCount();
            ArrayList selectedRow = popupTableDialog.getSelectedRow();
            if(selectedRow!=null)
            {
            int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
            shipmentMode = shipmentModes.get(index);
            shipmentModeText.setText(shipmentMode.getName());
            
            }
        }}catch(Exception e)
        {
          e.printStackTrace();
        }
    }//GEN-LAST:event_shipmentModeTextKeyPressed

    private void STextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_STextFieldFocusGained
        STextField.selectAll();
    }//GEN-LAST:event_STextFieldFocusGained

    private void MTextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_MTextFieldFocusGained
        MTextField.selectAll();
    }//GEN-LAST:event_MTextFieldFocusGained

    private void LTextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_LTextFieldFocusGained
        LTextField.selectAll();
    }//GEN-LAST:event_LTextFieldFocusGained

    private void XLTextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_XLTextFieldFocusGained
        XLTextField.selectAll();
    }//GEN-LAST:event_XLTextFieldFocusGained

    private void XXLTextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_XXLTextFieldFocusGained
        XXLTextField.selectAll();
    }//GEN-LAST:event_XXLTextFieldFocusGained

    private void STextField1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_STextField1FocusGained
       STextField1.selectAll();
    }//GEN-LAST:event_STextField1FocusGained

    private void MTextField1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_MTextField1FocusGained
        MTextField1.selectAll();
    }//GEN-LAST:event_MTextField1FocusGained

    private void LTextField1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_LTextField1FocusGained
        LTextField1.selectAll();
    }//GEN-LAST:event_LTextField1FocusGained

    private void XLTextField1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_XLTextField1FocusGained
        XLTextField1.selectAll();
    }//GEN-LAST:event_XLTextField1FocusGained

    private void XXLTextField1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_XXLTextField1FocusGained
        XXLTextField1.selectAll();
    }//GEN-LAST:event_XXLTextField1FocusGained

    private void STextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_STextFieldFocusLost
        if(STextField.getText().isEmpty())
                STextField.setText("0");
    }//GEN-LAST:event_STextFieldFocusLost

    private void MTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_MTextFieldFocusLost
         if(MTextField.getText().isEmpty())
                MTextField.setText("0");
    }//GEN-LAST:event_MTextFieldFocusLost

    private void LTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_LTextFieldFocusLost
        if(LTextField.getText().isEmpty())
                LTextField.setText("0");
    }//GEN-LAST:event_LTextFieldFocusLost

    private void XLTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_XLTextFieldFocusLost
        if(XLTextField.getText().isEmpty())
                XLTextField.setText("0");
    }//GEN-LAST:event_XLTextFieldFocusLost

    private void XXLTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_XXLTextFieldFocusLost
        if(XXLTextField.getText().isEmpty())
                XXLTextField.setText("0");
    }//GEN-LAST:event_XXLTextFieldFocusLost

    private void STextField1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_STextField1FocusLost
        if(STextField1.getText().isEmpty())
                STextField1.setText("0");
    }//GEN-LAST:event_STextField1FocusLost

    private void MTextField1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_MTextField1FocusLost
        if(MTextField1.getText().isEmpty())
                MTextField1.setText("0");
    }//GEN-LAST:event_MTextField1FocusLost

    private void LTextField1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_LTextField1FocusLost
        if(LTextField1.getText().isEmpty())
                LTextField1.setText("0");
    }//GEN-LAST:event_LTextField1FocusLost

    private void XLTextField1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_XLTextField1FocusLost
        if(XLTextField1.getText().isEmpty())
                XLTextField1.setText("0");
    }//GEN-LAST:event_XLTextField1FocusLost

    private void XXLTextField1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_XXLTextField1FocusLost
          if(XXLTextField1.getText().isEmpty())
                XXLTextField1.setText("0");
    }//GEN-LAST:event_XXLTextField1FocusLost

    private void itemQtyTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyTextFocusLost
       if(itemQtyText.getText().isEmpty())
           itemQtyText.setText("0");
    }//GEN-LAST:event_itemQtyTextFocusLost

    private void itemQtyText1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyText1FocusLost
        if(itemQtyText1.getText().isEmpty())
           itemQtyText1.setText("0");
    }//GEN-LAST:event_itemQtyText1FocusLost

    private void itemUnitPriceTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemUnitPriceTextFocusLost
        if(itemUnitPriceText.getText().isEmpty())
           itemUnitPriceText.setText("0.00");
    }//GEN-LAST:event_itemUnitPriceTextFocusLost

    private void itemUnitPriceText1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemUnitPriceText1FocusLost
         if(itemUnitPriceText1.getText().isEmpty())
           itemUnitPriceText1.setText("0.00");
    }//GEN-LAST:event_itemUnitPriceText1FocusLost

    private void sellingPriceTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_sellingPriceTextFocusLost
        if(sellingPriceText.getText().isEmpty())
           sellingPriceText.setText("0.00");
    }//GEN-LAST:event_sellingPriceTextFocusLost

    private void chargesTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesTextFocusLost
        if(chargesText.getText().isEmpty())
           chargesText.setText("0.00");
    }//GEN-LAST:event_chargesTextFocusLost

    private void chargesText1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesText1FocusLost
        if(chargesText1.getText().isEmpty())
           chargesText1.setText("0.00");
    }//GEN-LAST:event_chargesText1FocusLost

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        PurchaseReturnDAO purchaseReturnDAO= new PurchaseReturnDAO();
        List<PurchaseReturn> purchaseReturnList =purchaseReturnDAO.findAllPurchaseReturn(purchase);
        if(!purchaseReturnList.isEmpty())
        {
            MsgBox.warning("Purchase return exists for this purchase. Hence this cannot be deleted");
        }
        else
        {
            deletePurchase();
        } 
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void taxTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_taxTextFocusGained
            taxText.selectAll();
    }//GEN-LAST:event_taxTextFocusGained

    private void chargesTextBoxFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesTextBoxFocusGained
        chargesText.selectAll();
    }//GEN-LAST:event_chargesTextBoxFocusGained
    void deletePurchase()
    {
        double availableBal=0.00;
        double availableBal1=0.00;
        if(purchase!=null)
        {
            try {
                for(PurchaseItem purchaseItem:purchase.getPurchaseItems())
                {
                    cRUDServices.saveOrUpdateModel(purchaseItem.getItem());
                }
                
                //Updating Purchase Ledger available balance
                cRUDServices.saveOrUpdateModel(purchaseLedger);
                
                //Discount Received
                cRUDServices.saveOrUpdateModel(discountLedger);

                //Updating availableQty of Supplier Ledger
                if (supplierLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Asset")) {
                    try {
                        cRUDServices.saveOrUpdateModel(supplierLedger);
                    } catch (Exception ex) {
                        java.util.logging.Logger.getLogger(PurchaseView1.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                } else if (supplierLedger.getLedgerGroup().getAccountType().equalsIgnoreCase("Liability")) {
                    try {
                        cRUDServices.saveOrUpdateModel(supplierLedger);
                    } catch (Exception ex) {
                        java.util.logging.Logger.getLogger(PurchaseView1.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
                for(Ledger ledger : taxLedgerList)
                {
                    cRUDServices.saveOrUpdateModel(ledger);
                }
                for(Ledger ledger : chargesLedgerList)
                {
                    cRUDServices.saveOrUpdateModel(ledger);
                }
                //purchaseDAO.deletePurchase(purchase);
                cRUDServices.deleteModel(purchase);
                MsgBox.success("Purchase deleted successfully");
                clearAllFields();
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(PurchaseView1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
        {
            MsgBox.warning("No purchase to delete");
        }
    }
    void generateItemAndPurchaseItem(){
          
            try {
  
            double itemQty = Double.parseDouble(itemQtyText.getText());
            double itemUnitPrice = Double.parseDouble(itemUnitPriceText.getText());
            double itemQty1 = Double.parseDouble(itemQtyText1.getText());
            double itemUnitPrice1 = Double.parseDouble(itemUnitPriceText1.getText());
            double sellingPrice = Double.parseDouble(sellingPriceText.getText());
 
            
            int S=Integer.parseInt(STextField.getText());
            int M=Integer.parseInt(MTextField.getText());
            int L=Integer.parseInt(LTextField.getText());
            int XL=Integer.parseInt(XLTextField.getText());
            int XXL=Integer.parseInt(XXLTextField.getText());
            //Hidden values
            int S1=Integer.parseInt(STextField1.getText());
            int M1=Integer.parseInt(MTextField1.getText());
            int L1=Integer.parseInt(LTextField1.getText());
            int XL1=Integer.parseInt(XLTextField1.getText());
            int XXL1=Integer.parseInt(XXLTextField1.getText());
            
            //int totalSize = S + M + L + XL + XXL;
            
            Unit defaultUnit=unitDAO.findByName("Nos.");
            //////////////
            
            List<Item> existingItem = itemDAO.findAllItemsWithCode(itemDescriptionText.getText().toString(),sellingPrice,stockGroup,godown );
            List<Item> existingItemS = null;
            List<Item> existingItemM = null;
            List<Item> existingItemL = null;
            List<Item> existingItemXL = null;
            List<Item> existingItemXXL = null;
            

            if(existingItem.size()>0)
            {
                if(MsgBox.confirm("Barcode Already Exists!!!. Do you want to use the existing barcode?"))
                {
                    //Item itemObject = existingItem.get(0);
                    BarcodeSetting barCodeSetting = new BarcodeSetting(mainForm, true,existingItem);
                    Toolkit toolkit = Toolkit.getDefaultToolkit();  
                    Dimension screenSize = toolkit.getScreenSize(); 
                    int x = (screenSize.width - barCodeSetting.getWidth()) / 2;  
                    int y = (screenSize.height - barCodeSetting.getHeight()) / 2;  
                    barCodeSetting.setLocation(x, y);
                    barCodeSetting.setVisible(true);
                    
                    if(MainForm.oldBarcode!=null)
                    {
                        while((itemQty>0  &&(S>0 || M>0 || L>0 || XL>0 || XXL>0))||(itemQty1>0 && (S1>0 || M1>0 || L1>0 || XL1>0 || XXL1>0))){
                            if (((S > 0 && itemQty>0)||(S1>0 && itemQty1>0))) {
                                double exactQty=(itemQty<(double)S)?itemQty:(double)S;
                                double exactQty1=(itemQty1<(double)S1?itemQty1:(double)S1);
                                Item item = new Item();
                                existingItemS = itemDAO.findAllByItemCodeDescriptionSellingPriceStockGroupAndGodown(MainForm.oldBarcode.trim(),itemDescriptionText.getText() + " "+SLabel.getText(),sellingPrice,stockGroup,godown);

                                if(existingItemS.size()>0)
                                    item = existingItemS.get(0);
                                else
                                {
                                    //Item item1 = existingItem.get(0);
                                    item.setItemCode(MainForm.oldBarcode.trim());
                                    item.setDescription(itemDescriptionText.getText() + " "+SLabel.getText());
                                    item.setCompany(GlobalProperty.getCompany());
                                    item.setGodown(godown);
                                    item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                                    item.setStockGroup(stockGroup);
                                    item.setVisible(true);
                                    item.setUnit(defaultUnit);
                               }
                                //item.setAvailableQty(((exactQty>0)?exactQty:0));
                                //item.setAvailableQty1(((exactQty1>0)?exactQty1:0));
            //                    item.setCompany(GlobalProperty.getCompany());
            //                    item.setGodown(godown);
            //                    item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
            //                    item.setStockGroup(stockGroup);
            //                    item.setVisible(true);
            //                    item.setUnit(defaultUnit);

                                int index = findReplica(item);
                                if(index>-1)
                                {
                                    MsgBox.warning("Item Already Exists!!!");

                                    purchaseItem=purchaseItemList.get(index);
                                    purchaseItem.setQuantity(exactQty);
                                    purchaseItem.setQuantity1(exactQty1);
                                    purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                                    purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText()))*currency.getRate());
                                    purchaseItem.setDiscount(0.00);
                                    purchaseItemList.remove(index);
                                    purchaseItemList.add(index, purchaseItem);

                                }
                                else
                                {
                                    purchaseItem = new PurchaseItem();
                                    purchaseItem.setItem(item);
                                    purchaseItem.setQuantity(exactQty);
                                    purchaseItem.setQuantity1(exactQty1);
                                    purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                                    purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText()))*currency.getRate());
                                    purchaseItem.setDiscount(0.00);
                                    purchaseItemList.add(purchaseItem);
                                }

                                itemQty=itemQty>0?itemQty-=S:itemQty;
                                itemQty1=itemQty1>0?itemQty1-=S1:itemQty1;
                        }
                        if (((M > 0 && itemQty>0)||(M1 > 0 && itemQty1>0 ))) {
                            double exactQty=(itemQty<(double)M)?itemQty:(double)M;
                            double exactQty1=(itemQty<(double)M1)?itemQty1:(double)M1;
                            existingItemM = itemDAO.findAllByItemCodeDescriptionSellingPriceStockGroupAndGodown(MainForm.oldBarcode.trim(),itemDescriptionText.getText() + " "+MLabel.getText(),sellingPrice,stockGroup,godown);
                            Item item = new Item();
                            if(existingItemM.size()>0)
                                item = existingItemM.get(0);
                            else
                            {
        //                        Item item1 = existingItem.get(0);
                                item.setItemCode(MainForm.oldBarcode.trim());    
                                item.setDescription(itemDescriptionText.getText() + " "+MLabel.getText());
                                item.setCompany(GlobalProperty.getCompany());
                                item.setGodown(godown);
                                item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                                item.setStockGroup(stockGroup);
                                item.setVisible(true);
                                item.setUnit(defaultUnit);
                           }

                            //item.setItemCode(item.getItemCode());
                            //item.setDescription(itemDescriptionText.getText() + " "+MLabel.getText());
                            //item.setAvailableQty(item.getAvailableQty()+((exactQty>0)?exactQty:0));
                            //item.setAvailableQty1(item.getAvailableQty1()+((exactQty1>0)?exactQty:0));
                            //item.setCompany(GlobalProperty.getCompany());
                            //item.setGodown(godown);
                            //item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                            //item.setStockGroup(stockGroup);
                            //item.setVisible(true);
                            //item.setUnit(defaultUnit);

                            int index = findReplica(item);
                            if(index>-1)
                            {
                                MsgBox.warning("Item Already Exists!!!");
                                purchaseItem=purchaseItemList.get(index);
                                purchaseItem.setQuantity(exactQty);
                                purchaseItem.setQuantity1(exactQty1);
                                purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                                purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText()))*currency.getRate());
                                purchaseItem.setDiscount(0.00);
                                purchaseItemList.remove(index);
                                purchaseItemList.add(index, purchaseItem);
                            }
                            else
                            {
                                purchaseItem = new PurchaseItem();
                                purchaseItem.setItem(item);
                                purchaseItem.setQuantity(exactQty);
                                purchaseItem.setQuantity1(exactQty1);
                                purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                                purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText())) * currency.getRate());
                                purchaseItem.setDiscount(0.00);
                                purchaseItemList.add(purchaseItem);
                            }
                                itemQty=itemQty>0?itemQty-=M:itemQty;
                                itemQty1=itemQty1>0?itemQty1-=M1:itemQty1;


                        }
                        if (((L > 0 && itemQty>0)||(L1 >0 && itemQty1>0))){

                            double exactQty=(itemQty<(double)L)?itemQty:(double)L;
                            double exactQty1=(itemQty1<(double)L1)?itemQty1:(double)L1;                    
                            //Item item = new Item();
                            existingItemL = itemDAO.findAllByItemCodeDescriptionSellingPriceStockGroupAndGodown(MainForm.oldBarcode.trim(),itemDescriptionText.getText() + " "+LLabel.getText(),sellingPrice,stockGroup,godown);
                            Item item = new Item();
                            if(existingItemL.size()>0)
                                item = existingItemL.get(0);
                            else
                            {
        //                        Item item1 = existingItem.get(0);
                                item.setItemCode(MainForm.oldBarcode.trim());
                                item.setDescription(itemDescriptionText.getText() + " "+LLabel.getText());
                                item.setCompany(GlobalProperty.getCompany());
                                item.setGodown(godown);
                                item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                                item.setStockGroup(stockGroup);
                                item.setVisible(true);
                                item.setUnit(defaultUnit);
                            }

                            //item.setItemCode(item.getItemCode());
                            //item.setDescription(itemDescriptionText.getText() + " "+LLabel.getText());
                            //item.setAvailableQty(item.getAvailableQty()+((exactQty>0)?exactQty:0));
                            //item.setAvailableQty1(item.getAvailableQty1()+((exactQty>0)?exactQty:0));
        //                    item.setCompany(GlobalProperty.getCompany());
        //                    item.setGodown(godown);
        //                    item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
        //                    item.setStockGroup(stockGroup);
        //                    item.setVisible(true);
        //                    item.setUnit(defaultUnit);
                            int index = findReplica(item);
                            if(index>-1)
                            {
                                MsgBox.warning("Item Already Exists!!!");
                                purchaseItem=purchaseItemList.get(index);
                                purchaseItem.setQuantity(exactQty);
                                purchaseItem.setQuantity1(exactQty1);
                                purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                                purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText()))*currency.getRate());
                                purchaseItem.setDiscount(0.00);
                                purchaseItemList.remove(index);
                                purchaseItemList.add(index, purchaseItem);
                            }
                            else
                            {
                                purchaseItem = new PurchaseItem();
                                purchaseItem.setItem(item);
                                purchaseItem.setQuantity(exactQty);
                                purchaseItem.setQuantity1(exactQty1);
                                purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                                purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText())) * currency.getRate());
                                purchaseItem.setDiscount(0.00);
                                purchaseItemList.add(purchaseItem);
                            }

                            itemQty=itemQty>0?itemQty-=L:itemQty;
                            itemQty1=itemQty1>0?itemQty1-=L1:itemQty1;
                        }
                        if (((XL > 0 && itemQty>0)||(XL1 > 0 && itemQty1>0))) {
                            double exactQty=(itemQty<(double)XL)?itemQty:(double)XL;
                            double exactQty1=(itemQty<(double)XL1)?itemQty1:(double)XL1;                    
                            existingItemXL = itemDAO.findAllByItemCodeDescriptionSellingPriceStockGroupAndGodown(MainForm.oldBarcode.trim(),itemDescriptionText.getText() + " "+XLLabel.getText(),sellingPrice,stockGroup,godown);
                            Item item = new Item();
                            if(existingItemXL.size()>0)
                                item = existingItemXL.get(0);
                            else
                            {
        //                        Item item1 = existingItem.get(0);
                                item.setItemCode(MainForm.oldBarcode.trim());
                                item.setDescription(itemDescriptionText.getText() + " "+XLLabel.getText());
                                item.setCompany(GlobalProperty.getCompany());
                                item.setGodown(godown);
                                item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                                item.setStockGroup(stockGroup);
                                item.setVisible(true);
                                item.setUnit(defaultUnit);
                            }
                            //item.setItemCode(item.getItemCode());
                            //item.setDescription(itemDescriptionText.getText() + " "+XLLabel.getText());
                            //item.setAvailableQty(exactQty);
                            //item.setAvailableQty(exactQty1);
        //                    item.setCompany(GlobalProperty.getCompany());
        //                    item.setGodown(godown);
        //                    item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
        //                    item.setStockGroup(stockGroup);
        //                    item.setVisible(true);
        //                    item.setUnit(defaultUnit);

                            int index = findReplica(item);
                            if(index>-1)
                            {
                                MsgBox.warning("Item Already Exists!!!");
                                purchaseItem=purchaseItemList.get(index);
                                purchaseItem.setQuantity(exactQty);
                                purchaseItem.setQuantity1(exactQty1);
                                purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                                purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText()))*currency.getRate());
                                purchaseItem.setDiscount(0.00);
                                purchaseItemList.remove(index);
                                purchaseItemList.add(index, purchaseItem);
                            }
                            else
                            {
                                purchaseItem = new PurchaseItem();
                                purchaseItem.setItem(item);
                                purchaseItem.setQuantity(exactQty);
                                purchaseItem.setQuantity1(exactQty1);
                                purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                                purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText())) * currency.getRate());
                                purchaseItem.setDiscount(0.00);
                                purchaseItemList.add(purchaseItem);
                            }

                            itemQty=itemQty>0?itemQty-=XL:itemQty;
                            itemQty1=itemQty1>0?itemQty1-=XL1:itemQty1;
                        }
                        if (((XXL > 0 && itemQty>0)||(XXL1 > 0 && itemQty1>0))) {

                            double exactQty=(itemQty<(double)XXL)?itemQty:(double)XXL;
                            double exactQty1=(itemQty1<(double)XXL1)?itemQty1:(double)XXL1;

                            existingItemXXL = itemDAO.findAllByItemCodeDescriptionSellingPriceStockGroupAndGodown(MainForm.oldBarcode,itemDescriptionText.getText() + " "+XXLLabel.getText(),sellingPrice,stockGroup,godown);
                            Item item = new Item();
                            if(existingItemXXL.size()>0)
                                item = existingItemXXL.get(0);
                            else
                            {
        //                        Item item1 = existingItem.get(0);
                                item.setItemCode(MainForm.oldBarcode);
                                item.setDescription(itemDescriptionText.getText() + " "+XXLLabel.getText());
                                item.setCompany(GlobalProperty.getCompany());
                                item.setGodown(godown);
                                item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                                item.setStockGroup(stockGroup);
                                item.setVisible(true);
                                item.setUnit(defaultUnit);
                            }
                            //item.setItemCode(item.getItemCode());
                            //item.setDescription(itemDescriptionText.getText() + " "+XXLLabel.getText());
                            //item.setAvailableQty(exactQty);
                            //item.setAvailableQty1(exactQty1);
        //                    item.setCompany(GlobalProperty.getCompany());
        //                    item.setGodown(godown);
        //                    item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
        //                    item.setStockGroup(stockGroup);
        //                    item.setVisible(true);
        //                    item.setUnit(defaultUnit);
                            int index = findReplica(item);
                            if(index>-1)
                            {
                                MsgBox.warning("Item Already Exists!!!");
                                purchaseItem=purchaseItemList.get(index);
                                purchaseItem.setQuantity(exactQty);
                                purchaseItem.setQuantity1(exactQty1);
                                purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                                purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText()))*currency.getRate());
                                purchaseItem.setDiscount(0.00);
                                purchaseItemList.remove(index);
                                purchaseItemList.add(index, purchaseItem);
                            }
                            else
                            {
                                purchaseItem = new PurchaseItem();
                                purchaseItem.setItem(item);
                                purchaseItem.setQuantity(exactQty);
                                purchaseItem.setQuantity1(exactQty1);
                                purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                                purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText())) * currency.getRate());
                                purchaseItem.setDiscount(0.00);
                                purchaseItemList.add(purchaseItem);
                            }

                            itemQty=itemQty>0?itemQty-=XXL:itemQty;
                            itemQty1=itemQty1>0?itemQty1-=XXL1:itemQty1;
                         }
                       }
                    }
                }
                    else
                    {
                                while((itemQty>0  &&(S>0 || M>0 || L>0 || XL>0 || XXL>0))||(itemQty1>0 && (S1>0 || M1>0 || L1>0 || XL1>0 || XXL1>0))){
                                String authCode = commonService.generateAuthCode();
                                if ((S > 0 && itemQty>0)||(S1>0 && itemQty1>0) ) {
                                double exactQty=(itemQty<(double)S)?itemQty:(double)S;
                                double exactQty1=(itemQty1<(double)S1?itemQty1:(double)S1);
                                Item item = new Item();
                                item.setItemCode(authCode);
                                item.setDescription(itemDescriptionText.getText() + " "+SLabel.getText());

            //                    item.setAvailableQty((exactQty>0)?exactQty:0);
            //                    item.setAvailableQty1((exactQty1>0)?exactQty1:0);
                                item.setCompany(GlobalProperty.getCompany());
                                item.setGodown(godown);
                                item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                                item.setStockGroup(stockGroup);
                                item.setVisible(true);
                                item.setUnit(defaultUnit);

                                 if(findReplica(item)>-1)
                                {
                                    MsgBox.warning("Item Already Exists!!!");
                                    break;
                                }
                                else
                                {
                                    purchaseItem = new PurchaseItem();
                                    purchaseItem.setItem(item);
                                    purchaseItem.setQuantity(exactQty);
                                    purchaseItem.setQuantity1(exactQty1);
                                    purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                                    purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText()))*currency.getRate());
                                    purchaseItem.setDiscount(0.00);
                                    purchaseItemList.add(purchaseItem);
                                    itemQty=itemQty>0?itemQty-=S:itemQty;
                                    itemQty1=itemQty1>0?itemQty1-=S1:itemQty1;
                                }
                                
                            }
                            if ((M > 0 && itemQty>0)||(M1 > 0 && itemQty1>0 )) {
                                double exactQty=(itemQty<(double)M)?itemQty:(double)M;
                                double exactQty1=(itemQty<(double)M1)?itemQty1:(double)M1;
                                Item item = new Item();
                                item.setItemCode(authCode);
                                item.setDescription(itemDescriptionText.getText() + " "+MLabel.getText());
            //                    item.setAvailableQty(exactQty);
            //                    item.setAvailableQty1(exactQty1);
                                item.setCompany(GlobalProperty.getCompany());
                                item.setGodown(godown);
                                item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                                item.setStockGroup(stockGroup);
                                item.setVisible(true);
                                item.setUnit(defaultUnit);
                                
                                 if(findReplica(item)>-1)
                                {
                                    MsgBox.warning("Item Already Exists!!!");
                                    break;
                                }
                                else
                                 {
                                    purchaseItem = new PurchaseItem();
                                    purchaseItem.setItem(item);
                                    purchaseItem.setQuantity(exactQty);
                                    purchaseItem.setQuantity1(exactQty1);
                                    purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                                    purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText())) * currency.getRate());
                                    purchaseItem.setDiscount(0.00);
                                    purchaseItemList.add(purchaseItem);
                                    itemQty=itemQty>0?itemQty-=M:itemQty;
                                    itemQty1=itemQty1>0?itemQty1-=M1:itemQty1;
                                 }
                                
                            }
                            if ((L > 0 && itemQty>0)||(L1 >0 && itemQty1>0)) {

                                double exactQty=(itemQty<(double)L)?itemQty:(double)L;
                                double exactQty1=(itemQty1<(double)L1)?itemQty1:(double)L1;                    
                                Item item = new Item();
                                item.setItemCode(authCode);
                                item.setDescription(itemDescriptionText.getText() + " "+LLabel.getText());
            //                    item.setAvailableQty(exactQty);
            //                    item.setAvailableQty1(exactQty1);
                                item.setCompany(GlobalProperty.getCompany());
                                item.setGodown(godown);
                                item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                                item.setStockGroup(stockGroup);
                                item.setVisible(true);
                                item.setUnit(defaultUnit);

                                if(findReplica(item)>-1)
                                {
                                    MsgBox.warning("Item Already Exists!!!");
                                    break;
                                }
                                else
                                {
                                    purchaseItem = new PurchaseItem();
                                    purchaseItem.setItem(item);
                                    purchaseItem.setQuantity(exactQty);
                                    purchaseItem.setQuantity1(exactQty1);
                                    purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                                    purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText())) * currency.getRate());
                                    purchaseItem.setDiscount(0.00);
                                    purchaseItemList.add(purchaseItem);

                                    itemQty=itemQty>0?itemQty-=L:itemQty;
                                    itemQty1=itemQty1>0?itemQty1-=L1:itemQty1;
                                }
                            }
                            if ((XL > 0 && itemQty>0)||(XL1 > 0 && itemQty1>0)) {
                                double exactQty=(itemQty<(double)XL)?itemQty:(double)XL;
                                double exactQty1=(itemQty<(double)XL1)?itemQty1:(double)XL1;                    
                                Item item = new Item();
                                item.setItemCode(authCode);
                                item.setDescription(itemDescriptionText.getText() + " "+XLLabel.getText());
            //                    item.setAvailableQty(exactQty);
            //                    item.setAvailableQty(exactQty1);
                                item.setCompany(GlobalProperty.getCompany());
                                item.setGodown(godown);
                                item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                                item.setStockGroup(stockGroup);
                                item.setVisible(true);
                                item.setUnit(defaultUnit);
                                if(findReplica(item)>-1)
                                {
                                    MsgBox.warning("Item Already Exists!!!");
                                    break;
                                }
                                else
                                {
                                    purchaseItem = new PurchaseItem();
                                    purchaseItem.setItem(item);
                                    purchaseItem.setQuantity(exactQty);
                                    purchaseItem.setQuantity1(exactQty1);
                                    purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                                    purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText())) * currency.getRate());
                                    purchaseItem.setDiscount(0.00);
                                    purchaseItemList.add(purchaseItem);

                                    itemQty=itemQty>0?itemQty-=XL:itemQty;
                                    itemQty1=itemQty1>0?itemQty1-=XL1:itemQty1;
                                }
                            }
                            if ((XXL > 0 && itemQty>0)||(XXL1 > 0 && itemQty1>0)) {

                                double exactQty=(itemQty<(double)XXL)?itemQty:(double)XXL;
                                double exactQty1=(itemQty1<(double)XXL1)?itemQty1:(double)XXL1;

                                Item item = new Item();
                                item.setItemCode(authCode);
                                item.setDescription(itemDescriptionText.getText() + " "+XXLLabel.getText());
            //                    item.setAvailableQty(exactQty);
            //                    item.setAvailableQty1(exactQty1);
                                item.setCompany(GlobalProperty.getCompany());
                                item.setGodown(godown);
                                item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                                item.setStockGroup(stockGroup);
                                item.setVisible(true);
                                item.setUnit(defaultUnit);
                                if(findReplica(item)>-1)
                                {
                                    MsgBox.warning("Item Already Exists!!!");
                                    break;
                                }
                                else
                                {
                                    purchaseItem = new PurchaseItem();
                                    purchaseItem.setItem(item);
                                    purchaseItem.setQuantity(exactQty);
                                    purchaseItem.setQuantity1(exactQty1);
                                    purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                                    purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText())) * currency.getRate());
                                    purchaseItem.setDiscount(0.00);
                                    purchaseItemList.add(purchaseItem);

                                    itemQty=itemQty>0?itemQty-=XXL:itemQty;
                                    itemQty1=itemQty1>0?itemQty1-=XXL1:itemQty1;
                                }
                            }

                        }
                    }
            //}
            }
            else
                
            {
            //Actual Calculation
            while((itemQty>0  &&(S>0 || M>0 || L>0 || XL>0 || XXL>0))||(itemQty1>0 && (S1>0 || M1>0 || L1>0 || XL1>0 || XXL1>0))){
                
                    String authCode = commonService.generateAuthCode();
                    if ((S > 0 && itemQty>0)||(S1>0 && itemQty1>0) ) {
                    double exactQty=(itemQty<(double)S)?itemQty:(double)S;
                    double exactQty1=(itemQty1<(double)S1?itemQty1:(double)S1);
                    Item item = new Item();
                    item.setItemCode(authCode);
                    item.setDescription(itemDescriptionText.getText() + " "+SLabel.getText());
                    
//                    item.setAvailableQty((exactQty>0)?exactQty:0);
//                    item.setAvailableQty1((exactQty1>0)?exactQty1:0);
                    item.setCompany(GlobalProperty.getCompany());
                    item.setGodown(godown);
                    item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                    item.setStockGroup(stockGroup);
                    item.setVisible(true);
                    item.setUnit(defaultUnit);
                    
                    if(findReplica(item)>-1)
                    {
                        MsgBox.warning("Item Already Exists!!!");
                        break;
                    }
                    else
                    {
                        purchaseItem = new PurchaseItem();
                        purchaseItem.setItem(item);
                        purchaseItem.setQuantity(exactQty);
                        purchaseItem.setQuantity1(exactQty1);
                        purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                        purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText()))*currency.getRate());
                        purchaseItem.setDiscount(0.00);
                        purchaseItemList.add(purchaseItem);

                        itemQty=itemQty>0?itemQty-=S:itemQty;
                        itemQty1=itemQty1>0?itemQty1-=S1:itemQty1;
                    }
                }
                if ((M > 0 && itemQty>0)||(M1 > 0 && itemQty1>0 )) {
                    double exactQty=(itemQty<(double)M)?itemQty:(double)M;
                    double exactQty1=(itemQty<(double)M1)?itemQty1:(double)M1;
                    Item item = new Item();
                    item.setItemCode(authCode);
                    item.setDescription(itemDescriptionText.getText() + " "+MLabel.getText());
//                    item.setAvailableQty(exactQty);
//                    item.setAvailableQty1(exactQty1);
                    item.setCompany(GlobalProperty.getCompany());
                    item.setGodown(godown);
                    item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                    item.setStockGroup(stockGroup);
                    item.setVisible(true);
                    item.setUnit(defaultUnit);
                    if(findReplica(item)>-1)
                    {
                        MsgBox.warning("Item Already Exists!!!");
                        break;
                    }
                    else
                    { 
                        purchaseItem = new PurchaseItem();
                        purchaseItem.setItem(item);
                        purchaseItem.setQuantity(exactQty);
                        purchaseItem.setQuantity1(exactQty1);
                        purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                        purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText())) * currency.getRate());
                        purchaseItem.setDiscount(0.00);
                        purchaseItemList.add(purchaseItem);

                        itemQty=itemQty>0?itemQty-=M:itemQty;
                        itemQty1=itemQty1>0?itemQty1-=M1:itemQty1;
                    }
                }
                if ((L > 0 && itemQty>0)||(L1 >0 && itemQty1>0)) {
                    
                    double exactQty=(itemQty<(double)L)?itemQty:(double)L;
                    double exactQty1=(itemQty1<(double)L1)?itemQty1:(double)L1;                    
                    Item item = new Item();
                    item.setItemCode(authCode);
                    item.setDescription(itemDescriptionText.getText() + " "+LLabel.getText());
//                    item.setAvailableQty(exactQty);
//                    item.setAvailableQty1(exactQty1);
                    item.setCompany(GlobalProperty.getCompany());
                    item.setGodown(godown);
                    item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                    item.setStockGroup(stockGroup);
                    item.setVisible(true);
                    item.setUnit(defaultUnit);
                    if(findReplica(item)>-1)
                    {
                        MsgBox.warning("Item Already Exists!!!");
                        break;
                    }
                    else
                    {
                        purchaseItem = new PurchaseItem();
                        purchaseItem.setItem(item);
                        purchaseItem.setQuantity(exactQty);
                        purchaseItem.setQuantity1(exactQty1);
                        purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                        purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText())) * currency.getRate());
                        purchaseItem.setDiscount(0.00);
                        purchaseItemList.add(purchaseItem);

                        itemQty=itemQty>0?itemQty-=L:itemQty;
                        itemQty1=itemQty1>0?itemQty1-=L1:itemQty1;
                    }
                }
                if ((XL > 0 && itemQty>0)||(XL1 > 0 && itemQty1>0)) {
                    double exactQty=(itemQty<(double)XL)?itemQty:(double)XL;
                    double exactQty1=(itemQty<(double)XL1)?itemQty1:(double)XL1;                    
                    Item item = new Item();
                    item.setItemCode(authCode);
                    item.setDescription(itemDescriptionText.getText() + " "+XLLabel.getText());
                    item.setCompany(GlobalProperty.getCompany());
                    item.setGodown(godown);
                    item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                    item.setStockGroup(stockGroup);
                    item.setVisible(true);
                    item.setUnit(defaultUnit);
                    if(findReplica(item)>-1)
                    {
                        MsgBox.warning("Item Already Exists!!!");
                        break;
                    }
                    else
                    {
                        purchaseItem = new PurchaseItem();
                        purchaseItem.setItem(item);
                        purchaseItem.setQuantity(exactQty);
                        purchaseItem.setQuantity1(exactQty1);
                        purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                        purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText())) * currency.getRate());
                        purchaseItem.setDiscount(0.00);
                        purchaseItemList.add(purchaseItem);

                        itemQty=itemQty>0?itemQty-=XL:itemQty;
                        itemQty1=itemQty1>0?itemQty1-=XL1:itemQty1;
                    }
                }
                if ((XXL > 0 && itemQty>0)||(XXL1 > 0 && itemQty1>0)) {
                    
                    double exactQty=(itemQty<(double)XXL)?itemQty:(double)XXL;
                    double exactQty1=(itemQty1<(double)XXL1)?itemQty1:(double)XXL1;
                    
                    Item item = new Item();
                    item.setItemCode(authCode);
                    item.setDescription(itemDescriptionText.getText() + " "+XXLLabel.getText());
                    item.setCompany(GlobalProperty.getCompany());
                    item.setGodown(godown);
                    item.setSellingPrice(Double.parseDouble(sellingPriceText.getText()));
                    item.setStockGroup(stockGroup);
                    item.setVisible(true);
                    item.setUnit(defaultUnit);
                    if(findReplica(item)>-1)
                    {
                        MsgBox.warning("Item Already Exists!!!");
                        break;
                    }
                    else
                    {
                        purchaseItem = new PurchaseItem();
                        purchaseItem.setItem(item);
                        purchaseItem.setQuantity(exactQty);
                        purchaseItem.setQuantity1(exactQty1);
                        purchaseItem.setUnitPrice((Double.parseDouble(itemUnitPriceText.getText())) * currency.getRate());
                        purchaseItem.setUnitPrice1((Double.parseDouble(itemUnitPriceText1.getText())) * currency.getRate());
                        purchaseItem.setDiscount(0.00);
                        purchaseItemList.add(purchaseItem);

                        itemQty=itemQty>0?itemQty-=XXL:itemQty;
                        itemQty1=itemQty1>0?itemQty1-=XXL1:itemQty1;
                    }
                }
                
            }
            }

            
        } catch (Exception e) {
            log.error("generateItemAndPurchaseItem:",e);
        }
    }
    //method to find exisiting item in table
    int findReplica(Item item)
    {
        int check =0;
        int index =-1;
                Iterator<PurchaseItem> purchaseItems=purchaseItemList.iterator();
                while(purchaseItems.hasNext())
                {
                    PurchaseItem pi=purchaseItems.next();
                    
                    if((pi.getItem().getDescription().toString().equals(item.getDescription().toString()))&&(pi.getItem().getStockGroup().getName().toString().equals(item.getStockGroup().getName().toString()))&&(pi.getItem().getGodown().getName().toString().equals(item.getGodown().getName().toString())))
                    {
                        check=1;
                        index= purchaseItemList.indexOf(pi);
                        break;
                    }
                   
                }
                if(check==1)
                        return index;
                else
                        return  index;
        
                
    }
    //methode to clear all fields
    void clearAllFields() {
        try {
            saveButton.setText("<HTML><U>S</U>ave<HTML>");
            billDateFormattedText.setDate(new java.util.Date());
            CommonService.clearTextFields(new JTextField[]{invoiceNumberText, supplierText, deliveryDateFormattedText, salesTaxText,categoryText,itemDescriptionText});
            CommonService.clearTextArea(new JTextArea[]{addressTextArea});
            CommonService.clearCurrencyFields(new JTextField[]{chargesText});
            itemQtyText.setText("0");
            itemQtyText1.setText("0");
            itemUnitPriceText.setText("0.00");
            itemUnitPriceText1.setText("0.00");
            sellingPriceText.setText("0.00");
            //multiCurrencyComboBox.setSelectedIndex(0);
            //taxComboBox.setSelectedIndex(0);
            //chargesComboBox.setSelectedIndex(0);
            //shipmentComboBox.setSelectedIndex(0);
          //  currencyText.setText(multiCurrencyDAO.findAll().get(0).getName());
            taxText.setText("N/A");
            chargesTextBox.setText("N/A");
            shipmentModeText.setText("N/A");
            purchaseItemList = new ArrayList<PurchaseItem>();
            purchaseTaxList = new ArrayList<PurchaseTax>();
            purchaseChargesList = new ArrayList<PurchaseCharge>();
            supplierLedger = null;
            purchaseLedger = ledgerDAO.findByLedgerName("Purchase Account");
            
            grandTotal = 0.00;
            subTotal = 0.00;
            numberProperty = numberPropertyDAO.findInvoiceNo("Purchase");
            invoiceNumberText.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
//            Object currencyItem = multiCurrencyComboBox.getSelectedItem();
//            currency = (MultiCurrency) ((ComboKeyValue) currencyItem).getValue();
            String currencySymbol = (null == currency) ? "" : currency.getSymbol();
            grandTotalLabel.setText(currencySymbol + " " + grandTotal);
            netAmountLabel.setText(currencySymbol + "" + netTotal);
            discountText.setText(grandTotal + "");
            purchaseItemTableModel.setRowCount(0);

            //availableQtyLabel.setText("");
            netAmountLabel.setText("");
            purchase = null;
            totDiscount = 0.00;
            totDiscount1 = 0.00;
            discountText.setText("0.00");
            saveButton.setText("Save");
            supplierText.requestFocusInWindow();
        } catch (Exception e) {
            log.error("clearAllFields:", e);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel InvoiceDetailsPanel;
    private javax.swing.JLabel LLabel;
    private javax.swing.JPanel LPanel;
    private javax.swing.JTextField LTextField;
    private javax.swing.JTextField LTextField1;
    private javax.swing.JLabel MLabel;
    private javax.swing.JPanel MPanel;
    private javax.swing.JTextField MTextField;
    private javax.swing.JTextField MTextField1;
    private javax.swing.JLabel SLabel;
    private javax.swing.JPanel SPanel;
    private javax.swing.JTextField STextField;
    private javax.swing.JTextField STextField1;
    private javax.swing.JLabel XLLabel;
    private javax.swing.JPanel XLPanel;
    private javax.swing.JTextField XLTextField;
    private javax.swing.JTextField XLTextField1;
    private javax.swing.JLabel XXLLabel;
    private javax.swing.JPanel XXLPanel;
    private javax.swing.JTextField XXLTextField;
    private javax.swing.JTextField XXLTextField1;
    private javax.swing.JButton addItemButton;
    private javax.swing.JTextArea addressTextArea;
    private com.toedter.calendar.JDateChooser billDateFormattedText;
    private javax.swing.JPanel bottomPanel;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField categoryText;
    private javax.swing.JButton chargesAddButton;
    private javax.swing.JButton chargesMinusButton;
    private javax.swing.JPanel chargesPanel;
    private javax.swing.JPanel chargesPanel1;
    private javax.swing.JFormattedTextField chargesText;
    private javax.swing.JFormattedTextField chargesText1;
    private javax.swing.JTextField chargesTextBox;
    private javax.swing.JButton deleteButton;
    private javax.swing.JTextField deliveryDateFormattedText;
    private javax.swing.JLabel deliveryLabel;
    private javax.swing.JPanel discountPanel;
    private javax.swing.JFormattedTextField discountText;
    private javax.swing.JFormattedTextField discountText1;
    private javax.swing.JLabel godownLabel;
    private javax.swing.JTextField godownText;
    private javax.swing.JLabel grandTotalLabel;
    private javax.swing.JPanel grandTotalPanel1;
    private javax.swing.JTextField invoiceNumberText;
    private javax.swing.JTextField itemDescriptionText;
    private javax.swing.JPanel itemInfoPanel;
    private javax.swing.JTextField itemQtyText;
    private javax.swing.JTextField itemQtyText1;
    private javax.swing.JButton itemRemoveButton;
    private javax.swing.JFormattedTextField itemUnitPriceText;
    private javax.swing.JFormattedTextField itemUnitPriceText1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel netAmountLabel;
    private javax.swing.JButton newButton;
    private javax.swing.JRadioButton pantRadioButton;
    private javax.swing.JButton printBarcodeButton;
    private javax.swing.JTable purchaseItemTable;
    private javax.swing.JPanel qtyPanel;
    private javax.swing.JTextField salesTaxText;
    private javax.swing.JButton saveButton;
    private javax.swing.JPanel sellingPricePanel;
    private javax.swing.JFormattedTextField sellingPriceText;
    private javax.swing.JPanel shipmentModePanel;
    private javax.swing.JPanel shipmentModePanel1;
    private javax.swing.JTextField shipmentModeText;
    private javax.swing.JRadioButton shirtRadioButton;
    private javax.swing.JPanel sizePanel;
    private javax.swing.JTextField supplierText;
    private javax.swing.JPanel tableBorderPanel;
    private javax.swing.JButton taxAddButton;
    private javax.swing.JPanel taxPanel;
    private javax.swing.JTextField taxText;
    private javax.swing.JButton totDiscountAddButton;
    private javax.swing.JPanel unitPricePanel;
    // End of variables declaration//GEN-END:variables
}
