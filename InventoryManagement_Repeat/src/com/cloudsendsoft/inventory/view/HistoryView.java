/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.dao.ContraDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.JournalDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.PaymentDAO;
import com.cloudsendsoft.inventory.dao.PurchaseDAO;
import com.cloudsendsoft.inventory.dao.PurchaseOrderDAO;
import com.cloudsendsoft.inventory.dao.PurchaseOrderItemDAO;
import com.cloudsendsoft.inventory.dao.PurchaseReturnDAO;
import com.cloudsendsoft.inventory.dao.ReceiptDAO;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.dao.SalesReturnDAO;
import com.cloudsendsoft.inventory.dao.StockGroupDAO;
import com.cloudsendsoft.inventory.dao.StockMovementDAO;
import com.cloudsendsoft.inventory.model.Contra;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Journal;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.model.Payment;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseOrder;
import com.cloudsendsoft.inventory.model.PurchaseOrderItem;
import com.cloudsendsoft.inventory.model.PurchaseReturn;
import com.cloudsendsoft.inventory.model.Receipt;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesReturn;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.model.StockMovement;
import com.cloudsendsoft.inventory.report.JTableReport;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import static com.cloudsendsoft.inventory.view.PurchaseView.log;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.text.DateFormatSymbols;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class HistoryView extends javax.swing.JPanel {

    static final Logger log = Logger.getLogger(HistoryView.class.getName());

    CommonService commonService = new CommonService();

    DefaultTableModel historyTableModel = null;

    List<StockGroup> listOfStockGroup = null;
    List<Item> listOfItem = null;
    //List<PurchaseItem> listOfPurchaseItem=null;
    List<SalesReturn> listOfSalesReturn = null;
    List<Purchase> listOfPurchase = null;

    StockGroup stockGroup = null;
    Item item = null;
    String month = null;

    StockGroupDAO stockGroupDAO = new StockGroupDAO();
    ItemDAO itemDAO = new ItemDAO();
    SalesReturnDAO salesReturnDAO = new SalesReturnDAO();
    PurchaseDAO purchaseDAO = new PurchaseDAO();
    Purchase purchaseList = null;
    List<Sales> listOfSales = null;
    List<StockMovement> listOfStockMovements=null;
    SalesDAO salesDAO = new SalesDAO();
    Sales salesList = null;
    List<SalesPreforma> listOfSalesPreforma = null;
    SalesPreformaDAO salesPreformaDAO = new SalesPreformaDAO();
    SalesPreforma salesPreformaList = null;
    List<PurchaseReturn> listOfPurchaseReturn = null;
    PurchaseReturnDAO purchaseReturnDAO = new PurchaseReturnDAO();
    PurchaseReturn purchaseReturn = null;
    PaymentDAO paymentDAO = new PaymentDAO();
    StockMovementDAO stockMovementDAO=new StockMovementDAO();
    List<Payment> listOfPayment = null;
    Payment paymentList = null;

    List<Receipt> listOfReceipt = null;
    ReceiptDAO receiptDAO = new ReceiptDAO();
    Receipt receipt = null;
    Receipt receiptList = null;

    List<Journal> listOfJournal = null;
    JournalDAO journalDAO = new JournalDAO();
    Journal journalList = null;
    Journal journal = null;

    List<Contra> listOfContra = null;
    ContraDAO contraDAO = new ContraDAO();
    Contra contra = null;
    Contra contraList = null;
    
    StockMovement stockMove=new StockMovement();
    List<StockMovement> stockMovementDetails=new ArrayList<>();
    String historyTableClick;

    List<PurchaseOrderItem> listOfPurchaseOrderItem = null;
    List<PurchaseOrder> listOfPurchaseOrder = null;
    PurchaseOrderDAO purchaseOrderDAO = new PurchaseOrderDAO();
    PurchaseOrderItemDAO purchaseOrderItemDAO = new PurchaseOrderItemDAO();
    PurchaseOrder purchaseOrder = null;
    PurchaseOrder purchaseOrderList = null;

    //List<Sales> listOfSales=null;
    //PurchaseItemDAO purchaseItemDAO=new PurchaseItemDAO();
    //SalesItemDAO salesItemDAO=new SalesItemDAO();
    //SalesDAO salesDAO=new SalesDAO();
    List<Ledger> listOfLedger = null;
    Ledger ledgerList = null;
    SalesReturn salesReturn = null;
    LedgerDAO ledgerDAO = new LedgerDAO();
    //SalesReturn salesReturn=null;

    int selectedIndex = -1;
    int screen = 1;
    int index = 0;
    String historyType;
    String[] months = new DateFormatSymbols().getMonths();

    boolean isDuplicateColumnsVisible = false;
    
    public HistoryView(String historyType) {
        initComponents();
        
        //shortcut
            // HERE ARE THE KEY BINDINGS
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_W,
                                    java.awt.event.InputEvent.SHIFT_DOWN_MASK
                                    | java.awt.event.InputEvent.CTRL_DOWN_MASK), "forward");
            this.getActionMap().put("forward", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(isDuplicateColumnsVisible){
                       isDuplicateColumnsVisible = false; 
                    }else{
                        isDuplicateColumnsVisible = true; 
                    }
                    fillHistoryTable();
                                
                }
            });
            // END OF KEY BINDINGS
            //shortcut
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
             
                if(historyTable.getRowCount()>0)
                {
                    historyTable.setRowSelectionInterval(0,0);
                    historyTable.requestFocus();
                }

              }
        });
        commonService.setCurrentPeriodStartOnCalendar(fromDateChooser);
        commonService.setCurrentPeriodEndOnCalendar(toDateChooser);
        
        this.historyType = historyType;
        //table background color settings
        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);
        historyTable.setBackground(this.getBackground());
       // historyTable.getTableHeader().setBackground(this.getBackground());
       // historyTable.getTableHeader().setForeground(this.getBackground());

        //set table design
        historyTable.setRowHeight(GlobalProperty.tableRowHieght);
        historyTable.setFont(GlobalProperty.getFont());

        //listOfSalesReturn= salesReturnDAO.findAllSalesReturn();
        
        try {
            int slNo = 1;
            if ("PurchaseOrder".equalsIgnoreCase(historyType.trim())) {
                listOfPurchaseOrder = purchaseOrderDAO.findAllPurchase();

                

            }
            if ("CommercialInvoice".equalsIgnoreCase(historyType.trim())) {
                listOfSalesPreforma = salesPreformaDAO.findAllSalesPreformaCommercialInvoice();

                
            }
            if ("SalesReturn".equalsIgnoreCase(historyType.trim())) {
                listOfSalesReturn = salesReturnDAO.findAllSalesReturn();

                
            }
            if ("Purchase".equalsIgnoreCase(historyType.trim())) {
                listOfPurchase = purchaseDAO.findAllPurchase();
                
            }
            if ("Sales".equalsIgnoreCase(historyType.trim())) {
                listOfSales = salesDAO.findAllSales();

                

            }
            if ("SalesPreforma".equalsIgnoreCase(historyType.trim())) {
                listOfSalesPreforma = salesPreformaDAO.findAllSalesPreforma();

                
            }
            if ("PurchaseReturn".equalsIgnoreCase(historyType.trim())) {
                listOfPurchaseReturn = purchaseReturnDAO.findAllPurchaseReturn();

                
            }
            if ("Payment".equalsIgnoreCase(historyType.trim())) {
                listOfPayment = paymentDAO.findAllPayment();

                

            }
            if ("Receipt".equalsIgnoreCase(historyType.trim())) {
                listOfReceipt = receiptDAO.findAllReceipt();
                

            }
            if ("Journal".equalsIgnoreCase(historyType.trim())) {
                listOfJournal = journalDAO.findAllJournal();

                
            }
            if ("Contra".equalsIgnoreCase(historyType.trim())) {
                listOfContra = contraDAO.findAllContra();

                }
            if("StockMove".equalsIgnoreCase(historyType.trim()))
            {
             
              listOfStockMovements=stockMovementDAO.findAll();
            }

        } catch (Exception e) {
            log.error("fillStockSummaryTable:", e);
        }
        
        
        fromDateChooser.setDate(GlobalProperty.company.getStartDate());
        toDateChooser.setDate(GlobalProperty.company.getEndDate());
        fromDateChooser.setMinSelectableDate(GlobalProperty.company.getStartDate());
        toDateChooser.setMaxSelectableDate(GlobalProperty.company.getEndDate());
        
        fillHistoryTable();
    }

    void fillHistoryTable() {
        try {
            int slNo = 1;
            if ("PurchaseOrder".equalsIgnoreCase(historyType.trim())) {
                //listOfPurchaseOrder = purchaseOrderDAO.findAllPurchase();

                leftHeadingLabel.setText("Purchase Order History");
                
                     //table columns,model & size
                //rightHeadingLabel.setText("Items Purchased by the User");
                historyTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                        null,
                        new String[]{"Sr. No.", "Invoice No", "Name", "Bill Date", "Delivery Date", "Currency", "ShipmentMode", "Discount", "Grand Total"}
                ));
                historyTableModel = (DefaultTableModel) historyTable.getModel();
                CommonService.setWidthAsPercentages(historyTable, .1, .2, .3, .2, .2, .3, .3, .3, .3);

                historyTableModel.setRowCount(0);

                for (PurchaseOrder purchaseOrderList : listOfPurchaseOrder) {

                    historyTableModel.addRow(new Object[]{slNo++, purchaseOrderList.getInvoiceNumber(), purchaseOrderList.getSupplier().getLedgerName(), commonService.sqlDateToString(purchaseOrderList.getBillDate()), (purchaseOrderList.getDeliveryDate()), purchaseOrderList.getMultiCurrency().getName(),(purchaseOrderList.getShipmentMode()==null)?"":purchaseOrderList.getShipmentMode().getName(), purchaseOrderList.getMultiCurrency().getSymbol() + " " +commonService.formatIntoCurrencyAsString(purchaseOrderList.getDiscount()/purchaseOrderList.getCurrentCurrencyRate()), purchaseOrderList.getMultiCurrency().getSymbol() + " " +commonService.formatIntoCurrencyAsString(purchaseOrderList.getGrandTotal() / purchaseOrderList.getCurrentCurrencyRate())});

                }

            }
            if ("CommercialInvoice".equalsIgnoreCase(historyType.trim())) {
                leftHeadingLabel.setText("Commercial Invoice History");
                //table columns,model & size
                //rightHeadingLabel.setText("Items Purchased by the User");
                historyTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                        null,
                        new String[]{"Sr. No.", "Invoice No", "Name", "Bill Date", "Delivery", "Currency", "ShipmentMode", "Discount", "Grand Total"}
                ));
                historyTableModel = (DefaultTableModel) historyTable.getModel();
                CommonService.setWidthAsPercentages(historyTable, .03, .3, .5, .3, .3, .3, .3, .3, .3);

                historyTableModel.setRowCount(0);

                for (SalesPreforma salesPreformaList : listOfSalesPreforma) {

                    historyTableModel.addRow(new Object[]{slNo++, salesPreformaList.getCommercialInvoice().getInvoiceNumber(), salesPreformaList.getCustomer().getLedgerName(), commonService.sqlDateToString(salesPreformaList.getCommercialInvoice().getBillDate()), salesPreformaList.getDelivery(), salesPreformaList.getMultiCurrency().getName(), (salesPreformaList.getShipmentMode()==null)?"":salesPreformaList.getShipmentMode().getName(), salesPreformaList.getMultiCurrency().getSymbol()+ " "+commonService.formatIntoCurrencyAsString(salesPreformaList.getCommercialInvoice().getDiscount()/salesPreformaList.getCurrentCurrencyRate()), salesPreformaList.getMultiCurrency().getSymbol()+" "+commonService.formatIntoCurrencyAsString(salesPreformaList.getCommercialInvoice().getGrandTotal()/salesPreformaList.getCurrentCurrencyRate())});

                }
            }
            if ("SalesReturn".equalsIgnoreCase(historyType.trim())) {
               // listOfSalesReturn = salesReturnDAO.findAllSalesReturn();

                leftHeadingLabel.setText("Sales Return History");
                //table columns,model & size
                //rightHeadingLabel.setText("Items Purchased by the User");
                historyTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                        null,
                        new String[]{"Sr. No.", "Invoice No", "Name", "Bill Date", "Currency", "Grand Total"}
                ));
                historyTableModel = (DefaultTableModel) historyTable.getModel();
                CommonService.setWidthAsPercentages(historyTable, .05, .4, .6, .3, .3, .3);

                historyTableModel.setRowCount(0);

                for (SalesReturn salesReturnList : listOfSalesReturn) {

                    historyTableModel.addRow(new Object[]{slNo++, salesReturnList.getInvoiceNumber(), salesReturnList.getCustomer().getLedgerName(), commonService.sqlDateToString(salesReturnList.getBillDate()), salesReturnList.getMultiCurrency().getName(), salesReturnList.getMultiCurrency().getSymbol()+" "+commonService.formatIntoCurrencyAsString(salesReturnList.getGrandTotal()/salesReturnList.getSales().getCurrentCurrencyRate())});

                }
            }
            if ("Purchase".equalsIgnoreCase(historyType.trim())) {
                //listOfPurchase = purchaseDAO.findAllPurchase();
                leftHeadingLabel.setText("Purchase History");
                
                //table columns,model & size
                //rightHeadingLabel.setText("Items Purchased by the User");
                
                historyTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                        null,
                        new String[]{"Sr. No.", "Invoice No", "Name", "Bill Date", "Currency", "Discount", "Grand Total"}
                ));
                historyTableModel = (DefaultTableModel) historyTable.getModel();
                CommonService.setWidthAsPercentages(historyTable, .1, .2, .3, .2,  .3,.3, .3);

                historyTableModel.setRowCount(0);

                for (Purchase purchaseList : listOfPurchase) {

                    if(isDuplicateColumnsVisible){
                        historyTableModel.addRow(new Object[]{slNo++, purchaseList.getInvoiceNumber(), purchaseList.getSupplier().getLedgerName(), commonService.sqlDateToString(purchaseList.getBillDate()),  purchaseList.getMultiCurrency().getName(),  purchaseList.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString((purchaseList.getDiscount()+purchaseList.getDiscount1())/purchaseList.getCurrentCurrencyRate()), purchaseList.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString((purchaseList.getGrandTotal()+purchaseList.getGrandTotal1()) / purchaseList.getCurrentCurrencyRate())});
                    }else{
                        historyTableModel.addRow(new Object[]{slNo++, purchaseList.getInvoiceNumber(), purchaseList.getSupplier().getLedgerName(), commonService.sqlDateToString(purchaseList.getBillDate()),  purchaseList.getMultiCurrency().getName(),  purchaseList.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString(purchaseList.getDiscount()/purchaseList.getCurrentCurrencyRate()), purchaseList.getMultiCurrency().getSymbol() + " " + commonService.formatIntoCurrencyAsString(purchaseList.getGrandTotal() / purchaseList.getCurrentCurrencyRate())});
                    }
                }
            }
            if ("Sales".equalsIgnoreCase(historyType.trim())) {
                //listOfSales = salesDAO.findAllSales();

                leftHeadingLabel.setText("Sales History");
                //table columns,model & size
                //rightHeadingLabel.setText("Items Purchased by the User");
                historyTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                        null,
                        new String[]{"Sr. No.", "Invoice No", "Name", "Bill Date",  "Currency",  "Discount", "Grand Total"}
                ));
                historyTableModel = (DefaultTableModel) historyTable.getModel();
                CommonService.setWidthAsPercentages(historyTable, .05, .3, .5, .3, .3, .3, .4);

                historyTableModel.setRowCount(0);

                for (Sales salesList : listOfSales) {
                    if(isDuplicateColumnsVisible){
                        historyTableModel.addRow(new Object[]{slNo++, salesList.getInvoiceNumber(), salesList.getCustomer().getLedgerName(), commonService.sqlDateToString(salesList.getBillDate()), salesList.getMultiCurrency().getName(),  salesList.getMultiCurrency().getSymbol()+" "+commonService.formatIntoCurrencyAsString(salesList.getDiscount()/salesList.getCurrentCurrencyRate()), salesList.getMultiCurrency().getSymbol()+""+commonService.formatIntoCurrencyAsString((salesList.getGrandTotal()+salesList.getGrandTotal1())/salesList.getCurrentCurrencyRate())});
                    }else{
                        historyTableModel.addRow(new Object[]{slNo++, salesList.getInvoiceNumber(), salesList.getCustomer().getLedgerName(), commonService.sqlDateToString(salesList.getBillDate()),  salesList.getMultiCurrency().getName(),  salesList.getMultiCurrency().getSymbol()+" "+commonService.formatIntoCurrencyAsString(salesList.getDiscount()/salesList.getCurrentCurrencyRate()), salesList.getMultiCurrency().getSymbol()+""+commonService.formatIntoCurrencyAsString(salesList.getGrandTotal()/salesList.getCurrentCurrencyRate())});
                    }
                    

                }

            }
            if ("SalesPreforma".equalsIgnoreCase(historyType.trim())) {
               // listOfSalesPreforma = salesPreformaDAO.findAllSalesPreforma();

                leftHeadingLabel.setText("SalesPreforma History");
                //table columns,model & size
                //rightHeadingLabel.setText("Items Purchased by the User");
                historyTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                        null,
                        new String[]{"Sr. No.", "Invoice No", "Name", "Bill Date", "Delivery Date", "Currency", "ShipmentMode", "Discount", "Grand Total"}
                ));
                historyTableModel = (DefaultTableModel) historyTable.getModel();
                CommonService.setWidthAsPercentages(historyTable, .03, .4, .5, .3, .3, .3, .3, .3, .3);

                historyTableModel.setRowCount(0);

                for (SalesPreforma salesPreformaList : listOfSalesPreforma) {

                    historyTableModel.addRow(new Object[]{slNo++, salesPreformaList.getInvoiceNumber(), salesPreformaList.getCustomer().getLedgerName(), commonService.sqlDateToString(salesPreformaList.getBillDate()), salesPreformaList.getDelivery(), salesPreformaList.getMultiCurrency().getName(), (salesPreformaList.getShipmentMode()==null)?"":salesPreformaList.getShipmentMode().getName(), salesPreformaList.getMultiCurrency().getSymbol()+" "+commonService.formatIntoCurrencyAsString(salesPreformaList.getDiscount()/salesPreformaList.getCurrentCurrencyRate()), salesPreformaList.getMultiCurrency().getSymbol()+" "+commonService.formatIntoCurrencyAsString(salesPreformaList.getGrandTotal()/salesPreformaList.getCurrentCurrencyRate())});

                }
            }
            if ("PurchaseReturn".equalsIgnoreCase(historyType.trim())) {
                //listOfPurchaseReturn = purchaseReturnDAO.findAllPurchaseReturn();

                leftHeadingLabel.setText("Purchase Return History");
                //table columns,model & size
                //rightHeadingLabel.setText("Items Purchased by the User");
                historyTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                        null,
                        new String[]{"Sr. No.", "Invoice No", "Name", "Bill Date", "Currency", "Grand Total",}
                ));
                historyTableModel = (DefaultTableModel) historyTable.getModel();
                CommonService.setWidthAsPercentages(historyTable, .05, .4, .6, .3, .3, .4);

                historyTableModel.setRowCount(0);

                for (PurchaseReturn purchaseReturnList : listOfPurchaseReturn) {

                    historyTableModel.addRow(new Object[]{slNo++, purchaseReturnList.getInvoiceNumber(), purchaseReturnList.getSupplier().getLedgerName(), commonService.sqlDateToString(purchaseReturnList.getBillDate()), purchaseReturnList.getMultiCurrency().getName(), purchaseReturnList.getMultiCurrency().getSymbol()+" "+commonService.formatIntoCurrencyAsString(purchaseReturnList.getGrandTotal()/purchaseReturnList.getPurchase().getCurrentCurrencyRate())});

                }
            }
            if ("Payment".equalsIgnoreCase(historyType.trim())) {
                //listOfPayment = paymentDAO.findAllPayment();

                leftHeadingLabel.setText("Payment History");
                //table columns,model & size
                //rightHeadingLabel.setText("Items Purchased by the User");
                historyTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                        null,
                        new String[]{"Sr. No.", "Payment No", "Payment Date", "Ledger By", "Ledger To", "Narration", "Amount"}
                ));
                historyTableModel = (DefaultTableModel) historyTable.getModel();
                CommonService.setWidthAsPercentages(historyTable, .3, .3, .3, .5,.5, .8, .3);

                historyTableModel.setRowCount(0);

                for (Payment paymentList : listOfPayment) {

                    historyTableModel.addRow(new Object[]{slNo++, paymentList.getPaymentNo(), commonService.sqlDateToString(paymentList.getPaymentDate()), paymentList.getLedgerBy().getLedgerName(), paymentList.getLedgerTo().getLedgerName(), paymentList.getNarration(), commonService.formatIntoCurrencyAsString(paymentList.getAmount())});

                }

            }
            if ("Receipt".equalsIgnoreCase(historyType.trim())) {
               // listOfReceipt = receiptDAO.findAllReceipt();
                leftHeadingLabel.setText("Receipt History");
                //table columns,model & size
                //rightHeadingLabel.setText("Items Purchased by the User");
                historyTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                        null,
                        new String[]{"Sr. No.", "Receipt No", "Receipt Date", "Ledger By", "Ledger To", "Narration", "Amount"}
                ));
                historyTableModel = (DefaultTableModel) historyTable.getModel();
                CommonService.setWidthAsPercentages(historyTable, .3, .3, .3, .5,.5, .8, .3);

                historyTableModel.setRowCount(0);

                for (Receipt receiptList : listOfReceipt) {

                    historyTableModel.addRow(new Object[]{slNo++, receiptList.getReceiptNo(), commonService.sqlDateToString(receiptList.getReceiptDate()), receiptList.getLedgerBy().getLedgerName(), receiptList.getLedgerTo().getLedgerName(), receiptList.getNarration(), commonService.formatIntoCurrencyAsString(receiptList.getAmount())});

                }

            }
            if ("Journal".equalsIgnoreCase(historyType.trim())) {
                //listOfJournal = journalDAO.findAllJournal();

                leftHeadingLabel.setText("Journal History");
                //table columns,model & size
                //rightHeadingLabel.setText("Items Purchased by the User");
                historyTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                        null,
                        new String[]{"Sr. No.", "Journal No", "Journal Date", "Ledger By", "Ledger To", "Narration", "Amount"}
                ));
                historyTableModel = (DefaultTableModel) historyTable.getModel();
                CommonService.setWidthAsPercentages(historyTable, .3, .3, .3, .5,.5, .8, .3);

                historyTableModel.setRowCount(0);

                for (Journal journalList : listOfJournal) {

                    historyTableModel.addRow(new Object[]{slNo++, journalList.getJournalNo(), commonService.sqlDateToString(journalList.getJournalDate()), journalList.getLedgerBy().getLedgerName(), journalList.getLedgerTo().getLedgerName(), journalList.getNarration(), commonService.formatIntoCurrencyAsString(journalList.getAmount())});

                }
            }
            if ("Contra".equalsIgnoreCase(historyType.trim())) {
                //listOfContra = contraDAO.findAllContra();

                leftHeadingLabel.setText("Contra History");
                //table columns,model & size
                //rightHeadingLabel.setText("Items Purchased by the User");
                historyTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                        null,
                        new String[]{"Sr. No.", "Contra No", "Contra Date", "Ledger By", "Ledger To", "Narration", "Amount"}
                ));
                historyTableModel = (DefaultTableModel) historyTable.getModel();
                CommonService.setWidthAsPercentages(historyTable, .3, .3, .3, .5,.5, .8, .3);

                historyTableModel.setRowCount(0);

                for (Contra contraList : listOfContra) {

                    historyTableModel.addRow(new Object[]{slNo++, contraList.getContraNo(), commonService.sqlDateToString(contraList.getContraDate()), contraList.getLedgerBy().getLedgerName(), contraList.getLedgerTo().getLedgerName(), contraList.getNarration(), commonService.formatIntoCurrencyAsString(contraList.getAmount())});

                }
            }
            if ("StockMove".equalsIgnoreCase(historyType.trim())) {
                 System.out.println("Removing search..........");
                 //headingPanel.remove(searchButton);
                // headingPanel.remove(searchInvoiceText);
                 searchButton.setVisible(false); 
                 searchInvoiceText.setVisible(false);
                 jLabel3.setVisible(false); 
                leftHeadingLabel.setText("Stock Move History");
                historyTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                        null,
                        new String[]{"Sr.No.", "Item Code", "Item Name","Date Of Movement","Available Qty","From(Godown)","Moved Quantity","To(Godown)"}
                ));
                historyTableModel = (DefaultTableModel) historyTable.getModel();
                CommonService.setWidthAsPercentages(historyTable, .05, .4, .6, .4, .3, .4, .3,.4);
                historyTableModel.setRowCount(0);
                 if(listOfStockMovements.size()!=0)
                 {
                    for (StockMovement stockMoveList : listOfStockMovements) {
                     if(isDuplicateColumnsVisible)
                     {    
                       
                       historyTableModel.addRow(new Object[]{slNo++, stockMoveList.getItemCode(), stockMoveList.getName() ,commonService.sqlDateToString(stockMoveList.getDate()),(stockMoveList.getAvailableQty()+stockMoveList.getAvailableQty1()),stockMoveList.getFromGodown(),(stockMoveList.getQtyToMove()+stockMoveList.getQtyToMove1()),stockMoveList.getToGodown()});  
                     }
                     else
                     historyTableModel.addRow(new Object[]{slNo++, stockMoveList.getItemCode(), stockMoveList.getName() ,commonService.sqlDateToString(stockMoveList.getDate()),stockMoveList.getAvailableQty(),stockMoveList.getFromGodown(),stockMoveList.getQtyToMove(),stockMoveList.getToGodown()});
                    }
                 }
                 else
                 {
                  MsgBox.warning("No stock Movements to show......!!"); 
                 }
             }

        } catch (Exception e) {
            log.error("fillStockSummaryTable:", e);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headingPanel = new javax.swing.JPanel();
        leftHeadingLabel = new javax.swing.JLabel();
        searchInvoiceText = new javax.swing.JTextField();
        searchButton = new javax.swing.JButton();
        fromDateChooser = new com.toedter.calendar.JDateChooser();
        toDateChooser = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        printButton = new javax.swing.JButton();
        pdfButton = new javax.swing.JButton();
        bodyPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        historyTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        headingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        headingPanel.setOpaque(false);
        headingPanel.setPreferredSize(new java.awt.Dimension(1124, 50));

        leftHeadingLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        leftHeadingLabel.setText("Purchase History");

        searchButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/search.png"))); // NOI18N
        searchButton.setText("Search");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });

        fromDateChooser.setDateFormatString("dd/MM/yyyy");

        toDateChooser.setDateFormatString("dd/MM/yyyy");

        jLabel1.setText("From");

        jLabel2.setText("To");

        jLabel3.setText("OR    Invoice Number");

        printButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/print.png"))); // NOI18N
        printButton.setText("Print");
        printButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printButtonActionPerformed(evt);
            }
        });

        pdfButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/pdf-icon.png"))); // NOI18N
        pdfButton.setText("PDF");
        pdfButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdfButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout headingPanelLayout = new javax.swing.GroupLayout(headingPanel);
        headingPanel.setLayout(headingPanelLayout);
        headingPanelLayout.setHorizontalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addComponent(leftHeadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fromDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(toDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchInvoiceText, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pdfButton, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(printButton, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );
        headingPanelLayout.setVerticalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, headingPanelLayout.createSequentialGroup()
                        .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(fromDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)
                            .addComponent(toDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(searchInvoiceText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(searchButton)
                            .addComponent(printButton)
                            .addComponent(pdfButton))
                        .addContainerGap())
                    .addGroup(headingPanelLayout.createSequentialGroup()
                        .addComponent(leftHeadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11))))
        );

        add(headingPanel, java.awt.BorderLayout.PAGE_START);

        bodyPanel.setOpaque(false);

        historyTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        historyTable.setOpaque(false);
        historyTable.setSelectionBackground(new java.awt.Color(153, 153, 153));
        historyTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                historyTableMouseClicked(evt);
            }
        });
        historyTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                historyTableKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(historyTable);

        javax.swing.GroupLayout bodyPanelLayout = new javax.swing.GroupLayout(bodyPanel);
        bodyPanel.setLayout(bodyPanelLayout);
        bodyPanelLayout.setHorizontalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1124, Short.MAX_VALUE)
        );
        bodyPanelLayout.setVerticalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 397, Short.MAX_VALUE)
        );

        add(bodyPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void historyTableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_historyTableKeyPressed
       if(evt.getKeyCode()==KeyEvent.VK_ENTER)
       {    selectedIndex = historyTable.getSelectedRow();
           clickAction();
       }

    }//GEN-LAST:event_historyTableKeyPressed

    void clickAction() {
        try {
                   

            if ("PurchaseOrder".equalsIgnoreCase(historyType.trim())) {
                purchaseOrderList = listOfPurchaseOrder.get(selectedIndex);
                PurchaseOrderView purchaseOrderView = new PurchaseOrderView(MainForm.jMainFrame);
                purchaseOrderView.listPurchaseOrderItemHistory(purchaseOrderList);
                //PurchaseReturnItemView purchaseReturnItemView=new PurchaseReturnItemView(purchaseList);
                GlobalProperty.mainForm.getContentPanel().removeAll();
                GlobalProperty.mainForm.getContentPanel().repaint();
                GlobalProperty.mainForm.getContentPanel().revalidate();
                GlobalProperty.mainForm.getContentPanel().add(purchaseOrderView);
            }
            if ("CommercialInvoice".equalsIgnoreCase(historyType.trim())) {
                salesPreformaList = listOfSalesPreforma.get(selectedIndex);
                CommercialInvoiceView commercialInvoiceView = new CommercialInvoiceView(salesPreformaList);
                GlobalProperty.mainForm.getContentPanel().removeAll();
                GlobalProperty.mainForm.getContentPanel().repaint();
                GlobalProperty.mainForm.getContentPanel().revalidate();
                GlobalProperty.mainForm.getContentPanel().add(commercialInvoiceView);
            }

            if ("SalesReturn".equalsIgnoreCase(historyType.trim())) {
                salesReturn = listOfSalesReturn.get(selectedIndex);
                SalesReturnItemView salesReturnItemView = new SalesReturnItemView(salesReturn.getSales());
                salesReturnItemView.listSalesReturnItemHistory(salesReturn);
                GlobalProperty.mainForm.getContentPanel().removeAll();
                GlobalProperty.mainForm.getContentPanel().repaint();
                GlobalProperty.mainForm.getContentPanel().revalidate();
                GlobalProperty.mainForm.getContentPanel().add(salesReturnItemView);
            }
            if ("Purchase".equalsIgnoreCase(historyType.trim())) {
                purchaseList = listOfPurchase.get(selectedIndex);
                PurchaseView1 purchaseView = new PurchaseView1(MainForm.jMainFrame);
                purchaseView.listPurchaseItemHistory(purchaseList);
                GlobalProperty.mainForm.getContentPanel().removeAll();
                GlobalProperty.mainForm.getContentPanel().repaint();
                GlobalProperty.mainForm.getContentPanel().revalidate();
                GlobalProperty.mainForm.getContentPanel().add(purchaseView);
            }
            if ("Sales".equalsIgnoreCase(historyType.trim())) {
                salesList = listOfSales.get(selectedIndex);
                SalesView salesView = new SalesView(MainForm.jMainFrame);
                salesView.listSalesItemHistory(salesList);
                GlobalProperty.mainForm.getContentPanel().removeAll();
                GlobalProperty.mainForm.getContentPanel().repaint();
                GlobalProperty.mainForm.getContentPanel().revalidate();
                GlobalProperty.mainForm.getContentPanel().add(salesView);
            }
            if ("SalesPreforma".equalsIgnoreCase(historyType.trim())) {
                salesPreformaList = listOfSalesPreforma.get(selectedIndex);
                SalesPreformaView salesPreformaView = new SalesPreformaView(MainForm.jMainFrame);
                salesPreformaView.listSalesPreformaItemHistory(salesPreformaList);
                GlobalProperty.mainForm.getContentPanel().removeAll();
                GlobalProperty.mainForm.getContentPanel().repaint();
                GlobalProperty.mainForm.getContentPanel().revalidate();
                GlobalProperty.mainForm.getContentPanel().add(salesPreformaView);
            }
            if ("PurchaseReturn".equalsIgnoreCase(historyType.trim())) {
                purchaseReturn = listOfPurchaseReturn.get(selectedIndex);
                PurchaseReturnItemView purchaseReturnItemView = new PurchaseReturnItemView(purchaseReturn.getPurchase());
                purchaseReturnItemView.listPurchaseReturnItemHistory(purchaseReturn);
                GlobalProperty.mainForm.getContentPanel().removeAll();
                GlobalProperty.mainForm.getContentPanel().repaint();
                GlobalProperty.mainForm.getContentPanel().revalidate();
                GlobalProperty.mainForm.getContentPanel().add(purchaseReturnItemView);
            }
            if ("Payment".equalsIgnoreCase(historyType.trim())) {
                paymentList = listOfPayment.get(selectedIndex);
                JournalEntryView journalEntryView = new JournalEntryView("Payment");
                journalEntryView.listPaymentItemHistory(paymentList);
                GlobalProperty.mainForm.getContentPanel().removeAll();
                GlobalProperty.mainForm.getContentPanel().repaint();
                GlobalProperty.mainForm.getContentPanel().revalidate();
                GlobalProperty.mainForm.getContentPanel().add(journalEntryView);
            }
            if ("Receipt".equalsIgnoreCase(historyType.trim())) {
                receiptList = listOfReceipt.get(selectedIndex);
                JournalEntryView journalEntryView = new JournalEntryView("Receipt");
                journalEntryView.listReceiptItemHistory(receiptList);
                GlobalProperty.mainForm.getContentPanel().removeAll();
                GlobalProperty.mainForm.getContentPanel().repaint();
                GlobalProperty.mainForm.getContentPanel().revalidate();
                GlobalProperty.mainForm.getContentPanel().add(journalEntryView);
            }
            if ("Journal".equalsIgnoreCase(historyType.trim())) {
                journalList = listOfJournal.get(selectedIndex);
                JournalEntryView journalEntryView = new JournalEntryView("Journal");
                journalEntryView.listJournalItemHistory(journalList);
                GlobalProperty.mainForm.getContentPanel().removeAll();
                GlobalProperty.mainForm.getContentPanel().repaint();
                GlobalProperty.mainForm.getContentPanel().revalidate();
                GlobalProperty.mainForm.getContentPanel().add(journalEntryView);
            }
            if ("Contra".equalsIgnoreCase(historyType.trim())) {
                contraList = listOfContra.get(selectedIndex);
                JournalEntryView journalEntryView = new JournalEntryView("Contra");
                journalEntryView.listContraItemHistory(contraList);
                GlobalProperty.mainForm.getContentPanel().removeAll();
                GlobalProperty.mainForm.getContentPanel().repaint();
                GlobalProperty.mainForm.getContentPanel().revalidate();
                GlobalProperty.mainForm.getContentPanel().add(journalEntryView);
            }
            if ("StockMove".equalsIgnoreCase(historyType.trim())) {
                 
                 int row=historyTable.getSelectedRow();
                 historyTableClick=historyTable.getModel().getValueAt(row,1).toString();
                 String tableClickTotalAvail=historyTable.getModel().getValueAt(row,4).toString();
                 String tableClickQtyToMoveTotal=historyTable.getModel().getValueAt(row,6).toString();
                 stockMove = listOfStockMovements.get(selectedIndex);
                 StockMoveFrameView stockMoveFrameView=new StockMoveFrameView();
                 stockMoveFrameView.listStockMoveHistory(stockMove); 
                 stockMovementDetails=stockMovementDAO.findAllByItemCode(historyTableClick);
                 double availQty=stockMove.getAvailableQty();
                 double availQty1=stockMove.getAvailableQty1();
                 double availQtyTotal=availQty + availQty1;
                 double qtyToMove=stockMove.getQtyToMove();
                 double qtyToMove1=stockMove.getQtyToMove1();
                 double qtyToMoveTotal=qtyToMove + qtyToMove1; 
                 double clickAvailTotal=Double.parseDouble(tableClickTotalAvail);
                 double clickQtyMoveTotal=Double.parseDouble(tableClickQtyToMoveTotal);
                if(qtyToMoveTotal==clickQtyMoveTotal && availQtyTotal==clickAvailTotal)
                {
                   
                    stockMoveFrameView.addStockMoveDuplicateColumns();
                    stockMoveFrameView.listStockMoveHistory(stockMove);
                    GlobalProperty.mainForm.getjDesktopPane().add(stockMoveFrameView);
                    stockMoveFrameView.setVisible(true);
                }    
                else
                 {
                  stockMoveFrameView.listStockMoveHistory(stockMove);
                  GlobalProperty.mainForm.getjDesktopPane().add(stockMoveFrameView);
                  stockMoveFrameView.setVisible(true); 
                 }
             }
        } catch (Exception e) {
            log.error("clickAction:", e);
        }
    }
    private void historyTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_historyTableMouseClicked
        try {
            int row1 = historyTable.getSelectedRow();
            historyTableClick=historyTable.getModel().getValueAt(row1,1).toString();
            if (evt.getClickCount() == 2) {
                selectedIndex = historyTable.rowAtPoint(evt.getPoint());
                clickAction();
            }
        } catch (Exception e) {
            log.error("salesHistoryTableMouseClicked", e);
        }

    }//GEN-LAST:event_historyTableMouseClicked

    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed
        try {
            if ("PurchaseOrder".equalsIgnoreCase(historyType.trim())) {
                if ("".equals(searchInvoiceText.getText().trim())) {
                    listOfPurchaseOrder = purchaseOrderDAO.findAllByDate(commonService.jDateChooserToSqlDate(fromDateChooser), commonService.jDateChooserToSqlDate(toDateChooser));
                } else {
                    listOfPurchaseOrder = purchaseOrderDAO.findAllByInvoiceNo(searchInvoiceText.getText().trim());
                }
                if (listOfPurchaseOrder.size() == 0) {
                    MsgBox.warning("Records not found");
                } else {
                    fillHistoryTable();
                }
            }
            if ("CommercialInvoice".equalsIgnoreCase(historyType.trim())) {
                if ("".equals(searchInvoiceText.getText().trim())) {
                    listOfSalesPreforma = salesPreformaDAO.findAllByDate(commonService.jDateChooserToSqlDate(fromDateChooser), commonService.jDateChooserToSqlDate(toDateChooser));
                } else {
                    listOfSalesPreforma = salesPreformaDAO.findAllByInvoiceNo(searchInvoiceText.getText().trim());
                }
                if (listOfSalesPreforma.size() == 0) {
                    MsgBox.warning("Records not found");
                } else {
                    fillHistoryTable();
                }
            }
            if ("SalesReturn".equalsIgnoreCase(historyType.trim())) {
                if ("".equals(searchInvoiceText.getText().trim())) {
                    listOfSalesReturn = salesReturnDAO.findAllByDate(commonService.jDateChooserToSqlDate(fromDateChooser), commonService.jDateChooserToSqlDate(toDateChooser));
                } else {
                    listOfSalesReturn = salesReturnDAO.findAllByInvoiceNo(searchInvoiceText.getText().trim());
                }
                if (listOfSalesReturn.size() == 0) {
                    MsgBox.warning("Records not found");
                } else {
                    fillHistoryTable();
                }
            }
            if ("Purchase".equalsIgnoreCase(historyType.trim())) {
                if ("".equals(searchInvoiceText.getText().trim())) {
                    listOfPurchase = purchaseDAO.findAllByDate(commonService.jDateChooserToSqlDate(fromDateChooser), commonService.jDateChooserToSqlDate(toDateChooser));
                } else {
                    listOfPurchase = purchaseDAO.findAllByInvoiceNo(searchInvoiceText.getText().trim());
                }
                if (listOfPurchase.size() == 0) {
                    MsgBox.warning("Records not found");
                } else {
                    fillHistoryTable();
                }
            }
            if ("Sales".equalsIgnoreCase(historyType.trim())) {
                if ("".equals(searchInvoiceText.getText().trim())) {
                    listOfSales = salesDAO.findAllByDate(commonService.jDateChooserToSqlDate(fromDateChooser), commonService.jDateChooserToSqlDate(toDateChooser));
                } else {
                    listOfSales = salesDAO.findAllByInvoiceNo(searchInvoiceText.getText().trim());
                }
                if (listOfSales.size() == 0) {
                    MsgBox.warning("Records not found");
                } else {
                    fillHistoryTable();
                }
            }
            if ("SalesPreforma".equalsIgnoreCase(historyType.trim())) {
                if ("".equals(searchInvoiceText.getText().trim())) {
                    listOfSalesPreforma = salesPreformaDAO.findAllByDate(commonService.jDateChooserToSqlDate(fromDateChooser), commonService.jDateChooserToSqlDate(toDateChooser));
                } else {
                    listOfSalesPreforma = salesPreformaDAO.findAllByInvoiceNo(searchInvoiceText.getText().trim());
                }
                if (listOfSalesPreforma.size() == 0) {
                    MsgBox.warning("Records not found");
                } else {
                    fillHistoryTable();
                }

            }
            if ("PurchaseReturn".equalsIgnoreCase(historyType.trim())) {
                if ("".equals(searchInvoiceText.getText().trim())) {
                    listOfPurchaseReturn = purchaseReturnDAO.findAllByDate(commonService.jDateChooserToSqlDate(fromDateChooser), commonService.jDateChooserToSqlDate(toDateChooser));
                } else {
                    listOfPurchaseReturn = purchaseReturnDAO.findAllByInvoiceNo(searchInvoiceText.getText().trim());
                }
                if (listOfPurchaseReturn.size() == 0) {
                    MsgBox.warning("Records not found");
                } else {
                    fillHistoryTable();
                }
            }
            if ("Payment".equalsIgnoreCase(historyType.trim())) {
                if ("".equals(searchInvoiceText.getText().trim())) {
                    listOfPayment = paymentDAO.findAllByDate(commonService.jDateChooserToSqlDate(fromDateChooser), commonService.jDateChooserToSqlDate(toDateChooser));
                } else {
                    listOfPayment = paymentDAO.findAllByInvoiceNo(searchInvoiceText.getText().trim());
                }
                if (listOfPayment.size() == 0) {
                    MsgBox.warning("Records not found");
                } else {
                    fillHistoryTable();
                }
            }
            if ("Receipt".equalsIgnoreCase(historyType.trim())) {
                if ("".equals(searchInvoiceText.getText().trim())) {
                    listOfReceipt = receiptDAO.findAllByDate(commonService.jDateChooserToSqlDate(fromDateChooser), commonService.jDateChooserToSqlDate(toDateChooser));
                } else {
                    listOfReceipt = receiptDAO.findAllByInvoiceNo(searchInvoiceText.getText().trim());
                }
                if (listOfReceipt.size() == 0) {
                    MsgBox.warning("Records not found");
                } else {
                    fillHistoryTable();
                }

            }
            if ("Journal".equalsIgnoreCase(historyType.trim())) {
                if ("".equals(searchInvoiceText.getText().trim())) {
                    listOfJournal = journalDAO.findAllByDate(commonService.jDateChooserToSqlDate(fromDateChooser), commonService.jDateChooserToSqlDate(toDateChooser));
                } else {
                    listOfJournal = journalDAO.findAllByInvoiceNo(searchInvoiceText.getText().trim());
                }
                if (listOfJournal.size() == 0) {
                    MsgBox.warning("Records not found");
                } else {
                    fillHistoryTable();
                }

            }
            if ("Contra".equalsIgnoreCase(historyType.trim())) {
                if ("".equals(searchInvoiceText.getText().trim())) {
                    listOfContra = contraDAO.findAllByDate(commonService.jDateChooserToSqlDate(fromDateChooser), commonService.jDateChooserToSqlDate(toDateChooser));
                } else {
                    listOfContra = contraDAO.findAllByInvoiceNo(searchInvoiceText.getText().trim());
                }
                if (listOfContra.size() == 0) {
                    MsgBox.warning("Records not found");
                } else {
                    fillHistoryTable();
                }
            }

        } catch (Exception ex) {
            log.error("searchButtonActionPerformed:", ex);
        }

    }//GEN-LAST:event_searchButtonActionPerformed

    private void printButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printButtonActionPerformed
        // TODO add your handling code here:
//        try {
//            MessageFormat header = new MessageFormat("Purchase History-" + GlobalProperty.company.getStartDate() + "-" + GlobalProperty.company.getEndDate());
//
//            MessageFormat footer = new MessageFormat("Printed: ");
//            PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
//            aset.add(OrientationRequested.LANDSCAPE);
//            historyTable.setRowHeight(30);
//            int xx = 0;
//            int yy = 0; //top and bottom margin. Note that bottom margin cannot be less than 15 mm  
//            int ww = 845; //Width  
//            int hh = 599; //Height  
//            int units = MediaPrintableArea.MM;
//            aset.add(new MediaPrintableArea(xx, yy, ww, hh, units));
//
//            historyTable.print(JTable.PrintMode.FIT_WIDTH, header, footer, true, aset, true);
//        } catch (java.awt.print.PrinterException e) {
//            e.printStackTrace();
//        
        try {
        JTableReport jTableReport=new JTableReport();
        if ("PurchaseOrder".equalsIgnoreCase(historyType.trim()) || "CommercialInvoice".equalsIgnoreCase(historyType.trim()) || "Purchase".equalsIgnoreCase(historyType.trim()) || "Sales".equalsIgnoreCase(historyType.trim())|| "SalesPreforma".equalsIgnoreCase(historyType.trim())) {            
            jTableReport.createJTableReport(historyTable, historyType, 7, "print");   
        }
        if ("Journal".equalsIgnoreCase(historyType.trim())|| "Contra".equalsIgnoreCase(historyType.trim()) || "Payment".equalsIgnoreCase(historyType.trim()) || "Receipt".equalsIgnoreCase(historyType.trim())) {
            jTableReport.createJTableReport(historyTable, historyType, 7, "print"); 
        }
        if ("PurchaseReturn".equalsIgnoreCase(historyType.trim()) || "SalesReturn".equalsIgnoreCase(historyType.trim())) {
            jTableReport.createJTableReport(historyTable, historyType, 6, "print"); 
        }
       
           //jTableReport.createJTableReport(historyTable, month, screen)
                
            
        } catch (Exception ex) {
            log.error(ex);
        }

    }//GEN-LAST:event_printButtonActionPerformed

    private void pdfButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdfButtonActionPerformed
        //JTableReport jTableReport=new JTableReport();
        try {
        JTableReport jTableReport=new JTableReport();
        if ("PurchaseOrder".equalsIgnoreCase(historyType.trim()) || "CommercialInvoice".equalsIgnoreCase(historyType.trim()) || "Purchase".equalsIgnoreCase(historyType.trim()) || "Sales".equalsIgnoreCase(historyType.trim())|| "SalesPreforma".equalsIgnoreCase(historyType.trim())) {            
            jTableReport.createJTableReport(historyTable, historyType, 7, "pdf");   
        }
        if ("Journal".equalsIgnoreCase(historyType.trim())|| "Contra".equalsIgnoreCase(historyType.trim()) || "Payment".equalsIgnoreCase(historyType.trim()) || "Receipt".equalsIgnoreCase(historyType.trim())) {
            jTableReport.createJTableReport(historyTable, historyType, 7, "pdf"); 
        }
        if ("PurchaseReturn".equalsIgnoreCase(historyType.trim()) || "SalesReturn".equalsIgnoreCase(historyType.trim())) {
            jTableReport.createJTableReport(historyTable, historyType, 6, "pdf"); 
        }
       
           //jTableReport.createJTableReport(historyTable, month, screen)
                
            
        } catch (Exception ex) {
            log.error(ex);
        }

    }//GEN-LAST:event_pdfButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bodyPanel;
    private com.toedter.calendar.JDateChooser fromDateChooser;
    private javax.swing.JPanel headingPanel;
    private javax.swing.JTable historyTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel leftHeadingLabel;
    private javax.swing.JButton pdfButton;
    private javax.swing.JButton printButton;
    private javax.swing.JButton searchButton;
    private javax.swing.JTextField searchInvoiceText;
    private com.toedter.calendar.JDateChooser toDateChooser;
    // End of variables declaration//GEN-END:variables
}
