/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.MultiCurrencyDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.dao.ShipmentModeDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.report.SalesPreformaReport;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.ItemService;
import com.cloudsendsoft.inventory.services.LedgerService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.MultiCurrency;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseCharge;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseTax;
import com.cloudsendsoft.inventory.model.SalesPreformaTax;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaCharge;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.report.CommercialInvoiceReport;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class SalesPreformaView extends javax.swing.JPanel {

    /**
     * Creates new form PurchaseViewPanel
     */
    static double totalAmount = 0;
    double subTotal = 0.00;
    double netTotal = 0.00;
    double totDiscount = 0.00;
    double grandTotal = 0.00;

    String customerDetails = "";

    static final Logger log = Logger.getLogger(SalesPreformaView.class.getName());

    CRUDServices cRUDServices = new CRUDServices();
    ItemDAO itemDAO = new ItemDAO();
    CommonService commonService = new CommonService();
    MainForm mainForm = null;
    LedgerService ledgerService = new LedgerService();
    TaxDAO taxDAO = new TaxDAO();
    ChargesDAO chargesDAO = new ChargesDAO();
    MultiCurrencyDAO multiCurrencyDAO = new MultiCurrencyDAO();
    ShipmentModeDAO shipmentModeDAO = new ShipmentModeDAO();

    //global usage
    Ledger customerLedger = null;
    double itemQty = 0;
    List<SalesPreformaItem> salesPreformaItemList = new ArrayList<SalesPreformaItem>();
    List<SalesPreformaItem> salesPreformaItemListHistory = null;
    Item item = null;
    List<SalesPreformaTax> salesPreformaTaxList = new ArrayList<SalesPreformaTax>();
    List<SalesPreformaCharge> salesPreformaChargesList = new ArrayList<SalesPreformaCharge>();
    NumberProperty numberProperty = null;
    NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();

    MultiCurrency currency = null;

    SalesPreformaItem salesPreformaItem = null;
    DefaultTableModel salesPreformaItemTableModel = null;
    int check = 0;
    SalesPreforma salesPreforma = null;

    public SalesPreformaView(MainForm mainForm) {

        try {
            initComponents();

            commonService.setCurrentPeriodOnCalendar(billDateFormattedText);

            if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
                saveButton.setEnabled(false);
                newButton.setEnabled(false);
            }
            //invoice number load
            numberProperty = numberPropertyDAO.findInvoiceNo("SalesPreforma");
            invoiceNumberText.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
            //table background color removed
            jScrollPane1.setOpaque(false);
            jScrollPane1.getViewport().setOpaque(false);

            billDateFormattedText.setDate(new java.util.Date());
            expiryDateChooser.setDate(commonService.addDaysToDate(billDateFormattedText.getDate(), 10));

            this.mainForm = mainForm;
            salePreformaItemTable.setRowHeight(25);

            salePreformaItemTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                    null,
                    new String[]{"Sr. No.", "Item Code", "Description", "Unit Price", "Qty.", "Discount", "Total Amount"}
            ));
            salesPreformaItemTableModel = (DefaultTableModel) salePreformaItemTable.getModel();
            CommonService.setWidthAsPercentages(salePreformaItemTable, .03, .10, .20, .08, .08, .08, .08);

            // Combo boxes initialization
            for (Tax tacs : taxDAO.findAll()) {
                taxComboBox.addItem(new ComboKeyValue((tacs.getName()), tacs));
            }
            for (Charges charges : chargesDAO.findAll()) {
                chargesComboBox.addItem(new ComboKeyValue(charges.getName(), charges));
            }

            for (MultiCurrency currency : multiCurrencyDAO.findAll()) {
                if (currency == null) {
                    this.currency = currency;
                }
                multiCurrencyComboBox.addItem(new ComboKeyValue(currency.getName(), currency));
            }

            for (ShipmentMode shipmentMode : shipmentModeDAO.findAll()) {
                shipmentComboBox.addItem(new ComboKeyValue(shipmentMode.getName(), shipmentMode));
            }

            //jTable rows & columns alignment
            ((DefaultTableCellRenderer) salePreformaItemTable.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);

            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(JLabel.CENTER);
            salePreformaItemTable.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
            salePreformaItemTable.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
            salePreformaItemTable.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
            salePreformaItemTable.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
            //jTable rows & columns alignment
            setFocusOrder();

        } catch (Exception e) {
            log.error("SalesPreformaView:", e);
        }

    }

    void setFocusOrder() {
        try {
           
            customerText.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    deliveryText.requestFocusInWindow();
                }
           });
           deliveryText.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    paymentTermsText.requestFocusInWindow();
                }
           }); 
           paymentTermsText.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    deleveryTermsText.requestFocusInWindow();
                }
           });
           deleveryTermsText.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    packingText.requestFocusInWindow();
                }
           }); 
           packingText.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    multiCurrencyComboBox.requestFocusInWindow();
                }
           });
           multiCurrencyComboBox.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    itemCodeText.requestFocusInWindow();
                }
           }); 
           
           
           itemCodeText.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    itemQtyText.requestFocusInWindow();
                }
           });
           itemQtyText.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    itemDiscountText.requestFocusInWindow();
                }
           });
           itemDiscountText.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    addItemButton.requestFocusInWindow();
                }
           }); 
           addItemButton.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    taxComboBox.requestFocusInWindow();
                }
           }); 
           taxComboBox.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    taxAddButton.requestFocusInWindow();
                }
           });
           taxAddButton.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    chargesComboBox.requestFocusInWindow();
                }
           }); 
           chargesComboBox.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    chargesText.requestFocusInWindow();
                }
           });
           chargesText.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    chargesAddButton.requestFocusInWindow();
                }
           }); 
           chargesAddButton.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    shipmentComboBox.requestFocusInWindow();
                }
           });
           shipmentComboBox.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    discountText.requestFocusInWindow();
                }
           });
           discountText.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    totDiscountAddButton.requestFocusInWindow();
                }
           });
           totDiscountAddButton.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed(ActionEvent e) {
                    saveButton.requestFocusInWindow();
                }
           }); 
            
             
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bottomPanel = new javax.swing.JPanel();
        itemInfoPanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        itemDescriptionText = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        itemGodownText = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        itemCodeText = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        itemDiscountText = new javax.swing.JTextField();
        itemQtyText = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        itemPriceText = new javax.swing.JTextField();
        itemWeightText = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        availableQtyLabel = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        totAvalQtyLabel = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        addItemButton = new javax.swing.JButton();
        itemRemoveButton = new javax.swing.JButton();
        chargesPanel = new javax.swing.JPanel();
        chargesComboBox = new javax.swing.JComboBox();
        chargesText = new javax.swing.JFormattedTextField();
        chargesAddButton = new javax.swing.JButton();
        chargesMinusButton = new javax.swing.JButton();
        grandTotalPanel = new javax.swing.JPanel();
        grandTotalLabel = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        discountText = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        totDiscountAddButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        grossAmountLabel = new javax.swing.JLabel();
        grossAmntLabel = new javax.swing.JLabel();
        taxPanel = new javax.swing.JPanel();
        taxComboBox = new javax.swing.JComboBox();
        taxAddButton = new javax.swing.JButton();
        shipmentModePanel = new javax.swing.JPanel();
        shipmentComboBox = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        InvoiceDetailsPanel = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        invoiceNumberText = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        addressTextArea = new javax.swing.JTextArea();
        deliveryText = new javax.swing.JTextField();
        paymentTermsText = new javax.swing.JTextField();
        billDateFormattedText = new com.toedter.calendar.JDateChooser();
        expiryDateChooser = new com.toedter.calendar.JDateChooser();
        jLabel15 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        customerText = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        multiCurrencyComboBox = new javax.swing.JComboBox();
        packingText = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        deleveryTermsText = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        buttonPanel = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        printButton = new javax.swing.JButton();
        pdfButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        tableBorderPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        salePreformaItemTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        bottomPanel.setOpaque(false);

        itemInfoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Item Info*"));
        itemInfoPanel.setOpaque(false);
        itemInfoPanel.setPreferredSize(new java.awt.Dimension(382, 179));

        jPanel1.setOpaque(false);

        itemDescriptionText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemDescriptionTextKeyPressed(evt);
            }
        });

        jLabel19.setText("Godown");

        itemGodownText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemGodownText.setText("0");
        itemGodownText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemGodownTextFocusGained(evt);
            }
        });
        itemGodownText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemGodownTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemGodownTextKeyTyped(evt);
            }
        });

        jLabel7.setText("Item Code");

        jLabel20.setText("Discount");

        itemCodeText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemCodeTextActionPerformed(evt);
            }
        });
        itemCodeText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemCodeTextKeyPressed(evt);
            }
        });

        jLabel9.setText("Quantity");

        jLabel17.setText("Weight");

        itemDiscountText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemDiscountText.setText("0");
        itemDiscountText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemDiscountTextFocusGained(evt);
            }
        });
        itemDiscountText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemDiscountTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemDiscountTextKeyTyped(evt);
            }
        });

        itemQtyText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemQtyText.setText("0");
        itemQtyText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemQtyTextFocusGained(evt);
            }
        });
        itemQtyText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemQtyTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemQtyTextKeyTyped(evt);
            }
        });

        jLabel8.setText("Description");

        jLabel10.setText("Unit Price");

        itemPriceText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemPriceText.setText("0");
        itemPriceText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemPriceTextFocusGained(evt);
            }
        });
        itemPriceText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemPriceTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemPriceTextKeyTyped(evt);
            }
        });

        itemWeightText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        itemWeightText.setText("0");
        itemWeightText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                itemWeightTextFocusGained(evt);
            }
        });
        itemWeightText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itemWeightTextKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itemWeightTextKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                        .addGap(3, 3, 3))
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(itemDescriptionText)
                    .addComponent(itemQtyText, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                    .addComponent(itemPriceText)
                    .addComponent(itemWeightText)
                    .addComponent(itemGodownText)
                    .addComponent(itemDiscountText)
                    .addComponent(itemCodeText, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(2, 2, 2))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(16, 16, 16)
                        .addComponent(jLabel8))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(itemCodeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(itemDescriptionText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(itemQtyText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(itemPriceText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(itemWeightText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(itemGodownText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(itemDiscountText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20))
                .addContainerGap())
        );

        jPanel2.setOpaque(false);

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel6.setOpaque(false);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Available Quantity:");
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        availableQtyLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        availableQtyLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Quandity");

        totAvalQtyLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel23.setText("Total Avail");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel23)
                    .addComponent(jLabel4)
                    .addComponent(totAvalQtyLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(0, 3, Short.MAX_VALUE))
                            .addComponent(availableQtyLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(0, 0, 0))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(availableQtyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(totAvalQtyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel3.setOpaque(false);
        jPanel3.setLayout(new java.awt.GridLayout(1, 0));

        addItemButton.setText("+");
        addItemButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addItemButtonActionPerformed(evt);
            }
        });
        jPanel3.add(addItemButton);

        itemRemoveButton.setText("-");
        itemRemoveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemRemoveButtonActionPerformed(evt);
            }
        });
        jPanel3.add(itemRemoveButton);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout itemInfoPanelLayout = new javax.swing.GroupLayout(itemInfoPanel);
        itemInfoPanel.setLayout(itemInfoPanelLayout);
        itemInfoPanelLayout.setHorizontalGroup(
            itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(itemInfoPanelLayout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        itemInfoPanelLayout.setVerticalGroup(
            itemInfoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        chargesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Charges"));
        chargesPanel.setOpaque(false);

        chargesComboBox.setEditable(true);
        chargesComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                chargesComboBoxMouseClicked(evt);
            }
        });

        chargesText.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        chargesText.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        chargesText.setText("0.00");
        chargesText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                chargesTextFocusGained(evt);
            }
        });
        chargesText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                chargesTextKeyTyped(evt);
            }
        });

        chargesAddButton.setText("+");
        chargesAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesAddButtonActionPerformed(evt);
            }
        });

        chargesMinusButton.setText("-");
        chargesMinusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chargesMinusButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout chargesPanelLayout = new javax.swing.GroupLayout(chargesPanel);
        chargesPanel.setLayout(chargesPanelLayout);
        chargesPanelLayout.setHorizontalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, chargesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chargesComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chargesText))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        chargesPanelLayout.setVerticalGroup(
            chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chargesPanelLayout.createSequentialGroup()
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chargesAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chargesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chargesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chargesMinusButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chargesText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        grandTotalPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Grand Total", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N
        grandTotalPanel.setOpaque(false);

        grandTotalLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        grandTotalLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        grandTotalLabel.setText("0.00");
        grandTotalLabel.setFocusable(false);
        grandTotalLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setText("Net Amount:");

        discountText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                discountTextActionPerformed(evt);
            }
        });
        discountText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                discountTextFocusGained(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel22.setText("Discount:");

        totDiscountAddButton.setText("+");
        totDiscountAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totDiscountAddButtonActionPerformed(evt);
            }
        });

        grossAmountLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        grossAmountLabel.setText("Grand Total:");
        grossAmountLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        grossAmntLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        grossAmntLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        grossAmntLabel.setText("0.00");

        javax.swing.GroupLayout grandTotalPanelLayout = new javax.swing.GroupLayout(grandTotalPanel);
        grandTotalPanel.setLayout(grandTotalPanelLayout);
        grandTotalPanelLayout.setHorizontalGroup(
            grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(grandTotalPanelLayout.createSequentialGroup()
                .addComponent(jLabel22)
                .addGap(32, 32, 32)
                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totDiscountAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(discountText)))
            .addGroup(grandTotalPanelLayout.createSequentialGroup()
                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(grandTotalPanelLayout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(grandTotalLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(grossAmountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(grandTotalPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(grossAmntLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, grandTotalPanelLayout.createSequentialGroup()
                        .addComponent(jSeparator1)
                        .addContainerGap())))
        );
        grandTotalPanelLayout.setVerticalGroup(
            grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(grandTotalPanelLayout.createSequentialGroup()
                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(grandTotalLabel)
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(grandTotalPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(discountText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(totDiscountAddButton)
                .addGap(10, 10, 10)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(grossAmountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(grossAmntLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        taxPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Tax"));
        taxPanel.setOpaque(false);

        taxComboBox.setEditable(true);
        taxComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                taxComboBoxMouseClicked(evt);
            }
        });
        taxComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                taxComboBoxItemStateChanged(evt);
            }
        });

        taxAddButton.setText("+");
        taxAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                taxAddButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout taxPanelLayout = new javax.swing.GroupLayout(taxPanel);
        taxPanel.setLayout(taxPanelLayout);
        taxPanelLayout.setHorizontalGroup(
            taxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(taxPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(taxComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(taxAddButton)
                .addContainerGap())
        );
        taxPanelLayout.setVerticalGroup(
            taxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(taxPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(taxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(taxAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(taxComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        shipmentModePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        shipmentModePanel.setOpaque(false);

        shipmentComboBox.setEditable(true);
        shipmentComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                shipmentComboBoxMouseClicked(evt);
            }
        });

        jLabel3.setText("Shipment Mode");

        javax.swing.GroupLayout shipmentModePanelLayout = new javax.swing.GroupLayout(shipmentModePanel);
        shipmentModePanel.setLayout(shipmentModePanelLayout);
        shipmentModePanelLayout.setHorizontalGroup(
            shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, shipmentModePanelLayout.createSequentialGroup()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(shipmentComboBox, 0, 87, Short.MAX_VALUE))
        );
        shipmentModePanelLayout.setVerticalGroup(
            shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(shipmentModePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(shipmentModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(shipmentComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        jPanel5.setOpaque(false);

        InvoiceDetailsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Invoice Details"));
        InvoiceDetailsPanel.setOpaque(false);
        InvoiceDetailsPanel.setLayout(new java.awt.GridLayout(1, 0));

        jPanel4.setOpaque(false);

        jLabel18.setText("Packing");

        invoiceNumberText.setEditable(false);
        invoiceNumberText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                invoiceNumberTextActionPerformed(evt);
            }
        });
        invoiceNumberText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                invoiceNumberTextKeyPressed(evt);
            }
        });

        jLabel11.setText("Delivery Terms");

        jLabel6.setText("Bill Date*");

        addressTextArea.setColumns(20);
        addressTextArea.setLineWrap(true);
        addressTextArea.setRows(5);
        jScrollPane3.setViewportView(addressTextArea);

        deliveryText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deliveryTextActionPerformed(evt);
            }
        });
        deliveryText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                deliveryTextKeyPressed(evt);
            }
        });

        paymentTermsText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                paymentTermsTextKeyPressed(evt);
            }
        });

        billDateFormattedText.setDateFormatString("dd/MM/yyyy");
        billDateFormattedText.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                billDateFormattedTextPropertyChange(evt);
            }
        });

        expiryDateChooser.setDateFormatString("dd/MM/yyyy");

        jLabel15.setText("ExpiryDate*");

        jLabel14.setText("Delivery");

        jLabel5.setText("Invoice Number*");

        customerText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customerTextActionPerformed(evt);
            }
        });
        customerText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                customerTextKeyPressed(evt);
            }
        });

        jLabel16.setText("Multi Currency*");

        multiCurrencyComboBox.setEditable(true);
        multiCurrencyComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                multiCurrencyComboBoxMouseClicked(evt);
            }
        });
        multiCurrencyComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                multiCurrencyComboBoxItemStateChanged(evt);
            }
        });
        multiCurrencyComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                multiCurrencyComboBoxActionPerformed(evt);
            }
        });

        packingText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                packingTextKeyPressed(evt);
            }
        });

        jLabel13.setText("Customer*");

        jLabel1.setText("Address :");

        deleveryTermsText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleveryTermsTextActionPerformed(evt);
            }
        });
        deleveryTermsText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                deleveryTermsTextKeyPressed(evt);
            }
        });

        jLabel12.setText("Payment Terms");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(invoiceNumberText, javax.swing.GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
                    .addComponent(billDateFormattedText, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(customerText)
                    .addComponent(deliveryText))
                .addGap(10, 10, 10)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(deleveryTermsText)
                            .addComponent(paymentTermsText)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(packingText)
                            .addComponent(multiCurrencyComboBox, 0, 68, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, 61, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(expiryDateChooser, javax.swing.GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(billDateFormattedText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12)
                            .addComponent(paymentTermsText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(deleveryTermsText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11)
                            .addComponent(invoiceNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(packingText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(customerText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18)))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(deliveryText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(multiCurrencyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(expiryDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        InvoiceDetailsPanel.add(jPanel4);

        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new java.awt.GridLayout(1, 0));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("New");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(newButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        saveButton.setText("<HTML><U>S</U>ave<HTML>");
        saveButton.setToolTipText("");
        saveButton.setOpaque(false);
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(saveButton);

        printButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/print.png"))); // NOI18N
        printButton.setText("Print");
        printButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(printButton);

        pdfButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/pdf-icon.png"))); // NOI18N
        pdfButton.setText(" PDF");
        pdfButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdfButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(pdfButton);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/cancel.png"))); // NOI18N
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(cancelButton);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(InvoiceDetailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(InvoiceDetailsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout bottomPanelLayout = new javax.swing.GroupLayout(bottomPanel);
        bottomPanel.setLayout(bottomPanelLayout);
        bottomPanelLayout.setHorizontalGroup(
            bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bottomPanelLayout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(itemInfoPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chargesPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(taxPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(shipmentModePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(grandTotalPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        bottomPanelLayout.setVerticalGroup(
            bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bottomPanelLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bottomPanelLayout.createSequentialGroup()
                        .addComponent(taxPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chargesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(grandTotalPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, bottomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(shipmentModePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(itemInfoPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(bottomPanel, java.awt.BorderLayout.PAGE_END);

        tableBorderPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("SalesPreforma"));
        tableBorderPanel.setOpaque(false);

        jScrollPane1.setName(""); // NOI18N
        jScrollPane1.setOpaque(false);

        salePreformaItemTable.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        salePreformaItemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        salePreformaItemTable.setOpaque(false);
        salePreformaItemTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                salePreformaItemTableMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                salePreformaItemTableMouseEntered(evt);
            }
        });
        jScrollPane1.setViewportView(salePreformaItemTable);

        javax.swing.GroupLayout tableBorderPanelLayout = new javax.swing.GroupLayout(tableBorderPanel);
        tableBorderPanel.setLayout(tableBorderPanelLayout);
        tableBorderPanelLayout.setHorizontalGroup(
            tableBorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tableBorderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1205, Short.MAX_VALUE)
                .addContainerGap())
        );
        tableBorderPanelLayout.setVerticalGroup(
            tableBorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tableBorderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 296, Short.MAX_VALUE))
        );

        add(tableBorderPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    void fillSalesPreformaItemTable() {
        try {
           // boolean flag = false;
            salesPreformaItemTableModel.setRowCount(0);
            //BigDecimal grandTotal = new BigDecimal(0.00) ;
            //grandTotal.setScale(1);
            //subTotal = 0.00;
            netTotal = 0.00;

            int slNo = 1;
            String currencySymbol = (null == currency) ? "" : currency.getSymbol();

            //{"Sr. No.", "Item Code","Description", "Unit Price", "Qty.", "Total Amount"}
            for (SalesPreformaItem pItem : salesPreformaItemList) {
                double itemTotal = 0;
                if (salesPreforma == null) {
                    itemTotal = ((pItem.getUnitPrice()/currency.getRate()) - (pItem.getDiscount())) * pItem.getQuantity();
                    salesPreformaItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), currency.getSymbol() + " " + (pItem.getUnitPrice()/currency.getRate()), pItem.getQuantity(),currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(pItem.getDiscount()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal)});

                } else {
                    itemTotal = ((pItem.getUnitPrice()/ currency.getRate()) - (pItem.getDiscount()/ currency.getRate()))* pItem.getQuantity();
                    salesPreformaItemTableModel.addRow(new Object[]{slNo++, " " + (pItem.getItem().getItemCode()), " " + (pItem.getItem().getDescription()), currency.getSymbol() + " " + (pItem.getUnitPrice() / currency.getRate()), pItem.getQuantity(), commonService.formatIntoCurrencyAsString(pItem.getDiscount()/currency.getRate()), currency.getSymbol() + " " + commonService.formatIntoCurrencyAsString(itemTotal)});
                }
//grandTotal.add(BigDecimal.pa itemTotal);
                netTotal += itemTotal;
                //netTotal-=pItem.getDiscount();
            }

            if (netTotal > 0) {
                for (SalesPreformaTax salesPreformaTax : salesPreformaTaxList) {
//                    if (!flag) {
//                        salesPreformaItemTableModel.addRow(new Object[]{null});
//                        flag = true;
//                    }
                    //double taxTotal = subTotal * (purchaseTax.getTax().getTaxRate() / 100);
                    //if (salesPreforma == null) {
                        salesPreformaItemTableModel.addRow(new Object[]{"", "", "\t" + salesPreformaTax.getTax().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(salesPreformaTax.getAmount()/currency.getRate())});
                        netTotal += salesPreformaTax.getAmount();
//                    } else {
//                        salesPreformaItemTableModel.addRow(new Object[]{"", "", "\t" + salesPreformaTax.getTax().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(salesPreformaTax.getAmount() / salesPreforma.getCurrentCurrencyRate())});
//                        netTotal += salesPreformaTax.getAmount() / salesPreforma.getCurrentCurrencyRate();
//                    }
                }
               // flag = false;
                for (SalesPreformaCharge salesPreformaCharge : salesPreformaChargesList) {
                    //double chargesTotal = Double.parseDouble(chargesText.getText());
//                    if (!flag) {
//                        salesPreformaItemTableModel.addRow(new Object[]{null});
//                        flag = true;
//                    }
                   // if (salesPreforma == null) {
                        salesPreformaItemTableModel.addRow(new Object[]{"", "", "\t" + salesPreformaCharge.getCharges().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(salesPreformaCharge.getAmount())});
                        netTotal += salesPreformaCharge.getAmount();
//                    } else {
//                        salesPreformaItemTableModel.addRow(new Object[]{"", "", "\t" + salesPreformaCharge.getCharges().getName(), "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(salesPreformaCharge.getAmount() / salesPreforma.getCurrentCurrencyRate())});
//                        netTotal += salesPreformaCharge.getAmount() / salesPreforma.getCurrentCurrencyRate();
//                    }
                }
            }

            grandTotal = netTotal - totDiscount;
            if (totDiscount > 0) {

                salesPreformaItemTableModel.addRow(new Object[]{"", "", "Discount", "", "", "", currencySymbol + " " + commonService.formatIntoCurrencyAsString(totDiscount)});
            }
            grandTotalLabel.setText(currencySymbol + " " + commonService.formatIntoCurrencyAsString(netTotal));
            grossAmntLabel.setText(currencySymbol + "" + commonService.formatIntoCurrencyAsString(grandTotal));

            //clear fields
            CommonService.clearNumberFields(new JTextField[]{itemQtyText, itemDiscountText, itemWeightText, itemPriceText});
            CommonService.clearTextFields(new JTextField[]{itemCodeText, itemDescriptionText, itemGodownText});
            availableQtyLabel.setText("");
            totAvalQtyLabel.setText("");

        } catch (Exception e) {
            log.error("fillPurchaseItemTable:", e);
        }

    }

    public void listSalesPreformaItemHistory(SalesPreforma salesPreforma) {
        this.salesPreforma = salesPreforma;
        this.customerLedger = salesPreforma.getCustomer();
        //System.out.println("lllllllllllll"+sales.getPurchaseItems().size());
        //salesPreformaItemList = salesPreforma.getSalesPreformaItems();

        salesPreformaItemList = salesPreforma.getSalesPreformaItems();
        salesPreformaItemListHistory = new ArrayList<SalesPreformaItem>();
        
        for(SalesPreformaItem salesPreformaItem:salesPreformaItemList){
            //copy sales items 
            SalesPreformaItem salesPreformaItem1=new SalesPreformaItem();
            salesPreformaItem1.setItem(salesPreformaItem.getItem());
            salesPreformaItem1.setQuantity(salesPreformaItem.getQuantity());
            //salesPreformaItem1.setQuantity1(salesPreformaItem.getQuantity1());
            salesPreformaItemListHistory.add(salesPreformaItem1);
            
            
            salesPreformaItem.setUnitPrice(salesPreformaItem.getUnitPrice());
        }
        salesPreformaTaxList = salesPreforma.getSalesPreformaTaxs();
        for(SalesPreformaTax salesPreformaTax:salesPreformaTaxList){
            salesPreformaTax.setAmount(salesPreformaTax.getAmount()/salesPreforma.getCurrentCurrencyRate());
        }
        salesPreformaChargesList = salesPreforma.getSalesPreformaCharges();
        for(SalesPreformaCharge salesPreformaCharge:salesPreformaChargesList){
            salesPreformaCharge.setAmount(salesPreformaCharge.getAmount()/salesPreforma.getCurrentCurrencyRate());
        }
        //updated       billDateFormattedText.setText(commonService.sqlDateToString(salesPreforma.getBillDate()));
        billDateFormattedText.setDate(salesPreforma.getBillDate());
        invoiceNumberText.setText(salesPreforma.getInvoiceNumber());
        customerText.setText(salesPreforma.getCustomer().getName());
        //System.out.println("salesPreforma.getCustomer():"+sales.getCustomer().getName());
        deliveryText.setText(salesPreforma.getDelivery() + "");
        paymentTermsText.setText(salesPreforma.getPaymentTerms());
        deleveryTermsText.setText(salesPreforma.getDeleveryTerms());
        packingText.setText(salesPreforma.getPacking());
        totDiscount = salesPreforma.getDiscount()/salesPreforma.getCurrentCurrencyRate();
        //paymentTermsText.setText(purchase.());
        currency=salesPreforma.getMultiCurrency();
        currency.setRate(salesPreforma.getCurrentCurrencyRate());
        addressTextArea.setText(salesPreforma.getAddress());
        saveButton.setText("Update");
        check = 1;
        
        int count = multiCurrencyComboBox.getItemCount();
        for (int i = 0; i < count; i++) {
            ComboKeyValue ckv = (ComboKeyValue) multiCurrencyComboBox.getItemAt(i);
            if (ckv.getKey().trim().equalsIgnoreCase(salesPreforma.getMultiCurrency().getName())) {
                multiCurrencyComboBox.setSelectedItem(ckv);
            }
        }
        
        int count1 = shipmentComboBox.getItemCount();
        if(salesPreforma.getShipmentMode()!=null)
        {
        for (int i = 0; i < count1; i++) {
                ComboKeyValue ckv = (ComboKeyValue) shipmentComboBox.getItemAt(i);
                if (ckv.getKey().trim().equalsIgnoreCase(salesPreforma.getShipmentMode().getName())) {
                    shipmentComboBox.setSelectedItem(ckv);
                }
            }
        }
        fillSalesPreformaItemTable();
    }

    private void salePreformaItemTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salePreformaItemTableMouseClicked
        try {
            ArrayList row = CommonService.getTableRowData(salePreformaItemTable);
            String itemCode = row.get(1) + "";

            if (itemCode.trim().length() > 0) {
                
                salesPreformaItem = salesPreformaItemList.get(Integer.parseInt(row.get(0) + "") - 1);
                item=salesPreformaItem.getItem();
                itemCodeText.setText(salesPreformaItem.getItem().getItemCode());
                
                itemDescriptionText.setText(salesPreformaItem.getItem().getDescription());
                itemQtyText.setText(salesPreformaItem.getQuantity() + "");
                if (salesPreforma == null) {
                    itemPriceText.setText(salesPreformaItem.getUnitPrice()/currency.getRate() + "");
                } else {
                    itemPriceText.setText(salesPreformaItem.getUnitPrice() / salesPreforma.getCurrentCurrencyRate() + "");
                }
                itemWeightText.setText(salesPreformaItem.getItem().getWeight() + "");
                itemDiscountText.setText(salesPreformaItem.getDiscount() + "");
                itemGodownText.setText(salesPreformaItem.getItem().getGodown().getName());
                availableQtyLabel.setText(salesPreformaItem.getItem().getAvailableQty() + "");

            } else {
                String itemDesc = row.get(2) + "";
                itemDesc = itemDesc.trim();
                int count = taxComboBox.getItemCount();

                if (itemDesc.equalsIgnoreCase("discount")) {
                    totDiscountAddButton.setText("-");
                }
                for (int i = 0; i < count; i++) {
                    ComboKeyValue ckv = (ComboKeyValue) taxComboBox.getItemAt(i);
                    if (ckv.getKey().trim().equalsIgnoreCase(itemDesc)) {
                        taxComboBox.setSelectedItem(ckv);
                        taxAddButton.setText("-");
                    }
                }

                count = chargesComboBox.getItemCount();
                for (int i = 0; i < count; i++) {
                    ComboKeyValue ckv = (ComboKeyValue) chargesComboBox.getItemAt(i);
                    if (ckv.getKey().trim().equalsIgnoreCase(itemDesc)) {
                        chargesComboBox.setSelectedItem(ckv);
                        chargesText.setText(row.get(6) + "");
                    }
                }
            }

        } catch (Exception e) {
            log.error("salePreformaItemTableMouseClicked:", e);
        }
    }//GEN-LAST:event_salePreformaItemTableMouseClicked

    private void salePreformaItemTableMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salePreformaItemTableMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_salePreformaItemTableMouseEntered
    void saveSalesPreforma() {
        try {
            if (salesPreforma == null) {
                salesPreforma = new SalesPreforma();
                check = 0;
            }

            salesPreforma.setBillDate(commonService.utilDateToSqlDate(billDateFormattedText.getDate()));
            salesPreforma.setInvoiceNumber(invoiceNumberText.getText());
            salesPreforma.setCustomer(customerLedger);
            salesPreforma.setDelivery(deliveryText.getText());
            salesPreforma.setAddress(addressTextArea.getText().trim());
            salesPreforma.setPaymentTerms(paymentTermsText.getText());
            salesPreforma.setDeleveryTerms(deleveryTermsText.getText());
            salesPreforma.setPacking(packingText.getText());
            salesPreforma.setExpiryDate(commonService.jDateChooserToSqlDate(expiryDateChooser));
            salesPreforma.setMultiCurrency(currency);
            Object shipmentMod = shipmentComboBox.getSelectedItem();
            ShipmentMode shipmentMode = (ShipmentMode) ((ComboKeyValue) shipmentMod).getValue();
            if (shipmentMode.getName().equalsIgnoreCase("N/A")) {
                salesPreforma.setShipmentMode(null);
            }else{
                salesPreforma.setShipmentMode(shipmentMode);
            }
            for (SalesPreformaItem salesPreformaItem : salesPreformaItemList) {
                salesPreformaItem.setUnitPrice(salesPreformaItem.getUnitPrice());
                salesPreformaItem.setDiscount(salesPreformaItem.getDiscount() * currency.getRate());
            }
            salesPreforma.setSalesPreformaItems(salesPreformaItemList);
            for (SalesPreformaTax salesPreformaTax : salesPreformaTaxList) {
                salesPreformaTax.setAmount(salesPreformaTax.getAmount() * currency.getRate());
            }
            salesPreforma.setSalesPreformaTaxs(salesPreformaTaxList);
            for (SalesPreformaCharge salesPreformaCharge : salesPreformaChargesList) {
                salesPreformaCharge.setAmount(salesPreformaCharge.getAmount() * currency.getRate());
            }
            salesPreforma.setSalesPreformaCharges(salesPreformaChargesList);
            salesPreforma.setCurrentCurrencyRate(currency.getRate());
            salesPreforma.setGrandTotal(grandTotal * currency.getRate());
            salesPreforma.setCompany(GlobalProperty.getCompany());

            salesPreforma.setDiscount(totDiscount * currency.getRate());
                   // if (check == 0) {
            //     cRUDServices.saveModel(salesPreforma);
            // } else {
            cRUDServices.saveOrUpdateModel(salesPreforma);
            //  }
            numberProperty.setNumber(numberProperty.getNumber() + 1);
            cRUDServices.saveOrUpdateModel(numberProperty);
            
            listSalesPreformaItemHistory(salesPreforma);
        } catch (Exception e) {
            log.error("saveSalesPreforma", e);
        }
    }
    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed

        try {

                if (expiryDateChooser.getDate() != null && billDateFormattedText.getDate() != null && customerLedger != null && salesPreformaItemList != null) {
                    if (MsgBox.confirm("Are you sure you want to save?")) {
                        saveSalesPreforma();
                        MsgBox.success("Sales preforma Saved Successfully");
                        //clearAllFields();
                    }
                } else {
                    MsgBox.warning("Required fields are not entered");
                }
            

        } catch (Exception ex) {
            log.error("saveButtonActionPerformed:", ex);
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        CommonService.removeJpanel(mainForm.getContentPanel());
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void taxAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_taxAddButtonActionPerformed
        Object taxItem = taxComboBox.getSelectedItem();
        Tax tax = (Tax) ((ComboKeyValue) taxItem).getValue();

        if (tax.getTaxRate() > 0 && netTotal > 0 && !tax.getName().equalsIgnoreCase("N/A")) {
            if (taxAddButton.getText().trim().equalsIgnoreCase("+")) {
                SalesPreformaTax salesPreformaTax = new SalesPreformaTax();

                Iterator<SalesPreformaTax> iter = salesPreformaTaxList.iterator();
                while (iter.hasNext()) {
                    if (tax.getName().equalsIgnoreCase(iter.next().getTax().getName())) {
                        iter.remove();
                    }
                }
                
                salesPreformaTax.setTax(tax);
                salesPreformaTax.setTaxRate(tax.getTaxRate());
                double taxTotal = (netTotal * tax.getTaxRate()) / 100;
                salesPreformaTax.setAmount(taxTotal);
                salesPreformaTaxList.add(salesPreformaTax);
                taxComboBox.setSelectedIndex(0);
            } else {
                Iterator<SalesPreformaTax> iter = salesPreformaTaxList.iterator();
                while (iter.hasNext()) {
                    if (tax.getName().equalsIgnoreCase(iter.next().getTax().getName())) {
                        iter.remove();
                    }
                }
                taxAddButton.setText("+");
            }

            fillSalesPreformaItemTable();
        }
        taxComboBox.setSelectedIndex(0);
    }//GEN-LAST:event_taxAddButtonActionPerformed

    private void taxComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_taxComboBoxItemStateChanged

    }//GEN-LAST:event_taxComboBoxItemStateChanged

    private void packingTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_packingTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_packingTextKeyPressed

    private void deleveryTermsTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_deleveryTermsTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleveryTermsTextKeyPressed

    private void deleveryTermsTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleveryTermsTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleveryTermsTextActionPerformed

    private void paymentTermsTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_paymentTermsTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_paymentTermsTextKeyPressed

    private void customerTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_customerTextKeyPressed
        try {

            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                //String productId = productIdText.getText().trim();
                List<Ledger> customers = ledgerService.populatePopupTableByCustomer(customerText.getText().trim(), popupTableDialog);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();
                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    customerLedger = customers.get(index);
                    customerText.setText(customerLedger.getLedgerName());

                    customerDetails = "";
                    if (customerLedger.getLedgerName() != null) {
                        if (customerLedger.getLedgerName().trim().length() > 0) {
                            customerDetails += customerLedger.getLedgerName() + "\n";
                        }
                    }
                    if (customerLedger.getAddress() != null) {
                        if (customerLedger.getAddress().trim().length() > 0) {
                            customerDetails += customerLedger.getAddress() + "\n";
                        }
                    }

                    if (customerLedger.getEmail() != null) {
                        if (customerLedger.getEmail().trim().length() > 0) {
                            customerDetails += "Email:" + customerLedger.getEmail() + "\n";
                        }
                    }
                    if (customerLedger.getTelephone() != null) {
                        if (customerLedger.getTelephone().trim().length() > 0) {
                            customerDetails += "Phone:" + customerLedger.getTelephone() + "\n";
                        }
                    }
                    if (customerLedger.getFax() != null) {
                        if (customerLedger.getFax().trim().length() > 0) {
                            customerDetails += "Fax:" + customerLedger.getFax();
                        }
                    }

                    //
                    addressTextArea.setText(customerDetails);
                    //salesTaxText.setText(customerLedger.getSalesTaxNo());
                }
            }

        } catch (Exception e) {
            log.error("productIdTextKeyPressed:", e);
        }
    }//GEN-LAST:event_customerTextKeyPressed

    private void multiCurrencyComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_multiCurrencyComboBoxItemStateChanged
        //        System.err.println("Propert changed");
        Object currencyItem = multiCurrencyComboBox.getSelectedItem();
        
        if(((ComboKeyValue) currencyItem)!=null){
            MultiCurrency tempCurrency=(MultiCurrency) ((ComboKeyValue) currencyItem).getValue();
            if(salesPreforma!=null){
                if(salesPreforma.getMultiCurrency().getId()!=tempCurrency.getId()){
                    currency = (MultiCurrency) ((ComboKeyValue) currencyItem).getValue();
                }
            }else{
                currency = (MultiCurrency) ((ComboKeyValue) currencyItem).getValue();
            }
        fillSalesPreformaItemTable();
        }
    }//GEN-LAST:event_multiCurrencyComboBoxItemStateChanged

    private void chargesMinusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesMinusButtonActionPerformed
        try {
            Object chargesItem = chargesComboBox.getSelectedItem();
            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();
            Iterator<SalesPreformaCharge> iter = salesPreformaChargesList.iterator();
            while (iter.hasNext()) {
                if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                    iter.remove();
                }
            }
            chargesComboBox.setSelectedIndex(0);
            chargesText.setText("0.00");
            fillSalesPreformaItemTable();

        } catch (Exception e) {
            log.error("chargesMinusButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesMinusButtonActionPerformed

    private void chargesAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chargesAddButtonActionPerformed

        try {
            Object chargesItem = chargesComboBox.getSelectedItem();
            Charges charges = (Charges) ((ComboKeyValue) chargesItem).getValue();
            double chargeAmt = Double.parseDouble(chargesText.getText());
            if (Double.parseDouble(chargesText.getText()) > 0 && !charges.getName().equalsIgnoreCase("N/A")) {
                SalesPreformaCharge salesPreformaCharge = new SalesPreformaCharge();
                
                Iterator<SalesPreformaCharge> iter = salesPreformaChargesList.iterator();
                while (iter.hasNext()) {
                    if (charges.getName().equalsIgnoreCase(iter.next().getCharges().getName())) {
                        iter.remove();
                    }
                }
                
                salesPreformaCharge.setCharges(charges);
                salesPreformaCharge.setAmount(chargeAmt);
                salesPreformaChargesList.add(salesPreformaCharge);
                chargesComboBox.setSelectedIndex(0);
                chargesText.setText("0.00");
                fillSalesPreformaItemTable();
            }

        } catch (Exception e) {
            log.error("chargesAddButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_chargesAddButtonActionPerformed

    private void chargesTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chargesTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_chargesTextKeyTyped

    private void chargesTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_chargesTextFocusGained
        chargesText.selectAll();
    }//GEN-LAST:event_chargesTextFocusGained

    private void customerTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customerTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customerTextActionPerformed

    private void deliveryTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deliveryTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deliveryTextActionPerformed

    private void deliveryTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_deliveryTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_deliveryTextKeyPressed

    private void invoiceNumberTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_invoiceNumberTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_invoiceNumberTextActionPerformed

    private void invoiceNumberTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_invoiceNumberTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_invoiceNumberTextKeyPressed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        clearAllFields();
        saveButton.setText("Save");
    }//GEN-LAST:event_newButtonActionPerformed

    private void itemGodownTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemGodownTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemGodownTextKeyTyped

    private void itemGodownTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemGodownTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemGodownTextKeyPressed

    private void itemGodownTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemGodownTextFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_itemGodownTextFocusGained

    private void itemWeightTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemWeightTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemWeightTextKeyTyped

    private void itemWeightTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemWeightTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemWeightTextKeyPressed

    private void itemWeightTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemWeightTextFocusGained
        itemWeightText.selectAll();
    }//GEN-LAST:event_itemWeightTextFocusGained

    private void itemPriceTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemPriceTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemPriceTextKeyTyped

    private void itemPriceTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemPriceTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemPriceTextKeyPressed

    private void itemPriceTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemPriceTextFocusGained
        itemPriceText.selectAll();
    }//GEN-LAST:event_itemPriceTextFocusGained

    private void itemQtyTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyTextKeyTyped
        CommonService.numberValidator(evt);
    }//GEN-LAST:event_itemQtyTextKeyTyped

    private void itemQtyTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemQtyTextKeyPressed

    }//GEN-LAST:event_itemQtyTextKeyPressed

    private void itemQtyTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemQtyTextFocusGained
        itemQtyText.selectAll();
    }//GEN-LAST:event_itemQtyTextFocusGained

    private void itemRemoveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemRemoveButtonActionPerformed
        if (null != salesPreformaItem) {
            salesPreformaItemList.remove(salesPreformaItem);
            fillSalesPreformaItemTable();
            salesPreformaItem = null;
        }
    }//GEN-LAST:event_itemRemoveButtonActionPerformed

    private void addItemButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addItemButtonActionPerformed

        try {
            Boolean conf = true;
            if (item.getSellingPrice() <= (Double.parseDouble(itemPriceText.getText()))*currency.getRate()) {
                if (Double.parseDouble(itemPriceText.getText().trim()) >= Double.parseDouble(itemDiscountText.getText().trim())) {
                    if (Double.parseDouble(itemQtyText.getText()) > (item.getAvailableQty())) {
                        conf = MsgBox.confirm("No sufficient quandity in the selected godown,Do you want to continue?");
                    }
                    if (conf == true) {
                        if (CommonService.stringValidator(new String[]{itemCodeText.getText(), itemDescriptionText.getText()})) {
                            double itemQty = Double.parseDouble(itemQtyText.getText());

                            if ((Double.parseDouble(itemQtyText.getText()) > 0) && (itemQtyText.getText().length() > 0)) {
                                SalesPreformaItem salesPreformaItem = null;
                                for (SalesPreformaItem pItem : salesPreformaItemList) {
                                    if (pItem.getItem().getItemCode().equalsIgnoreCase(item.getItemCode()) && pItem.getItem().getGodown().getName().equalsIgnoreCase(item.getGodown().getName())) {
                                        salesPreformaItem = pItem;
                                        break;
                                    }
                                }

                                if (null == salesPreformaItem) {
                                    salesPreformaItem = new SalesPreformaItem();
                                    salesPreformaItemList.add(salesPreformaItem);
                                }

                                salesPreformaItem.setItem(item);
                                salesPreformaItem.setQuantity(itemQty);
                                salesPreformaItem.setUnitPrice(Double.parseDouble(itemPriceText.getText())*currency.getRate());
                                salesPreformaItem.setDiscount(Double.parseDouble(itemDiscountText.getText()));

                                fillSalesPreformaItemTable();
                            }

                        } else {
                            MsgBox.warning("No Item Selected.");
                        }
                    } else {
                        //clearAllFields();
                        //fillSalesPreformaItemTable();
                        itemCodeText.selectAll();
                    }
                } else {
                    MsgBox.warning("Item discount is more than Item price");
                }

            } else {
                MsgBox.warning("Selling Price should be  greater than or Equal to Item Price ");
            }
        } catch (Exception e) {
            log.error("addItemButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_addItemButtonActionPerformed

    private void itemCodeTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemCodeTextKeyPressed
        try {
            ItemService itemService = new ItemService();
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                String productId = itemCodeText.getText().trim();
                List<Item> items = itemService.populateSalesPreformaPopupTableByProductId(productId, popupTableDialog, salesPreformaItemList,currency,salesPreformaItemListHistory);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();

                itemQtyText.requestFocusInWindow();

                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    item = items.get(index);
                    itemCodeText.setText(item.getItemCode());
                    itemDescriptionText.setText(item.getDescription());
                    availableQtyLabel.setText(item.getAvailableQty() + "");
                    itemWeightText.setText(item.getWeight() + "");
                    itemGodownText.setText(item.getGodown().getName());
                    itemPriceText.setText((item.getSellingPrice()/currency.getRate()) + "");
                    itemQty = itemDAO.findTotItemQty(item.getItemCode());
                    totAvalQtyLabel.setText(itemQty + "");
                } else {
                    MsgBox.warning("No Item Selected.");
                    itemQtyText.requestFocusInWindow();
                }
            }
        } catch (Exception e) {
            log.error("productIdTextKeyPressed:", e);
        }
    }//GEN-LAST:event_itemCodeTextKeyPressed

    private void itemDescriptionTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemDescriptionTextKeyPressed
        try {
            ItemService itemService = new ItemService();
            PopupTableDialog popupTableDialog = new PopupTableDialog(mainForm, true);
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                String itemName = itemDescriptionText.getText().trim();
                List<Item> items = itemService.populateSalesPreformaPopupTableByItemName(itemName, popupTableDialog, salesPreformaItemList,currency,salesPreformaItemListHistory);
                popupTableDialog.setVisible(true);
                ArrayList selectedRow = popupTableDialog.getSelectedRow();

                itemQtyText.requestFocusInWindow();

                if (null != selectedRow) {
                    int index = Integer.parseInt(selectedRow.get(0) + "") - 1;
                    item = items.get(index);
                    itemCodeText.setText(item.getItemCode());
                    itemDescriptionText.setText(item.getDescription());
                    availableQtyLabel.setText(item.getAvailableQty() + "");
                    itemWeightText.setText(item.getWeight() + "");
                    itemGodownText.setText(item.getGodown().getName());
                    itemPriceText.setText(item.getSellingPrice()/currency.getRate() + "");
                    itemQty = itemDAO.findTotItemQty(item.getItemCode());
                    totAvalQtyLabel.setText(itemQty + "");
                } else {
                    MsgBox.warning("No Item Selected.");
                    itemDescriptionText.requestFocusInWindow();
                }
            }
        } catch (Exception e) {
            log.error("itemNameTextKeyPressed:", e);
        }
    }//GEN-LAST:event_itemDescriptionTextKeyPressed

    private void itemDiscountTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_itemDiscountTextFocusGained
        itemDiscountText.selectAll();
    }//GEN-LAST:event_itemDiscountTextFocusGained

    private void itemDiscountTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemDiscountTextKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemDiscountTextKeyPressed

    private void itemDiscountTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemDiscountTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_itemDiscountTextKeyTyped

    private void discountTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_discountTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_discountTextActionPerformed

    private void totDiscountAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totDiscountAddButtonActionPerformed

        if (totDiscountAddButton.getText().trim().equalsIgnoreCase("+")) {
            totDiscount = Double.parseDouble(discountText.getText().trim());
        } else {
            totDiscount = 0;
            discountText.setText("0.00");
            totDiscountAddButton.setText("+");
        }

        fillSalesPreformaItemTable();
    }//GEN-LAST:event_totDiscountAddButtonActionPerformed

    private void itemCodeTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemCodeTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemCodeTextActionPerformed

    private void printButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printButtonActionPerformed
        try {

            if (salesPreformaItemList.size() > 0) {
                if (expiryDateChooser.getDate() != null && billDateFormattedText.getDate() != null && customerLedger != null && salesPreformaItemList != null) {
                    if (MsgBox.confirm("Are you sure you want to save and print?")) {
                        saveSalesPreforma();
                        //print
                        SalesPreformaReport report = new SalesPreformaReport();
                        if (report.createSalesPreformaReport(salesPreforma, "print")) {
                            clearAllFields();
                        } else {
                            MsgBox.warning("Something went wrong, please re-click the button again!");
                        }
                    }
                } else {
                    MsgBox.warning("Required fields are not entered !");
                }
            } else {
                MsgBox.warning("You don't have any item to save & print..");
            }
        } catch (Exception ex) {
            log.error("printButtonActionPerformed:", ex);
        }

    }//GEN-LAST:event_printButtonActionPerformed

    private void multiCurrencyComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_multiCurrencyComboBoxMouseClicked
        // TODO add your handling code here:
        multiCurrencyComboBox.removeAllItems();
        for (MultiCurrency currency : multiCurrencyDAO.findAll()) {
            if (currency == null) {
                this.currency = currency;
            }
            multiCurrencyComboBox.addItem(new ComboKeyValue(currency.getName(), currency));
        }
    }//GEN-LAST:event_multiCurrencyComboBoxMouseClicked

    private void taxComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_taxComboBoxMouseClicked
        // TODO add your handling code here:
        taxComboBox.removeAllItems();
        for (Tax tacs : taxDAO.findAll()) {
            taxComboBox.addItem(new ComboKeyValue((tacs.getName()), tacs));
        }
    }//GEN-LAST:event_taxComboBoxMouseClicked

    private void chargesComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chargesComboBoxMouseClicked
        // TODO add your handling code here:
        chargesComboBox.removeAllItems();
        for (Charges charges : chargesDAO.findAll()) {
            chargesComboBox.addItem(new ComboKeyValue(charges.getName(), charges));
        }
    }//GEN-LAST:event_chargesComboBoxMouseClicked

    private void shipmentComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_shipmentComboBoxMouseClicked
        // TODO add your handling code here:
        shipmentComboBox.removeAllItems();
        for (ShipmentMode shipmentMode : shipmentModeDAO.findAll()) {
            shipmentComboBox.addItem(new ComboKeyValue(shipmentMode.getName(), shipmentMode));
        }
    }//GEN-LAST:event_shipmentComboBoxMouseClicked

    private void pdfButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdfButtonActionPerformed
        try {
            if (expiryDateChooser.getDate() != null && billDateFormattedText.getDate() != null && customerLedger != null && salesPreformaItemList != null) {
                if (MsgBox.confirm("Are you sure you want to save and create pdf?")) {
                    saveSalesPreforma();
                    SalesPreformaReport report = new SalesPreformaReport();
                    if (report.createSalesPreformaReport(salesPreforma, "pdf")) {
                        clearAllFields();
                    } else {
                        MsgBox.warning("Something went wrong, please re-click the button again!");
                    }
                }
            } else {
                MsgBox.warning("Required fields are not entered");
            }
        } catch (Exception e) {
            log.error("printButtonActionPerformed", e);
        }
    }//GEN-LAST:event_pdfButtonActionPerformed

    private void billDateFormattedTextPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_billDateFormattedTextPropertyChange
        expiryDateChooser.setDate(commonService.addDaysToDate(billDateFormattedText.getDate(), 10));
    }//GEN-LAST:event_billDateFormattedTextPropertyChange

    private void multiCurrencyComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_multiCurrencyComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_multiCurrencyComboBoxActionPerformed

    private void discountTextFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_discountTextFocusGained
        discountText.selectAll();
    }//GEN-LAST:event_discountTextFocusGained

    void clearAllFields() {
        try {
            billDateFormattedText.setDate(new java.util.Date());
            CommonService.clearTextFields(new JTextField[]{deliveryText, customerText, deliveryText, paymentTermsText, deleveryTermsText, packingText, invoiceNumberText});
            CommonService.clearCurrencyFields(new JTextField[]{chargesText});
            multiCurrencyComboBox.setSelectedIndex(0);
            taxComboBox.setSelectedIndex(0);
            chargesComboBox.setSelectedIndex(0);
            shipmentComboBox.setSelectedIndex(0);
            addressTextArea.setText("");
            List<SalesPreformaItem> salesPreformaItemList
                    = salesPreformaItemList = new ArrayList<SalesPreformaItem>();
            salesPreformaTaxList = null;
            salesPreformaChargesList = null;
            customerLedger = null;
            grossAmntLabel.setText("0.00");
            grandTotal = 0.00;
            subTotal = 0.00;
            salesPreformaItemTableModel.setRowCount(0);
            Object currencyItem = multiCurrencyComboBox.getSelectedItem();
            currency = (MultiCurrency) ((ComboKeyValue) currencyItem).getValue();
            String currencySymbol = (null == currency) ? "" : currency.getSymbol();
            grandTotalLabel.setText(currencySymbol + " " + grandTotal);
            numberProperty = numberPropertyDAO.findInvoiceNo("SalesPreforma");
            invoiceNumberText.setText(numberProperty.getCategoryPrefix() + numberProperty.getNumber());
            salesPreforma = null;
            totDiscount = 0.00;
            discountText.setText("0.00");
        } catch (Exception e) {
            log.error("clearAllFields:", e);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel InvoiceDetailsPanel;
    private javax.swing.JButton addItemButton;
    private javax.swing.JTextArea addressTextArea;
    private javax.swing.JLabel availableQtyLabel;
    private com.toedter.calendar.JDateChooser billDateFormattedText;
    private javax.swing.JPanel bottomPanel;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton chargesAddButton;
    private javax.swing.JComboBox chargesComboBox;
    private javax.swing.JButton chargesMinusButton;
    private javax.swing.JPanel chargesPanel;
    private javax.swing.JFormattedTextField chargesText;
    private javax.swing.JTextField customerText;
    private javax.swing.JTextField deleveryTermsText;
    private javax.swing.JTextField deliveryText;
    private javax.swing.JTextField discountText;
    private com.toedter.calendar.JDateChooser expiryDateChooser;
    private javax.swing.JLabel grandTotalLabel;
    private javax.swing.JPanel grandTotalPanel;
    private javax.swing.JLabel grossAmntLabel;
    private javax.swing.JLabel grossAmountLabel;
    private javax.swing.JTextField invoiceNumberText;
    private javax.swing.JTextField itemCodeText;
    private javax.swing.JTextField itemDescriptionText;
    private javax.swing.JTextField itemDiscountText;
    private javax.swing.JTextField itemGodownText;
    private javax.swing.JPanel itemInfoPanel;
    private javax.swing.JTextField itemPriceText;
    private javax.swing.JTextField itemQtyText;
    private javax.swing.JButton itemRemoveButton;
    private javax.swing.JTextField itemWeightText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JComboBox multiCurrencyComboBox;
    private javax.swing.JButton newButton;
    private javax.swing.JTextField packingText;
    private javax.swing.JTextField paymentTermsText;
    private javax.swing.JButton pdfButton;
    private javax.swing.JButton printButton;
    private javax.swing.JTable salePreformaItemTable;
    private javax.swing.JButton saveButton;
    private javax.swing.JComboBox shipmentComboBox;
    private javax.swing.JPanel shipmentModePanel;
    private javax.swing.JPanel tableBorderPanel;
    private javax.swing.JButton taxAddButton;
    private javax.swing.JComboBox taxComboBox;
    private javax.swing.JPanel taxPanel;
    private javax.swing.JLabel totAvalQtyLabel;
    private javax.swing.JButton totDiscountAddButton;
    // End of variables declaration//GEN-END:variables
}
