package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.components.ComboKeyValue;
import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.CompanyDAO;
import com.cloudsendsoft.inventory.dao.GodownDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.NumberPropertyDAO;
import com.cloudsendsoft.inventory.dao.PackingTypeDAO;
import com.cloudsendsoft.inventory.dao.PurchaseDAO;
import com.cloudsendsoft.inventory.dao.PurchaseReturnDAO;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesPreformaDAO;
import com.cloudsendsoft.inventory.dao.SalesReturnDAO;
import com.cloudsendsoft.inventory.dao.ShipmentModeDAO;
import com.cloudsendsoft.inventory.dao.StockGroupDAO;
import com.cloudsendsoft.inventory.dao.TaxDAO;
import com.cloudsendsoft.inventory.dao.UnitDAO;
import com.cloudsendsoft.inventory.services.ChargesService;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.services.StockGroupService;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Company;
import com.cloudsendsoft.inventory.model.Godown;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.NumberProperty;
import com.cloudsendsoft.inventory.model.PackingType;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseCharge;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseReturn;
import com.cloudsendsoft.inventory.model.PurchaseReturnItem;
import com.cloudsendsoft.inventory.model.PurchaseTax;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesPreforma;
import com.cloudsendsoft.inventory.model.SalesPreformaItem;
import com.cloudsendsoft.inventory.model.SalesReturn;
import com.cloudsendsoft.inventory.model.SalesReturnItem;
import com.cloudsendsoft.inventory.model.ShipmentMode;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.model.Tax;
import com.cloudsendsoft.inventory.model.Unit;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author sangeeth
 */
public class SplitCompanyView extends javax.swing.JInternalFrame {

    /**
     * Creates new form AddItem
     */
    static final Logger log = Logger.getLogger(SplitCompanyView.class.getName());
    CRUDServices cRUDServices = new CRUDServices();
    CompanyDAO companyDAO = new CompanyDAO();

    PurchaseDAO purchaseDAO = new PurchaseDAO();
    PurchaseReturnDAO purchaseReturnDAO = new PurchaseReturnDAO();

    SalesDAO salesDAO = new SalesDAO();
    SalesReturnDAO salesReturnDAO = new SalesReturnDAO();

    //using for Commercial Invoice
    SalesPreformaDAO salesPreformaDAO = new SalesPreformaDAO();

    ItemDAO itemDAO = new ItemDAO();

    List<PurchaseItem> listofPurchaseItem = new ArrayList<>(0);

    Company refCompany = null;
    Company company=null;
    
    public SplitCompanyView() {
        initComponents();

        if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
            mainPanel.remove(buttonPanel);
            mainPanel.repaint();
            mainPanel.revalidate();
        }

        this.getRootPane().setDefaultButton(addButton);
        KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);

        fiancialYearStart.setDate(GlobalProperty.getCompany().getStartDate());
        fiancialYearEnd.setDate(GlobalProperty.getCompany().getEndDate());

        //load reference company
        for (Company comp : companyDAO.findAll()) {
            refCompanyComboBox.addItem(new ComboKeyValue((comp.getName()), comp));
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        fiancialYearStart = new com.toedter.calendar.JDateChooser();
        jLabel23 = new javax.swing.JLabel();
        fiancialYearEnd = new com.toedter.calendar.JDateChooser();
        refCompanyComboBox = new javax.swing.JComboBox();
        buttonPanel = new javax.swing.JPanel();
        addButton = new javax.swing.JButton();

        setClosable(true);
        setTitle("Split Company");
        getContentPane().setLayout(new java.awt.CardLayout());

        mainPanel.setBackground(new java.awt.Color(255, 255, 255));

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel4.setOpaque(false);

        jLabel1.setText("Unit Name");

        jLabel9.setText("Financial Year from");

        fiancialYearStart.setDateFormatString("dd/MM/yyyy");
        fiancialYearStart.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                fiancialYearStartPropertyChange(evt);
            }
        });

        jLabel23.setText("To");

        fiancialYearEnd.setDateFormatString("dd/MM/yyyy");
        fiancialYearEnd.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                fiancialYearEndPropertyChange(evt);
            }
        });

        refCompanyComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "N/A" }));
        refCompanyComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                refCompanyComboBoxItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(16, 16, 16)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(fiancialYearStart, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fiancialYearEnd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(refCompanyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(refCompanyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30)
                        .addComponent(jLabel9))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(fiancialYearStart, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(fiancialYearEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        buttonPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new java.awt.GridLayout(1, 0));

        addButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        addButton.setText("Save");
        addButton.setEnabled(false);
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(addButton);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(mainPanel, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        try {
            System.out.println("inside...");
            java.util.Date startDate = fiancialYearStart.getDate();
            java.util.Date endDate = fiancialYearEnd.getDate();

            //company information
            company = new Company();
            company.setAccountName(GlobalProperty.getCompany().getAccountName());
            company.setAccountNumber(GlobalProperty.getCompany().getAccountNumber());
            company.setBankName(GlobalProperty.getCompany().getBankName());
            company.setBankPlace(GlobalProperty.getCompany().getBankPlace());
            company.setBranch(GlobalProperty.getCompany().getBranch());
            company.setEmail(GlobalProperty.getCompany().getEmail());
            company.setEndDate(new java.sql.Date(endDate.getTime()));
            company.setFax(GlobalProperty.getCompany().getFax());
            company.setIncomeTaxNumber(GlobalProperty.getCompany().getIncomeTaxNumber());
            company.setIsDefault(false);
            company.setMailingAddress(GlobalProperty.getCompany().getMailingAddress());
            company.setManagingDirector(GlobalProperty.getCompany().getManagingDirector());
            company.setName(GlobalProperty.getCompany().getName());
            company.setPhone(GlobalProperty.getCompany().getPhone());
            company.setPinCode(GlobalProperty.getCompany().getPinCode());
            company.setSalesTaxNumber(GlobalProperty.getCompany().getSalesTaxNumber());
            company.setStartDate(new java.sql.Date(startDate.getTime()));
            company.setState(GlobalProperty.getCompany().getState());
            company.setSwiftCode(GlobalProperty.getCompany().getSwiftCode());
            cRUDServices.saveOrUpdateModel(company);
           
            //Adding charges    
            ChargesDAO chargesDAO = new ChargesDAO();
            List<Charges> chargesList = chargesDAO.findAll(refCompany);
            for (Charges chrg : chargesList) {
                Charges newChrg = new Charges();
                newChrg.setName(chrg.getName());
                newChrg.setCompany(company);
                cRUDServices.saveOrUpdateModel(newChrg);

            }
            
            //Adding godown    
            GodownDAO godownDAO = new GodownDAO();
            List<Godown> godownList = godownDAO.findAll();
            for (Godown godown : godownList) {
                Godown newGodown = new Godown();
                newGodown.setName(godown.getName());
                newGodown.setColor(godown.getColor());
                newGodown.setCompany(company);
                cRUDServices.saveOrUpdateModel(newGodown);
            }
            //Adding StockGroup    
            StockGroupDAO stockGroupDAO = new StockGroupDAO();
            List<StockGroup> stockGroupList = stockGroupDAO.findAll();
            for (StockGroup stockGroup : stockGroupList) {
                StockGroup newStockGroup = new StockGroup();
                newStockGroup.setName(stockGroup.getName());
                newStockGroup.setUnderGroup(stockGroup.getUnderGroup());
                newStockGroup.setCompany(company);
                cRUDServices.saveOrUpdateModel(newStockGroup);
            }
            //Adding Ledger Group    
            LedgerDAO ledgerDAO = new LedgerDAO();
            List<Ledger> ledgerList = ledgerDAO.findAll();
            for (Ledger ledger : ledgerList) {
                Ledger newLedger = new Ledger();
                newLedger.setLedgerName(ledger.getLedgerName());
                newLedger.setOpeningBalance(ledger.getAvailableBalance());
                newLedger.setAvailableBalance(ledger.getAvailableBalance());
                newLedger.setAvailableBalance1(ledger.getAvailableBalance1());
                newLedger.setCompany(company);
                newLedger.setLedgerGroup(ledger.getLedgerGroup());
                newLedger.setAddress(ledger.getAddress());
                newLedger.setEmail(ledger.getEmail());
                newLedger.setFax(ledger.getFax());
                newLedger.setIncomeTaxNo(ledger.getIncomeTaxNo());
                cRUDServices.saveOrUpdateModel(newLedger);
            }
            
            //Adding Number Property
            List<NumberProperty> numberPropertyList = null;
            NumberPropertyDAO numberPropertyDAO = new NumberPropertyDAO();
            numberPropertyList = numberPropertyDAO.findAll(refCompany);
            for (NumberProperty numberProp : numberPropertyList) {
                NumberProperty newNumberProperty = new NumberProperty();
                newNumberProperty.setCategory(numberProp.getCategory());
                newNumberProperty.setCategoryPrefix(numberProp.getCategoryPrefix());
                newNumberProperty.setNumber(numberProp.getNumber() + 1);
                newNumberProperty.setCompany(company);
                cRUDServices.saveOrUpdateModel(newNumberProperty);
            }

            //Adding Packing Type
            PackingTypeDAO packingTypeDAO = new PackingTypeDAO();
            List<PackingType> packingTypeList = packingTypeDAO.findAll(refCompany);
            for (PackingType packingType : packingTypeList) {
                PackingType newPackingType = new PackingType();
                newPackingType.setName(packingType.getName());
                newPackingType.setWeight(packingType.getWeight());
                newPackingType.setCompany(company);
                cRUDServices.saveOrUpdateModel(newPackingType);
            }

            List<ShipmentMode> shipmentModesList = null;
            ShipmentModeDAO shipmentModeDAO = new ShipmentModeDAO();
            shipmentModesList = shipmentModeDAO.findAll(refCompany);
            for (ShipmentMode shipmentMode : shipmentModesList) {
                ShipmentMode newShipmentMode = new ShipmentMode();
                newShipmentMode.setName(shipmentMode.getName());
                newShipmentMode.setCompany(company);
                cRUDServices.saveOrUpdateModel(newShipmentMode);
            }
            
            List<Tax> taxList = null;
            TaxDAO taxDAO = new TaxDAO();
            taxList = taxDAO.findAll();
            for (Tax tax : taxList) {
                Tax newTax = new Tax();
                newTax.setName(tax.getName());
                newTax.setTaxRate(tax.getTaxRate());
                newTax.setCompany(company);
                cRUDServices.saveOrUpdateModel(newTax);
            }
            
            List<Unit> unitList = null;
            UnitDAO unitDAO = new UnitDAO();
            unitList = unitDAO.findAll();
            for (Unit unit : unitList) {
                Unit newUnit = new Unit();
                newUnit.setName(unit.getName());
                newUnit.setCompany(company);
                cRUDServices.saveOrUpdateModel(newUnit);

            }

            //...................*********************...................
            //Closing Stock - Using FIFO methode
            //To get all exact purchase items after deducting purchase returns qty
            List<Purchase> listOfPurchase = purchaseDAO.findAllByBillDateAscTill(startDate);
            List<PurchaseReturn> listOfPurchaseReturns = purchaseReturnDAO.findAllPurchaseReturnTill(startDate);
            List<Sales> listOfSales = salesDAO.findAllSales();
            List<SalesReturn> listOfSalesReturns = salesReturnDAO.findAllSalesReturnTill(startDate);
            List<SalesPreforma> listOfCommercialInvoice = salesPreformaDAO.findAllSalesPreformaCommercialInvoiceTill(startDate);

            HashMap<Item, Double> itemAvailableQtyHashMap = new HashMap<>(0);
            HashMap<Item, Double> itemAvailableBalanceHashMap = new HashMap<>(0);
            Set<String> stockGroupNameList = new TreeSet<String>();

            //opening...
            List<Item> listOfItems = itemDAO.findAll();
            //calculating available qty by adding salesReturn 
            for (Item item : listOfItems) {
                if (itemAvailableQtyHashMap.get(item) == null) {
                    itemAvailableQtyHashMap.put(item, item.getOpeningQty());
                    itemAvailableBalanceHashMap.put(item, item.getOpeningQty() * item.getOpeningUnitPrice());//to get total balance of each item
                } else {
                    itemAvailableQtyHashMap.put(item, item.getOpeningQty() + itemAvailableQtyHashMap.get(item));
                    itemAvailableBalanceHashMap.put(item, (item.getOpeningQty() * item.getOpeningUnitPrice()) + itemAvailableBalanceHashMap.get(item));
                }
                stockGroupNameList.add(item.getStockGroup().getName());
            }

            HashMap<Purchase, PurchaseReturn> purchasePurchaseReturnHashMap = new HashMap<Purchase, PurchaseReturn>(0);
            for (PurchaseReturn purchaseReturn : listOfPurchaseReturns) {
                purchasePurchaseReturnHashMap.put(purchaseReturn.getPurchase(), purchaseReturn);
            }
            for (Purchase purchase : listOfPurchase) {
                PurchaseReturn purchaseReturn = purchasePurchaseReturnHashMap.get(purchase);
                for (PurchaseItem purchaseItem : purchase.getPurchaseItems()) {
                    if (purchaseReturn != null) {
                        for (PurchaseReturnItem purchaseReturnItem : purchaseReturn.getPurchaseReturnItems()) {
                            if (purchaseReturnItem.getItem().equals(purchaseItem.getItem())) {
                                purchaseItem.setQuantity(purchaseItem.getQuantity() - purchaseReturnItem.getQuantity());
                                break;
                            }
                        }
                    }
                    listofPurchaseItem.add(purchaseItem);
                   
                    if (itemAvailableQtyHashMap.get(purchaseItem.getItem()) == null) {
                        itemAvailableQtyHashMap.put(purchaseItem.getItem(), purchaseItem.getQuantity());
                        itemAvailableBalanceHashMap.put(purchaseItem.getItem(), (purchaseItem.getQuantity() * purchaseItem.getUnitPrice()));
                    } else {
                        itemAvailableQtyHashMap.put(purchaseItem.getItem(), purchaseItem.getQuantity() + itemAvailableQtyHashMap.get(purchaseItem.getItem()));
                        itemAvailableBalanceHashMap.put(purchaseItem.getItem(), (purchaseItem.getQuantity() * purchaseItem.getUnitPrice()) + itemAvailableBalanceHashMap.get(purchaseItem.getItem()));
                    }
                    //for showing stockgroup name under closing stock as a category
                    stockGroupNameList.add(purchaseItem.getItem().getStockGroup().getName());
                }
            }
            //calculating available qty by adding salesReturn 
            for (SalesReturn salesReturn : listOfSalesReturns) {
                for (SalesReturnItem salesReturnItem : salesReturn.getSalesReturnItems()) {
                    if (itemAvailableQtyHashMap.get(salesReturnItem.getItem()) == null) {
                        itemAvailableQtyHashMap.put(salesReturnItem.getItem(), salesReturnItem.getQuantity());
                        itemAvailableBalanceHashMap.put(salesReturnItem.getItem(), (salesReturnItem.getQuantity() * salesReturnItem.getUnitPrice()));
                    } else {
                        itemAvailableQtyHashMap.put(salesReturnItem.getItem(), salesReturnItem.getQuantity() + itemAvailableQtyHashMap.get(salesReturnItem.getItem()));
                        itemAvailableBalanceHashMap.put(salesReturnItem.getItem(), (salesReturnItem.getQuantity() * salesReturnItem.getUnitPrice()) + itemAvailableBalanceHashMap.get(salesReturnItem.getItem()));
                    }
                    stockGroupNameList.add(salesReturnItem.getItem().getStockGroup().getName());
                }

            }
            //calculating available qty by deducting sales 
            for (Sales sales : listOfSales) {
                for (SalesItem salesItem : sales.getSalesItems()) {
                    if (itemAvailableQtyHashMap.get(salesItem.getItem()) == null) {
                        itemAvailableQtyHashMap.put(salesItem.getItem(), salesItem.getQuantity());//(double) removed
                        itemAvailableBalanceHashMap.put(salesItem.getItem(), (salesItem.getQuantity() * salesItem.getUnitPrice()));
                    } else {
                        itemAvailableQtyHashMap.put(salesItem.getItem(), itemAvailableQtyHashMap.get(salesItem.getItem()) - salesItem.getQuantity());
                        itemAvailableBalanceHashMap.put(salesItem.getItem(), itemAvailableBalanceHashMap.get(salesItem.getItem()) - (salesItem.getQuantity() * salesItem.getUnitPrice()));
                    }
                    stockGroupNameList.add(salesItem.getItem().getStockGroup().getName());
                }
            }

            //calculating available qty by deducting Commercial Invoice 
            for (SalesPreforma commercialInvoice : listOfCommercialInvoice) {
                for (SalesPreformaItem salesPreformaItem : commercialInvoice.getSalesPreformaItems()) {
                    if (itemAvailableQtyHashMap.get(salesPreformaItem.getItem()) == null) {
                        itemAvailableQtyHashMap.put(salesPreformaItem.getItem(), salesPreformaItem.getQuantity());//(double) removed
                        itemAvailableBalanceHashMap.put(salesPreformaItem.getItem(), (salesPreformaItem.getQuantity() * salesPreformaItem.getUnitPrice()));
                    } else {
                        itemAvailableQtyHashMap.put(salesPreformaItem.getItem(), itemAvailableQtyHashMap.get(salesPreformaItem.getItem()) - salesPreformaItem.getQuantity());
                        itemAvailableBalanceHashMap.put(salesPreformaItem.getItem(), itemAvailableBalanceHashMap.get(salesPreformaItem.getItem()) - (salesPreformaItem.getQuantity() * salesPreformaItem.getUnitPrice()));
                    }
                    stockGroupNameList.add(salesPreformaItem.getItem().getStockGroup().getName());
                }
            }

            List<Purchase> listOfPurchaseDesc = purchaseDAO.findAllByBillDateDescTill(startDate);
            Iterator it = itemAvailableQtyHashMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();
                Item item = (Item) pairs.getKey();

                System.out.println("item...      +"+item.getDescription()+" "+item.getAvailableQty());
                System.out.println("item... qty :+"+item.getDescription()+" "+(Double)pairs.getValue());
                System.out.println("item... bal :+"+item.getDescription()+" "+itemAvailableBalanceHashMap.get(item));
                
                //insert each item with closing qty & balance 
                Item newItem=new Item();
                newItem.setAvailableQty((Double)pairs.getValue());
                newItem.setCompany(company);
                newItem.setDescription(item.getDescription());
                
                newItem.setGodown(godownDAO.findByNameAndCompany(item.getGodown().getName(),company));
                
                newItem.setItemCode(item.getItemCode());
                newItem.setOpeningBal(itemAvailableBalanceHashMap.get(item));
                newItem.setOpeningQty((Double)pairs.getValue());
                newItem.setOpeningUnitPrice(0.00);
                newItem.setSellingPrice(item.getSellingPrice());
                
                StockGroupDAO subStockGroupDAO=new StockGroupDAO();
                newItem.setStockGroup(subStockGroupDAO.findByNameAndCompany(item.getStockGroup().getName(),company));
                
                UnitDAO unitDAO1=new UnitDAO();
                newItem.setUnit(unitDAO1.findByNameAndCompany(item.getUnit().getName(), company));
                
                newItem.setVisible(item.isVisible());
                newItem.setWeight(item.getWeight());
                cRUDServices.saveOrUpdateModel(newItem);
                
                //to get purchases of closing qty for transfering into new company
                double totalClosingQty = (Double) pairs.getValue();
                if (totalClosingQty > 0) {
                    for (Purchase purchase : listOfPurchaseDesc) {
                        PurchaseReturn purchaseReturn = purchasePurchaseReturnHashMap.get(purchase);
                        boolean flag = false;
                        for (PurchaseItem purchaseItem : purchase.getPurchaseItems()) {
                            if (purchaseReturn != null) {
                                if (purchaseReturn.getBillDate().before(startDate)) {
                                    for (PurchaseReturnItem purchaseReturnItem : purchaseReturn.getPurchaseReturnItems()) {
                                        if (purchaseReturnItem.getItem().equals(purchaseItem.getItem())) {
                                            purchaseItem.setQuantity(purchaseItem.getQuantity() - purchaseReturnItem.getQuantity());
                                            break;
                                        }
                                    }
                                }
                            }
                            if (purchaseItem.getItem().equals(item)) {
                                totalClosingQty -= purchaseItem.getQuantity();
                                flag = true;
                            }
                            if (totalClosingQty <= 0) {
                                break;
                            }
                        }
                        if (flag) {
                            System.out.println(".....transfer purchase..." + purchase.getInvoiceNumber());
                            
                            //insert remaining Purchases for new company (Closing stock purchases)
                            Purchase newPurchase=new Purchase();
                            newPurchase.setAddress(purchase.getAddress());
                            newPurchase.setBillDate(purchase.getBillDate());
                            newPurchase.setCompany(company);
                            newPurchase.setCurrentCurrencyRate(purchase.getCurrentCurrencyRate());
                            newPurchase.setDeliveryDate(purchase.getDeliveryDate());
                            newPurchase.setDiscount(purchase.getDiscount());
                            newPurchase.setGrandTotal(purchase.getGrandTotal());
                            newPurchase.setInvoiceNumber(purchase.getInvoiceNumber());
                            newPurchase.setMultiCurrency(purchase.getMultiCurrency());
                            
                            List<PurchaseCharge> chrgList=new ArrayList<>();
                            for(PurchaseCharge purchaseCharge:purchase.getPurchaseCharges()){
                                PurchaseCharge purchaseCharge1=new PurchaseCharge();
                                purchaseCharge1.setAmount(purchaseCharge.getAmount());
                                purchaseCharge1.setCharges(chargesDAO.findByNameAndCompany(purchaseCharge.getCharges().getName(),company));
                                purchaseCharge1.setCompany(company);
                                chrgList.add(purchaseCharge1);
                            }
                            newPurchase.setPurchaseCharges(chrgList);
                            
                            List<PurchaseItem> pItemList=new ArrayList<>();
                            for(PurchaseItem purchaseItem:purchase.getPurchaseItems()){
                                PurchaseItem purchaseItem1=new PurchaseItem();
                                purchaseItem1.setCompany(company);
                                purchaseItem1.setDiscount(purchaseItem.getDiscount());
                                
                                String itemCode=purchaseItem.getItem().getItemCode();
                                Godown godown=godownDAO.findByNameAndCompany(purchaseItem.getItem().getGodown().getName(), company);
                                purchaseItem1.setItem(itemDAO.findAllByItemCodeCompanyGodown(itemCode, company, godown));
                                
                                purchaseItem1.setQuantity(purchaseItem.getQuantity());
                                purchaseItem1.setUnitPrice(purchaseItem.getUnitPrice());
                                pItemList.add(purchaseItem1);
                            }
                            newPurchase.setPurchaseItems(pItemList);
                            
                            List<PurchaseTax> pTaxList=new ArrayList<>();
                            for(PurchaseTax purchaseTax:pTaxList){
                                PurchaseTax purchaseTax1=new PurchaseTax();
                                purchaseTax1.setAmount(purchaseTax.getAmount());
                                purchaseTax1.setCompany(company);
                                purchaseTax1.setTax(taxDAO.findByNameAndCompany(purchaseTax.getTax().getName(),company));
                                purchaseTax1.setTaxRate(purchaseTax.getTaxRate());
                                pTaxList.add(purchaseTax1);
                            }
                            newPurchase.setPurchaseTaxes(pTaxList);
                            
                            newPurchase.setShipmentMode(purchase.getShipmentMode());
     ///////////////////////////////                       /////newPurchase.setSupplier(purchase.gets);
                            cRUDServices.saveOrUpdateModel(newPurchase);
                            
                        }
                        if (totalClosingQty <= 0) {
                            break;
                        }
                    }
                }
            }
            //...................*********Closing Stock End************...................

        } catch (Exception e) {
            log.error("addButtonActionPerformed:", e);
        }
        MsgBox.success("Completed");
        this.dispose();

    }//GEN-LAST:event_addButtonActionPerformed

    private void refCompanyComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_refCompanyComboBoxItemStateChanged
        if (refCompanyComboBox.getSelectedItem() != "N/A") {
            //referencing company
            Object comp = refCompanyComboBox.getSelectedItem();
            refCompany = ((Company) ((ComboKeyValue) comp).getValue());
            fiancialYearStart.setDate(refCompany.getStartDate());
            fiancialYearEnd.setDate(refCompany.getEndDate());

            fiancialYearStart.setMinSelectableDate(refCompany.getStartDate());
            fiancialYearStart.setMaxSelectableDate(refCompany.getEndDate());

            fiancialYearEnd.setMinSelectableDate(refCompany.getStartDate());
            fiancialYearEnd.setMaxSelectableDate(refCompany.getEndDate());

        }
    }//GEN-LAST:event_refCompanyComboBoxItemStateChanged

    private void fiancialYearStartPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_fiancialYearStartPropertyChange
        fiancialYearEnd.setMinSelectableDate(fiancialYearStart.getDate());
    }//GEN-LAST:event_fiancialYearStartPropertyChange

    private void fiancialYearEndPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_fiancialYearEndPropertyChange
        fiancialYearStart.setMaxSelectableDate(fiancialYearEnd.getDate());
    }//GEN-LAST:event_fiancialYearEndPropertyChange


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JPanel buttonPanel;
    private com.toedter.calendar.JDateChooser fiancialYearEnd;
    private com.toedter.calendar.JDateChooser fiancialYearStart;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JComboBox refCompanyComboBox;
    // End of variables declaration//GEN-END:variables
}
