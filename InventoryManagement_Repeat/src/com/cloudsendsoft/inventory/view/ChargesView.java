package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.dao.CRUDServices;
import com.cloudsendsoft.inventory.dao.ChargesDAO;
import com.cloudsendsoft.inventory.dao.LedgerDAO;
import com.cloudsendsoft.inventory.dao.LedgerGroupDAO;
import com.cloudsendsoft.inventory.services.ChargesService;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.model.Charges;
import com.cloudsendsoft.inventory.model.Ledger;
import com.cloudsendsoft.inventory.model.LedgerGroup;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import com.cloudsendsoft.inventory.utilities.HibernateUtil;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sangeeth
 */
public class ChargesView extends javax.swing.JInternalFrame {

    /**
     * Creates new form AddItem
     */
    static final Logger log = Logger.getLogger(ChargesView.class.getName());
    CRUDServices cRUDServices = new CRUDServices();
    ;
    ChargesDAO chargesDAO = new ChargesDAO();
    ChargesService chargesService = new ChargesService();
    CommonService commonService = new CommonService();

    LedgerGroupDAO ledgerGroupDAO = new LedgerGroupDAO();
    LedgerGroup ledgerGroup = null;
    LedgerGroup ledgerGroupIncome = null;
    Ledger ledger = null;
    LedgerDAO ledgerDAO = new LedgerDAO();

    Charges charges = null;

    public ChargesView() {
        initComponents();
        this.getRootPane().setDefaultButton(addButton);
        KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);

        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);

        chargesListTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                null,
                new String[]{
                    "Charges"
                }
        ));
        chargesListTable.setRowHeight(GlobalProperty.tableRowHieght);

        ledgerGroup = ledgerGroupDAO.findByGroupName("Direct Expenses");
        //ledgerGroupIncome =ledgerGroupDAO.findByGroupName("Direct Incomes");
        
        chargesService.fillChargesListTable(chargesListTable);

        jScrollPane1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);

        if (!GlobalProperty.listOfPrivileges.contains("Edit")) {
            mainPanel.remove(buttonPanel);
            mainPanel.repaint();
            mainPanel.revalidate();
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        chargesListTable = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        chargesNameText = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        buttonPanel = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();
        updateButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();

        setClosable(true);
        setTitle("Charges");
        getContentPane().setLayout(new java.awt.CardLayout());

        mainPanel.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("List Of Charges"));
        jPanel2.setOpaque(false);

        chargesListTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        chargesListTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                chargesListTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(chargesListTable);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 508, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 465, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel4.setOpaque(false);

        jLabel1.setText("Charge Name");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(chargesNameText)
                .addGap(35, 35, 35))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(chargesNameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buttonPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new java.awt.GridLayout(1, 0));

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/new.png"))); // NOI18N
        newButton.setText("New");
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(newButton);

        addButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/save.png"))); // NOI18N
        addButton.setText("Save");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(addButton);

        updateButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/update.png"))); // NOI18N
        updateButton.setText("Update");
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(updateButton);

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/delete-icon.png"))); // NOI18N
        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(deleteButton);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(mainPanel, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        try {

            String chargesName = chargesNameText.getText().trim();
            charges = chargesDAO.findByName(chargesName);
            ledger = ledgerDAO.findByLedgerName(chargesName);
            if (null == charges && null == ledger) {
                if (CommonService.stringValidator(new String[]{chargesName})) {
                    if (MsgBox.confirm("Are you sure you want to save?")) {
                        charges = new Charges();
                        charges.setName(chargesName);
                        charges.setCompany(GlobalProperty.getCompany());
                        cRUDServices.saveModel(charges);

                        //adding tax to ledger under DirectExpense
                        ledger = new Ledger();
                        ledger.setAvailableBalance(0.00);
                        ledger.setLedgerName(chargesName);
                        ledger.setLedgerGroup(ledgerGroup);
                        ledger.setCompany(GlobalProperty.getCompany());
                        cRUDServices.saveModel(ledger);
                        
                        //adding tax to ledger under Direct Income
//                        ledger = new Ledger();
//                        ledger.setAvailableBalance(0.00);
//                        ledger.setLedgerName(chargesName+" Sales");
//                        ledger.setLedgerGroup(ledgerGroupIncome);
//                        ledger.setCompany(GlobalProperty.getCompany());
//                        cRUDServices.saveModel(ledger);
                        
                        MsgBox.success("Successfully Added " + chargesName);
                        CommonService.clearTextFields(new JTextField[]{chargesNameText});
                        chargesService.fillChargesListTable(chargesListTable);
                        chargesNameText.requestFocusInWindow();
                    }
                } else {
                    MsgBox.warning("Charges Name is mandatory.");
                    chargesNameText.requestFocusInWindow();
                }
            } else {
                if (null != chargesName) {
                    MsgBox.warning("Charges Name '" + chargesName + "' is already exists.");
                } else {
                    MsgBox.warning("Ledger Name '" + chargesName + "' is already exists.");
                }
                chargesNameText.requestFocusInWindow();
            }
        } catch (Exception e) {
            log.error("addButtonActionPerformed:", e);
        }

    }//GEN-LAST:event_addButtonActionPerformed

    private void chargesListTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chargesListTableMouseClicked
        try {

            ArrayList selectedRow = commonService.getTableRowData(chargesListTable);
            charges = chargesDAO.findByName(selectedRow.get(0) + "");
            chargesNameText.setText(charges.getName());
            ledger = ledgerDAO.findByLedgerName(selectedRow.get(0) + "");
        } catch (Exception e) {
            log.error("chargesListTableMouseClicked", e);
        }
    }//GEN-LAST:event_chargesListTableMouseClicked

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
        try {

            String chargesName = chargesNameText.getText().trim();
            if (null != charges && null != ledger) {
                if (CommonService.stringValidator(new String[]{chargesName})) {
                    if (MsgBox.confirm("Are you sure you want to update?")) {
                        charges.setName(chargesName);
                        cRUDServices.saveOrUpdateModel(charges);

                        ledger.setLedgerName(chargesName);
                        cRUDServices.saveOrUpdateModel(ledger);

                        MsgBox.success("Successfully Updated " + chargesName);
                        CommonService.clearTextFields(new JTextField[]{chargesNameText});
                        chargesService.fillChargesListTable(chargesListTable);
                        chargesNameText.requestFocusInWindow();
                    }
                } else {
                    MsgBox.warning("Charges Name is mandatory.");
                    chargesNameText.requestFocusInWindow();
                }
            } else {
                if (null == charges) {
                    MsgBox.warning("Charges Name '" + chargesName + "' Not Found.");
                } else {
                    MsgBox.warning("Ledger Name '" + chargesName + "' Not Found.");
                }
                chargesNameText.requestFocusInWindow();
            }
        } catch (Exception e) {
            log.error("updateButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_updateButtonActionPerformed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        CommonService.clearTextFields(new JTextField[]{chargesNameText});
    }//GEN-LAST:event_newButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        Session session = null;
        try {
            String chargesName = chargesNameText.getText().trim();
            if (null != charges && null != ledger) {
                if (!charges.getName().equalsIgnoreCase("N/A")) {
                    if (MsgBox.confirm("Are you sure you want to delete?")) {
                        try {
                            if (ledger.getAvailableBalance() == 0) {

                                SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
                                session = sessionFactory.openSession();
                                Transaction transaction = session.beginTransaction();
                                session.delete(charges);
                                transaction.commit();
                                session.clear();
                                session.close();

                                sessionFactory = HibernateUtil.getSessionFactory();
                                session = sessionFactory.openSession();
                                transaction = session.beginTransaction();
                                session.delete(ledger);
                                transaction.commit();

                                MsgBox.success("Successfully Deleted " + chargesName);
                                CommonService.clearTextFields(new JTextField[]{chargesNameText});
                                chargesService.fillChargesListTable(chargesListTable);
                                chargesNameText.requestFocusInWindow();
                            } else {
                                MsgBox.abort("Charge '" + chargesName + "' transactions already entered in ledger");
                            }
                        } catch (Exception e) {
                            MsgBox.warning("Tax '" + chargesName + "' is already used for transactions!");
                            chargesNameText.requestFocusInWindow();
                        } finally {
                            if (session != null) {
                                session.clear();
                                session.close();
                            }
                        }
                    }
                }

            } else {
                if (null == charges) {
                    MsgBox.warning("Charges Name '" + chargesName + "' Not Found.");
                } else {
                    MsgBox.warning("Ledger Name '" + chargesName + "' Not Found.");
                }
                chargesNameText.requestFocusInWindow();
            }
        } catch (Exception e) {
            log.error("deleteButtonActionPerformed:", e);
        }
    }//GEN-LAST:event_deleteButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JTable chargesListTable;
    private javax.swing.JTextField chargesNameText;
    private javax.swing.JButton deleteButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton newButton;
    private javax.swing.JButton updateButton;
    // End of variables declaration//GEN-END:variables
}
