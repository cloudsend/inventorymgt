
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.view;

import com.cloudsendsoft.inventory.bean.StockSummaryBean;
import com.cloudsendsoft.inventory.components.CTableModel;
import com.cloudsendsoft.inventory.dao.ClosingStockDAO;
import com.cloudsendsoft.inventory.dao.GodownDAO;
import com.cloudsendsoft.inventory.dao.ItemDAO;
import com.cloudsendsoft.inventory.dao.PurchaseDAO;
import com.cloudsendsoft.inventory.dao.PurchaseItemDAO;
import com.cloudsendsoft.inventory.dao.PurchaseReturnDAO;
import com.cloudsendsoft.inventory.dao.PurchaseReturnItemDAO;
import com.cloudsendsoft.inventory.dao.SalesDAO;
import com.cloudsendsoft.inventory.dao.SalesItemDAO;
import com.cloudsendsoft.inventory.dao.SalesReturnDAO;
import com.cloudsendsoft.inventory.dao.SalesReturnItemDAO;
import com.cloudsendsoft.inventory.dao.StockGroupDAO;
import com.cloudsendsoft.inventory.model.Godown;
import com.cloudsendsoft.inventory.services.CommonService;
import com.cloudsendsoft.inventory.model.Item;
import com.cloudsendsoft.inventory.model.Purchase;
import com.cloudsendsoft.inventory.model.PurchaseItem;
import com.cloudsendsoft.inventory.model.PurchaseReturn;
import com.cloudsendsoft.inventory.model.PurchaseReturnItem;
import com.cloudsendsoft.inventory.model.Sales;
import com.cloudsendsoft.inventory.model.SalesItem;
import com.cloudsendsoft.inventory.model.SalesReturn;
import com.cloudsendsoft.inventory.model.SalesReturnItem;
import com.cloudsendsoft.inventory.model.StockGroup;
import com.cloudsendsoft.inventory.report.JTableReport;
import com.cloudsendsoft.inventory.services.MsgBox;
import com.cloudsendsoft.inventory.services.PLAService;
import com.cloudsendsoft.inventory.services.StockGroupService;
import com.cloudsendsoft.inventory.utilities.GlobalProperty;
import static com.cloudsendsoft.inventory.utilities.GlobalProperty.company;
import static com.cloudsendsoft.inventory.view.MainForm.jMainFrame;
import static com.cloudsendsoft.inventory.view.PurchaseView.log;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Sangeeth
 */
public class StockSummary extends javax.swing.JPanel {

    static final Logger log = Logger.getLogger(StockSummary.class.getName());

    CommonService commonService = new CommonService();

    CTableModel stockSummaryTableModel = null;

    List<StockGroup> listOfStockGroup = null;
    List<StockGroup> listOfStockGroupToShowOnTable = new ArrayList<StockGroup>();

    List<Item> listOfItem = null;

    List<PurchaseItem> listOfPurchaseItem = null;
    List<Purchase> listOfPurchase = null;

    List<PurchaseReturnItem> listOfPurchaseReturnItem = null;
    List<PurchaseReturn> listOfPurchaseReturn = null;

    List<SalesItem> listOfSalesItem = null;
    List<Sales> listOfSales = null;

    List<SalesReturnItem> listOfSalesReturnItem = null;
    List<SalesReturn> listOfSalesReturn = null;

    List<Godown> listOfGodowns = null;
    StockGroup stockGroup = null;
    Item item = null;
    String month = null;
    Godown godown = null;

    StockGroupDAO stockGroupDAO = new StockGroupDAO();
    ItemDAO itemDAO = new ItemDAO();
    PurchaseDAO purchaseDAO = new PurchaseDAO();
    SalesDAO salesDAO = new SalesDAO();

    SalesReturnDAO salesReturnDAO = new SalesReturnDAO();
    PurchaseReturnDAO purchaseReturnDAO = new PurchaseReturnDAO();

    PurchaseItemDAO purchaseItemDAO = new PurchaseItemDAO();
    SalesItemDAO salesItemDAO = new SalesItemDAO();

    PurchaseReturnItemDAO purchaseReturnItemDAO = new PurchaseReturnItemDAO();
    SalesReturnItemDAO salesReturnItemDAO = new SalesReturnItemDAO();

    GodownDAO godownDAO = new GodownDAO();
    int screen = 1;
    int index = 0;
    String type = "Stock Group";
    String particularsField = "";

    String[] months = new String[13];

    String invoiceDate = null;
    String invoiceType = null;
    String invoiceLedger = null;
    String invoiceNumber = null;

    PurchaseView purchaseView = null;
    SalesView salesView = null;
    boolean nullSubList = false;
    boolean isDuplicateColumnsVisible = false;
    StockGroup tempStockGroup = new StockGroup();
    String nameSub = null;
    List<Item> itemList = null;
    PLAService plaService = new PLAService();

    ClosingStockDAO closingStockDAO = new ClosingStockDAO();
    List closingQtyValue = null;
    List closingItemsList = null;

    Date fromDate = GlobalProperty.getCompany().getStartDate();
    Date toDate = GlobalProperty.getCompany().getEndDate();
    public StockSummary() {
        initComponents();
        log.info("${app.root}/:" + System.getProperty("java.io.tmpdir"));
        try {
            
            commonService.setCurrentPeriodOnCalendar(fromDateChooser);
            commonService.setCurrentPeriodOnCalendar(toDateChooser);
            fromDateChooser.setDate(GlobalProperty.getCompany().getStartDate());
            toDateChooser.setDate(GlobalProperty.getCompany().getEndDate());
            //shortcut

            //table background color settings
            jScrollPane1.setOpaque(false);
            jScrollPane1.getViewport().setOpaque(false);
            //set table design
            stockSummaryTable.setRowHeight(GlobalProperty.tableRowHieght);
            stockSummaryTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {

                //alternate row color jtable
                @Override
                public Component getTableCellRendererComponent(JTable table,
                        Object value, boolean isSelected, boolean hasFocus,
                        int row, int column) {
                    Component c = super.getTableCellRendererComponent(table,
                            value, isSelected, hasFocus, row, column);
                    c.setBackground(row % 2 == 0 ? Color.GRAY : Color.LIGHT_GRAY);
                    return c;
                }
            ;
            });

            listOfStockGroup = stockGroupDAO.findAllByNameASC();
            for (StockGroup stockGroup : listOfStockGroup) {
                if (stockGroup.getName().equalsIgnoreCase("General")) {
                    listOfStockGroupToShowOnTable.add(stockGroup);
                    break;
                }
            }
            stockGroup = listOfStockGroupToShowOnTable.get(0);
//            listOfPurchase = purchaseDAO.findAllByBillDateASC(fromDate, toDate);
//            listOfSales = salesDAO.findAllByDate(fromDate, toDate);
//            listOfPurchaseReturn = purchaseReturnDAO.findAllByDate(fromDate, toDate);
//            listOfSalesReturn = salesReturnDAO.findAllByDate(fromDate, toDate);
            //month from company start month to end month
            String monthNames[] = new DateFormatSymbols().getMonths();
            int startIndex = GlobalProperty.company.getStartDate().getMonth();
            int j = 0, i = 0, k = 0;
            for (i = startIndex, j = 0; i < monthNames.length; i++, j++) {
                months[j] = monthNames[i];
            }
            j--;
            for (k = 0; k < startIndex; k++, j++) {
                months[j] = monthNames[k];
            }

            //stockSummaryFillTable("General");
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    stockSummaryTable.requestFocus();
                    if (stockSummaryTable.getRowCount() > 0) {
                        stockSummaryTable.setRowSelectionInterval(0, 0);
                    }

                }
            });
            
            // HERE ARE THE KEY BINDINGS
            this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).
                    put(KeyStroke.getKeyStroke(KeyEvent.VK_W,
                                    java.awt.event.InputEvent.SHIFT_DOWN_MASK
                                    | java.awt.event.InputEvent.CTRL_DOWN_MASK), "forward");
            this.getActionMap().put("forward", new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (isDuplicateColumnsVisible) {
                        isDuplicateColumnsVisible = false;
                    } else {
                        isDuplicateColumnsVisible = true;
                    }
                    fillStockSummary(listOfStockGroupToShowOnTable);
                }
            });
            // END OF KEY BINDINGS
            fillStockSummary(listOfStockGroupToShowOnTable);
        } catch (Exception e) {
            log.error("StockSummary:", e);
        }
    }

  

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headingPanel = new javax.swing.JPanel();
        leftHeadingLabel = new javax.swing.JLabel();
        rightHeadingLabel = new javax.swing.JLabel();
        pdfButton = new javax.swing.JButton();
        xlsButton = new javax.swing.JButton();
        fromDateChooser = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();
        toDateChooser = new com.toedter.calendar.JDateChooser();
        jLabel2 = new javax.swing.JLabel();
        filterButton = new javax.swing.JButton();
        bodyPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        stockSummaryTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.BorderLayout());

        headingPanel.setBackground(new java.awt.Color(47, 105, 142));
        headingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        headingPanel.setPreferredSize(new java.awt.Dimension(1124, 50));

        leftHeadingLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        leftHeadingLabel.setForeground(new java.awt.Color(255, 255, 255));
        leftHeadingLabel.setText("Stock Summary");

        rightHeadingLabel.setForeground(new java.awt.Color(255, 255, 255));
        rightHeadingLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        pdfButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/pdf-icon.png"))); // NOI18N
        pdfButton.setText("PDF");
        pdfButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdfButtonActionPerformed(evt);
            }
        });

        xlsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/xls-icon.png"))); // NOI18N
        xlsButton.setText("XLS");
        xlsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xlsButtonActionPerformed(evt);
            }
        });

        fromDateChooser.setDateFormatString("dd/MM/yyyy");
        fromDateChooser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fromDateChooserMouseClicked(evt);
            }
        });
        fromDateChooser.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                fromDateChooserFocusLost(evt);
            }
        });
        fromDateChooser.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                fromDateChooserPropertyChange(evt);
            }
        });
        fromDateChooser.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                fromDateChooserInputMethodTextChanged(evt);
            }
        });

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("From");

        toDateChooser.setDateFormatString("dd/MM/yyyy");
        toDateChooser.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                toDateChooserFocusGained(evt);
            }
        });
        toDateChooser.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                toDateChooserPropertyChange(evt);
            }
        });

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("To");

        filterButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cloudsendsoft/inventory/icons/search.png"))); // NOI18N
        filterButton.setText("Filter");
        filterButton.setOpaque(false);
        filterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filterButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout headingPanelLayout = new javax.swing.GroupLayout(headingPanel);
        headingPanel.setLayout(headingPanelLayout);
        headingPanelLayout.setHorizontalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headingPanelLayout.createSequentialGroup()
                .addComponent(leftHeadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 67, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fromDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(toDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(filterButton)
                .addGap(33, 33, 33)
                .addComponent(pdfButton, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xlsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(rightHeadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        headingPanelLayout.setVerticalGroup(
            headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, headingPanelLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                        .addComponent(fromDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)
                        .addComponent(toDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2)
                        .addComponent(filterButton))
                    .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(headingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(leftHeadingLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                            .addComponent(xlsButton)
                            .addComponent(pdfButton))
                        .addComponent(rightHeadingLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        add(headingPanel, java.awt.BorderLayout.PAGE_START);

        bodyPanel.setBackground(new java.awt.Color(255, 255, 255));
        bodyPanel.setOpaque(false);

        stockSummaryTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        stockSummaryTable.setOpaque(false);
        stockSummaryTable.setSelectionBackground(new java.awt.Color(153, 153, 153));
        stockSummaryTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stockSummaryTableMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                stockSummaryTableMouseEntered(evt);
            }
        });
        stockSummaryTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                stockSummaryTableKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                stockSummaryTableKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(stockSummaryTable);

        javax.swing.GroupLayout bodyPanelLayout = new javax.swing.GroupLayout(bodyPanel);
        bodyPanel.setLayout(bodyPanelLayout);
        bodyPanelLayout.setHorizontalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1124, Short.MAX_VALUE)
        );
        bodyPanelLayout.setVerticalGroup(
            bodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 397, Short.MAX_VALUE)
        );

        add(bodyPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents
      

    

    public void fillStockSummary(List<StockGroup> listOfStockGroupToShow) {
        int slNo = 0;
        if (type.equalsIgnoreCase("Stock Group")) {
            //Stock Groups Page
            leftHeadingLabel.setText("Stock Summary");
            rightHeadingLabel.setText(particularsField);
            //table columns,model & size
            stockSummaryTable.setShowGrid(false);
            stockSummaryTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                    null,
                    new String[]{"Sr. No.", "Particulars", "Type", "Quantity", "Rate", "Value"}
            ));
            stockSummaryTableModel = (CTableModel) stockSummaryTable.getModel();
            CommonService.setWidthAsPercentages(stockSummaryTable, .04, .60, .30, .20, .20, .20);

            stockSummaryTableModel.setRowCount(0);
            //for (StockGroup stockGroup : listOfStockGroupToShow) {
            Iterator<StockGroup> stockIterator = listOfStockGroupToShow.iterator();
            while (stockIterator.hasNext()) {
                StockGroup stockGroup = stockIterator.next();
                closingQtyValue = closingStockDAO.getClosingStockBasedOnDateRange(stockGroup,fromDate,toDate);
                //for (Object object : closingQtyValue) {
                Iterator<Object> resultIter = closingQtyValue.iterator();
                while (resultIter.hasNext()) {
                    Object object = resultIter.next();
                    Map row = (Map) object;
                    if (row.get("cqnty") != null && row.get("finalStock") != null) {
                        double closingQty = ((BigDecimal) row.get("cqnty")).doubleValue();
                        double closingValue = ((BigDecimal) row.get("finalStock")).doubleValue();
                        stockSummaryTableModel.addRow(new Object[]{++slNo, stockGroup.getName(), "Stock Group", closingQty, commonService.formatIntoCurrencyAsString(closingValue / closingQty), closingValue});
                    } else {
                        stockIterator.remove();
                    }
                }
            }
            if (closingItemsList != null) {
                for (Object object : closingItemsList) {
                    Map row = (Map) object;
                    if (row.get("item_Name") != null && row.get("quantity") != null && row.get("closingStock") != null) {
                        double itemClosingQty = ((BigDecimal) row.get("quantity")).doubleValue();
                        double itemClosingValue = ((BigDecimal) row.get("closingStock")).doubleValue();
                        stockSummaryTableModel.addRow(new Object[]{++slNo, row.get("item_Name"), "Item", itemClosingQty, commonService.formatIntoCurrencyAsString(itemClosingValue / itemClosingQty), itemClosingValue});
                    }
                }
            }
        } else {

            double openingQty = 0.00;
            double openingBal = 0.00;

            double inwardsQty = 0.00;
            double inwardsValue = 0.00;
            double outwardsQty = 0.00;
            double outwardsValue = 0.00;

            double closingQty = 0.00;
            double closingBal = 0.00;

            double totalAmount = 0.00;
            double totalQuantity = 0.00;
            double totalSalesQty = 0.00;
            double closingQuantity = 0;

            int count = 0;
            
            listOfPurchase = purchaseDAO.findAllByBillDateASC(fromDate, toDate);
            listOfSales = salesDAO.findAllByDate(fromDate, toDate);
            listOfPurchaseReturn = purchaseReturnDAO.findAllByDate(fromDate, toDate);
            listOfSalesReturn = salesReturnDAO.findAllByDate(fromDate, toDate);
            
            leftHeadingLabel.setText("Stock Vouchers");
            rightHeadingLabel.setText(particularsField + " " + CommonService.sqlDateToString(company.getStartDate()) + " To " + CommonService.sqlDateToString(company.getEndDate()));
            stockSummaryTable.setModel(new com.cloudsendsoft.inventory.components.CTableModel(
                    null,
                    new String[]{"Date", "Particulars", "Invoice Type", "Invoice No.", "Inwards Qty", "Inwards Value", "Outwards Qty", "Outwards Value", "Closing Qty"}
            ));
            stockSummaryTableModel = (CTableModel) stockSummaryTable.getModel();
            CommonService.setWidthAsPercentages(stockSummaryTable, .05, .35, .10, .10, .10, .10, .10, .10, .10);
            stockSummaryTableModel.setRowCount(0);
            itemList = itemDAO.findItemByDescriptionAndStockGroup(particularsField, stockGroup);
            Iterator<Item> itemIterator = itemList.iterator();
            while (itemIterator.hasNext()) {
                item = itemIterator.next();
                if (item != null) {
                    List<Purchase> purchaseList = new ArrayList<>();
                    List<PurchaseItem> purchaseItemList = new ArrayList<>();
                    List<PurchaseReturn> purchaseReturnList = new ArrayList<>();
                    List<PurchaseReturnItem> purchaseReturnItemList = new ArrayList<>();
                    stockSummaryTableModel.addRow(new Object[]{"", item.getItemCode() + "   " + item.getDescription()});
                    openingQty = item.getOpeningQty();
                    openingBal = item.getOpeningBal();
                    if (isDuplicateColumnsVisible) {
                        openingQty = item.getOpeningQty1();
                        openingBal = item.getOpeningBal1();
                    }
                    leftHeadingLabel.setText("Stock Vouchers");

                    rightHeadingLabel.setText(item.getDescription() + " " + CommonService.sqlDateToString(fromDate) + " To " + CommonService.sqlDateToString(toDate));

                    StockSummaryBean stockSummaryBean = new StockSummaryBean();
                    List<StockSummaryBean> listOfStockSummaryBean = new ArrayList<>();
                    for (Purchase purchase : listOfPurchase) {

                        for (PurchaseItem purchaseItem : purchase.getPurchaseItems()) {
                            //if ((purchaseItem.getItem().getItemCode().equalsIgnoreCase(item.getItemCode())) && (purchaseItem.getItem().getDescription().equalsIgnoreCase(item.getDescription())) && (purchaseItem.getItem().getGodown().getName().equals(item.getGodown().getName()))) {
                              if(purchaseItem.getItem().getId().equals(item.getId())){
                                stockSummaryBean = new StockSummaryBean();
                                stockSummaryBean.setDate(purchase.getBillDate());
                                stockSummaryBean.setParticulars(purchase.getSupplier().getLedgerName());
                                stockSummaryBean.setInvoiceType("Purchase");
                                stockSummaryBean.setInvoiceNumber(purchase.getInvoiceNumber());
                                stockSummaryBean.setOutwardsQty((double) 0.00);
                                stockSummaryBean.setOutwardsValue((double) 0.00);
                                if (isDuplicateColumnsVisible) {
                                    stockSummaryBean.setInwardsQty(purchaseItem.getQuantity() + purchaseItem.getQuantity1());
                                    stockSummaryBean.setInwardsValue((purchaseItem.getQuantity() * purchaseItem.getUnitPrice()) + (purchaseItem.getQuantity1() * purchaseItem.getUnitPrice1()));
                                } else {
                                    stockSummaryBean.setInwardsQty(purchaseItem.getQuantity());
                                    stockSummaryBean.setInwardsValue((purchaseItem.getQuantity() * purchaseItem.getUnitPrice()));
                                }
                                purchaseList.add(purchase);
                                purchaseItemList.add(purchaseItem);
                                listOfStockSummaryBean.add(stockSummaryBean);
                            }
                        }

                    }
                    for (PurchaseReturn purchaseReturn : listOfPurchaseReturn) {
                        
                        for (PurchaseReturnItem purchaseReturnItem : purchaseReturn.getPurchaseReturnItems()) {
                            //if ((purchaseReturnItem.getItem().getItemCode().equalsIgnoreCase(item.getItemCode())) && (purchaseReturnItem.getItem().getDescription().equalsIgnoreCase(item.getDescription())) && (purchaseReturnItem.getItem().getGodown().getName().equals(item.getGodown().getName()))) {
                            if ((purchaseReturnItem.getItem().getId().equals(item.getId()))) {
                                stockSummaryBean = new StockSummaryBean();
                                stockSummaryBean.setDate(purchaseReturn.getBillDate());
                                stockSummaryBean.setParticulars(purchaseReturn.getSupplier().getLedgerName());
                                stockSummaryBean.setInvoiceType("Purchase Return");
                                stockSummaryBean.setInvoiceNumber(purchaseReturn.getInvoiceNumber());
                                stockSummaryBean.setInwardsQty((double) 0.00);
                                stockSummaryBean.setInwardsValue((double) 0.00);
                                if (isDuplicateColumnsVisible) {
                                    stockSummaryBean.setOutwardsQty(purchaseReturnItem.getQuantity() + purchaseReturnItem.getQuantity1());
                                    stockSummaryBean.setOutwardsValue((purchaseReturnItem.getQuantity() * purchaseReturnItem.getUnitPrice()) + (purchaseReturnItem.getQuantity1() * purchaseReturnItem.getUnitPrice1()));
                                } else {
                                    stockSummaryBean.setOutwardsQty(purchaseReturnItem.getQuantity());
                                    stockSummaryBean.setOutwardsValue((purchaseReturnItem.getQuantity() * purchaseReturnItem.getUnitPrice()));
                                }
                                purchaseReturnList.add(purchaseReturn);
                                purchaseReturnItemList.add(purchaseReturnItem);
                                listOfStockSummaryBean.add(stockSummaryBean);
                                break;
                            }
                        }
                    }

                    for (Sales sales : listOfSales) {
                        for (SalesItem salesItem : sales.getSalesItems()) {
                            //if ((salesItem.getItem().getItemCode().equalsIgnoreCase(item.getItemCode())) && (salesItem.getItem().getDescription().equalsIgnoreCase(item.getDescription())) && (salesItem.getItem().getGodown().getName().equals(item.getGodown().getName()))) {
                              if(salesItem.getItem().getId().equals(item.getId())){
                                stockSummaryBean = new StockSummaryBean();
                                stockSummaryBean.setDate(sales.getBillDate());
                                stockSummaryBean.setParticulars(sales.getCustomer().getLedgerName());
                                stockSummaryBean.setInvoiceType("Sales");
                                stockSummaryBean.setInvoiceNumber(sales.getInvoiceNumber());
                                stockSummaryBean.setInwardsQty((double) 0.00);
                                stockSummaryBean.setInwardsValue((double) 0.00);
                                if (isDuplicateColumnsVisible) {
                                    stockSummaryBean.setOutwardsQty(salesItem.getQuantity() + salesItem.getQuantity1());
                                    stockSummaryBean.setOutwardsValue((salesItem.getQuantity() + salesItem.getQuantity1()) * salesItem.getUnitPrice());
                                } else {
                                    stockSummaryBean.setOutwardsQty(salesItem.getQuantity());
                                    stockSummaryBean.setOutwardsValue((salesItem.getQuantity()) * salesItem.getUnitPrice());
                                }
                                listOfStockSummaryBean.add(stockSummaryBean);
                                break;
                            }
                        }
                    }
                    for (SalesReturn salesReturn : listOfSalesReturn) {
                        for (SalesReturnItem salesReturnItem : salesReturn.getSalesReturnItems()) {
                            //if ((salesReturnItem.getItem().getItemCode().equalsIgnoreCase(item.getItemCode())) && (salesReturnItem.getItem().getDescription().equalsIgnoreCase(item.getDescription())) && (salesReturnItem.getItem().getGodown().getName().equals(item.getGodown().getName()))) {
                              if(salesReturnItem.getItem().getId().equals(item.getId())){
                                stockSummaryBean = new StockSummaryBean();
                                stockSummaryBean.setDate(salesReturn.getBillDate());
                                stockSummaryBean.setParticulars(salesReturn.getCustomer().getLedgerName());
                                stockSummaryBean.setInvoiceType("Sales");
                                stockSummaryBean.setInvoiceNumber(salesReturn.getInvoiceNumber());
                                stockSummaryBean.setOutwardsQty((double) 0.00);
                                stockSummaryBean.setOutwardsValue((double) 0.00);
                                if (isDuplicateColumnsVisible) {
                                    stockSummaryBean.setInwardsQty(salesReturnItem.getQuantity() + salesReturnItem.getQuantity1());
                                    stockSummaryBean.setInwardsValue((salesReturnItem.getQuantity() * salesReturnItem.getUnitPrice()) + (salesReturnItem.getQuantity1() * salesReturnItem.getUnitPrice()));
                                } else {
                                    stockSummaryBean.setInwardsQty(salesReturnItem.getQuantity());
                                    stockSummaryBean.setInwardsValue((salesReturnItem.getQuantity()));
                                }
                                listOfStockSummaryBean.add(stockSummaryBean);
                                break;
                            }
                        }
                    }
                    Collections.sort(listOfStockSummaryBean);
                    if(openingBal!=0 )
                    {
                        stockSummaryTableModel.addRow(new Object[]{GlobalProperty.company.getStartDate(), "Opening Balance", "", "", openingQty, openingBal, 0, 0, openingQty, openingBal});
                    }
                    
                    for (StockSummaryBean stockSummaryBeans : listOfStockSummaryBean) {
                        stockSummaryTableModel.addRow(new Object[]{stockSummaryBeans.getDate(), stockSummaryBeans.getParticulars(),
                            stockSummaryBeans.getInvoiceType(), stockSummaryBeans.getInvoiceNumber(), stockSummaryBeans.getInwardsQty(), stockSummaryBeans.getInwardsValue(), stockSummaryBeans.getOutwardsQty(), stockSummaryBeans.getOutwardsValue()});
                    }

                    closingBal = 0.00;
                    closingQty = 0.00;
                    inwardsQty = 0.00;
                    inwardsValue = 0.00;
                    outwardsQty = 0.00;
                    outwardsValue = 0.00;

                    closingQty = 0.00;
                    closingBal = 0.00;

                    inwardsValue = 0.00;
                    outwardsValue = 0.00;
                    inwardsQty = 0.00;
                    outwardsQty = 0.00;

                    //get all rows from jtable
                    for (count++; count < stockSummaryTableModel.getRowCount(); count++) {
                        if (!stockSummaryTableModel.getValueAt(count, 0).toString().isEmpty()) {
                            inwardsQty = Double.parseDouble(stockSummaryTableModel.getValueAt(count, 4) + "");
                            inwardsValue = Double.parseDouble(stockSummaryTableModel.getValueAt(count, 5) + "");

                            closingQty += inwardsQty;

                            outwardsQty = Double.parseDouble(stockSummaryTableModel.getValueAt(count, 6) + "");
                            outwardsValue = Double.parseDouble(stockSummaryTableModel.getValueAt(count, 7) + "");
                            closingQty -= outwardsQty;

                            closingQuantity += closingQty;
                            for (PurchaseItem purItem : purchaseItemList) {
                                if (closingQty != 0) {
                                    double unitPrice = purItem.getUnitPrice();
                                    double quantity = purItem.getQuantity();
                                    if (quantity <= closingQty) {
                                        closingBal += quantity * unitPrice;
                                        closingQty -= quantity;
                                    } else {
                                        closingBal += closingQty * unitPrice;
                                        closingQty = 0;
                                        break;
                                    }
                                }

                            }
                            if (closingQty != 0) {
                                closingBal += closingQty * item.getOpeningUnitPrice();
                                closingQty = 0;
                            }
                            closingQty = 0;
                            closingBal = 0;
                            if (!stockSummaryTableModel.getValueAt(count, 0).toString().isEmpty()) {
                                Date sqlDate = (Date) stockSummaryTableModel.getValueAt(count, 0);
                                stockSummaryTableModel.setValueAt(CommonService.sqlDateToString(sqlDate), count, 0);
                            }
                            if (!stockSummaryTableModel.getValueAt(count, 1).toString().equalsIgnoreCase("Opening Balance")) {
                                stockSummaryTableModel.setValueAt(commonService.blankWhenZero(inwardsQty), count, 4);
                                stockSummaryTableModel.setValueAt((inwardsValue > 0) ? commonService.formatIntoCurrencyAsString(inwardsValue) : commonService.blankWhenZero(inwardsValue), count, 5);
                                stockSummaryTableModel.setValueAt(commonService.blankWhenZero(outwardsQty), count, 6);
                                stockSummaryTableModel.setValueAt((outwardsValue > 0) ? commonService.formatIntoCurrencyAsString(outwardsValue) : commonService.blankWhenZero(outwardsValue), count, 7);
                                stockSummaryTableModel.setValueAt(commonService.blankWhenZero(closingQuantity), count, 8);

                            } else {
                                stockSummaryTableModel.setValueAt(commonService.blankWhenZero(openingQty), count, 4);
                                stockSummaryTableModel.setValueAt((openingBal > 0) ? commonService.formatIntoCurrencyAsString(openingBal) : commonService.blankWhenZero(openingBal), count, 5);
                                stockSummaryTableModel.setValueAt("", count, 6);
                                stockSummaryTableModel.setValueAt("", count, 7);
                                stockSummaryTableModel.setValueAt(openingQty, count, 8);
                            }

                        }
                    }

                }
            }

        }
    }

    

   
    private void stockSummaryTableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_stockSummaryTableKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            if (stockGroup.getName().equalsIgnoreCase("General")) {
                type = "Stock Group";
                closingItemsList.clear();
                listOfStockGroupToShowOnTable.clear();
                listOfStockGroupToShowOnTable.add(stockGroup);
            } else if (type.equalsIgnoreCase("Stock Group")) {
                    stockGroup = stockGroup.getUnderGroup();
                    particularsField = stockGroup.getName();
                    closingItemsList = closingStockDAO.getStockListBasedOnDateRange(stockGroup,fromDate,toDate);
                    listOfStockGroupToShowOnTable.clear();
                    for (StockGroup stockGroup : listOfStockGroup) {
                        if (stockGroup.getUnderGroup() != null) {
                            if (this.stockGroup.getId() == stockGroup.getUnderGroup().getId()) {
                                listOfStockGroupToShowOnTable.add(stockGroup);
                            }
                        }
                    }
                }
            else
            {
                stockGroup=item.getStockGroup();
                type="Stock Group";
                System.out.println("Stock name::"+stockGroup.getName());
                particularsField = stockGroup.getName();
                    closingItemsList = closingStockDAO.getStockListBasedOnDateRange(stockGroup,fromDate,toDate);
                    listOfStockGroupToShowOnTable.clear();
                    for (StockGroup stockGroup : listOfStockGroup) {
                        if (stockGroup.getUnderGroup() != null) {
                            if (this.stockGroup.getId() == stockGroup.getUnderGroup().getId()) {
                                listOfStockGroupToShowOnTable.add(stockGroup);
                            }
                        }
                    }
            }
            
            fillStockSummary(listOfStockGroupToShowOnTable);

        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            index = stockSummaryTable.getSelectedRow();
            type = (String) stockSummaryTableModel.getValueAt(index, 2);
            particularsField = (String) stockSummaryTableModel.getValueAt(index, 1);
            if (type.equalsIgnoreCase("Stock Group")) {

                stockGroup = listOfStockGroupToShowOnTable.get(index);
                closingItemsList = closingStockDAO.getStockListBasedOnDateRange(stockGroup,fromDate,toDate);
                listOfStockGroupToShowOnTable.clear();
                for (StockGroup stockGroup : listOfStockGroup) {
                    if (stockGroup.getUnderGroup() != null) {
                        if (this.stockGroup.getId() == stockGroup.getUnderGroup().getId()) {
                            listOfStockGroupToShowOnTable.add(stockGroup);
                        }
                    }
                }
            }

            fillStockSummary(listOfStockGroupToShowOnTable);
        }
    }//GEN-LAST:event_stockSummaryTableKeyPressed

    private void stockSummaryTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stockSummaryTableMouseClicked

        try {
            if (evt.getClickCount() == 2) {
                if(!rightHeadingLabel.getText().equalsIgnoreCase("Stock Vouchers"))
                {
                    index = stockSummaryTable.getSelectedRow();
                    type = (String) stockSummaryTableModel.getValueAt(index, 2);
                    particularsField = (String) stockSummaryTableModel.getValueAt(index, 1);
                    if (type.equalsIgnoreCase("Stock Group")) {
                        String stockName = (String) stockSummaryTableModel.getValueAt(index, 1);
                        stockGroup = listOfStockGroupToShowOnTable.get(index);
                        closingItemsList = closingStockDAO.getStockListBasedOnDateRange(stockGroup,fromDate,toDate);
                        listOfStockGroupToShowOnTable.clear();
                        for (StockGroup stockGroup : listOfStockGroup) {
                            if (stockGroup.getUnderGroup() != null) {
                                if (this.stockGroup.getId() == stockGroup.getUnderGroup().getId()) {
                                    listOfStockGroupToShowOnTable.add(stockGroup);
                                }
                            }
                        }
                    } else {

                    }
                    fillStockSummary(listOfStockGroupToShowOnTable);
                }
                
            }
        } catch (Exception e) {
            log.error("stockSummaryTableMouseClicked", e);
        }
    }//GEN-LAST:event_stockSummaryTableMouseClicked

    private void pdfButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdfButtonActionPerformed
        try {
            JTableReport jTableReport = new JTableReport();
            if (screen == 1) {
                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_StockGroup", 5, "pdf");
            } else if (screen == 2) {
                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_Item", 7, "pdf");
            } //            else if (screen == 3) {
            //                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_Monthly", 7, "pdf");
            //            } 
            else if (screen == 3) {
                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_StockVoucher", 9, "pdf");
            }
        } catch (Exception ex) {
            log.error("pdfButtonActionPerformed:", ex);
        }
    }//GEN-LAST:event_pdfButtonActionPerformed

    private void xlsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xlsButtonActionPerformed
        try {
            JTableReport jTableReport = new JTableReport();
            if (screen == 1) {
                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_StockGroup", 5, "xls");
            } else if (screen == 2) {
                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_Item", 7, "xls");
//            } else if (screen == 3) {
//                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_Monthly", 7, "xls");
            } else if (screen == 3) {
                jTableReport.createJTableReport(stockSummaryTable, "StockSummary_StockVoucher", 9, "xls");
            }
        } catch (Exception ex) {
            log.error("xlsButtonActionPerformed:", ex);
        }
    }//GEN-LAST:event_xlsButtonActionPerformed

    private void stockSummaryTableMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stockSummaryTableMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_stockSummaryTableMouseEntered

    private void stockSummaryTableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_stockSummaryTableKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_stockSummaryTableKeyReleased

    private void fromDateChooserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fromDateChooserMouseClicked
        try {
            fromDate =commonService.jDateChooserToSqlDate(fromDateChooser);
            toDateChooser.setMinSelectableDate(fromDate);
            System.out.println("fromDate="+fromDate);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(PLAccountView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_fromDateChooserMouseClicked

    private void fromDateChooserFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fromDateChooserFocusLost

    }//GEN-LAST:event_fromDateChooserFocusLost

    private void fromDateChooserPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_fromDateChooserPropertyChange
        toDateChooser.setMinSelectableDate(fromDateChooser.getDate());
    }//GEN-LAST:event_fromDateChooserPropertyChange

    private void fromDateChooserInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_fromDateChooserInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_fromDateChooserInputMethodTextChanged

    private void toDateChooserFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_toDateChooserFocusGained

    }//GEN-LAST:event_toDateChooserFocusGained

    private void toDateChooserPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_toDateChooserPropertyChange
        fromDateChooser.setMaxSelectableDate(toDateChooser.getDate());
    }//GEN-LAST:event_toDateChooserPropertyChange

    private void filterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filterButtonActionPerformed
        try {
            fromDate =commonService.jDateChooserToSqlDate(fromDateChooser);
            toDate = commonService.jDateChooserToSqlDate(toDateChooser);

        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(PLAccountView.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (type.equalsIgnoreCase("Stock Group")&&!stockGroup.getName().equalsIgnoreCase("General")) {
            closingItemsList = closingStockDAO.getStockListBasedOnDateRange(stockGroup,fromDate,toDate);
//                listOfStockGroupToShowOnTable.clear();
//                for (StockGroup stockGroup : listOfStockGroup) {
//                    if (stockGroup.getUnderGroup() != null) {
//                        if (this.stockGroup.getId() == stockGroup.getUnderGroup().getId()) {
//                            listOfStockGroupToShowOnTable.add(stockGroup);
//                        }
//                    }
//                }
            }
        fillStockSummary(listOfStockGroupToShowOnTable);

    }//GEN-LAST:event_filterButtonActionPerformed

   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bodyPanel;
    private javax.swing.JButton filterButton;
    private com.toedter.calendar.JDateChooser fromDateChooser;
    private javax.swing.JPanel headingPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel leftHeadingLabel;
    private javax.swing.JButton pdfButton;
    private javax.swing.JLabel rightHeadingLabel;
    private javax.swing.JTable stockSummaryTable;
    private com.toedter.calendar.JDateChooser toDateChooser;
    private javax.swing.JButton xlsButton;
    // End of variables declaration//GEN-END:variables
}
