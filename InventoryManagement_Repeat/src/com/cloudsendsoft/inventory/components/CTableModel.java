/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.components;

import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author user
 */
public class CTableModel extends DefaultTableModel{

    @Override
    public boolean isCellEditable(int row, int column) {
       // return super.isCellEditable(row, column); //To change body of generated methods, choose Tools | Templates.
       return false;
    }

    public CTableModel() {
    }

    public CTableModel(int rowCount, int columnCount) {
        super(rowCount, columnCount);
    }

    public CTableModel(Vector columnNames, int rowCount) {
        super(columnNames, rowCount);
    }

    public CTableModel(Object[] columnNames, int rowCount) {
        super(columnNames, rowCount);
    }

    public CTableModel(Vector data, Vector columnNames) {
        super(data, columnNames);
    }

    public CTableModel(Object[][] data, Object[] columnNames) {
        super(data, columnNames);
    }
    
    
}
