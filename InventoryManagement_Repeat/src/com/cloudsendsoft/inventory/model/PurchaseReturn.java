/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.model;

import java.sql.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author Sangeeth
 */
@Entity
public class PurchaseReturn {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    
    @Column(name="billDate")
    private Date billDate;
    
    @Column(name = "invoiceNumber", nullable = false)
    String invoiceNumber;
    
    @ManyToOne
    Ledger supplier;
    
    @Column(name="deliveryDate")
    private String deliveryDate;
    
     
    @Column(name="grandTotal", columnDefinition="Decimal(10,2) default '0.00'")
    Double grandTotal;
    
    @Column(name="grandTotal1", columnDefinition="Decimal(10,2) default '0.00'")
    Double grandTotal1;
    
    @ManyToOne
    MultiCurrency multiCurrency;
    
    @ManyToOne
    Purchase purchase;
    
    @ManyToOne
    ShipmentMode shipmentMode;
    
    @ManyToOne
    Company company;
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    List<PurchaseReturnItem> purchaseReturnItems;
         
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    List<PurchaseReturnCharge> purchaseReturnCharges;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }
    
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Ledger getSupplier() {
        return supplier;
    }

    public void setSupplier(Ledger supplier) {
        this.supplier = supplier;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    
    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public MultiCurrency getMultiCurrency() {
        return multiCurrency;
    }

    public void setMultiCurrency(MultiCurrency multiCurrency) {
        this.multiCurrency = multiCurrency;
    }

    public ShipmentMode getShipmentMode() {
        return shipmentMode;
    }

    public void setShipmentMode(ShipmentMode shipmentMode) {
        this.shipmentMode = shipmentMode;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    
    public List<PurchaseReturnItem> getPurchaseReturnItems() {
        return purchaseReturnItems;
    }

    public void setPurchaseReturnItems(List<PurchaseReturnItem> purchaseReturnItems) {
        this.purchaseReturnItems = purchaseReturnItems;
    }

    public List<PurchaseReturnCharge> getPurchaseReturnCharges() {
        return purchaseReturnCharges;
    }

    public void setPurchaseReturnCharges(List<PurchaseReturnCharge> purchaseReturnCharges) {
        this.purchaseReturnCharges = purchaseReturnCharges;
    }

    public Double getGrandTotal1() {
        return grandTotal1;
    }

    public void setGrandTotal1(Double grandTotal1) {
        this.grandTotal1 = grandTotal1;
    }

}
