/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.model;

import java.sql.Date;
import java.util.Currency;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author Sangeeth
 */
@Entity
public class SalesPreforma {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    
    @Column(name="billDate")
    private Date billDate;
    
    @Column(name="expiryDate")
    private Date expiryDate;
    
    @Column(name = "invoiceNumber", nullable = false)
    String invoiceNumber;
    
    @Column(name = "address")
    String address;
        
    @ManyToOne
    Ledger customer;
    
    @Column(name="delivery")
    String delivery;
    
    @Column(name = "paymentTerms")
    String paymentTerms;
    
    @Column(name = "deleveryTerms")
    String deleveryTerms;
    
    @Column(name = "packing")
    String packing;
    
    @Column(name="discount",columnDefinition="Decimal(10,2) default '0.00'")
    Double discount;
    
    @Column(name = "currentCurrencyRate",columnDefinition="Decimal(10,2) default '0.00'")
    Double currentCurrencyRate=0.00;
    
    @Column(name="grandTotal",columnDefinition="Decimal(10,2) default '0.00'")
    Double grandTotal;
           
    @ManyToOne
    MultiCurrency multiCurrency;
    
    @ManyToOne
    Company company;
    
    @OneToOne(cascade = CascadeType.ALL)
    CommercialInvoice commercialInvoice;
    
    @OneToOne(cascade = CascadeType.ALL)
    PackingList packingList;
    
    @ManyToOne
    ShipmentMode shipmentMode;
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    List<SalesPreformaItem> salesPreformaItems;
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    List<SalesPreformaTax> salesPreformaTaxs;
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    List<SalesPreformaCharge> salesPreformaCharges;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
    
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Ledger getCustomer() {
        return customer;
    }

    public void setCustomer(Ledger customer) {
        this.customer = customer;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getPaymentTerms() {
        return paymentTerms;
    }

    public void setPaymentTerms(String paymentTerms) {
        this.paymentTerms = paymentTerms;
    }

    public String getDeleveryTerms() {
        return deleveryTerms;
    }

    public void setDeleveryTerms(String deleveryTerms) {
        this.deleveryTerms = deleveryTerms;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getCurrentCurrencyRate() {
        return currentCurrencyRate;
    }

    public void setCurrentCurrencyRate(Double currentCurrencyRate) {
        this.currentCurrencyRate = currentCurrencyRate;
    }

    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

   
    public MultiCurrency getMultiCurrency() {
        return multiCurrency;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public void setMultiCurrency(MultiCurrency multiCurrency) {
        this.multiCurrency = multiCurrency;
    }

    public CommercialInvoice getCommercialInvoice() {
        return commercialInvoice;
    }

    public void setCommercialInvoice(CommercialInvoice commercialInvoice) {
        this.commercialInvoice = commercialInvoice;
    }
    
    public ShipmentMode getShipmentMode() {
        return shipmentMode;
    }

    public void setShipmentMode(ShipmentMode shipmentMode) {
        this.shipmentMode = shipmentMode;
    }

    public List<SalesPreformaItem> getSalesPreformaItems() {
        return salesPreformaItems;
    }

    public void setSalesPreformaItems(List<SalesPreformaItem> salesPreformaItems) {
        this.salesPreformaItems = salesPreformaItems;
    }

    public List<SalesPreformaTax> getSalesPreformaTaxs() {
        return salesPreformaTaxs;
    }

    public void setSalesPreformaTaxs(List<SalesPreformaTax> salesPreformaTaxs) {
        this.salesPreformaTaxs = salesPreformaTaxs;
    }

    public List<SalesPreformaCharge> getSalesPreformaCharges() {
        return salesPreformaCharges;
    }

    public void setSalesPreformaCharges(List<SalesPreformaCharge> salesPreformaCharges) {
        this.salesPreformaCharges = salesPreformaCharges;
    }

    public PackingList getPackingList() {
        return packingList;
    }

    public void setPackingList(PackingList packingList) {
        this.packingList = packingList;
    }

    
   
    
    
}
