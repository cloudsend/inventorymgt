/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.model;

import java.sql.Date;
import java.util.Currency;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author Sangeeth
 */
@Entity
public class PackingList {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    
    private Date invoiceDate;
    
    @Column(name = "invoiceNumber")
    String invoiceNumber;
    
    @Column(name = "address")
    String address;
    
    Long numberOfPack;
    
    //for commercial invoice
    String totalNumberOfPackages;
    String casesList;
    String netWeight;
    String grossWeight;
    
    @ManyToOne
    Ledger customer;
    
    /*@ManyToOne
    Company company;*/
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    List<PackingListItem> PackingListItems;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Ledger getCustomer() {
        return customer;
    }

    public void setCustomer(Ledger customer) {
        this.customer = customer;
    }

    /*public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }*/
    
    public List<PackingListItem> getPackingListItems() {
        return PackingListItems;
    }

    public void setPackingListItems(List<PackingListItem> PackingListItems) {
        this.PackingListItems = PackingListItems;
    }

    public Long getNumberOfPack() {
        return numberOfPack;
    }

    public void setNumberOfPack(Long numberOfPack) {
        this.numberOfPack = numberOfPack;
    }

    public String getTotalNumberOfPackages() {
        return totalNumberOfPackages;
    }

    public void setTotalNumberOfPackages(String totalNumberOfPackages) {
        this.totalNumberOfPackages = totalNumberOfPackages;
    }

    
    public String getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(String netWeight) {
        this.netWeight = netWeight;
    }

    public String getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(String grossWeight) {
        this.grossWeight = grossWeight;
    }

    public String getCasesList() {
        return casesList;
    }

    public void setCasesList(String casesList) {
        this.casesList = casesList;
    }

    
}
