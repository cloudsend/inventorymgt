/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.model;

import java.sql.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author Sangeeth
 */
@Entity
public class PurchaseOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "address")
    String address;
    
    @Column(name="billDate")
    private Date billDate;
    
    @Column(name="expiryDate")
    private Date expiryDate;
    
    @Column(name = "invoiceNumber", nullable = false)
    String invoiceNumber;
    
    @ManyToOne
    Ledger supplier;
    
    @Column(name="deliveryDate")
    String deliveryDate;
    
    @Column(name="discount", columnDefinition="Decimal(10,2) default '0.00'")
    Double discount;
    
    @Column(name="grandTotal", columnDefinition="Decimal(10,2) default '0.00'")
    Double grandTotal;
    
    @Column(name = "currentCurrencyRate",columnDefinition="Decimal(10,2) default '0.00'")
    Double currentCurrencyRate=0.00;
    
    @ManyToOne
    MultiCurrency multiCurrency;
    
    @ManyToOne
    Company company;
    
    @ManyToOne
    ShipmentMode shipmentMode;
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    List<PurchaseOrderItem> purchaseOrderItems;
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    List<PurchaseOrderTax> purchaseOrderTaxes;
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    List<PurchaseOrderCharge> purchaseOrderCharges;

    public List<PurchaseOrderItem> getPurchaseOrderItems() {
        return purchaseOrderItems;
    }

    public void setPurchaseOrderItems(List<PurchaseOrderItem> purchaseOrderItems) {
        this.purchaseOrderItems = purchaseOrderItems;
    }

    public List<PurchaseOrderTax> getPurchaseOrderTaxes() {
        return purchaseOrderTaxes;
    }

    public void setPurchaseOrderTaxes(List<PurchaseOrderTax> purchaseOrderTaxes) {
        this.purchaseOrderTaxes = purchaseOrderTaxes;
    }

    public List<PurchaseOrderCharge> getPurchaseOrderCharges() {
        return purchaseOrderCharges;
    }

    public void setPurchaseOrderCharges(List<PurchaseOrderCharge> purchaseOrderCharges) {
        this.purchaseOrderCharges = purchaseOrderCharges;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
    
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Ledger getSupplier() {
        return supplier;
    }

    public void setSupplier(Ledger supplier) {
        this.supplier = supplier;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getCurrentCurrencyRate() {
        return currentCurrencyRate;
    }

    public void setCurrentCurrencyRate(Double currentCurrencyRate) {
        this.currentCurrencyRate = currentCurrencyRate;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
    
    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public MultiCurrency getMultiCurrency() {
        return multiCurrency;
    }

    public void setMultiCurrency(MultiCurrency multiCurrency) {
        this.multiCurrency = multiCurrency;
    }

    public ShipmentMode getShipmentMode() {
        return shipmentMode;
    }

    public void setShipmentMode(ShipmentMode shipmentMode) {
        this.shipmentMode = shipmentMode;
    }
    
    
    
}
