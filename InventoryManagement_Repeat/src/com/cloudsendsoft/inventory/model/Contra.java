/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudsendsoft.inventory.model;

import java.sql.Date;
import java.util.Currency;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author Sangeeth
 */
@Entity
public class Contra {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    
    @Column(name="contraNo")
    private String contraNo;
    
    @Column(name="contraDate")
    private Date contraDate;
    
    @Column(name="amount",columnDefinition="Decimal(10,2) default '0.00'")
    private Double amount;
    
    @Column(name="narration")
    private String narration;
    
    @ManyToOne
    Ledger ledgerBy;
    
    @ManyToOne
    Ledger ledgerTo;
    
    @ManyToOne
    Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContraNo() {
        return contraNo;
    }

    public void setContraNo(String contraNo) {
        this.contraNo = contraNo;
    }

    public Date getContraDate() {
        return contraDate;
    }

    public void setContraDate(Date contraDate) {
        this.contraDate = contraDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public Ledger getLedgerBy() {
        return ledgerBy;
    }

    public void setLedgerBy(Ledger ledgerBy) {
        this.ledgerBy = ledgerBy;
    }

    public Ledger getLedgerTo() {
        return ledgerTo;
    }

    public void setLedgerTo(Ledger ledgerTo) {
        this.ledgerTo = ledgerTo;
    }

    
    
}
