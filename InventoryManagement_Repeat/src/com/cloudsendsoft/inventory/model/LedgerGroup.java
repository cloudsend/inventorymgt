package com.cloudsendsoft.inventory.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author sangeeth
 */

@Entity
@Table(name = "LedgerGroup")
public class LedgerGroup {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    Integer id;
    
    @Column(name = "groupName", nullable = false,unique=true)
    String groupName;
    
    //@Column(name = "stmntType", nullable = true)
    //String stmntType;
    
    @Column(name = "accountType", nullable = true)
    String accountType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
    
}
