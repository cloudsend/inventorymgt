
package com.cloudsendsoft.inventory.model;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Sangeeth
 */
@Entity
public class StockMovement {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    Long id;
    
    @Column(name="Date")
    private Date dateOfStockMovement;
    
    @Column(name = "itemName")
    String name;
    
    @ManyToOne
    Company company;
    
    @Column(name="itemCode")
    String itemCode;
    
    @Column(name="AvailableQty",columnDefinition="Decimal(10,2) default '0.00'")
    double availQty;
    
     @Column(name="AvailableQty1",columnDefinition="Decimal(10,2) default '0.00'")
    double availQty1;
    
    @Column(name="QtyToMove",columnDefinition="Decimal(10,2) default '0.00'")
    double QtyToMove;
    
    @Column(name="QtyToMove1",columnDefinition="Decimal(10,2) default '0.00'")
    double QtyToMove1;
    
    @Column (name="fromGodown")
    String fromGodown;
    
    @Column (name="toGodown")
    String toGodown;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }
    
    public String getFromGodown() {
        return fromGodown;
    }

    public void setFromGodown(String fromGodown) {
        this.fromGodown = fromGodown;
    }
    
    public String getToGodown() {
        return toGodown;
    }

    public void setToGodown(String toGodown) {
        this.toGodown = toGodown;
    }
    
    public double getAvailableQty() {
        return availQty;
    }

    public void setAvailableQty(double availQty) {
        this.availQty = availQty;
    }
    
    public double getQtyToMove() {
        return QtyToMove;
    }

    public double getAvailableQty1() {
        return availQty1;
    }

    public void setAvailableQty1(double availQty1) {
        this.availQty1 = availQty1;
    }
    public void setQtyToMove(double QtyToMove) {
        this.QtyToMove = QtyToMove;
    }
    
    public double getQtyToMove1() {
        return QtyToMove1;
    }

    public void setQtyToMove1(double QtyToMove1) {
        this.QtyToMove1 = QtyToMove1;
    }
    
    public Date getDate() {
        return dateOfStockMovement;
    }

    public void setDate(Date dateOfStockMovement) {
        this.dateOfStockMovement = dateOfStockMovement;
    }

}
