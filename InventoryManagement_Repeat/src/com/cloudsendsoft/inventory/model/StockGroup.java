
package com.cloudsendsoft.inventory.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author Sangeeth
 */
@Entity
public class StockGroup {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    Long id;
    
    @Column(name = "name")
    String name;
    
    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="group_id")
    private StockGroup underGroup;
 
    @OneToMany(mappedBy="underGroup")
    private Set<StockGroup> stockGroups = new HashSet<StockGroup>();
    
    @ManyToOne
    Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StockGroup getUnderGroup() {
        return underGroup;
    }

    public void setUnderGroup(StockGroup underGroup) {
        this.underGroup = underGroup;
    }

    public Set<StockGroup> getStockGroups() {
        return stockGroups;
    }

    public void setStockGroups(Set<StockGroup> stockGroups) {
        this.stockGroups = stockGroups;
    }

     
}
