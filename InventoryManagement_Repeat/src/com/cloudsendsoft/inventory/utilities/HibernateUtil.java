package com.cloudsendsoft.inventory.utilities;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author sangeeth
 */
public class HibernateUtil {

    private static SessionFactory sessionFactory = null;

    public static SessionFactory getSessionFactory() {
        try {
            if (sessionFactory == null) {
                sessionFactory = new Configuration().configure("/com/cloudsendsoft/inventory/utilities/hibernate.cfg.xml")
                        .buildSessionFactory();
            }
        } catch (HibernateException exception) {
            exception.printStackTrace();
        }
        return sessionFactory;
    }
}
