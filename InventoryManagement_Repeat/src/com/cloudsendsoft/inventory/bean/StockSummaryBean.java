/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.bean;

import java.sql.Date;

/**
 *
 * @author LOGON SOFT 1
 */
public class StockSummaryBean implements Comparable<StockSummaryBean>{
    Date date;
    String particulars;
    String invoiceType;
    String invoiceNumber;
    Double inwardsQty;
    Double inwardsValue;
    Double outwardsQty;
    Double outwardsValue;
    Double closingQty;
    Double closingValue;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getParticulars() {
        return particulars;
    }

    public void setParticulars(String particulars) {
        this.particulars = particulars;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Double getInwardsQty() {
        return inwardsQty;
    }

    public void setInwardsQty(Double inwardsQty) {
        this.inwardsQty = inwardsQty;
    }

    public Double getInwardsValue() {
        return inwardsValue;
    }

    public void setInwardsValue(Double inwardsValue) {
        this.inwardsValue = inwardsValue;
    }

    public Double getOutwardsQty() {
        return outwardsQty;
    }

    public void setOutwardsQty(Double outwardsQty) {
        this.outwardsQty = outwardsQty;
    }

    public Double getOutwardsValue() {
        return outwardsValue;
    }

    public void setOutwardsValue(Double outwardsValue) {
        this.outwardsValue = outwardsValue;
    }

    public Double getClosingQty() {
        return closingQty;
    }

    public void setClosingQty(Double closingQty) {
        this.closingQty = closingQty;
    }

    public Double getClosingValue() {
        return closingValue;
    }

    public void setClosingValue(Double closingValue) {
        this.closingValue = closingValue;
    }



    @Override
    public int compareTo(StockSummaryBean o) {
            return getDate().compareTo(o.getDate());
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

    
    
}
