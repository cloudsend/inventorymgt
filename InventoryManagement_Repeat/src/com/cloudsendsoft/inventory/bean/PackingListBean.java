/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cloudsendsoft.inventory.bean;

/**
 *
 * @author Sangeeth
 */
public class PackingListBean {
    
    Integer srNo;
    String itemCode;
    String description;
    Long numberOfPack;
    Double netWeight;
    Double grossWeight;
    String kindOfPack;
    Double contentOfPack;

    public Integer getSrNo() {
        return srNo;
    }

    public void setSrNo(Integer srNo) {
        this.srNo = srNo;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getNumberOfPack() {
        return numberOfPack;
    }

    public void setNumberOfPack(Long numberOfPack) {
        this.numberOfPack = numberOfPack;
    }

    public Double getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(Double netWeight) {
        this.netWeight = netWeight;
    }

    public Double getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(Double grossWeight) {
        this.grossWeight = grossWeight;
    }

    public String getKindOfPack() {
        return kindOfPack;
    }

    public void setKindOfPack(String kindOfPack) {
        this.kindOfPack = kindOfPack;
    }

    public Double getContentOfPack() {
        return contentOfPack;
    }

    public void setContentOfPack(Double contentOfPack) {
        this.contentOfPack = contentOfPack;
    }

    
    
      
}
